package Zaaksysteem::Backend::Sysin::Modules::KvKAPI;
use Moose;

extends 'Zaaksysteem::Backend::Sysin::Modules';
with qw/
    MooseX::Log::Log4perl
    Zaaksysteem::Backend::Sysin::Modules::Roles::ProcessorParams
/;

=head1 NAME

Zaaksysteem::Backend::Sysin::Modules::KvKAPI - KvK API-koppeling

=head1 DESCRIPTION

This interface deals with the KvK API and logable transactions

=cut

use BTTW::Tools;

use JSON::XS ();
use List::Util qw(first);
use URI;
use Zaaksysteem::BR::Subject::Constants 'REMOTE_SEARCH_MODULE_NAME_KVKAPI';
use Zaaksysteem::BR::Subject;
use Zaaksysteem::External::KvKAPI;
use Zaaksysteem::Object::Types::LegalEntityType;
use Zaaksysteem::Types qw(
    NonEmptyStr
    CompanyCocNumber
    CompanyCocLocationNumber
    CompanyCocRSIN
);

use Zaaksysteem::Event::RMQ;

=head1 CONSTANTS

=head2 INTERFACE_CONFIG_FIELDS

Config fields for this interface, empty for now

=cut

use constant INTERFACE_CONFIG_FIELDS    => [
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_kvkapi_key',
        type        => 'text',
        label       => 'KvK API key',
        required    => 1,
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name  => 'interface_kvkapi_eherkenning_request',
        type  => 'checkbox',
        label => 'Activeren voor eHerkenning',
        description =>
            qq{Bevraag de KvK-API na het inloggen met eHerkenning zodat de meest recente gegevens in Zaaksysteem opgeslagen worden.},
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name  => 'interface_kvkapi_spoof_mode',
        type  => 'checkbox',
        label => 'Gebruik de KvK test-API (spoof mode)',
        description =>
            qq{Als deze optie actief is, wordt de test-API van de KvK gebruikt. Deze API kent slechts een beperkte set (fictieve) bedrijven.},
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name => 'interface_kvkapi_alternative_host',
        type => 'checkbox',
        label => 'Gebruik alternatieve hostname',
        description => 'Als deze optie actief worden API requests via een alternatieve endpoint verwerkt.',
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name => 'interface_kvkapi_host',
        type => 'text',
        label => 'KvK API Hostname',
        when => 'interface_kvkapi_alternative_host',
        description => 'De alternative hostname waarde kan gebruikt worden om af te wijken van de standaard KvK server.',
        data => {
            placeholder => 'example.domain.tld'
        },
    ),
];

=head2 MODULE_SETTINGS

Configuration for this module

=cut

use constant MODULE_SETTINGS => {
    name                          => 'kvkapi',
    label                         => 'KvK API-koppeling',
    interface_config              => INTERFACE_CONFIG_FIELDS,
    direction                     => 'outgoing',
    manual_type                   => [],
    is_multiple                   => 0,
    is_manual                     => 1,
    retry_on_error                => 0,
    sensitive_config_fields       => [qw(kvkapi_key)],
    allow_multiple_configurations => 0,
    is_casetype_interface         => 0,
    trigger_definition            => {
        search_companies => {
            method => 'search_companies',
        },
        remote_import => {
            method => 'remote_import',
        },
        disable_subscription => {
            method => 'disable_subscription',
        },
        enable_object_subscription => {
            method => 'enable_subscription',
        },
    },
    description => qq{
<p>
    Deze koppeling zorgt voor de integratie tussen de API van de Kamer van
    Koophandel en Zaaksysteem. Om gebruik te kunnen maken van deze koppeling
    is een API-key nodig. Deze API-key wordt geleverd bij
    <a target="_blank" href="https://www.kvk.nl/producten-bestellen/kvk-api/">een abonnement bij de
    KvK</a>.
</p>
<p>
    Voor meer informatie kunt u terecht op de
    <a target="_blank" href="http://wiki.zaaksysteem.nl/wiki/Redirect_koppelprofiel_KvK_API_koppeling">
        Zaaksysteem Wiki
    </a>
</p>
},
};

around BUILDARGS => sub {
    my $orig  = shift;
    my $class = shift;

    return $class->$orig( %{ MODULE_SETTINGS() } );
};

=head1 METHODS

=head2 search_companies

    my $transaction = $interface->process_trigger('search_companies', { company => 'Mintlab' });

    my @result      = @{ $transaction->get_processor_params->{result} }

Searches for company entities matching the given params in remote systems

=cut

define_profile search_companies => (
    optional => {
        'address_residence.city' => NonEmptyStr,
    },
    constraint_methods  => {
        'address_residence.zipcode' => sub {
            my $val     = pop;

            return 1 if $val =~ /^[1-9][0-9]{3}[A-Z]{2}$/;
            return;
        },
        'address_residence.street_number' => sub {
            my $val     = pop;

            return 1 if $val =~ /^[0-9]+$/;
            return;
        }
    },
    field_filters   => {
        'address_residence.zipcode' => sub {
            my $val = shift;

            $val =~ s/\s//;

            return uc($val);
        },
    },
    require_some    => {
        coc_or_company_or_zipcode => [1, qw/rsin coc_number coc_location_number address_residence.zipcode company/],
    },
    dependency_groups => {
        zipcode_group       => [qw/address_residence.zipcode address_residence.street_number/],
    }
);

sub search_companies {
    my $self            = shift;
    my $params          = shift;
    my $interface       = shift;

    ### Error when required params are missing, but do not override $params
    $params = assert_profile($params)->valid;

    my $transaction = $interface->process(
        {
            processor_params        => {
                processor       => '_process_search_companies',
                request_params  => $params,
            },
            direction               => 'outgoing',
            external_transaction_id => 'unknown',
            input_data              => JSON::XS->new->utf8(0)->canonical->pretty->encode($params),
        }
    );

    throw(
        'sysin/modules/kvkapi/search_companies/no_results',
        'Failure in getting results',
    ) unless $transaction->processor_params && $transaction->processor_params->{result};

    return $transaction;
}

=head2 _process_search_companies

    $interface->process(
        {
            processor_params        => {
                processor       => '_process_search_companies',
                request_params  => { company => 'Mintlab' }
            },
            direction               => 'outgoing',
            external_transaction_id => 'unknown',
            input_data              => Data::Dumper::Dumper($params),
        }
    );

Actual processor for searching in other party for companies.

=cut

sub _process_search_companies {
    my $self = shift;
    my $record = shift;

    ### Every error is by default fatal (do not retry)
    $self->process_stash->{error_fatal} = 1;

    my $transaction = $self->_transaction;
    my $interface = $transaction->interface;
    my $model = $interface->model;

    my $params    = $transaction->get_processor_params()->{request_params};
    my $kvk_query = $model->map_params_to_kvk_query($params);

    $record->preview_string("Zoek (KvK): " .  dump_terse($kvk_query));
    $transaction->input_data(dump_terse({ filters => $kvk_query }));

    try {
        my $kvk_response = $model->search(%{$kvk_query});
        $record->output(dump_terse($kvk_response));

        my @company_subjects = map { $model->map_kvk_company_to_subject($_) } @$kvk_response;

        $transaction->processor_params(
            {
                %{ $transaction->processor_params },
                result  => \@company_subjects,
            }
        );

    } catch {
        $self->log->error($_);
        $transaction->error_fatal(1);
        $transaction->error_count(1);
        $record->output($_);
    };

    return $record->output();
}

=head1 METHODS

=head2 remote_import

    my $transaction = $interface->process_trigger('remote_import', { coc_number => 1234567 });

    my @result      = @{ $transaction->get_processor_params->{result} }

Retrieves the complete company profile on the remote KvK system

=cut

define_profile remote_import => (
    optional => {
        coc_number          => CompanyCocNumber,
        coc_location_number => CompanyCocLocationNumber,
        rsin                => CompanyCocRSIN,
    },
    require_some => {
        company_identifier => [ 1, qw[coc_number coc_location_number rsin] ]
    }
);

sub remote_import {
    my $self            = shift;
    my $params          = shift;
    my $interface       = shift;

    ### Error when required params are missing, but do not override $params
    $params = assert_profile($params)->valid;

    my $transaction = $interface->process(
        {
            processor_params        => {
                processor       => '_process_remote_import',
                request_params  => $params,
            },
            direction               => 'outgoing',
            external_transaction_id => 'unknown',
            input_data              => JSON::XS->new->utf8(0)->canonical->pretty->encode($params),
        }
    );

    throw(
        'sysin/modules/kvkapi/remote_import/no_results',
        'Failure in getting results',
    ) unless $transaction->processor_params && $transaction->processor_params->{result};

    return $transaction;
}


=head2 _process_remote_import

    $interface->process(
        {
            processor_params        => {
                processor       => '_process_remote_import',
                request_params  => { company => 'Mintlab' }
            },
            direction               => 'outgoing',
            external_transaction_id => 'unknown',
            input_data              => Data::Dumper::Dumper($params),
        }
    );

Actual processor for searching in other party for companies.

=cut

sub _process_remote_import {
    my $self = shift;
    my $record = shift;

    ### Every error is by default fatal (do not retry)
    $self->process_stash->{error_fatal} = 1;

    my $transaction = $self->_transaction;
    my $interface   = $transaction->interface;
    my $model       = $interface->model;

    my $params = $transaction->get_processor_params()->{request_params};
    my $kvk_query = $model->map_params_to_kvk_query($params);

    $record->preview_string(
        sprintf (
            'Profiel (KvK): "%s"',
            dump_terse($kvk_query)
        )
    );

    $transaction->input_data( dump_terse({ filters => $kvk_query }) );

    try {

        my $subject = $self->_import_kvk($record, %{$kvk_query});
        $transaction->processor_params(
            {
                %{ $transaction->processor_params },
                result => $subject,
            }
        );
    } catch {
        $self->log->error($_);
        $transaction->error_fatal(1);
        $transaction->error_count(1);
        $record->output($_);
        $record->is_error(1);
    };

    return $record->output()
}

sub disable_subscription {
    my $self            = shift;
    my $params          = shift;
    my $interface       = shift;

    my $transaction = $interface->process(
        {
            processor_params        => {
                processor       => '_process_disable_subscription',
                request_params  => $params,
            },
            direction               => 'outgoing',
            external_transaction_id => 'unknown',
            input_data              => JSON::XS->new->utf8(0)->canonical->pretty->encode($params),
        }
    );

    return $transaction;
}

sub _process_disable_subscription {
    my ($self, $record) = @_;

    my $transaction = $self->_transaction;
    my $interface   = $transaction->interface;
    my $model       = $interface->model;

    my $params = $transaction->get_processor_params()->{request_params};
    $record->preview_string(
        sprintf (
            'Deleting subscription of KvK: "%s"', dump_terse($params)
        )
    );

    $transaction->input_data(dump_terse($params));

    try {
        my $schema = $interface->result_source->schema;
        my $id = $params->{subscription_id};
        my $os = $schema->resultset('ObjectSubscription')->search_rs(
            {
                id           => $id,
                local_table  => 'Bedrijf',
                interface_id => $interface->id,
            }
        )->first;

        throw(
            'kvk/no_subscription',
            "No subcription with ID $id was found"
        ) if !$os;

        my $company = $schema->resultset('Bedrijf')->find($os->local_id);
        $company->disable_bedrijf;
        $company->discard_changes;
        $os->update({date_deleted => '\NOW()' });

        $record->output("Deleting company succesfull") if $company->deleted_on;
        $record->output("Deactivating company succesfull") if !$company->active;
    }
    catch {
        $self->log->error($_);
        $transaction->error_fatal(1);
        $transaction->error_count(1);
        $record->output($_);
        $record->is_error(1);
    };

    return $record->output()
}

sub enable_subscription {
    my $self            = shift;
    my $params          = shift;
    my $interface       = shift;

    my $transaction = $interface->process(
        {
            processor_params        => {
                processor       => '_process_enable_subscription',
                request_params  => $params,
            },
            direction               => 'outgoing',
            external_transaction_id => 'unknown',
            input_data              => JSON::XS->new->utf8(0)->canonical->pretty->encode($params),
        }
    );

    return $transaction;
}

sub _import_kvk {
    my ($self, $record, %kvk_query) = @_;

    my $transaction = $self->_transaction;
    my $interface   = $transaction->interface;
    my $model       = $interface->model;

    my $kvk_response = $model->profile(%kvk_query);
    $record->output(dump_terse($kvk_response));
    my $subject = $model->map_kvk_company_to_subject($kvk_response);

    my $schema = $interface->result_source->schema;

    my $bridge = Zaaksysteem::BR::Subject->new(
        schema => $schema,
        remote_search => 'kvkapi',
    );

    my $result = $bridge->save($subject);

    my $rmq = Zaaksysteem::Event::RMQ->new(
        schema => $schema,
    );

    my $nnp = $schema->resultset('Bedrijf')->find($result->subject->_table_id);

    my @changes = (
        {
            key       => 'geolocation',
            new_value => $nnp->location_to_geo_json,
            old_value => undef,
        }
    );

    my $event = $rmq->create_updated_company_event($nnp->uuid, @changes);

    $rmq->enqueue_publish_event($event);

    return $result;
}

sub _process_enable_subscription {
    my ($self, $record) = @_;

    my $transaction = $self->_transaction;
    my $interface   = $transaction->interface;
    my $model       = $interface->model;

    my $params = $transaction->get_processor_params()->{request_params};
    $record->preview_string(
        sprintf (
            '(re)enabling subscription of KvK: "%s"', dump_terse($params)
        )
    );

    $transaction->input_data(dump_terse($params));

    try {
        my $schema = $interface->result_source->schema;
        my $id = $params->{subscription_id};
        my $os = $schema->resultset('ObjectSubscription')->search_rs(
            {
                id           => $id,
                local_table  => 'Bedrijf',
                interface_id => $interface->id,
            }
        )->first;

        throw(
            'kvk/no_subscription',
            "No subcription with ID $id was found"
        ) if !$os;

        my $company = $schema->resultset('Bedrijf')->find($os->local_id);

        my %kvk_query;
        if ($company->dossiernummer) {
            $kvk_query{kvkNumber} = $company->dossiernummer;
            $kvk_query{branchNumber} = $company->vestigingsnummer
                if $company->vestigingsnummer;
        }
        else {
            $kvk_query{rsin} = $company->rsin;
        }

        $self->_import_kvk($record, %kvk_query);
    }
    catch {
        $self->log->error($_);
        $transaction->error_fatal(1);
        $transaction->error_count(1);
        $record->output($_);
        $record->is_error(1);
    };

    return $record->output()
}

sub _get_model {
    my ($self, $opts) = @_;

    my $config = $opts->{interface}->get_interface_config;

    my %kvk_args = (
        api_key => $config->{ kvkapi_key },
        spoofmode => $config->{ kvkapi_spoof_mode } // 0
    );

    if ($config->{ kvkapi_alternative_host }) {
        $kvk_args{ api_host } = $config->{ kvkapi_host };
    }

    return Zaaksysteem::External::KvKAPI->new(%kvk_args)
}

sub _transaction {
    return $_[0]->process_stash->{transaction}
}

1;

__END__

=head1 SEE ALSO

L<Zaaksysteem::Manual> L<Zaaksysteem::Manual::Template>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018-2020, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
