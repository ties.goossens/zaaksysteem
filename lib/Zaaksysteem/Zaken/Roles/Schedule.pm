package Zaaksysteem::Zaken::Roles::Schedule;

use Moose::Role;

use Zaaksysteem::Zaken::AdvanceResult;
use BTTW::Tools;

with 'Zaaksysteem::Zaken::Roles::ZaakSetup';


sub get_email_interface {
    my $self = shift;

    ### Get mailer interfae
    return $self
        ->result_source
        ->schema
        ->resultset('Interface')
        ->search_active(
            {
                module => 'email',
            }
        )->first;
}


sub pending_notifications {
    my $self = shift;

    my $transactions    = $self->get_email_interface->process_trigger(
        'get_pending',
        {
            case_id                     => $self->id,
        }
    );

    my @pending_notifications;

    while(my $transaction = $transactions->next) {
        push @pending_notifications, $transaction;
    }

    return \@pending_notifications;
}

=head2 reschedule_phase_notifications

Execute rules, then reschedule mails based on the outcome.

=cut

sub reschedule_phase_notifications {
    my ($self, $options) = @_;

    my $notifications   = $options->{notifications} or die "need notifications resultset";
    my $status          = $options->{status}        or die "need status";

    # Since zaak_id is stored as a json parameter, there is no database relationship
    my $scheduled_jobs = $self->result_source->schema->resultset('ScheduledJobs');
    my $rules_result = $self->execute_rules({status => $status});

    return {} unless exists $rules_result->{schedule_mail};

    return $scheduled_jobs->reschedule_pending({
        zaak_id         => $self->id,
        notifications   => $notifications,
        scheduled_mails => $rules_result->{schedule_mail},
    });
}


sub reschedule_notifications {
    my ($self, $options) = @_;

    my $statuses = $self->zaaktype_node_id->zaaktype_statussen->search({
        status => {'<=' => $self->milestone }
    }, {
        order_by => { '-asc' => 'status'}
    });

    my $scheduled_jobs = $self->result_source->schema->resultset('ScheduledJobs');

    eval {
        $self->result_source->schema->txn_do(sub {

            $scheduled_jobs->cancel_pending({
                zaak_id => $self->id
            });

            while(my $status = $statuses->next()) {
                my $notifications = $status->notifications();

                $self->reschedule_phase_notifications({
                    notifications => $notifications,
                    status        => $status->status,
                });
            }
        });
    };
    if ($@) {
        die "Error rescheduling notifications: $@";
    }
}


1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 get_email_interface

TODO: Fix the POD

=cut

=head2 pending_notifications

TODO: Fix the POD

=cut

=head2 reschedule_notifications

TODO: Fix the POD

=cut

