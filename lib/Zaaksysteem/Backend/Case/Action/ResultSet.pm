package Zaaksysteem::Backend::Case::Action::ResultSet;

use Moose;

use BTTW::Tools;


# TODO: Improve documentation and move to Zaaksysteem::Profile

BEGIN {
    extends 'DBIx::Class::ResultSet';
    with 'MooseX::Log::Log4perl';
}

sub type {
    return shift->search({ type => shift });
}

sub milestone {
    return shift->search(
        { 'casetype_status_id.status' => shift },
        { join => 'casetype_status_id' }
    );
}

sub current {
    return shift->search(
        { 'casetype_status_id.status' => { '=' => \'case_id.milestone' } },
        { join => [ 'casetype_status_id', 'case_id' ] }
    );
}

sub active {
    return shift->search({ automatic => 1 });
}

sub sorted {
    my ($self) = @_;

    return $self->search({}, { order_by => { -asc => 'me.id' }, prefetch => ['casetype_status_id']});
}


sub create_from_case {
    my ($self, $case) = @_;

    map { $self->create_from_phase($case, $_) } $case->zaaktype_node_id->zaaktype_statuses;
    $case->case_actions->sorted->apply_rules({case => $case});
}


=head2 create_from_phase

Given a specifc phase, create action records for every automatic action
that needs to be performed. These can include emails, template, changing
the allocations and creating a subcase (relatie).

=cut

sub create_from_phase {
    my ($self, $case, $phase) = @_;

    my @actions;

    push(@actions, map { $self->create_from_subject($case, $phase, $_) } $phase->zaaktype_subjects->search({}, {
        order_by => { -asc => 'me.id' }
    }));

    push(@actions, map { $self->create_from_zaaktype_relatie($case, $phase, $_) } $phase->zaaktype_relaties->search({}, {
        order_by => { -asc => 'me.id' }
    }));

    push(@actions, map { $self->create_from_template($case, $phase, $_) } $phase->zaaktype_sjablonens->search({}, {
        order_by => { -asc => 'me.id' }
    }));

    push(@actions, map { $self->create_from_email($case, $phase, $_) } $phase->zaaktype_notificaties->search({}, {
        order_by => { -asc => 'me.id' }
    }));

    my $object_types = $phase->zaaktype_kenmerken->search(
        { object_id => { '!=' => undef } },
        { order_by => { -asc => 'me.id' } }
    );

    push @actions, map { $self->create_from_type($case, $phase, $_) } map { $_->object_id } $object_types->all;

    # allocation has to happen after templates and emails so that previous assignee (behandelaar)
    # is still available for use in magic strings.
    unless ($phase->is_last) {
        push(
            @actions,
            $self->create({
                    case_id            => $case->id,
                    casetype_status_id => $phase->id,
                    type               => 'allocation',
                    label     => $phase->ou_id . ', ' . $phase->role_id,
                    automatic => $phase->role_set,
                    data      => {
                        description      => 'Toewijzing',
                        ou_id            => $phase->ou_id,
                        role_id          => $phase->role_id,
                        original_ou_id   => $phase->ou_id,
                        original_role_id => $phase->role_id,
                        role_set         => $phase->role_set,
                    }
                }
            )
        );
    }

    return @actions;
}

=head2 create_from_type

This method creates case actions based on the object-attributes contained in
the provided phase.

=cut

sub create_from_type {
    my ($self, $case, $phase, $object_type) = @_;

    $self->create({
        case_id => $case->id,
        casetype_status_id => $phase->id,
        type => 'object_mutation',
        label => sprintf('%s mutatie(s)', $object_type->get_object_attribute('name')->value),
        automatic => 0,
        data => {
            object_type_prefix => $object_type->get_object_attribute('prefix')->value,
            object_type_name => $object_type->get_object_attribute('name')->value,
            automatic_only => 1,
            description => sprintf(
                'Objectmutaties voor "%s" objecten',
                $object_type->get_object_attribute('name')->value
            )
        }
    });
}

# subcase is much sweeter, however currently a subcase is an instance
# and zaaktype_relatie is part of the case type, so i'm avoiding
# future confusion.
sub create_from_zaaktype_relatie {
    my ($self, $case, $phase, $relation) = @_;

    my $casetype = $relation->relatie_zaaktype_id;
    my $label = $casetype
                ? $casetype->zaaktype_node_id->titel
                : $relation->zaaktype_node_id->titel;


    # get_inflated_columns should also work, but than we need to make sure that
    # the zaaktype_node_id, zaaktype_status_id, created and last_modified are
    # not inflated. This is easier.
    my %data = $relation->get_columns;
    $data{copy_selected_attributes} = $relation->copy_selected_attributes;
    $data{related_casetype_uuid} = $casetype ? $casetype->uuid : undef;

    if ($data{eigenaar_id}) {
        my $betrokkene = $self->result_source->schema->betrokkene_model->get({}, $data{eigenaar_id});

        $data{eigenaar_uuid} = $betrokkene ? $betrokkene->uuid : undef;
    }

    return $self->create({
        case_id            => $case->id,
        casetype_status_id => $phase->id,
        type               => 'case',
        label              => $label,
        automatic          => $relation->status || 0,
        data               => \%data,
    });
}

sub create_from_email {
    my $self  = shift;
    my $case  = shift;
    my $phase = shift;
    my $email = shift;

    return $self->create({
        case_id => $case->id,
        casetype_status_id => $phase->id,
        type => 'email',
        label => $email->bibliotheek_notificaties_id->label,
        automatic => !$email->intern_block && $email->automatic ? 1 : 0,
        data => {
            rcpt => $email->rcpt,
            subject => $email->bibliotheek_notificaties_id->subject,
            body => $email->bibliotheek_notificaties_id->message,
            sender_address => $email->bibliotheek_notificaties_id->sender_address,
            sender => $email->bibliotheek_notificaties_id->sender,
            cc => $email->cc,
            bcc => $email->bcc,
            betrokkene_role => $email->betrokkene_role,
            email => $email->email,
            description => 'E-mail',
            behandelaar => $email->behandelaar,
            betrokkene_naam => $email->betrokkene_naam,
            intern_block => $email->intern_block,
            case_document_attachments => $email->case_document_attachments,
            automatic_phase => $email->automatic, #needed when rules disapply
            zaaktype_notificatie_id => $email->id,
            bibliotheek_notificaties_id => $email->get_column('bibliotheek_notificaties_id')
        }
    });
}

=head2 create_from_subject

    my $action = $rs->create_from_subject(
        $case,
        $phase,
        $subject
    );

Creates an action from C<zaaktype_standaard_betrokkene>

=cut

sub create_from_subject {
    my $self  = shift;
    my $case  = shift;
    my $phase = shift;
    my $subject = shift;

    my ($type)    = $subject->betrokkene_identifier =~ /^betrokkene-(.*)-.*$/;

    return $self->create({
        case_id => $case->id,
        casetype_status_id => $phase->id,
        type => 'subject',
        label => $subject->naam,
        automatic => 0,
        data => {
            map ( { $_ => ($subject->$_ || undef ) } qw/
                betrokkene_identifier
                naam
                rol
                magic_string_prefix
                gemachtigd
                notify
                uuid
            /),
            betrokkene_type     => $type,
        }
    });
}

sub _create_from_external_template {
    my ($self, $case, $phase, $template) = @_;


    my $library= $template->bibliotheek_sjablonen_id;
    if (!$library->get_column('interface_id')) {
        throw("create_from_template/no_interface", sprintf('No interface found for template %s', $template->id)) ;
    }

    return $self->create(
        {
            case_id            => $case->id,
            casetype_status_id => $phase->id,
            type               => 'template',
            label              => $template->display_name,
            automatic          => 0,
            data               => {
                description              => 'Extern sjabloon',
                bibliotheek_sjablonen_id => $library->id,
                bibliotheek_kenmerken_id =>
                    $template->get_column('bibliotheek_kenmerken_id'),
                filename              => $template->naam,
                target_format         => $template->target_format || 'docx',
                automatisch_genereren => 0,
                interface_id          => $library->get_column('interface_id'),
            }
        }
    );
}


sub create_from_template {
    my ($self, $case, $phase, $template) = @_;


    if ($template->bibliotheek_sjablonen_id->template_external_name) {
        return $self->_create_from_external_template($case, $phase, $template);
    }

    if (!$template->bibliotheek_sjablonen_id->filestore_id) {
        throw(
            "create_from_template/no_filestore",
            sprintf(
                'No filestore found for case type template %d (bibliotheek template %d)',
                $template->id,
                $template->get_column('bibliotheek_sjablonen_id'))
        );
    }

    return $self->create({
        case_id            => $case->id,
        casetype_status_id => $phase->id,
        type               => 'template',
        label              => $template->display_name,
        automatic          => $template->automatisch_genereren,
        data               => {
            description => 'Sjabloon',
            bibliotheek_sjablonen_id => $template->get_column('bibliotheek_sjablonen_id'),
            bibliotheek_kenmerken_id => $template->get_column('bibliotheek_kenmerken_id'),
            filename              => $template->naam,
            target_format         => $template->target_format || 'odt',
            automatisch_genereren => $template->automatisch_genereren,
            catalog_name          => $template->catalog_name,
        }
    });
}

sub apply_rules {
    my ($self, $options) = @_;

    my $case = $options->{ case } or die "need case";
    my $milestone = $options->{ milestone } || ($case->milestone + 1);

    my %rule_templates_lookup;
    my %rule_cases_lookup;
    my %scheduled_mails;
    my %toewijzing;
    my %action_counter;

    return $self->reset if $case->is_afgehandeld;

    # We're only going to apply rules to actions in the current milestone, thats the only one that matters
    my $rules_result = $case->execute_rules({ status => $milestone });

    # Build lookup tables
    if(exists $rules_result->{ templates }) {
        %rule_templates_lookup = map { $_ => 1 } @{ $rules_result->{ templates } };
    }

    if(exists $rules_result->{ cases }) {
        %rule_cases_lookup = map { $_ => 1 } @{ $rules_result->{ cases } };
    }

    while(my $action = $self->next()) {
        # Yeah, the counter state is a bit complex, but let me explain!
        # Since rules are applied to email notifications by their ordering, we need to check the position of the action
        # But we also need to track the phase it's in, otherwise we just increment the count for stati in the first phase
        # And that gives screwy results.
        my $counter = ++$action_counter{ sprintf('%s-%d', $action->type, $action->casetype_status_id->status) };

        # Ignore tainted actons, and actions in a different milestone.
        next if($action->tainted || $action->casetype_status_id->status != ($milestone));

        if($action->type eq 'template') {

            # first look if a rule wants to enable. if not, look at the static casetype setting and use that
            my $desired_value = exists $rule_templates_lookup{$counter} || $action->data->{automatisch_genereren};

            $action->set_automatic({
                automatic => $desired_value
            });

        } elsif($action->type eq 'case') {  # yes, identical to template.

            my $desired_value = exists $rule_cases_lookup{$counter} || $action->data->{status};

            $action->set_automatic({
                automatic => $desired_value
            });

        } elsif($action->type eq 'email') {

            my $do_update;

            if ($action->data->{rcpt} eq 'behandelaar'
                && $action->data->{behandelaar}
                && !$action->data->{betrokkene_naam})
            {
                my $betrokkene
                    = $self->result_source->schema->betrokkene_model
                    ->get({}, $action->data->{behandelaar});
                $action->data->{betrokkene_naam}
                    = $betrokkene
                    ? $betrokkene->display_name
                    : "Onbekende gebruiker";
                $action->make_column_dirty('data');
                $do_update = 1;
            }

            if(exists $rules_result->{schedule_mail}->{ $counter }) {
                unless($action->automatic) {
                    $action->automatic(1);
                    $do_update = 1;
                }

                my $send_date = $rules_result->{schedule_mail}->{$counter}->{send_date};
                if($send_date && $action->data->{send_date} ne $send_date) {
                    $action->data->{ send_date } = $send_date->iso8601;
                    $action->make_column_dirty('data');

                    $do_update = 1;
                }
            } else {
                # fall back to default behaviour. don't uncheck when
                unless($action->data->{automatic_phase}) {
                    if($action->automatic) {
                        $action->automatic(0);
                        $do_update = 1;
                    }
                }
            }

            $action->update if $do_update;

        } elsif($action->type eq 'allocation') {
            $self->update_allocation({
                action      => $action,
                rule_action => $rules_result->{toewijzing}
            });
        } elsif($action->type eq 'subject') {
            ### When the current action uuid is in the rules uuids, we start it
            if (
                $rules_result->{subjects} &&
                scalar @{ $rules_result->{subjects} } &&
                grep { $action->data->{uuid} eq $_ } @{ $rules_result->{subjects} }
            ) {
                $action->set_automatic({
                    automatic => 1
                });
            } else {
                $action->set_automatic({
                    automatic => 0
                });
            }
        } elsif($action->type eq 'object_mutation') {
            my $mutations = $case->object_data->object_mutation_lock_object_uuids->search({
                object_type => $action->data->{ object_type_prefix }
            });

            if($mutations->count) {
                $action->automatic(1);

                $action->data->{ mutations } = [
                    map { $_->TO_JSON } $mutations->all
                ];

                $action->make_column_dirty('data');

                $action->update;
            } else {
                $action->automatic(0);
                $action->update;
            }
        }
    }

    $self->reset;
}


=head2 update_allocation

Allocation actions work as follows. From ZTB phase management a hardcoded allocation action
can be set. This is mandatory for the first phase, optional for the middle phases, and unavailable
for the last phase.

Rules can be created to override the phase allocation. Although multiple rules can be created, only the
final one is executed. This allows for conditional overriding.

So:
- Phase allocation (if present)
- Final rule allocation (if present)

Case_action rows are created for every phase but the final one, with the 'automatic' flag determining
if the action is executed at phase advancing.

If rules are present they can override the standard behaviour.
The outcome of rules is influenced by user input. When the user input changes back and disables the rules, the ZTB
settings need to remain available in the action.

=cut

sub update_allocation {
    my ($self, $arguments) = @_;

    my $action      = $arguments->{action}      or die "need action";
    my $rule_action = $arguments->{rule_action}; #can be undef

    my $desired_values;
    my $action_data = $action->data;

    # first decide what we really want
    if ($rule_action) {
        $desired_values = {
            role_id   => $rule_action->{role_id},
            ou_id     => $rule_action->{ou_id},
            automatic => 1
        };
    } else {
        $desired_values = {
            role_id   => $action_data->{original_role_id},
            ou_id     => $action_data->{original_ou_id},
            automatic => $action_data->{role_set} || 0,
        };
    }

    # then see if that's different from the actual sitation, in which case, update
    unless (
        $action_data->{role_id} eq $desired_values->{role_id} &&
        $action_data->{ou_id}   eq $desired_values->{ou_id}   &&

        # bust ununitialized value warnings
        ($action->automatic ||'') eq ($desired_values->{automatic} || '')
    ) {
        $action_data->{role_id} = $desired_values->{role_id};
        $action_data->{ou_id}   = $desired_values->{ou_id};

        $action->data($action_data);
        $action->automatic($desired_values->{automatic});
        $action->update();
    }
}

=head2 hijack_allocation_action

This method allows the in-place update of allocation actions. The main use
case comes from the registration form, where it is possible to override the
registration phase allocation.

=cut

define_profile hijack_allocation_action => (
    required => {
        group => 'Zaaksysteem::Object::Types::Group',
        role => 'Zaaksysteem::Object::Types::Role'
    },
    optional => {
        route_only => 'Bool',
        phase_sequence => 'Int'
    },
    defaults => {
        route_only => 1,
        phase_sequence => 1
    }
);

sub hijack_allocation_action {
    my $self = shift;

    my $args = assert_profile(shift)->valid;

    my $action = $self->search(
        {
            'casetype_status_id.status' => $args->{ phase_sequence },
            'me.type' => 'allocation'
        },
        {
            join => 'casetype_status_id'
        }
    )->next;

    unless (defined $action) {
        throw('case/action/action_not_found', sprintf(
            'Unable to update allocation, case action not found (seq %d)',
            $args->{ phase_sequence }
        ));
    }

    my $action_data = $action->data;

    my $new_group_id = $args->{ group }->group_id;
    my $new_role_id = $args->{ role }->role_id;

    if ($new_group_id ne $action_data->{ ou_id } || $new_role_id ne $action_data->{ role_id }) {
        $action_data->{ ou_id } = $new_group_id;
        $action_data->{ role_id } = $new_role_id;
        $action_data->{ change_only_route_fields } = $args->{ route_only };

        $action->data($action_data);
        $action->data_tainted(1);
    }

    $action->label(sprintf(
        '%s, %s',
        $args->{ group }->name,
        $args->{ role }->name
    ));

    $action->automatic(1);
    $action->state_tainted(1);

    return $action->update;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 active

TODO: Fix the POD

=cut

=head2 apply_rules

TODO: Fix the POD

=cut

=head2 create_from_case

TODO: Fix the POD

=cut

=head2 create_from_email

TODO: Fix the POD

=cut

=head2 create_from_template

TODO: Fix the POD

=cut

=head2 create_from_zaaktype_relatie

TODO: Fix the POD

=cut

=head2 current

TODO: Fix the POD

=cut

=head2 milestone

TODO: Fix the POD

=cut

=head2 sorted

TODO: Fix the POD

=cut

=head2 type

TODO: Fix the POD

=cut

