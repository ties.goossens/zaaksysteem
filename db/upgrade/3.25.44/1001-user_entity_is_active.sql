BEGIN;

    ALTER TABLE user_entity ADD active BOOLEAN NOT NULL DEFAULT true;
    UPDATE user_entity SET active = false where date_deleted IS NOT NULL;

COMMIT;
