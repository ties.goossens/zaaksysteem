package Zaaksysteem::Constants::Users;

use warnings;
use strict;

=head1 NAME

Zaaksysteem::Constants::Users - Constants for user rights

=head1 DESCRIPTION

This module defined constants for user rights. Currently only does:

=over 4

=item C<REGULAR>

Regular logged in user

=item C<API>

API authenticated user

=item C<PIP>

Non-regular user, logged in via /pip

=item C<FORM>

Non-regular user, logged in via /form

=item C<ADMIN>

Logged in user, with C<admin> permission

=back

The exported constants are implemented as bitmasks, and can be combined using
bit-wise operations.

=head1 SYNOPSIS

    use Zaaksysteem::Constants::Users qw(PIP API REGULAR FORM);

    # Check if the user is logged in or not
    $c->assert_user(PIP);

    # Check if the user comes from API context, and has admin permissions
    $c->assert_user(API | ADMIN);

=cut

use Exporter qw[import];

our @EXPORT_OK  = qw(REGULAR API FORM PIP ADMIN);
our %EXPORT_TAGS  = ( all => \@EXPORT_OK );

use constant REGULAR  => (1 << 0);
use constant API      => (1 << 1);
use constant FORM     => (1 << 2);
use constant PIP      => (1 << 3);
use constant ADMIN    => (1 << 4);

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016-2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
