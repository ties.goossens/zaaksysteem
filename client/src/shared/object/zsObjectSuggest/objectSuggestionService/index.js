// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import assign from 'lodash/assign';
import escapeString from 'lodash/escape';
import get from 'lodash/get';
import merge from 'lodash/merge';
import result from 'lodash/result';
import { getCompanyAddressRows, getPersonAddressRows } from './format';
import propCheck from './../../../util/propCheck';

/* Labelize a natuurlijk persoon object for the spotenlighter
 *  @param {object} np - A NatuurlijkPersoon object
 */
function spotenlighterLabelPerson(np) {
  const birthDate = np.geboortedatum ? ` (${np.geboortedatum})` : '';
  let description = 'Geen adresgegevens beschikbaar';
  if (np.adres_id) {
    const rows = getPersonAddressRows(np.adres_id);
    description = rows !== undefined ? rows.join(', ') : '';
  }
  return {
    label: np.decorated_name + birthDate,
    description,
    link: `/main/contact-view/person/${np.uuid}`,
  };
}

/* Labelize a bedrijf object for the spotenlighter
 *  @param {object} bedrijf - A Bedrijf object
 */
function spotenlighterLabelBedrijf(bedrijf) {
  const rows = getCompanyAddressRows(bedrijf);
  const description = rows.join(', ');

  return {
    label: bedrijf.handelsnaam,
    description,
    link: `/main/contact-view/organization/${bedrijf.uuid}`,
  };
}

export const getBagLabel = (item) => {
  const {
    postcode,
    huisnummer,
    huisletter,
    huisnummer_toevoeging,
    straatnaam,
    plaats,
  } = item;
  const huisNummerString = huisnummer
    ? ` ${huisnummer}${huisletter}${
        huisnummer_toevoeging ? '-' : ''
      }${huisnummer_toevoeging}`
    : '';
  const postcodeString = postcode ? `${postcode} ` : '';

  return `${straatnaam}${huisNummerString}, ${postcodeString}${plaats}`;
};

export const checkData = (data) => {
  if (!data) {
    return [];
  }

  if (Array.isArray(data)) {
    return data;
  } else if (data.response) {
    return get(data, 'response.docs', []);
  }

  return get(data, 'data', []);
};

export default angular
  .module('shared.object.zsObjectSuggest.objectSuggestionService', [])
  .factory('objectSuggestionService', () => {
    let transformers = {
      natuurlijk_persoon: (item) => {
        return spotenlighterLabelPerson(item.object);
      },

      bedrijf: (item) => {
        return spotenlighterLabelBedrijf(item.object);
      },

      case: (item) => ({
        id: get(item, 'object.case_id'),
        label: `${item.object.id}: ${item.object.zaaktype_node_id.titel}`,
        description: escapeString(get(item, 'object.description', '')),
        link: `/intern/zaak/${item.object.id}`,
        type: 'case',
      }),

      'custom-object': (item) => ({
        id: item.id,
        label: item.meta.summary,
        type: 'custom-object',
      }),

      zaak: (item) => transformers.case(item),

      casetype: (item) => ({
        id: get(item, 'object.casetype_id'),
        label: get(item, 'object.zaaktype_node_id.titel'),
        description: escapeString(get(item, 'object.description', '')),
        data: merge({}, item.object, {
          values: {
            trigger: result(item, 'object.zaaktype_node_id.trigger'),
          },
        }),
        type: 'casetype',
      }),

      zaaktype: (item) => transformers.casetype(item),

      file: (item) => ({
        link: `/intern/zaak/${item.object.case_id}/documenten/`,
      }),

      attribute: (item) => ({
        id: item.id,
        label: item.label,
        data: item,
        type: 'attribute',
      }),

      bag: (item) => ({
        data: item,
        description: '',
        id: item.id,
        label: item.weergavenaam,
        name: item.id,
        type: 'bag_address',
      }),
      'bag-street': (item) => ({
        data: item,
        description: '',
        id: item.id,
        label: item.weergavenaam,
        name: item.id,
        type: 'bag_street',
      }),

      'search-v2': (item) => ({
        description: item.attributes.description,
        id: item.id,
        label: item.meta.summary,
        type: item.attributes.result_type,
      }),

      defaults: (item) => ({
        id: item.id,
        name: item.id,
        label: item.label,
        type: item.object_type,
        description: '',
        data: item.object,
        link: `/object/${item.id}`,
      }),
    };

    let handlers = {
      casetype: {
        request: (query) => {
          if (query.length < 3) {
            return null;
          }

          return {
            url: '/objectsearch/',
            params: {
              query,
              object_type: 'zaaktype',
            },
          };
        },
      },

      case: {
        request: (query) => {
          if (!query) {
            return null;
          }

          return {
            url: '/objectsearch/',
            params: {
              query,
              object_type: 'zaak',
            },
          };
        },
      },

      objecttypes: {
        request: (query) => {
          let params = {};

          if (query) {
            params.query = query;
          } else {
            params.all = 1;
          }

          return {
            url: '/objectsearch/objecttypes',
            params,
          };
        },
      },

      object: {
        request: (query) => {
          return query
            ? {
                url: '/objectsearch/objects',
                params: {
                  query,
                },
              }
            : null;
        },
      },

      'custom-object': {
        request: (query) => {
          return query
            ? {
                url: '/api/v2/cm/search',
                params: {
                  keyword: query,
                  type: 'custom_object',
                },
              }
            : null;
        },
      },

      medewerker: {
        request: (query) => {
          if (query.length < 3) {
            return null;
          }

          return {
            url: '/objectsearch/contact/medewerker',
            params: {
              query,
            },
          };
        },
      },

      bag: {
        request: (query) => {
          if (!query) {
            return null;
          }

          return {
            url:
              'https://geodata.nationaalgeoregister.nl/locatieserver/v3/suggest',
            params: {
              q: query,
              fq: 'type:adres',
            },
          };
        },
      },

      'bag-street': {
        request: (query) => {
          if (!query) {
            return null;
          }

          return {
            url:
              'https://geodata.nationaalgeoregister.nl/locatieserver/v3/suggest',
            params: {
              q: query,
              fq: 'type:weg',
            },
          };
        },
      },

      attribute: {
        request: (query) => {
          if (!query) {
            return null;
          }

          return {
            url: '/objectsearch/attributes',
            params: {
              query,
            },
          };
        },
      },

      'search-v2': {
        request: (keyword, type, session, queryOptions) => {
          if (!keyword) {
            return null;
          }

          return {
            url: '/api/v2/cm/search',
            params: {
              keyword,
              type:
                queryOptions.relationshipType === 'subject'
                  ? 'person,organization,employee'
                  : queryOptions.relationshipType,
              'filter[relationships.custom_object_type.id]':
                queryOptions.objectTypeUuid,
            },
          };
        },
      },

      defaults: {
        request: (query, type) => {
          if (query.length < 3) {
            return null;
          }

          return {
            url: '/objectsearch',
            params: {
              query,
              object_type: type,
            },
          };
        },
        reduce: (data, type) =>
          checkData(data).map((item) => {
            let transformedItem,
              transformer = transformers[item.object_type || type];

            transformedItem = transformers.defaults(item);

            if (transformer) {
              transformedItem = assign(transformedItem, transformer(item));
            }

            return transformedItem;
          }),
      },
    };

    let getHandler = (type) => handlers[type] || handlers.defaults;

    return {
      getRequestOptions: (preferredQuery, type, session, queryOptions) => {
        let query = preferredQuery;

        propCheck.throw(
          propCheck.shape({
            query: propCheck.string.optional,
            type: propCheck.string.optional,
            session: propCheck.object.optional,
            queryOptions: propCheck.object.optional,
          }),
          {
            query,
            type,
            session,
            queryOptions,
          }
        );

        const bagSettings = session ? session.configurable : {};
        let handler = getHandler(type);

        if (!query) {
          query = '';
        }

        let opts =
          typeof handler.request === 'function'
            ? handler.request(query, type, bagSettings, queryOptions)
            : handlers.defaults.request(query, type);

        return opts;
      },
      reduce: (data, type, session) => {
        propCheck.throw(
          propCheck.shape({
            type: propCheck.string.optional,
            data: propCheck.any.optional,
            session: propCheck.object.optional,
          }),
          {
            data,
            type,
            session,
          }
        );

        const bagSettings = session ? session.configurable : {};
        let handler = getHandler(type);

        let res =
          typeof handler.reduce === 'function'
            ? handler.reduce(data, type, bagSettings)
            : handlers.defaults.reduce(data, type);

        return res;
      },
    };
  }).name;
