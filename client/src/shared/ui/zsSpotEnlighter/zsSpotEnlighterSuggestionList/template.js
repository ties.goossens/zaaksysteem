// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import suggestionTemplate from './suggestion-template.html';
import templateLiteral from './template.html';

const domElement = angular.element(templateLiteral)[0];

angular
  .element(domElement.querySelector('.suggestion'))
  .replaceWith(suggestionTemplate);

export default domElement.outerHTML;
