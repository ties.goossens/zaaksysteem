package Zaaksysteem::Search::ZQL::Expression::Prefix;

use Moose;

use Data::Dumper;

extends 'Zaaksysteem::Search::ZQL::Expression';

has operator => ( is => 'ro', required => 1 );
has term => ( is => 'ro', required => 1 );

sub new_from_production {
    my $class = shift;
    my %item = @_;

    return $class->new(
        operator => $item{ prefix_operator },
        term => $item{ term }
    );
}

sub dbixify {
    my $self = shift;
    my $cmd = shift;

    my $cond = $self->term->dbixify;

    unless (eval { $cond->isa('Zaaksysteem::Search::Conditional') }) {
        throw('search/zql', 'Term for logical inversion *must* be a conditional');
    }

    $cond->invert(1);

    return $cond;
}

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 dbixify

TODO: Fix the POD

=cut

=head2 new_from_production

TODO: Fix the POD

=cut

