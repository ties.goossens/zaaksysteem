// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import keyBy from 'lodash/keyBy';
import mapValues from 'lodash/mapValues';
import mapKeys from 'lodash/mapKeys';
import shiftMonth from '../../../util/date/shiftMonth';

export default (dateLimit, idToNameMapping) => {
  let options = mapValues(keyBy('start end'.split(' ')), (key) => {
    let opts = dateLimit[key],
      dateGetter,
      dateTransform,
      delta;

    if (!opts || !opts.active) {
      return;
    }

    if (opts.reference === 'now') {
      dateGetter = () => new Date();
    } else {
      dateGetter = (values) => values[idToNameMapping[Number(opts.reference)]];
    }

    delta = opts.during === 'post' ? opts.num : -opts.num;

    switch (opts.term) {
      case 'months':
        dateTransform = (source) => source && shiftMonth(source, delta);
        break;

      case 'weeks':
        dateTransform = (source) =>
          source &&
          new Date(
            source.getFullYear(),
            source.getMonth(),
            source.getDate() + delta * 7
          );
        break;

      case 'days':
        dateTransform = (source) =>
          source &&
          new Date(
            source.getFullYear(),
            source.getMonth(),
            source.getDate() + delta
          );
        break;

      case 'years':
        dateTransform = (source) =>
          source &&
          new Date(
            source.getFullYear() + delta,
            source.getMonth(),
            source.getDate()
          );
        break;
    }

    return dateLimit[key] && dateLimit[key].active
      ? [
          '$values',
          (values) => {
            let source = dateGetter(values),
              date = dateTransform(source);

            return date || null;
          },
        ]
      : null;
  });

  options = mapKeys(options, (value, key) => {
    return key === 'start' ? 'min' : 'max';
  });

  return options;
};
