package Zaaksysteem::External::Postex;
use Zaaksysteem::Moose;

=head1 NAME

Zaaksysteem::External::Postex - A Postex integration module

=head1 DESCRIPTION

Integrate L<Postex> into Zaaksysteem. This module is the glue
layer between any module that wants to use Postex into
Zaaksysteem

=head1 SYNOPSIS

    use Zaaksysteem::External::Postex;

    my $model = Zaaksysteem::External::Postex->new(
        secret       => 'my-api-key',
        endpoint     => 'https://some.postex.nl/api',
        generator_id => '123456789',
    );


=cut

use MIME::Base64 qw(encode_base64);
use WebService::Postex;

=head1 ATTRIBUTES

=cut

has postex => (
    is       => 'ro',
    isa      => 'WebService::Postex',
    required => 1,
    handles  => [qw(generation_rest_upload)],
);

has communication => (
    is        => 'ro',
    isa       => 'Zaaksysteem::CommunicationTab',
    predicate => 'has_communication',
);

around BUILDARGS => sub {
    my $orig = shift;
    my $self = shift;
    my %args = @_;

    my $postex = WebService::Postex->new(
        secret       => delete $args{secret},
        base_uri     => delete $args{base_uri},
        generator_id => delete $args{generator_id},
    );

    return $self->$orig(%args, postex => $postex);
};

sub get_case_recipients {
    my ($self, $case, $type, $role) = @_;

    my @recipients;
    if ($type eq 'requestor') {
        @recipients = $case->aanvrager_object;
    }
    elsif ($type eq 'authorized') {
        @recipients = $case->get_betrokkene_objecten(
            {
                'pip_authorized'  => 1,
                'betrokkene_type' => { '!=' => 'medewerker' }
            }
        );
    }
    elsif ($type eq 'betrokkene') {
        @recipients = $case->get_betrokkene_objecten(
            {
                'LOWER(rol)'      => lc($role),
                'betrokkene_type' => { '!=' => 'medewerker' }
            }
        );
    }
    # When OCR is used Postex decides who the recipient is based on OCR.
    elsif ($type eq 'ocr') {
        return;
    }
    else {
        throw(
            "postex/case/recipient_type/invalid",
            "Unable to determine recipient type"
        );
    }

    return @recipients if @recipients;

    throw("postex/case/recipients/missing",
        "Unable to determine recipient(s)");

}


=head2 send_case_document

Implements all the logic to create a Postex document request. Returns a
payload hashref containing the messagestructure that can be delivered to Postex

=cut

define_profile send_case_document => (
    required => {
        case             => 'Zaaksysteem::Zaken::ComponentZaak',
        files            => 'Defined',
        sender           => 'Defined',
    },
    optional => {
        subject          => 'Str',
        body             => 'Str',
        recipients       => 'Defined',
    },
);

sub send_case_document {
    my $self = shift;
    my $options = assert_profile({@_})->valid;

    my $case = $options->{case};

    my @recipients = @{$options->{recipients} // []};
    my @files = @{$options->{files}};

    my %payload = (
        case    => $self->_build_case_metadata($case),
        message => {
            subject => $options->{subject},
            body    => $options->{body}
        },
        files => $self->_build_files_metadata(@files),
    );

    foreach (qw(body subject)) {
        $payload{message}{$_} = $options->{$_}
            if defined $options->{$_};
    }

    my $sender = $options->{sender}->as_object;

    # This means we have OCR as a type
    if (!@recipients) {
        $self->_send_case_document(
            case    => $case,
            sender  => $sender,
            payload => \%payload,
            files   => \@files,
        );
        return 1;
    }

    foreach my $recipient (@recipients) {
        $self->_send_case_document(
            case      => $case,
            sender    => $sender,
            recipient => $recipient,
            payload   => \%payload,
            files     => \@files,
        );
    }
    return 1;
}

sub _send_case_document {
    my $self = shift;
    my %args = @_;

    my $case      = $args{case};
    my $sender    = $args{sender};
    my $recipient = $args{recipient};
    my %payload   = %{ $args{payload} };
    my $files     = $args{files};

    my %recipient_log_data;
    if ($recipient) {
        $payload{recipient} = $self->_build_recipient_data($recipient);
        %recipient_log_data = (
            display_name => $recipient->volledige_naam,
            address => $recipient->is_briefadres
                ? $recipient->get_full_correspondence_address
                : $recipient->get_full_residence_address,
        );
    }
    else {
        %recipient_log_data = (
            display_name => 'OCR',
            address      => 'OCR',
        );
    }

    my $result = $self->generation_rest_upload(%payload);

    my $subject = $payload{message}{subject} // '<geen onderwerp opgegeven>';
    my $body    = $payload{message}{body} // '<geen inhoud opgegeven>';

    $case->trigger_logging('case/send_postex', {
        component => 'zaak',
        data => {
            zaak_id    => $case->id,
            ptx_result => $result,
            subject    => $subject,
            body       => $body,
            filenames  => [ map { $_->{name } } @{ $payload{files} } ],
            recipient  => \%recipient_log_data,
        }
    });

    return unless $self->has_communication;

    $self->_add_to_communication_tab(
        case      => $case,
        subject   => $subject,
        $recipient
              # Don't ask, old betrokkene model
            ? (recipient => $recipient->gm_extern_np->as_object)
            : (),
        body      => $body,
        sender    => $sender,
        files     => $files,
    );
    return 1;

}

sub get_files {
    my ($self, $case, $ids) = @_;

    my @files = $case->search_active_files($ids)->all();
    return @files if @files;

    throw("postex/files/missing", "No files attached to Postex");

};

sub _add_to_communication_tab {
    my ($self, %params) = @_;

    return unless $self->has_communication;

    my $tab  = $self->communication;
    my $type = 'postex';

    my $sender    = $params{sender};
    my $recipient = $params{recipient};

    my $thread = $tab->create_thread(
        case         => $params{case},
        thread_type  => 'external',
        message_type => $type,
        subject      => $params{subject},
        slug         => $params{body},
        created_by   => $sender,
        $recipient ? ( contact      => $recipient ) : (),
    );

    my $external = $tab->create_thread_message_external(
        type         => $type,
        content      => $params{body},
        subject      => $params{subject},
        direction    => 'outgoing',
        participants => [
            {
                role         => 'to',
                display_name => $recipient ? $recipient->display_name : 'OCR - Zie bijlage(s)',
                address      => 'devnull@postex.com',
            }
        ],
    );

    my $msg = $tab->create_thread_message(
        thread           => $thread,
        external_message => $external,
        created_by       => $sender,
        slug             => $params{body},
    );

    foreach my $file (@{$params{files}}) {
        $tab->add_attachment_to_message(
            thread_message => $msg,
            filestore      => $file->filestore_id,
            filename       => $file->filename,
        );
    }

}

=head2 _build_case_metadata

Build hash with information about a case

=cut

sub _build_case_metadata {
    my ($self, $case) = @_;
    return {
        id              => $case->id,
        case_type_id    => $case->zaaktype_node_id->id,
        case_type_title => $case->zaaktype_node_id->titel
    };
}

=head2 _build_recipient_data_np

Build recipient metadata for NatuurlijkPersoon

=cut

sub _build_recipient_data_np {
    my ($self, $aanvrager) = @_;

    my $adres = $aanvrager->is_briefadres
        ? $aanvrager->correspondentieadres
        : $aanvrager->verblijfsadres;

    return {
        type            => 'natuurlijk_persoon',
        bsn             => $aanvrager->bsn,
        display_name    => $aanvrager->display_name,
        voorletters     => $aanvrager->voorletters,
        naam            => $aanvrager->achternaam,
        adellijke_titel => $aanvrager->adellijke_titel,
        adres           => $self->_build_address(
            $aanvrager->is_briefadres ? 'correspondentie' : 'verblijf', $adres
        ),
    };
}

sub _build_address {
    my ($self, $type, $address) = @_;
    return {
        type                 => $type,
        straatnaam           => $address->straatnaam,
        postcode             => $address->postcode,
        woonplaats           => $address->woonplaats,
        huisnummertoevoeging => $address->huisnummertoevoeging,
        huisletter           => $address->huisletter,
        huisnummer           => $address->huisnummer,
        adres_buitenland1    => $address->adres_buitenland1,
        adres_buitenland2    => $address->adres_buitenland2,
        adres_buitenland3    => $address->adres_buitenland3,
        land                 => $self->_landnaam($address->landcode)
    };
}

=head2 _landnaam

Convert country code integer to Dutch label

=cut

sub _landnaam {
    my ($self, $landcode) = @_;
    return Zaaksysteem::Object::Types::CountryCode->new_from_dutch_code(
        $landcode
    )->label;
}


=head2 _build_recipient_data_bedrijf

Build recipient metadata for Bedrijf

=cut

sub _build_recipient_data_bedrijf {
    my ($self, $aanvrager) = @_;

    my $type    = $aanvrager->is_briefadres ? "correspondenctie" : "vestiging";
    my $address = $self->_build_address($type, $aanvrager);

    return {
        type         => 'bedrijf',
        handelsnaam  => $aanvrager->handelsnaam,
        email        => $aanvrager->email,
        adres        => {
            'tav' => $aanvrager->contact_naam,
            %$address,
        }
    };
}


=head2 _build_files_metadata

Takes a case and list of file ids. Encodes the files into base64 (in memory)
and returns an array containing a hash per file entry

Assumes there is enough memory to read a file into memory
Dies if no files are selected

=cut

sub _build_files_metadata {
    my ($self, @files) = @_;

    my @metadata;
    foreach my $file (@files) {

        my $content = encode_base64($file->filestore->content(), '');

        push(
            @metadata,
            {
                data      => $content,
                name      => $file->name,
                extension => $file->extension,
                version   => $file->version
            }
        );
    }

    return \@metadata;
}


=head2 _build_recipient_data

Builds recipient and address information block meatdata.  Delegates for
NatuurlijkPersoon and Bedrijf. Otherwise die with unsupported message.

=cut

sub _build_recipient_data {
    my ($self, $recipient) = @_;

    if ($recipient->isa('Zaaksysteem::Betrokkene::Object::NatuurlijkPersoon')) {
        return $self->_build_recipient_data_np($recipient->current);
    }

    if ($recipient->isa('Zaaksysteem::Betrokkene::Object::Bedrijf')) {
        return $self->_build_recipient_data_bedrijf($recipient->current);
    }

    throw('postex/subject/unsupported_type',
        "Unsupported betrokkene_type: " . $recipient->btype);
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2019, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
