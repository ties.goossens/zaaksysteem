package Zaaksysteem::Controller::Beheer::WOZ;

use Moose;
use namespace::autoclean;

use Zaaksysteem::Constants qw(MIMETYPES_ALLOWED);
use Zaaksysteem::ZTT::Context::WOZ;

BEGIN { extends 'Zaaksysteem::Controller' }

=head2 check_woz_objects

=cut

sub _check_woz {
    my ($self, $c) = @_;
    my $interface = $c->model('DB::Interface')->search_active({module => 'woz'})->first;
    if (!$interface) {
        $c->res->redirect("/");
        $c->detach();
    }

    $c->assert_any_user_permission('woz_objects');
    $c->stash->{interface} = $interface;
    return 1;
}

sub _get_woz_settings {
    my ($self, $c) = @_;

    $self->_check_woz($c);

    my $settings = $c->model('DB::Settings')->filter({
        filter => 'woz_'
    });

    foreach my $setting (@$settings) {
        $c->stash->{
            $setting->key
        } = $setting->value;
    }
}


sub index :Path :Args(0) {
    my ( $self, $c, $updated ) = @_;

    $self->_get_woz_settings($c);

    $c->stash->{sjablonen} = [$c->model('DB::BibliotheekSjablonen')->search({}, {
        order_by => 'label'
    })->all];

    $c->stash->{dagobert_editor} = 1;
    $c->stash->{template}   = 'beheer/import/woz/admin.tt';
}


sub updated : Local {
    my ($self, $c) = @_;

    $self->_get_woz_settings($c);

    $c->stash->{updated} = 1;

    $c->forward('index');
}


sub update : Local {
    my ($self, $c) = @_;

    my $tmpdir = File::Temp->newdir();

    $self->_get_woz_settings($c);

    my $params = $c->req->params();

    # dirty
    $params->{settings_woz_zaaktype_id} = $params->{zaaktype_id};

    # checkbox, need to explicify
    $params->{settings_woz_pip_comments} = !!$params->{settings_woz_pip_comments};

    if($params->{filestore_uuid}) {
        my $filestore_uuid = $params->{filestore_uuid};

        my $filestore_rs = $c->model("DB::Filestore");

        my $file = $filestore_rs->find({
            uuid => $filestore_uuid,
        });

        die "can't find uploaded file, aborting" unless $file;
        die "uuid ID for file is not set" unless $file->uuid;

        my $path = $file->get_path;
        die "can find path for file" unless $path;

        eval {
            $c->model('DB::WozObjects')->import_woz({
                file_path   => $path,
                tmp_dir     => $tmpdir,
            });

            $c->model('DB::Logging')->trigger('woz/import/upload', {
                component => 'woz_objects',
                data => {
                    file_id => $file->id,
                    file_uuid => $filestore_uuid
                }
            });
        };

        if($@) {
            $c->stash->{error} = "Error during WOZ import: " . $@;
            $c->stash->{template}   = 'beheer/import/woz/admin.tt';
            $c->detach();
        }
    }

    my $settings_rs = $c->model('DB::Settings');
    foreach my $param (keys %$params) {
        if($param =~ m|^settings_(.*)|) {
            my $key = $1;

            $settings_rs->store({
                key     => $key,
                value   => $params->{$param}
            });
        }
    }

    $c->model('DB::Logging')->trigger('woz/update', {
        component => 'woz_objects'
    });

    $c->res->redirect('/beheer/woz/updated');
    $c->detach();
}

sub view : Local: Args() {
    my ($self, $c, $owner, $id) = @_;

    $self->_get_woz_settings($c);

    if ($owner) {
        $c->stash->{owner} = $owner;

        $c->stash->{woz_objects} = $c->model('DB::WozObjects')->search(
            {
                owner => $owner,
                $id ? (id => $id) : (),
            }
        );
        $c->stash->{template} = "beheer/import/woz/ownerobjects.tt";
    }
    else {
        $c->stash->{woz_objects} = $c->model('DB::WozObjects')->search(
            {},
            {
                select   => ['owner', { count => 'me.id' }],
                as       => [qw/owner count_per_owner/],
                group_by => 'owner',
                order_by => { '-asc' => 'owner' },
            }
        );

        $c->stash->{template} = "beheer/import/woz/list.tt";
    }
    if ($c->stash->{woz_objects}->count >= 1000) {
        $Template::Directive::WHILE_MAX = $c->stash->{woz_objects}->count + 2;
    }
}

sub retrieve_object : Private {
    my ($self, $c, $id) = @_;

    my $params = $c->req->params;
    die "need owner and object_id" unless $params->{owner} && $params->{object_id};

    $c->stash->{woz_objects} = $c->model('DB::WozObjects')->search({
        id => $id,
        owner => $params->{owner},
        object_id => $params->{object_id}
    });

}


sub object : Local: Args() {
    my ($self, $c, $id) = @_;

    $c->forward('retrieve_object', [$id]);
    $c->stash->{template} = "beheer/import/woz/ownerobjects.tt";
}


sub report : Local : Args() {
    my ($self, $c, $id) = @_;

    $c->forward('retrieve_object', [$id]);

    $c->stash->{extra_body_class} = ' woz-body';
    $c->stash->{layout_type} = 'simple';
    $c->stash->{template} = "plugins/woz/report.tt";
}

sub report_pdf : Local : Args() {
    my ($self, $c, $id) = @_;

    $c->forward('retrieve_object', [$id]);

    my $settings = $c->model('DB::Settings')->filter({ filter => 'woz_' });
    my %settings;
    for my $setting (@$settings) {
        $settings{ $setting->key } = $setting->value;
    }

    my $bibliotheek_sjabloon = $c->model('DB::BibliotheekSjablonen')->find(
        $settings{woz_sjabloon}
    );

    my $context = Zaaksysteem::ZTT::Context::WOZ->new(
        woz_object => $c->stash->{woz_objects}->first,
        settings   => \%settings,
        betrokkene => $c->stash->{betrokkene},
    );

    my $pdf_data = $c->model('PDFGenerator')->generate_pdf(
        template => $bibliotheek_sjabloon,
        context  => $context
    );

    $c->res->content_type('application/pdf');
    $c->res->body($pdf_data);
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 index

TODO: Fix the POD

=cut

=head2 object

TODO: Fix the POD

=cut

=head2 report

TODO: Fix the POD

=cut

=head2 retrieve_object

TODO: Fix the POD

=cut

=head2 update

TODO: Fix the POD

=cut

=head2 updated

TODO: Fix the POD

=cut

=head2 view

TODO: Fix the POD

=cut

=head2 MIMETYPES_ALLOWED

TODO: Fix the POD

=cut

=head2 report_pdf

TODO: Fix the POD

=cut
