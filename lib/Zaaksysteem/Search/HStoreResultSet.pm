package Zaaksysteem::Search::HStoreResultSet;

use Moose::Role;
use namespace::autoclean;

use Encode qw(encode_utf8);
use Scalar::Util qw(blessed);
use BTTW::Tools;
use Zaaksysteem::DB::HStore;

requires qw(
    search
    hstore_column
    map_native_column
    get_stable_sort_key
);

=head1 NAME

Zaaksysteem::Search::HStoreResultSet - Role to perform searches using an HSTORE column.

=head1 METHODS

=head2 search_hstore

A wrapper around "search()" that queries a column of type HSTORE on the table
(as defined by \$self->hstore_column).

=head3 Arguments

This method takes two positional arguments:

=over

=item * query

Shallow hash containing the hstore keys and values to search for.

=item * options

Query options. Passed to C<< DBIx::Class::ResultSet->search() >>.

=back

=head3 Returns

Returns a L<DBIx::Class::ResultSet>.

=cut

sub search_hstore {
    my $self = shift;
    my ($query, $options) = @_;

    my $hprop_query = blessed($query) ? $query->evaluate($self) : $query;
    my $hprop_options = $self->_parse_options($options);

    return $self->search(
        $hprop_query,
        $hprop_options,
    );
}

=head2 map_search_args

This method converts a C<< key => value >> search argument hashref into
something that will query the hstore column instead.

    # #mapped_search_args will be hstore-escaped, and the values encoded for
    # Postgres
    my $mapped_search_args = $rs->map_search_args({
        key => 'value'
    });

=cut

sub map_search_args {
    my $self = shift;
    my $args = shift;

    # DBIx::Class::ResultSet->search(undef) works fine
    return undef unless defined $args;

    return {
        $self->hstore_column => {
            '@>' => Zaaksysteem::DB::HStore::encode($args)
        }
    };
}

sub _parse_options {
    my $self = shift;
    my ($options) = @_;

    return if not defined $options;

    my %rv;
    for my $key (keys %$options) {
        if ($key eq 'order_by') {
            $rv{$key} = $self->_parse_option_order_by($options->{$key});
        }
        elsif ($key eq 'select') {
            $rv{$key} = $self->_parse_option_select($options->{$key});
        }
        elsif ($key eq 'group_by') {
            $rv{ $key } = $self->_parse_option_group_by($options->{ $key });
        }
        else {
            $rv{$key} = $options->{$key};
        }
    }

    # If the query uses a group_by, add the counts for rows grouped by default
    # unless 'no_count' is truthy. Map the column to name 'count'.
    if (!$rv{ no_count } && exists $rv{ group_by }) {
        $rv{ select } = [ @{ $rv{ select } // [] }, { count => $self->hstore_column } ];
        $rv{ as } = [ @{ $rv{ as } // [] }, 'count' ];
    }

    return \%rv;
}

sub _parse_option_select {
    my $self = shift;
    my ($select) = @_;

    if (ref($select) eq 'ARRAY') {
        my @rv;

        for my $select_element (@$select) {
            push @rv, $self->_parse_option_select($select_element);
        }

        return \@rv;
    }
    elsif (ref($select) eq 'HASH') {
        if(keys(%$select) != 1 || !exists($select->{distinct})) {
            throw(
                'hstore/select_distinct',
                "Could not parse 'select' option. Expected something like: "
                . "select => [ { distinct => 'col' } ]"
            );
        }

        return {
            distinct => $self->map_hstore_key($select->{distinct}),
        };
    }
    else {
        # Don't know what called passed to us, let DBIx::Class sort out the error.
        return $self->map_hstore_key($select);
    }

    return;
}

sub _parse_option_group_by {
    my $self = shift;
    my ($group_by) = @_;

    unless (ref $group_by eq 'ARRAY') {
        throw('hstore/wrong_group_by', 'Expected an ARRAY reference for group by ( [ $column1, $column2, ... ] )');
    }

    my @rv;

    for my $column (@{ $group_by }) {
        push @rv, $self->map_hstore_key($column);
    }

    return \@rv;
}

sub _parse_option_order_by {
    my $self = shift;
    my ($order_by) = @_;

    my @rv;

    unless (ref $order_by eq 'HASH') {
        throw('hstore/wrong_order_by', 'Expecting a HASH ref; { alphanumeric => [ $column, ... ] }');
    }

    for my $style (keys %{ $order_by }) {
        my $colspec = $order_by->{ $style };

        unless (ref $colspec eq 'ARRAY') {
            throw('hstore/wrong_order_by_column', 'Expecting nested ARRAY ref; [ $column, ... ]');
        }

        for my $subcolspec (@{ $colspec }) {
            for my $direction (keys %{ $subcolspec }) {
                my $column = $subcolspec->{ $direction };
                my $mapped_column = $self->map_native_column($column);

                if (!defined $mapped_column) {
                    $mapped_column = $self->map_hstore_key($column);

                    if ($style eq 'numeric') {
                        $mapped_column = \[
                            sprintf(
                                "(NULLIF(SUBSTRING(%s FROM '([0-9]+(\\.[0-9]+)?)'), ''))::NUMERIC",
                                $mapped_column)
                        ];
                    }
                }

                push @rv, {
                    $direction => $mapped_column
                };
            }
        }
    }


    # Always append a "stable" sort order (by primary key, in this case) at the end.
    my $stable_sort = $self->get_stable_sort_key;
    push @rv, $stable_sort if $stable_sort;

    return \@rv;
}

=head2 map_hstore_key

TODO: Fix me

=cut

sub map_hstore_key {
    my $self = shift;
    my ($k) = @_;

    my $dbh = $self->result_source->storage->dbh;
    my $quoted_key = $dbh->quote($k);
    utf8::upgrade($quoted_key);

    return sprintf("%s->%s", $self->hstore_column, $quoted_key);
}

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

