#!/bin/bash

set -e

db=zaaksysteem
hostname=dev.zaaksysteem.nl

usage() {
    cat <<OEF

    $(basename $0) OPTIONS

    h       This help
    u       Hostname
    d       Database name
    f       File to upload
    n       New file to upload
    l       Use localhost for DB queries
    v       Debug this tool
OEF
    exit 0

}

while getopts "hd:f:u:n:lv" name
do
    case $name in
        h) usage;;
        d) db=$OPTARG;;
        f) file=$OPTARG;;
        n) new_file=$OPTARG;;
        u) hostname=$OPTARG;;
        l) PSQL_OPTS='-h localhost -U zaaksysteem';;
        v) set -x;;
    esac
done
shift $((OPTIND - 1))

do_sql() {
    psql -q -t $PSQL_OPTS $db "$*"
}

sql_exec_echo() {
    echo $(do_sql -c "$@")
}

die() {
    echo "$@" 1>&2
    exit 1
}


INTERFACE=$(sql_exec_echo \
    "SELECT uuid,id FROM interface WHERE module='xential' ORDER BY id DESC LIMIT 1")

INTERFACE_UUID=$(echo $INTERFACE | awk -F \|\  '{print $1}');
INTERFACE_ID=$(echo $INTERFACE | awk -F \| '/|/{gsub(/ /, "", $2); print $2}');

get_data() {

    local processor=$1
    if [ -z "$processor" ]
    then
    TID=$(sql_exec_echo "SELECT uuid, processor_params from transaction WHERE interface_id = $INTERFACE_ID order by id desc" |awk '{print $1}'| head -1 )

    else
        TID=$(sql_exec_echo "SELECT uuid, processor_params from transaction WHERE interface_id = $INTERFACE_ID order by id desc" | grep $processor | awk '{print $1}'| head -1 )

    fi


    if [[ -z $TID ]];
    then
        die "No transaction found for interface with ID $INTERFACE_ID"
    fi

    PROC_PARAMS=$(sql_exec_echo "SELECT processor_params from transaction WHERE interface_id = $INTERFACE_ID and uuid = '$TID'")

    echo $PROC_PARAMS | jq

    CID=$(echo $PROC_PARAMS | jq -r '.case');

    # Empty CID means we processed the transaction already, for testing
    # purposes we override the $CID

    if [ -z "$CID" ]
    then
        CASEUUID=$(echo $PROC_PARAMS | jq -r '.case_uuid');
    else
        CASEUUID=$(sql_exec_echo "SELECT uuid from zaak WHERE id = $CID")
    fi

    FILE_UUID=$(echo $PROC_PARAMS | jq -r '.filestore_uuid');

    BASE_URL="https://$hostname/api/v1/sysin/interface/$INTERFACE_UUID/trigger/api_post_file?transaction_uuid=$TID&case_uuid=$CASEUUID"
}

if [ -z "$file" ] && [ -z "$new_file" ]
then
    echo "No file or new file given, found the following transaction";
    get_data
    exit 2;
fi

if [ -n "$file" ]
then

    get_data _create_file_from_template

    curl -q -k --form "upload=@$file"  $BASE_URL | jq

    FILE_UUID=$(sql_exec_echo "SELECT fs.uuid FROM filestore fs JOIN file f on f.filestore_id = fs.id and f.generator = 'xential' and f.active_version = true order by f.id desc limit 1");
    echo "Added file $FILE_UUID";
fi

if [ -n "$new_file" ]
then
    get_data _process_request_edit_file
    curl -q -k --form "upload=@$new_file" \
        "$BASE_URL&file_uuid=$FILE_UUID" | jq
fi
