// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import parseSassVariables from '.';

describe('parseSassVariables', () => {
  test('should parse a single line', () => {
    const vars = parseSassVariables('$small-screen-up: 601px;');

    expect(vars['small-screen-up']).toBeDefined();
  });

  test('should strip !default from the variable value', () => {
    const vars = parseSassVariables('$small-screen-up: 601px !default;');

    expect(vars['small-screen-up']).toBe('601px');
  });

  test('should parse multiline', () => {
    const vars = parseSassVariables(`
			$small-screen-up: 601px !default;
			$medium-screen-up: 993px;
			$large-screen-up: 1201px;
		`);

    expect(vars['small-screen-up']).toBe('601px');
    expect(vars['medium-screen-up']).toBe('993px');
    expect(vars['large-screen-up']).toBe('1201px');
  });

  test('should ignore comments and whitespace', () => {
    const vars = parseSassVariables(`
			// Media Query Ranges
			$small-screen-up: 601px !default;
			$medium-screen-up: 993px !default;
			$large-screen-up: 1201px !default;
			$small-screen: 600px !default;
			$medium-screen: 992px !default;
			$large-screen: 1200px !default;

			$medium-and-up: "only screen and (min-width : #{$small-screen-up})" !default;
		`);

    expect(Object.keys(vars).length).toBe(7);
  });

  test('should strip quotes', () => {
    const vars = parseSassVariables(
      '$medium-and-up: "only screen and (min-width : 601px)" !default;'
    );

    expect(vars['medium-and-up']).toBe('only screen and (min-width : 601px)');
  });

  test('should replace references', () => {
    const vars = parseSassVariables(`
			// Media Query Ranges
			$small-screen-up: 601px !default;
			$medium-and-up: "only screen and (min-width : #{$small-screen-up})" !default;`);

    expect(vars['medium-and-up']).toBe('only screen and (min-width : 601px)');
  });

  test('should replace multiple references', () => {
    const vars = parseSassVariables(`
			// Media Query Ranges
			$min: 100px;
			$max: 200px;
			$query: "only screen and (min-width: #{$min}) and (max-width: #{$max})";`);

    expect(vars.query).toBe(
      'only screen and (min-width: 100px) and (max-width: 200px)'
    );
  });

  test('should throw an error when a reference is undefined', () => {
    expect(
      parseSassVariables.bind(
        null,
        '$medium-and-up: "only screen and (min-width : #{$small-screen-up})" !default;'
      )
    ).toThrow();
  });
});
