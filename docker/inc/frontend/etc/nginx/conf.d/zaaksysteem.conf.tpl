rewrite ^/tpl/zaak_v1/nl_NL/css/.*?common.css$ /page/minified/common/css/common.css last;
rewrite ^/html/(.*)$                           /html/nl/$1                          last;

### Backend
proxy_set_header X-Forwarded-For $remote_addr;
proxy_set_header Host $host;
proxy_set_header X-Real-IP $remote_addr;

# Allow client bodies ("file uploads") to be slightly over 2GB (so a 2GB file
# and some metadata can be uploaded at once)
client_max_body_size        2050M;

# Some generous buffers for request headers
large_client_header_buffers 4 512k;

resolver DNS_RESOLVER;

# { START Security-related headers
    # https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/X-Frame-Options
    # https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/X-Content-Type-Options
    # https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Referrer-Policy

    add_header X-Frame-Options        sameorigin  always;
    add_header X-Content-Type-Options nosniff     always;
    add_header Referrer-Policy        same-origin always;
    CSP_POLICY Content-Security-Policy "default-src 'self'  data: https://zaaksysteem.nl/ https://siteimproveanalytics.com https://www.gstatic.com/recaptcha/ https://www.google.com/recaptcha/api.js https://geodata.nationaalgeoregister.nl https://*.bus.koppel.app https://gistest.wetterskipfryslan.nl https://hofvantwente.obsurv.nl; frame-src 'self' https://gistest.wetterskipfryslan.nl/ https://gis.wetterskipfryslan.nl  https://www.google.com/ https://login.live.com/ https://*.officeapps.live.com/ https://*.bus.koppel.app; object-src 'self'; frame-ancestors 'self'; require-trusted-types-for 'script'; base-uri 'self'; connect-src *" always;
# } STOP Security-related headers

# { START backend

    location / {
        include fastcgi_params;
        fastcgi_param SCRIPT_NAME /;
        fastcgi_param HTTPS $fe_https;

        fastcgi_connect_timeout 60;
        fastcgi_send_timeout 3600;
        fastcgi_read_timeout 3600;
        fastcgi_buffer_size 128k;
        fastcgi_buffers 4 256k;
        fastcgi_busy_buffers_size 256k;
        fastcgi_temp_file_write_size 256k;
        fastcgi_intercept_errors on;

        ### Limit connections
        limit_conn_log_level notice;
        limit_conn_status 429;
        limit_conn perserver CONNECTION_PER_INSTANCE_LIMIT;

        set $proxyhost ZAAKSYSTEEM_HOST_API;
        fastcgi_pass $proxyhost:9083;
    }

    # This is a "UStore" download URL
    # See Zaaksysteem::Filestore::Engine::UStore for documentation
    location /download/ustore {
        internal;
        alias /opt/filestore;
    }
    
    # This is a "Swift" download URL
    # See Zaaksysteem::Filestore::Engine::Swift for documentation
    location ~ ^/download/swift/([A-Za-z0-9\.\_\-]+)/(http|https)/([A-Za-z0-9\.\-]+)/([1-9][0-9]+)/(.*) {
        internal;
    
        proxy_set_header X-Auth-Token $1;

        # Original:
        #   proxy_pass $2://$3:$4/$5;
        #
        # But we like to force HTTPS:
        #   proxy_pass https://$3:443/$5;
        set $download_url SWIFT_REDIRECT_URL;

        proxy_set_header Host $3;
        proxy_hide_header Content-Type;
        proxy_hide_header Accept-Ranges;

        proxy_pass $download_url;

        proxy_max_temp_file_size 0;
    }

    # This is a "S3" download URL
    # See Zaaksysteem::Filestore::Engine::S3 for documentation
    location ~ ^/download/s3/(http|https)/([^/]+)/(.*) {
        internal;

        set $download_protocol $1;
        set $download_host $2;
        set $download_uri $3;

        set $proxy_url $download_protocol://$download_host/$download_uri?$args;

        proxy_method GET;
        proxy_pass_request_body off;
        proxy_max_temp_file_size 0;

        proxy_set_header Host $download_host;
        proxy_set_header Authorization '';

        # Zaaksysteem determines (and hence overrides) the Content-Type.
        proxy_hide_header Content-Type;

        # Disable partial downloads -- it's all or nothing. This prevents the
        # PDF viewer from requesting the same file over and over.
        proxy_hide_header Accept-Ranges;
        proxy_hide_header x-amz-request-id;
        proxy_hide_header x-amz-meta-uid;
        proxy_hide_header x-amz-id-2;
        proxy_hide_header x-amz-meta-mode;
        proxy_hide_header x-amz-meta-mtime;
        proxy_hide_header x-amz-meta-gid;
        proxy_hide_header x-amz-version-id;

        proxy_pass $proxy_url;

        access_log /dev/stdout s3;

    }

# } END backend

# { START apps

    # API2CSV NodeJS App
    location /app/api2csv {
        set $proxyhost ZAAKSYSTEEM_HOST_CSV;
        proxy_pass http://$proxyhost:1030;
    }

    rewrite ^/apidocs$ /apidocs/ permanent;
    location /apidocs/ {
        set $proxyhost ZAAKSYSTEEM_HOST_SWAGGER;
        proxy_pass http://$proxyhost:8080;
    }

    rewrite ^/redoc$ /redoc/ permanent;
    location ~ /redoc/?(.*) {
        set $proxyhost ZAAKSYSTEEM_HOST_REDOC;
        proxy_pass http://$proxyhost:80/$1;
    }

    location /api/v2/admin {
        set $proxyhost ZAAKSYSTEEM_HOST_V2_ADMIN;
        proxy_pass http://$proxyhost:9084;
    }

    location /api/v2/cm {
        set $proxyhost ZAAKSYSTEEM_HOST_V2_CASE_MANAGEMENT;
        proxy_pass http://$proxyhost:9085;
    }

    location /api/v2/document {
        set $proxyhost ZAAKSYSTEEM_HOST_V2_DOCUMENT;
        proxy_request_buffering off;
        client_body_temp_path      /tmp/;
        client_body_in_file_only   on;
        client_body_buffer_size    1M;
        client_max_body_size       7G;
        proxy_pass http://$proxyhost:9086;
    }

    location /api/v2/file {
        set $proxyhost ZAAKSYSTEEM_HOST_V2_DOCUMENT;
        proxy_request_buffering off;
        client_body_temp_path      /tmp/;
        client_body_in_file_only   on;
        client_body_buffer_size    1M;
        client_max_body_size       7G;
        proxy_pass http://$proxyhost:9086;
    }

    location /api/v2/communication {
        set $proxyhost ZAAKSYSTEEM_HOST_V2_COMMUNICATION;
        proxy_pass http://$proxyhost:9087;
    }

    location /api/v2/geo {
        set $proxyhost ZAAKSYSTEEM_HOST_V2_GEO;
        proxy_pass http://$proxyhost:9084;
    }

    location /doc/api {
        alias /opt/zaaksysteem/apidocs/;
    }

    location ~ ^/(main|admin|my-pip|external-components|meeting) {
        set $proxyhost ZAAKSYSTEEM_HOST_FRONTEND_MONO;

        ## Make sure when the apps-nginx sends a redirect, we translate
        ## http://host:80 to https://host
        proxy_redirect http://$host:82 $scheme://$host;

        # Allow WebSocket connections
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "Upgrade";

        proxy_pass http://$proxyhost:82;
    }

# } END apps

location /zsnl_bag {
    proxy_pass https://development.zaaksysteem.nl:443;
    proxy_set_header Host "development.zaaksysteem.nl";
    proxy_ssl_name development.zaaksysteem.nl;
    proxy_ssl_verify off;
    proxy_ssl_server_name on;
}

# { START internal locations

    location ~ ^/(?<app>intern|mor|pdc|vergadering) {
        root /opt/zaaksysteem/root/assets;
        try_files $uri $uri/ /$app/index.html;

        error_page 404 =200 /$app/index.html;
    }

    # Exclude static files from running through the backend.
    location ~ ^/(assets|css|data\/|error_pages|examples|favicon\.ico|flexpaper|fonts|html|images|js|offline|partials|pdf\.js-with-viewer|robots.txt|tpl|webodf) {
        root /opt/zaaksysteem/root;
    }

# } STOP internal locations

