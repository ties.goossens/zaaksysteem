package Zaaksysteem::Export::TopX::Base;
use Moose::Role;
use Encode;

=head1 NAME

Zaaksysteem::Export::TopX::Base - Export cases in TopX format

=head1 DESCRIPTION

This model hides the logic for developers to transform data to TopX.

=head1 SYNOPSIS

    use Zaaksysteem::Export::TopX;

    my $model = Zaaksysteem::Export::TopX->new(
        user => $subject
    );

=cut

use List::Util qw(any);
use Scalar::Util qw(blessed);
use Zaaksysteem::XML::Compile;

with qw(
    Zaaksysteem::Moose::Role::Schema
    Zaaksysteem::Export::Tar
);

requires qw(_export_case_xml _export_case_document_xml);

has user => (
    is       => 'ro',
    isa      => 'Zaaksysteem::Schema::Subject',
    required => 1,
);

has objects => (
    is       => 'ro',
    isa      => 'Defined',
    required => 1
);

has xml => (
    is => 'ro',
    isa => 'Defined',
    builder => '_build_xml_instance',
    lazy => 1,
);

has case_relation => (
    is => 'ro',
    isa => 'Defined',
    builder => '_build_resultset_case_relations',
    lazy => 1,
);

has identification => (
    is => 'ro',
    isa => 'Str',
    predicate => 'has_identification',
);

has customer_config => (
    is       => 'ro',
    isa      => 'HashRef',
    lazy     => 1,
    builder  => '_build_customer_config',
    init_arg => undef,
);

sub _build_customer_config {
    my $self = shift;
    return $self->build_resultset('Config')->get_customer_config;
}

sub _build_resultset_case_relations {
    my $self = shift;
    return $self->build_resultset('CaseRelation');
}

sub _build_xml_instance {
    return Zaaksysteem::XML::Compile->xml_compile->add_class(
        'Zaaksysteem::XML::TopX::Instance'
    )->topx;
}

has confidentiality_mapping => (
    is      => 'ro',
    isa     => 'HashRef',
    default => sub {
        {
            public       => 'Niet vertrouwelijk',
            internal     => 'Organisatievertrouwelijk',
            confidential => 'Vertrouwelijk',
        }
    },
);

has file_confidentiality_mapping => (
    is      => 'ro',
    isa     => 'HashRef',
    default => sub {
        {
            'Confidentieel'     => 'Vertrouwelijk',
            'Openbaar'          => 'Niet vertrouwelijk',
            'Beperkt openbaar'  => 'Niet vertrouwelijk',
            'Intern'            => 'Organisatievertrouwelijk',
            'Zaakvertrouwelijk' => 'Niet vertrouwelijk',
        }
    },
);

has archive_mapping => (
    is      => 'ro',
    isa     => 'HashRef',
    default => sub {
        {
            'Bewaren (B)' => 'Overdracht',
            'Vernietigen (V)' => 'Vernietigen',
            'Overbrengen (O)' => 'Migratie',
        }
    },
);

has attribute_mapping => (
    is        => 'ro',
    isa       => 'ArrayRef',
    predicate => 'has_attribute_mapping',
);

sub _to_date {
    my ($self, $date) = @_;

    unless (blessed($date) && $date->isa('DateTime')) {
        $date = $self->_transform_case_attr_date($date);
    }
    return unless $date;
    return $date->set_time_zone('UTC')->iso8601 . 'Z';
}

sub _date_to_timestamp {
    my ($self, $date) = @_;

    $date = $self->_to_date($date);
    return unless defined $date;

    return (datumOfPeriode => { datumEnTijd => $date });
}

has _date_formatter => (
    is => 'ro',
    isa => 'DateTime::Format::Strptime',
    lazy => 1,
    builder => '_build_date_formatter',
);

sub _build_date_formatter {
    my $self = shift;
    return DateTime::Format::Strptime->new(
        time_zone => 'Europe/Amsterdam',
        pattern   => '%d-%m-%Y'
    );
}

sub _transform_case_attr_date {
    my ($self, $date) = @_;
    $date = $date->[0] if ref $date; # case attrs are arrayrefs by default
    my $dt = $self->_date_formatter->parse_datetime($date);
    return $dt if $dt;
    $self->log->info("Unable to parse date into DateTime object: $date");
    return;
}

my @ztc_optional  = (
    # Classificatie
    'ztc_classificatie_datum',
    'ztc_classificatie_bron',

    # Plaats
    'ztc_bewaarplaats',

    # Externe identificatiekenmerken
    'ztc_extern_kenmerk',
    'ztc_extern_kenmerk_toelichting',

    # Taal
    'ztc_taal',

    # Eventgeschiedenis
    # We don't use these
    #'ztc_conversiestatus',
    #'ztc_conversiestatus_toelichting',
    #'ztc_conversiestatus_verantwoordelijke',

    # Gebruiksrechten (documentniveau)
    # These should be on document level in the metadata, are missing
    #'ztc_gebruiksrechten_begindatum',
    #'ztc_gebruiksrechten_einddatum',
    #'ztc_gebruiksrechten_voorwaarden',

    # Openbaarheid
    'ztc_openbaarheid',
    'ztc_openbaarheid_embargodatum',

    # Integriteit
    'ztc_kwaliteitscontrole',

    # Dekking
    #'ztc_begindatum', # We use case registration date
    #'ztc_einddatum',  # We use case completion date
    'ztc_locatie_adres',
    'ztc_locatie_kaart',
    'ztc_locatie_omschrijving',
);

sub get_ztc_optional {
    return @ztc_optional;
}


sub _get_tmlo_fields_from_case {
    my ($self, $case) = @_;

    my $values = $case->field_values({use_magic_string_keys => 1});
    my %fields = map { $_ => $values->{$_} } @ztc_optional;
    if ($self->has_attribute_mapping) {
        foreach (@{$self->attribute_mapping}) {
            $fields{$_->{external_name}} = $values->{$_->{internal_name}};
        }
    }

    return \%fields;
}

sub _get_name_for_case {
    my ($self, $case) = @_;
    my @names;
    foreach (qw(onderwerp onderwerp_extern)) {
        my $name = $case->$_;
        push(@names, $name) if defined $name && length($name);
    }
    push (@names, $case->get_casetype_title);
    return \@names;
}

sub _get_event_log_for_case {
    my ($self, $case) = @_;

    my $rs = $case->loggings->search_rs(
        {
            created_by            => { '!=' => undef },
            created_by_name_cache => { '!=' => undef },
            -or => [{ event_type => { '!=' => 'case/view' } }]
        },
        { order => { -asc => 'id' } }
    );

    my @logging;
    while (my $entry = $rs->next) {
        push(
            @logging,
            {
                type                          => $entry->event_type,
                beschrijving                  => $entry->onderwerp,
                verantwoordelijkeFunctionaris => $entry->creator,
                $self->_date_to_timestamp($entry->created),
            }
        );
    }

    return \@logging;
}

sub _get_relations_for_case {
    my ($self, $case) = @_;

    my @relation;

    push(@relation, $self->generate_relation($self->identification, 'Archief'))
        if $self->has_identification;

    my $children = $case->zaak_children;
    while (my $child = $children->next) {
        push(@relation, $self->generate_relation($child->get_column, 'Dossier'))
    }

    my $rs = $self->case_relation->search({case_id => $case->id});
    while (my $rel = $rs->next) {
        my $rel_case = $rel->get_column("case_id_a") == $case->id
            ? $rel->case_id_b : $rel->case_id_a;

        push(
            @relation,
            $self->generate_relation(
                $rel_case->get_column, 'Dossier'
            )
        );
    }

    my $files = $case->search_active_files;
    while (my $file = $files->next) {
        push(@relation,
            $self->generate_relation($file->get_column('uuid'), 'Bestand'));
    }

    return \@relation;
}

sub _check_optional_tmlo {
    my ($self, $tmlo, @list) = @_;
    foreach (@list) {
        next if exists $tmlo->{$_} && defined $tmlo->{$_}[0];
        $self->log->info("Not able to find $_ in the case");
        return 0;
    }
    return 1;
}

sub _get_publicity {
    my ($self, $tmlo) = @_;

    return unless $self->_check_optional_tmlo($tmlo,
        qw(ztc_openbaarheid_embargodatum ztc_openbaarheid));

    return [
        {
            omschrijvingBeperkingen => $tmlo->{ztc_openbaarheid},
            $self->_date_to_timestamp($tmlo->{ztc_openbaarheid_embargodatum}),
        }
    ];

}

sub _get_classification {
    my ($self, $case, $tmlo) = @_;

    my $code = $case->get_classification_code;

    unless ($code) {
        $self->log->info("Skipping classification, no code found");
        return;
    }

    my $type = $case->get_process_type_name;
    unless ($type) {
        $self->log->info("Skipping classification, no process type name found");
        return;
    }

    my $source = $self->_get_single_attribute($tmlo, 'ztc_classificatie_bron');
    return unless defined $source;

    my $date = $self->_date_to_timestamp($tmlo->{ztc_classificatie_datum})
        if $tmlo->{ztc_classificatie_datum};

    my %classification = (
        code         => $code,
        omschrijving => $type,
        bron         => $source,
        $tmlo->{ztc_classificatie_datum}
            ? $self->_date_to_timestamp($tmlo->{ztc_classificatie_datum})
            : (),
    );

    return \%classification;

}

sub _get_storage_location {
    my ($self, $tmlo) = @_;
    return $self->_get_single_attribute($tmlo, 'ztc_bewaarplaats');
}

sub _get_external_reference {
    my ($self, $tmlo) = @_;

    my @extern = @{ $self->_get_array_attribute($tmlo, 'ztc_extern_kenmerk') // [] };

    my $size = @extern;
    my @external_refs;
    my $desc;
    for (my $i = 0; $i < $size; $i++) {
        $desc = $tmlo->{ztc_extern_kenmerk_toelichting}[$i];
        push(
            @external_refs,
            {
                nummerBinnenSysteem => $extern[$i],
                $desc ? (kenmerkSysteem => $desc) : (),
            }
        );
    }
    return \@external_refs;
}

sub _get_language {
    my ($self, $tmlo) = @_;

    if ($self->_check_optional_tmlo($tmlo, 'ztc_taal')) {
        return [ map { lc($_) } @{$tmlo->{ztc_taal}} ];
    }
    return ['dut']; # Let's default to Dutch
}

sub _get_confidentiality {
    my ($self, $case) = @_;

    return {
        classificatieNiveau => $self->confidentiality_mapping->{
            $case->confidentiality },
        $self->_date_to_timestamp($case->registratiedatum),
    }
}

sub _get_context {
    my ($self, $case, $tmlo) = @_;

    my $assignee = $case->behandelaar_object;
    return unless $assignee;

    my %context = (
        actor => {
            identificatiekenmerk => encode_utf8($assignee->naam),
            geautoriseerdeNaam   => $assignee->org_eenheid->name,
        },
    );
    return \%context;
}

sub _get_single_attribute {
    my ($self, $tmlo, $name) = @_;
    return unless $self->_check_optional_tmlo($tmlo, $name);
    return $tmlo->{$name}[0];
}

sub _get_array_attribute {
    my ($self, $tmlo, $name) = @_;
    return unless $self->_check_optional_tmlo($tmlo, $name);
    return $tmlo->{$name};
}

sub _get_integrity {
    my ($self, $tmlo) = @_;
    return $self->_get_single_attribute($tmlo, 'ztc_kwaliteitscontrole');
}

sub _get_coverage {
    my ($self, $case, $tmlo) = @_;

    my @geo;
    foreach (qw(kaart omschrijving adres)) {
        my $found = $self->_get_array_attribute($tmlo, "ztc_locatie_$_");
        push(@geo, @$found) if $found;
    }

    my @coverage = (
        {
            inTijd => {
                begin => { datumEnTijd => $self->_to_date($case->registratiedatum) },
                eind => { datumEnTijd => $self->_to_date($case->afhandeldatum) },
            },
            @geo ? ( geografischGebied => \@geo ) : (),
        }
    );

    return \@coverage;
}

sub _get_event_plan {
    my ($self, $case) = @_;

    my $result = $case->zaaktype_resultaat;
    my $mapping = $self->archive_mapping->{$result->archiefnominatie};
    return {
        type       => $mapping // $result->archiefnominatie,
        aanleiding => $result->resultaat,
        defined $result->comments ? (beschrijving => $result->comments) : (),
        $self->_date_to_timestamp($case->vernietigingsdatum),
    };
}

sub _create_archive_xml {
    my ($self) = @_;

    return unless $self->has_identification;

    my %data = (
        aggregatieniveau     => 'Archief',
        identificatiekenmerk => $self->identification,
        naam                 => 'TopX export',
    );

    return $self->_encode_to_xml({aggregatie => \%data});
}

sub _create_case_record_xml {
    my ($self, $id, $reference) = @_;

    my %data = (
        aggregatieniveau     => 'Record',
        identificatiekenmerk => $id,
        naam                 => "Record voor zaak $id",
        relatie              => $self->generate_relation($reference, 'Dossier'),
    );

    return $self->_encode_to_xml({aggregatie => \%data});
}

sub _create_case_xml {
    my ($self, $case) = @_;

    my $confidentiality = $self->_get_confidentiality($case);
    my $context         = $self->_get_context($case);
    my $event_plan      = $self->_get_event_plan($case);
    my $logging         = $self->_get_event_log_for_case($case);
    my $names           = $self->_get_name_for_case($case);
    my $relations       = $self->_get_relations_for_case($case);

    my $tmlo = $self->_get_tmlo_fields_from_case($case);

    my $classification = $self->_get_classification($case, $tmlo);
    my $coverage       = $self->_get_coverage($case, $tmlo);

    my $external_ref     = $self->_get_external_reference($tmlo);
    my $integrity        = $self->_get_integrity($tmlo);
    my $language         = $self->_get_language($tmlo);
    my $publicity        = $self->_get_publicity($tmlo);
    my $storage_location = $self->_get_storage_location($tmlo);

    my $id = $case->get_column('uuid');

    my %data = (
        aggregatieniveau => 'Dossier',

        identificatiekenmerk => $id,

        eventPlan         => $event_plan,
        naam              => $names,
        omschrijving      => $case->get_casetype_title,
        taal              => $language,
        vertrouwelijkheid => $confidentiality,

        $classification ? (classificatie              => $classification) : (),
        $context        ? (context                    => $context)        : (),
        $coverage       ? (dekking                    => $coverage)       : (),
        $external_ref   ? (externIdentificatiekenmerk => $external_ref)   : (),
        $integrity      ? (integriteit                => $integrity)      : (),
        $logging        ? (eventGeschiedenis          => $logging)        : (),
        $publicity      ? (openbaarheid               => $publicity)      : (),
        $relations      ? (relatie                    => $relations)      : (),
        $storage_location ? (plaats => $storage_location) : (),

        generiekeMetadata => [],
    );

    return try {
        return $self->_encode_to_xml({aggregatie => \%data});
    }
    catch {
        $self->log->info("Unable to serialize case: " . $case->id);
        die $_;
    };
}

sub _get_file_appearance {
    my ($self, $file) = @_;

    my $metadata = $file->metadata_id;

    unless ($metadata) {
        return {
            redactieGenre     => 'Geen ingesteld',
            verschijningsvorm => 'Geen ingesteld',
            structuur         => 'Geen ingesteld',
        };
    }

    return {
        redactieGenre     => $metadata->document_category // 'Geen ingesteld',
        verschijningsvorm => $metadata->appearance        // 'Geen ingesteld',
        structuur         => $metadata->structure         // 'Geen ingesteld',
    }
}

sub _get_file_confidentiality {
    my ($self, $file) = @_;

    my $metadata = $file->metadata_id;

    return unless $metadata && $metadata->trust_level;
    my $level = $metadata->trust_level;
    return unless $level;

    # Mapping from ZS types to TopX types, some match and are therefore not
    # mapped
    if (my $mapping = $self->file_confidentiality_mapping->{ $level }) {
        $level = $mapping;
    }

    return {
        classificatieNiveau => $level,
        $self->_date_to_timestamp($file->date_created),
    }
}

sub _get_file_rights {
    my ($self, $file) = @_;

    # idealy in file->metadata, but we don't have it

    return;
}

sub _get_file_format {
    my ($self, $file) = @_;
    my $fs = $file->filestore_id;

    return {
        identificatiekenmerk => $fs->uuid,
        bestandsnaam         => {
            naam     => $file->name,
            extensie => $file->extension
        },
        omvang             => $fs->size,
        bestandsformaat    => $fs->mimetype,
        fysiekeIntegriteit => {
            algoritme   => 'md5',
            waarde      => $fs->md5,
            datumEnTijd => $self->_to_date($fs->date_created),
        },
        datumAanmaak => $self->_to_date($file->date_created),
    };
}

sub generate_relation {
    my ($self, $relation_id, $type) = @_;

    return {
        relatieID   => $relation_id,
        typeRelatie => $type,
    };
}

sub _get_file_case_relation {
    my ($self, $file) = @_;

    my @relation;

    push(
        @relation,
        $self->generate_relation(
            $file->case_id->alternative_case_id,
            'Record'
        )
    );
    return \@relation;
}

sub _create_file_xml {
    my ($self, $file) = @_;

    my $confidentiality = $self->_get_file_confidentiality($file);
    my $rights          = $self->_get_file_rights($file);

    my %data = (
        aggregatieniveau     => 'Bestand',
        identificatiekenmerk => $file->uuid,
        naam                 => $file->filename,

        $confidentiality ? (vertrouwelijkheid => $confidentiality) : (),
        $rights          ? (gebruiksrechten   => $rights)          : (),

        relatie => $self->_get_file_case_relation($file),
        vorm    => $self->_get_file_appearance($file),
        formaat => $self->_get_file_format($file),
    );

    return try {
        return $self->_encode_to_xml({ bestand => \%data });
    }
    catch {
        $self->log->info("Unable to serialize file: " . $file->id);
        die $_;
    };
}

sub _encode_to_xml {
    my ($self, $data) = @_;

    return try {
        my $xml = $self->xml->tmlo_export('writer', $data);
        $self->log->trace($xml) if $self->log->is_trace;
        return $xml;
    }
    catch {
        $self->log->info("Unable to serialize data: " . dump_terse($data));
        die $_;
    };

}

sub export {
    my $self = shift;

    my $result = $self->objects;

    while (my $row = $result->next) {

        my $case = $row->get_source_object;

        if ($self->_export_case($case)) {
            $self->_export_case_documents($case);
        }

    }
    return 1;
}

sub _export_case {
    my ($self, $case) = @_;
    return unless $case->is_afgehandeld;

    my $xml = $self->_create_case_xml($case);
    return $self->_export_case_xml($case, $xml);
}

sub _export_case_documents {
    my ($self, $case) = @_;

    my $file_rs = $case->active_files->search_rs(undef,
        { prefetch => 'filestore_id' });

    while (my $file = $file_rs->next) {
        my $xml = $self->_create_file_xml($file);
        $self->_export_case_document_xml($case, $file, $xml);
    }
    return 1;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2020, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
