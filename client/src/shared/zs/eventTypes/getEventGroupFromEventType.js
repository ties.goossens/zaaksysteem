// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
export default (eventType) => {
  let type;

  switch (eventType) {
    case 'case/accept':
    case 'case/attribute/create':
    case 'case/attribute/remove':
    case 'case/attribute/removebulk':
    case 'case/attribute/update':
    case 'case/checklist/item/create':
    case 'case/checklist/item/remove':
    case 'case/checklist/item/update':
    case 'case/close':
    case 'case/close_child':
    case 'case/create':
    case 'case/delete':
    case 'case/duplicate':
    case 'case/duplicated':
    case 'case/early_settle':
    case 'case/email':
    case 'case/object/create':
    case 'case/object/delete':
    case 'case/object/update':
    case 'case/payment/status':
    case 'case/pip/feedback':
    case 'case/pip/updatefield':
    case 'case/reject':
    case 'case/relation/update':
    case 'case/reopen':
    case 'case/resume':
    case 'case/send_external_system_message':
    case 'case/subcase':
    case 'case/subject/add':
    case 'case/subject/remove':
    case 'case/suspend':
    case 'case/template/add':
    case 'case/template/render':
    case 'case/update/allocation':
    case 'case/update/case_type':
    case 'case/update/confidentiality':
    case 'case/update/field':
    case 'case/update/milestone':
    case 'case/update/piprequest':
    case 'case/update/purge_date':
    case 'case/update/registration_date':
    case 'case/update/relation':
    case 'case/update/requester':
    case 'case/update/result':
    case 'case/update/settle_date':
    case 'case/update/status':
    case 'case/update/target_date':
    case 'case/update/zorginstituut':
    case 'file/trash':
    case 'email/send':
      type = 'case';
      break;

    case 'case/document/accept':
    case 'case/document/assign':
    case 'case/document/copy':
    case 'case/document/create':
    case 'case/document/label':
    case 'case/document/metadata/update':
    case 'case/document/publish':
    case 'case/document/reject':
    case 'case/document/remove':
    case 'case/document/rename':
    case 'case/document/replace':
    case 'case/document/restore':
    case 'case/document/revert':
    case 'case/document/trash':
    case 'case/document/unpublish':
      type = 'document';
      break;

    case 'subject/contact_moment/create':
      type = 'contactmoment';
      break;

    case 'case/note/create':
    case 'contactmoment/note':
    case 'object/note':
    case 'subject/internalnote/create':
      type = 'note';
      break;

    case 'object/create':
    case 'object/delete':
    case 'object/queue/item_failed':
    case 'object/relation/add':
    case 'object/relation/delete':
    case 'object/update':
    case 'object/view':
      type = 'object';
      break;

    default:
      type = 'other';
      break;
  }

  return type;
};
