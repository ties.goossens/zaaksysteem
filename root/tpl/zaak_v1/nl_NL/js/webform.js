/* global mintloader_enabled,invoked_validate_function */

/* ***** BEGIN LICENSE BLOCK ********************************************
 * Version: EUPL 1.1
 *
 * The contents of this file are subject to the EUPL, Version 1.1 or
 * - as soon they will be approved by the European Commission -
 * subsequent versions of the EUPL (the "Licence");
 * you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://joinup.ec.europa.eu/software/page/eupl
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is Zaaksysteem
 *
 * The Initial Developer of the Original Code is
 * Mintlab B.V. <info@mintlab.nl>
 *
 * Portions created by the Initial Developer are Copyright (C) 2009-2011
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 * Michiel Ootjers <michiel@mintlab.nl>
 * Jonas Paarlberg <jonas@mintlab.nl>
 * Jan-Willem Buitenhuis <jw@mintlab.nl>
 * Peter Moen <peter@mintlab.nl>
 *
 * ***** END LICENSE BLOCK ******************************************** */

/*

triggered by a change in any input element, the webform is submitted and reloaded

*/

var ezra_webform_last_saved_state;
var webform_autosave_timeout_handle;

$(document).ready(function () {
    $("form.webform").each(function () {
        var form = $(this);

        installWebformListener(form);

        if (form.closest(".ezra_case_active_phase").length) {
            loadWebform({
                form: form,
            });
        }
    });
});

var webformTextfieldUpdateRegistry = new WebformTextfieldUpdateRegistry();
var previous_wymeditor_xhtml = {};
var poll_wym_editor_handle;
var case_wym_editor_handles = {};

// when a complete form is reloaded, stop polling. otherwise
// a race condition may occur: the wymeditor has an update pending,
// but in the meantime another field has been clicked which is
// hiding and clearing the wymeditor field. prevent the wymeditor
// field from re-submitting the value after it was just cleared.
function unregister_wymeditor_poll(my_form) {
    var form_id = my_form.attr("id");

    if (case_wym_editor_handles.hasOwnProperty(form_id)) {
        var poll_wym_editor_handle = case_wym_editor_handles[form_id];
        clearTimeout(poll_wym_editor_handle);
    }
}

function register_wymeditor_poll(my_form) {
    unregister_wymeditor_poll(my_form);

    var form_id = my_form.attr("id");

    case_wym_editor_handles[form_id] = setTimeout(function () {
        my_form.find(".ezra_dagobert_editor").each(function () {
            var element = $(this);
            var wymeditor_index = element.attr("wymeditor_index");
            var wym = jQuery.wymeditors(wymeditor_index);
            var xhtml;

            if (!wym.ready()) {
                return;
            }

            xhtml = wym.xhtml();

            if (!previous_wymeditor_xhtml.hasOwnProperty(wymeditor_index)) {
                previous_wymeditor_xhtml[wymeditor_index] = xhtml;
            }
            if (previous_wymeditor_xhtml[wymeditor_index] != xhtml) {
                previous_wymeditor_xhtml[wymeditor_index] = xhtml;
                wym.update();
                webformTextfieldUpdateRegistry.scheduleUpdate(element);
            }
        });

        register_wymeditor_poll(my_form);
    }, 200);
}

function notifyUploadComplete(options) {
    console.log("upload complete");
    var selector = options.pip ? 'form[name="caseForm"]' : "form.webform";

    $(selector + ' input[name="' + options.fieldname + '"]').each(function () {
        var element = $(this);
        var my_form = element.closest("form.webform");

        var spinner = my_form.find(".spinner-groot");
        spinner.css("visibility", "hidden");

        var mintloader = element.closest(".mintloader");
        var mintloader_element = mintloader.find(".new_upload").show();

        var uploadResponse = mintloader.find(".uploadresponse");
        var unaccepted_files = options.unaccepted_files;

        if (unaccepted_files > 0) {
            mintloader.find(".clear_queue_ie").show();
            mintloader
                .find('input[name="unaccepted_files_present"]')
                .val(unaccepted_files);
        }

        mintloader_element.find(".mintloader-error").remove();

        if (options.success) {
            var path =
                "/form/upload/remove_upload?kenmerk_id=" + options.fieldname;
            if ($("#zaak_id").attr("class")) {
                path = "/zaak/" + $("#zaak_id").attr("class") + "/remove_file/";
            }

            mintloader_element.append(
                '<div class="fileuploadthumbnail"><div class="filename_formatted">' +
                    options.filename +
                    "</div>" +
                    "</div>"
            );
        } else {
            mintloader_element.append(
                '<div class="has-icon mintloader-error">' +
                    '<div class="icon-alert icon" style="padding-left: 6px; margin-right: 6px;"></div>' +
                    options.error_message +
                    "</div>"
            );
        }
    });
}

function upload_file_ie(element) {
    var form = element.closest("form.webform");

    // in validate.js a handler is put on anything.
    // form.unbind('submit');

    var action = form.attr("action"); // save temp

    // in intake context, add the file to the active session
    var upload_action = "/upload";

    if ($("#zaak_id").attr("class")) {
        upload_action = "/zaak/" + $("#zaak_id").attr("class") + "/upload";
        if (form.attr("name") === "pip") {
            upload_action = "/pip" + upload_action;
        }
    } else if (form.hasClass("webform")) {
        upload_action = "/form/upload";
    }

    form.attr("action", upload_action);
    var fieldname = element.attr("name");

    var iframe_name = fieldname + "_file_upload_iframe";
    form.attr("target", iframe_name);

    var spinner = form.find(".spinner-groot");
    spinner.css("visibility", "visible");

    form.submit();
    form.removeAttr("target");
    form.attr("action", action);
    $.ztWaitStop();
    spinner.css("visibility", "hidden");
    element.replaceWith(element.clone(true));
}

function installWebformListener(form) {
    form.on("change", "input, select, textarea", function (e) {
        var element = $(this);

        if (element.hasClass("veldoptie_bag_adres")) {
            return;
        }

        if (element.hasClass("ezra-disable_fieldUpdate")) {
            return;
        }

        // file uploads shouldn't trigger an immediate refresh, since they take a while.
        // file uploader can trigger a reload
        if (element.attr("type") && element.attr("type") == "file") {
            if (!mintloader_enabled()) {
                upload_file_ie(element);
            }

            return;
        }

        updateField(element);
    });

    var handleTextInput = function (event) {
        var element = $(this);

        if (
            element.hasClass("ezra-disable_fieldUpdate") ||
            element.hasClass("veldoptie_bag_adres")
        ) {
            return;
        }

        webformTextfieldUpdateRegistry.scheduleUpdate(element);
    };

    form.on(
        "change, keypress, focus, paste, textInput, input",
        "input, textarea",
        handleTextInput
    );

    // TRAC 1270 - prevent enter key submitting form
    form.on("keypress", "input", function (event) {
        if (event.which == 13) {
            event.preventDefault();
        }
    });

    // only for zaak intake side.
    form.find(".webformcontent .submit_to_previous").click(function () {
        var container = $("form.webform");
        var extraopt = "submit_to_previous=1";
        var action = container.attr("action");

        if (action.match(/\?/)) {
            action += "&" + extraopt;
        } else {
            action += "?" + extraopt;
        }

        container.attr("action", action);
        container.unbind("submit").submit();

        return false;
    });

    form.find(".webformcontent .stap a").click(function (e) {
        var container = $("form.webform"),
            parseUrlParams = window.zsFetch("nl.mintlab.utils.parseUrlParams"),
            url = $(this).attr("href"),
            params;

        params = parseUrlParams(url);

        if (params.process_step_index === undefined) {
            return;
        }

        form.find('[name="process_step_index"]').val(params.process_step_index);

        container.attr("action", url);
        container.unbind("submit").submit();

        if (e.preventDefault) {
            e.preventDefault();
        }

        return false;
    });

    form.on("click", ".webformcontent [name=submit_to_next]", function () {
        var extraopt = "submit_to_next=1",
            doCheat = $("form.webform input[name=do_cheat]").is(":checked"),
            isValid = doCheat || $("form.webform .ng-invalid").length === 0;

        if (!isValid) {
            angular
                .element(document.body)
                .scope()
                .$root.$broadcast("systemMessage", {
                    type: "error",
                    content:
                        "Niet alle velden zijn correct ingevuld. Corrigeer de ongeldige velden en probeer het opnieuw.",
                });

            return false;
        }

        if ($("form.webform input[name=allow_cheat]").val() == "1" && doCheat) {
            extraopt += "&allow_cheat=1";
        }

        zvalidate($(this).closest("form"), {
            extraopt: extraopt,
        });

        return false;
    });

    form.on("click", ".webformcontent input.cancel_webform", function () {
        if ($("input[name='externe_login'][type='hidden']").val() == "1") {
            location.href = "/form/cancel";
        } else {
            location.href = "/";
        }

        return false;
    });

    register_wymeditor_poll(form);

    // unlock
    // form.on('click', '.ezra_unlock_registration_phase', function () {
    //     loadWebform({
    //         form: form,
    //         specialAction: 'unlock_registration_phase=1'
    //     });
    //     return false;
    // });

    // re-lock
    form.on("click", ".ezra_lock_registration_phase", function () {
        loadWebform({
            form: form,
            specialAction: "lock_registration_phase=1",
        });

        return false;
    });
}

function submitFileUpload() {
    var my_form = $("form.webform");

    var zaak_id = "0";

    if ($("#zaak_id").length) {
        zaak_id = $("#zaak_id").attr("class");
    }

    my_form
        .find(".spinner-groot .spinner-groot-message")
        .html("Een moment geduld, het bestand wordt toegevoegd.");
    my_form.find(".spinner-groot").css("visibility", "visible");
    my_form.unbind("submit").submit();
    my_form.attr("target", "");
    $.ztWaitStop();
}

function loadWebform(options) {
    var my_form = options.form;
    var callback = options.callback;
    var values = options.values;

    var spinner = my_form.find(".spinner-groot").first();
    spinner.css("visibility", "visible");

    var action =
        my_form.attr("action") +
        (values == undefined ? "fields" : "update_field");

    var phase = my_form.find("input[name=fase]").val();

    var xhr = my_form.data("xhr");
    var targetEl = my_form.find(".webformcontent .webform_inner");

    if (values == undefined) {
        values = "fase=" + phase;
    } else {
        webformTextfieldUpdateRegistry.clearPhase(my_form.attr("id"));
        unregister_wymeditor_poll(my_form);
    }

    // form action is determined on the actions. need a special case here,
    // can't get it without refactoring this whole function.
    // right way to do this is with a more elaborate javascript setup.
    if (options.specialAction) {
        values += "&" + options.specialAction;
    }

    xhr = $.ajax(action, {
        data: values,
        type: "POST",
    });

    my_form.data("xhr", xhr);

    xhr.done(function (responseText) {
        my_form.removeData("xhr");

        targetEl.html(responseText);

        clearTimeout(webform_autosave_timeout_handle);

        ezra_tooltip_handling();
        ezra_basic_functions();

        initializeEverything(targetEl.children());
        if (callback) {
            callback();
        }

        register_wymeditor_poll(my_form);
        spinner.css("visibility", "hidden");

        if (invoked_validate_function) {
            zvalidate(my_form, {
                dont_focus_first_error: true,
            });
        }

        if (options != undefined && options.hasOwnProperty("element")) {
            var element_id = options.element.attr("id");

            if (!options.element.hasClass("hasDatepicker")) {
                $("#" + element_id).focus();
            }
        }

        var angEl = angular.element(targetEl[0]),
            webformController = angEl.inheritedData("$zsCaseWebformController");

        if (webformController) {
            webformController.setLoaded();
        }
    });

    return false;
}

function updateField(element, callback /*, name*/) {
    if (
        element
            .closest("form.webform")
            .hasClass("ezra_disable_webform_update") ||
        element.hasClass("ng-invalid")
    ) {
        return;
    }

    if (element.hasClass("hasDatepicker")) {
        veldoptie_handling();
    }

    var webformController = angular
            .element(element[0])
            .inheritedData("$zsCaseWebformController"),
        rule_field = element.closest(".regels_enabled_kenmerk").length,
        webformFieldController = angular
            .element(element[0])
            .inheritedData("$zsCaseWebformFieldController");

    if (webformFieldController) {
        webformController.invalidate(webformFieldController.getAttributeName());
    }

    if (!rule_field) {
        webformTextfieldUpdateRegistry.clearField(element);
    }
}

function getCurrentTime() {
    var currentTime = new Date();
    var hours = currentTime.getHours();

    if (hours < 10) {
        hours = "0" + hours;
    }

    var minutes = currentTime.getMinutes();

    if (minutes < 10) {
        minutes = "0" + minutes;
    }

    var result = hours + ":" + minutes + " ";

    return result;
}
