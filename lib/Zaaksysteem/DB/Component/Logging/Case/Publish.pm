package Zaaksysteem::DB::Component::Logging::Case::Publish;
use Moose::Role;
with qw(Zaaksysteem::Moose::Role::LoggingSubject);


use JSON;

sub onderwerp {
    my $self = shift;

    return "Zaak gepubliceerd.";
}

sub onderwerp_extended {
    my $self = shift;

    return $self->onderwerp . "<br/><strong>Unpublish</strong>: " .
       $self->htmlify($self->data->{unpublish}) . "<br/><strong>Publish</strong>: " .
       $self->htmlify($self->data->{publish});
}

sub htmlify {
    my ($self, $xml) = @_;

    $xml =~ s|\<|&lt;|gis;
    $xml =~ s|\>|&gt;|gis;

    return '<pre class="logging-code">' . $xml . '</pre>';
}

around TO_JSON => sub {
    my $orig = shift;
    my $self = shift;

    my $data = $self->$orig(@_);

    $data->{ content } = "Unpublish:\n" . $self->data->{unpublish} .
        "\n\nPublish: " . $self->data->{publish};

    $data->{ expanded } = JSON::false;

    return $data;
};

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 htmlify

TODO: Fix the POD

=cut

=head2 onderwerp

TODO: Fix the POD

=cut

=head2 onderwerp_extended

TODO: Fix the POD

=cut

