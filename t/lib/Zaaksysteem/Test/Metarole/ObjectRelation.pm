package Zaaksysteem::Test::Metarole::ObjectRelation;

use Zaaksysteem::Test;

use UUID4::Tiny qw[create_uuid_string];

use Zaaksysteem::Object::Value;
use Zaaksysteem::Object::Reference::Instance;

package MetaroleRelationTestObject {
    use Moose;

    extends 'Zaaksysteem::Object';

    has foo => (
        is => 'rw',
        type => 'metarole_relation_test_object',
        label => 'foo attr',
        traits => [qw[OR]],
    );

    override type => sub {
        return 'metarole_relation_test_object'
    };
};

sub test_object_relation_zss_value {
    my $foo_a_id = create_uuid_string();
    my $foo_b_id = create_uuid_string();
    my $foo_c_id = create_uuid_string();

    my $foo_a = MetaroleRelationTestObject->new(id => $foo_a_id);
    my $foo_b = MetaroleRelationTestObject->new(id => $foo_b_id);
    my $foo_c = MetaroleRelationTestObject->new(id => $foo_c_id);

    $foo_a->foo($foo_b);

    my $foo_a_foo = $foo_a->_zss_get('foo');

    isa_ok $foo_a_foo, 'Zaaksysteem::Object::Value';
    is $foo_a_foo->type_name, 'object_ref', 'dereferenced foo value type name';
    is $foo_a_foo->value->id, $foo_b_id, 'dereferenced foo value reference id';

    my $new_value = Zaaksysteem::Object::Value->new(
        type => $foo_a_foo->type,
        value => Zaaksysteem::Object::Reference::Instance->new(
            type => $foo_c->type,
            id => $foo_c_id
        )
    );

    lives_ok {
        $foo_a->_zss_set('foo', $new_value);
    } 'setting new foo relation value';

    is $foo_a->foo->id, $foo_c_id, 'correct reference value set';
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
