// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

(function () {
  angular.module('Zaaksysteem').directive('ngFocusIn', function () {
    return function (scope, element, attrs) {
      element.bind('focusin', function () {
        scope.$eval(attrs.ngFocusIn);
      });
    };
  });
})();
