package Zaaksysteem::Controller::Auth;

use Moose;

use Zaaksysteem::Access;
use BTTW::Tools;

use Crypt::OpenSSL::Random qw(random_bytes);
use Digest::SHA qw(sha256_base64);
use DateTime;
use NetAddr::IP;
use List::Util qw(any);

BEGIN { extends 'Zaaksysteem::Controller' }

=head1 NAME

Zaaksysteem::Controller::Auth - Login/Logoff and authentication actions

=head1 DESCRIPTION

=head1 ACTIONS

=head2 login

This action provides the flow to login a user.

=head3 URL

C</auth/login>

=cut

sub login : Local : Access(*) {
    my ($self, $c) = @_;

    ### In case of an XHR, and we get here...define we are not logged in
    if ($c->req->is_xhr) {
        $c->res->status('401');
    }

    # Internal logins
    my $interface = $c->model('DB::Interface')->find_by_module_name('authldap');

    $c->stash->{no_js_includes} = 1;
    $c->stash->{ can_login_internally } = $interface->jpath('$.enable_user_login');

    # SAML
    $c->stash->{ idps } = [
        $c->model('DB::Interface')->search_module('samlidp', '$.login_type_user')
    ];

    $c->stash->{ success_endpoint } = $c->uri_for('/auth/login_saml');
    $c->stash->{ failure_endpoint } = $c->uri_for('/auth/saml/failure');

    my $referer = $c->flash->{ referer } // $c->req->params->{ referer };

    # set_referer is "smart", and URI-escapes as required
    $c->session->{ referer } = $c->set_referer($referer) if $referer;

    if ($c->user_exists && !$c->user->is_external_api) {
        $c->detach('login_success');
    }

    $c->stash->{ referer }    = $c->session->{ referer };

    if (exists $c->req->params->{ username }) {
        # Prevent internal logins with spoofed requests
        unless ($c->stash->{ can_login_internally }) {
            my $msg = sprintf(
                'Login attempt for user "%s", but internal logins are disabled',
                $c->req->params->{ username }
            );

            $c->log->warn($msg);

            throw('auth/login_disabled', $msg);
        }

        my $username = lc($c->req->params->{ username });

        $c->detach('login_failure') unless length $username;

        my $auth_ok = try {
            $c->assert_zs_login_token();

            return $c->authenticate({
                username => $username,
                password => $c->req->params->{ password },
            });
        } catch {
            $c->log->error('Something went wrong when logging in: ' . $_);
            return;
        };

        if ($auth_ok) {
            $auth_ok->log_login_attempt(
                ip      => $c->get_client_ip,
                success => 1,
                method  => 'regular',
            );
            $c->detach('login_success');
        }
        else {
            my $user = $c->model('DB::Subject')->search_rs(
                {
                    username     => $username,
                    subject_type => 'employee',
                }
            )->first;


            if ($user) {
                $user->log_login_attempt(
                    ip      => $c->get_client_ip,
                    success => 0,
                    method  => 'regular',
                );
            }

            $c->detach('login_failure');
        }

    }

    ### Not logged in, return to auth/login OR directly to SAML provider when directlogin is set and login_screen = 0
    if ($c->stash->{ idps } && @{ $c->stash->{ idps } } == 1 && !$c->stash->{ prevent_saml }) {
        my ($saml_interface) = @{ $c->stash->{ idps } };

        if ($saml_interface->jpath('$.login_type_user_direct')) {
            $c->res->redirect($c->uri_for(
                sprintf('/auth/saml/%d', $saml_interface->id),
                {
                    success_endpoint => $c->stash->{success_endpoint},
                    failure_endpoint => $c->uri_for('/auth/saml/failure'),
                }
            ));
            $c->res->body('');
            $c->detach;
        }
    }

    $c->create_zs_login_token();

    $c->stash->{template} = 'auth/login.tt';
    $c->detach;
}

=head2 logout

This action will logout and wipe the session of the currently logged in
user.

=head3 URL

C</auth/logout>

=cut

sub logout : Local : Access(*) {
    my ($self, $c) = @_;

    if ($c->session->{_saml_state}) {
        $c->forward('/auth/saml/initiate_single_logout');
    }

    $c->logout();
    $c->delete_session();

    $c->stash->{ message } = 'You have been logged out';

    if ($c->req->is_xhr) {
        $c->res->body('OK');
        $c->detach;
    }

    $c->res->redirect($c->uri_for('/auth/page'));

    # Make sure this action cannot be part of a forward/go chain.
    $c->detach;
}

=head2 login_saml

Perform an employee login using the SAML IdP configured for those logins.

=head3 URL

C</auth/login_saml>

=cut

sub login_saml : Local : Access(*) {
    my ($self, $c) = @_;

    $c->get_saml_session(1);

    my $saml_state = $c->session->{ _saml } || {};

    if (!$saml_state->{ success }) {
        $c->res->redirect($c->uri_for("/auth/login"));
        $c->detach;
    }

    # Load correct IdP interface into authentication module
    my $interface = $c->model('DB::Interface')->find($c->query_session('$._saml_state.idp_id'));

    unless (defined $interface && $interface->active) {
        throw('auth/saml/interface_config/invalid', sprintf(
            'SAML Identiy Provider interface configuration could not be found'
        ));
    }

    unless ($interface->jpath('$.login_type_user')) {
        throw('auth/saml/interface_config', sprintf(
            'Configuration does not allow employee logins using the "%s" SAML Identity Provider',
            $interface->name
        ));
    }

    $c->get_auth_realm('saml')->credential->interface($interface);

    if ($c->authenticate($saml_state, 'saml')) {
        $c->user->log_login_attempt(
            ip      => $c->get_client_ip,
            success => 1,
            method  => 'SAML-ADFS',
        );
        $c->detach("login_success");
    } else {
        my $username = $saml_state->{uid};

        my $user = $c->model('DB::Subject')->search_rs(
            {
                username     => $username,
                subject_type => 'employee',
            }
        )->first;

        if ($user) {
            $user->log_login_attempt(
                ip      => $c->get_client_ip,
                success => 0,
                method  => 'SAML-ADFS',
            );
        }

        $c->detach("login_failure");
    }
}

=head2 page

The login page itself.

=head3 URL

C</auth/page>

=cut

sub page : Local : Access(*) {
    my ($self, $c) = @_;

    $c->stash->{ prevent_saml } = 1;

    $c->detach('login');
}

=head2 retrieve_roles

This action hydrates a set of roles under a specific organisational unit.

This action is only referenced in C<zaaksysteem.js> code, and should be
regarded as I<deprecated>.

=head3 URL

C</auth/retrieve_roles/[OU_ID]>

=head3 Parameters

Additionally to the required C<OU> identifier, this action checks for the
C<select_complete_afdeling> GET parameter. If set to a true-ish value, the
'null' role will be added to the response. This role implies a wildcard for
any role within the OU.

=head3 Example response

    {
       "json" : {
          "roles" : [
             {
                "has_children" : null,
                "entry" : {
                   "gidNumber" : "30032",
                   "cn" : "Burgemeester",
                   "memberUid" : [
                      "cn=cvanderknaap,ou=College,o=dev,dc=zaaksysteem,dc=nl",
                      "cn=evanmilligen,ou=College,o=sprint,dc=zaaksysteem,dc=nl",
                      "cn=rslotjes,ou=Beheer,o=sprint,dc=zaaksysteem,dc=nl"
                   ],
                   "description" : "Burgemeester"
                },
                "system" : 0,
                "name" : "Burgemeester",
                "children" : [],
                "internal_id" : "30032",
                "members" : [
                   "cn=cvanderknaap,ou=College,o=dev,dc=zaaksysteem,dc=nl",
                   "cn=evanmilligen,ou=College,o=sprint,dc=zaaksysteem,dc=nl",
                   "cn=rslotjes,ou=Beheer,o=sprint,dc=zaaksysteem,dc=nl"
                ],
                "counter" : 7,
                "type" : "posixGroup",
                "dn" : "cn=Burgemeester,ou=College,o=sprint,dc=zaaksysteem,dc=nl"
             },
             "split",
             .
             .
             .
          ]
       }
    }

The C<roles> key is the main output of this action. Take not of the C<split>
string included in the example. This is not meant to be a selectable role, but
is meant to inform the frontend select widget to insert a non-selectable split
visually.

=cut

sub retrieve_roles : Local : DB('RO') {
    my ($self, $c, $ouid)   = @_;

    return unless $c->req->is_xhr;

    my $all_roles = $c->model('DB::Roles')->get_all_cached($c->stash);

    # Select all system roles regardless
    my @roles = grep { $_->system_role } @{ $all_roles };

    # If an OUID was provided, add
    if ($ouid) {

        my $group = $c->model('DB::Groups')->search_rs(
            { id => $ouid },
            { columns => 'path' }
        )->first;
        my @groups = @{$group->path};

        foreach (@{$all_roles}) {
            next if $_->system_role;
            my $group_id = $_->get_column('parent_group_id');

            push(@roles, $_) if any { $_ == $group_id } @groups;
        }
    }

    my @rv = sort { $a->{ name } cmp $b->{ name } } map {
        {
            children    => [],
            name        => ($_->system_role ? '' : '* ') . $_->name,
            internal_id => $_->id
        }
    } @roles;

    if($c->req->params->{ select_complete_afdeling }) {
        unshift @rv, 'split';
        unshift @rv, {
            children        => [],
            name            => 'Gehele afdeling',
            internal_id     => ''
        };
    }

    my $json = {
        'roles' => \@rv
    };

    $c->stash->{json} = $json;
    $c->forward('Zaaksysteem::View::JSONlegacy');
}

=head1 PRIVATE ACTIONS

=head2 login_success

Post-login success hook which increments the login counters, sets a XSRF
token, and decides if the user should be redirected and whereto.

=cut

sub login_success : Private {
    my ($self, $c) = @_;

    $c->statsd->increment('employee.login.ok.count', 1);

    delete $c->session->{login_failed};

    $c->change_session_id;

    my $random = random_bytes(16);

    $c->session->{ _xsrf_token } = sha256_base64(
        $random,
        $c->user->username,
        $c->config->{gemeente_id},
        DateTime->now->datetime,
    );

    $c->res->cookies->{ 'XSRF-TOKEN' } = {
        value => $c->session->{ _xsrf_token },
        secure => ($c->req->uri->scheme eq 'https' ? 1 : 0) # HAAAAACK
    };

    # Ensure there's no response body on the redirects below:
    $c->res->body('');

    # User exists, but hasn't been sorted into a department yet. Redirect to
    # first_login to set up the preferred_group_id and notify the admin(s)
    # responsible.
    if (!$c->user->is_sorted) {
        $c->res->redirect($c->uri_for('/first_login'));
    }
    elsif ($c->session->{referer}) {
        $c->res->redirect($c->session->{referer});
    } else {
        $c->res->redirect($c->uri_for('/'));
    }
    $c->detach;
}

=head2 login_failure

Private action, called when an employee login fails.

Sends a message to statsd and ups a counter in the session so a nice error
message can be shown.

=cut

sub login_failure : Private {
    my ($self, $c) = @_;

    $c->statsd->increment('employee.login.failed.count', 1);

    $c->session->{ login_failed }++;

    $c->res->redirect($c->uri_for('/auth/page'));
    $c->res->body('');

    $c->detach;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
