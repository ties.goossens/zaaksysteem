// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  'use strict';

  angular.module('Zaaksysteem').directive('zsLineChart', [
    'smartHttp',
    '$timeout',
    '$q',
    function (smartHttp, $timeout, $q) {
      return {
        scope: {
          zql: '=zsLineChartZql',
          profile: '=zsLineChartProfile',
          message: '=zsLineChartMessage',
          loading: '=zsLineChartLoading',
          exceedsLimit: '&',
        },
        link: function (scope, element /*, attrs*/) {
          var container = $(element).find('#search_query_chart_container'),
            httpCancel;

          function render(chartData) {
            if (!chartData.chart) {
              chartData.chart = {};
            }

            chartData.chart.renderTo = 'search_query_chart_container';

            //see how wide the chart is, so the exported graph will look the same
            chartData.exporting = { width: container.width() };

            if (scope.zsLineChartProfile === 'afhandeltermijn') {
              chartData.tooltip = {
                formatter: function () {
                  return '<b>' + this.point.name + '</b>: ' + this.y + ' %';
                },
              };
            }

            if (chartData.series) {
              container.highcharts(chartData);
              scope.message = '';
            } else {
              // necessary to work around highcharts behaviour
              container.html('');
              if (!scope.exceedsLimit()) {
                scope.message = 'Geen resultaten';
              } else {
                scope.message =
                  'Uw zoekopdracht bevat meer dan 2500 zoekresultaten. Verfijn de zoekopdracht om een grafiek te kunnen tonen, of richt een specifieke koppeling in voor managementinformatie.';
              }
            }
          }

          function reloadData() {
            var zql = scope.zql,
              profile = scope.profile;

            if (httpCancel) {
              httpCancel.resolve();
              httpCancel = null;
              scope.loading = false;
            }

            if (!zql || !profile || !profile.value || scope.exceedsLimit()) {
              render({});
              return;
            }

            scope.loading = true;
            httpCancel = $q.defer();

            smartHttp
              .connect({
                url: '/api/object_chart',
                method: 'GET',
                params: {
                  zql: zql,
                  chart_profile: profile.value,
                },
                timeout: httpCancel.promise,
              })
              .success(function (response) {
                render(response.json);
              })
              .error(function (response) {
                if (response) {
                  scope.message = response.result
                    ? response.result[0].messages.join(', ')
                    : 'Er is een onbekende fout opgetreden. Probeer het later opnieuw.';
                }
              })
              ['finally'](function () {
                httpCancel = null;
                scope.loading = false;
              });
          }

          scope.$watch('zql', function () {
            reloadData();
          });
          scope.$watch('profile', function () {
            reloadData();
          });
        },
      };
    },
  ]);
})();
