// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import get from 'lodash/get';
import uniqueId from 'lodash/uniqueId';
import vormTemplateServiceModule from '../../vormTemplateService';
import vormInvokeModule from '../../vormInvoke';
import geojsonTemplate from './geojson.html';
import geojsonDisplay from './geojsonDisplay.html';

const mapHeight = '315px';

export default angular
  .module('vorm.types.geojson', [vormTemplateServiceModule, vormInvokeModule])
  .directive('vormGeojson', [
    'vormInvoke',
    () => {
      return {
        template: geojsonTemplate,
        restrict: 'E',
        require: ['vormGeojson', 'ngModel'],
        scope: {
          delegate: '&',
          templateData: '&',
          inputId: '&',
          tabindex: '&',
        },
        bindToController: true,
        controller: [
          function () {
            let ctrl = this;
            let ngModel;
            let name;

            const templateData = ctrl.templateData();

            ctrl.context = () => {
              return templateData.context;
            };

            ctrl.link = (controllers) => {
              [ngModel] = controllers;
              ctrl.value = ngModel.$modelValue;

              name = uniqueId('geojson-');

              ngModel.$formatters.push((val) => {
                ctrl.value = val;
                return val;
              });
            };

            ctrl.getName = () => {
              return name;
            };

            ctrl.getMapSettings = () => get(templateData, 'mapSettings');

            ctrl.getInitialFeature = () => {
              const value = ctrl.value;

              if (!value) {
                return '';
              }

              return value;
            };

            ctrl.canDrawFeatures = () => true;

            ctrl.getMapHeight = () => mapHeight;

            ctrl.handleChange = (value) => {
              ngModel.$setViewValue(value);
            };
          },
        ],
        controllerAs: 'vormGeojson',
        link: (scope, element, attrs, controllers) => {
          controllers.shift().link(controllers);
        },
      };
    },
  ])
  .directive('vormGeojsonDisplay', [
    () => {
      return {
        template: geojsonDisplay,
        restrict: 'E',
        require: ['vormGeojsonDisplay'],
        scope: {
          delegateValue: '&',
          templateData: '&',
          inputId: '&',
          tabindex: '&',
        },
        bindToController: true,
        controller: [
          function () {
            let ctrl = this;

            const templateData = ctrl.templateData();

            ctrl.context = () => {
              return templateData.context;
            };

            ctrl.getMapSettings = () => get(templateData, 'mapSettings');

            ctrl.getInitialFeature = () => {
              const value = ctrl.delegateValue();

              return value;
            };

            ctrl.getMapHeight = () => mapHeight;
          },
        ],
        controllerAs: 'vormGeojsonDisplay',
      };
    },
  ])
  .run([
    'vormTemplateService',
    (vormTemplateService) => {
      vormTemplateService.registerType('geojson', {
        control: angular.element(
          '<vorm-geojson ng-model role="geojson" aria-labelledby="{{vm.getInputId()}}-label"></vorm-geojson>'
        ),
        display: angular.element(
          '<vorm-geojson-display delegate-value="delegate.value"></vorm-geojson-display>'
        ),
        defaults: {
          disableClear: true,
        },
      });
    },
  ]).name;
