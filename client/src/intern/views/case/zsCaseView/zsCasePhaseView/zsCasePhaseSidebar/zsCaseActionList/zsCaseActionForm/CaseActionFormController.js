// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import find from 'lodash/find';
import get from 'lodash/get';
import last from 'lodash/last';
import map from 'lodash/map';
import merge from 'lodash/merge';
import assign from 'lodash/assign';
import seamlessImmutable from 'seamless-immutable';
import email from './../../../../../forms/email';
import subCase from './../../../../../forms/subCase';
import templateForm from './../../../../../forms/template';
import allocation from './../../../../../forms/allocation';
import subject from '../../../../../forms/subject';
import getPreviewObjectFromValues from '../../../../../library/getPreviewObjectFromValues';

// in the old days an e-mail could only be sent to one colleague
// in casetype management this is still true and the data is still stored in the old way
// old: { behandelaar: 'betrokkene-medewerker-<id>', betrokkene_naam: <name> }
// new: { behandelaar: [ { id: <id>, name: <name> }, ... ] }
// this function checks whether it's old or new, and then returns as new
const getBehandelaar = (action) => {
  const { rcpt, behandelaar, betrokkene_naam } = action;

  if (rcpt === 'behandelaar' && behandelaar) {
    return typeof behandelaar === 'string'
      ? [
          {
            id: behandelaar.split('-')[2],
            label: betrokkene_naam || behandelaar,
          },
        ]
      : [...behandelaar];
  }

  return null;
};

const convertActionDataToEmailTemplate = (data) => {
  return {
    recipient_type: data.rcpt,
    betrokkene_role: data.betrokkene_role,
    email_subject: data.subject,
    email_content: data.body,
    email: data.email,
    id: data.zaaktype_notificatie_id,
    bibliotheek_notificaties_id: {
      label: 'action',
    },
  };
};

const getRole = (subjects, role) =>
  subjects.find((subject) => subject.role === role);

const getRoleUuid = (subjects, role) =>
  get(getRole(subjects, role), 'uuid', null);

export default class CaseActionFormController {
  static get $inject() {
    return [
      '$scope',
      'resource',
      'composedReducer',
      'vormValidator',
      'dateFilter',
    ];
  }

  constructor(scope, resource, composedReducer, vormValidator, dateFilter) {
    let ctrl = this;
    let action = ctrl.action();
    let fields = [];
    let reducers = [];
    let values = seamlessImmutable({});
    let form;
    let verb;
    let locals;

    let getData = () => {
      return reducers.reduce((value, reducer) => {
        return reducer(value);
      }, values);
    };

    const subjects = (ctrl.subjectRelations() || []).map((subjectRelation) => ({
      uuid: get(subjectRelation, 'relationships.subject.data.id'),
      name: get(
        subjectRelation,
        'relationships.subject.data.meta.display_name'
      ),
      role: get(subjectRelation, 'attributes.role'),
    }));

    let getSubcaseRequestor = () => {
      switch (get(getData(), 'subcase_requestor_type')) {
        case 'aanvrager':
          return getRoleUuid(subjects, 'Aanvrager');
        case 'anders':
          return get(getData(), 'eigenaar_uuid');
        case 'behandelaar':
          return getRoleUuid(subjects, 'Behandelaar');
        case 'betrokkene': {
          const concernedRole = get(getData(), 'eigenaar_role');

          return getRoleUuid(subjects, concernedRole);
        }
        case 'ontvanger':
          return getRoleUuid(subjects, 'Ontvanger');
        default:
          return null;
      }
    };

    const getPreviewFromValues = getPreviewObjectFromValues(scope);

    switch (action.type) {
      case 'case':
        {
          let startDate =
            action.data.start_delay &&
            action.data.start_delay.match(/^(\d{1,4}-?){3}$/)
              ? action.data.start_delay.split('-')
              : null;
          let startAfter =
            action.data.start_delay && action.data.start_delay.match(/^\d+$/)
              ? Number(action.data.start_delay)
              : String(action.data.start_delay);

          let defaultCopySubjectRoles = action.data.subject_role
            ? action.data.subject_role.reduce(
                (acc, role) => assign(acc, { [role]: true }),
                {}
              )
            : null;

          form = subCase({
            requestorName: get(ctrl.requestor(), 'instance.name'),
            phaseOptions: map(ctrl.phases(), (phaseObj) => ({
              value: String(phaseObj.seq),
              label: phaseObj.name,
            })).reverse(),
            isLastPhase:
              find(ctrl.phases(), {
                id: action.data.zaaktype_status_id,
              }) === last(ctrl.phases()),
            toggleExpand: () => {
              locals = locals.merge({ expanded: !locals.expanded });
            },
            relatedCasetypeUuid: action.data.related_casetype_uuid ? 1 : 0,
          });

          values = values.merge({
            allocation: {
              unit: String(action.data.ou_id),
              role: String(action.data.role_id),
            },
            creation_style: action.data.creation_style,
            copy_subject_role: defaultCopySubjectRoles !== null,
            copy_roles: defaultCopySubjectRoles,
            subcase_requestor_type: action.data.eigenaar_type,
            subcase_requestor_role: action.data.eigenaar_role,
            resolve_in_phase: action.data.required,
            type: action.data.relatie_type,
            requestor_type: action.data.eigenaar_searchtype,
            requestor:
              action.data.eigenaar_type === 'anders' && action.data.eigenaar_id
                ? {
                    label: action.data.eigenaar_id,
                    data: {
                      id: action.data.eigenaar_id.match(/\d+$/)[0],
                      uuid: action.data.eigenaar_uuid,
                      type: action.data.eigenaar_searchtype,
                    },
                  }
                : null,
            relatedCasetypeUuid: action.data.related_casetype_uuid,
            copy_attributes: Number(action.data.kopieren_kenmerken) === 1,
            copy_related_cases: Number(action.data.copy_related_cases) === 1,
            copy_related_objects:
              Number(action.data.copy_related_objects) === 1,
            copy_selected_attributes: action.data.copy_selected_attributes,
            automatic_assignment:
              Number(action.data.automatisch_behandelen) === 1,
            start_date: startDate
              ? new Date(
                  Number(startDate[2]),
                  Number(startDate[1]) - 1,
                  Number(startDate[0])
                )
              : null,
            start_after: startAfter,
            relaties_betrokkene_role_set:
              Number(action.data.relaties_betrokkene_role_set) === 1,
          });

          verb = 'Starten';

          reducers = reducers.concat((vals) => {
            return {
              creation_style: vals.creation_style,
              ou_id: get(vals.allocation, 'unit'),
              role_id: get(vals.allocation, 'role'),
              eigenaar_type: vals.subcase_requestor_type,
              eigenaar_role: vals.subcase_requestor_role,
              copy_subject_role: vals.copy_subject_role,
              subject_role: vals.copy_subject_role
                ? Object.keys(vals.copy_roles).filter(
                    (key) => vals.copy_roles[key]
                  )
                : [],
              required: vals.resolve_in_phase,
              relatie_type: vals.type,
              eigenaar_searchtype:
                vals.subcase_requestor_type === 'anders'
                  ? vals.requestor_type
                  : null,
              eigenaar_id:
                vals.subcase_requestor_type === 'anders'
                  ? `betrokkene-${vals.requestor_type}-${get(
                      vals.requestor,
                      'data.id'
                    )}`
                  : null,
              eigenaar_uuid:
                vals.subcase_requestor_type === 'anders' &&
                vals.requestor != null &&
                vals.requestor.data['uuid']
                  ? vals.requestor.data['uuid']
                  : null,
              kopieren_kenmerken: vals.copy_attributes ? 1 : 0,
              copy_related_cases: vals.copy_related_cases ? 1 : 0,
              copy_related_objects: vals.copy_related_objects ? 1 : 0,
              automatisch_behandelen: vals.automatic_assignment ? 1 : 0,
              copy_selected_attributes: vals.copy_selected_attributes,
              start_delay:
                vals.type === 'vervolgzaak_datum'
                  ? dateFilter(vals.start_date, 'dd-MM-yyyy')
                  : vals.type === 'vervolgzaak'
                  ? String(vals.start_after)
                  : null,
              subcase_requestor_type: vals.subcase_requestor_type,
              relaties_betrokkene_role_set: vals.relaties_betrokkene_role_set
                ? 1
                : 0,
            };
          });
        }
        break;
      case 'template':
        {
          form = templateForm(
            {
              templates: seamlessImmutable(ctrl.templates()).asMutable({
                deep: true,
              }),
              caseDocuments:
                seamlessImmutable(ctrl.caseDocuments()).asMutable({
                  deep: true,
                }) || [],
            },
            {
              showTemplateSelect: false,
            }
          );

          values = values.merge({
            template: action.data.bibliotheek_sjablonen_id,
            name: action.data.filename,
            filetype: action.data.target_format,
            case_document: action.data.bibliotheek_kenmerken_id,
          });

          reducers = reducers.concat((vals) => ({
            filename: vals.name,
            target_format: vals.filetype,
            bibliotheek_kenmerken_id: vals.case_document,
          }));

          verb = 'Aanmaken';
        }
        break;
      case 'email':
        {
          let getRequestorLink = () => {
            const subjectTypeV2 = {
              natuurlijk_persoon: 'person',
              bedrijf: 'organization',
              medewerker: 'employee',
            };
            const subjectType = get(ctrl.requestor(), 'instance.subject_type');
            const uuid = get(ctrl.requestor(), 'instance.uuid');

            return `/main/contact-view/${subjectTypeV2[subjectType]}/${uuid}`;
          };

          const caseCustomHtmlTemplateName = ctrl.caseCustomHtmlTemplateName();

          const customHtmlTemplateName =
            caseCustomHtmlTemplateName &&
            caseCustomHtmlTemplateName !== 'undefined'
              ? caseCustomHtmlTemplateName
              : undefined;

          form = email(
            {
              requestorName: get(ctrl.requestor(), 'instance.name'),
              requestorLink: getRequestorLink(),
              presetRequestor:
                get(ctrl.requestor(), 'instance.preset_client', 'Nee') === 'Ja',
              templates: [convertActionDataToEmailTemplate(action.data)],
              customHtmlTemplateName,
            },
            {
              showTemplateSelect: false,
            }
          );

          values = values.merge({
            recipient_type: action.data.rcpt,
            betrokkene_role: action.data.betrokkene_role,
            email_subject: action.data.subject,
            email_content: action.data.body,
            recipient_cc: action.data.cc,
            recipient_bcc: action.data.bcc,
            requestor_address: get(ctrl.requestor(), 'instance.email'),
            recipient_address:
              action.data.rcpt === 'overig'
                ? action.data.email
                : action.data.rcpt === 'aanvrager'
                ? get(ctrl.requestor(), 'instance.email')
                : null,
            behandelaar: getBehandelaar(action.data),
            template: action.data.zaaktype_notificatie_id,
          });

          if (action.data.case_document_attachments.length) {
            values = values.merge({
              case_documents: action.data.case_document_attachments,
            });
          }

          verb = 'Versturen';

          reducers = reducers.concat((vals) => {
            let result = {
              rcpt: vals.recipient_type,
              betrokkene_role: vals.betrokkene_role,
              subject: vals.email_subject,
              body: vals.email_content,
              cc: vals.recipient_cc,
              bcc: vals.recipient_bcc,
            };

            switch (vals.recipient_type) {
              case 'behandelaar':
                result.behandelaar = vals.behandelaar;
                break;
              case 'aanvrager':
                result.email = get(ctrl.requestor(), 'instance.email');
                break;
              case 'overig':
                result.email = vals.recipient_address;
                break;
              default:
                result.email = null;
                break;
            }

            if (action.data.case_document_attachments.length) {
              result = merge(result, {
                case_document_attachments: vals.case_documents.map(
                  (caseDoc) => caseDoc.case_document_ids
                ),
              });
            }

            return result;
          });
        }
        break;
      case 'allocation':
        {
          form = allocation({});

          values = values.merge({
            allocation: {
              unit: String(action.data.ou_id),
              role: String(action.data.role_id),
            },
          });

          reducers = reducers.concat((vals) => {
            return {
              ou_id: vals.allocation.unit,
              role_id: vals.allocation.role,
            };
          });

          verb = 'Wijzigen';
        }
        break;
      case 'subject':
        {
          form = subject({
            name: action.data.naam,
            role: action.data.rol,
            magic_string_prefix: action.data.magic_string_prefix,
            is_authorized: action.data.gemachtigd ? 'Ja' : 'Nee',
            email_confirmation: action.data.notify ? 'Ja' : 'Nee',
          });
        }
        break;
    }

    values = seamlessImmutable(form.getDefaults()).merge(values);

    locals = seamlessImmutable({
      $values: values,
      expanded: false,
      toggleTitle: false,
      emailPreviewActive: false,
    });

    resource('/api/v1/subject/role', { scope })
      .asPromise()
      .then((data) => {
        if (data) {
          const roleOptions = data.asMutable().map((item) => {
            const { label } = item.instance;

            return {
              label,
              value: label,
            };
          });

          fields = form.fields(roleOptions);
        }
      });

    const validityReducer = composedReducer(
      { scope },
      () => values,
      () => locals
    ).reduce((vals, loc) => {
      return vormValidator(fields, vals, null, loc);
    });

    const saveValidityReducer = composedReducer(
      { scope },
      () => values,
      () => locals
    ).reduce((vals, loc) => {
      let filteredFields = fields.filter((field) => {
        return field.name !== 'recipient_unavailable';
      });

      return vormValidator(filteredFields, vals, null, loc);
    });

    ctrl.getFields = () => fields;

    ctrl.getValues = () => values;

    ctrl.handleChange = (name, value) => {
      values = form.processChange(name, value, values);
      locals = locals.merge(
        {
          $values: values,
        },
        { deep: true }
      );
    };

    ctrl.handleSaveClick = () => {
      ctrl.onActionSave({ $data: getData() });
    };

    ctrl.handleExecuteClick = () => {
      let vals = {
        caseId: ctrl.caseId(),
        caseTypeUuid: action.data.related_casetype_uuid,
        aanvrager: getSubcaseRequestor(),
        contactkanaal: 'behandelaar',
      };

      if (
        get(getData(), 'creation_style') === 'form' &&
        vals.caseTypeUuid &&
        vals.aanvrager
      ) {
        const link = `/intern/aanvragen/${vals.caseTypeUuid}/?aanvrager=${
          vals.aanvrager || ''
        }&contactkanaal=${vals.contactkanaal}&origin=${vals.caseId}`;

        ctrl.onActionSave({ $data: getData(), $destination: link });
      } else {
        ctrl.onActionExecute({ $data: getData() });
      }
    };

    ctrl.getVerb = () => verb;

    ctrl.isValid = () => get(validityReducer.data(), 'valid');

    ctrl.canContinue = () => {
      if (
        action.type === 'case' &&
        get(getData(), 'subcase_requestor_type') != 'behandelaar' &&
        get(getData(), 'subcase_requestor_type') != 'betrokkene' &&
        get(getData(), 'subcase_requestor_type') != 'ontvanger'
      ) {
        return !!getSubcaseRequestor();
      } else {
        return true;
      }
    };

    ctrl.isSaveable = () => get(saveValidityReducer.data(), 'valid');

    ctrl.getValidity = () => get(validityReducer.data(), 'validations');

    ctrl.getLocals = () => locals;

    ctrl.canSave = () => ctrl.isSaveable();

    ctrl.canExecute = () => {
      return !action.automatic && ctrl.isValid() && ctrl.canContinue();
    };

    ctrl.canEdit = () => {
      let canEdit = form.getCapabilities().edit;

      if (canEdit === undefined) {
        return true;
      }

      return canEdit;
    };

    ctrl.toggleEmailPreview = () => {
      locals = locals.merge({
        emailPreviewActive: !locals.emailPreviewActive,
        previewData: getPreviewFromValues(values),
      });
    };

    ctrl.getEmailPreviewData = () => {
      return locals.previewData;
    };

    ctrl.isEmailAction = () => ctrl.action().type === 'email';
  }
}
