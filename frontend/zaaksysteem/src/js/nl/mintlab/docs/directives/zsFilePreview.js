// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

(function () {
  var EMPTY_GIF =
    'data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==';

  angular.module('Zaaksysteem.docs').directive('zsFilePreview', [
    '$document',
    '$window',
    'templateCompiler',
    '$timeout',
    function ($document, $window, templateCompiler, $timeout) {
      var fromLocalToGlobal = window.zsFetch(
          'nl.mintlab.utils.dom.fromLocalToGlobal'
        ),
        fromGlobalToLocal = window.zsFetch(
          'nl.mintlab.utils.dom.fromGlobalToLocal'
        ),
        getViewportSize = window.zsFetch(
          'nl.mintlab.utils.dom.getViewportSize'
        ),
        setMouseEnabled = window.zsFetch(
          'nl.mintlab.utils.dom.setMouseEnabled'
        ),
        getComputedStyle = $window.getComputedStyle,
        body = $document.find('body'),
        popup,
        img,
        arrow,
        center,
        popup_content;

      function createPopup(element) {
        popup = angular.element(element);
        popup_content = popup[0].querySelector('.file-preview-content');

        setMouseEnabled(popup[0]);

        body.append(popup);
        img = angular.element(popup[0].querySelector('img.file-preview-image'));
        img.bind('load', onImageLoad);
        arrow = angular.element(popup[0].querySelector('.file-preview-arrow'));
        popup.bind('webkitTransitionEnd transitionend', onTransitionEnd);
      }

      function showPopup(cnt) {
        center = cnt;
        popup.addClass('file-preview-visible');
        positionPopup();
      }

      function hidePopup() {
        popup.removeClass('file-preview-visible');
      }

      function onTransitionEnd() {
        if (getComputedStyle(popup[0]).opacity === '0') {
          img.attr('src', EMPTY_GIF);
        }
      }

      function setImage(url) {
        img.attr(
          'src',
          url + '&time=' + Math.round(new Date().getTime() / 10000)
        );
      }

      function onImageLoad() {
        $timeout(function () {
          positionPopup();
        });
      }

      function positionPopup() {
        var target = fromGlobalToLocal(popup[0].offsetParent, center),
          clientWidth,
          clientHeight,
          hOrient,
          viewportSize = getViewportSize(),
          arrowY = clientHeight / 2 - arrow[0].clientHeight / 2;

        // 16 = 8 + 8 (padding left+right, top+bottom)
        clientWidth = popup_content.clientWidth - 16;
        clientHeight = popup_content.clientHeight - 16;

        if (target.x - clientWidth > 0) {
          hOrient = 'left';
          target.x -= clientWidth;
        } else {
          hOrient = 'right';
        }

        // Prevent popup going over top of viewport
        if (target.y - clientHeight / 2 < 50) {
          arrowY = target.y - 50;
          target.y = 50;
          // Prevent popup going under bottom of viewport
        } else if (target.y + clientHeight / 2 > viewportSize.height) {
          arrowY = 99999;
          target.y = viewportSize.height - clientHeight - 15;
          // If popup is between top and bottom
        } else {
          arrowY += 5;
          target.y -= clientHeight / 2;
        }

        popup.css('left', target.x + 'px');
        popup.css('top', target.y + 'px');

        popup.attr('data-zs-file-preview-horizontal-orientation', hOrient);

        arrow.css('top', arrowY + 'px');
      }

      templateCompiler
        .getElement('/partials/directives/popup/file-preview.html')
        .then(createPopup);

      return {
        compile: function (/*tElement, tAttrs, transclude*/) {
          return function link(scope, element /*, attrs*/) {
            scope.showPreview = function () {
              var center,
                origin,
                url = scope.pip
                  ? scope.entity.pip_thumbnail_url
                  : scope.entity.thumbnail_url;

              if (!url) {
                return;
              }

              // 16 = 8 + 8 (padding left+right, top+bottom)
              url +=
                '?max_width=' +
                (popup_content.clientWidth - 16) +
                '&max_height=' +
                (popup_content.clientHeight - 16);

              setImage(url);

              center = {
                x: element[0].clientWidth / 2,
                y: element[0].clientHeight / 2,
              };
              origin = fromLocalToGlobal(element[0], center);

              showPopup(origin);
            };

            scope.hidePreview = function () {
              hidePopup();
            };
          };
        },
      };
    },
  ]);
})();
