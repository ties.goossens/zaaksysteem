// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  var MAX_CONTENT_LENGTH = 400;

  angular
    .module('Zaaksysteem.timeline')
    .controller('nl.mintlab.timeline.TimelineController', [
      '$scope',
      '$window',
      '$sce',
      'smartHttp',
      function ($scope, $window, $sce, smartHttp) {
        var TimelineEvent = window.zsFetch('nl.mintlab.timeline.TimelineEvent'),
          indexOf = window.zsFetch('nl.mintlab.utils.shims.indexOf'),
          safeApply = window.zsFetch('nl.mintlab.utils.safeApply');

        var _lastResult,
          _itemsById = {},
          _contactChannels = [
            {
              value: 'behandelaar',
              label: 'Behandelaar',
            },
            {
              value: 'balie',
              label: 'Balie',
            },
            {
              value: 'telefoon',
              label: 'Telefoon',
            },
            {
              value: 'post',
              label: 'Post',
            },
            {
              value: 'email',
              label: 'Email',
            },
            {
              value: 'webformulier',
              label: 'Webformulier',
            },
            {
              value: 'sociale media',
              label: 'Sociale media',
            },
          ],
          _filtersById = {};

        $scope.availableFilters = [
          {
            id: 'case',
            label: 'Zaken',
          },
          {
            id: 'contactmoment',
            label: 'Contactmomenten',
          },
          {
            id: 'document',
            label: 'Documenten',
          },
          {
            id: 'note',
            label: 'Notities',
          },
          {
            id: 'question',
            label: 'Vragen',
          },
          {
            id: 'product',
            label: 'Producten',
          },
        ];

        $scope.activeFilters = [];
        $scope.items = [];
        $scope.loading = false;

        function processFilters() {
          angular.forEach($scope.availableFilters, function (filter) {
            _filtersById[filter.id] = filter;
          });
        }

        processFilters();

        function loadData() {
          var url,
            params = {};

          if (!$scope.loading && (!_lastResult || _lastResult.next)) {
            if (_lastResult) {
              url = _lastResult.next;
              params = {};
            } else if ($scope.category === 'object') {
              url = '/api/object/' + $scope.id + '/action/log';
            } else {
              url = '/event/list';
              params = {
                for: $scope.id,
                category: $scope.category,
              };
            }

            $scope.loading = true;
            smartHttp
              .connect({
                method: 'GET',
                url: url,
                params: params,
              })
              .success(handleSuccess)
              .error(handleError);
          } else {
            $scope.loading = false;
          }
        }

        function handleSuccess(data) {
          $scope.loading = false;
          var items = data && data.result ? data.result : [],
            event;

          _lastResult = data;

          angular.forEach(items, function (item) {
            event = getItemById(item.id);
            for (var key in item) {
              event[key] = item[key];
            }

            if (event.changes) {
              event.changes = event.changes.map(function (change) {
                var values = [change.new_value, change.old_value].map(function (
                  value
                ) {
                  var mapped;

                  if (!_.isArray(value)) {
                    value = [value];
                  }

                  mapped = _.filter(value, _.identity)
                    .map(function (val) {
                      var formatted;

                      formatted =
                        typeof val === 'object' ? val['original_name'] : val;

                      return formatted;
                    })
                    .join(', ');

                  return mapped;
                });

                return {
                  field: change.field.replace('attribute.', ''),
                  new_value: values[0],
                  old_value: values[1],
                };
              });
            }
          });
          invalidateScroll();
        }

        function handleError() {
          $scope.loading = false;
        }

        function getItemById(id) {
          if (_itemsById[id]) {
            return _itemsById[id];
          }
          var event = new TimelineEvent();
          _itemsById[id] = event;
          $scope.items.push(event);
          return event;
        }

        function invalidateScroll() {
          $scope.$broadcast('scrollinvalidate');
        }

        $scope.toggleFilter = function (filter) {
          if (indexOf($scope.activeFilters, filter) === -1) {
            $scope.activateFilter(filter);
          } else {
            $scope.deactivateFilter(filter);
          }
        };

        $scope.activateFilter = function (filter) {
          if (indexOf($scope.activeFilters, filter) === -1) {
            $scope.activeFilters.push(filter);
          }
          invalidateScroll();
        };

        $scope.deactivateFilter = function (filter) {
          var index = indexOf($scope.activeFilters, filter);
          if (index !== -1) {
            $scope.activeFilters.splice(index, 1);
          }
          invalidateScroll();
        };

        $scope.isFiltered = function (filterId) {
          var filter = _filtersById[filterId];
          return (
            $scope.activeFilters.length === 0 ||
            indexOf($scope.activeFilters, filter) !== -1
          );
        };

        $scope.getItemVisibility = function (item) {
          var isFiltered = $scope.isFiltered(item.event_category);

          if (!isFiltered) {
            isFiltered =
              $scope.activeFilters.length &&
              _.some($scope.activeFilters, function (filter) {
                return filter.filter !== undefined
                  ? _.find([item], filter.filter)
                  : false;
              });
          }

          return isFiltered;
        };

        $scope.initialize = function (category, id) {
          $scope.category = category;
          $scope.id = id;

          if (category === 'case') {
            $scope.availableFilters = [
              { id: 'contactmoment', label: 'Contactmomenten' },
              { id: 'case-mutation', label: 'Wijzigingen' },
              { id: 'document', label: 'Documenten' },
              { id: 'note', label: 'Notities' },
            ];

            processFilters();
          } else if (category === 'object') {
            $scope.availableFilters = [
              {
                id: 'created',
                label: 'Aangemaakt',
                filter: { event_type: 'case/object/create' },
              },
              {
                id: 'related',
                label: 'Gerelateerd',
                filter: { event_type: 'case/object/relate' },
              },
              {
                id: 'updated',
                label: 'Gewijzigd',
                filter: { event_type: 'case/object/update' },
              },
              {
                id: 'deleted',
                label: 'Verwijderd',
                filter: { event_type: 'case/object/delete' },
              },
              {
                id: 'note',
                label: 'Notities',
                filter: { event_type: 'object/note' },
              },
            ];
          }
        };

        $scope.loadMoreData = function () {
          if (!$scope.loading) {
            loadData();
          }
        };

        $scope.getTruncatedText = function (item) {
          var content = item.content,
            match;
          if (!content) {
            return '';
          }

          if (content.length > MAX_CONTENT_LENGTH) {
            content = content.substr(0, MAX_CONTENT_LENGTH);
            match = content.match(/^([\s\S]*)\s+/m);
            if (match) {
              content = match[0];
            }
            content += ' ...';
          }

          return content;
        };

        /*
								'berichtenbox' and 'mijnoverheid' terms are both used in the event_type,
								depending on whether the Mijnoverheid Berichtenbox message is sent through
								the Documents screen or via a phase change.
						*/

        $scope.isTypeWithAttachments = function (item) {
          return (
            item.event_type &&
            (item.event_type.indexOf('email') === 0 ||
              item.event_type.indexOf('berichtenbox') !== -1 ||
              item.event_type.indexOf('mijnoverheid') !== -1)
          );
        };

        $scope.isTruncated = function (item) {
          return (
            item.content &&
            (!$scope.isTypeWithAttachments(item) ||
              item.content.length > MAX_CONTENT_LENGTH)
          );
        };

        $scope.showItemContent = function (item, expanded) {
          return (
            !$scope.isTypeWithAttachments(item) &&
            !(item.expanded === false && expanded)
          );
        };

        $scope.getSpotEnlighterLabel = function (rel) {
          var label;

          if ($scope.category === 'case') {
            if (rel && rel.object_type === 'bedrijf') {
              label = 'handelsnaam';
            } else if (rel && rel.voorletters !== undefined) {
              label = "voorletters + ' ' + geslachtsnaam";
            } else {
              label = 'naam';
            }
          } else {
            label = "id + ': ' + zaaktype_node_id.titel";
          }

          return label;
        };

        $scope.downloadLog = function () {
          $window.open(
            '/event/download?for=' +
              $scope.id +
              '&category=' +
              $scope.category +
              '&context=none'
          );
        };

        $scope.getItemDescription = function (item) {
          return $sce.trustAsHtml(item.description);
        };

        $scope.handleAttachmentClick = function (item, attachment) {
          $window.open(
            '/zaak/' +
              item.case_id +
              '/document/' +
              attachment.file_id +
              '/download'
          );
        };

        $scope.$on('scrollend', function (event /*, from, upto*/) {
          safeApply($scope, function () {
            loadData();
            event.stopPropagation();
          });
        });
      },
    ]);
})();
