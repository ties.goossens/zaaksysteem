package Zaaksysteem::Zaken::Roles::HStore;

use Moose::Role;

use BTTW::Tools;
use Data::Visitor::Callback;
use Hash::Flatten qw[unflatten flatten];
use List::Util qw[first];
use List::MoreUtils qw[any all];
use Zaaksysteem::Constants::Locks qw(TOUCH_CASE_OBJECT_RELATIONSHIPS_LOCK);
use Zaaksysteem::Object::Types::Case::Route;
use Zaaksysteem::Object::Types::Case::Result;
use Zaaksysteem::Object::Types::Case::Milestone;
use Zaaksysteem::StatsD;

=head1 NAME

Zaaksysteem::Zaken::Roles::HStore - Role that adds hstore column updating ability.

=head1 METHODS

=head2 update_hstore

Update the "hstore_properties" column with the value of the
C<object_attributes> attribute.

=cut

sub update_hstore {
    my $self = shift;

    my $case_object = $self->object_data;
    return if not $case_object;

    $case_object->class_uuid($self->zaaktype_id->_object->uuid);

    $self->clear_object_attributes();

    $case_object->replace_object_attributes(@{ $self->object_attributes });
    $case_object->acl_groupname($self->confidential ? 'confidential' : 'public');

    $self->update_object_relations;


    return $case_object->update;
}

=head2 update_object_relations

Update the object relations for the given case.

=cut

sub update_object_relations {
    my $self = shift;
    my $case = shift || $self->object_data;
    my @names = @_;

    @names = qw(
        casetype
        route
        assignee
        requestor
        coordinator
        outcome
        milestone
    ) if !@names;

    my $relations = $case->object_relation_object_ids->search_rs(
        { name => { -in => \@names } }
    );


    my %have_relations;
    while (my $rel = $relations->next) {
        $have_relations{$rel->name} = $rel;
    }

    # We don't need to update anything here
    return if keys %have_relations == @names;

    my $casetype_updated = 0;
    foreach (@names) {
        next if $have_relations{$_};
        $self->_update_object_relation($case, $_);
        $casetype_updated = 1 if $_ eq 'casetype';
    }

    # Update our preview strings in objects referencing this case. The hard
    # way, because we can't rely on the object model to do if for us.
    # XXX split brain
    $case->object_relation_object_uuids->update(
        {
            object_preview =>
                sprintf('%s %s', $self->id, $self->zaaktype_node_id->titel)
        }
    ) if $casetype_updated;

    return;
}

=head2 update_case_properties

This method updates the C<case_property> related table to reflect the state
of the object based on the current L</object_attributes>. Uses the
case_property upsert method to lock as little as possible.

=cut

sig update_case_properties => '@Zaaksysteem::Object::Attribute';

sub update_case_properties {
    my $self = shift;

    $self->_update_case_properties(@_);
    $self->_update_case_subject();
}

sig _update_case_properties => '@Zaaksysteem::Object::Attribute';

sub _update_case_properties {
    my ($self, @attributes) = @_;

    # Prepare a whack-a-mole for DateTime objects, which have a broken
    # TO_JSON implementation by default.
    my $date_flattening_visitor = Data::Visitor::Callback->new(
        'DateTime' => sub { sprintf('%sZ', $_->iso8601) }
    );

    my $properties_rs = $self->case_properties;
    my $properties = {};

    for my $attribute (@attributes) {
        unless ($attribute->grouping) {
            $self->log->warn(sprintf(
                'No case_property mapping for attribute "%s", skipping',
                $attribute->name
            ));

            next;
        }

        unless ($attribute->property_name) {
            throw('case/properties/update/attribute_invalid', sprintf(
                'Attribute "%s" does not declare a property name',
                $attribute->name
            ));
        }

        my $attr_set;

        if ($attribute->grouping eq 'case') {
            $attr_set = $properties->{ $attribute->property_name } //= [];
        } else {
            $attr_set = $properties->{ $attribute->grouping } //= [];
        }

        push @{ $attr_set }, $attribute;
    }

    my $case_id   = $self->id;
    my $case_uuid = $self->get_column('uuid');

    # I really don't understand why DBIx::Class makes using transactions
    # so 'cumbersome' by hiding the txn_* family of methods this deep in their
    # public APIs.
    $self->result_source->storage->txn_do(sub {
        my @keys = keys %{ $properties };

        # Prevent lock-step mutations on the set of properties we want update
        # This only selects rows that already exist, not ones that will be
        # created (which could still cause a deadlock).
        my @locked_property_ids = $properties_rs->search_rs(
            { name => \@keys, case_id => $self->id },
            { for => \'share' }
        )->get_column('id')->all;

        for my $property_name (@keys) {
            my ($first, @rest) = @{ $properties->{ $property_name } };

            my $ns = $first->name =~ m/^attribute\./ ? 'casetype' : 'case';

            my @attributes = map {
                $date_flattening_visitor->visit($_->TO_JSON)
            } ($first, @rest);

            # type is required for v1 / value, not value_v0, null indicates
            # 'idk' and should make it easy to filter later on.
            $properties_rs->new_result({
                name => $property_name,
                type => 'null',
                namespace => $ns,
                object_id => $case_uuid,
                case_id => $case_id,
                value_v0 => \@attributes
            })->upsert;
        }
    });
}

sub update_case_properties_for_documents {
    my ($self, $file_id) = shift;

    my @list = (
        Zaaksysteem::Attributes->get_predefined_case_attribute('case.case_documents'),
        Zaaksysteem::Attributes->get_predefined_case_attribute('case.documents'),
    );

    $_->init_system_value($self) for @list;

    if ($file_id) {
        $self->log->trace("looking for file $file_id");
        my $rs = $self->result_source->schema->resultset('FileCaseDocument');
        my $cd = $rs->search_rs(
            {
                'file.id' => $file_id,
            },
            {
                join => 'file',
            },
        )->first;

        if ($cd) {
            my $rs = $self->result_source->schema->resultset('File');
            $rs = $rs->search_rs(
                {
                    case_id => $self->id,
                    'file_case_document.file_id' => $cd->id,
                },
                {
                    join => [qw(file_id)]
                },
            );

            $self->log->trace(dump_terse([$rs->all]));
        }

    }
    $self->update_case_properties(@list);
}

=head2 _update_object_acl

This method will update the L<Zaaksysteem::Schema::ObjectAclEntry>s associated
with the case instance. It does a destructive replace of all ACL rules with
freshly generated rules.

=cut

# XXX TODO
# That said, this thing can be performance buzzkill in the long run, we can
# update more efficiently by just saving the diff to the rules (hell, lets
# start by not updating if nothing's changed)

sub update_object_acl {
    my $self = shift;

    my @acl_args;
    my $uuid = $self->get_column('uuid');

    my @existing_acls = $self->object_acl_entries->all;
    my $rs = $self->result_source->schema->resultset('ObjectAclEntry');

    # Iterator over zaak_authorisations
    my $acl_rs = $self->zaak_authorisations->search_rs;
    while (my $custom_acl = $acl_rs->next) {
        push @acl_args, {
            object_uuid => $uuid,
            entity_type => $custom_acl->entity_type,
            entity_id => $custom_acl->entity_id,
            capability => $custom_acl->capability,
            scope => $custom_acl->scope,
        }
    }

    # Iterator over aanvrager, coordinator and behandelaar objects
    # Skip unless defined, or skip when btype != medewerker
    # Store in hashmap indexed on betrokkene_identifier, so we don't
    # generate double entries
    my %subjects = map { $_->betrokkene_identifier => $_ } grep {
        defined && $_->btype eq 'medewerker'
    } (
        $self->aanvrager_object,
        $self->coordinator_object,
        $self->behandelaar_object
    );


    # Iterator over involved subjects, creating appropriate
    # ACL entries along the way.
    for my $security_id (keys %subjects) {
        my %sec_id = $subjects{ $security_id }->security_identity;

        for my $type (keys %sec_id) {
            push @acl_args, {
                object_uuid => $uuid,
                entity_type => $type,
                entity_id => $sec_id{ $type },
                capability => 'read',
                scope => 'instance'
            }, {
                object_uuid => $uuid,
                entity_type => $type,
                entity_id => $sec_id{ $type },
                capability => 'write',
                scope => 'instance'
            };
        }
    }

    # Comperator for in-place ACL updates
    my $cmp = sub {
        my ($a, $b) = @_;

        return if grep { defined $b->{ $_ } && $a->{ $_ } ne $b->{ $_ } } keys %{ $a };
        return 1;
    };

    my @new_acl_args;
    my @delete_uuids;

    # Compare new to old, if no existing record matches, it's a new ACL
    ARGS: for my $args (@acl_args) {
        for my $existing_acl (@existing_acls) {
            if ($cmp->($args, { $existing_acl->get_columns })) {
                next ARGS;
            }
        }

        push @new_acl_args, $args;
    }

    # Compare old to new, if no new records match, it's a deletion
    EXISTING: for my $existing_acl (@existing_acls) {
        for my $args (@acl_args) {
            if ($cmp->($args, { $existing_acl->get_columns })) {
                next EXISTING;
            }
        }

        push @delete_uuids, $existing_acl->uuid;
    }

    if (scalar @delete_uuids) {
        $rs->search({ uuid => { -in => \@delete_uuids } })->delete;
    }

    if (scalar @new_acl_args) {
        $rs->populate(\@new_acl_args);
    }

    return;
}

=head2 update_object_relationships

This method will update all L<Zaaksysteem::Schema::ObjectRelationships>
associated with the case instance. It does a destructive replace of all
existing relations, creating a fresh set from the currently available case
state data.

=cut

sub update_object_relationships {
    my $self = shift;

    my $schema = $self->result_source->schema;
    my $rs = $schema->resultset('ObjectRelationships');

    my $t0 = Zaaksysteem::StatsD->statsd->start;

    my $uuid = $self->get_column('uuid');

    my @relations_args;
    my $parent = $self->pid;
    if($parent && !$parent->is_deleted) {
        my $parent_uuid = $parent->get_column('uuid');
        push @relations_args, {
            owner_object_uuid => $uuid,

            object1_uuid => $parent_uuid,
            object1_type => 'case',
            type1        => 'parent',

            object2_uuid => $uuid,
            object2_type => 'case',
            type2        => 'child',
        };
    }

    my $children = $self->zaak_children->search_rs(
        {
            -and => {
                -or => [
                    { 'me.deleted' => undef },
                    { 'me.deleted' => { '<=' => \'NOW()' } }
                ]
            }
        },
    );

    while (my $child = $children->next) {
        my $child_uuid = $child->get_column('uuid');
        push @relations_args, {
            object1_uuid => $uuid,
            object1_type => 'case',
            type1        => 'parent',

            owner_object_uuid => $child_uuid,

            object2_uuid => $child_uuid,
            object2_type => 'case',
            type2        => 'child'
        };
    }

    # Other case relationships
    my $case_relations = $schema->resultset('CaseRelation')->search(
        { case_id    => $self->id },
    );
    while (my $cr = $case_relations->next) {
        if ($cr->get_column('case_id_a') == $self->id) {
            # We're A
            my $b_object = $cr->case_id_b;
            next if not $b_object;
            my $b_uuid = $b_object->get_column('uuid');

            push @relations_args, {
                object1_uuid => $uuid,
                object1_type => 'case',
                type1        => $cr->type_a,

                object2_uuid => $b_uuid,
                object2_type => 'case',
                type2        => $cr->type_b,
            };
        }
        else {
            # We're B
            my $a_object = $cr->case_id_a;
            next if not $a_object;
            my $a_uuid = $a_object->get_column('uuid');

            push @relations_args, {
                object1_uuid => $a_uuid,
                object1_type => 'case',
                type1        => $cr->type_a,

                object2_uuid => $uuid,
                object2_type => 'case',
                type2        => $cr->type_b
            };
        }
    }

    # Relation hash comperator for in-place updates
    my $cmp = sub {
        my ($a, $b) = @_;

        return if grep { defined $b->{ $_ } && $a->{ $_ } ne $b->{ $_ } } keys %{ $a };
        return 1;
    };

    my @existing_relations = $rs->search(
        {
            -or => [
                { object1_uuid => $uuid, object2_type => 'case' },
                { object2_uuid => $uuid, object1_type => 'case' }
            ]
        },
    )->all;

    my @new_relations_args;
    # Compare new to old, if no existing record matches, it's a new relation
    ARGS: for my $args (@relations_args) {
        for my $existing_relation (@existing_relations) {
            if ($cmp->($args, { $existing_relation->get_columns })) {
                next ARGS;
            }
        }
        push @new_relations_args, $args;
    }


    my @delete_uuids;
    # Compare old to new, if no existing record matches, it's a deletion
    EXISTING: for my $existing_relation (@existing_relations) {
        for my $args (@relations_args) {
            if ($cmp->($args, { $existing_relation->get_columns })) {
                next EXISTING;
            }
        }

        push @delete_uuids, $existing_relation->uuid;
    }

    if (scalar @delete_uuids) {
        $rs->search({ uuid => { -in => \@delete_uuids } })->delete;
    }

    if (scalar @new_relations_args) {
        $rs->populate(\@new_relations_args);
    }

    Zaaksysteem::StatsD->statsd->end('case_touch_relationship_update.time', $t0);

    return;

}

sub get_object_relation {
    my ($self, $object, $name) = @_;
    my $relation
        = $object->object_relation_object_ids->search_rs({ name => $name })
        ->first;

    return $relation if defined $relation;
    return $self->_update_object_relation($object, $name);

}

sub _update_object_relation {
    my ($self, $object, $name) = @_;

    $self->log->trace(sprintf(
        'Checking object relation "%s" for case %d',
        $name,
        $self->id
    ));

    if ($name eq 'route') {
        return $self->_rewrite_object_route($object);
    }

    if ($name eq 'casetype') {
        return $self->_rewrite_object_casetype($object);
    }

    if ($name eq 'outcome') {
        return $self->_rewrite_object_outcome($object);
    }

    if ($name eq 'milestone') {
        return $self->_rewrite_object_milestone($object);
    }

    if (any { $_ eq $name } qw[assignee requestor coordinator]) {
        return $self->_rewrite_object_subject($object, $name);
    }
}

sub _rewrite_object_milestone {
    my $self = shift;
    my $object = shift;

    my $milestone = $self->case_milestone;
    return unless $milestone;

    $self->log->info(sprintf(
        '(Re)generating milestone "%s" relation embedding for case %s',
        $milestone->milestone_label,
        $self->id
    ));

    return $object->object_relation_object_ids->create({
        name => 'milestone',
        object_type => 'case/milestone',
        object_preview => $milestone->TO_STRING,
        object_embedding => $milestone
    });
}

sub _rewrite_object_outcome {
    my $self = shift;
    my $object = shift;

    my $result = $self->case_outcome;
    return unless $result;

    $self->log->info(sprintf(
        '(Re)generating outcome "%s" relation embedding for case %s',
        $result->name,
        $self->id
    ));

    return $object->object_relation_object_ids->create({
        name => 'outcome',
        object_type => 'case/result',
        object_preview => $result->TO_STRING,
        object_embedding => $result
    });
}

sub _build_subject {
    my $self = shift;
    my $betrokkene = shift;
    my $type = $betrokkene->betrokkene_type;
    my $schema = $betrokkene->result_source->schema;

    my %map = (
        natuurlijk_persoon => 'NatuurlijkPersoon',
        bedrijf => 'Bedrijf',
        medewerker => 'Subject'
    );

    return unless exists $map{ $type };

    return $schema->resultset($map{ $type })->find(
        $betrokkene->gegevens_magazijn_id
    )->as_object;
}

sub _rewrite_object_subject {
    my $self = shift;
    my $object = shift;
    my $name = shift;

    my %map = (
        assignee => sub { $self->behandelaar },
        requestor => sub { $self->aanvrager },
        coordinator => sub { $self->coordinator },
    );

    return unless exists $map{ $name };

    my $gm_subject = $map{ $name }->();

    return unless $gm_subject;

    my $subject = $self->_build_subject($gm_subject);

    $self->log->info(sprintf(
        '(Re)generating %s relation "%s" embedding for case %s',
        $name,
        $subject->display_name,
        $self->id
    ));

    return $object->object_relation_object_ids->create({
        name => $name,
        object_type => 'subject',
        object_preview => $subject->TO_STRING,
        object_embedding => $subject
    });
}

sub _rewrite_object_casetype {
    my $self = shift;
    my $object = shift;

    $self->log->info(sprintf(
        '(Re)generating casetype "%s" embedding for case %s',
        $self->zaaktype_id->title,
        $self->id
    ));

    my $casetype = $self->zaaktype_id->build_casetype_object(
        zaaktype_node_id => $self->get_column('zaaktype_node_id')
    );

    return $object->object_relation_object_ids->create({
        name => 'casetype',
        object_type => 'casetype',
        object_preview => $casetype->TO_STRING,
        object_embedding => $casetype,
    });
}

sub _rewrite_object_route {
    my $self = shift;
    my $object = shift;

    $self->log->info(sprintf(
        '(Re)generating route embedding for case %s',
        $self->id
    ));

    # Schema-dependency because zaak.route_{ou,role} have no foreign key
    # constraints
    my $schema = $self->result_source->schema;

    my $group = $schema->resultset('Groups')->find($self->route_ou);
    my $role = $schema->resultset('Roles')->find($self->route_role);

    my %args;

    $args{ group } = $group->object if $group;
    $args{ role } = $role->object if $role;

    my $route = Zaaksysteem::Object::Types::Case::Route->new(%args);

    return $object->object_relation_object_ids->create({
        name => 'route',
        object_type => 'case/route',
        object_preview => $route->TO_STRING,
        object_embedding => $route
    });
}

sub _update_case_subject {
    my $self = shift;

    return unless $self->refresh_onderwerp;
    my @list = (
        Zaaksysteem::Attributes->get_predefined_case_attribute('case.subject'),
        Zaaksysteem::Attributes->get_predefined_case_attribute('case.subject_external'),
    );
    $_->init_system_value($self) for @list;
    $self->_update_case_properties(@list);

    return;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
