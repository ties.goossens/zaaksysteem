// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import assign from 'lodash/assign';
import find from 'lodash/find';
import first from 'lodash/first';
import get from 'lodash/get';
import includes from 'lodash/includes';
import isArray from 'lodash/isArray';
import isString from 'lodash/isString';
import merge from 'lodash/merge';
import sortByOrder from 'lodash/orderBy';
import uniq from 'lodash/uniq';
import seamlessImmutable from 'seamless-immutable';
import shortid from 'shortid';
import getAttributes from '../../getAttributes';
import { findTopic, getTitle } from '../../getFormattedTitle';
import scopedReducerFactory from '../../../../shared/api/resource/composedReducer/scopedReducerFactory';

/**
 * Controller for the proposalItemList component.
 * Status: Decoupled and superficially refactored.
 * ZS-TODO: split constructor into instance methods
 */
export default class ProposalItemListController {
  static get $inject() {
    return [
      '$scope',
      '$sce',
      '$location',
      '$stateParams',
      'dateFilter',
      '$state',
      'rwdService',
      'appService',
      'auxiliaryRouteService',
    ];
  }

  constructor(
    $scope,
    $sce,
    $location,
    $stateParams,
    dateFilter,
    $state,
    rwdService,
    appService,
    auxiliaryRouteService
  ) {
    const ctrl = this;
    const scopedReducer = scopedReducerFactory($scope);
    let sort = {};

    const appConfigReducer = scopedReducer(ctrl.appConfig).reduce(
      (config) => config
    );

    // This reducer outputs the relevant attributes that
    // are displayed in the proposals list column header.
    const attrReducer = scopedReducer(appConfigReducer, $state).reduce(
      (config, state) => {
        let attributes = getAttributes(config).voorstel || [];
        let docCol = {
          external_name: 'voorstelbijlagen',
          internal_name: {
            searchable_object_id: '$proposalAttachments',
            searchable_object_label: 'Bijlagen',
          },
          checked: 1,
        };

        if (state.current.name === 'meetingList') {
          return [
            {
              external_name: 'voorstelnummer',
              internal_name: {
                searchable_object_id: '$index',
                searchable_object_label: '',
              },
              checked: 1,
            },
          ]
            .concat(attributes)
            .concat(docCol);
        }

        return attributes.concat(docCol);
      }
    );

    // Outputs an object that contains the key in a
    // proposal object that needs to be sorted on + the order.
    const sortReducer = scopedReducer(() => sort, attrReducer).reduce(
      (userSort, attributes) => {
        let finalSort;
        let sortKey;

        if (userSort.key) {
          finalSort = userSort;
        } else {
          const firstCol = find(
            attributes,
            (attr) => attr.external_name.indexOf('voorstel') === 0
          );
          const key = firstCol.internal_name.searchable_object_id;

          finalSort = seamlessImmutable({
            key,
            reversed: false,
          });
        }

        switch (finalSort.key) {
          case 'zaaknummer':
            sortKey = 'instance.number';
            break;
          case 'zaaktype':
            sortKey = 'instance.casetype.instance.name';
            break;
          case 'vertrouwelijkheid':
            sortKey = 'instance.confidentiality';
            break;
          case 'resultaat':
            sortKey = 'instance.outcome.instance.result';
            break;
          default:
            sortKey = `instance.attributes.${finalSort.key}`;
        }

        finalSort = finalSort.merge({
          resolve: sortKey,
        });

        return finalSort;
      }
    );

    // Outputs the columns in the table header.
    const columnReducer = scopedReducer(attrReducer, sortReducer).reduce(
      (attributes, sortOptions) =>
        seamlessImmutable(attributes)
          .filter(
            (n) => n.external_name.indexOf('voorstel') === 0 && n.checked === 1
          )
          .map((attr) => {
            let isSortedOn =
              sortOptions.key ===
              get(attr, 'internal_name.searchable_object_id');
            let isSortReversed = isSortedOn && sortOptions.reversed;
            let colName = attr.external_name;
            let label =
              attr.internal_name.searchable_object_label_public ||
              attr.internal_name.searchable_object_label;
            let wideCol = false;
            let mediumCol = false;
            let smallCol = false;
            let colIcon;

            switch (colName) {
              case 'voorstelnummer':
                label = '';
                smallCol = true;
                break;
              case 'voorstelnotitie':
                label = '';
                smallCol = true;
                break;
              case 'voorstelzaaknummer':
                label = '#';
                mediumCol = true;
                break;
              case 'voorstelonderwerp':
                wideCol = true;
                break;
              case 'voorstelaanvrager':
                label = 'Aanvrager naam';
                break;
              case 'voorstelbijlagen':
                colIcon = 'file-document';
                label = '';
                smallCol = true;
                break;
            }

            return attr.merge({
              icon: isSortReversed
                ? 'chevron-up'
                : isSortedOn
                ? 'chevron-down'
                : '',
              classes: {
                'sorted-on': isSortedOn,
                'sort-reversed': isSortReversed,
              },
              colClass: {
                wide: wideCol,
                medium: mediumCol,
                small: smallCol,
              },
              label,
              colIcon,
            });
          })
    );

    ctrl.getConfigColumns = columnReducer.data;

    // In this reducer we calculate the number of
    // relevant attachments that are present on a case.
    const attachmentsReducer = scopedReducer(
      ctrl.proposals,
      appConfigReducer
    ).reduce((proposals, config) => {
      // First we get the relevant attachment attributes
      // that have been configured in the
      // currently active koppelprofiel.
      const attachmentAttributes = config.instance.interface_config.attribute_mapping
        .filter(
          (attribute) =>
            attribute.external_name.indexOf('voorstelbijlage') !== -1 &&
            attribute.internal_name
        )
        .map((attribute) => attribute.internal_name.searchable_object_id);

      // Then we map over all proposals,
      // calculate how many values are present
      // for those attributes in every proposal
      return proposals.map((proposal) => {
        const files = attachmentAttributes
          .map((label) => {
            return proposal.instance.attributes[label]
              ? proposal.instance.attributes[label]
              : [];
          })
          .reduce((acc, val) => acc.concat(val), []);

        // And return the original proposal object with an
        // extra key '$proposalAttachments' stuck on.
        // This allows easier displaying of values in
        // the fieldReducer and allows us to make use of
        // the sorting logic we already have for attribute values.
        return merge({}, proposal, {
          instance: {
            attributes: {
              $proposalAttachments: uniq(files).length,
            },
          },
        });
      });
    });

    // We use this reducer to sort proposals based on
    // a sort object that consists of a key and an order
    const sortedProposalReducer = scopedReducer(
      attachmentsReducer,
      ctrl.getConfigColumns,
      sortReducer,
      () => appService.state().grouped,
      $stateParams
    ).reduce((proposals, columns, sortOptions, isGrouped, stateParams) => {
      let sortedProposals;
      let state =
        stateParams.meetingType === 'open' ? ['open', 'new'] : ['resolved'];

      if (!isGrouped) {
        sortedProposals = proposals.filter((proposal) => {
          return includes(state, proposal.instance.status) ? proposal : null;
        });
      } else {
        sortedProposals = proposals;
      }

      if (sortOptions && sortOptions.key) {
        sortedProposals = seamlessImmutable(
          sortByOrder(
            sortedProposals,
            (proposal) => get(proposal, sortOptions.resolve),
            sortOptions.reversed ? 'desc' : 'asc'
          )
        );
      }

      return sortedProposals;
    });

    // We use this reducer to build the proposals
    // list that is displayed on bigger viewports.
    const proposalReducer = scopedReducer(
      sortedProposalReducer,
      ctrl.getConfigColumns
    ).reduce((sortedProposals, columns) => {
      let mutableCols = columns.asMutable();
      let formattedProposals;

      formattedProposals = sortedProposals.asMutable().map((proposal) =>
        assign(
          {
            $id: shortid(),
            id: proposal.reference,
            casenumber: proposal.instance.number,
            casetype: proposal.instance.casetype.instance.name,
            classes: {
              closed: !!(proposal.instance.status === 'resolved'),
            },
          },
          {
            values: mutableCols.map((column) => {
              let columnName = column.internal_name.searchable_object_id;
              let value = isArray(proposal.instance.attributes[columnName])
                ? first(proposal.instance.attributes[columnName])
                : proposal.instance.attributes[columnName];
              let tpl;

              if (isArray(value)) {
                if (value.length > 1) {
                  tpl = '<ul>';

                  value.map((el) => {
                    tpl += `<li>${el}</li>`;
                  });

                  tpl += '</ul>';
                } else {
                  tpl = `${value.join()}`;
                }
              } else if (isString(value) && value.split(' ').length > 20) {
                let resultArray = value.split(' ');

                if (resultArray.length > 20) {
                  resultArray = resultArray.slice(0, 20);
                  tpl = `${resultArray.join(' ')}...`;
                }
              } else {
                tpl = String(value || '-');
              }

              switch (columnName) {
                case '$index':
                  tpl = `<span class="proposal__number">${value}</span>`;
                  value = Number(value);
                  break;
                case 'zaaknummer':
                  tpl = String(proposal.instance.number || '');
                  break;
                case 'zaaktype':
                  tpl = String(proposal.instance.casetype.instance.name || '-');
                  break;
                case 'vertrouwelijkheid':
                  tpl = String(proposal.instance.confidentiality.mapped || '-');
                  break;
                case 'aanvrager_naam':
                  tpl = String(
                    `${proposal.instance.requestor.instance.subject.instance.first_names} ${proposal.instance.requestor.instance.subject.instance.surname}` ||
                      '-'
                  );
                  break;
                case 'resultaat':
                  tpl = String(proposal.instance.result || '-');
                  break;
                case '$proposalAttachments':
                  value = proposal.instance.attributes.$proposalAttachments;
                  tpl = String(value);
                  break;
                case 'proposalNote':
                  tpl = proposal.notes
                    ? '<i class="mdi mdi-note-plus"></i>'
                    : '';
                  break;
              }

              if (column.attribute_type === 'file') {
                tpl = `<a href="/download/${value.id}">Download</a>`;
              }

              if (includes(column.external_name, 'datum')) {
                tpl = String(dateFilter(value, 'dd MMM yyyy') || '-');
              }

              return {
                id: shortid(),
                name: columnName,
                template: $sce.trustAsHtml(tpl),
                value,
              };
            }),
          }
        )
      );

      return formattedProposals;
    });

    ctrl.getProposals = proposalReducer.data;

    // For smaller viewports
    const smallProposalReducer = scopedReducer(
      sortedProposalReducer,
      ctrl.getConfigColumns
    ).reduce((proposals, columns) =>
      proposals.map((proposal) => {
        const {
          instance: { attributes, casetype, number, status },
          notes,
          reference,
        } = proposal;
        const topic = findTopic(columns);
        let value = getTitle({
          attributes,
          casetype,
          topic,
        });

        if (isArray(value)) {
          value = first(value);
        }

        if (value.split(' ').length > 20) {
          let resultArray = value.split(' ');

          if (resultArray.length > 20) {
            resultArray = resultArray.slice(0, 20);
            value = `${resultArray.join(' ')}...`;
          }
        }

        return {
          $id: shortid(),
          id: reference,
          casenumber: number,
          subject: `${number}: ${value}`,
          agendapoint_number: attributes.$index,
          number_of_documents: attributes.$proposalAttachments,
          notes: notes || false,
          link: $state.href(
            //eslint-disable-next-line
            getDetailState(),
            {
              zaakID: number,
            }
          ),
          classes: {
            closed: !!(status === 'resolved'),
          },
        };
      })
    );

    ctrl.getSmallProposals = smallProposalReducer.data;

    // Function to sort. Modifies 'sort' variable
    // which in turn gets picked up by sortReducer.
    ctrl.sortByColumn = (column) => {
      let sortOptions = sortReducer.data();
      let key = sortOptions.key;
      let reversed = sortOptions.reversed;
      let colId = get(column, 'internal_name.searchable_object_id');

      if (sortOptions.key === colId) {
        reversed = !reversed;
      } else {
        reversed = false;
        key = colId;
      }

      sort = seamlessImmutable({
        key,
        reversed,
      });
    };

    let getDetailState = () =>
      auxiliaryRouteService.append($state.current, 'proposalDetail');

    ctrl.handleProposalClick = (proposal) => {
      $state.go(getDetailState(), {
        zaakID: proposal.casenumber,
      });
    };

    ctrl.getViewSize = () => {
      if (includes(rwdService.getActiveViews(), 'small-and-down')) {
        return 'small';
      }

      return 'wide';
    };

    ctrl.isGrouped = () => appService.state().grouped;
  }
}
