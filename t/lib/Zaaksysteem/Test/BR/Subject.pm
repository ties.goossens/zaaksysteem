package Zaaksysteem::Test::BR::Subject;
use Moose;
extends 'Zaaksysteem::Test::Moose';

use Zaaksysteem::Test;
use Zaaksysteem::BR::Subject;

use Zaaksysteem::Object::Types::Subject;
use Zaaksysteem::Object::Types::Company;
use Zaaksysteem::Object::Types::Address;
use Zaaksysteem::Object::Types::CountryCode;

use Data::Random::NL qw(generate_bsn generate_kvk);
use BTTW::Tools::RandomData qw(generate_uuid_v4);

sub get_bridge {

    my $s = Zaaksysteem::BR::Subject->new(
        schema => mock_dbix_schema,
        @_,
    );
    isa_ok($s, "Zaaksysteem::BR::Subject");
    return $s;
}

sub test_bridge_remote_nps {


    my $persons = [{ subject_type => 'person' }];

    my $mock_search_result = mock_strict(
        first => mock_strict(
            module_object => {
                get_natuurlijkpersoon_interface => {
                    process_trigger      => {
                        get_processor_params => sub {  return { result => $persons }},
                    }
                }
            }
        ),
    );

    my %rs_mapping;

    my $schema = mock_strict(
        resultset => sub {
            my $rs = shift;
            if (exists $rs_mapping{$rs}) {
                return $rs_mapping{$rs};
            }
            die "Did not define an rs_mapping for $rs";
        },
        txn_do => 1,
    );
    my %np = (
        id => 666,
        geboortedatum => DateTime->now(),
        indicatie_geheim => 1,
        datum_overlijden => DateTime->now(),
        adellijke_titel  => 1,
        correspondentieadres => 0,
        verblijfsadres => 0,
    );

    my $override
        = override(
        'Zaaksysteem::BR::Subject::Types::Person::_load_partner_from_row' =>
            sub { return {} });

    %rs_mapping = (
        Interface => mock_strict(
            search_active => $mock_search_result,
            find => 1,
        ),
        Logging => mock_strict(trigger => 1, process_trigger => 1),

        ContactData => mock_strict(
            search => { first => undef },
        ),

        NatuurlijkPersoon => mock_strict(
            search => {
                search_rs => { first => 1, next => 1 },
                first     => {
                    uuid => generate_uuid_v4,
                    subscription_id => undef,
                    as_object => 1,
                    result_source =>
                        { name => 'natuurlijk_persoon', schema => $schema },
                    _build_voorletters => 'W.G',
                    get_column => sub {
                        my $col = shift;
                        if (exists $np{$col}) {
                            return 1;
                        }
                        note "Did not define an np mapping for $col";
                        return;
                    },
                    %np,
                },
            },
            get_by_bsn => undef,
        ),
    );

    my $br = get_bridge(
        schema              => $schema,
        remote_search       => 'stufconfig',
        config_interface_id => 42,
    );

    my $bsn = generate_bsn(9);
    my @results;
    lives_ok(
        sub {
            @results = $br->search({
                subject_type              => 'person',
                'subject.personal_number' => $bsn,
            });
        }, "Search on NPS with '$bsn' works"
    );

    is(@results, 1, "Found one person");
    isa_ok($results[0], "Zaaksysteem::Object::Types::Subject");

    lives_ok(
        sub {
            $br->remote_import($results[0]);
        }, "Save works on NP works too",
    );

}

sub _get_fake_company {

    my $commerce_number = generate_kvk;
    my $branch_number   = sprintf("%012d", $commerce_number);

    my $address = Zaaksysteem::Object::Types::Address->new(
        city          => "Amsterdam",
        street        => "FooBarStreetz",
        street_number => 42,
        country =>
            Zaaksysteem::Object::Types::CountryCode->new_from_dutch_code(
            '6030')
    );

    my $company = Zaaksysteem::Object::Types::Company->new(
        coc_number          => $commerce_number,
        coc_location_number => $branch_number,
        company             => "Mintlab BV",
        address_residence   => $address,
    );

    my $subject = Zaaksysteem::Object::Types::Subject->new(
        subject_type => 'company',
        subject      => $company,
    );

    return $subject;
}


sub test_save_company {

    my @results;

    my %create_values;
    my %rs_mapping = (
        'Bedrijf' => mock_strict(
            search => {
                all => sub { return @results },
            },
            create          => sub {
                my $values = shift;
                %create_values = %$values;
                return mock_strict(
                    vestigingsnummer => undef,
                    update          => mock_strict,
                    discard_changes => 1,
                    id              => generate_bsn,
                    uuid            => generate_uuid_v4,
                    enable_bedrijf  => 1,
                )
            }
        ),
        ContactData => mock_strict(
            create => undef,
            search => { first => undef },
        ),
    );

    my $schema = mock_strict(
        resultset => sub {
            my $rs = shift;
            if (exists $rs_mapping{$rs}) {
                return $rs_mapping{$rs};
            }
            die "Did not define an rs_mapping for $rs";
        },
        txn_do => sub {
            my $coderef = shift;
            $coderef->();
        },
    );

    my $br = get_bridge(schema => $schema);

    my %update_values;
    my $subject = _get_fake_company();

    my $o = $br->save($subject);
    isa_ok($o, "Zaaksysteem::Object::Types::Subject");

    my %default = (
            # generated on demand, no need to check it
            dossiernummer    => ignore(),
            vestigingsnummer => ignore(),

            handelsnaam                    => 'Mintlab BV',
            rechtsvorm                     => undef,
            vestiging_adres_buitenland1    => undef,
            vestiging_adres_buitenland2    => undef,
            vestiging_adres_buitenland3    => undef,
            vestiging_huisletter           => undef,
            vestiging_huisnummer           => 42,
            vestiging_huisnummertoevoeging => undef,
            vestiging_landcode             => 6030,
            vestiging_postcode             => undef,
            vestiging_straatnaam           => 'FooBarStreetz',
            vestiging_woonplaats           => 'Amsterdam',

            vestiging_bag_id  => undef,
            vestiging_latlong => undef,

            oin                   => undef,
            rsin                  => undef,

            date_ceased           => undef,
            date_founded          => undef,
            date_registration     => undef,

            main_activity         => {},
            secondairy_activities => [],

            correspondentie_adres_buitenland1    => undef,
            correspondentie_adres_buitenland2    => undef,
            correspondentie_adres_buitenland3    => undef,
            correspondentie_huisletter           => undef,
            correspondentie_huisnummer           => undef,
            correspondentie_huisnummertoevoeging => undef,
            correspondentie_landcode             => 6030,
            correspondentie_postcode             => undef,
            correspondentie_straatnaam           => undef,
            correspondentie_woonplaats           => undef,
    );

    cmp_deeply(
        \%create_values,
        \%default,
        "Company created correctly",
    );

    @results = (
        mock_strict(
            vestigingsnummer => undef,
            update          => sub {
                my $values = shift;
                %update_values = %$values;
            },
            discard_changes => 1,
            id              => generate_bsn,
            uuid            => generate_uuid_v4,
            enable_bedrijf  => 1,
        )
    );

    $subject = _get_fake_company;
    $o = $br->save($subject);
    isa_ok($o, "Zaaksysteem::Object::Types::Subject");

    cmp_deeply(
        \%update_values,
        \%default,
        "Company updated correctly",
    );

    push(@results,
        mock_strict(vestigingsnummer => undef)
    );

    $subject = _get_fake_company;
    my $vestigingsnummer = $subject->subject->coc_location_number;

    @results = (
        mock_strict(vestigingsnummer => undef),
        mock_strict(
            vestigingsnummer => $vestigingsnummer,
            update           => sub {
                my $values = shift;
                %update_values = %$values;
            },
            discard_changes => 1,
            id              => generate_bsn,
            uuid            => generate_uuid_v4,
            enable_bedrijf  => 1,
        )
    );

    $o = $br->save($subject);
    cmp_deeply(
        \%update_values,
        {
            %default,
            vestigingsnummer => $vestigingsnummer,
        },
        "Company updated correctly",
    );


}

sub test_subject_utils {

    my $br = get_bridge();

    my $res = $br->map_search_params(
        { 'subject.coc_number' => 123456789},
        'company'
    );

    cmp_deeply(
        $res,
        { subject => { dossiernummer => 123456789 }, },
        "Expected outcome for chamber of commerce number"
    );

    $res = $br->map_search_params(
        {'subject.rsin' => "850220518"},
        'company'
    );

    cmp_deeply(
        $res,
        { subject => { rsin => 850220518 } },
        "Expected outcome for rsin"
    );
}


__PACKAGE__->meta->make_immutable;

__END__

=head1 NAME

Zaaksysteem::Test::BR::Subject - Test the ZS::BR::Subject code

=head1 SYNOPSIS

    prove -lv :: Zaaksysteem::Test::BR::Subject

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
