=head1 NAME

Zaaksysteem::Manual::API::V1::Document - Document retrieval and mutation API.

=head1 Description

This API-document describes the usage of our JSON Document API. Via the Document API it is possible
to retrieve, create and alter documents.


=head2 URL

The base URL for this API is:

    /api/v1/document

Make sure you use the HTTP Method C<GET> for retrieving, and C<POST> for change requests.

At this moment it is not possible to retreive documents by UUID, this is
due to how to Zaaksysteem deals with files. You can however retreive
documents based on their ID.

=head1 Retrieve data

=head2 get_by_number

   /api/v1/document/get_by_number/:id

Retreiving information about a document can be done via this call. You
will retreive information about a document via a L<Document
object|Zaaksysteem::Object::Types::Document> numberized in JSON if a
document could be found. If the document is I<reserved> you will get a
L<number object|Zaaksysteem::Object::Types::number> which corresponds to
the non-existing document. If a document cannot be found you will
receive a TODO..

B<Example call>

    curl \
        -H "API-Interface-Id: 42" \
        -H "Content-Type: application/json" \
        --digest -u "username:password" \
        https://localhost/api/v1/document/get_by_number/45

B<Request JSON>

An empty request body suffices

B<Response JSON>

=begin javascript

{
   "api_version" : 1,
   "development" : true,
   "request_id" : "dev-498425-029542",
   "result" : {
      "instance" : {
         "case" : null,
         "date_created" : "2018-02-09T10:02:12Z",
         "date_modified" : "2018-02-09T10:02:12Z",
         "file" : {
            "instance" : {
               "date_created" : "2018-02-09T09:56:29Z",
               "date_modified" : "2018-02-09T10:02:12Z",
               "md5" : "384cf868be6b7403c10f30696b766a99",
               "mimetype" : "text/html",
               "name" : "7fd",
               "size" : 256
            },
            "reference" : "90c6d903-54e0-45a6-9a17-29d5491b3189",
            "type" : "file"
         },
         "filename" : "yay.htm",
         "metadata" : {
            "instance" : {
               "appearance" : null,
               "category" : null,
               "date_created" : "2018-02-09T10:02:12Z",
               "date_modified" : "2018-02-09T10:02:12Z",
               "description" : null,
               "origin" : null,
               "origin_date" : null,
               "pronom_format" : "fmt/foo",
               "structure" : null,
               "trust_level" : "Zaakvertrouwelijk"
            },
            "reference" : null,
            "type" : "document/metadata"
         },
         "name" : "yay",
         "number" : 182,
         "version" : 11
      },
      "reference" : null,
      "type" : "document"
   },
   "status_code" : 200
}


=end javascript

=head1 Mutate data


=head2 reserve_number

   /api/v1/document/reserve_number

You can call this endpoint to reservce a document number

=begin javascript

{
   "api_version" : 1,
   "development" : true,
   "request_id" : "dev-498425-0f0845",
   "result" : {
      "instance" : {
         "date_created" : "2018-02-09T10:13:52Z",
         "date_modified" : "2018-02-09T10:13:52Z",
         "name" : "number",
         "object_class" : "document",
         "serial" : 206
      },
      "reference" : null,
      "type" : "serial"
   },
   "status_code" : 200
}

=end javascript

=head2 get_by_number/:id/upload

   /api/v1/document/get_by_number/:id/upload

This call allows you to upload a new file as for the given document. The response will be a L<Document object|Zaaksysteem::Types::Object::Document> as seen in the L<Zaaksysteem::Manual::API::V1::Document#get_by_number> call. In case you want to update the metadata of the document you will need to call the C<update> call.

=head2 get_by_number/:id/update

   /api/v1/document/get_by_number/:id/update

With this call you can update the metadata of a document. You can also
assign the file to a case by numeric value


B<Request JSON>

=begin javascript

{
   "number" : 42,
   "case_number" : 42,
   "metadata" : {
      "appearance" : "A text file with Windows line endings",,
      "category" : "Foo",
      "description" : "This file was send to someone",
      "origin" : "Intern",
      "origin_date" : "20170202",,
      "pronom_format" : "fmt/textfile",
      "structure" : "Text",
      "trust_level" : "Zaakvertrouwelijk",
      "status" : "original"
   }
}

=end javascript

B<Response JSON>

=begin javascript

{
   "api_version" : 1,
   "development" : true,
   "request_id" : "dev-498425-e4b486",
   "result" : {
      "instance" : {
         "case" : null,
         "date_created" : "2018-02-09T10:31:50Z",
         "date_modified" : "2018-02-09T10:31:50Z",
         "file" : {
            "instance" : {
               "date_created" : "2018-02-09T09:56:29Z",
               "date_modified" : "2018-02-09T10:31:50Z",
               "md5" : "384cf868be6b7403c10f30696b766a99",
               "mimetype" : "text/html",
               "name" : "7fd",
               "size" : 256
            },
            "reference" : "90c6d903-54e0-45a6-9a17-29d5491b3189",
            "type" : "file"
         },
         "filename" : "yay.htm",
         "metadata" : {
            "instance" : {
               "appearance" : "A text file with Windows line endings",
               "category" : "Foo",
               "date_created" : "2018-02-09T10:31:50Z",
               "date_modified" : "2018-02-09T10:31:50Z",
               "description" : "This file was send to someone",
               "origin" : "Intern",
               "origin_date" : "2017-02-02T00:00:00Z",
               "pronom_format" : "fmt/textfile",
               "structure" : "Text",
               "trust_level" : "Zaakvertrouwelijk"
            },
            "reference" : null,
            "type" : "document/metadata"
         },
         "name" : "yay",
         "number" : 42,
         "version" : 11
      },
      "reference" : null,
      "type" : "document"
   },
   "status_code" : 200
}

=end javascript

=head1 SEE ALSO

This document is based on the V1 API of Zaaksysteem, more information about the default format
of this API can be found in L<Zaaksysteem::Manual::API::V1>. Please make sure you read this
document before continuing.

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
