// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import vormTemplateServiceModule from './../../vorm/vormTemplateService';
import calendarModule from './../../vorm/types/calendar';
import vormObjectSuggestModule from './../../object/vormObjectSuggest';
import selectModule from './../../vorm/types/select';
import inputModule from './../../vorm/types/input';
import textareaModule from './../../vorm/types/textarea';
import richTextModule from './../../vorm/types/richText';
import radioModule from './../../vorm/types/radio';
import checkboxListModule from './../../vorm/types/checkboxList';
import fileModule from './../../vorm/types/file';
import mapModule from './../../vorm/types/map';
import zsBtwDisplayModule from './../../ui/zsBtwDisplay';
import each from 'lodash/each';

export default angular
  .module('caseAttrTemplateCompiler', [
    vormTemplateServiceModule,
    calendarModule,
    vormObjectSuggestModule,
    selectModule,
    inputModule,
    textareaModule,
    richTextModule,
    radioModule,
    checkboxListModule,
    fileModule,
    mapModule,
    zsBtwDisplayModule,
  ])
  .factory('caseAttrTemplateCompiler', [
    'vormTemplateService',
    (vormTemplateService) => {
      let compiler = vormTemplateService.clone(),
        templates,
        imgEl = angular.element(
          '<img class="delegate-value-display" ng-src="{{delegate.value}}" ng-show="delegate.value&&!delegate.isValid"/>'
        ),
        btwDisplay = `<zs-btw-display
            value="delegate.value"
            btw-type="vm.invokeData('btwType')"
            ng-if="vm.invokeData('btwType')">
          </zs-btw-display>`;

      templates = {
        image_from_url: {
          inherits: 'url',
          control: (el) => {
            el.append(imgEl.clone());

            return el;
          },
          display: (el) => {
            el.append(imgEl.clone());

            return el;
          },
        },
        case_file: {
          inherits: 'file',
          display: () => {
            return angular.element(
              `<div class="file-list-item">
                  <button type="button" ng-click="vm.templateData().click(delegate.value)" class="delegate-value-display file-list-item-button" ng-disabled="!delegate.value">
                    <div class="file-list-item-button-inner">

                      <zs-icon ng-if="delegate.value.confidential !== true" icon-type="file"></zs-icon>
                      <zs-icon ng-if="delegate.value.confidential === true" icon-type="eye-off" zs-tooltip="Bestand heeft een vertrouwelijkheidsaanduiding"></zs-icon>

                      {{delegate.value.filename}}
                    </div>
                  </button>
                  <zs-icon ng-if="delegate.value.is_archivable !== undefined && delegate.value.is_archivable === false" icon-type="alert-circle" zs-tooltip="Bestandstype is niet archiefwaardig" zs-tooltip-options="{ attachment: 'right middle', target: 'left middle', flip: true, offset: { x: -10, y: 0 } }"></zs-icon>
                  <a ng-href="{{vm.templateData().getDownloadUrl(delegate.value)}}" target="_blank" rel="noopener">
                    <zs-icon icon-type="download"></zs-icon>

                  </a>
                </div>`
            );
          },
        },
        valuta: {
          inherits: 'text',
          control: (el) =>
            angular.element(
              `<div class="vorm-input-wrapper" ng-class="{'not-empty': !vm.$empty}">
              ${el[0].outerHTML}
              ${btwDisplay}
            </div>`
            ),
          display: () =>
            angular.element(
              `<div class="delegate-value-display">
              <span>{{vm.toNumber(delegate.value) | currency}}</span>
              ${btwDisplay}
            </div>`
            ),
        },
      };

      each(templates, (value, key) => {
        compiler.registerType(key, value);
      });

      return compiler;
    },
  ]).name;
