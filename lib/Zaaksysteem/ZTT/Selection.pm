package Zaaksysteem::ZTT::Selection;

use Moose;

use Zaaksysteem::ZTT::MagicDirective;

=head1 NAME

Zaaksysteem::ZTT::Selection - Represent an abstract selection of text in a
template

=head1 ATTRIBUTES

=head2 tag

This attribute holds a reference to a L<Zaaksysteem::ZTT::Tag> object that was
detected in the selection.

=cut

has tag => (
    is => 'ro',
    isa => 'Zaaksysteem::ZTT::Tag',
    predicate => 'has_tag'
);

=head2 selection

This attribute holds a string representation of the complete selection.

=cut

has selection => (
    is => 'ro',
    isa => 'Str'
);

=head2 iterate

=cut

has iterate => (
    is => 'ro',
    isa => 'Str',
    predicate => 'is_iterable'
);

=head2 subtemplate

This attribute holds a flattened string representing the whole of a
subtemplate.

=cut

has subtemplate => (
    is => 'ro',
    isa => 'Str',
    predicate => 'has_subtemplate'
);

=head2 show_when

When set, this attribute should contain a valid ZTT expression. The expression
is used to determine if the show_when condition is met.

=cut

has show_when => (
    is => 'rw',
    isa => 'Defined',
    predicate => 'is_conditional'
);

=head2 name

This attribute may hold a string representing the name of the selection

=cut

has name => (
    is => 'ro',
    isa => 'Maybe[Str]'
);

=head1 METHODS

=head2 summary

Returns a comma seperated string of modifications this selection embeds

=cut

sub summary {
    my $self = shift;
    my $parser = Zaaksysteem::ZTT::MagicDirective->new;

    my @factoids;

    if ($self->is_iterable) {
        push @factoids, sprintf('iterate=%s', $self->iterate);
    }

    if ($self->is_conditional) {
        push @factoids, sprintf(
            'condition=\'%s\'',
            $parser->deparse($self->show_when->{ expression })
        );
    }

    if ($self->has_tag) {
        push @factoids, sprintf(
            'expression=\'%s\'',
            $parser->deparse($self->tag->expression)
        );

        if ($self->tag->formatter) {
            push @factoids, sprintf(
                'formatter=%s(%s)',
                $self->tag->formatter->{ name },
                join(', ', map { "$_" } @{ $self->tag->formatter->{ args } })
            );
        }
    }

    return join ', ', @factoids;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
