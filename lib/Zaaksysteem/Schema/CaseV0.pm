use utf8;
package Zaaksysteem::Schema::CaseV0;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Zaaksysteem::Schema::CaseV0

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<Zaaksysteem::Result>

=cut

use base 'Zaaksysteem::Result';
__PACKAGE__->table_class("DBIx::Class::ResultSource::View");

=head1 TABLE: C<case_v0>

=cut

__PACKAGE__->table("case_v0");
__PACKAGE__->result_source_instance->view_definition(" SELECT z.uuid AS id,\n    z.id AS object_id,\n    jsonb_build_object('class_uuid', zt.uuid, 'pending_changes', zm.pending_changes) AS \"case\",\n    (((((((((((((((jsonb_build_object('case.route_ou', z.route_ou, 'case.route_role', z.route_role, 'case.channel_of_contact', z.contactkanaal, 'case.html_email_template', z.html_email_template, 'case.current_deadline', zm.current_deadline, 'case.date_destruction', timestamp_to_perl_datetime((z.vernietigingsdatum)::timestamp with time zone), 'case.date_of_completion', timestamp_to_perl_datetime((z.afhandeldatum)::timestamp with time zone), 'case.date_of_registration', timestamp_to_perl_datetime((z.registratiedatum)::timestamp with time zone), 'case.date_target',\n        CASE\n            WHEN (z.status = 'stalled'::text) THEN 'Opgeschort'::text\n            ELSE timestamp_to_perl_datetime((z.streefafhandeldatum)::timestamp with time zone)\n        END, 'case.deadline_timeline', zm.deadline_timeline, 'case.stalled_since', timestamp_to_perl_datetime((zm.stalled_since)::timestamp with time zone), 'case.stalled_until', timestamp_to_perl_datetime((z.stalled_until)::timestamp with time zone), 'case.suspension_rationale', zm.opschorten, 'date_created', timestamp_to_perl_datetime((z.created)::timestamp with time zone), 'date_modified', timestamp_to_perl_datetime((z.last_modified)::timestamp with time zone), 'case.urgency', z.urgency, 'case.startdate', to_char(timezone('Europe/Amsterdam'::text, (z.registratiedatum)::timestamp with time zone), 'DD-MM-YYYY'::text), 'case.progress_days', get_date_progress_from_case(hstore(z.*)), 'case.progress_status', z.status_percentage, 'case.days_left', ((z.streefafhandeldatum)::date - COALESCE((z.afhandeldatum)::date, (now())::date)), 'case.lead_time_real', z.leadtime, 'case.destructable', is_destructable((z.vernietigingsdatum)::timestamp with time zone), 'case.number_status', z.milestone, 'case.milestone',\n        CASE\n            WHEN (zts_next.id IS NOT NULL) THEN zts.naam\n            ELSE NULL::text\n        END, 'case.phase',\n        CASE\n            WHEN (zts_next.id IS NOT NULL) THEN zts_next.fase\n            ELSE NULL::character varying\n        END, 'case.status', z.status, 'case.subject', z.onderwerp, 'case.subject_external', z.onderwerp_extern, 'case.archival_state', z.archival_state, 'case.confidentiality', get_confidential_mapping(z.confidentiality), 'case.payment_status', get_payment_status_mapping(z.payment_status), 'case.price', z.dutch_price, 'case.documents', COALESCE(( SELECT string_agg(concat(f.name, f.extension, ' (', f.document_status, ')'), ', '::text) AS string_agg\n           FROM file f\n          WHERE ((f.case_id = z.id) AND (f.date_deleted IS NULL) AND (f.active_version = true))), ''::text), 'case.case_documents', COALESCE(( SELECT string_agg(concat(case_documents.name, case_documents.extension, ' (', case_documents.document_status, ')'), ', '::text) AS string_agg\n           FROM (file case_documents\n             JOIN file_case_document fcd ON (((fcd.case_id = case_documents.case_id) AND (case_documents.id = fcd.file_id))))\n          WHERE (case_documents.case_id = z.id)), ''::text), 'case.num_unaccepted_updates', zm.unaccepted_attribute_update_count, 'case.num_unread_communication', zm.unread_communication_count, 'case.num_unaccepted_files', zm.unaccepted_files_count, 'case.aggregation_scope', 'Dossier') || jsonb_build_object('case.result', z.resultaat, 'case.result_description', ztr.label, 'case.result_explanation', ztr.comments, 'case.result_id', ztr.id, 'case.result_origin', ((ztr.properties)::jsonb -> 'herkomst'::text), 'case.result_process_term', ((ztr.properties)::jsonb -> 'procestermijn'::text), 'case.result_process_type_description', ((ztr.properties)::jsonb -> 'procestype_omschrijving'::text), 'case.result_process_type_explanation', ((ztr.properties)::jsonb -> 'procestype_toelichting'::text), 'case.result_process_type_generic', ((ztr.properties)::jsonb -> 'procestype_generiek'::text), 'case.result_process_type_name', ((ztr.properties)::jsonb -> 'procestype_naam'::text), 'case.result_process_type_number', ((ztr.properties)::jsonb -> 'procestype_nummer'::text), 'case.result_process_type_object', ((ztr.properties)::jsonb -> 'procestype_object'::text), 'case.result_selection_list_number', ((ztr.properties)::jsonb -> 'selectielijst_nummer'::text), 'case.retention_period_source_date', ztr.ingang, 'case.type_of_archiving', ztr.archiefnominatie, 'case.period_of_preservation', rpt.label, 'case.period_of_preservation_active',\n        CASE\n            WHEN (ztr.trigger_archival = true) THEN 'Ja'::text\n            ELSE 'Nee'::text\n        END, 'case.type_of_archiving', ztr.archiefnominatie, 'case.selection_list', ztr.selectielijst, 'case.active_selection_list', ztr.selectielijst)) || jsonb_build_object('case.lead_time_legal', ztd.afhandeltermijn, 'case.lead_time_service', ztd.servicenorm, 'case.principle_national', ztd.grondslag)) || jsonb_build_object('case.custom_number',\n        CASE\n            WHEN (char_length(z.prefix) > 0) THEN concat(z.prefix, '-', z.id)\n            ELSE (z.id)::text\n        END, 'case.number', z.id, 'case.number_master', z.number_master)) || jsonb_build_object('case.casetype', zt.uuid, 'case.casetype.id', zt.id, 'case.casetype.generic_category', bc.naam, 'case.casetype.initiator_type', ztd.handelingsinitiator, 'case.casetype.price.web', ztd.pdc_tarief, 'case.casetype.process_description', ztd.procesbeschrijving, 'case.casetype.publicity', ztd.openbaarheid, 'case.casetype.department', gr.name, 'case.casetype.route_role', ro.name)) || ztn.v0_json) || jsonb_build_object('case.parent_uuid', COALESCE((parent.uuid)::text, ''::text))) || jsonb_build_object('assignee', (z.assignee_v1_json ->> 'reference'::text), 'case.assignee', (z.assignee_v1_json ->> 'preview'::text), 'case.assignee.uuid', (z.assignee_v1_json ->> 'reference'::text), 'case.assignee.email', ((((z.assignee_v1_json -> 'instance'::text) -> 'subject'::text) -> 'instance'::text) ->> 'email_address'::text), 'case.assignee.initials', ((((z.assignee_v1_json -> 'instance'::text) -> 'subject'::text) -> 'instance'::text) ->> 'initials'::text), 'case.assignee.last_name', ((((z.assignee_v1_json -> 'instance'::text) -> 'subject'::text) -> 'instance'::text) ->> 'surname'::text), 'case.assignee.first_names', ((((z.assignee_v1_json -> 'instance'::text) -> 'subject'::text) -> 'instance'::text) ->> 'first_names'::text), 'case.assignee.phone_number', ((((z.assignee_v1_json -> 'instance'::text) -> 'subject'::text) -> 'instance'::text) ->> 'phone_number'::text), 'case.assignee.id', (split_part(((z.assignee_v1_json -> 'instance'::text) ->> 'old_subject_identifier'::text), '-'::text, 3))::integer, 'case.assignee.department', gassign.name, 'case.assignee.title', '')) || jsonb_build_object('coordinator', (z.coordinator_v1_json -> 'reference'::text), 'case.coordinator', (z.coordinator_v1_json -> 'preview'::text), 'case.coordinator.uuid', (z.coordinator_v1_json -> 'reference'::text), 'case.coordinator.email', ((((z.coordinator_v1_json -> 'instance'::text) -> 'subject'::text) -> 'instance'::text) ->> 'email_address'::text), 'case.coordinator.phone_number', ((((z.coordinator_v1_json -> 'instance'::text) -> 'subject'::text) -> 'instance'::text) ->> 'phone_number'::text), 'case.coordinator.id', (split_part(((z.coordinator_v1_json -> 'instance'::text) ->> 'old_subject_identifier'::text), '-'::text, 3))::integer, 'case.coordinator.title', '')) || case_subject_as_v0_json(hstore(requestor.*), 'requestor'::text, false)) || case_subject_as_v0_json(hstore(requestor.*), 'requestor'::text, true)) || jsonb_build_object('case.requestor.preset_client',\n        CASE\n            WHEN (z.preset_client = true) THEN 'Ja'::text\n            ELSE 'Nee'::text\n        END)) || case_subject_as_v0_json(hstore(recipient.*), 'recipient'::text, false)) || case_subject_as_v0_json(hstore(recipient.*), 'recipient'::text, true)) || case_location_as_v0_json(hstore(case_location.*))) || ( SELECT COALESCE(jsonb_object_agg(concat('attribute.', ca.magic_string), ca.value) FILTER (WHERE (ca.magic_string IS NOT NULL)), '{}'::jsonb) AS attributes\n           FROM case_attributes_v0 ca\n          WHERE (ca.case_id = z.id))) AS \"values\",\n    'case'::text AS object_type,\n    '{}'::text[] AS related_objects\n   FROM ((((((((((((((((((zaak z\n     JOIN zaak_meta zm ON ((z.id = zm.zaak_id)))\n     LEFT JOIN zaaktype zt ON ((z.zaaktype_id = zt.id)))\n     LEFT JOIN zaaktype_node ztn ON (((z.zaaktype_node_id = ztn.id) AND (zt.id = ztn.zaaktype_id))))\n     LEFT JOIN zaaktype_resultaten ztr ON ((z.resultaat_id = ztr.id)))\n     LEFT JOIN bibliotheek_categorie bc ON ((zt.bibliotheek_categorie_id = bc.id)))\n     LEFT JOIN zaaktype_definitie ztd ON ((ztn.zaaktype_definitie_id = ztd.id)))\n     LEFT JOIN groups gr ON ((z.route_ou = gr.id)))\n     LEFT JOIN subject assignee ON ((((z.assignee_v1_json ->> 'reference'::text))::uuid = assignee.uuid)))\n     LEFT JOIN groups gassign ON ((assignee.group_ids[1] = gassign.id)))\n     LEFT JOIN subject coordinator ON ((((z.coordinator_v1_json ->> 'reference'::text))::uuid = coordinator.uuid)))\n     LEFT JOIN roles ro ON ((z.route_role = ro.id)))\n     LEFT JOIN zaak parent ON ((z.pid = parent.id)))\n     LEFT JOIN zaak_betrokkenen requestor ON (((z.aanvrager = requestor.id) AND (z.id = requestor.zaak_id))))\n     LEFT JOIN zaak_betrokkenen recipient ON (((z.id = recipient.zaak_id) AND (recipient.rol = 'Ontvanger'::text))))\n     LEFT JOIN result_preservation_terms rpt ON ((ztr.bewaartermijn = rpt.code)))\n     LEFT JOIN zaak_bag case_location ON (((case_location.id = z.locatie_zaak) AND (z.id = case_location.zaak_id))))\n     LEFT JOIN zaaktype_status zts_next ON (((z.zaaktype_node_id = zts_next.zaaktype_node_id) AND (zts_next.status = (z.milestone + 1)))))\n     LEFT JOIN zaaktype_status zts ON (((z.zaaktype_node_id = zts.zaaktype_node_id) AND (zts.status = z.milestone))))");

=head1 ACCESSORS

=head2 id

  data_type: 'uuid'
  is_nullable: 1
  size: 16

=head2 object_id

  data_type: 'integer'
  is_nullable: 1

=head2 case

  data_type: 'jsonb'
  is_nullable: 1

=head2 values

  data_type: 'jsonb'
  is_nullable: 1

=head2 object_type

  data_type: 'text'
  is_nullable: 1

=head2 related_objects

  data_type: 'text[]'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "id",
  { data_type => "uuid", is_nullable => 1, size => 16 },
  "object_id",
  { data_type => "integer", is_nullable => 1 },
  "case",
  { data_type => "jsonb", is_nullable => 1 },
  "values",
  { data_type => "jsonb", is_nullable => 1 },
  "object_type",
  { data_type => "text", is_nullable => 1 },
  "related_objects",
  { data_type => "text[]", is_nullable => 1 },
);


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2021-09-28 14:34:38
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:Uw+lr4Cn+1S0FbHD77bUGg


# You can replace this text with custom code or comments, and it will be preserved on regeneration
require JSON::XS;


my @hash_fields = qw(
  case
  values
);

foreach (@hash_fields) {
  __PACKAGE__->inflate_column($_, {
    inflate => sub { JSON::XS->new->decode(shift // '{}') },
  });
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2021, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
