BEGIN;

  CREATE OR REPLACE FUNCTION update_unread_count() RETURNS void LANGUAGE plpgsql AS $$
  DECLARE
    r record;
  BEGIN
        FOR r IN
          SELECT DISTINCT
            id,
            case_id
          FROM thread
          WHERE unread_employee_count < 0
        LOOP
          INSERT INTO case_property (name, namespace, TYPE, value_v0, case_id, object_id)(
            SELECT
                'case.num_unread_communication',
                'case',
                'null',
                ('[{"name": "case.num_unread_communication", "value": ' || coalesce(sum(t.unread_employee_count), 0) || ', "human_label": "Ongelezen berichten", "human_value": "' || coalesce(sum(t.unread_employee_count), 0) || '", "attribute_type": "integer"}]')::JSONB,
                z.id,
                z.uuid
            FROM 
            zaak z
            LEFT JOIN thread t ON t.case_id = z.id 
            WHERE z.uuid IS NOT NULL and t.id= r.id
            GROUP BY
            z.id,
            z.uuid)
            ON CONFLICT (name, namespace, object_id, case_id)
            DO UPDATE SET value_v0 = excluded.value_v0;
            
            UPDATE
            object_data
            SET
                properties = jsonb_set(cast(properties AS jsonb), '{values,case.num_unread_communication}', ('{"name": "case.num_unread_communication", "value": ' || 0::varchar || ', "human_label": "Ongelezen berichten", "human_value": "' || 0::varchar || '", "attribute_type": "integer"}')::jsonb),
                index_hstore = index_hstore || hstore ('case.num_unread_communication',
                0::varchar)
            WHERE
            r.case_id = object_data.object_id
            AND object_data.object_class = 'case';

            update thread set unread_employee_count=0 where id = r.id;
        END LOOP;
      RETURN;
  END $$;
  SELECT update_unread_count();
  DROP FUNCTION update_unread_count;

COMMIT;




