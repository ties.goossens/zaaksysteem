// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import angularUiRouterModule from 'angular-ui-router';
import ocLazyLoadModule from 'oclazyload/dist/ocLazyLoad';
import assign from 'lodash/assign';
import find from 'lodash/find';
import first from 'lodash/head';
import get from 'lodash/get';
import isArray from 'lodash/isArray';
import seamlessImmutable from 'seamless-immutable';
import auxiliaryRouteModule from './../../../shared/util/route/auxiliaryRoute';
import composedReducerModule from './../../../shared/api/resource/composedReducer';
import observableStateParamsModule from './../../../shared/util/route/observableStateParams';
import onRouteActivateModule from './../../../shared/util/route/onRouteActivate';
import resourceModule from './../../../shared/api/resource';
import shouldReloadModule from './../../../shared/util/route/shouldReload';
import snackbarServiceModule from './../../../shared/ui/zsSnackbar/snackbarService';
import zsModalModule from './../../../shared/ui/zsModal';
import zsDisabledModule from './../../../shared/ui/zsDisabled';
import zsTrapKeyboardFocus from './../../../shared/ui/zsTrapKeyboardFocus';
import zsMoveFocusToModule from './../../../shared/ui/zsMoveFocusTo';
import assignToScope from './../../../shared/util/route/assignToScope';
import convertv0Case from './../../../shared/api/mock/convertv0Case';
import normalizePhaseName from './zsCaseView/zsCasePhaseView/normalizePhaseName';

import template from './template.html';

const ensureArray = (data) => (isArray(data) ? data : []);

export default angular
  .module('Zaaksysteem.intern.case.route', [
    angularUiRouterModule,
    auxiliaryRouteModule,
    composedReducerModule,
    observableStateParamsModule,
    ocLazyLoadModule,
    onRouteActivateModule,
    resourceModule,
    shouldReloadModule,
    snackbarServiceModule,
    zsModalModule,
    zsDisabledModule,
    zsMoveFocusToModule,
    zsTrapKeyboardFocus,
  ])
  .config([
    '$stateProvider',
    '$urlMatcherFactoryProvider',
    ($stateProvider, $urlMatcherFactoryProvider) => {
      $urlMatcherFactoryProvider.strictMode(false);

      const getLegacyErrorMessage = (distinct) =>
        [
          distinct,
          'Neem contact op met uw beheerder voor meer informatie',
        ].join(' ');

      $stateProvider
        .state('case', {
          url: '/zaak/{caseId}',
          resolve: {
            timeout: ['$timeout', ($timeout) => $timeout(angular.noop, 50)],
            caseId: [
              '$rootScope',
              '$state',
              '$stateParams',
              'resource',
              ($rootScope, $state, $stateParams, resource) => {
                // the param caseId can either be the caseNumber or the caseUuid
                // so we either return the caseNumber or call v2 to get the caseNumber
                if (isNaN(parseInt($stateParams.caseId, 10))) {
                  const caseIdResource = resource(
                    {
                      url: '/api/v2/cm/case/get_case',
                      params: {
                        case_uuid: $stateParams.caseId,
                      },
                    },
                    {
                      scope: $rootScope,
                    }
                  ).reduce((requestOptions, data) =>
                    get(data, 'data.attributes.number')
                  );

                  return caseIdResource.asPromise().then(() => {
                    const caseId = caseIdResource.data();

                    $state.go('case', { caseId }, { notify: false });

                    return caseId;
                  });
                } else {
                  return $stateParams.caseId;
                }
              },
            ],
            case: [
              '$rootScope',
              '$q',
              '$state',
              '$http',
              'snackbarService',
              'resource',
              'caseId',
              (
                $rootScope,
                $q,
                $state,
                $http,
                snackbarService,
                resource,
                caseId
              ) => {
                const caseResource = resource(
                  {
                    url: `/api/v0/case/${caseId}`,
                  },
                  {
                    scope: $rootScope,
                  }
                ).reduce((requestOptions, data) => {
                  const caseObj = first(data);

                  // reducers are called with error data,
                  // gotta check if it's an error first
                  if (!caseObj || !caseObj.id) {
                    return null;
                  }

                  return convertv0Case(caseObj);
                });

                return caseResource
                  .asPromise()
                  .then((response) => {
                    // a successful response can be gotten before the case registration is completed
                    // there are two stages in this (unfinished, finished)
                    // the unfinished stage is a static one, where 'instance' misses many properties
                    // 'number' is one of the missing properties
                    // so we use that to determine whether an error should be thrown
                    // in which case the caseNumber is extracted, so it can be used to provide a link to the case
                    const caseNumber = get(
                      caseResource.data(),
                      'instance.number'
                    );

                    if (!caseNumber) {
                      const extractFirstNumber = (str) =>
                        str.match(/^[^\d]*(\d+)/)[1];
                      const actionUrl = response.instance.actions[1][0].url;
                      const extractedCaseNumber = extractFirstNumber(actionUrl);

                      throw new Error(extractedCaseNumber);
                    }

                    $http({
                      url: `/api/v0/case/${caseNumber}/peruse`,
                      method: 'POST',
                    });

                    return caseResource;
                  })
                  .catch((response) => {
                    const type = response.data
                      ? get(first(response.data.result), 'type')
                      : 'case_creation_in_progress';
                    const messageTypes = {
                      'api/case/find_case':
                        'De zaak kon niet worden gevonden. Mogelijk is de zaak verwijderd.',
                      'api/case/authorization':
                        'U heeft niet voldoende rechten om deze zaak te bekijken.',
                      case_creation_in_progress:
                        'Het aanmaken van de zaak is nog niet voltooid.',
                      default: getLegacyErrorMessage(
                        'Er ging iets mis bij het laden van de zaak.'
                      ),
                    };
                    const labelTypes = {
                      case_creation_in_progress: 'Laad opnieuw',
                      default: 'Terug naar dashboard',
                    };
                    const linkTypes = {
                      case_creation_in_progress: `/intern/zaak/${response.message}/`,
                      default: $state.href('home'),
                    };

                    snackbarService.error(
                      messageTypes[type] || messageTypes['default'],
                      {
                        actions: [
                          {
                            type: 'link',
                            label: labelTypes[type] || labelTypes['default'],
                            link: linkTypes[type] || linkTypes['default'],
                          },
                        ],
                      }
                    );

                    return $q.reject(response);
                  });
              },
            ],
            casetype: [
              '$rootScope',
              '$q',
              '$state',
              'resource',
              'snackbarService',
              'case',
              (
                $rootScope,
                $q,
                $state,
                resource,
                snackbarService,
                caseResource
              ) => {
                const { reference } = caseResource.data().instance.casetype;
                let {
                  version,
                } = caseResource.data().instance.casetype.instance;
                let casetypeResource = resource(
                  {
                    url: `/api/v1/casetype/${reference}`,
                    params: {
                      version,
                    },
                  },
                  {
                    scope: $rootScope,
                  }
                ).reduce((requestOptions, data) => first(data));

                return casetypeResource
                  .asPromise()
                  .then(() => casetypeResource)
                  .catch((response) => {
                    snackbarService.error(
                      'Het zaaktype kan niet worden geladen.',
                      {
                        actions: [
                          {
                            type: 'link',
                            label: 'Terug naar dashboard',
                            link: $state.href('home'),
                          },
                        ],
                      }
                    );

                    return $q.reject(response);
                  });
              },
            ],
            jobs: [
              '$rootScope',
              'resource',
              'case',
              ($rootScope, resource, caseResource) => {
                const jobsResource = resource(
                  () => {
                    const reference = get(caseResource.data(), 'reference');

                    return reference
                      ? {
                          url: `/api/v1/case/${reference}/queue`,
                          params: {
                            paging: 100,
                          },
                        }
                      : null;
                  },
                  {
                    scope: $rootScope,
                  }
                ).reduce((requestOptions, data) =>
                  ensureArray(data).filter(
                    (job) =>
                      job.instance.status !== 'finished' &&
                      job.instance.status !== 'failed' &&
                      job.instance.status !== 'cancelled'
                  )
                );

                return jobsResource
                  .asPromise()
                  .then(() => jobsResource)
                  .catch(() => {
                    // not important enough to bug the user w/ a snack
                  });
              },
            ],
            rules: [
              '$rootScope',
              'resource',
              'caseId',
              ($rootScope, resource, caseId) => {
                const ruleResource = resource(
                  {
                    url: '/api/rules',
                    params: {
                      case_id: caseId,
                      zapi_no_pager: 1,
                    },
                  },
                  {
                    scope: $rootScope,
                  }
                );

                return ruleResource.asPromise().then(() => ruleResource);
              },
            ],
            relatedSubjectsResource: [
              '$rootScope',
              '$stateParams',
              'resource',
              'snackbarService',
              'case',
              (
                $rootScope,
                $stateParams,
                resource,
                snackbarService,
                caseResource
              ) => {
                const relatedSubjectsResource = resource(
                  {
                    url: '/api/v2/cm/case/get_subject_relations',
                    params: {
                      case_uuid: caseResource.data().reference,
                    },
                  },
                  { scope: $rootScope }
                );

                return relatedSubjectsResource
                  .asPromise()
                  .then(() => relatedSubjectsResource)
                  .catch(() => {
                    snackbarService.error(
                      getLegacyErrorMessage(
                        'De betrokkenen konden niet worden geladen.'
                      )
                    );
                    relatedSubjectsResource.destroy();
                  });
              },
            ],
            __SIDE_EFFECT__: [
              '$rootScope',
              '$ocLazyLoad',
              '$q',
              ($rootScope, $ocLazyLoad, $q) =>
                $q((resolve) => {
                  require(['./', './controller'], (...names) => {
                    $rootScope.$evalAsync(() => {
                      $ocLazyLoad.load(names.map((name) => ({ name })));
                      resolve();
                    });
                  });
                }),
            ],
          },
          template,
          controller: 'CaseController',
          controllerAs: 'vm',
          onActivate: [
            '$state',
            '$location',
            'auxiliaryRouteService',
            'case',
            ($state, $location, auxiliaryRouteService, caseResource) => {
              let names = auxiliaryRouteService.parseNames(
                $state.current.name,
                $location.url()
              );
              let state = auxiliaryRouteService.state(
                'case.phase',
                $state.current.name,
                $location.url()
              );
              let params = {
                phaseName: normalizePhaseName(
                  get(caseResource.data(), 'instance.phase') || ''
                ),
              };

              if (!params.phaseName) {
                params.phaseName = 'afhandelen';
              }

              if (names.base === 'case') {
                $state.go(state, params, { location: false });
              }
            },
          ],
          title: [
            'case',
            (caseResource) => {
              let caseObj = caseResource.data();
              let caseId = caseObj.instance.number;
              let casetypeName = caseObj.instance.casetype.instance.name;
              let subTitle = caseObj.instance.subject;
              let title = {
                mainTitle: `Zaak ${caseId}: ${casetypeName}`,
                subTitle,
              };

              return title;
            },
          ],
          settings: `<zs-case-settings 
               case-resource="viewController.getCaseResource()" 
               casetype-resource="viewController.getCasetypeResource()" 
               user="viewController.getUser()"
             ></zs-case-settings>`,
          actions: [
            {
              name: 'upload',
              label: 'Document uploaden',
              iconClass: 'upload',
              template: `<zs-case-file-upload
                  close="close($promise)"
                  case-uuid="viewController.getCaseUuid()"
                ></zs-case-file-upload>`,
              when: [
                'viewController',
                (viewController) => viewController.canUpload(),
              ],
            },
            {
              name: 'sjabloon',
              label: 'Sjabloon gebruiken',
              iconClass: 'file',
              template: `<zs-case-template-generate
                  on-close="close(viewController.createTemplate($values))"
                  case-id="viewController.getCaseNumber()"
                  data-templates="viewController.getCaseTemplates()"
                  case-documents="viewController.getCaseDocuments()"
                  data-requestor="viewController.getRequestor()"
                ></zs-case-template-generate>`,
              when: [
                'viewController',
                (viewController) =>
                  !viewController.isResolved() &&
                  viewController.canEdit() &&
                  get(viewController.getCaseTemplates(), 'length', 0) > 0,
              ],
            },
            {
              name: 'betrokkene',
              label: 'Betrokkene toevoegen',
              iconClass: 'account',
              template: `<zs-case-add-subject
                  on-subject-add="close($promise)"
                  case-id="viewController.getCaseNumber()"
                  case-uuid="viewController.getCaseUuid()"
                ></zs-case-add-subject>`,
              when: [
                'viewController',
                (viewController) =>
                  viewController.canEdit() &&
                  !viewController.isResolved() &&
                  !!viewController.getRequestor(),
              ],
            },
            {
              name: 'email',
              label: 'E-mail versturen',
              iconClass: 'email',
              template: `<zs-case-send-email
                  case-id="viewController.getCaseNumber()"
                  case-custom-html-template-name="viewController.getCaseCustomHtmlTemplateName()"
                  email-template-data="viewController.getEmailTemplateData()"
                  data-templates="viewController.getEmailTemplates()"
                  data-requestor="viewController.getRequestor()"
                  on-email-send="close($promise)"
                >
                </zs-case-send-email>`,
              when: [
                'viewController',
                (viewController) =>
                  viewController.canEdit() &&
                  !viewController.isResolved() &&
                  !!viewController.getRequestor(),
              ],
            },
            {
              name: 'geplande-zaak',
              label: 'Geplande zaak toevoegen',
              iconClass: 'calendar-plus',
              template: `<zs-case-plan
                  case-id="viewController.getCaseNumber()"
                  case-uuid="viewController.getCase().reference"
                  on-submit="close($promise)"
                ></zs-case-plan>`,
              when: [
                'viewController',
                (viewController) =>
                  viewController.canEdit() && !viewController.isResolved(),
              ],
            },
          ],
        })
        .state('case.phase', {
          url: '/fase/:phaseName/:tab/',
          template: `<zs-case-phase-view
              on-phase-advance="vm.handlePhaseAdvance($taskIds)"
              phase-name="phaseName"
              data-case="case"
              data-casetype="casetype"
              data-case-action-resource="vm.caseActionResource"
              data-case-actions="vm.getCaseActions()"
              data-case-actions-loading="vm.caseActionsLoading()"
              data-checklist-resource="vm.checklistResource"
              data-checklists="vm.getChecklists()"
              data-checklists-loading="vm.checklistsLoading()"
              data-mutation-resource="vm.mutationResource"
              data-mutations="vm.getMutations()"
              data-custom-object-resource="vm.customObjectResource"
              data-custom-objects="vm.getCustomObjects()"
              data-mutations-loading="vm.mutationsLoading()"
              data-rules="rules"
              data-tab="get('tab')"
              data-templates="vm.getCaseTemplates()"
              data-map-settings="vm.getMapSettings()"
              case-documents="vm.getCaseDocuments()"
              email-template-data="vm.getEmailTemplateData()"
              data-subject-relations="vm.getSubjectRelations()"
              data-requestor="vm.getRequestor()"
              can-advance="vm.canAdvance()"
              can-edit="vm.canEdit()&&!vm.isResolved()"
              is-resolved="vm.isResolved()"
              can-self-assign="!vm.isResolved()"
              data-user="vm.getUser()"
            ></zs-case-phase-view>`,
          title: [
            '$stateParams',
            'case',
            'casetype',
            ($stateParams, caseObj, casetype) => {
              const { phaseName } = $stateParams;
              const isCurrent =
                // eslint-disable-next-line quotes
                (get(caseObj, "values['case.phase']") || '').toLowerCase() ===
                phaseName;
              const phaseObj = find(
                // eslint-disable-next-line quotes
                get(casetype, "instance['phases']", []),
                (p) => p.name.toLowerCase() === phaseName
              );
              let label;

              if (phaseObj) {
                label = phaseObj.name;

                if (isCurrent) {
                  label += ' - Actieve fase';
                }
              }

              return label;
            },
          ],
          controller: assignToScope(['rules'], 'case', 'casetype', 'rules'),
          params: {
            tab: {
              squash: true,
              value: null,
            },
          },
          shouldReload: (current, currentParams, to, toParams) => {
            const sameName = get(current, 'name') === get(to, 'name');
            const samePhase =
              sameName &&
              get(currentParams, 'phaseName') === get(toParams, 'phaseName');

            return !samePhase;
          },
        })
        .state('case.docs', {
          url: '/documenten/:documentId/',
          title: 'Documenten',
          template: `<zs-case-document-view
              case="vm.getCase()"
              case-id="vm.getCaseNumber()"
              case-uuid="vm.getCaseUuid()"
              read-only="!vm.canEdit()||vm.isResolved()"
              can-upload="vm.canUpload()"
              can-delete="vm.canDelete()"
              on-file-accept="vm.reloadCaseResource()"
              on-file-reject="vm.reloadCaseResource()"
              on-file-update="vm.reloadCaseResource()"
              is-zoho-supported="vm.isZohoSupported()"
              is-ms-supported="vm.isMsSupported()"
              email-template-data="vm.getEmailTemplateData()"
            ></zs-case-document-view>`,
          params: {
            documentId: {
              squash: true,
              value: null,
            },
          },
          resolve: {
            __SIDE_EFFECT__: [
              '$rootScope',
              '$ocLazyLoad',
              '$q',
              ($rootScope, $ocLazyLoad, $q) =>
                $q((resolve) => {
                  require(['./zsCaseDocumentView'], (...names) => {
                    $rootScope.$evalAsync(() => {
                      $ocLazyLoad.load(names.map((name) => ({ name })));
                      resolve();
                    });
                  });
                }),
            ],
          },
        })
        .state('case.timeline', {
          url: '/timeline/:eventId/',
          title: 'Timeline',
          template: `<zs-case-timeline-view
              case-id="vm.getCaseNumber()"
              data-requestor="vm.getRequestor()"
              event-id="vm.getEventId()"
            ></zs-case-timeline-view>`,
          params: {
            eventId: {
              squash: true,
              value: null,
            },
          },
          resolve: {
            __SIDE_EFFECT__: [
              '$rootScope',
              '$ocLazyLoad',
              '$q',
              ($rootScope, $ocLazyLoad, $q) =>
                $q((resolve) => {
                  require(['./zsCaseTimelineView'], (...names) => {
                    $rootScope.$evalAsync(() => {
                      $ocLazyLoad.load(names.map((name) => ({ name })));
                      resolve();
                    });
                  });
                }),
            ],
          },
        })
        .state('case.communication', {
          url: '/communicatie/:action/:id',
          title: 'Communication',
          template: `<zs-case-communication-view
              case-uuid="vm.getCaseUuid()"
              thread-id="vm.getThreadId()"
            ></zs-case-communication-view>`,
          params: {
            action: {
              squash: true,
              value: null,
            },
            id: {
              squash: true,
              value: null,
            },
          },
          resolve: {
            __SIDE_EFFECT__: [
              '$rootScope',
              '$ocLazyLoad',
              '$q',
              ($rootScope, $ocLazyLoad, $q) =>
                $q((resolve) => {
                  require(['./zsCaseCommunicationView'], (...names) => {
                    $rootScope.$evalAsync(() => {
                      $ocLazyLoad.load(names.map((name) => ({ name })));
                      resolve();
                    });
                  });
                }),
            ],
          },
        })
        .state('case.location', {
          url: '/locatie/:more/',
          title: 'Kaart',
          params: {
            more: {
              squash: true,
              value: null,
            },
          },
          template: `<zs-case-map-view
               case-resource="vm.getCaseResource()"
               show-more="more()"
             ></zs-case-map-view>`,
          shouldReload: (current, currentParams, to) => {
            let sameName = get(current, 'name') === get(to, 'name');

            return !sameName;
          },
          controller: [
            '$scope',
            'observableStateParams',
            ($scope, observableStateParams) => {
              $scope.more = () => observableStateParams.get('more');
            },
          ],
        })
        .state('case.geojson', {
          url: '/locaties',
          title: 'Kaart',
          template: `<zs-geojson
            data-map-settings="vm.getMapSettings()"
            data-context="vm.getMapContext('CaseMap')"
            data-map-name="'case-location'"
            data-initial-feature="location"
            height="100%"
          ></zs-geojson>`,
          resolve: {
            location: [
              'case',
              (caseResource) => {
                const uuid = get(caseResource.data(), 'reference');

                return fetch(`/api/v2/geo/get_geo_features?uuid=${uuid}`)
                  .then((r) => r.json())
                  .then((res) => ({
                    type: 'FeatureCollection',
                    features: res.data.reduce((acc, collection) => {
                      return acc.concat(
                        get(collection, 'attributes.geo_json.features', [])
                      );
                    }, []),
                  }));
              },
            ],
          },
          controller: assignToScope('location'),
        })
        .state('case.relations', {
          url: '/relaties',
          title: 'Relaties',
          template: `<zs-case-relation-view
              case-uuid="vm.getCaseUuid()"
            ></zs-case-relation-view>`,
          resolve: {
            __SIDE_EFFECT__: [
              '$rootScope',
              '$ocLazyLoad',
              '$q',
              ($rootScope, $ocLazyLoad, $q) =>
                $q((resolve) => {
                  require(['./', './zsCaseRelationView'], (...names) => {
                    $rootScope.$evalAsync(() => {
                      $ocLazyLoad.load(names.map((name) => ({ name })));
                      resolve();
                    });
                  });
                }),
            ],
          },
        })
        .state('case.admin', {
          url: '/beheer/:action',
          auxiliary: true,
          resolve: {
            acls: [
              '$rootScope',
              '$q',
              'resource',
              '$stateParams',
              'case',
              ($rootScope, $q, resource, $stateParams, caseResource) => {
                const aclsResource = resource(
                  $stateParams.action === 'rechten'
                    ? {
                        url: `/api/v1/case/${
                          caseResource.data().reference
                        }/acl`,
                        params: {
                          rows_per_page: 100,
                        },
                      }
                    : null,
                  {
                    scope: $rootScope,
                    cache: {
                      disabled: true,
                    },
                  }
                ).reduce((requestOptions, data) => {
                  if (data) {
                    return seamlessImmutable(data.asMutable().reverse());
                  }

                  return data;
                });

                // immediately resolve to keep UI responsive (modal opens immediately)
                return $q.resolve(aclsResource);
              },
            ],
            actions: [
              '$rootScope',
              '$q',
              '$state',
              'composedReducer',
              'case',
              'casetype',
              (
                $rootScope,
                $q,
                $state,
                composedReducer,
                caseResource,
                casetypeResource
              ) =>
                $q((resolve) => {
                  require(['../../../shared/case/caseActions'], (actions) => {
                    $rootScope.$evalAsync(() => {
                      resolve(
                        composedReducer(
                          { scope: $rootScope },
                          caseResource,
                          casetypeResource
                        ).reduce((caseObj, casetype, acls) =>
                          actions({
                            caseObj,
                            casetype,
                            $state,
                            acls,
                          })
                        )
                      );
                    });
                  });
                }),
            ],
          },
          onExit: [
            'actions',
            (actions) => {
              actions.destroy();
            },
          ],
          onEnter: [
            '$rootScope',
            '$compile',
            '$state',
            'user',
            'case',
            'casetype',
            'acls',
            'actions',
            '$stateParams',
            'zsModal',
            (
              $rootScope,
              $compile,
              $state,
              user,
              caseResource,
              casetypeResource,
              aclsResource,
              actions,
              $stateParams,
              zsModal
            ) => {
              const action = find(actions.data(), {
                name: $stateParams.action,
              });
              let modal;
              let unregister;

              let closeModal = () => {
                modal.close();
                scope.$destroy();
                unregister();
              };

              const scope = $rootScope.$new(true);

              scope.action = action;
              scope.caseResource = caseResource;
              scope.casetypeResource = casetypeResource;
              scope.aclsResource = aclsResource;
              scope.user = user.data().instance.logged_in_user;

              scope.onSubmit = (promise, reload, willRedirect) => {
                modal.hide();

                promise
                  .then(() => {
                    modal.close();

                    if (!willRedirect) {
                      $state.go('^', null, { reload });
                    }
                  })
                  .catch(() => {
                    modal.show();
                  });
              };

              modal = zsModal({
                title: action.label,
                scope,
                el: $compile(`
                <zs-case-admin-view
                  data-action="action"
                  case-resource="caseResource"
                  data-user="user"
                  casetype-resource="casetypeResource"
                  acl-resource="aclsResource"
                  on-submit="onSubmit($promise, $reload, $willRedirect)"
                ></zs-case-admin-view>`)(scope),
              });

              modal.open();

              modal.onClose(() => {
                $state.go('^');

                return false;
              });

              unregister = $rootScope.$on('$stateChangeStart', closeModal);
            },
          ],
        })
        .state('case.info', {
          url: '/informatie',
          auxiliary: true,
          onEnter: [
            '$rootScope',
            '$compile',
            '$state',
            'case',
            'casetype',
            'zsModal',
            (
              $rootScope,
              $compile,
              $state,
              caseResource,
              casetypeResource,
              zsModal
            ) => {
              const scope = assign($rootScope.$new(true), {
                caseResource,
                casetypeResource,
              });

              const modal = zsModal({
                title: 'Meer informatie over de zaak',
                el: $compile(
                  `<zs-case-about-view 
                     case-resource="caseResource"
                     casetype-resource="casetypeResource"
                   ></zs-case-about-view>`
                )(scope),
              });

              const unregister = $rootScope.$on(
                '$stateChangeStart',
                closeModal
              );

              const closeModal = () => {
                modal.close();
                scope.$destroy();
                unregister();
              };

              modal.open();

              modal.onClose(() => {
                $state.go('^');

                return false;
              });
            },
          ],
        });
    },
  ]).name;
