--
-- PostgreSQL database dump
--

-- Dumped from database version 12.8 (Debian 12.8-1.pgdg100+1)
-- Dumped by pg_dump version 12.8 (Debian 12.8-1.pgdg100+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: hstore; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS hstore WITH SCHEMA public;


--
-- Name: EXTENSION hstore; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION hstore IS 'data type for storing sets of (key, value) pairs';


--
-- Name: pg_trgm; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS pg_trgm WITH SCHEMA public;


--
-- Name: EXTENSION pg_trgm; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION pg_trgm IS 'text similarity measurement and index searching based on trigrams';


--
-- Name: uuid-ossp; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS "uuid-ossp" WITH SCHEMA public;


--
-- Name: EXTENSION "uuid-ossp"; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION "uuid-ossp" IS 'generate universally unique identifiers (UUIDs)';


--
-- Name: confidentiality; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.confidentiality AS ENUM (
    'public',
    'internal',
    'confidential'
);


--
-- Name: contactmoment_medium; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.contactmoment_medium AS ENUM (
    'behandelaar',
    'balie',
    'telefoon',
    'post',
    'email',
    'webformulier'
);


--
-- Name: contactmoment_type; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.contactmoment_type AS ENUM (
    'email',
    'note'
);


--
-- Name: custom_object_type_status; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.custom_object_type_status AS ENUM (
    'active',
    'inactive'
);


--
-- Name: custom_object_version_content_archive_status; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.custom_object_version_content_archive_status AS ENUM (
    'archived',
    'to destroy',
    'to preserve'
);


--
-- Name: custom_object_version_status; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.custom_object_version_status AS ENUM (
    'active',
    'inactive',
    'draft'
);


--
-- Name: documentstatus; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.documentstatus AS ENUM (
    'original',
    'copy',
    'replaced',
    'converted'
);


--
-- Name: preferred_contact_channel; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.preferred_contact_channel AS ENUM (
    'email',
    'mail',
    'pip',
    'phone'
);


--
-- Name: zaaksysteem_bag_types; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.zaaksysteem_bag_types AS ENUM (
    'nummeraanduiding',
    'verblijfsobject',
    'pand',
    'openbareruimte'
);


--
-- Name: zaaksysteem_status; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.zaaksysteem_status AS ENUM (
    'new',
    'open',
    'resolved',
    'stalled',
    'deleted',
    'overdragen'
);


--
-- Name: zaaksysteem_trigger; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.zaaksysteem_trigger AS ENUM (
    'extern',
    'intern'
);


--
-- Name: appointment_attribute_value_to_jsonb(text, uuid); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.appointment_attribute_value_to_jsonb(value text, reference uuid, OUT value_json jsonb) RETURNS jsonb
    LANGUAGE plpgsql
    AS $$
  DECLARE
    appointment jsonb;
    preview text;
    start_time timestamp with time zone;
    end_time timestamp with time zone;
  BEGIN

      IF value = '{}'
      THEN
        RETURN;
      END IF;

      appointment := (value)::jsonb->'values';

      start_time := (appointment->'start_time'->'value'->>'__DateTime__')::timestamp with time zone;
      end_time := (appointment->'end_time'->'value'->>'__DateTime__')::timestamp with time zone;

      preview := concat(
        to_char(timezone('Europe/Amsterdam', start_time), 'YYYY-MM-DD HH:MI'),
        ' - ',
        to_char(timezone('Europe/Amsterdam', end_time), 'YYYY-MM-DD HH:MI')
      );

      SELECT INTO value_json jsonb_build_object(
        'type', 'appointment',
        'reference', reference,
        'preview', preview,
        'instance', jsonb_build_object(
          'date_created', (appointment->'date_created'->'value'->>'__DateTime__')::timestamp with time zone,
          'date_modified', (appointment->'date_modified'->'value'->>'__DateTime__')::timestamp with time zone,
          'start_time', start_time,
          'end_time', end_time,
          'plugin_type', appointment->'plugin_type'->>'value',
          'plugin_data', appointment->'plugin_data'->'value'
        )
      );
      SELECT INTO value_json to_jsonb(ARRAY[value_json]);
      RETURN;
  END;
  $$;


--
-- Name: attribute_date_value_to_text(text); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.attribute_date_value_to_text(value text, OUT datestamp text) RETURNS text
    LANGUAGE plpgsql
    AS $$
  DECLARE
    dd text;
    mm text;
    yy text;
    dt timestamp;
  BEGIN
    dd := split_part(value, '-', 1);
    mm := split_part(value, '-', 2);
    yy := split_part(value, '-', 3);

    -- We have attributes which are switched around :((
    IF length(yy) = 2 AND length(dd) = 4
    THEN
      -- 2019-10-21
      dt := make_date(dd::int, mm::int, yy::int);
    ELSIF length(yy) > 2 and length(dd) = 4
    THEN
      -- and we have actual timestamps
      dt := value::date;
    ELSE
      -- 25-10-2019
      dt := make_date(yy::int, mm::int, dd::int);
    END IF;

    SELECT INTO datestamp timestamp_to_perl_datetime(dt::timestamp with time zone at time zone 'Europe/Amsterdam');
  EXCEPTION WHEN OTHERS THEN
    RETURN;

  END;
  $$;


--
-- Name: attribute_file_value_to_v0_jsonb(text[]); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.attribute_file_value_to_v0_jsonb(value text[], OUT value_json jsonb) RETURNS jsonb
    LANGUAGE plpgsql
    AS $$
  DECLARE
    length int;
    m text;

    r record;

  BEGIN

    value_json := '[]'::jsonb;

    FOR r IN
      SELECT
        f.accepted as accepted,
        f.confidential as confidential,
        CONCAT(f.name, f.extension) as filename,
        f.id as id,
        fs.uuid as uuid,
        fs.size as filesize,
        fs.original_name as original_name,
        fs.mimetype as mimetype,
        fs.md5 as md5,
        fs.is_archivable as is_archivable
      FROM
        filestore fs
      JOIN
        file f
      ON
        (f.filestore_id = fs.id)
      WHERE
        fs.uuid = ANY(value::uuid[])
      ORDER by f.id
    LOOP

      select into value_json value_json || jsonb_build_object(
        'accepted', CASE WHEN r.accepted = true THEN 1 ELSE 0 END,
        'confidential', r.confidential,
        'file_id', r.id,
        'filename', r.filename,
        'is_archivable', CASE WHEN r.is_archivable = true THEN 1 ELSE 0 END,
        'md5', r.md5,
        'mimetype', r.mimetype,
        'original_name', r.original_name,
        'size', r.filesize,
        'thumbnail_uuid', null,
        'uuid', r.uuid
      );
    END LOOP;

    RETURN;


  END;
  $$;


--
-- Name: attribute_value_to_jsonb(text[], text); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.attribute_value_to_jsonb(value text[], value_type text, OUT value_json jsonb) RETURNS jsonb
    LANGUAGE plpgsql
    AS $$
  DECLARE
    length int;
    m text;

  BEGIN

      value_json := '[]'::jsonb;
      SELECT INTO length cardinality(COALESCE(value, '{}'::text[]));

      IF length = 0 and value_type = 'file'
      THEN
        value_json := '[]'::jsonb;
        RETURN;
      ELSIF length = 0
      THEN
        value_json := '[null]'::jsonb;
        RETURN;
      END IF;

      IF value_type = 'date'
      THEN
        SELECT INTO value array_agg(attribute_date_value_to_text(value[1]));
      END IF;

      IF value_type LIKE 'bag_%'
      THEN
        SELECT INTO value_json bag_attribute_value_to_jsonb(value, value_type);
        RETURN;
      END IF;

      IF value_type IN ('geojson', 'address_v2', 'appointment_v2', 'relationship')
      THEN

          FOREACH m IN ARRAY value
          LOOP
            SELECT INTO value_json value_json || jsonb_build_array((CONCAT('[', m::text, ']')::json ->> 0)::json);
          END LOOP;

      ELSIF value_type IN ('checkbox', 'select')
      THEN
        SELECT INTO value_json to_jsonb(ARRAY[value]);
      ELSE
        SELECT INTO value_json to_jsonb(value);
      END IF;
      RETURN;
  END;
  $$;


--
-- Name: attribute_value_to_v0(text[], text, boolean); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.attribute_value_to_v0(value text[], value_type text, value_mvp boolean, OUT value_json jsonb) RETURNS jsonb
    LANGUAGE plpgsql
    AS $$
  DECLARE
    length int;
    m text;

  BEGIN

    value_json := null;
    SELECT INTO length cardinality(COALESCE(value, '{}'::text[]));

    IF length = 0 AND value_type = 'file'
    THEN
      value_json := '[]'::jsonb;
      RETURN;
    ELSIF length = 0
    THEN
      RETURN;
    END IF;

    IF value_type = 'date'
    THEN
      SELECT INTO value_json to_jsonb(attribute_date_value_to_text(value[1]));
      RETURN;
    END IF;


    IF value_type LIKE 'bag_%'
    THEN

      SELECT INTO value_json bag_attribute_value_to_jsonb(value, value_type);
      RETURN;

    END IF;

    IF value_type = 'file'
    THEN

      SELECT INTO value_json attribute_file_value_to_v0_jsonb(value);
      RETURN;

    END IF;


    IF value_type IN ('geojson', 'address_v2', 'appointment_v2', 'relationship')
    THEN

        IF value_mvp = true
        THEN
          value_json := '[]'::jsonb;
          FOREACH m IN ARRAY value
          LOOP
            SELECT INTO value_json value_json || to_jsonb(m::jsonb);
          END LOOP;
        ELSE
          SELECT INTO value_json to_jsonb(value[1]::jsonb);
        END IF;
        RETURN;
    END IF;

    IF value_type IN ('checkbox', 'select')
    THEN
      SELECT INTO value_json to_jsonb(value);
    END IF;


    IF value_type = 'valuta'
    THEN
      value_json := value[1]::numeric;
      RETURN;
    END IF;

    IF value_mvp = true
    THEN
      SELECT INTO value_json to_jsonb(value);
    ELSE
      SELECT INTO value_json to_jsonb(value[1]);
    END IF;


    RETURN;
  END;
  $$;


--
-- Name: bag_attribute_value_to_jsonb(text[], text); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.bag_attribute_value_to_jsonb(value text[], value_type text, OUT value_json jsonb) RETURNS jsonb
    LANGUAGE plpgsql
    AS $$
  DECLARE
    length int;
    val text;

    btype text;
    bid text;

  BEGIN

      value_json := '[]'::jsonb;
      SELECT INTO length cardinality(COALESCE(value, '{}'::text[]));
      IF length = 0
      THEN
        value_json := '[null]'::jsonb;
        RETURN;
      END IF;

      FOREACH val IN ARRAY value
      LOOP

        btype := split_part(val, '-', 1);
        bid   := split_part(val, '-', 2);

        SELECT INTO value_json value_json || bag_type_id_to_json(btype, bid);

      END LOOP;
      RETURN;
  END;
  $$;


--
-- Name: bag_attribute_value_to_jsonb(text[], text, integer); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.bag_attribute_value_to_jsonb(value text[], value_type text, case_id integer, OUT value_json jsonb) RETURNS jsonb
    LANGUAGE plpgsql
    AS $$
  DECLARE
    length int;
    val text;

    btype text;
    bid text;

  BEGIN

      value_json := '[]'::jsonb;
      SELECT INTO length cardinality(COALESCE(value, '{}'::text[]));
      IF length = 0
      THEN
        value_json := '[null]'::jsonb;
        RETURN;
      END IF;

      FOREACH val IN ARRAY value
      LOOP

        btype := split_part(val, '-', 1);
        bid   := split_part(val, '-', 2);

        SELECT INTO value_json value_json || bag_type_id_to_json(btype, bid);

      END LOOP;
      RETURN;
  END;
  $$;


--
-- Name: bag_type_id_to_json(text, text); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.bag_type_id_to_json(btype text, bid text, OUT value_json jsonb) RETURNS jsonb
    LANGUAGE plpgsql
    AS $$
  DECLARE
    val text;

    bag_record jsonb;
    bag_json jsonb;

    display_name text;
    lat_long text;
  BEGIN

    bid := lpad(bid, 16, '0');

    SELECT INTO bag_record bag_data FROM bag_cache bc WHERE
      bc.bag_type = btype and bc.bag_id = bid;

    SELECT INTO lat_long REGEXP_REPLACE(bag_record->>'centroide_ll','POINT\((.*) (.*)\)','\2,\1');

    SELECT INTO bag_json json_build_object(
        'identification', bid
    );

    IF btype = 'nummeraanduiding'
    THEN

      SELECT INTO bag_json bag_json || jsonb_build_object(
        'straat', bag_record->'straatnaam',
        'gps_lat_lon', lat_long,
        'huisnummer', bag_record->'huisnummer',
        'postcode', bag_record->'postcode',
        'huisletter', bag_record->'huisletter',
        'huisnummertoevoeging', bag_record->'huisnummertoevoeging',
        'woonplaats', bag_record->'woonplaatsnaam',

        -- We don't have the data in the DB, if there.. fill them.
        -- This is just here to keep the API the same.
        'end_date', null,
        'start_date', null,
        'under_investigation', null,
        'document_date', null,
        'document_number', null
      );

    ELSIF btype = 'openbareruimte'
    THEN

      SELECT INTO bag_json bag_json || jsonb_build_object(
        'straat', bag_record->'straatnaam',
        'gps_lat_lon', lat_long
      );

    ELSIF btype = 'verblijfsobject'
    THEN

      SELECT INTO bag_json bag_json || jsonb_build_object(
        'surface_area', bag_record->'wfs_data'->'oppervlakte',
        'year_of_construction', bag_record->'wfs_data'->'bouwjaar',
        'gebruiksdoel', bag_record->'wfs_data'->'gebruiksdoel'
      );

    END IF;

    display_name := bag_record->>'weergavenaam';

    select into value_json jsonb_build_object(
      'human_identifier', display_name,
      'bag_id', CONCAT(btype, '-', bid),
      'address_data', bag_json
    );

  RETURN;
  END;
  $$;


--
-- Name: case_location_as_v0_json(public.hstore); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.case_location_as_v0_json(location public.hstore, OUT value_json jsonb) RETURNS jsonb
    LANGUAGE plpgsql
    AS $$
  DECLARE
    btype text;
    bid text;

    types text[];
    key text;
    brecord jsonb;

  BEGIN

    SELECT INTO value_json jsonb_build_object();

    types := ARRAY[
      'ligplaats',
      'nummeraanduiding',
      'openbareruimte',
      'pand',
      'standplaats',
      'verblijfsobject',
      'woonplaats'
    ];

    FOREACH btype in ARRAY types
    LOOP

      IF btype = 'ligplaats'
      THEN
          bid := location->'bag_ligplaats_id';
      ELSIF btype = 'nummeraanduiding'
      THEN
          bid := location->'bag_nummeraanduiding_id';
      ELSIF btype = 'openbareruimte'
      THEN
          bid := location->'bag_openbareruimte_id';
      ELSIF btype = 'pand'
      THEN
          bid := location->'bag_pand_id';
      ELSIF btype = 'verblijfsobject'
      THEN
          bid := location->'bag_verblijfsobject_id';
      ELSIF btype = 'woonplaats'
      THEN
          bid := location->'bag_woonplaats_id';
      ELSIF btype = 'standplaats'
      THEN
          bid := location->'bag_standplaats_id';
      END IF;

      -- we don't store it, so we just fill it here for now
      SELECT INTO value_json value_json || jsonb_build_object(
        CONCAT('case.correspondence_location.', btype), null
      );

      key := CONCAT('case.case_location.', btype);

      IF bid IS NULL
      THEN
        SELECT INTO value_json value_json || jsonb_build_object(
          key, null
        );
        CONTINUE;
      END IF;

      SELECT INTO brecord bag_type_id_to_json(btype, bid);

      SELECT INTO value_json value_json || jsonb_build_object(
        key, brecord
      );

    END LOOP;



  END;
  $$;


--
-- Name: case_subject_as_v0_json(public.hstore, text, boolean); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.case_subject_as_v0_json(zb public.hstore, type text, historic boolean, OUT json jsonb) RETURNS jsonb
    LANGUAGE plpgsql
    AS $$
  DECLARE
    btype text;

    gm_id int;
    b_id int;

    subject record;
    s_hstore hstore;
    subject_json json;

    contactdata_person int;
    contactdata_company int;

  BEGIN

    contactdata_person  := 1;
    contactdata_company := 2;

    btype := zb->'betrokkene_type';
    gm_id := (zb->'gegevens_magazijn_id')::int;
    b_id  := (zb->'betrokkene_id')::int;

    IF historic = true
    THEN
      type := CONCAT(type, '_snapshot');
    END IF;

    IF btype IS NULL THEN
      SELECT INTO json empty_subject_as_v0_json(type);
      RETURN;
    END IF;

    IF btype NOT IN ('medewerker', 'natuurlijk_persoon', 'bedrijf') THEN
      RAISE EXCEPTION 'Unknown betrokkene type %', btype;
    END IF;

    IF btype = 'medewerker' THEN

      -- There is no difference between historic data for employees

      SELECT INTO
        subject
        s.*,
        gr.name as department

      FROM
        subject s
      LEFT JOIN
        groups gr
      ON
        s.group_ids[1] = gr.id
      WHERE
        s.subject_type = 'employee'
      AND
        s.id = gm_id;

      s_hstore := hstore(subject);

      SELECT INTO json employee_as_v0_json(s_hstore, type);

    ELSIF btype = 'natuurlijk_persoon' THEN

      IF historic = false
      THEN

        SELECT INTO
          subject
          np.*,
          a.straatnaam as street,
          a.huisnummer as street_number,
          a.huisletter as street_number_letter,
          a.huisnummertoevoeging as street_number_suffix,
          a.postcode as zipcode,
          a.woonplaats as city,
          a.functie_adres as address_type,
          mc.json as municipality,
          cc.label as country,
          a.adres_buitenland1 as foreign_address_line_1,
          a.adres_buitenland2 as foreign_address_line_2,
          a.adres_buitenland3 as foreign_address_line_3,
          a.bag_id as bag_id,
          a.geo_lat_long[0] as latitude,
          a.geo_lat_long[1] as longitude,
          cd.mobiel as mobile,
          cd.telefoonnummer as phone_number,
          cd.email as email_address
        FROM
          natuurlijk_persoon np
        LEFT JOIN
          adres a
        ON
          np.adres_id = a.id
        LEFT JOIN
          country_code cc
        ON
          cc.dutch_code = a.landcode
        LEFT JOIN
          municipality_code_v1_view mc
        ON
          mc.dutch_code = a.gemeente_code
        LEFT JOIN
          contact_data cd
        ON
          (cd.gegevens_magazijn_id = np.id and cd.betrokkene_type = contactdata_person)

        WHERE
          np.id = gm_id;
      ELSE

        SELECT INTO
          subject
          np.*,
          a.straatnaam as street,
          a.huisnummer as street_number,
          a.huisletter as street_number_letter,
          a.huisnummertoevoeging as street_number_suffix,
          a.postcode as zipcode,
          a.woonplaats as city,
          a.functie_adres as address_type,
          mc.json as municipality,
          cc.label as country,
          a.adres_buitenland1 as foreign_address_line_1,
          a.adres_buitenland2 as foreign_address_line_2,
          a.adres_buitenland3 as foreign_address_line_3,
          a.bag_id as bag_id,
          a.geo_lat_long[0] as latitude,
          a.geo_lat_long[1] as longitude,
          cd.mobiel as mobile,
          cd.telefoonnummer as phone_number,
          cd.email as email_address
        FROM
          gm_natuurlijk_persoon np
        LEFT JOIN
          gm_adres a
        ON
          np.adres_id = a.id
        LEFT JOIN
          country_code cc
        ON
          cc.dutch_code = a.landcode
        LEFT JOIN
          municipality_code_v1_view mc
        ON
          mc.dutch_code = a.gemeente_code
        LEFT JOIN
          contact_data cd
        ON
          (cd.gegevens_magazijn_id = np.id and cd.betrokkene_type = contactdata_person)

        WHERE
          np.gegevens_magazijn_id = gm_id and np.id = b_id;
      END IF;

      s_hstore := hstore(subject);

      SELECT INTO json person_as_v0_json(s_hstore, type);

    ELSIF btype = 'bedrijf' THEN

      IF historic = false
      THEN

        SELECT INTO subject
        b.*,
        let.label as company_type,
        cc.label as country_of_residence,
        cd.mobiel as mobile,
        cd.telefoonnummer as phone_number,
        cd.email as email_address
        FROM
          bedrijf b
        LEFT JOIN
          legal_entity_type let
        ON
          let.code = b.rechtsvorm
        LEFT JOIN
          country_code cc
        ON
          cc.dutch_code = b.vestiging_landcode
        LEFT JOIN
          contact_data cd
        ON
          (cd.gegevens_magazijn_id = b.id and cd.betrokkene_type = contactdata_company)
        WHERE
          b.id = gm_id;
      ELSE

        SELECT INTO subject
        b.*,
        let.label as company_type,
        cc.label as country_of_residence,
        cd.mobiel as mobile,
        cd.telefoonnummer as phone_number,
        cd.email as email_address
        FROM
          gm_bedrijf b
        LEFT JOIN
          legal_entity_type let
        ON
          let.code = b.rechtsvorm
        LEFT JOIN
          country_code cc
        ON
          cc.dutch_code = b.vestiging_landcode
        LEFT JOIN
          contact_data cd
        ON
          (cd.gegevens_magazijn_id = b.id and cd.betrokkene_type = contactdata_company)
        WHERE
          b.gegevens_magazijn_id = gm_id and b.id = b_id;

      END IF;

      s_hstore := hstore(subject);

      SELECT INTO json company_as_v0_json(s_hstore, type);
      RETURN;

    END IF;

  END;
  $$;


--
-- Name: case_subject_json(public.hstore); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.case_subject_json(zb public.hstore, OUT json jsonb) RETURNS jsonb
    LANGUAGE plpgsql
    AS $$
  DECLARE
    btype text;
    gm_id int;
    old_id text;

    subject record;
    s_hstore hstore;
    subject_json json;

    subject_uuid uuid;
    subject_type text;
    display_name text;

  BEGIN

    btype := zb->'betrokkene_type';

    IF btype IS NULL THEN
      json := null;
      RETURN;
    END IF;

    IF btype NOT IN ('medewerker', 'natuurlijk_persoon', 'bedrijf') THEN
      RAISE EXCEPTION 'Unknown betrokkene type %', btype;
    END IF;


    gm_id := (zb->'gegevens_magazijn_id')::int;
    old_id := 'betrokkene-' || btype || '-' || gm_id;

    IF btype = 'medewerker' THEN
      subject_type := 'employee';

      SELECT INTO subject * FROM subject s WHERE s.subject_type = 'employee'
        AND id = gm_id;

      s_hstore := hstore(subject);
      SELECT INTO subject_json subject_employee_json(s_hstore);
      SELECT INTO display_name get_display_name_for_employee(s_hstore);

    ELSIF btype = 'natuurlijk_persoon' THEN

      subject_type := 'person';

      SELECT INTO
        subject
        np.*,
        a.straatnaam as street,
        a.huisnummer as street_number,
        a.huisletter as street_number_letter,
        a.huisnummertoevoeging as street_number_suffix,
        a.postcode as zipcode,
        a.woonplaats as city,
        a.functie_adres as address_type,
        mc.json as municipality,
        cc.json as country,
        a.adres_buitenland1 as foreign_address_line_1,
        a.adres_buitenland2 as foreign_address_line_2,
        a.adres_buitenland3 as foreign_address_line_3,
        a.bag_id as bag_id,
        a.geo_lat_long[0] as latitude,
        a.geo_lat_long[1] as longitude,
        cd.mobiel as mobile,
        cd.telefoonnummer as phone_number,
        cd.email as email_address
      FROM
        natuurlijk_persoon np
      LEFT JOIN
        adres a
      ON
        np.adres_id = a.id
      LEFT JOIN
        country_code_v1_view cc
      ON
        cc.dutch_code = a.landcode
      LEFT JOIN
        municipality_code_v1_view mc
      ON
        mc.dutch_code = a.gemeente_code
      LEFT JOIN
        contact_data cd
      ON
        -- 1 or 2 is much clearer than natuurlijk_persoon or bedrijf ey
        -- :/
        (cd.gegevens_magazijn_id = np.id and cd.betrokkene_type = 1)

      WHERE
        np.id = gm_id;

      s_hstore := hstore(subject);

      SELECT INTO subject_json subject_person_json(s_hstore);
      SELECT INTO display_name get_display_name_for_person(s_hstore);

    ELSIF btype = 'bedrijf' THEN

      subject_type := 'company';

      SELECT INTO subject
      b.*,
      cc_vestiging.json as vestiging_country,
      cc_correspondentie.json as correspondentie_country,
      let.json as company_type
      FROM
        bedrijf b
      LEFT JOIN
        country_code_v1_view cc_vestiging
      ON
        cc_vestiging.dutch_code = b.vestiging_landcode
      LEFT JOIN
        country_code_v1_view cc_correspondentie
      ON
        cc_correspondentie.dutch_code = b.correspondentie_landcode
      LEFT JOIN
        legal_entity_v1_view let
      ON
        let.code = b.rechtsvorm
      WHERE
        b.id = gm_id
      ;

      s_hstore := hstore(subject);

      SELECT INTO subject_json subject_company_json(s_hstore);
      SELECT INTO display_name get_display_name_for_company(s_hstore);

    END IF;

    SELECT INTO json json_build_object(
        'preview', display_name,
        'type', 'subject',
        'reference', subject.uuid,
        'instance', json_build_object(
          'date_created', timestamp_to_perl_datetime(NOW()),
          'date_modified', timestamp_to_perl_datetime(NOW()),
          'display_name', display_name,
          'external_subscription', null,
          'old_subject_identifier', old_id,
          'subject_type', subject_type,
          'subject', subject_json
        )
    );

  END;
  $$;


--
-- Name: check_queue_object_id_constraint(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.check_queue_object_id_constraint() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
  BEGIN

    IF NEW.object_id IS NOT NULL
    THEN
      IF NOT EXISTS (SELECT uuid FROM object_data WHERE uuid = NEW.object_id)
      THEN
        RAISE EXCEPTION 'Nonexistent UUID --> %', NEW.object_id
          USING HINT = 'Please check your object_id';
      END IF;
    END IF;

    return NEW;

  END $$;


--
-- Name: check_queue_parent_id_constraint(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.check_queue_parent_id_constraint() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
  BEGIN

    IF NEW.parent_id IS NOT NULL
    THEN
      IF NOT EXISTS (SELECT id FROM queue WHERE id = NEW.parent_id)
      THEN
        RAISE EXCEPTION 'Nonexistent UUID --> %', NEW.parent_id
          USING HINT = 'Please check your parent_id';
      END IF;
    END IF;

    return NEW;

  END $$;


--
-- Name: company_as_v0_json(public.hstore, text); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.company_as_v0_json(subject public.hstore, type text, OUT json jsonb) RETURNS jsonb
    LANGUAGE plpgsql
    AS $$
  DECLARE
    key text;
    is_correspondence boolean;
    tmp text;

    active boolean;
    bid text;

  BEGIN

    key := CONCAT('case.', type, '.');

    is_correspondence := false;

    FOREACH tmp IN ARRAY ARRAY['woonplaats', 'straatnaam', 'postcode', 'adres_buitenland1', 'adres_buitenland2', 'adres_buitenland3']
    LOOP
      tmp := CONCAT('correspondentie_', tmp);
      IF subject->tmp IS NOT NULL AND LENGTH(subject->tmp) > 0 THEN
        is_correspondence := true;
        EXIT;
      END IF;
    END LOOP;


    SELECT INTO json jsonb_build_object(

      CONCAT(key, 'foreign_residence_address_line1'), subject->'vestiging_adres_buitenland1',
      CONCAT(key, 'foreign_residence_address_line2'), subject->'vestiging_adres_buitenland2',
      CONCAT(key, 'foreign_residence_address_line3'), subject->'vestiging_adres_buitenland3',

      CONCAT(key, 'correspondence_house_number'), complete_house_number(
        subject->'correspondentie_huisnummer', subject->'correspondentie_huisletter', subject->'correspondentie_huisnummertoevoeging'
      ),
      CONCAT(key, 'correspondence_place_of_residence'), subject->'correspondentie_woonplaats',
      CONCAT(key, 'correspondence_street'), subject->'correspondentie_straatnaam',
      CONCAT(key, 'correspondence_zipcode'), subject->'correspondentie_postcode',

      CONCAT(key, 'residence_house_number'), complete_house_number(
        subject->'vestiging_huisnummer', subject->'vestiging_huisletter', subject->'vestiging_huisnummertoevoeging'
      ),
      CONCAT(key, 'residence_place_of_residence'), subject->'vestiging_woonplaats',
      CONCAT(key, 'residence_street'), subject->'vestiging_straatnaam',
      CONCAT(key, 'residence_zipcode'), subject->'vestiging_postcode'

    );

    IF is_correspondence = true THEN
      SELECT INTO json json || jsonb_build_object(
        CONCAT(key, 'street'),  subject->'correspondentie_straatnaam',
        CONCAT(key, 'zipcode'), subject->'correspondentie_postcode',
        CONCAT(key, 'place_of_residence'), subject->'correspondentie_woonplaats',
        CONCAT(key, 'house_number'), complete_house_number(
          subject->'correspondentie_huisnummer', subject->'correspondentie_huisletter', subject->'correspondentie_huisnummertoevoeging'
        )
      );
    ELSE
      SELECT INTO json json || jsonb_build_object(
        CONCAT(key, 'street'),  subject->'vestiging_straatnaam',
        CONCAT(key, 'zipcode'), subject->'vestiging_postcode',
        CONCAT(key, 'place_of_residence'), subject->'vestiging_woonplaats',
        CONCAT(key, 'house_number'), complete_house_number(
          subject->'vestiging_huisnummer', subject->'vestiging_huisletter', subject->'vestiging_huisnummertoevoeging'
        )
      );
    END IF;

    -- active does not exists in the gm_ table of bedrijf
    -- and is always active, since well, you can't create cases with
    -- inactive people
    active := true;

    IF subject->'active' IS NOT NULL
    THEN
      active := (subject->'active')::boolean;
    END IF;

    IF subject->'gegevens_magazijn_id' IS NOT NULL
    THEN
      bid := CONCAT('betrokkene-bedrijf-', subject->'gegevens_magazijn_id');
    ELSE
      bid := CONCAT('betrokkene-bedrijf-', subject->'id');
    END IF;

    SELECT INTO json json || jsonb_build_object(
      type, subject->'uuid',
      CONCAT(key, 'type'), 'Niet natuurlijk persoon',
      CONCAT(key, 'subject_type'), 'bedrijf',

      CONCAT(key, 'status'), CASE WHEN active = true THEN 'Actief' ELSE 'Inactief' END,

      CONCAT(key, 'id'), bid,

      CONCAT(key, 'uuid'), subject->'uuid',
      CONCAT(key, 'preferred_contact_channel'), subject->'preferred_contact_channel',
      CONCAT(key, 'country_of_residence'), COALESCE(
        subject->'country_of_residence',
        CONCAT('Onbekende landcode: ', subject->'vestiging_landcode')
      ),

      CONCAT(key, 'name'), subject->'handelsnaam',
      CONCAT(key, 'display_name'), subject->'handelsnaam',
      CONCAT(key, 'coc'), subject->'dossiernummer',
      CONCAT(key, 'establishment_number'), subject->'vestigingsnummer',
      -- we have the actual data?
      CONCAT(key, 'type_of_business_entity'), (subject->'rechtsvorm')::int,
      CONCAT(key, 'trade_name'), subject->'handelsnaam',
      CONCAT(key, 'has_correspondence_address'), is_correspondence,

      CONCAT(key, 'email'), subject->'email_address',
      CONCAT(key, 'mobile_number'), subject->'mobile',
      CONCAT(key, 'phone_number'), subject->'phone_number',

      -- Not relevant for a company
      CONCAT(key, 'investigation'), null,
      CONCAT(key, 'a_number'), null,
      CONCAT(key, 'bsn'), null,
      CONCAT(key, 'family_name'), null,
      CONCAT(key, 'first_names'), null,
      CONCAT(key, 'surname'), null,
      CONCAT(key, 'full_name'), null,
      CONCAT(key, 'gender'), null,
      CONCAT(key, 'salutation'), null,
      CONCAT(key, 'salutation1'), null,
      CONCAT(key, 'salutation2'), null,
      CONCAT(key, 'initials'), null,
      CONCAT(key, 'is_secret'), null,
      CONCAT(key, 'naamgebruik'), null,
      CONCAT(key, 'noble_title'), null,
      CONCAT(key, 'place_of_birth'), null,
      CONCAT(key, 'surname_prefix'), null,
      CONCAT(key, 'country_of_birth'), null,
      CONCAT(key, 'date_of_birth'), null,
      CONCAT(key, 'date_of_death'), null,
      CONCAT(key, 'date_of_divorce'), null,
      CONCAT(key, 'date_of_marriage'), null,
      CONCAT(key, 'login'), null,
      CONCAT(key, 'department'), null,
      CONCAT(key, 'title'), null,
      CONCAT(key, 'password'), null

    );

  END;
  $$;


--
-- Name: complete_house_number(text, text, text); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.complete_house_number(house_number text, letter text, postfix text, OUT complete text) RETURNS text
    LANGUAGE plpgsql
    AS $$
  DECLARE

  BEGIN

    IF house_number IS NULL
    THEN
      complete = '';
      RETURN;
    END IF;

    complete := house_number::text;

    IF letter IS NOT NULL AND length(letter) > 0
    THEN
      complete := CONCAT(complete, ' ', letter);
    END IF;


    IF postfix IS NOT NULL AND length(postfix) > 0
    THEN
      complete := CONCAT(complete, ' - ', postfix);
    END IF;

    RETURN;

  END;
  $$;


--
-- Name: country_code_to_json(public.hstore); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.country_code_to_json(unit public.hstore, OUT unit_json jsonb) RETURNS jsonb
    LANGUAGE plpgsql
    AS $$
  BEGIN
      SELECT INTO unit_json json_build_object(
        'reference', unit->'uuid',
        'preview', unit->'label',
        'type', 'country_code',
        'instance', json_build_object(
          'label', unit->'label',
          'alpha_one', null,
          'alpha_two', null,
          'alpha_three', null,
          'code', null,
          'dutch_code', lpad(unit->'dutch_code', 4, '0')
        )
      );
      RETURN;
  END;
  $$;


--
-- Name: create_queue_partition(date); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.create_queue_partition(t_stamp date) RETURNS void
    LANGUAGE plpgsql
    AS $$
  DECLARE
    t_name text;
    t_end date;
    t_start date;
  BEGIN

    t_start := t_stamp;
    t_end := t_stamp + interval '1 month';

    SELECT INTO t_name REGEXP_REPLACE('queue_' || t_stamp::text, '-', '', 'g');

    RAISE NOTICE '%', t_name;

    EXECUTE format('
       CREATE TABLE %I PARTITION OF queue_part
         FOR VALUES FROM (%L) TO (%L)
    ', t_name, t_start, t_end);

    EXECUTE format('
      CREATE TRIGGER insert_leading_qitem
      BEFORE INSERT ON %I
      FOR EACH ROW
      EXECUTE PROCEDURE set_leading_qitem();
    ' , t_name);

    EXECUTE format('
      CREATE TRIGGER check_queue_object_id_constraint
      BEFORE INSERT ON %I
      FOR EACH ROW
      EXECUTE PROCEDURE check_queue_object_id_constraint();
    ' , t_name);

    EXECUTE format('
      CREATE TRIGGER check_queue_parent_id_constraint
      BEFORE INSERT ON %I
      FOR EACH ROW
      EXECUTE PROCEDURE check_queue_parent_id_constraint();
    ' , t_name);

    EXECUTE format('
      CREATE INDEX ON %I((data::jsonb->%L->>%L)) WHERE type = %L
    ' , t_name, 'create-args', 'duplicate_prevention_token', 'create_case_form');

  END $$;


--
-- Name: employee_as_v0_json(public.hstore, text); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.employee_as_v0_json(subject public.hstore, type text, OUT json jsonb) RETURNS jsonb
    LANGUAGE plpgsql
    AS $$
  DECLARE
    key text;

    properties jsonb;
    config_row jsonb;

    ue record;
    is_active boolean;

  BEGIN

    key := CONCAT('case.', type, '.');

    properties := (subject->'properties')::jsonb;

    SELECT INTO config_row jsonb_object_agg(config.parameter, config.value) from config where parameter like 'customer_info%';

    is_active := false;
    FOR ue IN
      select distinct(active) as active from user_entity where subject_id = (subject->'id')::int and date_deleted is null
    LOOP
      IF ue.active = true THEN
        is_active := true;
        EXIT;
      END IF;
    END LOOP;

    SELECT INTO json jsonb_build_object(

      type, subject->'uuid',
      CONCAT(key, 'type'), 'Behandelaar',
      CONCAT(key, 'subject_type'), 'medewerker',

      CONCAT(key, 'status'), CASE WHEN is_active = true THEN 'Actief' ELSE 'Inactief' END,

      CONCAT(key, 'uuid'), subject->'uuid',
      CONCAT(key, 'id'), CONCAT('betrokkene-medewerker-', subject->'id'),
      CONCAT(key, 'preferred_contact_channel'), null,
      CONCAT(key, 'country_of_residence'), null,

      CONCAT(key, 'display_name'), properties->'displayname',
      CONCAT(key, 'name'), properties->'displayname',
      CONCAT(key, 'full_name'), properties->'displayname',
      CONCAT(key, 'initials'), properties->'initials',
      CONCAT(key, 'naamgebruik'), properties->'sn',
      CONCAT(key, 'surname'), properties->'sn',

      CONCAT(key, 'department'), subject->'department',

      CONCAT(key, 'email'), properties->'mail',
      CONCAT(key, 'phone_number'), properties->'telephonenumber',

      -- blub
      CONCAT(key, 'mobile_number'), null,
      CONCAT(key, 'login'), null,
      CONCAT(key, 'title'), null,
      CONCAT(key, 'password'), null,

      -- In use...
      CONCAT(key, 'correspondence_house_number'), config_row->'customer_info_huisnummer',
      CONCAT(key, 'correspondence_place_of_residence'), config_row->'customer_info_woonplaats',
      CONCAT(key, 'correspondence_street'), config_row->'customer_info_straatnaam',
      CONCAT(key, 'correspondence_zipcode'), config_row->'customer_info_postcode',

      CONCAT(key, 'residence_house_number'), config_row->'customer_info_huisnummer',
      CONCAT(key, 'residence_place_of_residence'), config_row->'customer_info_woonplaats',
      CONCAT(key, 'residence_street'), config_row->'customer_info_straatnaam',
      CONCAT(key, 'residence_zipcode'), config_row->'customer_info_postcode',

      CONCAT(key, 'house_number'), config_row->'customer_info_huisnummer',
      CONCAT(key, 'place_of_residence'), config_row->'customer_info_woonplaats',
      CONCAT(key, 'street'), config_row->'customer_info_straatnaam',
      CONCAT(key, 'zipcode'), config_row->'customer_info_postcode'

    ) -- work around >100 keys
    || jsonb_build_object(
      -- Not relevant for an employee
      CONCAT(key, 'coc'), null,
      CONCAT(key, 'establishment_number'), null,
      CONCAT(key, 'type_of_business_entity'), null,
      CONCAT(key, 'trade_name'), null,
      CONCAT(key, 'has_correspondence_address'), null,
      CONCAT(key, 'foreign_residence_address_line1'), null,
      CONCAT(key, 'foreign_residence_address_line2'), null,
      CONCAT(key, 'foreign_residence_address_line3'), null,
      CONCAT(key, 'investigation'), null,
      CONCAT(key, 'a_number'), null,
      CONCAT(key, 'bsn'), null,
      CONCAT(key, 'family_name'), null,
      CONCAT(key, 'first_names'), null,
      CONCAT(key, 'gender'), null,
      CONCAT(key, 'salutation'), null,
      CONCAT(key, 'salutation1'), null,
      CONCAT(key, 'salutation2'), null,
      CONCAT(key, 'is_secret'), null,
      CONCAT(key, 'naamgebruik'), null,
      CONCAT(key, 'noble_title'), null,
      CONCAT(key, 'place_of_birth'), null,
      CONCAT(key, 'surname_prefix'), null,
      CONCAT(key, 'country_of_birth'), null,
      CONCAT(key, 'date_of_birth'), null,
      CONCAT(key, 'date_of_death'), null,
      CONCAT(key, 'date_of_divorce'), null,
      CONCAT(key, 'date_of_marriage'), null

    );

  END;
  $$;


--
-- Name: empty_subject_as_v0_json(text); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.empty_subject_as_v0_json(type text, OUT json jsonb) RETURNS jsonb
    LANGUAGE plpgsql
    AS $$
  DECLARE
    key text;

    field text;
    fields text[];

  BEGIN

    key := CONCAT('case.', type, '.');

    fields := ARRAY[
      'a_number',
      'bsn',
      'coc',
      'correspondence_house_number',
      'correspondence_place_of_residence',
      'correspondence_street',
      'correspondence_zipcode',
      'country_of_birth',
      'country_of_residence',
      'date_of_birth',
      'date_of_death',
      'date_of_divorce',
      'date_of_marriage',
      'department',
      'display_name',
      'email',
      'establishment_number',
      'family_name',
      'first_names',
      'foreign_residence_address_line1',
      'foreign_residence_address_line2',
      'foreign_residence_address_line3',
      'full_name',
      'gender',
      'has_correspondence_address',
      'house_number',
      'id',
      'initials',
      'investigation',
      'is_secret',
      'login',
      'mobile_number',
      'naamgebruik',
      'name',
      'noble_title',
      'password',
      'phone_number',
      'place_of_birth',
      'place_of_residence',
      'preferred_contact_channel',
      'residence_house_number',
      'residence_place_of_residence',
      'residence_street',
      'residence_zipcode',
      'salutation',
      'salutation1',
      'salutation2',
      'status',
      'street',
      'subject_type',
      'surname',
      'surname_prefix',
      'title',
      'trade_name',
      'type',
      'type_of_business_entity',
      'uuid',
      'zipcode'
    ];

    json := '{}'::jsonb;
    FOREACH field IN ARRAY fields
    LOOP
      SELECT INTO json json || jsonb_build_object(
        CONCAT(key, field), null
      );
    END LOOP;

  END;
  $$;


--
-- Name: generate_intials_for_np(text); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.generate_intials_for_np(name text, OUT initials text) RETURNS text
    LANGUAGE plpgsql
    AS $$
    DECLARE
      names TEXT[];
      i int;
    BEGIN

      IF name is NULL or LENGTH(name) <1
      THEN
        initials := '';
        RETURN;
      END IF;

      names := regexp_split_to_array(name, '\s+');

      initials := substr(names[1], 1, 1);
      IF array_upper(names, 1) > 2
      THEN
        FOR i in 2 ..array_length(names, 1)
        LOOP
          initials := concat(initials, '.', substr(names[i], 1, 1));
        END LOOP;
      END IF;

      initials := UPPER(concat(initials, '.', ''));
      RETURN;

    END $$;


--
-- Name: get_case_status_perc(integer, integer); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.get_case_status_perc(milestone integer, node integer, OUT percentage numeric) RETURNS numeric
    LANGUAGE plpgsql
    AS $$
  DECLARE
    status_count int;
  BEGIN

    select into status_count count(id) from zaaktype_status where zaaktype_node_id = node;

    IF status_count = 0
    THEN
      percentage := '0';
      RETURN;
    END IF;

    percentage := 100 * milestone / status_count;

  RETURN;
  END;
  $$;


--
-- Name: get_confidential_mapping(public.confidentiality); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.get_confidential_mapping(type public.confidentiality, OUT mapping jsonb) RETURNS jsonb
    LANGUAGE plpgsql
    AS $$
  DECLARE
    mapped text;
  BEGIN

    IF type = 'public'
    THEN
      mapped := 'Openbaar';
    ELSIF type = 'internal'
    THEN
      mapped := 'Intern';
    ELSIF type = 'confidential'
    THEN
      mapped := 'Geheim';
    ELSE
      RAISE EXCEPTION 'Unknown confidentiality type %', type;
    END IF;


    mapping := jsonb_build_object(
      'original', type,
      'mapped', mapped
    );

  END;
  $$;


--
-- Name: get_date_progress_from_case(public.hstore); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.get_date_progress_from_case(zaak public.hstore, OUT percentage text) RETURNS text
    LANGUAGE plpgsql
    AS $$
  DECLARE
    completion_date timestamp with time zone;
    completion_epoch int;
    start_epoch int;
    target_epoch int;

    current_difference int;
    max_difference int;

    perc int;
  BEGIN

    percentage := '';
    IF zaak->'status' = 'stalled'
    THEN
      RETURN;
    END IF;

    IF zaak->'afhandeldatum' IS NOT NULL
    THEN
      completion_date := (zaak->'afhandeldatum')::timestamp without time zone;
    ELSE
      SELECT INTO completion_date NOW()::timestamp without time zone;
    END IF;

    SELECT INTO completion_epoch date_part('epoch', completion_date);
    SELECT INTO start_epoch date_part('epoch', (zaak->'registratiedatum')::timestamp without time zone);

    current_difference := completion_epoch - start_epoch;

    SELECT INTO target_epoch date_part('epoch', (zaak->'streefafhandeldatum')::timestamp without time zone);

    max_difference := target_epoch - start_epoch;
    max_difference := GREATEST(max_difference, 1);

    perc := ROUND(100 * ( current_difference::numeric / max_difference::numeric ));
    percentage := perc::text;

    RETURN;
  END;
  $$;


--
-- Name: get_display_name_for_company(public.hstore); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.get_display_name_for_company(nnp public.hstore, OUT display_name text) RETURNS text
    LANGUAGE plpgsql
    AS $$
  DECLARE
  BEGIN

    display_name := nnp->'handelsnaam';
    RETURN;
  END;
  $$;


--
-- Name: get_display_name_for_employee(public.hstore); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.get_display_name_for_employee(emp public.hstore, OUT display_name text) RETURNS text
    LANGUAGE plpgsql
    AS $$
  DECLARE
  BEGIN

    display_name := (emp->'properties')::jsonb->>'displayname';
    RETURN;
  END;
  $$;


--
-- Name: get_display_name_for_person(public.hstore); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.get_display_name_for_person(np public.hstore, OUT display_name text) RETURNS text
    LANGUAGE plpgsql
    AS $$
  DECLARE
    initials text;
    surname text;
  BEGIN

    initials := np->'voorletters';
    surname := np->'naamgebruik';

    IF surname IS NULL
    THEN
        surname := np->'geslachtsnaam';
    END IF;

    IF initials = '' OR initials IS NULL
    THEN
      display_name := surname;
    ELSE
      display_name := concat(initials, ' ', surname);
    END IF;

  END;
  $$;


--
-- Name: get_payment_status_mapping(text); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.get_payment_status_mapping(type text, OUT mapping jsonb) RETURNS jsonb
    LANGUAGE plpgsql
    AS $$
  DECLARE
    mapped text;
  BEGIN

    IF type IS NULL
    THEN
      RETURN;
    ELSIF type = 'pending'
    THEN
      mapped := 'Wachten op bevestiging';
    ELSIF type = 'offline'
    THEN
      mapped := 'Later betalen';
    ELSIF type = 'failed'
    THEN
      mapped := 'Niet geslaagd';
    ELSIF type = 'success'
    THEN
      mapped := 'Geslaagd';
    ELSE
      RAISE EXCEPTION 'Unknown payment status type %', type;
    END IF;

    mapping := jsonb_build_object(
      'original', type,
      'mapped', mapped
    );

  END;
  $$;


--
-- Name: get_subject_by_legacy_id(text); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.get_subject_by_legacy_id(legacy_id text, OUT subject_uuid uuid) RETURNS uuid
    LANGUAGE plpgsql
    AS $$
    DECLARE
      betrokkene_id TEXT[];
      subject_type TEXT;
      subject_id INT;

    BEGIN
        betrokkene_id = string_to_array(legacy_id, '-');

        -- on our dev we have some weirdness going on
        -- unsure about live data
        IF array_length(betrokkene_id, 1) = 3
        THEN
          subject_type = betrokkene_id[2]::text;
          subject_id = betrokkene_id[3]::int;
        ELSE
          subject_type = betrokkene_id[4]::text;
          subject_id = betrokkene_id[5]::int;
        END IF;

        /* Employee or medewerker, work around a bug we have */
        IF subject_type = 'medewerker' OR subject_type = 'employee'
        THEN
          SELECT INTO subject_uuid uuid FROM subject where id = subject_id;
        ELSIF subject_type = 'natuurlijk_persoon'
        THEN
          SELECT INTO subject_uuid uuid FROM natuurlijk_persoon
          where id = subject_id;
        ELSIF subject_type = 'bedrijf'
        THEN
          SELECT INTO subject_uuid uuid FROM bedrijf where id = subject_id;
        END IF;

        RETURN;
    END $$;


--
-- Name: get_subject_by_uuid(uuid); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.get_subject_by_uuid(subject_uuid uuid, OUT subject_type text) RETURNS text
    LANGUAGE plpgsql
    AS $$
    DECLARE
      found uuid;
    BEGIN
          SELECT INTO found uuid FROM natuurlijk_persoon where uuid = subject_uuid;

          IF found IS NOT NULL
          THEN
            subject_type := 'person';
          END IF;

          SELECT INTO found uuid FROM bedrijf where uuid = subject_uuid;
          IF found is not null THEN
            subject_type := 'company';
          END IF;

          SELECT INTO found uuid FROM subject where uuid = subject_uuid
            AND subject.subject_type = 'employee';
          IF found is not null THEN
            subject_type := 'employee';
          END IF;

        RETURN;
    END $$;


--
-- Name: get_subject_display_name_by_uuid(uuid); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.get_subject_display_name_by_uuid(subject_uuid uuid, OUT display_name text) RETURNS text
    LANGUAGE plpgsql
    AS $$
    DECLARE
      found record;
      initials text;
      surname text;
    BEGIN
      SELECT INTO found * FROM natuurlijk_persoon where uuid = subject_uuid;

      IF found.uuid IS NOT NULL
      THEN

        SELECT INTO display_name get_display_name_for_person(hstore(found));
        RETURN;
      END IF;

      SELECT INTO found * FROM bedrijf where uuid = subject_uuid;
      IF found.uuid IS NOT NULL THEN
        SELECT INTO display_name get_display_name_for_company(hstore(found));
        RETURN;
      END IF;

      SELECT INTO found * FROM subject where uuid = subject_uuid
        AND subject.subject_type = 'employee';

      IF found.uuid IS NOT NULL THEN
        SELECT INTO display_name get_display_name_for_employee(hstore(found));
        RETURN;
      END IF;

      RETURN;
    END $$;


--
-- Name: hstore_to_timestamp(character varying); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.hstore_to_timestamp(date_field character varying) RETURNS timestamp without time zone
    LANGUAGE sql IMMUTABLE
    AS $_$















































  SELECT to_timestamp($1, 'YYYY-MM-DD"T"HH24:MI:SS')::TIMESTAMP;















































$_$;


--
-- Name: insert_file_timestamps(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.insert_file_timestamps() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
        BEGIN
            NEW.date_created = NOW() AT TIME ZONE 'UTC';
            NEW.date_modified = NOW() AT TIME ZONE 'UTC';
            RETURN NEW;
        END;
    $$;


--
-- Name: insert_timestamps(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.insert_timestamps() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
        BEGIN
            IF NEW.created is NULL
            THEN
                NEW.created = NOW();
            END IF;
            IF NEW.last_modified is NULL
            THEN
                NEW.last_modified = NOW();
            END IF;
            RETURN NEW;
        END;
    $$;


--
-- Name: is_destructable(timestamp with time zone); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.is_destructable(destruction_date timestamp with time zone, OUT destruction boolean) RETURNS boolean
    LANGUAGE plpgsql
    AS $$
  BEGIN


    IF destruction_date IS NOT NULL AND destruction_date < NOW()
    THEN
      destruction := true;
    ELSE
      destruction := false;
    END IF;
  END;
  $$;


--
-- Name: legal_entity_code_to_json(public.hstore); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.legal_entity_code_to_json(unit public.hstore, OUT unit_json jsonb) RETURNS jsonb
    LANGUAGE plpgsql
    AS $$
  BEGIN
      SELECT INTO unit_json json_build_object(
        'reference', unit->'uuid',
        'preview', unit->'label',
        'type', 'legal_entity_type',
        'instance', json_build_object(
          'label', unit->'label',
          'code', unit->'code',
          'active', (unit->'active')::boolean,
          'description', null
        )
      );
      RETURN;
  END;
  $$;


--
-- Name: municipality_code_to_json(public.hstore); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.municipality_code_to_json(unit public.hstore, OUT unit_json jsonb) RETURNS jsonb
    LANGUAGE plpgsql
    AS $$
  BEGIN
      SELECT INTO unit_json json_build_object(
        'reference', unit->'uuid',
        'preview', unit->'label',
        'type', 'municipality_code',
        'instance', json_build_object(
          'label', unit->'label',
          'dutch_code', lpad(unit->'dutch_code', 4, '0'),
          'alternative_name', unit->'alternative_name',
          'date_created', NOW(),
          'date_modified', NOW()
        )
      );
      RETURN;
  END;
  $$;


--
-- Name: org_unit(public.hstore, text); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.org_unit(org public.hstore, type text, OUT org_json jsonb) RETURNS jsonb
    LANGUAGE plpgsql
    AS $$
  DECLARE
    org_instance jsonb;
    org_id text;
  BEGIN

    IF type = 'role'
    THEN
      org_id := 'role_id';
    ELSE
      org_id := 'group_id';
    END IF;


      SELECT INTO org_instance jsonb_build_object(
        'name', org->'name',
        'description', org->'description',
        'date_modified', timestamp_to_perl_datetime((org->'date_modified')::timestamp with time zone),
        'date_created', timestamp_to_perl_datetime((org->'date_created')::timestamp with time zone)
      );

      IF type = 'role' THEN
        SELECT INTO org_instance org_instance || jsonb_build_object(
          'system_role', (org->'system_role')::boolean
        );
        SELECT INTO org_instance org_instance || jsonb_build_object(
          'role_id', (org->'id')::int
        );
      ELSE
        SELECT INTO org_instance org_instance || jsonb_build_object(
          'group_id', (org->'id')::int
        );
      END IF;

      SELECT INTO org_json json_build_object(
        'reference', org->'uuid',
        'preview', org->'name',
        'type', type,
        'instance', org_instance
      );

      RETURN;
  END;
  $$;


--
-- Name: person_as_v0_json(public.hstore, text); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.person_as_v0_json(np public.hstore, type text, OUT json jsonb) RETURNS jsonb
    LANGUAGE plpgsql
    AS $$
  DECLARE
    key text;
    gender text;
    salutation text;
    salutation1 text;
    salutation2 text;

    address_inactive text;
    address_active text;
    house_number text;

    investigation boolean;
    active boolean;
    tmp text;
    bid text;

  BEGIN

    key := CONCAT('case.', type, '.');

    investigation := false;

    FOREACH tmp IN ARRAY ARRAY['persoon', 'huwelijk', 'overlijden', 'verblijfplaats']
    LOOP
      tmp := CONCAT('onderzoek_', tmp);
      IF (np->tmp)::boolean = true THEN
        investigation := true;
        EXIT;
      END IF;
    END LOOP;

    gender := LOWER(np->'geslachtsaanduiding');

    IF gender = 'm'
    THEN
      gender := 'man';
      salutation := 'meneer';
      salutation1 := 'heer';
      salutation2 := 'de heer';
    ELSIF gender = 'v'
    THEN
      gender := 'vrouw';
      salutation := 'mevrouw';
      salutation1 := 'heer';
      salutation2 := 'de heer';
    ELSE
      gender := '';
      salutation := '';
      salutation1 := '';
      salutation2 := '';
    END IF;

    house_number := complete_house_number(
      np->'street_number',
      np->'street_number_letter',
      np->'street_number_suffix'
    );

    SELECT INTO json jsonb_build_object(

      /* This is so wrong on so many levels:
       *
       * This needs to be NULL if the country_code = 6030 This can only
       * be used when address_type = 'W'. Yet, we allow 'B' and
       * incorrect data. For now, respect the old incorrect API.
       */
      CONCAT(key, 'foreign_residence_address_line1'), np->'foreign_address_line_1',
      CONCAT(key, 'foreign_residence_address_line2'), np->'foreign_address_line_2',
      CONCAT(key, 'foreign_residence_address_line3'), np->'foreign_address_line_3',

      /* This is also wrong.
       *
       * This needs to be NULL if the country_code != 6030
       */
      CONCAT(key, 'street'), np->'street',
      CONCAT(key, 'zipcode'), np->'zipcode',
      CONCAT(key, 'place_of_residence'), np->'city',
      CONCAT(key, 'house_number'), house_number
    );

    IF np->'address_type' = 'W' THEN
      address_active = 'residence';
      address_inactive = 'correspondence';
    ELSE
      address_inactive = 'residence';
      address_active = 'correspondence';
    END IF;

    SELECT INTO json json || jsonb_build_object(
      CONCAT(key, address_active, '_house_number'), house_number,
      CONCAT(key, address_active, '_place_of_residence'), np->'city',
      CONCAT(key, address_active, '_street'), np->'street',
      CONCAT(key, address_active, '_zipcode'), np->'zipcode',

      CONCAT(key, address_inactive, '_house_number'), '',
      CONCAT(key, address_inactive, '_place_of_residence'), null,
      CONCAT(key, address_inactive, '_street'), null,
      CONCAT(key, address_inactive, '_zipcode'), null
    );

    -- active does not exists in the gm_ table of NP
    -- and is always active, since well, you can't create cases with
    -- inactive people
    active := true;
    IF np->'active' IS NOT NULL
    THEN
      active := (np->'active')::boolean;
    END IF;

    IF np->'gegevens_magazijn_id' IS NOT NULL
    THEN
      bid := CONCAT('betrokkene-natuurlijk_persoon-', np->'gegevens_magazijn_id');
    ELSE
      bid := CONCAT('betrokkene-natuurlijk_persoon-', np->'id');
    END IF;

    SELECT INTO json json || jsonb_build_object(
      type, np->'uuid',
      CONCAT(key, 'type'), 'Natuurlijk persoon',
      CONCAT(key, 'subject_type'), 'natuurlijk_persoon',

      CONCAT(key, 'status'), CASE WHEN active = true THEN 'Actief' ELSE 'Inactief' END,
      CONCAT(key, 'id'), bid,

      CONCAT(key, 'uuid'), np->'uuid',
      CONCAT(key, 'a_number'), np->'a_nummer',
      CONCAT(key, 'bsn'), np->'burgerservicenummer',

      CONCAT(key, 'family_name'), np->'geslachtsnaam',
      CONCAT(key, 'first_names'), np->'voornamen',
      CONCAT(key, 'surname'), np->'naamgebruik',
      CONCAT(key, 'full_name'), CONCAT(np->'voorletters', ' ', np->'naamgebruik'),
      CONCAT(key, 'display_name'), CONCAT(np->'voorletters', ' ', np->'naamgebruik'),
      CONCAT(key, 'gender'), gender,
      CONCAT(key, 'salutation'), salutation,
      CONCAT(key, 'salutation1'), salutation1,
      CONCAT(key, 'salutation2'), salutation2,

      CONCAT(key, 'initials'), np->'voorletters',
      CONCAT(key, 'is_secret'), np->'indicatie_geheim',
      CONCAT(key, 'naamgebruik'), np->'naamgebruik',
      CONCAT(key, 'name'), CONCAT(np->'voorletters', ' ', np->'naamgebruik'),
      CONCAT(key, 'noble_title'), np->'adellijke_titel',
      CONCAT(key, 'place_of_birth'), np->'geboorteplaats',
      CONCAT(key, 'preferred_contact_channel'), np->'preferred_contact_channel',

      CONCAT(key, 'investigation'), investigation,
      CONCAT(key, 'surname_prefix'), np->'voorvoegsel',

      CONCAT(key, 'country_of_birth'), np->'geboorteland',
      CONCAT(key, 'country_of_residence'), np->'country',
      CONCAT(key, 'date_of_birth'), (np->'geboortedatum')::date,
      CONCAT(key, 'date_of_death'), (np->'overlijdensdatum')::date,
      CONCAT(key, 'date_of_divorce'), (np->'datum_huwelijk_ontbinding')::date,
      CONCAT(key, 'date_of_marriage'), (np->'datum_huwelijk')::date,

      CONCAT(key, 'email'), np->'email_address',
      CONCAT(key, 'mobile_number'), np->'mobile',
      CONCAT(key, 'phone_number'), np->'phone_number',
      CONCAT(key, 'has_correspondence_address'), CASE WHEN np->'address_type' = 'W' THEN false ELSE true END,

      -- Not relevant for NP
      CONCAT(key, 'coc'), null,
      CONCAT(key, 'department'), null,
      CONCAT(key, 'establishment_number'), null,
      CONCAT(key, 'login'), null,
      CONCAT(key, 'title'), null,
      CONCAT(key, 'trade_name'), null,
      CONCAT(key, 'type_of_business_entity'), null,
      CONCAT(key, 'password'), null
    );

  END;
  $$;


--
-- Name: position_matrix(integer[], integer[]); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.position_matrix(groups integer[], roles integer[], OUT json jsonb) RETURNS jsonb
    LANGUAGE plpgsql
    AS $$
  DECLARE
    role_id int;
    group_id int;

    gr jsonb;
    role json;

    pos jsonb;
  BEGIN

      pos := '[]'::jsonb; -- || does nothing when value is NULL

      FOREACH group_id IN ARRAY groups
      LOOP
        SELECT INTO gr v1_json FROM groups WHERE id = group_id;
        FOREACH role_id IN ARRAY roles
        LOOP
          SELECT INTO role v1_json FROM roles WHERE id = role_id;
          SELECT INTO pos pos || jsonb_build_object(
            'preview', 'position(unsynched)',
            'reference', NULL,
            'type', 'position',
            'instance', json_build_object(
              'date_modified', timestamp_to_perl_datetime(NOW()),
              'date_created', timestamp_to_perl_datetime(NOW()),
              'group', gr,
              'role', role
            )
          );
        END LOOP;
      END LOOP;

      SELECT INTO json json_build_object(
        'type', 'set',
        'reference', null,
        'instance', json_build_object(
          'pager', null,
          'rows', pos
        )
      );
  END;
  $$;


--
-- Name: refresh_casetype_document_id_map(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.refresh_casetype_document_id_map() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
        BEGIN
            REFRESH MATERIALIZED VIEW zaaktype_document_kenmerken_map;
            RETURN NULL;
        END
    $$;


--
-- Name: refresh_casetype_end_status(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.refresh_casetype_end_status() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
  BEGIN
    REFRESH MATERIALIZED VIEW casetype_end_status;
    RETURN NULL;
  END
  $$;


--
-- Name: refresh_casetype_v1_reference(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.refresh_casetype_v1_reference() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
  BEGIN
    REFRESH MATERIALIZED VIEW casetype_v1_reference;
    RETURN NULL;
  END
  $$;


--
-- Name: refresh_subject_position_matrix(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.refresh_subject_position_matrix() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    BEGIN
        REFRESH MATERIALIZED VIEW CONCURRENTLY subject_position_matrix;
        RETURN NULL;
    END
    $$;


--
-- Name: set_confidential_on_case(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.set_confidential_on_case() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
  BEGIN
    IF NEW.confidentiality IS NOT NULL
    THEN
      IF NEW.confidentiality = 'confidential'
      THEN
        NEW.confidential = true;
      ELSE
        NEW.confidential = false;
      END IF;
    END IF;
    RETURN NEW;
  END
  $$;


--
-- Name: set_leading_qitem(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.set_leading_qitem() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
  DECLARE
    counter int;
    case_uuid uuid;
    case_id int;
    json_no jsonb;
    json_uuid jsonb;
  BEGIN

    IF NEW.type = 'touch_case'
    THEN
      case_uuid := NEW.data::jsonb->>'case_object_id';
      case_id := NEW.data::jsonb->>'case_number';
      json_no := '{}'::jsonb;
      json_uuid := '{}'::jsonb;

      if case_id is not null
      then
        select into case_uuid uuid from zaak where id = case_id;
      else
        select into case_id id from zaak where uuid = case_uuid;
      end if;

      json_no   := json_build_object('case_number',    case_id::text);
      json_uuid := json_build_object('case_object_id', case_uuid);

      UPDATE queue set status = 'cancelled' WHERE type = 'touch_case'
        AND priority < NEW.priority
        AND status in ('pending', 'waiting')
        AND (
          data::jsonb @> json_no
          OR
          data::jsonb @> json_uuid
        );

      SELECT INTO counter count(id) FROM queue WHERE type = 'touch_case'
        AND priority >= NEW.priority
        AND status in ('pending', 'waiting')
        AND (
          data::jsonb @> json_no
          OR
          data::jsonb @> json_uuid
        );

      IF counter > 0
      THEN
        NEW.status := 'cancelled';
      END IF;

    END IF;

    return NEW;

  END $$;


--
-- Name: subject_company_json(public.hstore); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.subject_company_json(subject public.hstore, OUT json jsonb) RETURNS jsonb
    LANGUAGE plpgsql
    AS $$
  DECLARE

    address_cor json;
    address_res json;

    latitude float;
    longitude float;

  BEGIN

    SELECT INTO json subject_json(subject, 'company');

    latitude := ((subject->'vestiging_latlong')::point)[0];
    longitude := ((subject->'vestiging_latlong')::point)[1];

    IF (
        subject->'vestiging_adres_buitenland1' IS NOT NULL
        AND
        subject->'vestiging_adres_buitenland2' IS NOT NULL
        AND
        subject->'vestiging_adres_buitenland3' IS NOT NULL
        )
      OR (subject->'vestiging_straatnaam' IS NOT NULL
          AND subject->'vestiging_postcode' IS NOT NULL
      )
    THEN
      address_res := json_build_object(
        'preview', 'address(unsynched)',
        'reference', null,
        'type', 'address',
        'instance', json_build_object(
          'bag_id', (subject->'vestiging_bag_id')::bigint,
          'city', subject->'vestiging_woonplaats',
          'street', subject->'vestiging_straatnaam',
          'street_number', (subject->'vestiging_huisnummer')::int,
          'street_number_suffix', subject->'vestiging_huisnummertoevoeging',
          'street_number_letter', subject->'vestiging_huisletter',
          'foreign_address_line1', subject->'vestiging_adres_buitenland1',
          'foreign_address_line2', subject->'vestiging_adres_buitenland2',
          'foreign_address_line3', subject->'vestiging_adres_buitenland3',
          'country', (subject->'vestiging_country')::jsonb,
          'zipcode', subject->'vestiging_postcode',
          'latitude', latitude,
          'longitude', longitude,
          -- non-existent data
          'municipality', null,
          'date_created', timestamp_to_perl_datetime(NOW()),
          'date_modified', timestamp_to_perl_datetime(NOW())
        )
      );
    END IF;

    IF (
        subject->'correspondentie_adres_buitenland1' IS NOT NULL
        AND
        subject->'correspondentie_adres_buitenland2' IS NOT NULL
        AND
        subject->'correspondentie_adres_buitenland3' IS NOT NULL
        )
      OR (subject->'correspondentie_straatnaam' IS NOT NULL
          AND subject->'correspondentie_postcode' IS NOT NULL
      )
    THEN
      address_cor := json_build_object(
        'preview', 'address(unsynched)',
        'reference', null,
        'type', 'address',
        'instance', json_build_object(
          'bag_id', (subject->'correspondentie_bag_id')::bigint,
          'city', subject->'correspondentie_woonplaats',
          'street', subject->'correspondentie_straatnaam',
          'street_number', (subject->'correspondentie_huisnummer')::int,
          'street_number_suffix', subject->'correspondentie_huisnummertoevoeging',
          'street_number_letter', subject->'correspondentie_huisletter',
          'foreign_address_line1', subject->'correspondentie_adres_buitenland1',
          'foreign_address_line2', subject->'correspondentie_adres_buitenland2',
          'foreign_address_line3', subject->'correspondentie_adres_buitenland3',
          'zipcode', subject->'correspondentie_postcode',
          'country', (subject->'correspondentie_country')::jsonb,
          'latitude', null,
          'longitude', null,
          -- non-existent data
          'municipality', null,
          'date_created', timestamp_to_perl_datetime(NOW()),
          'date_modified', timestamp_to_perl_datetime(NOW())
        )
      );
    END IF;

    IF subject->'main_activity' = '{}'
    THEN
      SELECT INTO subject subject || hstore('main_activity', null);
    END IF;

    IF subject->'secondairy_activities' = '[]'
    THEN
      SELECT INTO subject subject || hstore('secondairy_activities', null);
    END IF;

    SELECT INTO json json || jsonb_build_object(
      'instance', json_build_object(
        'address_residence', address_res,
        'address_correspondence', address_cor,
        'email_address', subject->'email',
        'phone_number', subject->'phone_number',
        'mobile_phone_number', subject->'mobile',
        'coc_number', lpad(subject->'dossiernummer', 8, '0'),
        'coc_location_number', lpad(subject->'vestigingsnummer', 12, '0'),
        'date_ceased', (subject->'date_ceased')::date,
        'date_founded', (subject->'date_founded')::date,
        'date_registration', (subject->'date_registration')::date,
        'main_activity', (subject->'main_activity')::jsonb,
        'secondairy_activities', (subject->'secondairy_activities')::jsonb,
        'company_type', (subject->'company_type')::jsonb,
        'oin', (subject->'oin')::bigint,
        'rsin', (subject->'rsin')::bigint,
        'company', (subject->'handelsnaam')::text,
        -- non-existent data
        'date_created', timestamp_to_perl_datetime(NOW()),
        'date_modified', timestamp_to_perl_datetime(NOW())
      )
    );

    RETURN;
  END;
  $$;


--
-- Name: subject_employee_json(public.hstore); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.subject_employee_json(subject public.hstore, OUT json jsonb) RETURNS jsonb
    LANGUAGE plpgsql
    AS $$
  DECLARE
    properties jsonb;
    preview text;
    positions jsonb;

  BEGIN
      properties := (subject->'properties')::jsonb;

      SELECT INTO json subject_json(subject, 'employee');

      SELECT INTO positions position_matrix(
        COALESCE((subject->'group_ids')::int[], '{}'::int[]),
        COALESCE((subject->'role_ids')::int[], '{}'::int[])
      );

      SELECT INTO json json || jsonb_build_object(
        'instance', json_build_object(
          'username', subject->'username',
          'initials', properties->'initials',
          'first_names', properties->'givenname',
          'surname', properties->'sn',
          'display_name', properties->'displayname',
          'email_address', properties->'mail',
          'phone_number', properties->'telephonenumber',
          'settings', (subject->'settings')::jsonb,
          'positions', positions,
          'date_modified', timestamp_to_perl_datetime((subject->'last_modified')::timestamp with time zone),
          'date_created', timestamp_to_perl_datetime((subject->'last_modified')::timestamp with time zone)
        )
      );
      RETURN;
  END;
  $$;


--
-- Name: subject_json(public.hstore, text); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.subject_json(subject public.hstore, s_type text, OUT json jsonb) RETURNS jsonb
    LANGUAGE plpgsql
    AS $$
  DECLARE
    preview text;
  BEGIN
      -- uuid == 36 long, need last 6 chars, 31, 36
      SELECT INTO preview substring(subject->'uuid', 31, 36);

      SELECT INTO json jsonb_build_object(
        'reference', subject->'uuid',
        'preview', s_type || '(...' || preview || ')',
        'type', s_type
      );
      RETURN;
  END;
  $$;


--
-- Name: subject_person_json(public.hstore); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.subject_person_json(subject public.hstore, OUT json jsonb) RETURNS jsonb
    LANGUAGE plpgsql
    AS $$
  DECLARE

    address json;
    address_cor json;
    address_res json;

    partner json;
    surname text;

  BEGIN

    SELECT INTO json subject_json(subject, 'person');

    address := json_build_object(
      'preview', 'address(unsynched)',
      'reference', null,
      'type', 'address',
      'instance', json_build_object(
        'bag_id', subject->'bag_id',
        'city', subject->'city',
        'street', subject->'street',
        'street_number', (subject->'street_number')::int,
        'street_number_suffix', subject->'street_number_suffix',
        'street_number_letter', subject->'street_number_letter',
        'foreign_address_line1', subject->'foreign_address_line1',
        'foreign_address_line2', subject->'foreign_address_line2',
        'foreign_address_line3', subject->'foreign_address_line3',
        'zipcode', subject->'zipcode',
        'country', (subject->'country')::jsonb,
        'municipality', (subject->'municipality')::jsonb,
        'latitude', (subject->'latitude')::float,
        'longitude', (subject->'longitude')::float,
        'date_created', timestamp_to_perl_datetime(NOW()),
        'date_modified', timestamp_to_perl_datetime(NOW())
      )
    );

    IF subject->'address_type' = 'W' THEN
          address_res := address;
    ELSE
          address_cor := address;
    END IF;

    IF   subject->'partner_a_nummer' IS NOT NULL
      OR subject->'partner_burgerservicenummer' IS NOT NULL
      OR subject->'partner_geslachtsnaam' IS NOT NULL
      OR subject->'partner_voorvoegsel' IS NOT NULL
    THEN

      IF subject->'partner_voorvoegsel' IS NOT NULL
      THEN
        surname := CONCAT(subject->'partner_voorvoegsel', ' ', subject->'partner_geslachtsnaam');
      ELSE
        surname := subject->'partner_geslachtsnaam';
      END IF;

      SELECT INTO partner json_build_object(
        'instance', json_build_object(
          'surname', surname,
          'family_name' , subject->'partner_geslachtsnaam',
          'prefix', subject->'partner_voorvoegsel',
          'personal_number', lpad(subject->'partner_burgerservicenummer', 9, '0'),
          'personal_number_a', lpad(subject->'partner_a_nummer', 10, '0'),
          -- We have no information on these items
          'first_names' , null,
          'use_of_name', null,
          'is_local_resident', false,
          'gender', null,
          'noble_title', null,
          'place_of_birth', null,
          'initials', null,
          'date_of_birth', null,
          'is_secret', false,
          'email_address', null,
          'mobile_phone_number', null,
          'address_correspondence', null,
          'address_residence', null,
          'date_of_death', null,
          'phone_number', null,
          'partner', null,
          'date_created', timestamp_to_perl_datetime(NOW()),
          'date_modified', timestamp_to_perl_datetime(NOW())
        ),
        'type', 'person',
        'preview', 'person(unsynched)',
        'reference', null
      );
    END IF;

    SELECT INTO json json || jsonb_build_object(
      'instance', json_build_object(
        'address_residence', address_res,
        'address_correspondence', address_cor,
        'email_address', subject->'email_address',
        'phone_number', subject->'phone_number',
        'mobile_phone_number', subject->'mobile',
        'partner', partner,
        -- non-existent data
        'date_created', timestamp_to_perl_datetime(NOW()), -- np table?
        'date_modified', timestamp_to_perl_datetime(NOW()), -- np table?
        -- we have it
        'date_of_birth', (subject->'geboortedatum')::date,
        'date_of_death', (subject->'datum_overlijden')::date,
        'family_name', subject->'geslachtsnaam',
        'first_names', subject->'voornamen',
        'gender', subject->'geslachtsaanduiding',
        'initials', subject->'voorletters',
        'is_local_resident', (subject->'in_gemeente')::boolean,
        'is_secret', CASE WHEN subject->'indicatie_geheim' = '0' THEN false ELSE true END,
        'noble_title', subject->'adellijke_titel',
        'personal_number', lpad(subject->'burgerservicenummer', 9, '0'),
        'personal_number_a', lpad(subject->'a_nummer', 10, '0'),
        'place_of_birth', subject->'geboorteplaats',
        'prefix', subject->'aanhef_aanschrijving',
        'surname', COALESCE(subject->'naamgebruik', subject->'geslachtsnaam'),
        'use_of_name', subject->'aanduiding_naamgebruik'
      )
    );

    RETURN;
  END;
  $$;


--
-- Name: sync_zaak_betrokkenen_cache(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.sync_zaak_betrokkenen_cache() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
    affected RECORD;
BEGIN
    IF TG_OP = 'INSERT' OR TG_OP = 'UPDATE' THEN
        affected := NEW;
    ELSIF TG_OP = 'DELETE' THEN
        affected := OLD;
    END IF;

    IF affected.zaak_id IS NULL THEN
        -- Betrokkene without a case. Weird situation.
        RETURN affected;
    END IF;

    UPDATE
        zaak
    SET
        betrokkenen_cache = q.betrokkenen
    FROM (
        SELECT
            z.id AS zaak_id,
            json_object_agg(r.uuid, jsonb_build_object('roles', r.roles, 'pip_authorized', r.authorized)) AS betrokkenen
        FROM
            zaak z,
            (
                SELECT
                    zb.zaak_id,
                    zb.subject_id AS uuid,
                    json_object_agg(zb.id, jsonb_build_object('pip_authorized', zb.pip_authorized, 'authorisation', zb.authorisation, 'role', coalesce(zb.rol, CASE WHEN zb.id = z.aanvrager THEN
                                    'Aanvrager'
                                WHEN zb.id = z.behandelaar THEN
                                    'Behandelaar'
                                WHEN zb.id = z.coordinator THEN
                                    'Coordinator'
                                ELSE
                                    'Onbekende rol'
                                END))) AS roles,
                    sum(
                        CASE WHEN zb.pip_authorized THEN
                            1
                        ELSE
                            0
                        END)::int::boolean AS authorized
                FROM
                    zaak_betrokkenen zb
                    JOIN zaak z ON zb.zaak_id = z.id
                WHERE
                    zb.deleted IS NULL
                GROUP BY
                    zb.subject_id,
                    zb.zaak_id) AS r
            WHERE
                r.zaak_id = z.id
                AND r.uuid IS NOT NULL
            GROUP BY
                z.id) AS q
        WHERE (q.zaak_id = zaak.id AND zaak.id = affected.zaak_id);
    RETURN affected;
END;
$$;


--
-- Name: timestamp_to_perl_datetime(timestamp with time zone); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.timestamp_to_perl_datetime(dt timestamp with time zone, OUT tt text) RETURNS text
    LANGUAGE plpgsql
    AS $$
  BEGIN
    SELECT INTO tt to_char(dt::timestamp with time zone at time zone 'UTC', 'YYYY-MM-DD"T"HH24:MI:SS"Z"');
  END;
  $$;


--
-- Name: update_aanvrager_type(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.update_aanvrager_type() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
    IF NEW.aanvrager IS NOT NULL THEN
        SELECT
            betrokkene_type
        FROM
            zaak_betrokkenen
        WHERE
            id = NEW.aanvrager INTO NEW.aanvrager_type;
    ELSE
        NEW.aanvrager_type := NULL;
    END IF;
    RETURN NEW;
END;
$$;


--
-- Name: update_display_name_for_file(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.update_display_name_for_file() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
  DECLARE
    subject_uuid uuid;
    dn text;
  BEGIN
      IF NEW.created_by IS NOT NULL
      THEN

        IF NEW.created_by NOT LIKE 'betrokkene-%'
        THEN
            NEW.created_by_display_name := NEW.created_by;
            RETURN NEW;
        END IF;

        SELECT INTO subject_uuid get_subject_by_legacy_id(NEW.created_by);

        IF subject_uuid IS NULL
        THEN
            NEW.created_by_display_name := '<Onbekend>';
            RETURN NEW;
        END IF;

        SELECT INTO dn get_subject_display_name_by_uuid(subject_uuid);
        IF dn IS NOT NULL
        THEN
            NEW.created_by_display_name := dn;
          RETURN NEW;
        END IF;
      END IF;

      RETURN NEW;
  END $$;


--
-- Name: update_file_timestamps(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.update_file_timestamps() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
        BEGIN
            NEW.date_modified = NOW() AT TIME ZONE 'UTC';
            RETURN NEW;
        END;
    $$;


--
-- Name: update_group_json(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.update_group_json() RETURNS trigger
    LANGUAGE plpgsql
    AS $$

    DECLARE
      j_group JSONB;
    BEGIN

      SELECT INTO j_group org_unit(hstore(NEW), 'group');
      NEW.v1_json := j_group;
      RETURN NEW;
    END;
  $$;


--
-- Name: update_name_cache_for_logging(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.update_name_cache_for_logging() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
  DECLARE
    subject_uuid uuid;
    dn text;
  BEGIN
      IF NEW.created_by IS NOT NULL AND NEW.created_by_name_cache IS NULL
      THEN

        IF NEW.created_by NOT LIKE 'betrokkene-%'
        THEN
            NEW.created_by_name_cache := NEW.created_by;
            NEW.created_by := NULL;
            RETURN NEW;
        END IF;

        SELECT INTO subject_uuid get_subject_by_legacy_id(NEW.created_by);

        IF subject_uuid IS NULL
        THEN
            NEW.created_by_name_cache := '<onbekend>';
            RETURN NEW;
        END IF;

        SELECT INTO dn get_subject_display_name_by_uuid(subject_uuid);
        IF dn IS NOT NULL
        THEN
            NEW.created_by_name_cache := dn;
          RETURN NEW;
        END IF;
      END IF;

      RETURN NEW;
  END $$;


--
-- Name: update_requestor_json(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.update_requestor_json() RETURNS trigger
    LANGUAGE plpgsql
    AS $$

    BEGIN

      IF TG_OP = 'INSERT' OR (TG_OP = 'UPDATE' AND NEW.aanvrager != OLD.aanvrager) THEN
        SELECT INTO NEW.requestor_v1_json v1_json FROM zaak_betrokkenen zb WHERE zb.id = NEW.aanvrager AND zb.zaak_id = NEW.id;
      END IF;

      IF TG_OP = 'INSERT' OR (TG_OP = 'UPDATE' AND NEW.behandelaar != OLD.behandelaar) THEN

        IF NEW.behandelaar IS NOT NULL
        THEN
          SELECT INTO NEW.assignee_v1_json v1_json FROM zaak_betrokkenen zb WHERE zb.id = NEW.behandelaar AND zb.zaak_id = NEW.id;
        ELSE
          NEW.assignee_v1_json := '{}';
        END IF;
      END IF;

      IF TG_OP = 'INSERT' OR (TG_OP = 'UPDATE' AND NEW.coordinator != OLD.coordinator) THEN

        IF NEW.coordinator IS NOT NULL
        THEN
          SELECT INTO NEW.coordinator_v1_json v1_json FROM zaak_betrokkenen zb WHERE zb.id = NEW.coordinator AND zb.zaak_id = NEW.id;
        ELSE
          NEW.coordinator_v1_json := '{}';
        END IF;
      END IF;
      RETURN NEW;

    END;

  $$;


--
-- Name: update_role_json(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.update_role_json() RETURNS trigger
    LANGUAGE plpgsql
    AS $$

    DECLARE
      j_role JSONB;
    BEGIN

      SELECT INTO j_role org_unit(hstore(NEW), 'role');
      NEW.v1_json := j_role;
      RETURN NEW;
    END;
  $$;


--
-- Name: update_subject_json(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.update_subject_json() RETURNS trigger
    LANGUAGE plpgsql
    AS $$

    BEGIN

      IF TG_OP = 'INSERT'
      THEN
        SELECT INTO
          NEW.requestor_v1_json
          case_subject_json(hstore(zb))
        FROM
          zaak_betrokkenen zb
        WHERE
          -- zb does not have the zaak id yet on create..
          zb.id = NEW.aanvrager;

      ELSIF TG_OP = 'UPDATE' AND (NEW.requestor_v1_json IS NULL OR NEW.aanvrager != OLD.aanvrager)
      THEN
        SELECT INTO
          NEW.requestor_v1_json
          case_subject_json(hstore(zb))
        FROM
          zaak_betrokkenen zb
        WHERE
          -- zb does not have the zaak id yet on update, this is handled
          -- later on..
           zb.id = NEW.aanvrager;

      END IF;

      IF NEW.behandelaar IS NOT NULL
      THEN
        IF TG_OP = 'INSERT' OR (TG_OP = 'UPDATE' AND ( OLD.behandelaar IS NULL OR NEW.behandelaar != OLD.behandelaar))
        THEN
          SELECT INTO
            NEW.assignee_v1_json
            case_subject_json(hstore(zb))
          FROM
            zaak_betrokkenen zb
          WHERE
            zb.id = NEW.behandelaar AND zb.zaak_id = NEW.id;
        END IF;
      ELSE
        NEW.assignee_v1_json := NULL;
      END IF;


      IF NEW.coordinator IS NOT NULL
      THEN
        IF TG_OP = 'INSERT' OR (TG_OP = 'UPDATE' AND ( OLD.coordinator IS NULL OR NEW.coordinator != OLD.coordinator))
        THEN
          SELECT INTO
            NEW.coordinator_v1_json
            case_subject_json(hstore(zb))
          FROM
            zaak_betrokkenen zb
          WHERE
            zb.id = NEW.coordinator AND zb.zaak_id = NEW.id;
        END IF;
      ELSE
        NEW.coordinator_v1_json := NULL;
      END IF;

      RETURN NEW;

    END;

  $$;


--
-- Name: update_timestamps(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.update_timestamps() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
        BEGIN
            IF NEW.last_modified is NULL
            THEN
                NEW.last_modified = NOW();
            END IF;
            RETURN NEW;
        END;
    $$;


--
-- Name: update_zaak_percentage(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.update_zaak_percentage() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
  BEGIN

    SELECT INTO NEW.status_percentage get_case_status_perc(
      NEW.milestone,
      NEW.zaaktype_node_id
    );
    RETURN NEW;

  END
  $$;


--
-- Name: zaak_meta_update_unread_count(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.zaak_meta_update_unread_count() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    BEGIN
        UPDATE zaak_meta SET unread_communication_count = (SELECT GREATEST(SUM(unread_employee_count), 0) FROM thread WHERE case_id = NEW.case_id) WHERE zaak_id = NEW.case_id;

        RETURN NEW;
    END
$$;


--
-- Name: zaaktype_node_as_v0(public.hstore); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.zaaktype_node_as_v0(ztn public.hstore, OUT unit_json jsonb) RETURNS jsonb
    LANGUAGE plpgsql
    AS $$
  DECLARE
    properties JSONB;
  BEGIN

    properties := (ztn->'properties')::jsonb;

    SELECT INTO unit_json json_build_object(
      -- properties mapping
      'case.casetype.adjourn_period', properties->'verdagingstermijn',
      'case.casetype.archive_classification_code', properties->'archiefclassicatiecode',
      'case.casetype.designation_of_confidentiality', properties->'vertrouwelijkheidsaanduiding',
      'case.casetype.eform', properties->'e_formulier',
      'case.casetype.extension', properties->'verlenging_mogelijk',
      'case.casetype.extension_period', properties->'verlengingstermijn',
      'case.casetype.goal', properties->'doel',
      'case.casetype.lex_silencio_positivo', properties->'lex_silencio_positivo',
      'case.casetype.motivation', properties->'aanleiding',
      'case.casetype.objection_and_appeal', properties->'beroep_mogelijk',
      'case.casetype.penalty', properties->'wet_dwangsom',
      'case.casetype.price.counter', properties->'pdc_tarief_balie',
      'case.casetype.price.email', properties->'pdc_tarief_email',
      'case.casetype.price.employee', properties->'pdc_tarief_behandelaar',
      'case.casetype.price.post', properties->'pdc_tarief_post',
      'case.casetype.price.telephone', properties->'pdc_tarief_telefoon',
      'case.casetype.principle_local', properties->'lokale_grondslag',
      'case.casetype.publication', properties->'publicatie',
      'case.casetype.registration_bag', properties->'bag',
      'case.casetype.supervisor', properties->'verantwoordelijke',
      'case.casetype.supervisor_relation', properties->'verantwoordingsrelatie',
      'case.casetype.suspension', properties->'opschorten_mogelijk',
      'case.casetype.text_for_publication', properties->'publicatietekst',
      'case.casetype.wkpb', properties->'wkpb',
      -- ztn itself
      'case.casetype.version', (ztn->'version')::int,
      'case.casetype.description', ztn->'zaaktype_omschrijving',
      'case.casetype.name', ztn->'titel',
      'case.casetype.node.id', (ztn->'id')::int,
      'case.casetype.id', (ztn->'zaaktype_id')::int,
      'case.casetype.keywords', ztn->'zaaktype_trefwoorden',
      'case.casetype.identification', ztn->'code',
      'case.casetype.initiator_source', ztn->'trigger',
      'case.casetype.version_date_of_creation', (ztn->'created')::timestamp with time zone,
      -- how is this even possible?
      'case.casetype.version_date_of_expiration', (ztn->'deleted')::timestamp with time zone
    );

    RETURN;
  END;
  $$;


--
-- Name: zaaktype_node_v0_json(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.zaaktype_node_v0_json() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    BEGIN
      SELECT INTO NEW.v0_json zaaktype_node_as_v0(hstore(NEW));
      RETURN NEW;
    END;
  $$;


SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: adres; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.adres (
    id integer NOT NULL,
    straatnaam text,
    huisnummer bigint,
    huisletter character(1),
    huisnummertoevoeging text,
    nadere_aanduiding character varying(35),
    postcode character varying(6),
    woonplaats text,
    gemeentedeel text,
    functie_adres character(1) NOT NULL,
    datum_aanvang_bewoning date,
    woonplaats_id character varying(32),
    gemeente_code smallint,
    hash character varying(32),
    import_datum timestamp(6) without time zone,
    deleted_on timestamp(6) without time zone,
    adres_buitenland1 text,
    adres_buitenland2 text,
    adres_buitenland3 text,
    landcode integer DEFAULT 6030,
    natuurlijk_persoon_id integer,
    bag_id text,
    geo_lat_long point
);


--
-- Name: adres_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.adres_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: adres_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.adres_id_seq OWNED BY public.adres.id;


--
-- Name: alternative_authentication_activation_link; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.alternative_authentication_activation_link (
    token text NOT NULL,
    subject_id uuid NOT NULL,
    expires timestamp without time zone NOT NULL
);


--
-- Name: bag_cache; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.bag_cache (
    id integer NOT NULL,
    bag_type character varying NOT NULL,
    bag_id character(16) NOT NULL,
    bag_data jsonb NOT NULL,
    date_created timestamp without time zone DEFAULT timezone('UTC'::text, CURRENT_TIMESTAMP) NOT NULL,
    last_modified timestamp without time zone DEFAULT timezone('UTC'::text, CURRENT_TIMESTAMP) NOT NULL,
    refresh_after timestamp without time zone DEFAULT (timezone('UTC'::text, CURRENT_TIMESTAMP) + '24:00:00'::interval) NOT NULL,
    CONSTRAINT bag_cache_bag_type_check CHECK (((bag_type)::text = ANY (ARRAY[('ligplaats'::character varying)::text, ('nummeraanduiding'::character varying)::text, ('openbareruimte'::character varying)::text, ('pand'::character varying)::text, ('standplaats'::character varying)::text, ('verblijfsobject'::character varying)::text, ('woonplaats'::character varying)::text])))
);


--
-- Name: bag_cache_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

ALTER TABLE public.bag_cache ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.bag_cache_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: bag_ligplaats; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.bag_ligplaats (
    identificatie character varying(16) NOT NULL,
    begindatum character varying(14) NOT NULL,
    einddatum character varying(14),
    officieel character varying(1),
    status character varying(80) NOT NULL,
    hoofdadres character varying(16) NOT NULL,
    inonderzoek character varying(1) NOT NULL,
    documentdatum character varying(14),
    documentnummer character varying(20),
    correctie character varying(1),
    id integer NOT NULL
);


--
-- Name: TABLE bag_ligplaats; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.bag_ligplaats IS '55 : een ligplaats is een formeel door de gemeenteraad als zodanig aangewezen plaats in het water, al dan niet aangevuld met een op de oever aanwezig terrein of een gedeelte daarvan, dat bestemd is voor het permanent afmeren van een voor woon-, bedrijfsmatige- of recreatieve doeleinden geschikt vaartuig.';


--
-- Name: COLUMN bag_ligplaats.identificatie; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_ligplaats.identificatie IS '58.01 : de unieke aanduiding van een ligplaats.';


--
-- Name: COLUMN bag_ligplaats.begindatum; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_ligplaats.begindatum IS '58.91 : de begindatum van een periode waarin een of meer gegevens die worden bijgehouden over een ligplaats een wijziging hebben ondergaan.';


--
-- Name: COLUMN bag_ligplaats.einddatum; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_ligplaats.einddatum IS '58.92 : de einddatum van een periode waarin er geen wijzigingen hebben plaatsgevonden in de gegevens die worden bijgehouden over een ligplaats.';


--
-- Name: COLUMN bag_ligplaats.officieel; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_ligplaats.officieel IS '58.02 : een aanduiding waarmee kan worden aangegeven dat een object in de registratie is opgenomen als gevolg van een feitelijke constatering, zonder dat er op het moment van opname sprake is van een formele grondslag voor deze opname.';


--
-- Name: COLUMN bag_ligplaats.status; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_ligplaats.status IS '58.03 : de fase van de levenscyclus van een ligplaats, waarin de betreffende ligplaats zich bevindt.';


--
-- Name: COLUMN bag_ligplaats.hoofdadres; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_ligplaats.hoofdadres IS '58:10 : de identificatiecode nummeraanduiding waaronder het hoofdadres van een ligplaats, dat in het kader van de basis gebouwen registratie als zodanig is aangemerkt, is opgenomen in de basis registratie adressen.';


--
-- Name: COLUMN bag_ligplaats.inonderzoek; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_ligplaats.inonderzoek IS '58.93 : een aanduiding waarmee wordt aangegeven dat een onderzoek wordt uitgevoerd naar de juistheid van een of meerdere gegevens van het betreffende object.';


--
-- Name: COLUMN bag_ligplaats.documentdatum; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_ligplaats.documentdatum IS '58.97 : de datum waarop het brondocument is vastgesteld, op basis waarvan een opname, mutatie of een in de historie plaatsen van gegevens ten aanzien van een ligplaats heeft plaatsgevonden.';


--
-- Name: COLUMN bag_ligplaats.documentnummer; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_ligplaats.documentnummer IS '58.98 : de unieke aanduiding van het brondocument op basis waarvan een opname, mutatie of een verwijdering van gegevens ten aanzien van een ligplaats heeft plaatsgevonden, binnen een gemeente.';


--
-- Name: COLUMN bag_ligplaats.correctie; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_ligplaats.correctie IS 'het gegeven is gecorrigeerd.';


--
-- Name: bag_ligplaats_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.bag_ligplaats_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: bag_ligplaats_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.bag_ligplaats_id_seq OWNED BY public.bag_ligplaats.id;


--
-- Name: bag_ligplaats_nevenadres; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.bag_ligplaats_nevenadres (
    identificatie character varying(16) NOT NULL,
    begindatum character varying(14) NOT NULL,
    nevenadres character varying(16) NOT NULL,
    correctie character varying(1) NOT NULL
);


--
-- Name: TABLE bag_ligplaats_nevenadres; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.bag_ligplaats_nevenadres IS 'koppeltabel voor nevenadressen bij ligplaats';


--
-- Name: COLUMN bag_ligplaats_nevenadres.identificatie; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_ligplaats_nevenadres.identificatie IS '58.01 : de unieke aanduiding van een ligplaats.';


--
-- Name: COLUMN bag_ligplaats_nevenadres.begindatum; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_ligplaats_nevenadres.begindatum IS '58.91 : de begindatum van een periode waarin een of meer gegevens die worden bijgehouden over een ligplaats een wijziging hebben ondergaan.';


--
-- Name: COLUMN bag_ligplaats_nevenadres.nevenadres; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_ligplaats_nevenadres.nevenadres IS '58.11 : de identificatiecodes nummeraanduiding waaronder nevenadressen van een ligplaats, die in het kader van de basis gebouwen registratie als zodanig zijn aangemerkt, zijn opgenomen in de basis registratie adressen.';


--
-- Name: COLUMN bag_ligplaats_nevenadres.correctie; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_ligplaats_nevenadres.correctie IS 'het gegeven is gecorrigeerd.';


--
-- Name: bag_nummeraanduiding; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.bag_nummeraanduiding (
    identificatie character varying(16) NOT NULL,
    begindatum character varying(14) NOT NULL,
    einddatum character varying(14),
    huisnummer bigint NOT NULL,
    officieel character varying(1),
    huisletter character varying(1),
    huisnummertoevoeging text,
    postcode character varying(6),
    woonplaats character varying(4),
    inonderzoek character varying(1) NOT NULL,
    openbareruimte character varying(16) NOT NULL,
    type character varying(20) NOT NULL,
    documentdatum character varying(14),
    documentnummer character varying(20),
    status character varying(80) NOT NULL,
    correctie character varying(1),
    id integer NOT NULL,
    gps_lat_lon point
);


--
-- Name: TABLE bag_nummeraanduiding; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.bag_nummeraanduiding IS '11.2 : een nummeraanduiding is een door de gemeenteraad als zodanig toegekende aanduiding van een adresseerbaar object.';


--
-- Name: COLUMN bag_nummeraanduiding.identificatie; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_nummeraanduiding.identificatie IS '11.02 : de unieke aanduiding van een nummeraanduiding.';


--
-- Name: COLUMN bag_nummeraanduiding.begindatum; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_nummeraanduiding.begindatum IS '11.62 : de begindatum van een periode waarin een of meer gegevens die worden bijgehouden over een nummeraanduiding een wijziging hebben ondergaan.';


--
-- Name: COLUMN bag_nummeraanduiding.einddatum; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_nummeraanduiding.einddatum IS '11.63 : de einddatum van een periode waarin er geen wijzigingen hebben plaatsgevonden in de gegevens die worden bijgehouden over een nummeraanduiding.';


--
-- Name: COLUMN bag_nummeraanduiding.huisnummer; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_nummeraanduiding.huisnummer IS '11.20 : een door of namens het gemeentebestuur ten aanzien van een adresseerbaar object toegekende nummering.';


--
-- Name: COLUMN bag_nummeraanduiding.officieel; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_nummeraanduiding.officieel IS '11.21 : een aanduiding waarmee kan worden aangegeven dat een object in de registratie is opgenomen als gevolg van een feitelijke constatering, zonder dat er op het moment van opname sprake is van een formele grondslag voor deze opname.';


--
-- Name: COLUMN bag_nummeraanduiding.huisletter; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_nummeraanduiding.huisletter IS '11.30 : een door of namens het gemeentebestuur ten aanzien van  een adresseerbaar object toegekende toevoeging aan een huisnummer in de vorm van een alfanumeriek teken.';


--
-- Name: COLUMN bag_nummeraanduiding.huisnummertoevoeging; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_nummeraanduiding.huisnummertoevoeging IS '11.40 : een door of namens het gemeentebestuur ten aanzien van  een adresseerbaar object toegekende nadere toevoeging aan een huisnummer of een combinatie van huisnummer en huisletter.';


--
-- Name: COLUMN bag_nummeraanduiding.postcode; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_nummeraanduiding.postcode IS '11.60 : de door tnt post vastgestelde code behorende bij een bepaalde combinatie van een straatnaam en een huisnummer.';


--
-- Name: COLUMN bag_nummeraanduiding.woonplaats; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_nummeraanduiding.woonplaats IS '11.61 : unieke aanduiding van de woonplaats waarbinnen het object waaraan de nummeraanduiding is toegekend is gelegen.';


--
-- Name: COLUMN bag_nummeraanduiding.inonderzoek; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_nummeraanduiding.inonderzoek IS '11.64 : een aanduiding waarmee wordt aangegeven dat een onderzoek wordt uitgevoerd naar de juistheid van een of meerdere gegevens van het betreffende object.';


--
-- Name: COLUMN bag_nummeraanduiding.openbareruimte; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_nummeraanduiding.openbareruimte IS '11.65 : de unieke aanduiding van een openbare ruimte waaraan een adresseerbaar object is gelegen.';


--
-- Name: COLUMN bag_nummeraanduiding.type; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_nummeraanduiding.type IS '11.66 : de aard van een als zodanig benoemde nummeraanduiding.';


--
-- Name: COLUMN bag_nummeraanduiding.documentdatum; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_nummeraanduiding.documentdatum IS '11.67 : de datum waarop het brondocument is vastgesteld, op basis waarvan een opname, mutatie of een verwijdering van gegevens ten aanzien van een nummeraanduiding heeft plaatsgevonden.';


--
-- Name: COLUMN bag_nummeraanduiding.documentnummer; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_nummeraanduiding.documentnummer IS '11.68 : de unieke aanduiding van het brondocument op basis waarvan een opname, mutatie of een verwijdering van gegevens ten aanzien van een nummeraanduiding heeft plaatsgevonden, binnen een gemeente.';


--
-- Name: COLUMN bag_nummeraanduiding.status; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_nummeraanduiding.status IS '11.69 : de fase van de levenscyclus van een nummeraanduiding, waarin de betreffende nummeraanduiding zich bevindt.';


--
-- Name: COLUMN bag_nummeraanduiding.correctie; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_nummeraanduiding.correctie IS 'het gegeven is gecorrigeerd.';


--
-- Name: bag_nummeraanduiding_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.bag_nummeraanduiding_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: bag_nummeraanduiding_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.bag_nummeraanduiding_id_seq OWNED BY public.bag_nummeraanduiding.id;


--
-- Name: bag_openbareruimte; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.bag_openbareruimte (
    identificatie character varying(16) NOT NULL,
    begindatum character varying(14) NOT NULL,
    einddatum character varying(14),
    naam character varying(80) NOT NULL,
    officieel character varying(1),
    woonplaats character varying(4),
    type character varying(40) NOT NULL,
    inonderzoek character varying(1) NOT NULL,
    documentdatum character varying(14),
    documentnummer character varying(20),
    status character varying(80) NOT NULL,
    correctie character varying(1),
    id integer NOT NULL,
    gps_lat_lon point
);


--
-- Name: TABLE bag_openbareruimte; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.bag_openbareruimte IS '11.1 : een openbare ruimte is een door de gemeenteraad als zodanig aangewezen benaming van een binnen een woonplaats gelegen buitenruimte.';


--
-- Name: COLUMN bag_openbareruimte.identificatie; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_openbareruimte.identificatie IS '11.01 : de unieke aanduiding van een openbare ruimte.';


--
-- Name: COLUMN bag_openbareruimte.begindatum; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_openbareruimte.begindatum IS '11.12 : de begindatum van een periode waarin een of meer gegevens die worden bijgehouden over een openbare ruimte een wijziging hebben ondergaan.';


--
-- Name: COLUMN bag_openbareruimte.einddatum; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_openbareruimte.einddatum IS '11.13 : de einddatum van een periode waarin er geen wijzigingen hebben plaatsgevonden in de gegevens die worden bijgehouden over een openbare ruimte.';


--
-- Name: COLUMN bag_openbareruimte.naam; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_openbareruimte.naam IS '11.10 : een naam die aan een openbare ruimte is toegekend in een daartoe strekkend formeel gemeentelijk besluit.';


--
-- Name: COLUMN bag_openbareruimte.officieel; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_openbareruimte.officieel IS '11.11 : een aanduiding waarmee kan worden aangegeven dat een object in de registratie is opgenomen als gevolg van een feitelijke constatering, zonder dat er op het moment van opname sprake is van een formele grondslag voor deze opname.';


--
-- Name: COLUMN bag_openbareruimte.woonplaats; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_openbareruimte.woonplaats IS '11.15 : unieke aanduiding van de woonplaats waarbinnen een openbare ruimte is gelegen.';


--
-- Name: COLUMN bag_openbareruimte.type; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_openbareruimte.type IS '11.16 : de aard van de als zodanig benoemde openbare ruimte.';


--
-- Name: COLUMN bag_openbareruimte.inonderzoek; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_openbareruimte.inonderzoek IS '11.14 : een aanduiding waarmee wordt aangegeven dat een onderzoek wordt uitgevoerd naar de juistheid van een of meerdere gegevens van het betreffende object.';


--
-- Name: COLUMN bag_openbareruimte.documentdatum; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_openbareruimte.documentdatum IS '11.17 : de datum waarop het brondocument is vastgesteld, op basis waarvan een opname, mutatie of een verwijdering van gegevens ten aanzien van een openbare ruimte heeft plaatsgevonden.';


--
-- Name: COLUMN bag_openbareruimte.documentnummer; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_openbareruimte.documentnummer IS '11.18 : de unieke aanduiding van het brondocument op basis waarvan een opname, mutatie of een verwijdering van gegevens ten aanzien van een openbare ruimte heeft plaatsgevonden, binnen een gemeente.';


--
-- Name: COLUMN bag_openbareruimte.status; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_openbareruimte.status IS '11.19 : de fase van de levenscyclus van een openbare ruimte, waarin de betreffende openbare ruimte zich bevindt.';


--
-- Name: COLUMN bag_openbareruimte.correctie; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_openbareruimte.correctie IS 'het gegeven is gecorrigeerd.';


--
-- Name: bag_openbareruimte_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.bag_openbareruimte_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: bag_openbareruimte_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.bag_openbareruimte_id_seq OWNED BY public.bag_openbareruimte.id;


--
-- Name: bag_pand; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.bag_pand (
    identificatie character varying(16) NOT NULL,
    begindatum character varying(14) NOT NULL,
    einddatum character varying(14),
    officieel character varying(1),
    bouwjaar integer,
    status character varying(80) NOT NULL,
    inonderzoek character varying(1) NOT NULL,
    documentdatum character varying(14),
    documentnummer character varying(20),
    correctie character varying(1),
    id integer NOT NULL
);


--
-- Name: TABLE bag_pand; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.bag_pand IS '55 : een pand is de kleinste, bij de totstandkoming functioneel en bouwkundig constructief zelfstandige eenheid, die direct en duurzaam met de aarde is verbonden.';


--
-- Name: COLUMN bag_pand.identificatie; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_pand.identificatie IS '55.01 : de unieke aanduiding van een pand';


--
-- Name: COLUMN bag_pand.begindatum; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_pand.begindatum IS '55.91 : de begindatum van een periode waarin een of meer gegevens die worden bijgehouden over een pand een wijziging hebben ondergaan.';


--
-- Name: COLUMN bag_pand.einddatum; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_pand.einddatum IS '55.92 : de einddatum van een periode waarin er geen wijzigingen hebben plaatsgevonden in de gegevens die worden bijgehouden over een pand.';


--
-- Name: COLUMN bag_pand.officieel; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_pand.officieel IS '55.02 : een aanduiding waarmee kan worden aangegeven dat een object in de registratie is opgenomen als gevolg van een feitelijke constatering, zonder dat er op het moment van opname sprake is van een formele grondslag voor deze opname';


--
-- Name: COLUMN bag_pand.bouwjaar; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_pand.bouwjaar IS '55.30 : de aanduiding van het jaar waarin een pand oorspronkelijk als bouwkundig gereed is opgeleverd.';


--
-- Name: COLUMN bag_pand.status; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_pand.status IS '55.31 : de fase van de levenscyclus van een pand, waarin het betreffende pand zich bevindt.';


--
-- Name: COLUMN bag_pand.inonderzoek; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_pand.inonderzoek IS '55.93 : een aanduiding waarmee wordt aangegeven dat een onderzoek wordt uitgevoerd naar de juistheid van een of meerdere gegevens van het betreffende object.';


--
-- Name: COLUMN bag_pand.documentdatum; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_pand.documentdatum IS '55.97 : de datum waarop het brondocument is vastgesteld, op basis waarvan een opname, mutatie of een verwijdering van gegevens ten aanzien van een pand heeft plaatsgevonden.';


--
-- Name: COLUMN bag_pand.documentnummer; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_pand.documentnummer IS '55.98 : de unieke aanduiding van het brondocument op basis waarvan een opname, mutatie of een verwijdering van gegevens ten aanzien van een pand heeft plaatsgevonden, binnen een gemeente.';


--
-- Name: COLUMN bag_pand.correctie; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_pand.correctie IS 'het gegeven is gecorrigeerd.';


--
-- Name: bag_pand_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.bag_pand_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: bag_pand_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.bag_pand_id_seq OWNED BY public.bag_pand.id;


--
-- Name: bag_standplaats; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.bag_standplaats (
    identificatie character varying(16) NOT NULL,
    begindatum character varying(14) NOT NULL,
    einddatum character varying(14),
    officieel character varying(1),
    status character varying(80) NOT NULL,
    hoofdadres character varying(16) NOT NULL,
    inonderzoek character varying(1) NOT NULL,
    documentdatum character varying(14),
    documentnummer character varying(20),
    correctie character varying(1),
    id integer NOT NULL
);


--
-- Name: TABLE bag_standplaats; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.bag_standplaats IS '57 : een standplaats is een formeel door de gemeenteraad als zodanig aangewezen terrein of een gedeelte daarvan, dat bestemd is voor het permanent plaatsen van een niet direct en duurzaam met de aarde verbonden en voor woon -, bedrijfsmatige - of recreatieve doeleinden geschikte ruimte.';


--
-- Name: COLUMN bag_standplaats.identificatie; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_standplaats.identificatie IS '57.01 : de unieke aanduiding van een standplaats.';


--
-- Name: COLUMN bag_standplaats.begindatum; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_standplaats.begindatum IS '57.91 : de begindatum van een periode waarin een of meer gegevens die worden bijgehouden over een standplaats een wijziging hebben ondergaan.';


--
-- Name: COLUMN bag_standplaats.einddatum; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_standplaats.einddatum IS '57.92 : de einddatum van een periode waarin er geen wijzigingen hebben plaatsgevonden in de gegevens die worden bijgehouden over een standplaats.';


--
-- Name: COLUMN bag_standplaats.officieel; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_standplaats.officieel IS '57.02 : een aanduiding waarmee kan worden aangegeven dat een object in de registratie is opgenomen als gevolg van een feitelijke constatering, zonder dat er op het moment van opname sprake is van een formele grondslag voor deze opname.';


--
-- Name: COLUMN bag_standplaats.status; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_standplaats.status IS '57.03 : de fase van de levenscyclus van een standplaats, waarin de betreffende standplaats zich bevindt.';


--
-- Name: COLUMN bag_standplaats.hoofdadres; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_standplaats.hoofdadres IS '57:10 : de identificatiecode nummeraanduiding waaronder het hoofdadres van een standplaats, dat in het kader van de basis gebouwen registratie als zodanig is aangemerkt, is opgenomen in de basis registratie adressen.';


--
-- Name: COLUMN bag_standplaats.inonderzoek; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_standplaats.inonderzoek IS '57.93 : een aanduiding waarmee wordt aangegeven dat een onderzoek wordt uitgevoerd naar de juistheid van een of meerdere gegevens van het betreffende object.';


--
-- Name: COLUMN bag_standplaats.documentdatum; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_standplaats.documentdatum IS '57.97 : de datum waarop het brondocument is vastgesteld, op basis waarvan een opname, mutatie of een in de historie plaatsen van gegevens ten aanzien van een standplaats heeft plaatsgevonden.';


--
-- Name: COLUMN bag_standplaats.documentnummer; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_standplaats.documentnummer IS '57.98 : de unieke aanduiding van het brondocument op basis waarvan een opname, mutatie of een verwijdering van gegevens ten aanzien van een standplaats heeft plaatsgevonden, binnen een gemeente.';


--
-- Name: COLUMN bag_standplaats.correctie; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_standplaats.correctie IS 'het gegeven is gecorrigeerd.';


--
-- Name: bag_standplaats_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.bag_standplaats_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: bag_standplaats_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.bag_standplaats_id_seq OWNED BY public.bag_standplaats.id;


--
-- Name: bag_verblijfsobject; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.bag_verblijfsobject (
    identificatie character varying(16) NOT NULL,
    begindatum character varying(14) NOT NULL,
    einddatum character varying(14),
    officieel character varying(1),
    hoofdadres character varying(16) NOT NULL,
    oppervlakte integer,
    status character varying(80) NOT NULL,
    inonderzoek character varying(1) NOT NULL,
    documentdatum character varying(14),
    documentnummer character varying(20),
    correctie character varying(1),
    id integer NOT NULL
);


--
-- Name: TABLE bag_verblijfsobject; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.bag_verblijfsobject IS '56 : een verblijfsobject is de kleinste binnen een of meerdere panden gelegen en voor woon -, bedrijfsmatige - of recreatieve doeleinden geschikte eenheid van gebruik, die ontsloten wordt via een eigen toegang vanaf de openbare weg, een erf of een gedeelde verkeersruimte en die onderwerp kan zijn van rechtshandelingen.';


--
-- Name: COLUMN bag_verblijfsobject.identificatie; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_verblijfsobject.identificatie IS '56.01 : de unieke aanduiding van een verblijfsobject';


--
-- Name: COLUMN bag_verblijfsobject.begindatum; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_verblijfsobject.begindatum IS '56.91 : de begindatum van een periode waarin een of meer gegevens die worden bijgehouden over een verblijfsobject een wijziging hebben ondergaan.';


--
-- Name: COLUMN bag_verblijfsobject.einddatum; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_verblijfsobject.einddatum IS '56.92 : de einddatum van een periode waarin er geen wijzigingen hebben plaatsgevonden in de gegevens die worden bijgehouden over een verblijfsobject.';


--
-- Name: COLUMN bag_verblijfsobject.officieel; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_verblijfsobject.officieel IS '56.02 : een aanduiding waarmee kan worden aangegeven dat een object in de registratie is opgenomen als gevolg van een feitelijke constatering, zonder dat er op het moment van opname sprake is van een formele grondslag voor deze opname';


--
-- Name: COLUMN bag_verblijfsobject.hoofdadres; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_verblijfsobject.hoofdadres IS '56:10 : de identificatiecode nummeraanduiding waaronder het hoofdadres van een verblijfsobject, dat in het kader van de basis gebouwen registratie als zodanig is aangemerkt, is opgenomen in de basis registratie adressen.';


--
-- Name: COLUMN bag_verblijfsobject.oppervlakte; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_verblijfsobject.oppervlakte IS '56.31 : de gebruiksoppervlakte van een verblijfsobject in gehele vierkante meters.';


--
-- Name: COLUMN bag_verblijfsobject.status; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_verblijfsobject.status IS '56.32 : de fase van de levenscyclus van een verblijfsobject, waarin het betreffende verblijfsobject zich bevindt.';


--
-- Name: COLUMN bag_verblijfsobject.inonderzoek; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_verblijfsobject.inonderzoek IS '56.93 : een aanduiding waarmee wordt aangegeven dat een onderzoek wordt uitgevoerd naar de juistheid van een of meerdere gegevens van het betreffende object.';


--
-- Name: COLUMN bag_verblijfsobject.documentdatum; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_verblijfsobject.documentdatum IS '56.97 : de datum waarop het brondocument is vastgesteld, op basis waarvan een opname, mutatie of een verwijdering van gegevens ten aanzien van een verblijfsobject heeft plaatsgevonden.';


--
-- Name: COLUMN bag_verblijfsobject.documentnummer; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_verblijfsobject.documentnummer IS '56.98 : de unieke aanduiding van het brondocument op basis waarvan een opname, mutatie of een verwijdering van gegevens ten aanzien van een verblijfsobject heeft plaatsgevonden, binnen een gemeente.';


--
-- Name: COLUMN bag_verblijfsobject.correctie; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_verblijfsobject.correctie IS 'het gegeven is gecorrigeerd.';


--
-- Name: bag_verblijfsobject_gebruiksdoel; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.bag_verblijfsobject_gebruiksdoel (
    identificatie character varying(16) NOT NULL,
    begindatum character varying(14),
    gebruiksdoel character varying(80) NOT NULL,
    correctie character varying(1),
    id integer NOT NULL
);


--
-- Name: TABLE bag_verblijfsobject_gebruiksdoel; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.bag_verblijfsobject_gebruiksdoel IS 'koppeltabel voor gebruiksdoelen bij verblijfsobject';


--
-- Name: COLUMN bag_verblijfsobject_gebruiksdoel.identificatie; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_verblijfsobject_gebruiksdoel.identificatie IS '56.01 : de unieke aanduiding van een verblijfsobject';


--
-- Name: COLUMN bag_verblijfsobject_gebruiksdoel.begindatum; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_verblijfsobject_gebruiksdoel.begindatum IS '56.91 : de begindatum van een periode waarin een of meer gegevens die worden bijgehouden over een verblijfsobject een wijziging hebben ondergaan.';


--
-- Name: COLUMN bag_verblijfsobject_gebruiksdoel.gebruiksdoel; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_verblijfsobject_gebruiksdoel.gebruiksdoel IS '56.30 : een categorisering van de gebruiksdoelen van het betreffende verblijfsobject, zoals dit  formeel door de overheid als zodanig is toegestaan.';


--
-- Name: COLUMN bag_verblijfsobject_gebruiksdoel.correctie; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_verblijfsobject_gebruiksdoel.correctie IS 'het gegeven is gecorrigeerd.';


--
-- Name: bag_verblijfsobject_gebruiksdoel_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.bag_verblijfsobject_gebruiksdoel_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: bag_verblijfsobject_gebruiksdoel_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.bag_verblijfsobject_gebruiksdoel_id_seq OWNED BY public.bag_verblijfsobject_gebruiksdoel.id;


--
-- Name: bag_verblijfsobject_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.bag_verblijfsobject_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: bag_verblijfsobject_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.bag_verblijfsobject_id_seq OWNED BY public.bag_verblijfsobject.id;


--
-- Name: bag_verblijfsobject_pand; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.bag_verblijfsobject_pand (
    identificatie character varying(16) NOT NULL,
    begindatum character varying(14),
    pand character varying(16) NOT NULL,
    correctie character varying(1),
    id integer NOT NULL
);


--
-- Name: TABLE bag_verblijfsobject_pand; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.bag_verblijfsobject_pand IS 'koppeltabel voor panden bij verblijfsobject';


--
-- Name: COLUMN bag_verblijfsobject_pand.identificatie; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_verblijfsobject_pand.identificatie IS '56.01 : de unieke aanduiding van een verblijfsobject';


--
-- Name: COLUMN bag_verblijfsobject_pand.begindatum; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_verblijfsobject_pand.begindatum IS '56.91 : de begindatum van een periode waarin een of meer gegevens die worden bijgehouden over een verblijfsobject een wijziging hebben ondergaan.';


--
-- Name: COLUMN bag_verblijfsobject_pand.pand; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_verblijfsobject_pand.pand IS '56.90 : de unieke aanduidingen van de panden waarvan het verblijfsobject onderdeel uitmaakt.';


--
-- Name: COLUMN bag_verblijfsobject_pand.correctie; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_verblijfsobject_pand.correctie IS 'het gegeven is gecorrigeerd.';


--
-- Name: bag_verblijfsobject_pand_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.bag_verblijfsobject_pand_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: bag_verblijfsobject_pand_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.bag_verblijfsobject_pand_id_seq OWNED BY public.bag_verblijfsobject_pand.id;


--
-- Name: bag_woonplaats; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.bag_woonplaats (
    identificatie character varying(16) NOT NULL,
    begindatum character varying(14) NOT NULL,
    einddatum character varying(14),
    officieel character varying(1),
    naam character varying(80) NOT NULL,
    status character varying(80) NOT NULL,
    inonderzoek character varying(1) NOT NULL,
    documentdatum character varying(14),
    documentnummer character varying(20),
    correctie character varying(1),
    id integer NOT NULL
);


--
-- Name: TABLE bag_woonplaats; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.bag_woonplaats IS '11.7 : een woonplaats is een door de gemeenteraad als zodanig aangewezen gedeelte van het gemeentelijk grondgebied.';


--
-- Name: COLUMN bag_woonplaats.identificatie; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_woonplaats.identificatie IS '11.03 : de landelijk unieke aanduiding van een woonplaats, zoals vastgesteld door de beheerder van de landelijke tabel voor woonplaatsen.';


--
-- Name: COLUMN bag_woonplaats.begindatum; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_woonplaats.begindatum IS '11.73 : de begindatum van een periode waarin een of meer gegevens die worden bijgehouden over een woonplaats een wijziging hebben ondergaan.';


--
-- Name: COLUMN bag_woonplaats.einddatum; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_woonplaats.einddatum IS '11.74 : de einddatum van een periode waarin er geen wijzigingen hebben plaatsgevonden in de gegevens die worden bijgehouden over een woonplaats.';


--
-- Name: COLUMN bag_woonplaats.officieel; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_woonplaats.officieel IS '11.72 : een aanduiding waarmee kan worden aangegeven dat een object in de registratie is opgenomen als gevolg van een feitelijke constatering, zonder dat er op het moment van opname sprake is van een formele grondslag voor deze opname.';


--
-- Name: COLUMN bag_woonplaats.naam; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_woonplaats.naam IS '11.70 : de benaming van een door het gemeentebestuur aangewezen woonplaats.';


--
-- Name: COLUMN bag_woonplaats.status; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_woonplaats.status IS '11.79 : de fase van de levenscyclus van een woonplaats, waarin de betreffende woonplaats zich bevindt.';


--
-- Name: COLUMN bag_woonplaats.inonderzoek; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_woonplaats.inonderzoek IS '11.75 : een aanduiding waarmee wordt aangegeven dat een onderzoek wordt uitgevoerd naar de juistheid van een of meerdere gegevens van het betreffende object.';


--
-- Name: COLUMN bag_woonplaats.documentdatum; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_woonplaats.documentdatum IS '11.77 : de datum waarop het brondocument is vastgesteld, op basis waarvan een opname, mutatie of een verwijdering van gegevens ten aanzien van een woonplaats heeft plaatsgevonden.';


--
-- Name: COLUMN bag_woonplaats.documentnummer; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_woonplaats.documentnummer IS '11.78 : de unieke aanduiding van het brondocument op basis waarvan een opname, mutatie of een verwijdering van gegevens ten aanzien van een woonplaats heeft plaatsgevonden, binnen een gemeente.';


--
-- Name: COLUMN bag_woonplaats.correctie; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_woonplaats.correctie IS 'het gegeven is gecorrigeerd.';


--
-- Name: bag_woonplaats_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.bag_woonplaats_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: bag_woonplaats_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.bag_woonplaats_id_seq OWNED BY public.bag_woonplaats.id;


--
-- Name: searchable; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.searchable (
    search_index tsvector,
    search_term text,
    object_type text,
    searchable_id integer NOT NULL,
    search_order text
);


--
-- Name: bedrijf; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.bedrijf (
    search_index tsvector,
    search_term text,
    object_type text DEFAULT 'bedrijf'::character varying,
    id integer NOT NULL,
    dossiernummer character varying(8),
    subdossiernummer character varying(4),
    hoofdvestiging_dossiernummer character varying(8),
    hoofdvestiging_subdossiernummer character varying(4),
    vorig_dossiernummer character varying(8),
    vorig_subdossiernummer character varying(4),
    handelsnaam text,
    rechtsvorm smallint,
    kamernummer smallint,
    faillisement smallint,
    surseance smallint,
    telefoonnummer character varying(10),
    email character varying(128),
    vestiging_adres text,
    vestiging_straatnaam text,
    vestiging_huisnummer bigint,
    vestiging_huisnummertoevoeging text,
    vestiging_postcodewoonplaats text,
    vestiging_postcode character varying(6),
    vestiging_woonplaats text,
    correspondentie_adres text,
    correspondentie_straatnaam text,
    correspondentie_huisnummer bigint,
    correspondentie_huisnummertoevoeging text,
    correspondentie_postcodewoonplaats text,
    correspondentie_postcode character varying(6),
    correspondentie_woonplaats text,
    hoofdactiviteitencode integer,
    nevenactiviteitencode1 integer,
    nevenactiviteitencode2 integer,
    werkzamepersonen integer,
    contact_naam character varying(64),
    contact_aanspreektitel character varying(45),
    contact_voorletters character varying(19),
    contact_voorvoegsel character varying(8),
    contact_geslachtsnaam character varying(95),
    contact_geslachtsaanduiding character varying(1),
    authenticated smallint,
    authenticatedby text,
    fulldossiernummer text,
    import_datum timestamp(6) without time zone,
    deleted_on timestamp(6) without time zone,
    verblijfsobject_id character varying(16),
    system_of_record character varying(32),
    system_of_record_id bigint,
    vestigingsnummer bigint,
    vestiging_huisletter text,
    correspondentie_huisletter text,
    vestiging_adres_buitenland1 text,
    vestiging_adres_buitenland2 text,
    vestiging_adres_buitenland3 text,
    vestiging_landcode integer DEFAULT 6030,
    correspondentie_adres_buitenland1 text,
    correspondentie_adres_buitenland2 text,
    correspondentie_adres_buitenland3 text,
    correspondentie_landcode integer DEFAULT 6030,
    uuid uuid DEFAULT public.uuid_generate_v4(),
    rsin bigint,
    oin bigint,
    main_activity jsonb DEFAULT '{}'::jsonb NOT NULL,
    secondairy_activities jsonb DEFAULT '[]'::jsonb NOT NULL,
    date_founded date,
    date_registration date,
    date_ceased date,
    vestiging_latlong point,
    vestiging_bag_id bigint,
    preferred_contact_channel public.preferred_contact_channel DEFAULT 'pip'::public.preferred_contact_channel,
    active boolean DEFAULT true NOT NULL,
    pending boolean DEFAULT false NOT NULL,
    related_custom_object_id integer
)
INHERITS (public.searchable);


--
-- Name: bedrijf_authenticatie; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.bedrijf_authenticatie (
    id integer NOT NULL,
    gegevens_magazijn_id integer,
    login integer,
    password character varying(255),
    created timestamp without time zone,
    last_modified timestamp without time zone
);


--
-- Name: bedrijf_authenticatie_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.bedrijf_authenticatie_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: bedrijf_authenticatie_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.bedrijf_authenticatie_id_seq OWNED BY public.bedrijf_authenticatie.id;


--
-- Name: bedrijf_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.bedrijf_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: bedrijf_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.bedrijf_id_seq OWNED BY public.bedrijf.id;


--
-- Name: beheer_import; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.beheer_import (
    id integer NOT NULL,
    importtype character varying(256),
    succesvol integer,
    finished timestamp without time zone,
    import_create integer,
    import_update integer,
    error integer,
    error_message text,
    entries integer,
    created timestamp without time zone,
    last_modified timestamp without time zone
);


--
-- Name: beheer_import_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.beheer_import_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: beheer_import_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.beheer_import_id_seq OWNED BY public.beheer_import.id;


--
-- Name: beheer_import_log; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.beheer_import_log (
    id integer NOT NULL,
    import_id integer,
    old_data text,
    new_data text,
    created timestamp without time zone,
    last_modified timestamp without time zone,
    kolom text,
    identifier text,
    action character varying(255)
);


--
-- Name: beheer_import_log_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.beheer_import_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: beheer_import_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.beheer_import_log_id_seq OWNED BY public.beheer_import_log.id;


--
-- Name: betrokkene_notes; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.betrokkene_notes (
    id integer NOT NULL,
    betrokkene_exid integer,
    betrokkene_type text,
    betrokkene_from text,
    ntype text,
    subject text,
    message text,
    created timestamp without time zone,
    last_modified timestamp without time zone
);


--
-- Name: betrokkene_notes_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.betrokkene_notes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: betrokkene_notes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.betrokkene_notes_id_seq OWNED BY public.betrokkene_notes.id;


--
-- Name: betrokkenen; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.betrokkenen (
    id integer NOT NULL,
    btype integer,
    gm_natuurlijk_persoon_id integer,
    naam text
);


--
-- Name: betrokkenen_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.betrokkenen_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: betrokkenen_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.betrokkenen_id_seq OWNED BY public.betrokkenen.id;


--
-- Name: bibliotheek_categorie; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.bibliotheek_categorie (
    search_index tsvector,
    search_term text,
    object_type text DEFAULT 'bibliotheek_categorie'::character varying,
    id integer NOT NULL,
    naam character varying(256),
    label text,
    description text,
    help text,
    created timestamp without time zone,
    last_modified timestamp without time zone,
    system integer,
    pid integer,
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL
)
INHERITS (public.searchable);


--
-- Name: bibliotheek_categorie_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.bibliotheek_categorie_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: bibliotheek_categorie_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.bibliotheek_categorie_id_seq OWNED BY public.bibliotheek_categorie.id;


--
-- Name: bibliotheek_kenmerken; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.bibliotheek_kenmerken (
    search_index tsvector,
    search_term text,
    object_type text DEFAULT 'bibliotheek_kenmerken'::character varying,
    id integer NOT NULL,
    naam character varying(256),
    value_type text,
    value_default text DEFAULT ''::text NOT NULL,
    label text,
    description text,
    help text DEFAULT ''::text NOT NULL,
    magic_string text,
    created timestamp without time zone,
    last_modified timestamp without time zone,
    bibliotheek_categorie_id integer,
    document_categorie text,
    system integer,
    deleted timestamp without time zone,
    file_metadata_id integer,
    version integer DEFAULT 1 NOT NULL,
    properties text DEFAULT '{}'::text,
    naam_public text DEFAULT ''::text NOT NULL,
    type_multiple boolean DEFAULT false NOT NULL,
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    relationship_type text,
    relationship_name text,
    relationship_uuid uuid,
    CONSTRAINT bibliotheek_kenmerken_value_type_check CHECK ((value_type = ANY (ARRAY['text_uc'::text, 'checkbox'::text, 'richtext'::text, 'date'::text, 'file'::text, 'bag_straat_adres'::text, 'email'::text, 'valutaex'::text, 'bag_openbareruimte'::text, 'text'::text, 'bag_openbareruimtes'::text, 'url'::text, 'valuta'::text, 'option'::text, 'bag_adres'::text, 'select'::text, 'valutain6'::text, 'valutaex6'::text, 'valutaex21'::text, 'image_from_url'::text, 'bag_adressen'::text, 'valutain'::text, 'calendar'::text, 'calendar_supersaas'::text, 'bag_straat_adressen'::text, 'googlemaps'::text, 'numeric'::text, 'valutain21'::text, 'textarea'::text, 'bankaccount'::text, 'subject'::text, 'geolatlon'::text, 'appointment'::text, 'geojson'::text, 'relationship'::text, 'address_v2'::text, 'appointment_v2'::text])))
)
INHERITS (public.searchable);


--
-- Name: bibliotheek_kenmerken_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.bibliotheek_kenmerken_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: bibliotheek_kenmerken_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.bibliotheek_kenmerken_id_seq OWNED BY public.bibliotheek_kenmerken.id;


--
-- Name: bibliotheek_kenmerken_values; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.bibliotheek_kenmerken_values (
    id integer NOT NULL,
    bibliotheek_kenmerken_id integer,
    value text,
    active boolean DEFAULT true NOT NULL,
    sort_order integer NOT NULL
);


--
-- Name: bibliotheek_kenmerken_values_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.bibliotheek_kenmerken_values_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: bibliotheek_kenmerken_values_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.bibliotheek_kenmerken_values_id_seq OWNED BY public.bibliotheek_kenmerken_values.id;


--
-- Name: bibliotheek_kenmerken_values_sort_order_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.bibliotheek_kenmerken_values_sort_order_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: bibliotheek_kenmerken_values_sort_order_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.bibliotheek_kenmerken_values_sort_order_seq OWNED BY public.bibliotheek_kenmerken_values.sort_order;


--
-- Name: bibliotheek_notificatie_kenmerk; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.bibliotheek_notificatie_kenmerk (
    id integer NOT NULL,
    bibliotheek_notificatie_id integer NOT NULL,
    bibliotheek_kenmerken_id integer NOT NULL
);


--
-- Name: bibliotheek_notificatie_kenmerk_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.bibliotheek_notificatie_kenmerk_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: bibliotheek_notificatie_kenmerk_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.bibliotheek_notificatie_kenmerk_id_seq OWNED BY public.bibliotheek_notificatie_kenmerk.id;


--
-- Name: bibliotheek_notificaties; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.bibliotheek_notificaties (
    search_index tsvector,
    search_term text,
    object_type text DEFAULT 'bibliotheek_notificaties'::character varying,
    id integer NOT NULL,
    bibliotheek_categorie_id integer,
    label text,
    subject text,
    message text,
    created timestamp without time zone,
    last_modified timestamp without time zone,
    deleted timestamp without time zone,
    sender text,
    sender_address text,
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL
)
INHERITS (public.searchable);


--
-- Name: bibliotheek_notificaties_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.bibliotheek_notificaties_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: bibliotheek_notificaties_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.bibliotheek_notificaties_id_seq OWNED BY public.bibliotheek_notificaties.id;


--
-- Name: bibliotheek_sjablonen; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.bibliotheek_sjablonen (
    search_index tsvector,
    search_term text,
    object_type text DEFAULT 'bibliotheek_sjablonen'::character varying,
    id integer NOT NULL,
    bibliotheek_categorie_id integer,
    naam character varying(256),
    label text,
    description text,
    help text,
    created timestamp without time zone,
    last_modified timestamp without time zone,
    filestore_id integer,
    deleted timestamp without time zone,
    interface_id integer,
    template_external_name text,
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    CONSTRAINT filestore_or_interface CHECK (((filestore_id IS NOT NULL) OR (interface_id IS NOT NULL)))
)
INHERITS (public.searchable);


--
-- Name: bibliotheek_sjablonen_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.bibliotheek_sjablonen_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: bibliotheek_sjablonen_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.bibliotheek_sjablonen_id_seq OWNED BY public.bibliotheek_sjablonen.id;


--
-- Name: bibliotheek_sjablonen_magic_string; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.bibliotheek_sjablonen_magic_string (
    id integer NOT NULL,
    bibliotheek_sjablonen_id integer,
    value text
);


--
-- Name: bibliotheek_sjablonen_magic_string_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.bibliotheek_sjablonen_magic_string_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: bibliotheek_sjablonen_magic_string_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.bibliotheek_sjablonen_magic_string_id_seq OWNED BY public.bibliotheek_sjablonen_magic_string.id;


--
-- Name: case_authorisation_map; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.case_authorisation_map (
    key text NOT NULL,
    legacy_key text NOT NULL
);


--
-- Name: groups; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.groups (
    id integer NOT NULL,
    path integer[] NOT NULL,
    name text,
    description text,
    date_created timestamp without time zone,
    date_modified timestamp without time zone,
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    v1_json jsonb DEFAULT '{}'::jsonb NOT NULL
);


--
-- Name: roles; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.roles (
    id integer NOT NULL,
    parent_group_id integer,
    name text,
    description text,
    system_role boolean,
    date_created timestamp without time zone,
    date_modified timestamp without time zone,
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    v1_json jsonb DEFAULT '{}'::jsonb NOT NULL
);


--
-- Name: subject; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.subject (
    id integer NOT NULL,
    uuid uuid DEFAULT public.uuid_generate_v4(),
    subject_type text NOT NULL,
    properties text DEFAULT '{}'::text NOT NULL,
    settings text DEFAULT '{}'::text NOT NULL,
    username character varying NOT NULL,
    last_modified timestamp without time zone DEFAULT now(),
    role_ids integer[] DEFAULT '{}'::integer[] NOT NULL,
    group_ids integer[] DEFAULT '{}'::integer[] NOT NULL,
    nobody boolean DEFAULT false NOT NULL,
    system boolean DEFAULT false NOT NULL,
    related_custom_object_id integer,
    CONSTRAINT subject_subject_type_check CHECK ((subject_type = ANY (ARRAY['person'::text, 'company'::text, 'employee'::text])))
);


--
-- Name: subject_position_matrix; Type: MATERIALIZED VIEW; Schema: public; Owner: -
--

CREATE MATERIALIZED VIEW public.subject_position_matrix AS
 SELECT s.id AS subject_id,
    unrolled_groups.gid AS group_id,
    unrolled_roles.rid AS role_id,
    ((((unrolled_groups.gid)::character varying)::text || '|'::text) || ((unrolled_roles.rid)::character varying)::text) AS "position"
   FROM public.subject s,
    ( SELECT s_1.id AS subject_id,
            unnest(s_1.role_ids) AS rid
           FROM public.subject s_1) unrolled_roles,
    ( SELECT s_1.id AS subject_id,
            unnest(g.path) AS gid
           FROM (public.subject s_1
             JOIN public.groups g ON ((s_1.group_ids[1] = g.id)))) unrolled_groups,
    public.roles ro,
    public.groups gr
  WHERE ((s.id = unrolled_roles.subject_id) AND (s.id = unrolled_groups.subject_id) AND (ro.id = unrolled_roles.rid) AND (gr.id = unrolled_groups.gid))
  GROUP BY s.id, unrolled_groups.gid, unrolled_roles.rid
  WITH NO DATA;


--
-- Name: zaak; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.zaak (
    search_index tsvector,
    search_term text,
    object_type text DEFAULT 'zaak'::character varying,
    id integer NOT NULL,
    pid integer,
    relates_to integer,
    zaaktype_id integer NOT NULL,
    zaaktype_node_id integer NOT NULL,
    milestone integer NOT NULL,
    contactkanaal character varying(128) NOT NULL,
    aanvraag_trigger public.zaaksysteem_trigger NOT NULL,
    onderwerp text,
    resultaat text,
    besluit text,
    coordinator integer,
    behandelaar integer,
    aanvrager integer NOT NULL,
    route_ou integer,
    route_role integer,
    locatie_zaak integer,
    locatie_correspondentie integer,
    streefafhandeldatum timestamp(6) without time zone,
    registratiedatum timestamp(6) without time zone NOT NULL,
    afhandeldatum timestamp(6) without time zone,
    vernietigingsdatum timestamp(6) without time zone,
    created timestamp(6) without time zone NOT NULL,
    last_modified timestamp(6) without time zone NOT NULL,
    deleted timestamp(6) without time zone,
    vervolg_van integer,
    aanvrager_gm_id integer,
    behandelaar_gm_id integer,
    coordinator_gm_id integer,
    uuid uuid,
    payment_status text,
    payment_amount numeric(100,2),
    confidentiality public.confidentiality DEFAULT 'public'::public.confidentiality NOT NULL,
    stalled_until timestamp without time zone,
    onderwerp_extern text,
    archival_state text,
    status text NOT NULL,
    duplicate_prevention_token uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    resultaat_id integer,
    urgency text,
    preset_client boolean DEFAULT false NOT NULL,
    prefix text DEFAULT ''::text NOT NULL,
    confidential boolean DEFAULT false NOT NULL,
    html_email_template character varying,
    number_master integer,
    requestor_v1_json jsonb NOT NULL,
    assignee_v1_json jsonb,
    coordinator_v1_json jsonb,
    aanvrager_type character varying,
    dutch_price text GENERATED ALWAYS AS (COALESCE(replace((payment_amount)::text, '.'::text, ','::text), '-'::text)) STORED,
    betrokkenen_cache jsonb DEFAULT '{}'::jsonb NOT NULL,
    status_percentage numeric(100,2) DEFAULT 0 NOT NULL,
    leadtime integer GENERATED ALWAYS AS (
CASE
    WHEN (afhandeldatum IS NOT NULL) THEN date_part('day'::text, (afhandeldatum - registratiedatum))
    ELSE NULL::double precision
END) STORED,
    CONSTRAINT archival_state_value CHECK ((archival_state = ANY (ARRAY['overdragen'::text, 'vernietigen'::text]))),
    CONSTRAINT status_value CHECK ((status = ANY (ARRAY['new'::text, 'open'::text, 'stalled'::text, 'resolved'::text, 'deleted'::text])))
)
INHERITS (public.searchable);


--
-- Name: zaak_authorisation; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.zaak_authorisation (
    id integer NOT NULL,
    zaak_id integer NOT NULL,
    capability text NOT NULL,
    entity_id text NOT NULL,
    entity_type text NOT NULL,
    scope text NOT NULL
);


--
-- Name: zaaktype_authorisation; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.zaaktype_authorisation (
    id integer NOT NULL,
    zaaktype_node_id integer,
    recht text NOT NULL,
    created timestamp without time zone,
    last_modified timestamp without time zone,
    deleted timestamp without time zone,
    role_id integer,
    ou_id integer,
    zaaktype_id integer,
    confidential boolean DEFAULT false NOT NULL,
    CONSTRAINT zaaktype_authorisation_recht CHECK ((recht = ANY (ARRAY['zaak_beheer'::text, 'zaak_edit'::text, 'zaak_read'::text, 'zaak_search'::text])))
);


--
-- Name: case_acl; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW public.case_acl AS
 SELECT z.id AS case_id,
    z.uuid AS case_uuid,
    cam.key AS permission,
    s.id AS subject_id,
    s.uuid AS subject_uuid,
    z.zaaktype_id AS casetype_id
   FROM (((public.zaak z
     JOIN public.zaaktype_authorisation za ON (((za.zaaktype_id = z.zaaktype_id) AND (za.confidential = z.confidential))))
     JOIN (public.subject_position_matrix spm
     JOIN public.subject s ON ((spm.subject_id = s.id))) ON (((spm.role_id = za.role_id) AND (spm.group_id = za.ou_id))))
     JOIN public.case_authorisation_map cam ON ((za.recht = cam.legacy_key)))
UNION ALL
 SELECT z.id AS case_id,
    z.uuid AS case_uuid,
    za.capability AS permission,
    s.id AS subject_id,
    s.uuid AS subject_uuid,
    z.zaaktype_id AS casetype_id
   FROM ((public.zaak z
     JOIN public.zaak_authorisation za ON (((za.zaak_id = z.id) AND (za.entity_type = 'position'::text))))
     JOIN (public.subject_position_matrix spm
     JOIN public.subject s ON ((spm.subject_id = s.id))) ON ((spm."position" = za.entity_id)))
UNION ALL
 SELECT z.id AS case_id,
    z.uuid AS case_uuid,
    za.capability AS permission,
    s.id AS subject_id,
    s.uuid AS subject_uuid,
    z.zaaktype_id AS casetype_id
   FROM ((public.zaak z
     JOIN public.zaak_authorisation za ON (((za.zaak_id = z.id) AND (za.entity_type = 'user'::text))))
     JOIN public.subject s ON (((s.username)::text = za.entity_id)));


--
-- Name: case_action; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.case_action (
    id integer NOT NULL,
    case_id integer NOT NULL,
    casetype_status_id integer,
    type character varying(64),
    label character varying(255),
    automatic boolean,
    data text,
    state_tainted boolean DEFAULT false,
    data_tainted boolean DEFAULT false
);


--
-- Name: case_action_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.case_action_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: case_action_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.case_action_id_seq OWNED BY public.case_action.id;


--
-- Name: case_attributes; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW public.case_attributes AS
SELECT
    NULL::integer AS case_id,
    NULL::text[] AS value,
    NULL::text AS magic_string,
    NULL::integer AS library_id,
    NULL::text AS value_type,
    NULL::boolean AS mvp;


--
-- Name: object_data; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.object_data (
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    object_id integer,
    object_class text NOT NULL,
    properties text DEFAULT '{}'::text NOT NULL,
    index_hstore public.hstore,
    date_created timestamp without time zone DEFAULT now() NOT NULL,
    date_modified timestamp without time zone,
    text_vector tsvector,
    class_uuid uuid,
    acl_groupname text,
    invalid boolean DEFAULT false,
    CONSTRAINT object_data_acl_groupname_check CHECK (((acl_groupname IS NULL) OR (character_length(acl_groupname) > 0)))
);


--
-- Name: zaak_kenmerk; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.zaak_kenmerk (
    zaak_id integer NOT NULL,
    bibliotheek_kenmerken_id integer NOT NULL,
    id integer NOT NULL,
    value text[] NOT NULL,
    magic_string text NOT NULL
);


--
-- Name: zaaktype_kenmerken; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.zaaktype_kenmerken (
    id integer NOT NULL,
    bibliotheek_kenmerken_id integer,
    label text,
    help text,
    created timestamp without time zone,
    last_modified timestamp without time zone,
    zaaktype_node_id integer,
    zaak_status_id integer,
    pip integer,
    zaakinformatie_view integer DEFAULT 1,
    bag_zaakadres integer,
    value_default text,
    pip_can_change boolean,
    publish_public integer,
    is_systeemkenmerk boolean DEFAULT false,
    required_permissions text,
    version integer,
    help_extern text,
    object_id uuid,
    object_metadata text DEFAULT '{}'::text NOT NULL,
    label_multiple text,
    properties text DEFAULT '{}'::text,
    is_group boolean DEFAULT false NOT NULL,
    referential boolean DEFAULT false NOT NULL,
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    custom_object_uuid uuid,
    value_mandatory boolean DEFAULT false NOT NULL
);


--
-- Name: case_attributes_appointments; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW public.case_attributes_appointments AS
 SELECT z.id AS case_id,
    COALESCE(od.properties, '{}'::text) AS value,
    bk.magic_string,
    bk.id AS library_id,
    od.uuid AS reference
   FROM ((((public.zaak z
     JOIN public.zaaktype_kenmerken ztk ON ((z.zaaktype_node_id = ztk.zaaktype_node_id)))
     JOIN public.bibliotheek_kenmerken bk ON (((bk.id = ztk.bibliotheek_kenmerken_id) AND (bk.value_type = 'appointment'::text))))
     LEFT JOIN public.zaak_kenmerk zk ON (((z.id = zk.zaak_id) AND (zk.bibliotheek_kenmerken_id = ztk.bibliotheek_kenmerken_id))))
     LEFT JOIN public.object_data od ON (((zk.value[1])::uuid = od.uuid)))
  GROUP BY z.id, COALESCE(od.properties, '{}'::text), bk.magic_string, bk.id, od.uuid;


--
-- Name: searchable_searchable_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.searchable_searchable_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: searchable_searchable_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.searchable_searchable_id_seq OWNED BY public.searchable.searchable_id;


--
-- Name: file; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.file (
    search_index tsvector,
    search_term text,
    object_type text DEFAULT 'file'::character varying,
    searchable_id integer DEFAULT nextval('public.searchable_searchable_id_seq'::regclass),
    id integer NOT NULL,
    filestore_id integer NOT NULL,
    name text NOT NULL,
    extension character varying(10) NOT NULL,
    root_file_id integer,
    version integer DEFAULT 1,
    case_id integer,
    metadata_id integer,
    subject_id character varying(100),
    directory_id integer,
    creation_reason text NOT NULL,
    accepted boolean DEFAULT false NOT NULL,
    rejection_reason text,
    reject_to_queue boolean DEFAULT false,
    is_duplicate_name boolean DEFAULT false NOT NULL,
    publish_pip boolean DEFAULT false NOT NULL,
    publish_website boolean DEFAULT false NOT NULL,
    date_created timestamp without time zone DEFAULT now() NOT NULL,
    created_by character varying(100) NOT NULL,
    date_modified timestamp without time zone,
    modified_by character varying(100),
    date_deleted timestamp without time zone,
    deleted_by character varying(100),
    destroyed boolean DEFAULT false,
    scheduled_jobs_id integer,
    intake_owner character varying,
    active_version boolean DEFAULT false NOT NULL,
    is_duplicate_of integer,
    queue boolean DEFAULT true NOT NULL,
    document_status public.documentstatus DEFAULT 'original'::public.documentstatus NOT NULL,
    generator text,
    lock_timestamp timestamp without time zone,
    lock_subject_id uuid,
    lock_subject_name text,
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    confidential boolean DEFAULT false NOT NULL,
    rejected_by_display_name text,
    intake_group_id integer,
    intake_role_id integer,
    skip_intake boolean DEFAULT false NOT NULL,
    created_by_display_name text,
    CONSTRAINT lock_fields_check CHECK (((lock_timestamp IS NULL) OR ((lock_subject_id IS NOT NULL) AND (lock_subject_name IS NOT NULL))))
)
INHERITS (public.searchable);


--
-- Name: file_case_document; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.file_case_document (
    id integer NOT NULL,
    file_id integer NOT NULL,
    magic_string text NOT NULL,
    bibliotheek_kenmerken_id integer NOT NULL,
    case_id integer NOT NULL
);


--
-- Name: filestore; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.filestore (
    id integer NOT NULL,
    uuid uuid NOT NULL,
    thumbnail_uuid uuid,
    original_name text NOT NULL,
    size bigint NOT NULL,
    mimetype character varying(160) NOT NULL,
    md5 character varying(100) NOT NULL,
    date_created timestamp without time zone DEFAULT now() NOT NULL,
    storage_location text[],
    is_archivable boolean DEFAULT false NOT NULL,
    virus_scan_status text DEFAULT 'pending'::text NOT NULL,
    CONSTRAINT filestore_virus_scan_status_check CHECK ((virus_scan_status ~ '^(pending|ok|found(:.*)?)$'::text))
);


--
-- Name: case_documents; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW public.case_documents AS
 SELECT z.id AS case_id,
    (COALESCE(array_agg(fs.uuid) FILTER (WHERE (fs.uuid IS NOT NULL)), '{}'::uuid[]))::text[] AS value,
    bk.magic_string,
    bk.id AS library_id
   FROM (((public.zaak z
     JOIN public.zaaktype_kenmerken ztk ON ((z.zaaktype_node_id = ztk.zaaktype_node_id)))
     JOIN public.bibliotheek_kenmerken bk ON (((ztk.bibliotheek_kenmerken_id = bk.id) AND (bk.value_type = 'file'::text))))
     LEFT JOIN (public.file_case_document fcd
     JOIN (public.file f
     JOIN public.filestore fs ON ((f.filestore_id = fs.id))) ON ((fcd.file_id = f.id))) ON (((z.id = fcd.case_id) AND (bk.id = fcd.bibliotheek_kenmerken_id))))
  GROUP BY z.id, bk.magic_string, bk.id;


--
-- Name: case_attributes_v0; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW public.case_attributes_v0 AS
 SELECT case_attributes.case_id,
    case_attributes.magic_string,
    case_attributes.library_id,
    public.attribute_value_to_v0(case_attributes.value, case_attributes.value_type, case_attributes.mvp) AS value
   FROM public.case_attributes
UNION ALL
 SELECT case_attributes_appointments.case_id,
    case_attributes_appointments.magic_string,
    case_attributes_appointments.library_id,
    to_jsonb((case_attributes_appointments.reference)::text) AS value
   FROM public.case_attributes_appointments
UNION ALL
 SELECT case_documents.case_id,
    case_documents.magic_string,
    case_documents.library_id,
    public.attribute_value_to_v0(case_documents.value, 'file'::text, true) AS value
   FROM public.case_documents;


--
-- Name: case_attributes_v1; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW public.case_attributes_v1 AS
 SELECT case_attributes.case_id,
    case_attributes.magic_string,
    case_attributes.library_id,
    public.attribute_value_to_jsonb(case_attributes.value, case_attributes.value_type) AS value
   FROM public.case_attributes
UNION ALL
 SELECT case_attributes_appointments.case_id,
    case_attributes_appointments.magic_string,
    case_attributes_appointments.library_id,
    public.appointment_attribute_value_to_jsonb(case_attributes_appointments.value, case_attributes_appointments.reference) AS value
   FROM public.case_attributes_appointments
UNION ALL
 SELECT case_documents.case_id,
    case_documents.magic_string,
    case_documents.library_id,
    public.attribute_value_to_jsonb(case_documents.value, 'file'::text) AS value
   FROM public.case_documents;


--
-- Name: case_property; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.case_property (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    name text NOT NULL,
    type text NOT NULL,
    namespace text NOT NULL,
    value jsonb,
    value_v0 jsonb,
    case_id integer NOT NULL,
    object_id uuid NOT NULL,
    CONSTRAINT case_property_min_value_count CHECK (((value IS NOT NULL) OR (value_v0 IS NOT NULL)))
);


--
-- Name: COLUMN case_property.name; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.case_property.name IS 'unified magic_string/v1 attribute/v0 property';


--
-- Name: COLUMN case_property.type; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.case_property.type IS 'de-normalized type of the /value/ of the property in the context of the referent object';


--
-- Name: COLUMN case_property.value; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.case_property.value IS 'syzygy-style value blob; {"value_type_name":"text","value":"myval","meta":"data"}';


--
-- Name: COLUMN case_property.value_v0; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.case_property.value_v0 IS 'object_data-style value blob; {"human_label":"Aanvrager KvK-nummer","human_value":"123456789","value":":123456789","name":"case.requestor.coc","attribute_type":"text"}';


--
-- Name: case_relation; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.case_relation (
    id integer NOT NULL,
    case_id_a integer,
    case_id_b integer,
    order_seq_a integer,
    order_seq_b integer,
    type_a character varying(64),
    type_b character varying(64),
    uuid uuid DEFAULT public.uuid_generate_v4()
);


--
-- Name: case_relation_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.case_relation_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: case_relation_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.case_relation_id_seq OWNED BY public.case_relation.id;


--
-- Name: case_relationship_view; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW public.case_relationship_view AS
 SELECT cr.case_id_a AS case_id,
    cr.case_id_b AS relation_id,
    cr.type_a AS type,
    z.uuid AS relation_uuid
   FROM (public.case_relation cr
     JOIN public.zaak z ON (((z.id = cr.case_id_b) AND (z.deleted IS NULL))))
UNION
 SELECT cr.case_id_b AS case_id,
    cr.case_id_a AS relation_id,
    cr.type_b AS type,
    z.uuid AS relation_uuid
   FROM (public.case_relation cr
     JOIN public.zaak z ON (((z.id = cr.case_id_a) AND (z.deleted IS NULL))))
UNION
 SELECT parent.id AS case_id,
    child.id AS relation_id,
    'parent'::character varying AS type,
    child.uuid AS relation_uuid
   FROM (public.zaak parent
     JOIN public.zaak child ON ((child.pid = parent.id)))
  WHERE ((child.deleted IS NULL) AND (parent.deleted IS NULL));


--
-- Name: case_relationship_json_view; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW public.case_relationship_json_view AS
 SELECT case_relationship_view.case_id,
    case_relationship_view.type,
    jsonb_agg(json_build_object('type', 'case', 'reference', case_relationship_view.relation_uuid)) AS relationship
   FROM public.case_relationship_view
  GROUP BY case_relationship_view.case_id, case_relationship_view.type;


--
-- Name: result_preservation_terms; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.result_preservation_terms (
    id integer NOT NULL,
    code integer NOT NULL,
    label text NOT NULL,
    unit text NOT NULL,
    unit_amount numeric NOT NULL
);


--
-- Name: zaak_bag; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.zaak_bag (
    id integer NOT NULL,
    pid integer,
    zaak_id integer,
    bag_type public.zaaksysteem_bag_types,
    bag_id character varying(255),
    bag_verblijfsobject_id character varying(255),
    bag_openbareruimte_id character varying(255),
    bag_nummeraanduiding_id character varying(255),
    bag_pand_id character varying(255),
    bag_standplaats_id character varying(255),
    bag_ligplaats_id character varying(255),
    bag_coordinates_wsg point
);


--
-- Name: zaak_betrokkenen; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.zaak_betrokkenen (
    id integer NOT NULL,
    zaak_id integer,
    betrokkene_type character varying(128),
    betrokkene_id integer,
    gegevens_magazijn_id integer,
    verificatie character varying(128),
    naam character varying(255),
    rol text,
    magic_string_prefix text,
    deleted timestamp without time zone,
    uuid uuid DEFAULT public.uuid_generate_v4(),
    pip_authorized boolean DEFAULT false NOT NULL,
    subject_id uuid,
    authorisation text,
    bibliotheek_kenmerken_id integer
);


--
-- Name: zaak_meta; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.zaak_meta (
    id integer NOT NULL,
    zaak_id integer,
    verlenging character varying(255),
    opschorten character varying(255),
    deel character varying(255),
    gerelateerd character varying(255),
    vervolg character varying(255),
    afhandeling character varying(255),
    stalled_since timestamp without time zone,
    current_deadline jsonb DEFAULT '{}'::jsonb NOT NULL,
    deadline_timeline jsonb DEFAULT '[]'::jsonb NOT NULL,
    last_modified timestamp without time zone DEFAULT now() NOT NULL,
    index_hstore public.hstore,
    text_vector tsvector,
    unread_communication_count integer DEFAULT 0 NOT NULL,
    unaccepted_attribute_update_count integer DEFAULT 0 NOT NULL,
    pending_changes jsonb DEFAULT '{}'::jsonb NOT NULL,
    unaccepted_files_count integer DEFAULT 0 NOT NULL,
    CONSTRAINT unread_minimum CHECK ((unread_communication_count >= 0))
);


--
-- Name: zaaktype; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.zaaktype (
    search_index tsvector,
    search_term text,
    object_type text DEFAULT 'zaaktype'::character varying,
    id integer NOT NULL,
    zaaktype_node_id integer,
    version integer,
    created timestamp without time zone,
    last_modified timestamp without time zone,
    deleted timestamp without time zone,
    bibliotheek_categorie_id integer,
    active boolean DEFAULT true NOT NULL,
    uuid uuid DEFAULT public.uuid_generate_v4()
)
INHERITS (public.searchable);


--
-- Name: zaaktype_definitie; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.zaaktype_definitie (
    id integer NOT NULL,
    openbaarheid character varying(255),
    handelingsinitiator character varying(255),
    grondslag text,
    procesbeschrijving character varying(255),
    afhandeltermijn character varying(255),
    afhandeltermijn_type character varying(255),
    selectielijst character varying(255),
    servicenorm character varying(255),
    servicenorm_type character varying(255),
    pdc_voorwaarden text,
    pdc_description text,
    pdc_meenemen text,
    pdc_tarief text,
    omschrijving_upl character varying(255),
    aard character varying(255),
    extra_informatie text,
    preset_client character varying(255),
    extra_informatie_extern text
);


--
-- Name: zaaktype_node; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.zaaktype_node (
    id integer NOT NULL,
    zaaktype_id integer,
    zaaktype_rt_queue text,
    code text,
    trigger text,
    titel character varying(128),
    version integer,
    active integer,
    created timestamp without time zone,
    last_modified timestamp without time zone,
    deleted timestamp without time zone,
    webform_toegang integer,
    webform_authenticatie text,
    adres_relatie text,
    aanvrager_hergebruik integer,
    automatisch_aanvragen integer,
    automatisch_behandelen integer,
    toewijzing_zaakintake integer,
    toelichting character varying(128),
    online_betaling integer,
    zaaktype_definitie_id integer,
    adres_andere_locatie integer,
    adres_aanvrager integer,
    bedrijfid_wijzigen integer,
    zaaktype_vertrouwelijk integer,
    zaaktype_trefwoorden text,
    zaaktype_omschrijving text,
    extra_relaties_in_aanvraag boolean,
    properties text DEFAULT '{}'::text NOT NULL,
    contact_info_intake boolean DEFAULT false NOT NULL,
    is_public boolean DEFAULT false,
    prevent_pip boolean DEFAULT false NOT NULL,
    contact_info_email_required boolean DEFAULT false,
    contact_info_phone_required boolean DEFAULT false,
    contact_info_mobile_phone_required boolean DEFAULT false,
    moeder_zaaktype_id integer,
    logging_id integer,
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    adres_geojson boolean DEFAULT false,
    v0_json jsonb DEFAULT '{}'::jsonb NOT NULL
);


--
-- Name: zaaktype_resultaten; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.zaaktype_resultaten (
    id integer NOT NULL,
    zaaktype_node_id integer,
    zaaktype_status_id integer,
    resultaat text,
    ingang text,
    bewaartermijn integer,
    created timestamp without time zone,
    last_modified timestamp without time zone,
    dossiertype character varying(50),
    label text,
    selectielijst text,
    archiefnominatie text,
    comments text,
    external_reference text,
    trigger_archival boolean DEFAULT true NOT NULL,
    selectielijst_brondatum date,
    selectielijst_einddatum date,
    properties text DEFAULT '{}'::text NOT NULL,
    standaard_keuze boolean DEFAULT false NOT NULL
);


--
-- Name: zaaktype_status; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.zaaktype_status (
    id integer NOT NULL,
    zaaktype_node_id integer,
    status integer,
    status_type text,
    naam text,
    created timestamp without time zone,
    last_modified timestamp without time zone,
    ou_id integer,
    role_id integer,
    checklist integer,
    fase character varying(255),
    role_set integer,
    termijn integer DEFAULT 0
);


--
-- Name: case_v0; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW public.case_v0 AS
 SELECT z.uuid AS id,
    z.id AS object_id,
    jsonb_build_object('class_uuid', zt.uuid, 'pending_changes', zm.pending_changes) AS "case",
    (((((((((((((((jsonb_build_object('case.route_ou', z.route_ou, 'case.route_role', z.route_role, 'case.channel_of_contact', z.contactkanaal, 'case.html_email_template', z.html_email_template, 'case.current_deadline', zm.current_deadline, 'case.date_destruction', public.timestamp_to_perl_datetime((z.vernietigingsdatum)::timestamp with time zone), 'case.date_of_completion', public.timestamp_to_perl_datetime((z.afhandeldatum)::timestamp with time zone), 'case.date_of_registration', public.timestamp_to_perl_datetime((z.registratiedatum)::timestamp with time zone), 'case.date_target',
        CASE
            WHEN (z.status = 'stalled'::text) THEN 'Opgeschort'::text
            ELSE public.timestamp_to_perl_datetime((z.streefafhandeldatum)::timestamp with time zone)
        END, 'case.deadline_timeline', zm.deadline_timeline, 'case.stalled_since', public.timestamp_to_perl_datetime((zm.stalled_since)::timestamp with time zone), 'case.stalled_until', public.timestamp_to_perl_datetime((z.stalled_until)::timestamp with time zone), 'case.suspension_rationale', zm.opschorten, 'date_created', public.timestamp_to_perl_datetime((z.created)::timestamp with time zone), 'date_modified', public.timestamp_to_perl_datetime((z.last_modified)::timestamp with time zone), 'case.urgency', z.urgency, 'case.startdate', to_char(timezone('Europe/Amsterdam'::text, (z.registratiedatum)::timestamp with time zone), 'DD-MM-YYYY'::text), 'case.progress_days', public.get_date_progress_from_case(public.hstore(z.*)), 'case.progress_status', z.status_percentage, 'case.days_left', ((z.streefafhandeldatum)::date - COALESCE((z.afhandeldatum)::date, (now())::date)), 'case.lead_time_real', z.leadtime, 'case.destructable', public.is_destructable((z.vernietigingsdatum)::timestamp with time zone), 'case.number_status', z.milestone, 'case.milestone',
        CASE
            WHEN (zts_next.id IS NOT NULL) THEN zts.naam
            ELSE NULL::text
        END, 'case.phase',
        CASE
            WHEN (zts_next.id IS NOT NULL) THEN zts_next.fase
            ELSE NULL::character varying
        END, 'case.status', z.status, 'case.subject', z.onderwerp, 'case.subject_external', z.onderwerp_extern, 'case.archival_state', z.archival_state, 'case.confidentiality', public.get_confidential_mapping(z.confidentiality), 'case.payment_status', public.get_payment_status_mapping(z.payment_status), 'case.price', z.dutch_price, 'case.documents', COALESCE(( SELECT string_agg(concat(f.name, f.extension, ' (', f.document_status, ')'), ', '::text) AS string_agg
           FROM public.file f
          WHERE ((f.case_id = z.id) AND (f.date_deleted IS NULL) AND (f.active_version = true))), ''::text), 'case.case_documents', COALESCE(( SELECT string_agg(concat(case_documents.name, case_documents.extension, ' (', case_documents.document_status, ')'), ', '::text) AS string_agg
           FROM (public.file case_documents
             JOIN public.file_case_document fcd ON (((fcd.case_id = case_documents.case_id) AND (case_documents.id = fcd.file_id))))
          WHERE (case_documents.case_id = z.id)), ''::text), 'case.num_unaccepted_updates', zm.unaccepted_attribute_update_count, 'case.num_unread_communication', zm.unread_communication_count, 'case.num_unaccepted_files', zm.unaccepted_files_count, 'case.aggregation_scope', 'Dossier') || jsonb_build_object('case.result', z.resultaat, 'case.result_description', ztr.label, 'case.result_explanation', ztr.comments, 'case.result_id', ztr.id, 'case.result_origin', ((ztr.properties)::jsonb -> 'herkomst'::text), 'case.result_process_term', ((ztr.properties)::jsonb -> 'procestermijn'::text), 'case.result_process_type_description', ((ztr.properties)::jsonb -> 'procestype_omschrijving'::text), 'case.result_process_type_explanation', ((ztr.properties)::jsonb -> 'procestype_toelichting'::text), 'case.result_process_type_generic', ((ztr.properties)::jsonb -> 'procestype_generiek'::text), 'case.result_process_type_name', ((ztr.properties)::jsonb -> 'procestype_naam'::text), 'case.result_process_type_number', ((ztr.properties)::jsonb -> 'procestype_nummer'::text), 'case.result_process_type_object', ((ztr.properties)::jsonb -> 'procestype_object'::text), 'case.result_selection_list_number', ((ztr.properties)::jsonb -> 'selectielijst_nummer'::text), 'case.retention_period_source_date', ztr.ingang, 'case.type_of_archiving', ztr.archiefnominatie, 'case.period_of_preservation', rpt.label, 'case.period_of_preservation_active',
        CASE
            WHEN (ztr.trigger_archival = true) THEN 'Ja'::text
            ELSE 'Nee'::text
        END, 'case.type_of_archiving', ztr.archiefnominatie, 'case.selection_list', ztr.selectielijst, 'case.active_selection_list', ztr.selectielijst)) || jsonb_build_object('case.lead_time_legal', ztd.afhandeltermijn, 'case.lead_time_service', ztd.servicenorm, 'case.principle_national', ztd.grondslag)) || jsonb_build_object('case.custom_number',
        CASE
            WHEN (char_length(z.prefix) > 0) THEN concat(z.prefix, '-', z.id)
            ELSE (z.id)::text
        END, 'case.number', z.id, 'case.number_master', z.number_master)) || jsonb_build_object('case.casetype', zt.uuid, 'case.casetype.id', zt.id, 'case.casetype.generic_category', bc.naam, 'case.casetype.initiator_type', ztd.handelingsinitiator, 'case.casetype.price.web', ztd.pdc_tarief, 'case.casetype.process_description', ztd.procesbeschrijving, 'case.casetype.publicity', ztd.openbaarheid, 'case.casetype.department', gr.name, 'case.casetype.route_role', ro.name)) || ztn.v0_json) || jsonb_build_object('case.parent_uuid', COALESCE((parent.uuid)::text, ''::text))) || jsonb_build_object('assignee', (z.assignee_v1_json ->> 'reference'::text), 'case.assignee', (z.assignee_v1_json ->> 'preview'::text), 'case.assignee.uuid', (z.assignee_v1_json ->> 'reference'::text), 'case.assignee.email', ((((z.assignee_v1_json -> 'instance'::text) -> 'subject'::text) -> 'instance'::text) ->> 'email_address'::text), 'case.assignee.initials', ((((z.assignee_v1_json -> 'instance'::text) -> 'subject'::text) -> 'instance'::text) ->> 'initials'::text), 'case.assignee.last_name', ((((z.assignee_v1_json -> 'instance'::text) -> 'subject'::text) -> 'instance'::text) ->> 'surname'::text), 'case.assignee.first_names', ((((z.assignee_v1_json -> 'instance'::text) -> 'subject'::text) -> 'instance'::text) ->> 'first_names'::text), 'case.assignee.phone_number', ((((z.assignee_v1_json -> 'instance'::text) -> 'subject'::text) -> 'instance'::text) ->> 'phone_number'::text), 'case.assignee.id', (split_part(((z.assignee_v1_json -> 'instance'::text) ->> 'old_subject_identifier'::text), '-'::text, 3))::integer, 'case.assignee.department', gassign.name, 'case.assignee.title', '')) || jsonb_build_object('coordinator', (z.coordinator_v1_json -> 'reference'::text), 'case.coordinator', (z.coordinator_v1_json -> 'preview'::text), 'case.coordinator.uuid', (z.coordinator_v1_json -> 'reference'::text), 'case.coordinator.email', ((((z.coordinator_v1_json -> 'instance'::text) -> 'subject'::text) -> 'instance'::text) ->> 'email_address'::text), 'case.coordinator.phone_number', ((((z.coordinator_v1_json -> 'instance'::text) -> 'subject'::text) -> 'instance'::text) ->> 'phone_number'::text), 'case.coordinator.id', (split_part(((z.coordinator_v1_json -> 'instance'::text) ->> 'old_subject_identifier'::text), '-'::text, 3))::integer, 'case.coordinator.title', '')) || public.case_subject_as_v0_json(public.hstore(requestor.*), 'requestor'::text, false)) || public.case_subject_as_v0_json(public.hstore(requestor.*), 'requestor'::text, true)) || jsonb_build_object('case.requestor.preset_client',
        CASE
            WHEN (z.preset_client = true) THEN 'Ja'::text
            ELSE 'Nee'::text
        END)) || public.case_subject_as_v0_json(public.hstore(recipient.*), 'recipient'::text, false)) || public.case_subject_as_v0_json(public.hstore(recipient.*), 'recipient'::text, true)) || public.case_location_as_v0_json(public.hstore(case_location.*))) || ( SELECT COALESCE(jsonb_object_agg(concat('attribute.', ca.magic_string), ca.value) FILTER (WHERE (ca.magic_string IS NOT NULL)), '{}'::jsonb) AS attributes
           FROM public.case_attributes_v0 ca
          WHERE (ca.case_id = z.id))) AS "values",
    'case'::text AS object_type,
    '{}'::text[] AS related_objects
   FROM ((((((((((((((((((public.zaak z
     JOIN public.zaak_meta zm ON ((z.id = zm.zaak_id)))
     LEFT JOIN public.zaaktype zt ON ((z.zaaktype_id = zt.id)))
     LEFT JOIN public.zaaktype_node ztn ON (((z.zaaktype_node_id = ztn.id) AND (zt.id = ztn.zaaktype_id))))
     LEFT JOIN public.zaaktype_resultaten ztr ON ((z.resultaat_id = ztr.id)))
     LEFT JOIN public.bibliotheek_categorie bc ON ((zt.bibliotheek_categorie_id = bc.id)))
     LEFT JOIN public.zaaktype_definitie ztd ON ((ztn.zaaktype_definitie_id = ztd.id)))
     LEFT JOIN public.groups gr ON ((z.route_ou = gr.id)))
     LEFT JOIN public.subject assignee ON ((((z.assignee_v1_json ->> 'reference'::text))::uuid = assignee.uuid)))
     LEFT JOIN public.groups gassign ON ((assignee.group_ids[1] = gassign.id)))
     LEFT JOIN public.subject coordinator ON ((((z.coordinator_v1_json ->> 'reference'::text))::uuid = coordinator.uuid)))
     LEFT JOIN public.roles ro ON ((z.route_role = ro.id)))
     LEFT JOIN public.zaak parent ON ((z.pid = parent.id)))
     LEFT JOIN public.zaak_betrokkenen requestor ON (((z.aanvrager = requestor.id) AND (z.id = requestor.zaak_id))))
     LEFT JOIN public.zaak_betrokkenen recipient ON (((z.id = recipient.zaak_id) AND (recipient.rol = 'Ontvanger'::text))))
     LEFT JOIN public.result_preservation_terms rpt ON ((ztr.bewaartermijn = rpt.code)))
     LEFT JOIN public.zaak_bag case_location ON (((case_location.id = z.locatie_zaak) AND (z.id = case_location.zaak_id))))
     LEFT JOIN public.zaaktype_status zts_next ON (((z.zaaktype_node_id = zts_next.zaaktype_node_id) AND (zts_next.status = (z.milestone + 1)))))
     LEFT JOIN public.zaaktype_status zts ON (((z.zaaktype_node_id = zts.zaaktype_node_id) AND (zts.status = z.milestone))));


--
-- Name: casetype_end_status; Type: MATERIALIZED VIEW; Schema: public; Owner: -
--

CREATE MATERIALIZED VIEW public.casetype_end_status AS
 SELECT l1.id,
    l1.zaaktype_node_id,
    l1.status,
    l1.status_type,
    l1.naam,
    l1.created,
    l1.last_modified,
    l1.ou_id,
    l1.role_id,
    l1.checklist,
    l1.fase,
    l1.role_set,
    l1.termijn
   FROM (public.zaaktype_status l1
     JOIN ( SELECT zaaktype_status.zaaktype_node_id,
            max(zaaktype_status.status) AS phase
           FROM public.zaaktype_status
          GROUP BY zaaktype_status.zaaktype_node_id) l2 ON (((l1.status = l2.phase) AND (l1.zaaktype_node_id = l2.zaaktype_node_id))))
  WITH NO DATA;


--
-- Name: casetype_v1_reference; Type: MATERIALIZED VIEW; Schema: public; Owner: -
--

CREATE MATERIALIZED VIEW public.casetype_v1_reference AS
 SELECT zt.id,
    ztn.titel AS title,
    zt.uuid,
    ztn.version,
    ztn.id AS casetype_node_id
   FROM (public.zaaktype zt
     JOIN public.zaaktype_node ztn ON ((ztn.zaaktype_id = zt.id)))
  WITH NO DATA;


--
-- Name: case_v1; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW public.case_v1 AS
 SELECT c.number,
    c.id,
    c.number_parent,
    c.number_master,
    c.number_previous,
    c.subject,
    c.subject_external,
    c.status,
    c.date_created,
    c.date_modified,
    c.date_destruction,
    c.date_of_completion,
    c.date_of_registration,
    c.date_target,
    c.html_email_template,
    c.payment_status,
    c.price,
    c.channel_of_contact,
    c.archival_state,
    c.confidentiality,
    c.stalled_since,
    c.stalled_until,
    c.current_deadline,
    c.deadline_timeline,
    c.result_id,
    c.result,
    c.active_selection_list,
    c.outcome,
    c.casetype,
    c.route,
    c.suspension_rationale,
    c.premature_completion_rationale,
    (zts.fase)::text AS phase,
    c.relations,
    c.case_relationships,
    c.requestor,
    c.assignee,
    c.coordinator,
    c.aggregation_scope,
    c.case_location,
    c.correspondence_location,
    ( SELECT COALESCE(jsonb_object_agg(ca.magic_string, ca.value) FILTER (WHERE (ca.magic_string IS NOT NULL)), '{}'::jsonb) AS "coalesce"
           FROM public.case_attributes_v1 ca
          WHERE (ca.case_id = c.number)) AS attributes,
    ( SELECT json_build_object('preview', zts.fase, 'reference', NULL::unknown, 'type', 'case/milestone', 'instance', json_build_object('date_created', public.timestamp_to_perl_datetime(now()), 'date_modified', public.timestamp_to_perl_datetime(now()), 'phase_label',
                CASE
                    WHEN (zts.id IS NOT NULL) THEN zts.fase
                    ELSE zts_end.fase
                END, 'phase_sequence_number',
                CASE
                    WHEN (zts.id IS NOT NULL) THEN zts.status
                    ELSE zts_end.status
                END, 'milestone_label', zts_previous.naam, 'milestone_sequence_number', zts_previous.status, 'last_sequence_number', zts_end.status)) AS json_build_object
           FROM public.casetype_end_status zts_end
          WHERE (c.zaaktype_node_id = zts_end.zaaktype_node_id)) AS milestone
   FROM ((( SELECT z.id AS number,
            z.uuid AS id,
            z.pid AS number_parent,
            z.number_master,
            z.vervolg_van AS number_previous,
            z.onderwerp AS subject,
            z.onderwerp_extern AS subject_external,
            z.status,
            z.created AS date_created,
            z.last_modified AS date_modified,
            z.vernietigingsdatum AS date_destruction,
            z.afhandeldatum AS date_of_completion,
            z.registratiedatum AS date_of_registration,
            z.streefafhandeldatum AS date_target,
            z.html_email_template,
            z.payment_status,
            z.dutch_price AS price,
            z.contactkanaal AS channel_of_contact,
            z.archival_state,
            z.zaaktype_node_id,
            z.milestone AS raw_milestone,
            public.get_confidential_mapping(z.confidentiality) AS confidentiality,
                CASE
                    WHEN (z.status = 'stalled'::text) THEN zm.stalled_since
                    ELSE NULL::timestamp without time zone
                END AS stalled_since,
                CASE
                    WHEN (z.status = 'stalled'::text) THEN z.stalled_until
                    ELSE NULL::timestamp without time zone
                END AS stalled_until,
            zm.current_deadline,
            zm.deadline_timeline,
            ztr.id AS result_id,
            ztr.resultaat AS result,
            ztr.selectielijst AS active_selection_list,
                CASE
                    WHEN (ztr.id IS NOT NULL) THEN json_build_object('reference', NULL::unknown, 'type', 'case/result', 'preview',
                    CASE
                        WHEN (ztr.label IS NOT NULL) THEN ztr.label
                        ELSE ztr.resultaat
                    END, 'instance', json_build_object('date_created', public.timestamp_to_perl_datetime((ztr.created)::timestamp with time zone), 'date_modified', public.timestamp_to_perl_datetime((ztr.last_modified)::timestamp with time zone), 'archival_type', ztr.archiefnominatie, 'dossier_type', ztr.dossiertype, 'name',
                    CASE
                        WHEN (ztr.label IS NOT NULL) THEN ztr.label
                        ELSE ztr.resultaat
                    END, 'result', ztr.resultaat, 'retention_period', ztr.bewaartermijn, 'selection_list',
                    CASE
                        WHEN (ztr.selectielijst = ''::text) THEN NULL::text
                        ELSE ztr.selectielijst
                    END, 'selection_list_start', ztr.selectielijst_brondatum, 'selection_list_end', ztr.selectielijst_einddatum))
                    ELSE NULL::json
                END AS outcome,
            json_build_object('preview', ct_ref.title, 'reference', ct_ref.uuid, 'instance', json_build_object('version', ct_ref.version, 'name', ct_ref.title), 'type', 'casetype') AS casetype,
            json_build_object('preview', ((gr.name || ', '::text) || role.name), 'reference', NULL::unknown, 'type', 'case/route', 'instance', json_build_object('date_created', public.timestamp_to_perl_datetime(now()), 'date_modified', public.timestamp_to_perl_datetime(now()), 'group', gr.v1_json, 'role', role.v1_json)) AS route,
                CASE
                    WHEN (z.status = 'stalled'::text) THEN zm.opschorten
                    ELSE NULL::character varying
                END AS suspension_rationale,
                CASE
                    WHEN (z.status = 'resolved'::text) THEN zm.afhandeling
                    ELSE NULL::character varying
                END AS premature_completion_rationale,
            json_build_object('type', 'set', 'instance', json_build_object('rows', COALESCE(crp.relationship, '[]'::jsonb))) AS relations,
            json_build_object('parent', z.pid, 'continuation', json_build_object('type', 'set', 'instance', json_build_object('rows', COALESCE(continuation.relationship, '[]'::jsonb))), 'child', json_build_object('type', 'set', 'instance', json_build_object('rows', COALESCE(children.relationship, '[]'::jsonb))), 'plain', json_build_object('type', 'set', 'instance', json_build_object('rows', COALESCE(crp.relationship, '[]'::jsonb)))) AS case_relationships,
            z.requestor_v1_json AS requestor,
            z.assignee_v1_json AS assignee,
            z.coordinator_v1_json AS coordinator,
            'Dossier'::text AS aggregation_scope,
            NULL::text AS case_location,
            NULL::text AS correspondence_location
           FROM ((((((((public.zaak z
             LEFT JOIN public.zaak_meta zm ON ((zm.zaak_id = z.id)))
             LEFT JOIN public.casetype_v1_reference ct_ref ON ((z.zaaktype_node_id = ct_ref.casetype_node_id)))
             LEFT JOIN public.zaaktype_resultaten ztr ON ((z.resultaat_id = ztr.id)))
             LEFT JOIN public.groups gr ON ((z.route_ou = gr.id)))
             LEFT JOIN public.roles role ON ((z.route_role = role.id)))
             LEFT JOIN public.case_relationship_json_view crp ON (((z.id = crp.case_id) AND ((crp.type)::text = 'plain'::text))))
             LEFT JOIN public.case_relationship_json_view continuation ON (((z.id = continuation.case_id) AND ((continuation.type)::text = 'initiator'::text))))
             LEFT JOIN public.case_relationship_json_view children ON (((z.id = children.case_id) AND ((children.type)::text = 'parent'::text))))
          WHERE (z.deleted IS NULL)) c
     LEFT JOIN public.zaaktype_status zts ON (((c.zaaktype_node_id = zts.zaaktype_node_id) AND (zts.status = (c.raw_milestone + 1)))))
     LEFT JOIN public.zaaktype_status zts_previous ON (((c.zaaktype_node_id = zts_previous.zaaktype_node_id) AND (zts_previous.status = c.raw_milestone))));


--
-- Name: checklist; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.checklist (
    id integer NOT NULL,
    case_id integer NOT NULL,
    case_milestone integer
);


--
-- Name: checklist_antwoord_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.checklist_antwoord_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: checklist_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.checklist_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: checklist_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.checklist_id_seq OWNED BY public.checklist.id;


--
-- Name: checklist_item; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.checklist_item (
    id integer NOT NULL,
    checklist_id integer NOT NULL,
    label text,
    state boolean DEFAULT false NOT NULL,
    sequence integer,
    user_defined boolean DEFAULT true NOT NULL,
    deprecated_answer character varying(8),
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    due_date date,
    description text,
    assignee_id integer
);


--
-- Name: checklist_item_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.checklist_item_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: checklist_item_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.checklist_item_id_seq OWNED BY public.checklist_item.id;


--
-- Name: config; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.config (
    id integer NOT NULL,
    parameter character varying(128),
    value text,
    advanced boolean DEFAULT true NOT NULL,
    uuid uuid DEFAULT public.uuid_generate_v4(),
    definition_id uuid NOT NULL
);


--
-- Name: config_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.config_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: config_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.config_id_seq OWNED BY public.config.id;


--
-- Name: contact_data; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.contact_data (
    id integer NOT NULL,
    gegevens_magazijn_id integer,
    betrokkene_type integer,
    mobiel character varying(255),
    telefoonnummer character varying(255),
    email character varying(255),
    created timestamp without time zone,
    last_modified timestamp without time zone,
    note text
);


--
-- Name: contact_data_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.contact_data_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: contact_data_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.contact_data_id_seq OWNED BY public.contact_data.id;


--
-- Name: contact_relationship; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.contact_relationship (
    id integer NOT NULL,
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    contact integer NOT NULL,
    contact_uuid uuid NOT NULL,
    contact_type text NOT NULL,
    relation integer NOT NULL,
    relation_uuid uuid NOT NULL,
    relation_type text NOT NULL
);


--
-- Name: contact_relationship_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.contact_relationship_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: contact_relationship_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.contact_relationship_id_seq OWNED BY public.contact_relationship.id;


--
-- Name: custom_object; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.custom_object (
    id integer NOT NULL,
    uuid uuid NOT NULL,
    custom_object_version_id integer
);


--
-- Name: custom_object_relationship; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.custom_object_relationship (
    id integer NOT NULL,
    relationship_type text NOT NULL,
    relationship_magic_string_prefix text,
    custom_object_id integer NOT NULL,
    custom_object_version_id integer,
    related_document_id integer,
    related_case_id integer,
    related_custom_object_id integer,
    related_uuid uuid NOT NULL,
    related_person_id integer,
    related_organization_id integer,
    related_employee_id integer,
    source_custom_field_type_id integer,
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    CONSTRAINT custom_object_relationship_at_least_one_relationship CHECK ((num_nonnulls(related_document_id, related_case_id, related_custom_object_id, related_person_id, related_employee_id, related_organization_id) = 1)),
    CONSTRAINT source_custom_field_reference CHECK ((((source_custom_field_type_id IS NOT NULL) AND (related_case_id IS NOT NULL)) OR (source_custom_field_type_id IS NULL)))
);


--
-- Name: contact_relationship_view; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW public.contact_relationship_view AS
 SELECT contact_relationship.contact,
    contact_relationship.contact_uuid,
    contact_relationship.contact_type,
    contact_relationship.relation,
    contact_relationship.relation_uuid,
    contact_relationship.relation_type
   FROM public.contact_relationship
UNION ALL
 SELECT custom_object_relationship.related_person_id AS contact,
    custom_object_relationship.related_uuid AS contact_uuid,
    'person'::text AS contact_type,
    custom_object_relationship.custom_object_id AS relation,
    co.uuid AS relation_uuid,
    'custom_object'::text AS relation_type
   FROM (public.custom_object_relationship
     JOIN public.custom_object co ON ((custom_object_relationship.custom_object_id = co.id)))
  WHERE (custom_object_relationship.related_person_id IS NOT NULL)
UNION ALL
 SELECT custom_object_relationship.related_organization_id AS contact,
    custom_object_relationship.related_uuid AS contact_uuid,
    'company'::text AS contact_type,
    custom_object_relationship.custom_object_id AS relation,
    co.uuid AS relation_uuid,
    'custom_object'::text AS relation_type
   FROM (public.custom_object_relationship
     JOIN public.custom_object co ON ((custom_object_relationship.custom_object_id = co.id)))
  WHERE (custom_object_relationship.related_organization_id IS NOT NULL)
UNION ALL
 SELECT custom_object_relationship.related_employee_id AS contact,
    custom_object_relationship.related_uuid AS contact_uuid,
    'employee'::text AS contact_type,
    custom_object_relationship.custom_object_id AS relation,
    co.uuid AS relation_uuid,
    'custom_object'::text AS relation_type
   FROM (public.custom_object_relationship
     JOIN public.custom_object co ON ((custom_object_relationship.custom_object_id = co.id)))
  WHERE (custom_object_relationship.related_employee_id IS NOT NULL);


--
-- Name: contactmoment; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.contactmoment (
    id integer NOT NULL,
    subject_id character varying(100),
    case_id integer,
    type public.contactmoment_type NOT NULL,
    medium public.contactmoment_medium NOT NULL,
    date_created timestamp without time zone DEFAULT now() NOT NULL,
    created_by text NOT NULL,
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL
);


--
-- Name: contactmoment_email; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.contactmoment_email (
    id integer NOT NULL,
    filestore_id integer NOT NULL,
    contactmoment_id integer NOT NULL,
    body text NOT NULL,
    subject text NOT NULL,
    recipient text NOT NULL,
    cc text,
    bcc text
);


--
-- Name: contactmoment_email_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.contactmoment_email_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: contactmoment_email_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.contactmoment_email_id_seq OWNED BY public.contactmoment_email.id;


--
-- Name: contactmoment_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.contactmoment_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: contactmoment_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.contactmoment_id_seq OWNED BY public.contactmoment.id;


--
-- Name: contactmoment_note; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.contactmoment_note (
    id integer NOT NULL,
    message character varying,
    contactmoment_id integer NOT NULL
);


--
-- Name: contactmoment_note_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.contactmoment_note_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: contactmoment_note_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.contactmoment_note_id_seq OWNED BY public.contactmoment_note.id;


--
-- Name: country_code; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.country_code (
    id integer NOT NULL,
    dutch_code integer NOT NULL,
    label text NOT NULL,
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    historical boolean DEFAULT false NOT NULL
);


--
-- Name: country_code_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.country_code_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: country_code_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.country_code_id_seq OWNED BY public.country_code.id;


--
-- Name: country_code_v1_view; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW public.country_code_v1_view AS
 SELECT r.id,
    r.dutch_code,
    r.uuid,
    r.label,
    f.unit_json AS json
   FROM public.country_code r,
    LATERAL public.country_code_to_json(public.hstore(r.*)) f(unit_json);


--
-- Name: custom_object_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.custom_object_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: custom_object_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.custom_object_id_seq OWNED BY public.custom_object.id;


--
-- Name: custom_object_relationship_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.custom_object_relationship_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: custom_object_relationship_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.custom_object_relationship_id_seq OWNED BY public.custom_object_relationship.id;


--
-- Name: custom_object_type; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.custom_object_type (
    id integer NOT NULL,
    uuid uuid NOT NULL,
    catalog_folder_id integer,
    custom_object_type_version_id integer,
    authorization_definition jsonb
);


--
-- Name: custom_object_type_acl; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW public.custom_object_type_acl AS
 SELECT authorization_subquery.custom_object_type_id,
    roles.id AS role_id,
    groups.id AS group_id,
    authorization_subquery."authorization"
   FROM ((( SELECT custom_object_type.id AS custom_object_type_id,
            (t.role ->> 'uuid'::text) AS role_uuid,
            (t.department ->> 'uuid'::text) AS group_uuid,
            t."authorization"
           FROM (public.custom_object_type
             CROSS JOIN LATERAL jsonb_to_recordset((custom_object_type.authorization_definition -> 'authorizations'::text)) t("authorization" text, role json, department json))) authorization_subquery
     JOIN public.roles ON (((roles.uuid)::text = authorization_subquery.role_uuid)))
     JOIN public.groups ON (((groups.uuid)::text = authorization_subquery.group_uuid)));


--
-- Name: custom_object_type_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.custom_object_type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: custom_object_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.custom_object_type_id_seq OWNED BY public.custom_object_type.id;


--
-- Name: custom_object_type_version; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.custom_object_type_version (
    id integer NOT NULL,
    uuid uuid NOT NULL,
    name text,
    title text,
    status public.custom_object_type_status DEFAULT 'active'::public.custom_object_type_status NOT NULL,
    version integer NOT NULL,
    custom_field_definition jsonb,
    authorizations jsonb,
    relationship_definition jsonb,
    date_created timestamp without time zone DEFAULT now(),
    last_modified timestamp without time zone DEFAULT now(),
    date_deleted timestamp without time zone,
    custom_object_type_id integer,
    audit_log jsonb DEFAULT '{}'::jsonb NOT NULL,
    external_reference text,
    subtitle text
);


--
-- Name: custom_object_type_version_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.custom_object_type_version_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: custom_object_type_version_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.custom_object_type_version_id_seq OWNED BY public.custom_object_type_version.id;


--
-- Name: custom_object_version; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.custom_object_version (
    id integer NOT NULL,
    uuid uuid NOT NULL,
    title text,
    status public.custom_object_version_status DEFAULT 'active'::public.custom_object_version_status NOT NULL,
    version integer NOT NULL,
    custom_object_version_content_id integer NOT NULL,
    custom_object_type_version_id integer NOT NULL,
    date_created timestamp without time zone DEFAULT now(),
    last_modified timestamp without time zone DEFAULT now(),
    date_deleted timestamp without time zone,
    custom_object_id integer,
    external_reference text,
    subtitle text
);


--
-- Name: custom_object_version_content; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.custom_object_version_content (
    id integer NOT NULL,
    archive_status public.custom_object_version_content_archive_status,
    archive_ground text,
    archive_retention integer,
    custom_fields jsonb
);


--
-- Name: custom_object_version_content_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.custom_object_version_content_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: custom_object_version_content_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.custom_object_version_content_id_seq OWNED BY public.custom_object_version_content.id;


--
-- Name: custom_object_version_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.custom_object_version_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: custom_object_version_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.custom_object_version_id_seq OWNED BY public.custom_object_version.id;


--
-- Name: directory; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.directory (
    id integer NOT NULL,
    name text NOT NULL,
    case_id integer NOT NULL,
    original_name text NOT NULL,
    path integer[] DEFAULT ARRAY[]::integer[] NOT NULL,
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    CONSTRAINT directory_no_self_parent CHECK ((NOT (ARRAY[id] <@ path)))
);


--
-- Name: directory_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.directory_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: directory_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.directory_id_seq OWNED BY public.directory.id;


--
-- Name: export_queue; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.export_queue (
    id integer NOT NULL,
    uuid uuid DEFAULT public.uuid_generate_v4(),
    subject_id integer NOT NULL,
    subject_uuid uuid NOT NULL,
    expires timestamp with time zone DEFAULT (now() + '3 days'::interval) NOT NULL,
    token text NOT NULL,
    filestore_id integer NOT NULL,
    filestore_uuid uuid NOT NULL,
    downloaded integer DEFAULT 0 NOT NULL
);


--
-- Name: export_queue_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.export_queue_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: export_queue_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.export_queue_id_seq OWNED BY public.export_queue.id;


--
-- Name: file_annotation; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.file_annotation (
    id uuid NOT NULL,
    file_id integer NOT NULL,
    subject character varying(255) NOT NULL,
    properties text DEFAULT '{}'::text NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL,
    modified timestamp without time zone
);


--
-- Name: file_case_document_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.file_case_document_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: file_case_document_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.file_case_document_id_seq OWNED BY public.file_case_document.id;


--
-- Name: file_derivative; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.file_derivative (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    file_id integer NOT NULL,
    filestore_id integer NOT NULL,
    max_width integer NOT NULL,
    max_height integer NOT NULL,
    date_generated timestamp without time zone DEFAULT now() NOT NULL,
    type text NOT NULL,
    CONSTRAINT file_derivative_type CHECK ((type = ANY (ARRAY['pdf'::text, 'thumbnail'::text, 'doc'::text, 'docx'::text])))
);


--
-- Name: file_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.file_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: file_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.file_id_seq OWNED BY public.file.id;


--
-- Name: file_metadata; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.file_metadata (
    id integer NOT NULL,
    description text,
    trust_level text DEFAULT 'Zaakvertrouwelijk'::character varying NOT NULL,
    origin text,
    document_category text,
    origin_date date,
    pronom_format text,
    appearance text,
    structure text,
    creation_date date,
    CONSTRAINT file_metadata_origin_check CHECK ((origin ~ '(Inkomend|Uitgaand|Intern)'::text)),
    CONSTRAINT file_metadata_trust_level_check CHECK ((trust_level ~ '(Openbaar|Beperkt openbaar|Intern|Zaakvertrouwelijk|Vertrouwelijk|Confidentieel|Geheim|Zeer geheim)'::text))
);


--
-- Name: file_metadata_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.file_metadata_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: file_metadata_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.file_metadata_id_seq OWNED BY public.file_metadata.id;


--
-- Name: filestore_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.filestore_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: filestore_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.filestore_id_seq OWNED BY public.filestore.id;


--
-- Name: gegevensmagazijn_subjecten; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.gegevensmagazijn_subjecten (
    subject_uuid uuid NOT NULL,
    nnp_uuid uuid NOT NULL
);


--
-- Name: gm_adres; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.gm_adres (
    id integer NOT NULL,
    straatnaam character varying(80),
    huisnummer bigint,
    huisletter character(1),
    huisnummertoevoeging text,
    nadere_aanduiding character varying(35),
    postcode character varying(6),
    woonplaats character varying(75),
    gemeentedeel character varying(75),
    functie_adres character(1) NOT NULL,
    datum_aanvang_bewoning date,
    woonplaats_id character varying(32),
    gemeente_code smallint,
    hash character varying(32),
    import_datum timestamp(6) without time zone,
    adres_buitenland1 text,
    adres_buitenland2 text,
    adres_buitenland3 text,
    landcode integer DEFAULT 6030,
    natuurlijk_persoon_id integer,
    deleted_on timestamp(6) without time zone,
    bag_id text,
    geo_lat_long point
);


--
-- Name: gm_adres_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.gm_adres_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: gm_adres_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.gm_adres_id_seq OWNED BY public.gm_adres.id;


--
-- Name: gm_bedrijf; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.gm_bedrijf (
    id integer NOT NULL,
    gegevens_magazijn_id integer,
    dossiernummer character varying(8),
    subdossiernummer character varying(4),
    hoofdvestiging_dossiernummer character varying(8),
    hoofdvestiging_subdossiernummer character varying(4),
    vorig_dossiernummer character varying(8),
    vorig_subdossiernummer character varying(4),
    handelsnaam text,
    rechtsvorm smallint,
    kamernummer smallint,
    faillisement smallint,
    surseance smallint,
    telefoonnummer character varying(15),
    email character varying(128),
    vestiging_adres text,
    vestiging_straatnaam text,
    vestiging_huisnummer bigint,
    vestiging_huisnummertoevoeging text,
    vestiging_postcodewoonplaats text,
    vestiging_postcode character varying(6),
    vestiging_woonplaats text,
    correspondentie_adres text,
    correspondentie_straatnaam text,
    correspondentie_huisnummer bigint,
    correspondentie_huisnummertoevoeging text,
    correspondentie_postcodewoonplaats text,
    correspondentie_postcode character varying(6),
    correspondentie_woonplaats text,
    hoofdactiviteitencode integer,
    nevenactiviteitencode1 integer,
    nevenactiviteitencode2 integer,
    werkzamepersonen integer,
    contact_naam character varying(64),
    contact_aanspreektitel character varying(45),
    contact_voorletters character varying(19),
    contact_voorvoegsel character varying(8),
    contact_geslachtsnaam character varying(95),
    contact_geslachtsaanduiding character varying(1),
    authenticated smallint,
    authenticatedby text,
    import_datum timestamp(6) without time zone,
    verblijfsobject_id character varying(16),
    vestigingsnummer bigint,
    vestiging_huisletter text,
    correspondentie_huisletter text,
    vestiging_adres_buitenland1 text,
    vestiging_adres_buitenland2 text,
    vestiging_adres_buitenland3 text,
    vestiging_landcode integer DEFAULT 6030,
    correspondentie_adres_buitenland1 text,
    correspondentie_adres_buitenland2 text,
    correspondentie_adres_buitenland3 text,
    correspondentie_landcode integer DEFAULT 6030,
    rsin bigint,
    oin bigint,
    main_activity jsonb DEFAULT '{}'::jsonb NOT NULL,
    secondairy_activities jsonb DEFAULT '[]'::jsonb NOT NULL,
    date_founded date,
    date_registration date,
    date_ceased date,
    vestiging_latlong point,
    vestiging_bag_id bigint,
    preferred_contact_channel public.preferred_contact_channel DEFAULT 'pip'::public.preferred_contact_channel
);


--
-- Name: gm_bedrijf_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.gm_bedrijf_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: gm_bedrijf_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.gm_bedrijf_id_seq OWNED BY public.gm_bedrijf.id;


--
-- Name: gm_natuurlijk_persoon; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.gm_natuurlijk_persoon (
    id integer NOT NULL,
    gegevens_magazijn_id integer,
    betrokkene_type integer,
    burgerservicenummer character varying(9),
    a_nummer character varying(10),
    voorletters character varying(50),
    voornamen character varying(200),
    geslachtsnaam character varying(200),
    voorvoegsel character varying(50),
    geslachtsaanduiding character varying(3),
    nationaliteitscode1 smallint,
    nationaliteitscode2 smallint,
    nationaliteitscode3 smallint,
    geboorteplaats character varying(75),
    geboorteland character varying(75),
    geboortedatum timestamp without time zone,
    aanhef_aanschrijving character varying(10),
    voorletters_aanschrijving character varying(20),
    voornamen_aanschrijving character varying(200),
    naam_aanschrijving character varying(200),
    voorvoegsel_aanschrijving character varying(50),
    burgerlijke_staat character(1),
    indicatie_geheim character(1),
    import_datum timestamp(6) without time zone,
    adres_id integer,
    authenticatedby text,
    authenticated smallint,
    datum_overlijden timestamp(6) without time zone,
    verblijfsobject_id character varying(16),
    aanduiding_naamgebruik character varying(1),
    onderzoek_persoon boolean,
    onderzoek_huwelijk boolean,
    onderzoek_overlijden boolean,
    onderzoek_verblijfplaats boolean,
    partner_a_nummer character varying(50),
    partner_burgerservicenummer character varying(50),
    partner_voorvoegsel character varying(50),
    partner_geslachtsnaam character varying(50),
    datum_huwelijk timestamp without time zone,
    datum_huwelijk_ontbinding timestamp without time zone,
    landcode integer DEFAULT 6030 NOT NULL,
    naamgebruik text,
    adellijke_titel text,
    preferred_contact_channel public.preferred_contact_channel DEFAULT 'pip'::public.preferred_contact_channel,
    surname text GENERATED ALWAYS AS (COALESCE(naamgebruik, (geslachtsnaam)::text)) STORED
);


--
-- Name: gm_natuurlijk_persoon_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.gm_natuurlijk_persoon_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: gm_natuurlijk_persoon_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.gm_natuurlijk_persoon_id_seq OWNED BY public.gm_natuurlijk_persoon.id;


--
-- Name: groups_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.groups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.groups_id_seq OWNED BY public.groups.id;


--
-- Name: interface; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.interface (
    id integer NOT NULL,
    name text NOT NULL,
    active boolean NOT NULL,
    case_type_id integer,
    max_retries integer NOT NULL,
    interface_config text NOT NULL,
    multiple boolean DEFAULT false NOT NULL,
    module text NOT NULL,
    date_deleted timestamp without time zone,
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    objecttype_id uuid
);


--
-- Name: interface_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.interface_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: interface_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.interface_id_seq OWNED BY public.interface.id;


--
-- Name: legal_entity_type; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.legal_entity_type (
    id integer NOT NULL,
    code integer NOT NULL,
    label text NOT NULL,
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    active boolean DEFAULT true NOT NULL
);


--
-- Name: legal_entity_type_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.legal_entity_type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: legal_entity_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.legal_entity_type_id_seq OWNED BY public.legal_entity_type.id;


--
-- Name: legal_entity_v1_view; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW public.legal_entity_v1_view AS
 SELECT r.id,
    r.code,
    r.uuid,
    r.label,
    f.unit_json AS json
   FROM public.legal_entity_type r,
    LATERAL public.legal_entity_code_to_json(public.hstore(r.*)) f(unit_json);


--
-- Name: logging; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.logging (
    id integer NOT NULL,
    zaak_id integer,
    betrokkene_id character varying(128),
    aanvrager_id character varying(128),
    is_bericht integer,
    component character varying(64),
    component_id integer,
    seen integer,
    onderwerp text,
    bericht text,
    created timestamp without time zone,
    last_modified timestamp without time zone,
    deleted_on timestamp without time zone,
    event_type text,
    event_data text,
    created_by text,
    modified_by text,
    deleted_by text,
    created_for text,
    created_by_name_cache character varying,
    object_uuid uuid,
    restricted boolean DEFAULT false NOT NULL,
    uuid uuid DEFAULT public.uuid_generate_v4()
);


--
-- Name: logging_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.logging_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: logging_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.logging_id_seq OWNED BY public.logging.id;


--
-- Name: message; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.message (
    id integer NOT NULL,
    message text NOT NULL,
    subject_id character varying,
    logging_id integer NOT NULL,
    is_read boolean DEFAULT false,
    is_archived boolean DEFAULT false NOT NULL,
    created timestamp without time zone DEFAULT timezone('UTC'::text, CURRENT_TIMESTAMP) NOT NULL
);


--
-- Name: message_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.message_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: message_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.message_id_seq OWNED BY public.message.id;


--
-- Name: municipality_code; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.municipality_code (
    id integer NOT NULL,
    dutch_code integer NOT NULL,
    label text NOT NULL,
    alternative_name text,
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    historical boolean DEFAULT false NOT NULL
);


--
-- Name: municipality_code_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.municipality_code_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: municipality_code_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.municipality_code_id_seq OWNED BY public.municipality_code.id;


--
-- Name: municipality_code_v1_view; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW public.municipality_code_v1_view AS
 SELECT r.id,
    r.dutch_code,
    r.uuid,
    r.label,
    f.unit_json AS json
   FROM public.municipality_code r,
    LATERAL public.municipality_code_to_json(public.hstore(r.*)) f(unit_json);


--
-- Name: natuurlijk_persoon; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.natuurlijk_persoon (
    search_index tsvector,
    search_term text,
    object_type text DEFAULT 'natuurlijk_persoon'::character varying,
    id integer NOT NULL,
    burgerservicenummer character varying(9),
    a_nummer character varying(10),
    voorletters character varying(50),
    voornamen text,
    geslachtsnaam text,
    voorvoegsel character varying(50),
    geslachtsaanduiding character varying(3),
    nationaliteitscode1 smallint,
    nationaliteitscode2 smallint,
    nationaliteitscode3 smallint,
    geboorteplaats text,
    geboorteland text,
    geboortedatum timestamp without time zone,
    aanhef_aanschrijving character varying(10),
    voorletters_aanschrijving character varying(20),
    voornamen_aanschrijving character varying(200),
    naam_aanschrijving text,
    voorvoegsel_aanschrijving character varying(50),
    burgerlijke_staat character(1),
    indicatie_geheim character(1),
    land_waarnaar_vertrokken smallint,
    import_datum timestamp(6) without time zone,
    adres_id integer,
    authenticated boolean DEFAULT false NOT NULL,
    authenticatedby text,
    deleted_on timestamp(6) without time zone,
    verblijfsobject_id character varying(16),
    datum_overlijden timestamp without time zone,
    aanduiding_naamgebruik character varying(1),
    onderzoek_persoon boolean,
    onderzoek_huwelijk boolean,
    onderzoek_overlijden boolean,
    onderzoek_verblijfplaats boolean,
    partner_a_nummer character varying(50),
    partner_burgerservicenummer character varying(50),
    partner_voorvoegsel character varying(50),
    partner_geslachtsnaam character varying(50),
    datum_huwelijk timestamp without time zone,
    datum_huwelijk_ontbinding timestamp without time zone,
    in_gemeente boolean DEFAULT false,
    landcode integer DEFAULT 6030 NOT NULL,
    naamgebruik text,
    uuid uuid DEFAULT public.uuid_generate_v4(),
    active boolean DEFAULT true NOT NULL,
    adellijke_titel text,
    preferred_contact_channel public.preferred_contact_channel DEFAULT 'pip'::public.preferred_contact_channel,
    pending boolean DEFAULT false NOT NULL,
    related_custom_object_id integer,
    gemeentecode smallint,
    surname text GENERATED ALWAYS AS (COALESCE(naamgebruik, geslachtsnaam)) STORED
)
INHERITS (public.searchable);


--
-- Name: natuurlijk_persoon_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.natuurlijk_persoon_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: natuurlijk_persoon_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.natuurlijk_persoon_id_seq OWNED BY public.natuurlijk_persoon.id;


--
-- Name: object_acl_entry; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.object_acl_entry (
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    object_uuid uuid NOT NULL,
    entity_type text NOT NULL,
    entity_id text NOT NULL,
    capability text NOT NULL,
    scope text DEFAULT 'instance'::text NOT NULL,
    groupname text,
    CONSTRAINT object_acl_entry_groupname_check CHECK (((groupname IS NULL) OR (character_length(groupname) > 0))),
    CONSTRAINT object_acl_entry_scope_check CHECK ((scope = ANY (ARRAY['instance'::text, 'type'::text])))
);


--
-- Name: object_bibliotheek_entry; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.object_bibliotheek_entry (
    search_index tsvector,
    search_term text NOT NULL,
    object_type text NOT NULL,
    searchable_id integer DEFAULT nextval('public.searchable_searchable_id_seq'::regclass),
    id integer NOT NULL,
    bibliotheek_categorie_id integer,
    object_uuid uuid NOT NULL,
    name text NOT NULL
)
INHERITS (public.searchable);


--
-- Name: object_bibliotheek_entry_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.object_bibliotheek_entry_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: object_bibliotheek_entry_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.object_bibliotheek_entry_id_seq OWNED BY public.object_bibliotheek_entry.id;


--
-- Name: object_mutation; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.object_mutation (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    object_uuid uuid,
    object_type text DEFAULT 'object'::text,
    lock_object_uuid uuid,
    type text NOT NULL,
    "values" text DEFAULT '{}'::text NOT NULL,
    date_created timestamp without time zone DEFAULT now(),
    subject_id integer NOT NULL,
    executed boolean DEFAULT false NOT NULL,
    CONSTRAINT object_mutation_type_check CHECK ((type = ANY (ARRAY['create'::text, 'update'::text, 'delete'::text, 'relate'::text])))
);


--
-- Name: object_relation; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.object_relation (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    name text NOT NULL,
    object_type text NOT NULL,
    object_uuid uuid,
    object_embedding text,
    object_id uuid,
    object_preview text,
    CONSTRAINT object_relation_ref_xor_embed CHECK (((object_uuid IS NULL) <> (object_embedding IS NULL)))
);


--
-- Name: object_relationships; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.object_relationships (
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    object1_uuid uuid NOT NULL,
    object2_uuid uuid NOT NULL,
    type1 text NOT NULL,
    type2 text NOT NULL,
    object1_type text NOT NULL,
    object2_type text NOT NULL,
    blocks_deletion boolean DEFAULT false NOT NULL,
    title1 text,
    title2 text,
    owner_object_uuid uuid
);


--
-- Name: object_subscription; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.object_subscription (
    id integer NOT NULL,
    interface_id integer NOT NULL,
    external_id character varying(255) NOT NULL,
    local_table character varying(100) NOT NULL,
    local_id character varying(255) NOT NULL,
    date_created timestamp without time zone DEFAULT now(),
    date_deleted timestamp without time zone,
    object_preview text,
    config_interface_id integer
);


--
-- Name: object_subscription_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.object_subscription_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: object_subscription_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.object_subscription_id_seq OWNED BY public.object_subscription.id;


--
-- Name: parkeergebied; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.parkeergebied (
    id integer NOT NULL,
    bag_hoofdadres bigint,
    postcode character varying(6) NOT NULL,
    straatnaam character varying(255) NOT NULL,
    huisnummer integer,
    huisletter character varying(8),
    huisnummertoevoeging character varying(4),
    parkeergebied_id integer,
    parkeergebied character varying(255),
    created timestamp without time zone,
    last_modified timestamp without time zone,
    woonplaats character varying(255)
);


--
-- Name: parkeergebied_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.parkeergebied_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: parkeergebied_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.parkeergebied_id_seq OWNED BY public.parkeergebied.id;


--
-- Name: queue; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.queue (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    object_id uuid,
    status text DEFAULT 'pending'::text NOT NULL,
    type text NOT NULL,
    label text NOT NULL,
    data text DEFAULT '{}'::text NOT NULL,
    date_created timestamp without time zone DEFAULT statement_timestamp() NOT NULL,
    date_started timestamp without time zone,
    date_finished timestamp without time zone,
    parent_id uuid,
    priority integer DEFAULT 1000 NOT NULL,
    metadata text DEFAULT '{}'::text NOT NULL,
    CONSTRAINT queue_status_check1 CHECK ((status = ANY (ARRAY['pending'::text, 'running'::text, 'finished'::text, 'failed'::text, 'waiting'::text, 'cancelled'::text, 'postponed'::text])))
);


--
-- Name: queue_partitioned; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.queue_partitioned (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    object_id uuid,
    status text DEFAULT 'pending'::text NOT NULL,
    type text NOT NULL,
    label text NOT NULL,
    data text DEFAULT '{}'::text NOT NULL,
    date_created timestamp without time zone DEFAULT statement_timestamp() NOT NULL,
    date_started timestamp without time zone,
    date_finished timestamp without time zone,
    parent_id uuid,
    priority integer DEFAULT 1000 NOT NULL,
    metadata text DEFAULT '{}'::text NOT NULL,
    CONSTRAINT queue_status_check CHECK ((status = ANY (ARRAY['pending'::text, 'running'::text, 'finished'::text, 'failed'::text, 'waiting'::text, 'cancelled'::text, 'postponed'::text])))
)
PARTITION BY RANGE (date_created);


--
-- Name: queue_20210101; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.queue_20210101 (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    object_id uuid,
    status text DEFAULT 'pending'::text NOT NULL,
    type text NOT NULL,
    label text NOT NULL,
    data text DEFAULT '{}'::text NOT NULL,
    date_created timestamp without time zone DEFAULT statement_timestamp() NOT NULL,
    date_started timestamp without time zone,
    date_finished timestamp without time zone,
    parent_id uuid,
    priority integer DEFAULT 1000 NOT NULL,
    metadata text DEFAULT '{}'::text NOT NULL,
    CONSTRAINT queue_status_check CHECK ((status = ANY (ARRAY['pending'::text, 'running'::text, 'finished'::text, 'failed'::text, 'waiting'::text, 'cancelled'::text, 'postponed'::text])))
);
ALTER TABLE ONLY public.queue_partitioned ATTACH PARTITION public.queue_20210101 FOR VALUES FROM ('2021-01-01 00:00:00') TO ('2021-02-01 00:00:00');


--
-- Name: queue_20210201; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.queue_20210201 (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    object_id uuid,
    status text DEFAULT 'pending'::text NOT NULL,
    type text NOT NULL,
    label text NOT NULL,
    data text DEFAULT '{}'::text NOT NULL,
    date_created timestamp without time zone DEFAULT statement_timestamp() NOT NULL,
    date_started timestamp without time zone,
    date_finished timestamp without time zone,
    parent_id uuid,
    priority integer DEFAULT 1000 NOT NULL,
    metadata text DEFAULT '{}'::text NOT NULL,
    CONSTRAINT queue_status_check CHECK ((status = ANY (ARRAY['pending'::text, 'running'::text, 'finished'::text, 'failed'::text, 'waiting'::text, 'cancelled'::text, 'postponed'::text])))
);
ALTER TABLE ONLY public.queue_partitioned ATTACH PARTITION public.queue_20210201 FOR VALUES FROM ('2021-02-01 00:00:00') TO ('2021-03-01 00:00:00');


--
-- Name: queue_20210301; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.queue_20210301 (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    object_id uuid,
    status text DEFAULT 'pending'::text NOT NULL,
    type text NOT NULL,
    label text NOT NULL,
    data text DEFAULT '{}'::text NOT NULL,
    date_created timestamp without time zone DEFAULT statement_timestamp() NOT NULL,
    date_started timestamp without time zone,
    date_finished timestamp without time zone,
    parent_id uuid,
    priority integer DEFAULT 1000 NOT NULL,
    metadata text DEFAULT '{}'::text NOT NULL,
    CONSTRAINT queue_status_check CHECK ((status = ANY (ARRAY['pending'::text, 'running'::text, 'finished'::text, 'failed'::text, 'waiting'::text, 'cancelled'::text, 'postponed'::text])))
);
ALTER TABLE ONLY public.queue_partitioned ATTACH PARTITION public.queue_20210301 FOR VALUES FROM ('2021-03-01 00:00:00') TO ('2021-04-01 00:00:00');


--
-- Name: queue_20210401; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.queue_20210401 (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    object_id uuid,
    status text DEFAULT 'pending'::text NOT NULL,
    type text NOT NULL,
    label text NOT NULL,
    data text DEFAULT '{}'::text NOT NULL,
    date_created timestamp without time zone DEFAULT statement_timestamp() NOT NULL,
    date_started timestamp without time zone,
    date_finished timestamp without time zone,
    parent_id uuid,
    priority integer DEFAULT 1000 NOT NULL,
    metadata text DEFAULT '{}'::text NOT NULL,
    CONSTRAINT queue_status_check CHECK ((status = ANY (ARRAY['pending'::text, 'running'::text, 'finished'::text, 'failed'::text, 'waiting'::text, 'cancelled'::text, 'postponed'::text])))
);
ALTER TABLE ONLY public.queue_partitioned ATTACH PARTITION public.queue_20210401 FOR VALUES FROM ('2021-04-01 00:00:00') TO ('2021-05-01 00:00:00');


--
-- Name: queue_20210501; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.queue_20210501 (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    object_id uuid,
    status text DEFAULT 'pending'::text NOT NULL,
    type text NOT NULL,
    label text NOT NULL,
    data text DEFAULT '{}'::text NOT NULL,
    date_created timestamp without time zone DEFAULT statement_timestamp() NOT NULL,
    date_started timestamp without time zone,
    date_finished timestamp without time zone,
    parent_id uuid,
    priority integer DEFAULT 1000 NOT NULL,
    metadata text DEFAULT '{}'::text NOT NULL,
    CONSTRAINT queue_status_check CHECK ((status = ANY (ARRAY['pending'::text, 'running'::text, 'finished'::text, 'failed'::text, 'waiting'::text, 'cancelled'::text, 'postponed'::text])))
);
ALTER TABLE ONLY public.queue_partitioned ATTACH PARTITION public.queue_20210501 FOR VALUES FROM ('2021-05-01 00:00:00') TO ('2021-06-01 00:00:00');


--
-- Name: queue_20210601; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.queue_20210601 (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    object_id uuid,
    status text DEFAULT 'pending'::text NOT NULL,
    type text NOT NULL,
    label text NOT NULL,
    data text DEFAULT '{}'::text NOT NULL,
    date_created timestamp without time zone DEFAULT statement_timestamp() NOT NULL,
    date_started timestamp without time zone,
    date_finished timestamp without time zone,
    parent_id uuid,
    priority integer DEFAULT 1000 NOT NULL,
    metadata text DEFAULT '{}'::text NOT NULL,
    CONSTRAINT queue_status_check CHECK ((status = ANY (ARRAY['pending'::text, 'running'::text, 'finished'::text, 'failed'::text, 'waiting'::text, 'cancelled'::text, 'postponed'::text])))
);
ALTER TABLE ONLY public.queue_partitioned ATTACH PARTITION public.queue_20210601 FOR VALUES FROM ('2021-06-01 00:00:00') TO ('2021-07-01 00:00:00');


--
-- Name: queue_20210701; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.queue_20210701 (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    object_id uuid,
    status text DEFAULT 'pending'::text NOT NULL,
    type text NOT NULL,
    label text NOT NULL,
    data text DEFAULT '{}'::text NOT NULL,
    date_created timestamp without time zone DEFAULT statement_timestamp() NOT NULL,
    date_started timestamp without time zone,
    date_finished timestamp without time zone,
    parent_id uuid,
    priority integer DEFAULT 1000 NOT NULL,
    metadata text DEFAULT '{}'::text NOT NULL,
    CONSTRAINT queue_status_check CHECK ((status = ANY (ARRAY['pending'::text, 'running'::text, 'finished'::text, 'failed'::text, 'waiting'::text, 'cancelled'::text, 'postponed'::text])))
);
ALTER TABLE ONLY public.queue_partitioned ATTACH PARTITION public.queue_20210701 FOR VALUES FROM ('2021-07-01 00:00:00') TO ('2021-08-01 00:00:00');


--
-- Name: queue_20210801; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.queue_20210801 (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    object_id uuid,
    status text DEFAULT 'pending'::text NOT NULL,
    type text NOT NULL,
    label text NOT NULL,
    data text DEFAULT '{}'::text NOT NULL,
    date_created timestamp without time zone DEFAULT statement_timestamp() NOT NULL,
    date_started timestamp without time zone,
    date_finished timestamp without time zone,
    parent_id uuid,
    priority integer DEFAULT 1000 NOT NULL,
    metadata text DEFAULT '{}'::text NOT NULL,
    CONSTRAINT queue_status_check CHECK ((status = ANY (ARRAY['pending'::text, 'running'::text, 'finished'::text, 'failed'::text, 'waiting'::text, 'cancelled'::text, 'postponed'::text])))
);
ALTER TABLE ONLY public.queue_partitioned ATTACH PARTITION public.queue_20210801 FOR VALUES FROM ('2021-08-01 00:00:00') TO ('2021-09-01 00:00:00');


--
-- Name: queue_20210901; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.queue_20210901 (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    object_id uuid,
    status text DEFAULT 'pending'::text NOT NULL,
    type text NOT NULL,
    label text NOT NULL,
    data text DEFAULT '{}'::text NOT NULL,
    date_created timestamp without time zone DEFAULT statement_timestamp() NOT NULL,
    date_started timestamp without time zone,
    date_finished timestamp without time zone,
    parent_id uuid,
    priority integer DEFAULT 1000 NOT NULL,
    metadata text DEFAULT '{}'::text NOT NULL,
    CONSTRAINT queue_status_check CHECK ((status = ANY (ARRAY['pending'::text, 'running'::text, 'finished'::text, 'failed'::text, 'waiting'::text, 'cancelled'::text, 'postponed'::text])))
);
ALTER TABLE ONLY public.queue_partitioned ATTACH PARTITION public.queue_20210901 FOR VALUES FROM ('2021-09-01 00:00:00') TO ('2021-10-01 00:00:00');


--
-- Name: queue_20211001; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.queue_20211001 (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    object_id uuid,
    status text DEFAULT 'pending'::text NOT NULL,
    type text NOT NULL,
    label text NOT NULL,
    data text DEFAULT '{}'::text NOT NULL,
    date_created timestamp without time zone DEFAULT statement_timestamp() NOT NULL,
    date_started timestamp without time zone,
    date_finished timestamp without time zone,
    parent_id uuid,
    priority integer DEFAULT 1000 NOT NULL,
    metadata text DEFAULT '{}'::text NOT NULL,
    CONSTRAINT queue_status_check CHECK ((status = ANY (ARRAY['pending'::text, 'running'::text, 'finished'::text, 'failed'::text, 'waiting'::text, 'cancelled'::text, 'postponed'::text])))
);
ALTER TABLE ONLY public.queue_partitioned ATTACH PARTITION public.queue_20211001 FOR VALUES FROM ('2021-10-01 00:00:00') TO ('2021-11-01 00:00:00');


--
-- Name: queue_20211101; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.queue_20211101 (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    object_id uuid,
    status text DEFAULT 'pending'::text NOT NULL,
    type text NOT NULL,
    label text NOT NULL,
    data text DEFAULT '{}'::text NOT NULL,
    date_created timestamp without time zone DEFAULT statement_timestamp() NOT NULL,
    date_started timestamp without time zone,
    date_finished timestamp without time zone,
    parent_id uuid,
    priority integer DEFAULT 1000 NOT NULL,
    metadata text DEFAULT '{}'::text NOT NULL,
    CONSTRAINT queue_status_check CHECK ((status = ANY (ARRAY['pending'::text, 'running'::text, 'finished'::text, 'failed'::text, 'waiting'::text, 'cancelled'::text, 'postponed'::text])))
);
ALTER TABLE ONLY public.queue_partitioned ATTACH PARTITION public.queue_20211101 FOR VALUES FROM ('2021-11-01 00:00:00') TO ('2021-12-01 00:00:00');


--
-- Name: queue_20211201; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.queue_20211201 (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    object_id uuid,
    status text DEFAULT 'pending'::text NOT NULL,
    type text NOT NULL,
    label text NOT NULL,
    data text DEFAULT '{}'::text NOT NULL,
    date_created timestamp without time zone DEFAULT statement_timestamp() NOT NULL,
    date_started timestamp without time zone,
    date_finished timestamp without time zone,
    parent_id uuid,
    priority integer DEFAULT 1000 NOT NULL,
    metadata text DEFAULT '{}'::text NOT NULL,
    CONSTRAINT queue_status_check CHECK ((status = ANY (ARRAY['pending'::text, 'running'::text, 'finished'::text, 'failed'::text, 'waiting'::text, 'cancelled'::text, 'postponed'::text])))
);
ALTER TABLE ONLY public.queue_partitioned ATTACH PARTITION public.queue_20211201 FOR VALUES FROM ('2021-12-01 00:00:00') TO ('2022-01-01 00:00:00');


--
-- Name: queue_20xx; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.queue_20xx (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    object_id uuid,
    status text DEFAULT 'pending'::text NOT NULL,
    type text NOT NULL,
    label text NOT NULL,
    data text DEFAULT '{}'::text NOT NULL,
    date_created timestamp without time zone DEFAULT statement_timestamp() NOT NULL,
    date_started timestamp without time zone,
    date_finished timestamp without time zone,
    parent_id uuid,
    priority integer DEFAULT 1000 NOT NULL,
    metadata text DEFAULT '{}'::text NOT NULL,
    CONSTRAINT queue_status_check CHECK ((status = ANY (ARRAY['pending'::text, 'running'::text, 'finished'::text, 'failed'::text, 'waiting'::text, 'cancelled'::text, 'postponed'::text])))
);
ALTER TABLE ONLY public.queue_partitioned ATTACH PARTITION public.queue_20xx FOR VALUES FROM ('1970-01-01 00:00:00') TO ('2021-01-01 00:00:00');


--
-- Name: remote_api_keys; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.remote_api_keys (
    id integer NOT NULL,
    key character varying(60) NOT NULL,
    permissions text NOT NULL
);


--
-- Name: remote_api_keys_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.remote_api_keys_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: remote_api_keys_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.remote_api_keys_id_seq OWNED BY public.remote_api_keys.id;


--
-- Name: result_preservation_terms_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.result_preservation_terms_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: result_preservation_terms_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.result_preservation_terms_id_seq OWNED BY public.result_preservation_terms.id;


--
-- Name: rights; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.rights (
    name text NOT NULL,
    description text NOT NULL
);


--
-- Name: role_rights; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.role_rights (
    rights_name text NOT NULL,
    role_id integer NOT NULL
);


--
-- Name: roles_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.roles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: roles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.roles_id_seq OWNED BY public.roles.id;


--
-- Name: sbus_logging; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.sbus_logging (
    id integer NOT NULL,
    sbus_traffic_id integer,
    pid integer,
    mutatie_type text,
    object text,
    params text,
    kerngegeven text,
    label text,
    changes text,
    error boolean,
    error_message text,
    created timestamp without time zone,
    modified timestamp without time zone
);


--
-- Name: sbus_logging_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.sbus_logging_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: sbus_logging_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.sbus_logging_id_seq OWNED BY public.sbus_logging.id;


--
-- Name: sbus_traffic; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.sbus_traffic (
    id integer NOT NULL,
    sbus_type text,
    object text,
    operation text,
    input text,
    input_raw text,
    output text,
    output_raw text,
    error boolean,
    error_message text,
    created timestamp without time zone,
    modified timestamp without time zone
);


--
-- Name: sbus_traffic_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.sbus_traffic_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: sbus_traffic_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.sbus_traffic_id_seq OWNED BY public.sbus_traffic.id;


--
-- Name: scheduled_jobs; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.scheduled_jobs (
    id integer NOT NULL,
    task character varying(255) NOT NULL,
    scheduled_for timestamp without time zone,
    parameters text,
    created timestamp without time zone,
    last_modified timestamp without time zone,
    deleted timestamp without time zone,
    schedule_type character varying(16),
    case_id integer,
    uuid uuid DEFAULT public.uuid_generate_v4(),
    CONSTRAINT scheduled_jobs_schedule_type_check CHECK (((schedule_type)::text ~ '(manual|time)'::text))
);


--
-- Name: scheduled_jobs_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.scheduled_jobs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: scheduled_jobs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.scheduled_jobs_id_seq OWNED BY public.scheduled_jobs.id;


--
-- Name: search_query; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.search_query (
    id integer NOT NULL,
    settings text,
    ldap_id integer,
    name character varying(256),
    sort_index integer
);


--
-- Name: search_query_delen; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.search_query_delen (
    id integer NOT NULL,
    search_query_id integer,
    ou_id integer,
    role_id integer
);


--
-- Name: search_query_delen_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.search_query_delen_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: search_query_delen_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.search_query_delen_id_seq OWNED BY public.search_query_delen.id;


--
-- Name: search_query_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.search_query_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: search_query_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.search_query_id_seq OWNED BY public.search_query.id;


--
-- Name: service_geojson; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.service_geojson (
    id integer NOT NULL,
    uuid uuid NOT NULL,
    geo_json jsonb NOT NULL
);


--
-- Name: service_geojson_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

ALTER TABLE public.service_geojson ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.service_geojson_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: service_geojson_relationship; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.service_geojson_relationship (
    id integer NOT NULL,
    service_geojson_id integer NOT NULL,
    related_uuid uuid NOT NULL,
    uuid uuid NOT NULL
);


--
-- Name: service_geojson_relationship_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

ALTER TABLE public.service_geojson_relationship ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.service_geojson_relationship_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: session_invitation; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.session_invitation (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    subject_id uuid NOT NULL,
    object_id uuid,
    object_type text,
    date_created timestamp without time zone DEFAULT now() NOT NULL,
    date_expires timestamp without time zone NOT NULL,
    token text NOT NULL,
    action_path text,
    CONSTRAINT object_reference CHECK ((((object_id IS NOT NULL) AND (object_type IS NOT NULL)) OR ((object_id IS NULL) AND (object_type IS NULL))))
);


--
-- Name: settings; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.settings (
    id integer NOT NULL,
    key character varying(255),
    value text
);


--
-- Name: settings_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.settings_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: settings_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.settings_id_seq OWNED BY public.settings.id;


--
-- Name: subject_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.subject_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: subject_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.subject_id_seq OWNED BY public.subject.id;


--
-- Name: subject_login_history; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.subject_login_history (
    id integer NOT NULL,
    ip inet NOT NULL,
    subject_id integer NOT NULL,
    subject_uuid uuid NOT NULL,
    success boolean DEFAULT true NOT NULL,
    method text NOT NULL,
    date_attempt timestamp with time zone DEFAULT now() NOT NULL
);


--
-- Name: subject_login_history_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.subject_login_history_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: subject_login_history_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.subject_login_history_id_seq OWNED BY public.subject_login_history.id;


--
-- Name: thread; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.thread (
    id integer NOT NULL,
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    contact_uuid uuid,
    contact_displayname text,
    case_id integer,
    created timestamp without time zone NOT NULL,
    last_modified timestamp without time zone,
    thread_type text NOT NULL,
    last_message_cache text NOT NULL,
    message_count integer DEFAULT 0 NOT NULL,
    unread_pip_count integer DEFAULT 0 NOT NULL,
    unread_employee_count integer DEFAULT 0 NOT NULL,
    attachment_count integer DEFAULT 0 NOT NULL,
    CONSTRAINT message_count_positive CHECK ((message_count >= 0)),
    CONSTRAINT message_type_check CHECK ((thread_type = ANY (ARRAY['note'::text, 'contact_moment'::text, 'external'::text])))
);


--
-- Name: thread_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.thread_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: thread_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.thread_id_seq OWNED BY public.thread.id;


--
-- Name: thread_message; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.thread_message (
    id integer NOT NULL,
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    thread_id integer NOT NULL,
    type text NOT NULL,
    message_slug text NOT NULL,
    created_by_uuid uuid,
    created_by_displayname text,
    created timestamp without time zone NOT NULL,
    last_modified timestamp without time zone,
    thread_message_note_id integer,
    thread_message_contact_moment_id integer,
    thread_message_external_id integer,
    message_date timestamp without time zone,
    CONSTRAINT check_exactly_one_column CHECK ((((
CASE
    WHEN (thread_message_note_id IS NULL) THEN 0
    ELSE 1
END +
CASE
    WHEN (thread_message_contact_moment_id IS NULL) THEN 0
    ELSE 1
END) +
CASE
    WHEN (thread_message_external_id IS NULL) THEN 0
    ELSE 1
END) = 1)),
    CONSTRAINT message_type_check CHECK ((type = ANY (ARRAY['note'::text, 'contact_moment'::text, 'external'::text])))
);


--
-- Name: thread_message_attachment; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.thread_message_attachment (
    id integer NOT NULL,
    filestore_id integer NOT NULL,
    thread_message_id integer NOT NULL,
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    filename character varying NOT NULL
);


--
-- Name: thread_message_attachment_derivative; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.thread_message_attachment_derivative (
    id integer NOT NULL,
    thread_message_attachment_id integer NOT NULL,
    filestore_id integer NOT NULL,
    max_width integer NOT NULL,
    max_height integer NOT NULL,
    date_generated timestamp without time zone DEFAULT now() NOT NULL,
    type text NOT NULL,
    CONSTRAINT file_derivative_type CHECK ((type = ANY (ARRAY['pdf'::text, 'thumbnail'::text, 'doc'::text, 'docx'::text])))
);


--
-- Name: thread_message_attachment_derivative_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.thread_message_attachment_derivative_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: thread_message_attachment_derivative_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.thread_message_attachment_derivative_id_seq OWNED BY public.thread_message_attachment_derivative.id;


--
-- Name: thread_message_attachment_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.thread_message_attachment_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: thread_message_attachment_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.thread_message_attachment_id_seq OWNED BY public.thread_message_attachment.id;


--
-- Name: thread_message_contact_moment; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.thread_message_contact_moment (
    id integer NOT NULL,
    content text NOT NULL,
    contact_channel text NOT NULL,
    direction text,
    recipient_uuid uuid NOT NULL,
    recipient_displayname text NOT NULL,
    CONSTRAINT contact_moment_channels CHECK ((contact_channel = ANY (ARRAY['assignee'::text, 'frontdesk'::text, 'phone'::text, 'mail'::text, 'email'::text, 'webform'::text, 'social_media'::text, 'external_application'::text]))),
    CONSTRAINT message_direction_check CHECK ((direction = ANY (ARRAY['incoming'::text, 'outgoing'::text])))
);


--
-- Name: thread_message_contact_moment_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.thread_message_contact_moment_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: thread_message_contact_moment_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.thread_message_contact_moment_id_seq OWNED BY public.thread_message_contact_moment.id;


--
-- Name: thread_message_external; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.thread_message_external (
    id integer NOT NULL,
    type text NOT NULL,
    content text NOT NULL,
    subject text NOT NULL,
    participants text DEFAULT '[]'::text NOT NULL,
    direction text NOT NULL,
    source_file_id integer,
    read_pip timestamp without time zone,
    read_employee timestamp without time zone,
    attachment_count integer DEFAULT 0 NOT NULL,
    failure_reason text,
    CONSTRAINT external_message_type CHECK ((type = ANY (ARRAY['pip'::text, 'email'::text, 'postex'::text]))),
    CONSTRAINT thread_message_external_direction_constraint CHECK ((direction = ANY (ARRAY['incoming'::text, 'outgoing'::text, 'unspecified'::text])))
);


--
-- Name: thread_message_external_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.thread_message_external_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: thread_message_external_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.thread_message_external_id_seq OWNED BY public.thread_message_external.id;


--
-- Name: thread_message_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.thread_message_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: thread_message_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.thread_message_id_seq OWNED BY public.thread_message.id;


--
-- Name: thread_message_note; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.thread_message_note (
    id integer NOT NULL,
    content text NOT NULL
);


--
-- Name: thread_message_note_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.thread_message_note_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: thread_message_note_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.thread_message_note_id_seq OWNED BY public.thread_message_note.id;


--
-- Name: transaction; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.transaction (
    id integer NOT NULL,
    interface_id integer NOT NULL,
    external_transaction_id character varying(250),
    input_data text,
    input_file integer,
    automated_retry_count integer,
    date_created timestamp without time zone DEFAULT now() NOT NULL,
    date_last_retry timestamp without time zone,
    date_next_retry timestamp without time zone,
    processed boolean DEFAULT false,
    date_deleted timestamp without time zone,
    error_count integer DEFAULT 0,
    direction character varying(255) DEFAULT 'incoming'::character varying NOT NULL,
    success_count integer DEFAULT 0,
    total_count integer DEFAULT 0,
    processor_params text,
    error_fatal boolean,
    preview_data text DEFAULT '{}'::text NOT NULL,
    error_message text,
    text_vector tsvector,
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    CONSTRAINT input_data_or_input_file CHECK (((input_data IS NOT NULL) OR (input_file IS NOT NULL))),
    CONSTRAINT transaction_direction_check CHECK ((((direction)::text = 'incoming'::text) OR ((direction)::text = 'outgoing'::text)))
);


--
-- Name: transaction_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.transaction_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: transaction_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.transaction_id_seq OWNED BY public.transaction.id;


--
-- Name: transaction_record; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.transaction_record (
    id integer NOT NULL,
    transaction_id integer,
    input text NOT NULL,
    output text NOT NULL,
    is_error boolean DEFAULT false NOT NULL,
    date_executed timestamp without time zone DEFAULT now() NOT NULL,
    date_deleted timestamp without time zone,
    preview_string text,
    last_error text,
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL
);


--
-- Name: transaction_record_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.transaction_record_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: transaction_record_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.transaction_record_id_seq OWNED BY public.transaction_record.id;


--
-- Name: transaction_record_to_object; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.transaction_record_to_object (
    id integer NOT NULL,
    transaction_record_id integer,
    local_table character varying(100),
    local_id character varying(255),
    mutations text,
    date_deleted timestamp without time zone,
    mutation_type character varying(100)
);


--
-- Name: transaction_record_to_object_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.transaction_record_to_object_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: transaction_record_to_object_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.transaction_record_to_object_id_seq OWNED BY public.transaction_record_to_object.id;


--
-- Name: user_app_lock; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.user_app_lock (
    type character(40) NOT NULL,
    type_id character(20) NOT NULL,
    create_unixtime integer NOT NULL,
    session_id character(40) NOT NULL,
    uidnumber integer NOT NULL
);


--
-- Name: user_entity; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.user_entity (
    id integer NOT NULL,
    uuid uuid DEFAULT public.uuid_generate_v4(),
    source_interface_id integer,
    source_identifier text NOT NULL,
    subject_id integer,
    date_created timestamp without time zone,
    date_deleted timestamp without time zone,
    properties text DEFAULT '{}'::text,
    password character varying(255),
    active boolean DEFAULT true NOT NULL
);


--
-- Name: user_entity_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.user_entity_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: user_entity_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.user_entity_id_seq OWNED BY public.user_entity.id;


--
-- Name: woz_objects; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.woz_objects (
    id integer NOT NULL,
    object_data text,
    owner character varying(255) NOT NULL,
    object_id character varying(32) NOT NULL
);


--
-- Name: woz_objects_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.woz_objects_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: woz_objects_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.woz_objects_id_seq OWNED BY public.woz_objects.id;


--
-- Name: zaak_authorisation_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.zaak_authorisation_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: zaak_authorisation_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.zaak_authorisation_id_seq OWNED BY public.zaak_authorisation.id;


--
-- Name: zaak_bag_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.zaak_bag_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: zaak_bag_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.zaak_bag_id_seq OWNED BY public.zaak_bag.id;


--
-- Name: zaak_betrokkenen_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.zaak_betrokkenen_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: zaak_betrokkenen_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.zaak_betrokkenen_id_seq OWNED BY public.zaak_betrokkenen.id;


--
-- Name: zaak_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.zaak_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: zaak_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.zaak_id_seq OWNED BY public.zaak.id;


--
-- Name: zaak_kenmerk_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.zaak_kenmerk_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: zaak_kenmerk_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.zaak_kenmerk_id_seq OWNED BY public.zaak_kenmerk.id;


--
-- Name: zaak_meta_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.zaak_meta_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: zaak_meta_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.zaak_meta_id_seq OWNED BY public.zaak_meta.id;


--
-- Name: zaak_onafgerond; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.zaak_onafgerond (
    zaaktype_id integer NOT NULL,
    betrokkene character(50) NOT NULL,
    json_string text NOT NULL,
    afronden boolean,
    create_unixtime integer
);


--
-- Name: zaak_subcase; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.zaak_subcase (
    id integer NOT NULL,
    zaak_id integer NOT NULL,
    relation_zaak_id integer,
    required integer
);


--
-- Name: zaak_subcase_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.zaak_subcase_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: zaak_subcase_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.zaak_subcase_id_seq OWNED BY public.zaak_subcase.id;


--
-- Name: zaaktype_authorisation_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.zaaktype_authorisation_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: zaaktype_authorisation_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.zaaktype_authorisation_id_seq OWNED BY public.zaaktype_authorisation.id;


--
-- Name: zaaktype_betrokkenen; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.zaaktype_betrokkenen (
    id integer NOT NULL,
    zaaktype_node_id integer,
    betrokkene_type text,
    created timestamp without time zone,
    last_modified timestamp without time zone
);


--
-- Name: zaaktype_betrokkenen_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.zaaktype_betrokkenen_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: zaaktype_betrokkenen_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.zaaktype_betrokkenen_id_seq OWNED BY public.zaaktype_betrokkenen.id;


--
-- Name: zaaktype_definitie_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.zaaktype_definitie_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: zaaktype_definitie_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.zaaktype_definitie_id_seq OWNED BY public.zaaktype_definitie.id;


--
-- Name: zaaktype_document_kenmerken_map; Type: MATERIALIZED VIEW; Schema: public; Owner: -
--

CREATE MATERIALIZED VIEW public.zaaktype_document_kenmerken_map AS
 SELECT t.zaaktype_node_id,
    t.referential,
    t.bibliotheek_kenmerken_id,
    t.magic_string,
    t.name,
    t.public_name,
    t.case_document_uuid,
    t.case_document_id,
    t.show_on_pip,
    t.show_on_website
   FROM ( SELECT ztk.zaaktype_node_id,
            ztk.referential,
            ztk.bibliotheek_kenmerken_id,
            (COALESCE(ztk.pip, 0))::boolean AS show_on_pip,
            (COALESCE(ztk.publish_public, 0))::boolean AS show_on_website,
            bk.magic_string,
            bk.naam AS name,
            COALESCE(NULLIF(ztk.label, ''::text), (bk.naam)::text) AS public_name,
            ztk.uuid AS case_document_uuid,
            ztk.id AS case_document_id,
            row_number() OVER (PARTITION BY ztk.bibliotheek_kenmerken_id, ztk.zaaktype_node_id ORDER BY zs.status, ztk.id) AS row_num
           FROM ((public.zaaktype_kenmerken ztk
             JOIN public.zaaktype_status zs ON ((zs.id = ztk.zaak_status_id)))
             JOIN public.bibliotheek_kenmerken bk ON (((bk.id = ztk.bibliotheek_kenmerken_id) AND (bk.value_type = 'file'::text))))) t
  WHERE (t.row_num = 1)
  WITH NO DATA;


--
-- Name: zaaktype_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.zaaktype_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: zaaktype_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.zaaktype_id_seq OWNED BY public.zaaktype.id;


--
-- Name: zaaktype_kenmerken_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.zaaktype_kenmerken_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: zaaktype_kenmerken_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.zaaktype_kenmerken_id_seq OWNED BY public.zaaktype_kenmerken.id;


--
-- Name: zaaktype_node_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.zaaktype_node_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: zaaktype_node_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.zaaktype_node_id_seq OWNED BY public.zaaktype_node.id;


--
-- Name: zaaktype_notificatie; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.zaaktype_notificatie (
    id integer NOT NULL,
    zaaktype_node_id integer,
    zaak_status_id integer,
    label text,
    rcpt text,
    onderwerp text,
    bericht text,
    created timestamp without time zone,
    last_modified timestamp without time zone,
    intern_block integer,
    email text,
    bibliotheek_notificaties_id integer,
    behandelaar character varying(255),
    automatic integer,
    cc text,
    bcc text,
    betrokkene_role text
);


--
-- Name: zaaktype_notificatie_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.zaaktype_notificatie_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: zaaktype_notificatie_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.zaaktype_notificatie_id_seq OWNED BY public.zaaktype_notificatie.id;


--
-- Name: zaaktype_regel; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.zaaktype_regel (
    id integer NOT NULL,
    zaaktype_node_id integer,
    zaak_status_id integer,
    naam text,
    created timestamp without time zone,
    last_modified timestamp without time zone,
    settings text,
    active boolean DEFAULT false,
    is_group boolean DEFAULT false NOT NULL
);


--
-- Name: zaaktype_regel_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.zaaktype_regel_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: zaaktype_regel_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.zaaktype_regel_id_seq OWNED BY public.zaaktype_regel.id;


--
-- Name: zaaktype_relatie; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.zaaktype_relatie (
    id integer NOT NULL,
    zaaktype_node_id integer,
    relatie_zaaktype_id integer,
    zaaktype_status_id integer,
    relatie_type text,
    eigenaar_type text DEFAULT 'aanvrager'::text NOT NULL,
    start_delay character varying(255),
    created timestamp without time zone,
    last_modified timestamp without time zone,
    kopieren_kenmerken integer,
    ou_id integer,
    role_id integer,
    automatisch_behandelen boolean,
    required character varying(12),
    betrokkene_authorized boolean,
    betrokkene_notify boolean,
    betrokkene_id text,
    betrokkene_role text,
    betrokkene_role_set text,
    betrokkene_prefix text,
    eigenaar_id text,
    eigenaar_role text,
    show_in_pip boolean DEFAULT false NOT NULL,
    pip_label text,
    subject_role text[],
    copy_subject_role boolean DEFAULT false,
    copy_related_cases boolean DEFAULT false NOT NULL,
    copy_related_objects boolean DEFAULT false NOT NULL,
    copy_selected_attributes public.hstore,
    creation_style character varying DEFAULT 'background'::character varying NOT NULL,
    status boolean DEFAULT false NOT NULL,
    CONSTRAINT zaaktype_relatie_creation_style_check CHECK (((creation_style)::text = ANY (ARRAY[('background'::character varying)::text, ('form'::character varying)::text])))
);


--
-- Name: zaaktype_relatie_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.zaaktype_relatie_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: zaaktype_relatie_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.zaaktype_relatie_id_seq OWNED BY public.zaaktype_relatie.id;


--
-- Name: zaaktype_resultaten_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.zaaktype_resultaten_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: zaaktype_resultaten_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.zaaktype_resultaten_id_seq OWNED BY public.zaaktype_resultaten.id;


--
-- Name: zaaktype_sjablonen; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.zaaktype_sjablonen (
    id integer NOT NULL,
    zaaktype_node_id integer,
    bibliotheek_sjablonen_id integer,
    help text,
    zaak_status_id integer,
    created timestamp without time zone,
    last_modified timestamp without time zone,
    automatisch_genereren integer,
    bibliotheek_kenmerken_id integer,
    target_format character varying(5),
    custom_filename text,
    label text
);


--
-- Name: zaaktype_sjablonen_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.zaaktype_sjablonen_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: zaaktype_sjablonen_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.zaaktype_sjablonen_id_seq OWNED BY public.zaaktype_sjablonen.id;


--
-- Name: zaaktype_standaard_betrokkenen; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.zaaktype_standaard_betrokkenen (
    id integer NOT NULL,
    zaaktype_node_id integer NOT NULL,
    zaak_status_id integer NOT NULL,
    betrokkene_type text NOT NULL,
    betrokkene_identifier text NOT NULL,
    naam text NOT NULL,
    rol text NOT NULL,
    magic_string_prefix text,
    gemachtigd boolean DEFAULT false NOT NULL,
    notify boolean DEFAULT false NOT NULL,
    uuid uuid DEFAULT public.uuid_generate_v4()
);


--
-- Name: zaaktype_standaard_betrokkenen_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.zaaktype_standaard_betrokkenen_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: zaaktype_standaard_betrokkenen_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.zaaktype_standaard_betrokkenen_id_seq OWNED BY public.zaaktype_standaard_betrokkenen.id;


--
-- Name: zaaktype_status_checklist_item; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.zaaktype_status_checklist_item (
    id integer NOT NULL,
    casetype_status_id integer NOT NULL,
    label text,
    external_reference text
);


--
-- Name: zaaktype_status_checklist_item_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.zaaktype_status_checklist_item_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: zaaktype_status_checklist_item_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.zaaktype_status_checklist_item_id_seq OWNED BY public.zaaktype_status_checklist_item.id;


--
-- Name: zaaktype_status_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.zaaktype_status_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: zaaktype_status_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.zaaktype_status_id_seq OWNED BY public.zaaktype_status.id;


--
-- Name: zorginstituut_identificatie_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.zorginstituut_identificatie_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: adres id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.adres ALTER COLUMN id SET DEFAULT nextval('public.adres_id_seq'::regclass);


--
-- Name: bag_ligplaats id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bag_ligplaats ALTER COLUMN id SET DEFAULT nextval('public.bag_ligplaats_id_seq'::regclass);


--
-- Name: bag_nummeraanduiding id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bag_nummeraanduiding ALTER COLUMN id SET DEFAULT nextval('public.bag_nummeraanduiding_id_seq'::regclass);


--
-- Name: bag_openbareruimte id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bag_openbareruimte ALTER COLUMN id SET DEFAULT nextval('public.bag_openbareruimte_id_seq'::regclass);


--
-- Name: bag_pand id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bag_pand ALTER COLUMN id SET DEFAULT nextval('public.bag_pand_id_seq'::regclass);


--
-- Name: bag_standplaats id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bag_standplaats ALTER COLUMN id SET DEFAULT nextval('public.bag_standplaats_id_seq'::regclass);


--
-- Name: bag_verblijfsobject id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bag_verblijfsobject ALTER COLUMN id SET DEFAULT nextval('public.bag_verblijfsobject_id_seq'::regclass);


--
-- Name: bag_verblijfsobject_gebruiksdoel id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bag_verblijfsobject_gebruiksdoel ALTER COLUMN id SET DEFAULT nextval('public.bag_verblijfsobject_gebruiksdoel_id_seq'::regclass);


--
-- Name: bag_verblijfsobject_pand id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bag_verblijfsobject_pand ALTER COLUMN id SET DEFAULT nextval('public.bag_verblijfsobject_pand_id_seq'::regclass);


--
-- Name: bag_woonplaats id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bag_woonplaats ALTER COLUMN id SET DEFAULT nextval('public.bag_woonplaats_id_seq'::regclass);


--
-- Name: bedrijf searchable_id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bedrijf ALTER COLUMN searchable_id SET DEFAULT nextval('public.searchable_searchable_id_seq'::regclass);


--
-- Name: bedrijf id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bedrijf ALTER COLUMN id SET DEFAULT nextval('public.bedrijf_id_seq'::regclass);


--
-- Name: bedrijf_authenticatie id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bedrijf_authenticatie ALTER COLUMN id SET DEFAULT nextval('public.bedrijf_authenticatie_id_seq'::regclass);


--
-- Name: beheer_import id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.beheer_import ALTER COLUMN id SET DEFAULT nextval('public.beheer_import_id_seq'::regclass);


--
-- Name: beheer_import_log id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.beheer_import_log ALTER COLUMN id SET DEFAULT nextval('public.beheer_import_log_id_seq'::regclass);


--
-- Name: betrokkene_notes id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.betrokkene_notes ALTER COLUMN id SET DEFAULT nextval('public.betrokkene_notes_id_seq'::regclass);


--
-- Name: betrokkenen id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.betrokkenen ALTER COLUMN id SET DEFAULT nextval('public.betrokkenen_id_seq'::regclass);


--
-- Name: bibliotheek_categorie searchable_id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bibliotheek_categorie ALTER COLUMN searchable_id SET DEFAULT nextval('public.searchable_searchable_id_seq'::regclass);


--
-- Name: bibliotheek_categorie id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bibliotheek_categorie ALTER COLUMN id SET DEFAULT nextval('public.bibliotheek_categorie_id_seq'::regclass);


--
-- Name: bibliotheek_kenmerken searchable_id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bibliotheek_kenmerken ALTER COLUMN searchable_id SET DEFAULT nextval('public.searchable_searchable_id_seq'::regclass);


--
-- Name: bibliotheek_kenmerken id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bibliotheek_kenmerken ALTER COLUMN id SET DEFAULT nextval('public.bibliotheek_kenmerken_id_seq'::regclass);


--
-- Name: bibliotheek_kenmerken_values id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bibliotheek_kenmerken_values ALTER COLUMN id SET DEFAULT nextval('public.bibliotheek_kenmerken_values_id_seq'::regclass);


--
-- Name: bibliotheek_kenmerken_values sort_order; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bibliotheek_kenmerken_values ALTER COLUMN sort_order SET DEFAULT nextval('public.bibliotheek_kenmerken_values_sort_order_seq'::regclass);


--
-- Name: bibliotheek_notificatie_kenmerk id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bibliotheek_notificatie_kenmerk ALTER COLUMN id SET DEFAULT nextval('public.bibliotheek_notificatie_kenmerk_id_seq'::regclass);


--
-- Name: bibliotheek_notificaties searchable_id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bibliotheek_notificaties ALTER COLUMN searchable_id SET DEFAULT nextval('public.searchable_searchable_id_seq'::regclass);


--
-- Name: bibliotheek_notificaties id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bibliotheek_notificaties ALTER COLUMN id SET DEFAULT nextval('public.bibliotheek_notificaties_id_seq'::regclass);


--
-- Name: bibliotheek_sjablonen searchable_id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bibliotheek_sjablonen ALTER COLUMN searchable_id SET DEFAULT nextval('public.searchable_searchable_id_seq'::regclass);


--
-- Name: bibliotheek_sjablonen id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bibliotheek_sjablonen ALTER COLUMN id SET DEFAULT nextval('public.bibliotheek_sjablonen_id_seq'::regclass);


--
-- Name: bibliotheek_sjablonen_magic_string id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bibliotheek_sjablonen_magic_string ALTER COLUMN id SET DEFAULT nextval('public.bibliotheek_sjablonen_magic_string_id_seq'::regclass);


--
-- Name: case_action id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.case_action ALTER COLUMN id SET DEFAULT nextval('public.case_action_id_seq'::regclass);


--
-- Name: case_relation id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.case_relation ALTER COLUMN id SET DEFAULT nextval('public.case_relation_id_seq'::regclass);


--
-- Name: checklist id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.checklist ALTER COLUMN id SET DEFAULT nextval('public.checklist_id_seq'::regclass);


--
-- Name: checklist_item id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.checklist_item ALTER COLUMN id SET DEFAULT nextval('public.checklist_item_id_seq'::regclass);


--
-- Name: config id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.config ALTER COLUMN id SET DEFAULT nextval('public.config_id_seq'::regclass);


--
-- Name: contact_data id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.contact_data ALTER COLUMN id SET DEFAULT nextval('public.contact_data_id_seq'::regclass);


--
-- Name: contact_relationship id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.contact_relationship ALTER COLUMN id SET DEFAULT nextval('public.contact_relationship_id_seq'::regclass);


--
-- Name: contactmoment id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.contactmoment ALTER COLUMN id SET DEFAULT nextval('public.contactmoment_id_seq'::regclass);


--
-- Name: contactmoment_email id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.contactmoment_email ALTER COLUMN id SET DEFAULT nextval('public.contactmoment_email_id_seq'::regclass);


--
-- Name: contactmoment_note id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.contactmoment_note ALTER COLUMN id SET DEFAULT nextval('public.contactmoment_note_id_seq'::regclass);


--
-- Name: country_code id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.country_code ALTER COLUMN id SET DEFAULT nextval('public.country_code_id_seq'::regclass);


--
-- Name: custom_object id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.custom_object ALTER COLUMN id SET DEFAULT nextval('public.custom_object_id_seq'::regclass);


--
-- Name: custom_object_relationship id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.custom_object_relationship ALTER COLUMN id SET DEFAULT nextval('public.custom_object_relationship_id_seq'::regclass);


--
-- Name: custom_object_type id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.custom_object_type ALTER COLUMN id SET DEFAULT nextval('public.custom_object_type_id_seq'::regclass);


--
-- Name: custom_object_type_version id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.custom_object_type_version ALTER COLUMN id SET DEFAULT nextval('public.custom_object_type_version_id_seq'::regclass);


--
-- Name: custom_object_version id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.custom_object_version ALTER COLUMN id SET DEFAULT nextval('public.custom_object_version_id_seq'::regclass);


--
-- Name: custom_object_version_content id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.custom_object_version_content ALTER COLUMN id SET DEFAULT nextval('public.custom_object_version_content_id_seq'::regclass);


--
-- Name: directory id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.directory ALTER COLUMN id SET DEFAULT nextval('public.directory_id_seq'::regclass);


--
-- Name: export_queue id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.export_queue ALTER COLUMN id SET DEFAULT nextval('public.export_queue_id_seq'::regclass);


--
-- Name: file id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.file ALTER COLUMN id SET DEFAULT nextval('public.file_id_seq'::regclass);


--
-- Name: file_case_document id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.file_case_document ALTER COLUMN id SET DEFAULT nextval('public.file_case_document_id_seq'::regclass);


--
-- Name: file_metadata id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.file_metadata ALTER COLUMN id SET DEFAULT nextval('public.file_metadata_id_seq'::regclass);


--
-- Name: filestore id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.filestore ALTER COLUMN id SET DEFAULT nextval('public.filestore_id_seq'::regclass);


--
-- Name: gm_adres id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.gm_adres ALTER COLUMN id SET DEFAULT nextval('public.gm_adres_id_seq'::regclass);


--
-- Name: gm_bedrijf id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.gm_bedrijf ALTER COLUMN id SET DEFAULT nextval('public.gm_bedrijf_id_seq'::regclass);


--
-- Name: gm_natuurlijk_persoon id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.gm_natuurlijk_persoon ALTER COLUMN id SET DEFAULT nextval('public.gm_natuurlijk_persoon_id_seq'::regclass);


--
-- Name: groups id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.groups ALTER COLUMN id SET DEFAULT nextval('public.groups_id_seq'::regclass);


--
-- Name: interface id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.interface ALTER COLUMN id SET DEFAULT nextval('public.interface_id_seq'::regclass);


--
-- Name: legal_entity_type id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.legal_entity_type ALTER COLUMN id SET DEFAULT nextval('public.legal_entity_type_id_seq'::regclass);


--
-- Name: logging id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.logging ALTER COLUMN id SET DEFAULT nextval('public.logging_id_seq'::regclass);


--
-- Name: message id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.message ALTER COLUMN id SET DEFAULT nextval('public.message_id_seq'::regclass);


--
-- Name: municipality_code id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.municipality_code ALTER COLUMN id SET DEFAULT nextval('public.municipality_code_id_seq'::regclass);


--
-- Name: natuurlijk_persoon searchable_id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.natuurlijk_persoon ALTER COLUMN searchable_id SET DEFAULT nextval('public.searchable_searchable_id_seq'::regclass);


--
-- Name: natuurlijk_persoon id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.natuurlijk_persoon ALTER COLUMN id SET DEFAULT nextval('public.natuurlijk_persoon_id_seq'::regclass);


--
-- Name: object_bibliotheek_entry id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.object_bibliotheek_entry ALTER COLUMN id SET DEFAULT nextval('public.object_bibliotheek_entry_id_seq'::regclass);


--
-- Name: object_subscription id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.object_subscription ALTER COLUMN id SET DEFAULT nextval('public.object_subscription_id_seq'::regclass);


--
-- Name: parkeergebied id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.parkeergebied ALTER COLUMN id SET DEFAULT nextval('public.parkeergebied_id_seq'::regclass);


--
-- Name: remote_api_keys id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.remote_api_keys ALTER COLUMN id SET DEFAULT nextval('public.remote_api_keys_id_seq'::regclass);


--
-- Name: result_preservation_terms id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.result_preservation_terms ALTER COLUMN id SET DEFAULT nextval('public.result_preservation_terms_id_seq'::regclass);


--
-- Name: roles id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.roles ALTER COLUMN id SET DEFAULT nextval('public.roles_id_seq'::regclass);


--
-- Name: sbus_logging id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sbus_logging ALTER COLUMN id SET DEFAULT nextval('public.sbus_logging_id_seq'::regclass);


--
-- Name: sbus_traffic id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sbus_traffic ALTER COLUMN id SET DEFAULT nextval('public.sbus_traffic_id_seq'::regclass);


--
-- Name: scheduled_jobs id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.scheduled_jobs ALTER COLUMN id SET DEFAULT nextval('public.scheduled_jobs_id_seq'::regclass);


--
-- Name: search_query id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.search_query ALTER COLUMN id SET DEFAULT nextval('public.search_query_id_seq'::regclass);


--
-- Name: search_query_delen id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.search_query_delen ALTER COLUMN id SET DEFAULT nextval('public.search_query_delen_id_seq'::regclass);


--
-- Name: searchable searchable_id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.searchable ALTER COLUMN searchable_id SET DEFAULT nextval('public.searchable_searchable_id_seq'::regclass);


--
-- Name: settings id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.settings ALTER COLUMN id SET DEFAULT nextval('public.settings_id_seq'::regclass);


--
-- Name: subject id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.subject ALTER COLUMN id SET DEFAULT nextval('public.subject_id_seq'::regclass);


--
-- Name: subject_login_history id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.subject_login_history ALTER COLUMN id SET DEFAULT nextval('public.subject_login_history_id_seq'::regclass);


--
-- Name: thread id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.thread ALTER COLUMN id SET DEFAULT nextval('public.thread_id_seq'::regclass);


--
-- Name: thread_message id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.thread_message ALTER COLUMN id SET DEFAULT nextval('public.thread_message_id_seq'::regclass);


--
-- Name: thread_message_attachment id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.thread_message_attachment ALTER COLUMN id SET DEFAULT nextval('public.thread_message_attachment_id_seq'::regclass);


--
-- Name: thread_message_attachment_derivative id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.thread_message_attachment_derivative ALTER COLUMN id SET DEFAULT nextval('public.thread_message_attachment_derivative_id_seq'::regclass);


--
-- Name: thread_message_contact_moment id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.thread_message_contact_moment ALTER COLUMN id SET DEFAULT nextval('public.thread_message_contact_moment_id_seq'::regclass);


--
-- Name: thread_message_external id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.thread_message_external ALTER COLUMN id SET DEFAULT nextval('public.thread_message_external_id_seq'::regclass);


--
-- Name: thread_message_note id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.thread_message_note ALTER COLUMN id SET DEFAULT nextval('public.thread_message_note_id_seq'::regclass);


--
-- Name: transaction id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.transaction ALTER COLUMN id SET DEFAULT nextval('public.transaction_id_seq'::regclass);


--
-- Name: transaction_record id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.transaction_record ALTER COLUMN id SET DEFAULT nextval('public.transaction_record_id_seq'::regclass);


--
-- Name: transaction_record_to_object id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.transaction_record_to_object ALTER COLUMN id SET DEFAULT nextval('public.transaction_record_to_object_id_seq'::regclass);


--
-- Name: user_entity id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.user_entity ALTER COLUMN id SET DEFAULT nextval('public.user_entity_id_seq'::regclass);


--
-- Name: woz_objects id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.woz_objects ALTER COLUMN id SET DEFAULT nextval('public.woz_objects_id_seq'::regclass);


--
-- Name: zaak searchable_id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak ALTER COLUMN searchable_id SET DEFAULT nextval('public.searchable_searchable_id_seq'::regclass);


--
-- Name: zaak id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak ALTER COLUMN id SET DEFAULT nextval('public.zaak_id_seq'::regclass);


--
-- Name: zaak_authorisation id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak_authorisation ALTER COLUMN id SET DEFAULT nextval('public.zaak_authorisation_id_seq'::regclass);


--
-- Name: zaak_bag id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak_bag ALTER COLUMN id SET DEFAULT nextval('public.zaak_bag_id_seq'::regclass);


--
-- Name: zaak_betrokkenen id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak_betrokkenen ALTER COLUMN id SET DEFAULT nextval('public.zaak_betrokkenen_id_seq'::regclass);


--
-- Name: zaak_kenmerk id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak_kenmerk ALTER COLUMN id SET DEFAULT nextval('public.zaak_kenmerk_id_seq'::regclass);


--
-- Name: zaak_meta id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak_meta ALTER COLUMN id SET DEFAULT nextval('public.zaak_meta_id_seq'::regclass);


--
-- Name: zaak_subcase id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak_subcase ALTER COLUMN id SET DEFAULT nextval('public.zaak_subcase_id_seq'::regclass);


--
-- Name: zaaktype searchable_id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype ALTER COLUMN searchable_id SET DEFAULT nextval('public.searchable_searchable_id_seq'::regclass);


--
-- Name: zaaktype id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype ALTER COLUMN id SET DEFAULT nextval('public.zaaktype_id_seq'::regclass);


--
-- Name: zaaktype_authorisation id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_authorisation ALTER COLUMN id SET DEFAULT nextval('public.zaaktype_authorisation_id_seq'::regclass);


--
-- Name: zaaktype_betrokkenen id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_betrokkenen ALTER COLUMN id SET DEFAULT nextval('public.zaaktype_betrokkenen_id_seq'::regclass);


--
-- Name: zaaktype_definitie id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_definitie ALTER COLUMN id SET DEFAULT nextval('public.zaaktype_definitie_id_seq'::regclass);


--
-- Name: zaaktype_kenmerken id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_kenmerken ALTER COLUMN id SET DEFAULT nextval('public.zaaktype_kenmerken_id_seq'::regclass);


--
-- Name: zaaktype_node id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_node ALTER COLUMN id SET DEFAULT nextval('public.zaaktype_node_id_seq'::regclass);


--
-- Name: zaaktype_notificatie id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_notificatie ALTER COLUMN id SET DEFAULT nextval('public.zaaktype_notificatie_id_seq'::regclass);


--
-- Name: zaaktype_regel id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_regel ALTER COLUMN id SET DEFAULT nextval('public.zaaktype_regel_id_seq'::regclass);


--
-- Name: zaaktype_relatie id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_relatie ALTER COLUMN id SET DEFAULT nextval('public.zaaktype_relatie_id_seq'::regclass);


--
-- Name: zaaktype_resultaten id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_resultaten ALTER COLUMN id SET DEFAULT nextval('public.zaaktype_resultaten_id_seq'::regclass);


--
-- Name: zaaktype_sjablonen id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_sjablonen ALTER COLUMN id SET DEFAULT nextval('public.zaaktype_sjablonen_id_seq'::regclass);


--
-- Name: zaaktype_standaard_betrokkenen id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_standaard_betrokkenen ALTER COLUMN id SET DEFAULT nextval('public.zaaktype_standaard_betrokkenen_id_seq'::regclass);


--
-- Name: zaaktype_status id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_status ALTER COLUMN id SET DEFAULT nextval('public.zaaktype_status_id_seq'::regclass);


--
-- Name: zaaktype_status_checklist_item id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_status_checklist_item ALTER COLUMN id SET DEFAULT nextval('public.zaaktype_status_checklist_item_id_seq'::regclass);


--
-- Name: natuurlijk_persoon adres_id; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.natuurlijk_persoon
    ADD CONSTRAINT adres_id UNIQUE (id);


--
-- Name: adres adres_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.adres
    ADD CONSTRAINT adres_pkey PRIMARY KEY (id);


--
-- Name: alternative_authentication_activation_link alternative_authentication_activation_link_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.alternative_authentication_activation_link
    ADD CONSTRAINT alternative_authentication_activation_link_pkey PRIMARY KEY (token);


--
-- Name: bag_cache bag_cache_bag_type_bag_id_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bag_cache
    ADD CONSTRAINT bag_cache_bag_type_bag_id_key UNIQUE (bag_type, bag_id);


--
-- Name: bag_cache bag_cache_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bag_cache
    ADD CONSTRAINT bag_cache_pkey PRIMARY KEY (id);


--
-- Name: bag_ligplaats bag_ligplaats_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bag_ligplaats
    ADD CONSTRAINT bag_ligplaats_pkey PRIMARY KEY (id);


--
-- Name: bag_nummeraanduiding bag_nummeraanduiding_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bag_nummeraanduiding
    ADD CONSTRAINT bag_nummeraanduiding_pkey PRIMARY KEY (id);


--
-- Name: bag_openbareruimte bag_openbareruimte_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bag_openbareruimte
    ADD CONSTRAINT bag_openbareruimte_pkey PRIMARY KEY (id);


--
-- Name: bag_pand bag_pand_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bag_pand
    ADD CONSTRAINT bag_pand_pkey PRIMARY KEY (id);


--
-- Name: bag_standplaats bag_standplaats_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bag_standplaats
    ADD CONSTRAINT bag_standplaats_pkey PRIMARY KEY (id);


--
-- Name: bag_verblijfsobject_gebruiksdoel bag_verblijfsobject_gebruiksdoel_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bag_verblijfsobject_gebruiksdoel
    ADD CONSTRAINT bag_verblijfsobject_gebruiksdoel_pkey PRIMARY KEY (id);


--
-- Name: bag_verblijfsobject_pand bag_verblijfsobject_pand_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bag_verblijfsobject_pand
    ADD CONSTRAINT bag_verblijfsobject_pand_pkey PRIMARY KEY (id);


--
-- Name: bag_verblijfsobject bag_verblijfsobject_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bag_verblijfsobject
    ADD CONSTRAINT bag_verblijfsobject_pkey PRIMARY KEY (id);


--
-- Name: bag_woonplaats bag_woonplaats_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bag_woonplaats
    ADD CONSTRAINT bag_woonplaats_pkey PRIMARY KEY (id);


--
-- Name: bedrijf_authenticatie bedrijf_authenticatie_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bedrijf_authenticatie
    ADD CONSTRAINT bedrijf_authenticatie_pkey PRIMARY KEY (id);


--
-- Name: bedrijf bedrijf_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bedrijf
    ADD CONSTRAINT bedrijf_pkey PRIMARY KEY (id);


--
-- Name: bedrijf bedrijf_uuid_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bedrijf
    ADD CONSTRAINT bedrijf_uuid_key UNIQUE (uuid);


--
-- Name: beheer_import_log beheer_import_log_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.beheer_import_log
    ADD CONSTRAINT beheer_import_log_pkey PRIMARY KEY (id);


--
-- Name: beheer_import beheer_import_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.beheer_import
    ADD CONSTRAINT beheer_import_pkey PRIMARY KEY (id);


--
-- Name: betrokkene_notes betrokkene_notes_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.betrokkene_notes
    ADD CONSTRAINT betrokkene_notes_pkey PRIMARY KEY (id);


--
-- Name: betrokkenen betrokkenen_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.betrokkenen
    ADD CONSTRAINT betrokkenen_pkey PRIMARY KEY (id);


--
-- Name: bibliotheek_categorie bibliotheek_categorie_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bibliotheek_categorie
    ADD CONSTRAINT bibliotheek_categorie_pkey PRIMARY KEY (id);


--
-- Name: bibliotheek_kenmerken bibliotheek_kenmerken_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bibliotheek_kenmerken
    ADD CONSTRAINT bibliotheek_kenmerken_pkey PRIMARY KEY (id);


--
-- Name: bibliotheek_kenmerken_values bibliotheek_kenmerken_values_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bibliotheek_kenmerken_values
    ADD CONSTRAINT bibliotheek_kenmerken_values_pkey PRIMARY KEY (id);


--
-- Name: bibliotheek_notificatie_kenmerk bibliotheek_notificatie_kenmerk_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bibliotheek_notificatie_kenmerk
    ADD CONSTRAINT bibliotheek_notificatie_kenmerk_pkey PRIMARY KEY (id);


--
-- Name: bibliotheek_notificaties bibliotheek_notificaties_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bibliotheek_notificaties
    ADD CONSTRAINT bibliotheek_notificaties_pkey PRIMARY KEY (id);


--
-- Name: bibliotheek_sjablonen_magic_string bibliotheek_sjablonen_magic_string_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bibliotheek_sjablonen_magic_string
    ADD CONSTRAINT bibliotheek_sjablonen_magic_string_pkey PRIMARY KEY (id);


--
-- Name: bibliotheek_sjablonen bibliotheek_sjablonen_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bibliotheek_sjablonen
    ADD CONSTRAINT bibliotheek_sjablonen_pkey PRIMARY KEY (id);


--
-- Name: case_action case_actions_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.case_action
    ADD CONSTRAINT case_actions_pkey PRIMARY KEY (id);


--
-- Name: case_authorisation_map case_authorisation_map_key_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.case_authorisation_map
    ADD CONSTRAINT case_authorisation_map_key_key UNIQUE (key);


--
-- Name: case_authorisation_map case_authorisation_map_legacy_key_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.case_authorisation_map
    ADD CONSTRAINT case_authorisation_map_legacy_key_key UNIQUE (legacy_key);


--
-- Name: case_authorisation_map case_authorisation_map_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.case_authorisation_map
    ADD CONSTRAINT case_authorisation_map_pkey PRIMARY KEY (key, legacy_key);


--
-- Name: case_property case_property_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.case_property
    ADD CONSTRAINT case_property_pkey PRIMARY KEY (id);


--
-- Name: case_relation case_relation_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.case_relation
    ADD CONSTRAINT case_relation_pkey PRIMARY KEY (id);


--
-- Name: checklist_item checklist_item_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.checklist_item
    ADD CONSTRAINT checklist_item_pkey PRIMARY KEY (id);


--
-- Name: checklist_item checklist_item_uuid_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.checklist_item
    ADD CONSTRAINT checklist_item_uuid_key UNIQUE (uuid);


--
-- Name: checklist checklist_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.checklist
    ADD CONSTRAINT checklist_pkey PRIMARY KEY (id);


--
-- Name: config config_definition_id_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.config
    ADD CONSTRAINT config_definition_id_key UNIQUE (definition_id);


--
-- Name: config config_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.config
    ADD CONSTRAINT config_pkey PRIMARY KEY (id);


--
-- Name: contact_data contact_data_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.contact_data
    ADD CONSTRAINT contact_data_pkey PRIMARY KEY (id);


--
-- Name: contact_relationship contact_relationship_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.contact_relationship
    ADD CONSTRAINT contact_relationship_pkey PRIMARY KEY (id);


--
-- Name: contactmoment_email contactmoment_email_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.contactmoment_email
    ADD CONSTRAINT contactmoment_email_pkey PRIMARY KEY (id);


--
-- Name: contactmoment_note contactmoment_note_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.contactmoment_note
    ADD CONSTRAINT contactmoment_note_pkey PRIMARY KEY (id);


--
-- Name: contactmoment contactmoment_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.contactmoment
    ADD CONSTRAINT contactmoment_pkey PRIMARY KEY (id);


--
-- Name: country_code country_code_dutch_code_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.country_code
    ADD CONSTRAINT country_code_dutch_code_key UNIQUE (dutch_code);


--
-- Name: custom_object custom_object_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.custom_object
    ADD CONSTRAINT custom_object_pkey PRIMARY KEY (id);


--
-- Name: custom_object_relationship custom_object_relationship_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.custom_object_relationship
    ADD CONSTRAINT custom_object_relationship_pkey PRIMARY KEY (id);


--
-- Name: custom_object_relationship custom_object_relationship_uuid_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.custom_object_relationship
    ADD CONSTRAINT custom_object_relationship_uuid_key UNIQUE (uuid);


--
-- Name: custom_object_type custom_object_type_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.custom_object_type
    ADD CONSTRAINT custom_object_type_pkey PRIMARY KEY (id);


--
-- Name: custom_object_type custom_object_type_uuid_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.custom_object_type
    ADD CONSTRAINT custom_object_type_uuid_key UNIQUE (uuid);


--
-- Name: custom_object_type_version custom_object_type_version_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.custom_object_type_version
    ADD CONSTRAINT custom_object_type_version_pkey PRIMARY KEY (id);


--
-- Name: custom_object_type_version custom_object_type_version_uuid_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.custom_object_type_version
    ADD CONSTRAINT custom_object_type_version_uuid_key UNIQUE (uuid);


--
-- Name: custom_object custom_object_uuid_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.custom_object
    ADD CONSTRAINT custom_object_uuid_key UNIQUE (uuid);


--
-- Name: custom_object_version_content custom_object_version_content_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.custom_object_version_content
    ADD CONSTRAINT custom_object_version_content_pkey PRIMARY KEY (id);


--
-- Name: custom_object_version custom_object_version_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.custom_object_version
    ADD CONSTRAINT custom_object_version_key UNIQUE (id, custom_object_id);


--
-- Name: custom_object_version custom_object_version_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.custom_object_version
    ADD CONSTRAINT custom_object_version_pkey PRIMARY KEY (id);


--
-- Name: custom_object_version custom_object_version_uuid_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.custom_object_version
    ADD CONSTRAINT custom_object_version_uuid_key UNIQUE (uuid);


--
-- Name: directory directory_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.directory
    ADD CONSTRAINT directory_pkey PRIMARY KEY (id);


--
-- Name: directory directory_uuid_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.directory
    ADD CONSTRAINT directory_uuid_key UNIQUE (uuid);


--
-- Name: export_queue export_queue_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.export_queue
    ADD CONSTRAINT export_queue_pkey PRIMARY KEY (id);


--
-- Name: file_annotation file_annotation_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.file_annotation
    ADD CONSTRAINT file_annotation_pkey PRIMARY KEY (id);


--
-- Name: file_case_document file_case_document_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.file_case_document
    ADD CONSTRAINT file_case_document_pkey PRIMARY KEY (id);


--
-- Name: file_metadata file_metadata_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.file_metadata
    ADD CONSTRAINT file_metadata_pkey PRIMARY KEY (id);


--
-- Name: file file_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.file
    ADD CONSTRAINT file_pkey PRIMARY KEY (id);


--
-- Name: file_derivative file_thumbnail_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.file_derivative
    ADD CONSTRAINT file_thumbnail_pkey PRIMARY KEY (id);


--
-- Name: filestore filestore_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.filestore
    ADD CONSTRAINT filestore_pkey PRIMARY KEY (id);


--
-- Name: gegevensmagazijn_subjecten gegevensmagazijn_subjecten_subject_uuid_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.gegevensmagazijn_subjecten
    ADD CONSTRAINT gegevensmagazijn_subjecten_subject_uuid_key UNIQUE (subject_uuid);


--
-- Name: gm_adres gm_adres_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.gm_adres
    ADD CONSTRAINT gm_adres_pkey PRIMARY KEY (id);


--
-- Name: gm_bedrijf gm_bedrijf_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.gm_bedrijf
    ADD CONSTRAINT gm_bedrijf_pkey PRIMARY KEY (id);


--
-- Name: gm_natuurlijk_persoon gm_natuurlijk_persoon_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.gm_natuurlijk_persoon
    ADD CONSTRAINT gm_natuurlijk_persoon_pkey PRIMARY KEY (id);


--
-- Name: groups groups_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.groups
    ADD CONSTRAINT groups_pkey PRIMARY KEY (id);


--
-- Name: interface interface_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.interface
    ADD CONSTRAINT interface_pkey PRIMARY KEY (id);


--
-- Name: legal_entity_type legal_entity_type_code_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.legal_entity_type
    ADD CONSTRAINT legal_entity_type_code_key UNIQUE (code);


--
-- Name: logging logging_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.logging
    ADD CONSTRAINT logging_pkey PRIMARY KEY (id);


--
-- Name: logging logging_uuid_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.logging
    ADD CONSTRAINT logging_uuid_key UNIQUE (uuid);


--
-- Name: message message_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.message
    ADD CONSTRAINT message_pkey PRIMARY KEY (id);


--
-- Name: municipality_code municipality_code_dutch_code_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.municipality_code
    ADD CONSTRAINT municipality_code_dutch_code_key UNIQUE (dutch_code);


--
-- Name: directory name_case_id; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.directory
    ADD CONSTRAINT name_case_id UNIQUE (name, case_id);


--
-- Name: natuurlijk_persoon natuurlijk_persoon_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.natuurlijk_persoon
    ADD CONSTRAINT natuurlijk_persoon_pkey PRIMARY KEY (id);


--
-- Name: natuurlijk_persoon natuurlijk_persoon_uuid_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.natuurlijk_persoon
    ADD CONSTRAINT natuurlijk_persoon_uuid_key UNIQUE (uuid);


--
-- Name: object_acl_entry object_acl_entry_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.object_acl_entry
    ADD CONSTRAINT object_acl_entry_pkey PRIMARY KEY (uuid);


--
-- Name: object_data object_data_object_class_object_id_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.object_data
    ADD CONSTRAINT object_data_object_class_object_id_key UNIQUE (object_class, object_id);


--
-- Name: object_data object_data_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.object_data
    ADD CONSTRAINT object_data_pkey PRIMARY KEY (uuid);


--
-- Name: object_mutation object_mutation_object_uuid_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.object_mutation
    ADD CONSTRAINT object_mutation_object_uuid_key UNIQUE (object_uuid);


--
-- Name: object_mutation object_mutation_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.object_mutation
    ADD CONSTRAINT object_mutation_pkey PRIMARY KEY (id);


--
-- Name: object_relation object_relation_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.object_relation
    ADD CONSTRAINT object_relation_pkey PRIMARY KEY (id);


--
-- Name: object_relationships object_relationships_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.object_relationships
    ADD CONSTRAINT object_relationships_pkey PRIMARY KEY (uuid);


--
-- Name: object_subscription object_subscription_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.object_subscription
    ADD CONSTRAINT object_subscription_pkey PRIMARY KEY (id);


--
-- Name: object_bibliotheek_entry object_type_bibliotheek_entry_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.object_bibliotheek_entry
    ADD CONSTRAINT object_type_bibliotheek_entry_pkey PRIMARY KEY (id);


--
-- Name: config parameter_unique; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.config
    ADD CONSTRAINT parameter_unique UNIQUE (parameter);


--
-- Name: parkeergebied parkeergebied_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.parkeergebied
    ADD CONSTRAINT parkeergebied_pkey PRIMARY KEY (id);


--
-- Name: bag_ligplaats_nevenadres pk_ligplaats_nevenadres; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bag_ligplaats_nevenadres
    ADD CONSTRAINT pk_ligplaats_nevenadres PRIMARY KEY (identificatie, begindatum, correctie, nevenadres);


--
-- Name: queue queue_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.queue
    ADD CONSTRAINT queue_pkey PRIMARY KEY (id);


--
-- Name: remote_api_keys remote_api_keys_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.remote_api_keys
    ADD CONSTRAINT remote_api_keys_pkey PRIMARY KEY (id);


--
-- Name: result_preservation_terms result_preservation_terms_code_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.result_preservation_terms
    ADD CONSTRAINT result_preservation_terms_code_key UNIQUE (code);


--
-- Name: result_preservation_terms result_preservation_terms_label_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.result_preservation_terms
    ADD CONSTRAINT result_preservation_terms_label_key UNIQUE (label);


--
-- Name: rights rights_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.rights
    ADD CONSTRAINT rights_pkey PRIMARY KEY (name);


--
-- Name: role_rights role_rights_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.role_rights
    ADD CONSTRAINT role_rights_pkey PRIMARY KEY (rights_name, role_id);


--
-- Name: roles roles_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.roles
    ADD CONSTRAINT roles_pkey PRIMARY KEY (id);


--
-- Name: sbus_logging sbus_logging_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sbus_logging
    ADD CONSTRAINT sbus_logging_pkey PRIMARY KEY (id);


--
-- Name: sbus_traffic sbus_traffic_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sbus_traffic
    ADD CONSTRAINT sbus_traffic_pkey PRIMARY KEY (id);


--
-- Name: scheduled_jobs scheduled_jobs_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.scheduled_jobs
    ADD CONSTRAINT scheduled_jobs_pkey PRIMARY KEY (id);


--
-- Name: scheduled_jobs scheduled_jobs_uuid_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.scheduled_jobs
    ADD CONSTRAINT scheduled_jobs_uuid_key UNIQUE (uuid);


--
-- Name: search_query_delen search_query_delen_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.search_query_delen
    ADD CONSTRAINT search_query_delen_pkey PRIMARY KEY (id);


--
-- Name: search_query search_query_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.search_query
    ADD CONSTRAINT search_query_pkey PRIMARY KEY (id);


--
-- Name: service_geojson service_geojson_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.service_geojson
    ADD CONSTRAINT service_geojson_pkey PRIMARY KEY (id);


--
-- Name: service_geojson_relationship service_geojson_relationship_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.service_geojson_relationship
    ADD CONSTRAINT service_geojson_relationship_pkey PRIMARY KEY (id);


--
-- Name: service_geojson_relationship service_geojson_relationship_related_uuid_service_geojson_i_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.service_geojson_relationship
    ADD CONSTRAINT service_geojson_relationship_related_uuid_service_geojson_i_key UNIQUE (related_uuid, service_geojson_id);


--
-- Name: service_geojson_relationship service_geojson_relationship_uuid_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.service_geojson_relationship
    ADD CONSTRAINT service_geojson_relationship_uuid_key UNIQUE (uuid);


--
-- Name: service_geojson service_geojson_uuid_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.service_geojson
    ADD CONSTRAINT service_geojson_uuid_key UNIQUE (uuid);


--
-- Name: session_invitation session_invitation_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.session_invitation
    ADD CONSTRAINT session_invitation_pkey PRIMARY KEY (id);


--
-- Name: settings settings_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.settings
    ADD CONSTRAINT settings_pkey PRIMARY KEY (id);


--
-- Name: bibliotheek_kenmerken_values sort_order_unique; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bibliotheek_kenmerken_values
    ADD CONSTRAINT sort_order_unique UNIQUE (sort_order, bibliotheek_kenmerken_id);


--
-- Name: subject_login_history subject_login_history_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.subject_login_history
    ADD CONSTRAINT subject_login_history_pkey PRIMARY KEY (id);


--
-- Name: subject subject_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.subject
    ADD CONSTRAINT subject_pkey PRIMARY KEY (id);


--
-- Name: subject subject_uuid_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.subject
    ADD CONSTRAINT subject_uuid_key UNIQUE (uuid);


--
-- Name: thread_message_attachment_derivative thread_message_attachment_derivative_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.thread_message_attachment_derivative
    ADD CONSTRAINT thread_message_attachment_derivative_pkey PRIMARY KEY (id);


--
-- Name: thread_message_attachment thread_message_attachment_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.thread_message_attachment
    ADD CONSTRAINT thread_message_attachment_pkey PRIMARY KEY (id);


--
-- Name: thread_message_attachment thread_message_attachment_uuid_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.thread_message_attachment
    ADD CONSTRAINT thread_message_attachment_uuid_key UNIQUE (uuid);


--
-- Name: thread_message_contact_moment thread_message_contact_moment_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.thread_message_contact_moment
    ADD CONSTRAINT thread_message_contact_moment_pkey PRIMARY KEY (id);


--
-- Name: thread_message_external thread_message_external_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.thread_message_external
    ADD CONSTRAINT thread_message_external_pkey PRIMARY KEY (id);


--
-- Name: thread_message_note thread_message_note_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.thread_message_note
    ADD CONSTRAINT thread_message_note_pkey PRIMARY KEY (id);


--
-- Name: thread_message thread_message_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.thread_message
    ADD CONSTRAINT thread_message_pkey PRIMARY KEY (id);


--
-- Name: thread_message thread_message_thread_message_contact_moment_id_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.thread_message
    ADD CONSTRAINT thread_message_thread_message_contact_moment_id_key UNIQUE (thread_message_contact_moment_id);


--
-- Name: thread_message thread_message_thread_message_external_id_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.thread_message
    ADD CONSTRAINT thread_message_thread_message_external_id_key UNIQUE (thread_message_external_id);


--
-- Name: thread_message thread_message_thread_message_note_id_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.thread_message
    ADD CONSTRAINT thread_message_thread_message_note_id_key UNIQUE (thread_message_note_id);


--
-- Name: thread_message thread_message_uuid_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.thread_message
    ADD CONSTRAINT thread_message_uuid_key UNIQUE (uuid);


--
-- Name: thread thread_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.thread
    ADD CONSTRAINT thread_pkey PRIMARY KEY (id);


--
-- Name: thread thread_uuid_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.thread
    ADD CONSTRAINT thread_uuid_key UNIQUE (uuid);


--
-- Name: transaction transaction_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.transaction
    ADD CONSTRAINT transaction_pkey PRIMARY KEY (id);


--
-- Name: transaction_record transaction_record_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.transaction_record
    ADD CONSTRAINT transaction_record_pkey PRIMARY KEY (id);


--
-- Name: transaction_record_to_object transaction_record_to_object_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.transaction_record_to_object
    ADD CONSTRAINT transaction_record_to_object_pkey PRIMARY KEY (id);


--
-- Name: user_app_lock user_app_lock_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.user_app_lock
    ADD CONSTRAINT user_app_lock_pkey PRIMARY KEY (uidnumber, type, type_id);


--
-- Name: user_entity user_entity_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.user_entity
    ADD CONSTRAINT user_entity_pkey PRIMARY KEY (id);


--
-- Name: woz_objects woz_objects_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.woz_objects
    ADD CONSTRAINT woz_objects_pkey PRIMARY KEY (id);


--
-- Name: zaak_authorisation zaak_authorisation_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak_authorisation
    ADD CONSTRAINT zaak_authorisation_pkey PRIMARY KEY (id);


--
-- Name: zaak_bag zaak_bag_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak_bag
    ADD CONSTRAINT zaak_bag_pkey PRIMARY KEY (id);


--
-- Name: zaak_betrokkenen zaak_betrokkenen_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak_betrokkenen
    ADD CONSTRAINT zaak_betrokkenen_pkey PRIMARY KEY (id);


--
-- Name: zaak zaak_duplicate_prevention_token_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak
    ADD CONSTRAINT zaak_duplicate_prevention_token_key UNIQUE (duplicate_prevention_token);


--
-- Name: zaak_kenmerk zaak_kenmerk_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak_kenmerk
    ADD CONSTRAINT zaak_kenmerk_pkey PRIMARY KEY (id);


--
-- Name: zaak_meta zaak_meta_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak_meta
    ADD CONSTRAINT zaak_meta_pkey PRIMARY KEY (id);


--
-- Name: zaak_meta zaak_meta_uniq_zaak_id; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak_meta
    ADD CONSTRAINT zaak_meta_uniq_zaak_id UNIQUE (zaak_id);


--
-- Name: zaak_onafgerond zaak_onafgerond_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak_onafgerond
    ADD CONSTRAINT zaak_onafgerond_pkey PRIMARY KEY (zaaktype_id, betrokkene);


--
-- Name: zaak zaak_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak
    ADD CONSTRAINT zaak_pkey PRIMARY KEY (id);


--
-- Name: zaak_subcase zaak_subcase_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak_subcase
    ADD CONSTRAINT zaak_subcase_pkey PRIMARY KEY (id);


--
-- Name: zaaktype_authorisation zaaktype_authorisation_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_authorisation
    ADD CONSTRAINT zaaktype_authorisation_pkey PRIMARY KEY (id);


--
-- Name: zaaktype_betrokkenen zaaktype_betrokkenen_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_betrokkenen
    ADD CONSTRAINT zaaktype_betrokkenen_pkey PRIMARY KEY (id);


--
-- Name: zaaktype_status_checklist_item zaaktype_checklist_item_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_status_checklist_item
    ADD CONSTRAINT zaaktype_checklist_item_pkey PRIMARY KEY (id);


--
-- Name: zaaktype_definitie zaaktype_definitie_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_definitie
    ADD CONSTRAINT zaaktype_definitie_pkey PRIMARY KEY (id);


--
-- Name: zaaktype_kenmerken zaaktype_kenmerken_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_kenmerken
    ADD CONSTRAINT zaaktype_kenmerken_pkey PRIMARY KEY (id);


--
-- Name: zaaktype_kenmerken zaaktype_kenmerken_uuid_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_kenmerken
    ADD CONSTRAINT zaaktype_kenmerken_uuid_key UNIQUE (uuid);


--
-- Name: zaaktype_node zaaktype_node_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_node
    ADD CONSTRAINT zaaktype_node_pkey PRIMARY KEY (id);


--
-- Name: zaaktype_notificatie zaaktype_notificatie_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_notificatie
    ADD CONSTRAINT zaaktype_notificatie_pkey PRIMARY KEY (id);


--
-- Name: zaaktype zaaktype_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype
    ADD CONSTRAINT zaaktype_pkey PRIMARY KEY (id);


--
-- Name: zaaktype_regel zaaktype_regel_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_regel
    ADD CONSTRAINT zaaktype_regel_pkey PRIMARY KEY (id);


--
-- Name: zaaktype_relatie zaaktype_relatie_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_relatie
    ADD CONSTRAINT zaaktype_relatie_pkey PRIMARY KEY (id);


--
-- Name: zaaktype_resultaten zaaktype_resultaten_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_resultaten
    ADD CONSTRAINT zaaktype_resultaten_pkey PRIMARY KEY (id);


--
-- Name: zaaktype_sjablonen zaaktype_sjablonen_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_sjablonen
    ADD CONSTRAINT zaaktype_sjablonen_pkey PRIMARY KEY (id);


--
-- Name: zaaktype_standaard_betrokkenen zaaktype_standaard_betrokkenen_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_standaard_betrokkenen
    ADD CONSTRAINT zaaktype_standaard_betrokkenen_pkey PRIMARY KEY (id);


--
-- Name: zaaktype_status zaaktype_status_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_status
    ADD CONSTRAINT zaaktype_status_pkey PRIMARY KEY (id);


--
-- Name: zaaktype zaaktype_uuid_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype
    ADD CONSTRAINT zaaktype_uuid_key UNIQUE (uuid);


--
-- Name: alternative_authentication_activation_link_subject_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX alternative_authentication_activation_link_subject_id_idx ON public.alternative_authentication_activation_link USING btree (subject_id);


--
-- Name: beheer_import_log_idx_import_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX beheer_import_log_idx_import_id ON public.beheer_import_log USING btree (import_id);


--
-- Name: bibliotheek_categorie_pid_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX bibliotheek_categorie_pid_idx ON public.bibliotheek_categorie USING btree (pid);


--
-- Name: bibliotheek_categorie_uuid_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX bibliotheek_categorie_uuid_idx ON public.bibliotheek_categorie USING btree (uuid);


--
-- Name: bibliotheek_category_unique_name; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX bibliotheek_category_unique_name ON public.bibliotheek_categorie USING btree (naam, COALESCE(pid, '-1'::integer));


--
-- Name: bibliotheek_kenmerken_bibliotheek_categorie_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX bibliotheek_kenmerken_bibliotheek_categorie_id_idx ON public.bibliotheek_kenmerken USING btree (bibliotheek_categorie_id);


--
-- Name: bibliotheek_kenmerken_deleted_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX bibliotheek_kenmerken_deleted_idx ON public.bibliotheek_kenmerken USING btree (deleted);


--
-- Name: bibliotheek_kenmerken_magic_string; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX bibliotheek_kenmerken_magic_string ON public.bibliotheek_kenmerken USING btree (magic_string) WHERE (deleted IS NULL);


--
-- Name: bibliotheek_kenmerken_magic_string_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX bibliotheek_kenmerken_magic_string_idx ON public.bibliotheek_kenmerken USING btree (magic_string);


--
-- Name: bibliotheek_kenmerken_magic_string_unique_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX bibliotheek_kenmerken_magic_string_unique_id ON public.bibliotheek_kenmerken USING btree (magic_string) WHERE (deleted IS NULL);


--
-- Name: bibliotheek_kenmerken_uuid_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX bibliotheek_kenmerken_uuid_idx ON public.bibliotheek_kenmerken USING btree (uuid);


--
-- Name: bibliotheek_kenmerken_value_type_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX bibliotheek_kenmerken_value_type_idx ON public.bibliotheek_kenmerken USING btree (value_type);


--
-- Name: bibliotheek_kenmerken_values_idx_bibliotheek_kenmerken_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX bibliotheek_kenmerken_values_idx_bibliotheek_kenmerken_id ON public.bibliotheek_kenmerken_values USING btree (bibliotheek_kenmerken_id);


--
-- Name: bibliotheek_notificaties_bibliotheek_categorie_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX bibliotheek_notificaties_bibliotheek_categorie_id_idx ON public.bibliotheek_notificaties USING btree (bibliotheek_categorie_id);


--
-- Name: bibliotheek_notificaties_deleted_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX bibliotheek_notificaties_deleted_idx ON public.bibliotheek_notificaties USING btree (deleted);


--
-- Name: bibliotheek_notificaties_uuid_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX bibliotheek_notificaties_uuid_idx ON public.bibliotheek_notificaties USING btree (uuid);


--
-- Name: bibliotheek_sjablonen_bibliotheek_categorie_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX bibliotheek_sjablonen_bibliotheek_categorie_id_idx ON public.bibliotheek_sjablonen USING btree (bibliotheek_categorie_id);


--
-- Name: bibliotheek_sjablonen_deleted_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX bibliotheek_sjablonen_deleted_idx ON public.bibliotheek_sjablonen USING btree (deleted);


--
-- Name: bibliotheek_sjablonen_interface_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX bibliotheek_sjablonen_interface_id_idx ON public.bibliotheek_sjablonen USING btree (interface_id);


--
-- Name: bibliotheek_sjablonen_template_uuid_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX bibliotheek_sjablonen_template_uuid_idx ON public.bibliotheek_sjablonen USING btree (template_external_name);


--
-- Name: bibliotheek_sjablonen_uuid_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX bibliotheek_sjablonen_uuid_idx ON public.bibliotheek_sjablonen USING btree (uuid);


--
-- Name: burgerservicenummer; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX burgerservicenummer ON public.natuurlijk_persoon USING btree (burgerservicenummer, authenticated) WHERE (deleted_on IS NULL);


--
-- Name: case_property_case_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX case_property_case_id_idx ON public.case_property USING btree (case_id);


--
-- Name: case_property_name_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX case_property_name_idx ON public.case_property USING btree (name);


--
-- Name: case_property_name_ref_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX case_property_name_ref_idx ON public.case_property USING btree (name, namespace, object_id, case_id);


--
-- Name: INDEX case_property_name_ref_idx; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON INDEX public.case_property_name_ref_idx IS 'enforce uniqueness of param<->object rows, multiple values not allowed';


--
-- Name: case_property_object_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX case_property_object_id_idx ON public.case_property USING btree (object_id);


--
-- Name: case_property_type_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX case_property_type_idx ON public.case_property USING btree (type);


--
-- Name: case_relation_case_id_a_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX case_relation_case_id_a_idx ON public.case_relation USING btree (case_id_a);


--
-- Name: case_relation_case_id_b_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX case_relation_case_id_b_idx ON public.case_relation USING btree (case_id_b);


--
-- Name: casetype_end_status_casetype_node_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX casetype_end_status_casetype_node_idx ON public.casetype_end_status USING btree (zaaktype_node_id);


--
-- Name: casetype_v1_reference_casetype_node_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX casetype_v1_reference_casetype_node_idx ON public.casetype_v1_reference USING btree (casetype_node_id);


--
-- Name: checklist_item_assignee_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX checklist_item_assignee_id_idx ON public.checklist_item USING btree (assignee_id);


--
-- Name: checklist_item_assignee_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX checklist_item_assignee_idx ON public.checklist_item USING btree (assignee_id);


--
-- Name: checklist_item_due_date_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX checklist_item_due_date_idx ON public.checklist_item USING btree (due_date);


--
-- Name: checklist_item_text_search_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX checklist_item_text_search_idx ON public.checklist_item USING gin (label public.gin_trgm_ops, description public.gin_trgm_ops);


--
-- Name: contact_data_betrokkene_type_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX contact_data_betrokkene_type_idx ON public.contact_data USING btree (betrokkene_type);


--
-- Name: contact_data_gegevens_magazijn_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX contact_data_gegevens_magazijn_id_idx ON public.contact_data USING btree (gegevens_magazijn_id);


--
-- Name: contact_relationship_unique_link_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX contact_relationship_unique_link_idx ON public.contact_relationship USING btree (LEAST((contact || contact_type), (relation || relation_type)), GREATEST((contact || contact_type), (relation || relation_type)));


--
-- Name: contact_relationship_uuid_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX contact_relationship_uuid_idx ON public.contact_relationship USING btree (uuid);


--
-- Name: contactmoment_uuid_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX contactmoment_uuid_idx ON public.contactmoment USING btree (uuid);


--
-- Name: custom_object_relationship_related_employee_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX custom_object_relationship_related_employee_id_idx ON public.custom_object_relationship USING btree (related_employee_id);


--
-- Name: custom_object_type_uuid_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX custom_object_type_uuid_idx ON public.custom_object_type USING btree (uuid);


--
-- Name: custom_object_type_version_date_deleted_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX custom_object_type_version_date_deleted_idx ON public.custom_object_type_version USING btree (date_deleted);


--
-- Name: custom_object_type_version_uuid_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX custom_object_type_version_uuid_idx ON public.custom_object_type_version USING btree (uuid);


--
-- Name: custom_object_uuid_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX custom_object_uuid_idx ON public.custom_object USING btree (uuid);


--
-- Name: custom_object_version_external_reference_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX custom_object_version_external_reference_idx ON public.custom_object_version USING gin (external_reference public.gin_trgm_ops);


--
-- Name: custom_object_version_subtitle_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX custom_object_version_subtitle_idx ON public.custom_object_version USING gin (subtitle public.gin_trgm_ops);


--
-- Name: custom_object_version_title_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX custom_object_version_title_idx ON public.custom_object_version USING gin (title public.gin_trgm_ops);


--
-- Name: custom_object_version_uuid_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX custom_object_version_uuid_idx ON public.custom_object_version USING btree (uuid);


--
-- Name: export_queue_subject_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX export_queue_subject_id_idx ON public.export_queue USING btree (subject_id);


--
-- Name: file_accepted_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX file_accepted_idx ON public.file USING btree (accepted);


--
-- Name: file_case_document_magic_string_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX file_case_document_magic_string_idx ON public.file_case_document USING btree (magic_string);


--
-- Name: file_case_document_unique_labels_per_file; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX file_case_document_unique_labels_per_file ON public.file_case_document USING btree (file_id, bibliotheek_kenmerken_id, case_id);


--
-- Name: file_case_documents_case_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX file_case_documents_case_idx ON public.file_case_document USING btree (case_id);


--
-- Name: file_case_documents_case_library_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX file_case_documents_case_library_idx ON public.file_case_document USING btree (case_id);


--
-- Name: file_case_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX file_case_id_idx ON public.file USING btree (case_id);


--
-- Name: file_date_deleted; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX file_date_deleted ON public.file USING btree (date_deleted);


--
-- Name: file_derivative_file_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX file_derivative_file_id_idx ON public.file_derivative USING btree (file_id);


--
-- Name: file_derivative_type_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX file_derivative_type_idx ON public.file_derivative USING btree (type);


--
-- Name: file_destroyed_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX file_destroyed_idx ON public.file USING btree (destroyed);


--
-- Name: file_search_index_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX file_search_index_idx ON public.file USING gin (search_index);


--
-- Name: filestore_storage_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX filestore_storage_idx ON public.filestore USING gin (storage_location);


--
-- Name: gegevensmagazijn_subjecten_subject_uuid_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX gegevensmagazijn_subjecten_subject_uuid_idx ON public.gegevensmagazijn_subjecten USING btree (subject_uuid);


--
-- Name: gm_natuurlijk_persoon_idx_adres_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX gm_natuurlijk_persoon_idx_adres_id ON public.gm_natuurlijk_persoon USING btree (adres_id);


--
-- Name: groups_uuid_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX groups_uuid_idx ON public.groups USING btree (uuid);


--
-- Name: idx_case_action_casetype_status_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_case_action_casetype_status_id ON public.case_action USING btree (casetype_status_id);


--
-- Name: idx_zaaktype_kenmerken_zaak_status_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_zaaktype_kenmerken_zaak_status_id ON public.zaaktype_kenmerken USING btree (zaak_status_id);


--
-- Name: idx_zaaktype_notificatie_zaak_status_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_zaaktype_notificatie_zaak_status_id ON public.zaaktype_notificatie USING btree (zaak_status_id);


--
-- Name: idx_zaaktype_regel_zaak_status_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_zaaktype_regel_zaak_status_id ON public.zaaktype_regel USING btree (zaak_status_id);


--
-- Name: idx_zaaktype_sjablonen_zaak_status_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_zaaktype_sjablonen_zaak_status_id ON public.zaaktype_sjablonen USING btree (zaak_status_id);


--
-- Name: interface_uuid_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX interface_uuid_idx ON public.interface USING btree (uuid);


--
-- Name: logging_component_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX logging_component_id_idx ON public.logging USING btree (component_id) WHERE (component_id IS NOT NULL);


--
-- Name: logging_component_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX logging_component_idx ON public.logging USING btree (component) WHERE (component IS NOT NULL);


--
-- Name: logging_created_by_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX logging_created_by_idx ON public.logging USING btree (created_by);


--
-- Name: logging_created_for_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX logging_created_for_idx ON public.logging USING btree (created_for);


--
-- Name: logging_created_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX logging_created_idx ON public.logging USING btree (created);


--
-- Name: logging_custom_object_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX logging_custom_object_idx ON public.logging USING btree ((((event_data)::json ->> 'custom_object_uuid'::text))) WHERE (((component)::text = 'custom_object'::text) AND (((event_data)::json ->> 'custom_object_uuid'::text) IS NOT NULL));


--
-- Name: logging_event_type_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX logging_event_type_idx ON public.logging USING btree (event_type);


--
-- Name: logging_restricted_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX logging_restricted_idx ON public.logging USING btree (restricted);


--
-- Name: logging_zaak_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX logging_zaak_id_idx ON public.logging USING btree (zaak_id);


--
-- Name: message_created_desc_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX message_created_desc_idx ON public.message USING btree (subject_id, is_archived, created DESC);


--
-- Name: message_subject_archived_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX message_subject_archived_idx ON public.message USING btree (subject_id, is_archived);


--
-- Name: message_subject_read_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX message_subject_read_idx ON public.message USING btree (subject_id, is_read);


--
-- Name: natuurlijk_persoon_burgerservicenummer; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX natuurlijk_persoon_burgerservicenummer ON public.natuurlijk_persoon USING btree (burgerservicenummer);


--
-- Name: natuurlijk_persoon_idx_adres_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX natuurlijk_persoon_idx_adres_id ON public.natuurlijk_persoon USING btree (adres_id);


--
-- Name: object_acl_entity_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX object_acl_entity_idx ON public.object_acl_entry USING btree (entity_type, entity_id);


--
-- Name: object_acl_entry_groupname_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX object_acl_entry_groupname_idx ON public.object_acl_entry USING btree (groupname);


--
-- Name: object_acl_entry_permission_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX object_acl_entry_permission_idx ON public.object_acl_entry USING btree (capability);


--
-- Name: object_acl_entry_unique_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX object_acl_entry_unique_idx ON public.object_acl_entry USING btree (object_uuid, entity_type, entity_id, capability, scope, groupname);


--
-- Name: object_acl_object_uuid_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX object_acl_object_uuid_idx ON public.object_acl_entry USING btree (object_uuid);


--
-- Name: object_bibliotheek_entry_bibliotheek_categorie_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX object_bibliotheek_entry_bibliotheek_categorie_id_idx ON public.object_bibliotheek_entry USING btree (bibliotheek_categorie_id);


--
-- Name: object_data_assignee_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX object_data_assignee_idx ON public.object_data USING btree (((index_hstore OPERATOR(public.->) 'case.assignee'::text)));


--
-- Name: object_data_case_assignee_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX object_data_case_assignee_id_idx ON public.object_data USING btree ((((index_hstore OPERATOR(public.->) 'case.assignee.id'::text))::numeric)) WHERE ((object_class = 'case'::text) AND (invalid IS FALSE));


--
-- Name: object_data_case_casetype_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX object_data_case_casetype_id_idx ON public.object_data USING btree ((((index_hstore OPERATOR(public.->) 'case.casetype.id'::text))::numeric)) WHERE ((object_class = 'case'::text) AND (invalid IS FALSE));


--
-- Name: object_data_case_date_current_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX object_data_case_date_current_idx ON public.object_data USING btree (((public.hstore_to_timestamp(((index_hstore OPERATOR(public.->) 'case.date_current'::text))::character varying))::date));


--
-- Name: object_data_case_number_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX object_data_case_number_idx ON public.object_data USING btree (((index_hstore OPERATOR(public.->) 'case.number'::text)));


--
-- Name: object_data_case_phase_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX object_data_case_phase_idx ON public.object_data USING btree (((index_hstore OPERATOR(public.->) 'case.phase'::text)));


--
-- Name: object_data_case_requestor_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX object_data_case_requestor_id_idx ON public.object_data USING btree (((index_hstore OPERATOR(public.->) 'case.requestor.id'::text))) WHERE ((object_class = 'case'::text) AND (invalid IS FALSE));


--
-- Name: object_data_case_route_role_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX object_data_case_route_role_idx ON public.object_data USING btree ((((index_hstore OPERATOR(public.->) 'case.route_ou'::text))::numeric), (((index_hstore OPERATOR(public.->) 'case.route_ou'::text))::numeric)) WHERE ((object_class = 'case'::text) AND (invalid IS FALSE));


--
-- Name: object_data_case_status_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX object_data_case_status_id_idx ON public.object_data USING btree (((index_hstore OPERATOR(public.->) 'case.status'::text))) WHERE ((object_class = 'case'::text) AND (invalid IS FALSE));


--
-- Name: object_data_casetype_description_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX object_data_casetype_description_idx ON public.object_data USING btree (((index_hstore OPERATOR(public.->) 'casetype.description'::text)));


--
-- Name: object_data_casetype_name_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX object_data_casetype_name_idx ON public.object_data USING btree (((index_hstore OPERATOR(public.->) 'casetype.name'::text)));


--
-- Name: object_data_completion_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX object_data_completion_idx ON public.object_data USING btree (((public.hstore_to_timestamp(((index_hstore OPERATOR(public.->) 'case.date_of_completion'::text))::character varying))::date)) WHERE ((object_class = 'case'::text) AND (invalid IS FALSE));


--
-- Name: object_data_hstore_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX object_data_hstore_idx ON public.object_data USING gin (index_hstore);


--
-- Name: object_data_object_class_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX object_data_object_class_idx ON public.object_data USING btree (object_class);


--
-- Name: object_data_object_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX object_data_object_id_idx ON public.object_data USING btree (object_id);


--
-- Name: object_data_recipient_name_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX object_data_recipient_name_idx ON public.object_data USING btree (((index_hstore OPERATOR(public.->) 'recipient.full_name'::text)));


--
-- Name: object_data_regdate_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX object_data_regdate_idx ON public.object_data USING btree (((public.hstore_to_timestamp(((index_hstore OPERATOR(public.->) 'case.date_of_registration'::text))::character varying))::date)) WHERE ((object_class = 'case'::text) AND (invalid IS FALSE));


--
-- Name: object_data_status_casetype_assignee_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX object_data_status_casetype_assignee_idx ON public.object_data USING btree (((index_hstore OPERATOR(public.->) 'case.status'::text)), (((index_hstore OPERATOR(public.->) 'case.casetype.id'::text))::numeric), (((index_hstore OPERATOR(public.->) 'case.assignee.id'::text))::numeric)) WHERE ((object_class = 'case'::text) AND (invalid IS FALSE));


--
-- Name: object_data_target_date_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX object_data_target_date_idx ON public.object_data USING btree (((public.hstore_to_timestamp(((index_hstore OPERATOR(public.->) 'case.case.date_target'::text))::character varying))::date)) WHERE ((object_class = 'case'::text) AND (invalid IS FALSE));


--
-- Name: object_mutation_subject_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX object_mutation_subject_id_idx ON public.object_mutation USING btree (subject_id);


--
-- Name: object_relation_name_and_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX object_relation_name_and_id_idx ON public.object_relation USING btree (name, object_id);


--
-- Name: object_relation_name_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX object_relation_name_idx ON public.object_relation USING btree (name);


--
-- Name: object_text_vector_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX object_text_vector_idx ON public.object_data USING gin (text_vector);


--
-- Name: queue_20210101_expr_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX queue_20210101_expr_idx ON public.queue_20210101 USING btree (((((data)::jsonb -> 'create-args'::text) ->> 'duplicate_prevention_token'::text))) WHERE (type = 'create_case_form'::text);


--
-- Name: queue_20210201_expr_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX queue_20210201_expr_idx ON public.queue_20210201 USING btree (((((data)::jsonb -> 'create-args'::text) ->> 'duplicate_prevention_token'::text))) WHERE (type = 'create_case_form'::text);


--
-- Name: queue_20210301_expr_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX queue_20210301_expr_idx ON public.queue_20210301 USING btree (((((data)::jsonb -> 'create-args'::text) ->> 'duplicate_prevention_token'::text))) WHERE (type = 'create_case_form'::text);


--
-- Name: queue_20210401_expr_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX queue_20210401_expr_idx ON public.queue_20210401 USING btree (((((data)::jsonb -> 'create-args'::text) ->> 'duplicate_prevention_token'::text))) WHERE (type = 'create_case_form'::text);


--
-- Name: queue_20210501_expr_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX queue_20210501_expr_idx ON public.queue_20210501 USING btree (((((data)::jsonb -> 'create-args'::text) ->> 'duplicate_prevention_token'::text))) WHERE (type = 'create_case_form'::text);


--
-- Name: queue_20210601_expr_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX queue_20210601_expr_idx ON public.queue_20210601 USING btree (((((data)::jsonb -> 'create-args'::text) ->> 'duplicate_prevention_token'::text))) WHERE (type = 'create_case_form'::text);


--
-- Name: queue_20210701_expr_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX queue_20210701_expr_idx ON public.queue_20210701 USING btree (((((data)::jsonb -> 'create-args'::text) ->> 'duplicate_prevention_token'::text))) WHERE (type = 'create_case_form'::text);


--
-- Name: queue_20210801_expr_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX queue_20210801_expr_idx ON public.queue_20210801 USING btree (((((data)::jsonb -> 'create-args'::text) ->> 'duplicate_prevention_token'::text))) WHERE (type = 'create_case_form'::text);


--
-- Name: queue_20210901_expr_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX queue_20210901_expr_idx ON public.queue_20210901 USING btree (((((data)::jsonb -> 'create-args'::text) ->> 'duplicate_prevention_token'::text))) WHERE (type = 'create_case_form'::text);


--
-- Name: queue_20211001_expr_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX queue_20211001_expr_idx ON public.queue_20211001 USING btree (((((data)::jsonb -> 'create-args'::text) ->> 'duplicate_prevention_token'::text))) WHERE (type = 'create_case_form'::text);


--
-- Name: queue_20211101_expr_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX queue_20211101_expr_idx ON public.queue_20211101 USING btree (((((data)::jsonb -> 'create-args'::text) ->> 'duplicate_prevention_token'::text))) WHERE (type = 'create_case_form'::text);


--
-- Name: queue_20211201_expr_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX queue_20211201_expr_idx ON public.queue_20211201 USING btree (((((data)::jsonb -> 'create-args'::text) ->> 'duplicate_prevention_token'::text))) WHERE (type = 'create_case_form'::text);


--
-- Name: queue_date_created_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX queue_date_created_idx ON public.queue USING btree (date_created);


--
-- Name: queue_date_finished_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX queue_date_finished_idx ON public.queue USING btree (date_finished);


--
-- Name: queue_object_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX queue_object_id_idx ON public.queue USING btree (object_id);


--
-- Name: queue_priority_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX queue_priority_idx ON public.queue USING btree (priority);


--
-- Name: queue_status_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX queue_status_idx ON public.queue USING btree (status);


--
-- Name: roles_uuid_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX roles_uuid_idx ON public.roles USING btree (uuid);


--
-- Name: scheduled_jobs_case_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX scheduled_jobs_case_id_idx ON public.scheduled_jobs USING btree (case_id);


--
-- Name: scheduled_jobs_task_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX scheduled_jobs_task_idx ON public.scheduled_jobs USING btree (task);


--
-- Name: session_invitation_subject_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX session_invitation_subject_id_idx ON public.session_invitation USING btree (subject_id);


--
-- Name: subject_id_username_covering_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX subject_id_username_covering_idx ON public.subject USING btree (id, username);


--
-- Name: subject_login_history_subject_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX subject_login_history_subject_id_idx ON public.subject_login_history USING btree (subject_id);


--
-- Name: subject_position_matrix_group_role_subject_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX subject_position_matrix_group_role_subject_idx ON public.subject_position_matrix USING btree (subject_id, role_id, group_id);


--
-- Name: subject_position_matrix_position_subject_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX subject_position_matrix_position_subject_idx ON public.subject_position_matrix USING btree (subject_id, "position");


--
-- Name: thread_case_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX thread_case_id_idx ON public.thread USING btree (case_id);


--
-- Name: thread_contact_uuid_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX thread_contact_uuid_idx ON public.thread USING btree (contact_uuid);


--
-- Name: thread_message_contact_moment_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX thread_message_contact_moment_id_idx ON public.thread_message USING btree (thread_message_contact_moment_id);


--
-- Name: thread_message_external_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX thread_message_external_id_idx ON public.thread_message USING btree (thread_message_external_id);


--
-- Name: thread_message_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX thread_message_id_idx ON public.thread_message USING btree (thread_id);


--
-- Name: thread_message_note_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX thread_message_note_id_idx ON public.thread_message USING btree (thread_message_note_id);


--
-- Name: thread_uuid_reply_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX thread_uuid_reply_idx ON public.thread USING btree ((((('x'::text || translate("right"((uuid)::text, 12), '-'::text, ''::text)))::bit(48))::bigint));


--
-- Name: tmp_queue_no_parent; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX tmp_queue_no_parent ON public.queue USING btree (id) WHERE (parent_id IS NULL);


--
-- Name: tmp_queue_parent; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX tmp_queue_parent ON public.queue USING btree (id) WHERE (parent_id IS NOT NULL);


--
-- Name: transaction_date_deleted_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX transaction_date_deleted_idx ON public.transaction USING btree (date_deleted);


--
-- Name: transaction_date_next_retry_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX transaction_date_next_retry_idx ON public.transaction USING btree (date_next_retry);


--
-- Name: transaction_error_fatal_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX transaction_error_fatal_idx ON public.transaction USING btree (error_fatal);


--
-- Name: transaction_external_transaction_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX transaction_external_transaction_id_idx ON public.transaction USING btree (external_transaction_id);


--
-- Name: transaction_interface_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX transaction_interface_id_idx ON public.transaction USING btree (interface_id);


--
-- Name: transaction_record_transaction_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX transaction_record_transaction_id_idx ON public.transaction_record USING btree (transaction_id);


--
-- Name: transaction_record_uuid_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX transaction_record_uuid_idx ON public.transaction_record USING btree (uuid);


--
-- Name: transaction_text_vector_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX transaction_text_vector_idx ON public.transaction USING gist (text_vector);


--
-- Name: transaction_uuid_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX transaction_uuid_idx ON public.transaction USING btree (uuid);


--
-- Name: user_entity_source_interface_id_source_identifier_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX user_entity_source_interface_id_source_identifier_idx ON public.user_entity USING btree (source_interface_id, source_identifier);


--
-- Name: user_entity_subject_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX user_entity_subject_id_idx ON public.user_entity USING btree (subject_id);


--
-- Name: zaak_aanvrager_covering_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaak_aanvrager_covering_idx ON public.zaak USING btree (aanvrager_type, aanvrager_gm_id, id, uuid);


--
-- Name: zaak_aanvrager_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaak_aanvrager_idx ON public.zaak USING btree (aanvrager);


--
-- Name: zaak_authorisation_all_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaak_authorisation_all_idx ON public.zaak_authorisation USING btree (entity_type, capability, zaak_id);


--
-- Name: zaak_authorisation_all_without_case_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaak_authorisation_all_without_case_idx ON public.zaak_authorisation USING btree (entity_type, capability);


--
-- Name: zaak_authorisation_entity_type_capability_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaak_authorisation_entity_type_capability_idx ON public.zaak_authorisation USING btree (entity_type, capability) WHERE (entity_type = 'position'::text);


--
-- Name: zaak_authorisation_entity_type_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaak_authorisation_entity_type_idx ON public.zaak_authorisation USING btree (entity_type);


--
-- Name: zaak_bag_type_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaak_bag_type_id_idx ON public.zaak_bag USING btree (bag_type, bag_id);


--
-- Name: zaak_behandelaar_covering_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaak_behandelaar_covering_idx ON public.zaak USING btree (behandelaar_gm_id, id, uuid);


--
-- Name: zaak_behandelaar_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaak_behandelaar_idx ON public.zaak USING btree (behandelaar);


--
-- Name: zaak_betrokkenen_betrokkene_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaak_betrokkenen_betrokkene_id_idx ON public.zaak_betrokkenen USING btree (betrokkene_id);


--
-- Name: zaak_betrokkenen_betrokkene_type_betrokkene_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaak_betrokkenen_betrokkene_type_betrokkene_id_idx ON public.zaak_betrokkenen USING btree (betrokkene_type, betrokkene_id);


--
-- Name: zaak_betrokkenen_cache_uuid; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaak_betrokkenen_cache_uuid ON public.zaak USING gin (betrokkenen_cache);


--
-- Name: zaak_betrokkenen_deleted_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaak_betrokkenen_deleted_idx ON public.zaak_betrokkenen USING btree (deleted);


--
-- Name: zaak_betrokkenen_gegevens_magazijn_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaak_betrokkenen_gegevens_magazijn_index ON public.zaak_betrokkenen USING btree (gegevens_magazijn_id);


--
-- Name: zaak_betrokkenen_magic_string_prefix_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaak_betrokkenen_magic_string_prefix_idx ON public.zaak_betrokkenen USING btree (magic_string_prefix);


--
-- Name: zaak_betrokkenen_subject_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaak_betrokkenen_subject_id_idx ON public.zaak_betrokkenen USING btree (subject_id);


--
-- Name: zaak_betrokkenen_zaak_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaak_betrokkenen_zaak_id_idx ON public.zaak_betrokkenen USING btree (zaak_id);


--
-- Name: zaak_confidential_covering_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaak_confidential_covering_idx ON public.zaak USING btree (confidential, zaaktype_id, id);


--
-- Name: zaak_confidential_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaak_confidential_idx ON public.zaak USING btree (confidential);


--
-- Name: zaak_coordinator_covering_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaak_coordinator_covering_idx ON public.zaak USING btree (coordinator_gm_id, id, uuid);


--
-- Name: zaak_coordinator_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaak_coordinator_idx ON public.zaak USING btree (coordinator);


--
-- Name: zaak_id_confidential_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaak_id_confidential_idx ON public.zaak USING btree (id, confidential);


--
-- Name: zaak_kenmerk_bibliotheek_kenmerken_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX zaak_kenmerk_bibliotheek_kenmerken_id ON public.zaak_kenmerk USING btree (zaak_id, bibliotheek_kenmerken_id);


--
-- Name: zaak_kenmerk_zaak_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaak_kenmerk_zaak_id_idx ON public.zaak_kenmerk USING btree (zaak_id);


--
-- Name: zaak_last_modified; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaak_last_modified ON public.zaak USING btree (last_modified);


--
-- Name: zaak_meta_index_hstore_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaak_meta_index_hstore_idx ON public.zaak_meta USING gin (index_hstore) WITH (gin_pending_list_limit='128');


--
-- Name: zaak_meta_text_vector_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaak_meta_text_vector_idx ON public.zaak_meta USING gin (text_vector) WITH (gin_pending_list_limit='128');


--
-- Name: zaak_meta_unread_communications_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaak_meta_unread_communications_idx ON public.zaak_meta USING btree (unread_communication_count);


--
-- Name: zaak_onderwerp_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaak_onderwerp_idx ON public.zaak USING gin (onderwerp public.gin_trgm_ops);


--
-- Name: zaak_open_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaak_open_idx ON public.zaak USING btree (id) WHERE (deleted IS NULL);


--
-- Name: zaak_pid_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaak_pid_idx ON public.zaak USING btree (pid);


--
-- Name: zaak_uuid_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX zaak_uuid_idx ON public.zaak USING btree (uuid) WHERE (uuid IS NOT NULL);


--
-- Name: zaak_zaaktype_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaak_zaaktype_id_idx ON public.zaak USING btree (zaaktype_id);


--
-- Name: zaaktype_authorisation_idx_zaaktype_node_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaaktype_authorisation_idx_zaaktype_node_id ON public.zaaktype_authorisation USING btree (zaaktype_node_id);


--
-- Name: zaaktype_authorisation_recht_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaaktype_authorisation_recht_idx ON public.zaaktype_authorisation USING btree (recht);


--
-- Name: zaaktype_betrokkenen_idx_zaaktype_node_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaaktype_betrokkenen_idx_zaaktype_node_id ON public.zaaktype_betrokkenen USING btree (zaaktype_node_id);


--
-- Name: zaaktype_bibliotheek_categorie_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaaktype_bibliotheek_categorie_id_idx ON public.zaaktype USING btree (bibliotheek_categorie_id);


--
-- Name: zaaktype_deleted_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaaktype_deleted_idx ON public.zaaktype USING btree (deleted);


--
-- Name: zaaktype_document_kenmerken_map_by_case_document_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaaktype_document_kenmerken_map_by_case_document_id ON public.zaaktype_document_kenmerken_map USING btree (zaaktype_node_id, case_document_id);


--
-- Name: zaaktype_document_kenmerken_map_by_case_document_uuid; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaaktype_document_kenmerken_map_by_case_document_uuid ON public.zaaktype_document_kenmerken_map USING btree (case_document_uuid);


--
-- Name: zaaktype_document_kenmerken_map_by_casetype_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaaktype_document_kenmerken_map_by_casetype_idx ON public.zaaktype_document_kenmerken_map USING btree (zaaktype_node_id, bibliotheek_kenmerken_id);


--
-- Name: zaaktype_idx_zaaktype_node_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaaktype_idx_zaaktype_node_id ON public.zaaktype USING btree (zaaktype_node_id);


--
-- Name: zaaktype_kenmerken_bag_zaakadres_or_map_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaaktype_kenmerken_bag_zaakadres_or_map_idx ON public.zaaktype_kenmerken USING btree (bag_zaakadres) WHERE ((bag_zaakadres = 1) OR ((properties)::jsonb @> '{"map_case_location": "1"}'::jsonb));


--
-- Name: zaaktype_kenmerken_zaaktype_node_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaaktype_kenmerken_zaaktype_node_idx ON public.zaaktype_kenmerken USING btree (zaaktype_node_id);


--
-- Name: zaaktype_node_idx_zaaktype_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaaktype_node_idx_zaaktype_id ON public.zaaktype_node USING btree (zaaktype_id);


--
-- Name: zaaktype_node_titel_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaaktype_node_titel_idx ON public.zaaktype_node USING gin (titel public.gin_trgm_ops);


--
-- Name: zaaktype_node_uuid_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX zaaktype_node_uuid_idx ON public.zaaktype_node USING btree (uuid);


--
-- Name: zaaktype_node_zaaktype_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaaktype_node_zaaktype_id_idx ON public.zaaktype_node USING btree (zaaktype_id);


--
-- Name: zaaktype_relatie_idx_relatie_zaaktype_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaaktype_relatie_idx_relatie_zaaktype_id ON public.zaaktype_relatie USING btree (relatie_zaaktype_id);


--
-- Name: zaaktype_relatie_idx_zaaktype_node_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaaktype_relatie_idx_zaaktype_node_id ON public.zaaktype_relatie USING btree (zaaktype_node_id);


--
-- Name: zaaktype_relatie_idx_zaaktype_status_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaaktype_relatie_idx_zaaktype_status_id ON public.zaaktype_relatie USING btree (zaaktype_status_id);


--
-- Name: zaaktype_resultaten_idx_zaaktype_node_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaaktype_resultaten_idx_zaaktype_node_id ON public.zaaktype_resultaten USING btree (zaaktype_node_id);


--
-- Name: zaaktype_resultaten_idx_zaaktype_status_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaaktype_resultaten_idx_zaaktype_status_id ON public.zaaktype_resultaten USING btree (zaaktype_status_id);


--
-- Name: zaaktype_status_idx_zaaktype_node_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaaktype_status_idx_zaaktype_node_id ON public.zaaktype_status USING btree (zaaktype_node_id);


--
-- Name: zaaktype_status_zaaktype_node_status_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaaktype_status_zaaktype_node_status_idx ON public.zaaktype_status USING btree (zaaktype_node_id, status);


--
-- Name: zaaktype_zaaktype_node_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaaktype_zaaktype_node_id_idx ON public.zaaktype USING btree (zaaktype_node_id);


--
-- Name: case_attributes _RETURN; Type: RULE; Schema: public; Owner: -
--

CREATE OR REPLACE VIEW public.case_attributes AS
 SELECT z.id AS case_id,
    COALESCE(zk.value, '{}'::text[]) AS value,
    bk.magic_string,
    bk.id AS library_id,
    bk.value_type,
    bk.type_multiple AS mvp
   FROM (((public.zaak z
     JOIN public.zaaktype_kenmerken ztk ON ((z.zaaktype_node_id = ztk.zaaktype_node_id)))
     JOIN public.bibliotheek_kenmerken bk ON (((bk.id = ztk.bibliotheek_kenmerken_id) AND (bk.value_type <> ALL (ARRAY['file'::text, 'appointment'::text])))))
     LEFT JOIN public.zaak_kenmerk zk ON (((z.id = zk.zaak_id) AND (zk.bibliotheek_kenmerken_id = ztk.bibliotheek_kenmerken_id))))
  GROUP BY z.id, COALESCE(zk.value, '{}'::text[]), bk.magic_string, bk.id, bk.value_type;


--
-- Name: bedrijf_authenticatie bedrijf_authenticatie_insert_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER bedrijf_authenticatie_insert_trigger BEFORE INSERT ON public.bedrijf_authenticatie FOR EACH ROW EXECUTE FUNCTION public.insert_timestamps();


--
-- Name: bedrijf_authenticatie bedrijf_authenticatie_update_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER bedrijf_authenticatie_update_trigger BEFORE UPDATE ON public.bedrijf_authenticatie FOR EACH ROW EXECUTE FUNCTION public.update_timestamps();


--
-- Name: beheer_import beheer_import_insert_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER beheer_import_insert_trigger BEFORE INSERT ON public.beheer_import FOR EACH ROW EXECUTE FUNCTION public.insert_timestamps();


--
-- Name: beheer_import_log beheer_import_log_insert_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER beheer_import_log_insert_trigger BEFORE INSERT ON public.beheer_import_log FOR EACH ROW EXECUTE FUNCTION public.insert_timestamps();


--
-- Name: beheer_import_log beheer_import_log_update_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER beheer_import_log_update_trigger BEFORE UPDATE ON public.beheer_import_log FOR EACH ROW EXECUTE FUNCTION public.update_timestamps();


--
-- Name: beheer_import beheer_import_update_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER beheer_import_update_trigger BEFORE UPDATE ON public.beheer_import FOR EACH ROW EXECUTE FUNCTION public.update_timestamps();


--
-- Name: betrokkene_notes betrokkene_notes_insert_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER betrokkene_notes_insert_trigger BEFORE INSERT ON public.betrokkene_notes FOR EACH ROW EXECUTE FUNCTION public.insert_timestamps();


--
-- Name: betrokkene_notes betrokkene_notes_update_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER betrokkene_notes_update_trigger BEFORE UPDATE ON public.betrokkene_notes FOR EACH ROW EXECUTE FUNCTION public.update_timestamps();


--
-- Name: bibliotheek_categorie bibliotheek_categorie_insert_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER bibliotheek_categorie_insert_trigger BEFORE INSERT ON public.bibliotheek_categorie FOR EACH ROW EXECUTE FUNCTION public.insert_timestamps();


--
-- Name: bibliotheek_categorie bibliotheek_categorie_update_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER bibliotheek_categorie_update_trigger BEFORE UPDATE ON public.bibliotheek_categorie FOR EACH ROW EXECUTE FUNCTION public.update_timestamps();


--
-- Name: bibliotheek_kenmerken bibliotheek_kenmerken_insert_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER bibliotheek_kenmerken_insert_trigger BEFORE INSERT ON public.bibliotheek_kenmerken FOR EACH ROW EXECUTE FUNCTION public.insert_timestamps();


--
-- Name: bibliotheek_kenmerken bibliotheek_kenmerken_update_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER bibliotheek_kenmerken_update_trigger BEFORE UPDATE ON public.bibliotheek_kenmerken FOR EACH ROW EXECUTE FUNCTION public.update_timestamps();


--
-- Name: bibliotheek_notificaties bibliotheek_notificaties_insert_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER bibliotheek_notificaties_insert_trigger BEFORE INSERT ON public.bibliotheek_notificaties FOR EACH ROW EXECUTE FUNCTION public.insert_timestamps();


--
-- Name: bibliotheek_notificaties bibliotheek_notificaties_update_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER bibliotheek_notificaties_update_trigger BEFORE UPDATE ON public.bibliotheek_notificaties FOR EACH ROW EXECUTE FUNCTION public.update_timestamps();


--
-- Name: bibliotheek_sjablonen bibliotheek_sjablonen_insert_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER bibliotheek_sjablonen_insert_trigger BEFORE INSERT ON public.bibliotheek_sjablonen FOR EACH ROW EXECUTE FUNCTION public.insert_timestamps();


--
-- Name: bibliotheek_sjablonen bibliotheek_sjablonen_update_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER bibliotheek_sjablonen_update_trigger BEFORE UPDATE ON public.bibliotheek_sjablonen FOR EACH ROW EXECUTE FUNCTION public.update_timestamps();


--
-- Name: queue_20210101 check_queue_object_id_constraint; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER check_queue_object_id_constraint BEFORE INSERT ON public.queue_20210101 FOR EACH ROW EXECUTE FUNCTION public.check_queue_object_id_constraint();


--
-- Name: queue_20210201 check_queue_object_id_constraint; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER check_queue_object_id_constraint BEFORE INSERT ON public.queue_20210201 FOR EACH ROW EXECUTE FUNCTION public.check_queue_object_id_constraint();


--
-- Name: queue_20210301 check_queue_object_id_constraint; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER check_queue_object_id_constraint BEFORE INSERT ON public.queue_20210301 FOR EACH ROW EXECUTE FUNCTION public.check_queue_object_id_constraint();


--
-- Name: queue_20210401 check_queue_object_id_constraint; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER check_queue_object_id_constraint BEFORE INSERT ON public.queue_20210401 FOR EACH ROW EXECUTE FUNCTION public.check_queue_object_id_constraint();


--
-- Name: queue_20210501 check_queue_object_id_constraint; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER check_queue_object_id_constraint BEFORE INSERT ON public.queue_20210501 FOR EACH ROW EXECUTE FUNCTION public.check_queue_object_id_constraint();


--
-- Name: queue_20210601 check_queue_object_id_constraint; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER check_queue_object_id_constraint BEFORE INSERT ON public.queue_20210601 FOR EACH ROW EXECUTE FUNCTION public.check_queue_object_id_constraint();


--
-- Name: queue_20210701 check_queue_object_id_constraint; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER check_queue_object_id_constraint BEFORE INSERT ON public.queue_20210701 FOR EACH ROW EXECUTE FUNCTION public.check_queue_object_id_constraint();


--
-- Name: queue_20210801 check_queue_object_id_constraint; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER check_queue_object_id_constraint BEFORE INSERT ON public.queue_20210801 FOR EACH ROW EXECUTE FUNCTION public.check_queue_object_id_constraint();


--
-- Name: queue_20210901 check_queue_object_id_constraint; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER check_queue_object_id_constraint BEFORE INSERT ON public.queue_20210901 FOR EACH ROW EXECUTE FUNCTION public.check_queue_object_id_constraint();


--
-- Name: queue_20211001 check_queue_object_id_constraint; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER check_queue_object_id_constraint BEFORE INSERT ON public.queue_20211001 FOR EACH ROW EXECUTE FUNCTION public.check_queue_object_id_constraint();


--
-- Name: queue_20211101 check_queue_object_id_constraint; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER check_queue_object_id_constraint BEFORE INSERT ON public.queue_20211101 FOR EACH ROW EXECUTE FUNCTION public.check_queue_object_id_constraint();


--
-- Name: queue_20211201 check_queue_object_id_constraint; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER check_queue_object_id_constraint BEFORE INSERT ON public.queue_20211201 FOR EACH ROW EXECUTE FUNCTION public.check_queue_object_id_constraint();


--
-- Name: queue_20210101 check_queue_parent_id_constraint; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER check_queue_parent_id_constraint BEFORE INSERT ON public.queue_20210101 FOR EACH ROW EXECUTE FUNCTION public.check_queue_parent_id_constraint();


--
-- Name: queue_20210201 check_queue_parent_id_constraint; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER check_queue_parent_id_constraint BEFORE INSERT ON public.queue_20210201 FOR EACH ROW EXECUTE FUNCTION public.check_queue_parent_id_constraint();


--
-- Name: queue_20210301 check_queue_parent_id_constraint; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER check_queue_parent_id_constraint BEFORE INSERT ON public.queue_20210301 FOR EACH ROW EXECUTE FUNCTION public.check_queue_parent_id_constraint();


--
-- Name: queue_20210401 check_queue_parent_id_constraint; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER check_queue_parent_id_constraint BEFORE INSERT ON public.queue_20210401 FOR EACH ROW EXECUTE FUNCTION public.check_queue_parent_id_constraint();


--
-- Name: queue_20210501 check_queue_parent_id_constraint; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER check_queue_parent_id_constraint BEFORE INSERT ON public.queue_20210501 FOR EACH ROW EXECUTE FUNCTION public.check_queue_parent_id_constraint();


--
-- Name: queue_20210601 check_queue_parent_id_constraint; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER check_queue_parent_id_constraint BEFORE INSERT ON public.queue_20210601 FOR EACH ROW EXECUTE FUNCTION public.check_queue_parent_id_constraint();


--
-- Name: queue_20210701 check_queue_parent_id_constraint; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER check_queue_parent_id_constraint BEFORE INSERT ON public.queue_20210701 FOR EACH ROW EXECUTE FUNCTION public.check_queue_parent_id_constraint();


--
-- Name: queue_20210801 check_queue_parent_id_constraint; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER check_queue_parent_id_constraint BEFORE INSERT ON public.queue_20210801 FOR EACH ROW EXECUTE FUNCTION public.check_queue_parent_id_constraint();


--
-- Name: queue_20210901 check_queue_parent_id_constraint; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER check_queue_parent_id_constraint BEFORE INSERT ON public.queue_20210901 FOR EACH ROW EXECUTE FUNCTION public.check_queue_parent_id_constraint();


--
-- Name: queue_20211001 check_queue_parent_id_constraint; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER check_queue_parent_id_constraint BEFORE INSERT ON public.queue_20211001 FOR EACH ROW EXECUTE FUNCTION public.check_queue_parent_id_constraint();


--
-- Name: queue_20211101 check_queue_parent_id_constraint; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER check_queue_parent_id_constraint BEFORE INSERT ON public.queue_20211101 FOR EACH ROW EXECUTE FUNCTION public.check_queue_parent_id_constraint();


--
-- Name: queue_20211201 check_queue_parent_id_constraint; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER check_queue_parent_id_constraint BEFORE INSERT ON public.queue_20211201 FOR EACH ROW EXECUTE FUNCTION public.check_queue_parent_id_constraint();


--
-- Name: contact_data contact_data_insert_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER contact_data_insert_trigger BEFORE INSERT ON public.contact_data FOR EACH ROW EXECUTE FUNCTION public.insert_timestamps();


--
-- Name: contact_data contact_data_update_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER contact_data_update_trigger BEFORE UPDATE ON public.contact_data FOR EACH ROW EXECUTE FUNCTION public.update_timestamps();


--
-- Name: custom_object_type_version custom_object_type_version_update_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER custom_object_type_version_update_trigger BEFORE UPDATE ON public.custom_object_type_version FOR EACH ROW EXECUTE FUNCTION public.update_timestamps();


--
-- Name: custom_object_version custom_object_version_update_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER custom_object_version_update_trigger BEFORE UPDATE ON public.custom_object_version FOR EACH ROW EXECUTE FUNCTION public.update_timestamps();


--
-- Name: file file_insert_timestamp_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER file_insert_timestamp_trigger BEFORE INSERT ON public.file FOR EACH ROW EXECUTE FUNCTION public.insert_file_timestamps();


--
-- Name: file insert_display_name_for_file_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER insert_display_name_for_file_trigger BEFORE INSERT ON public.file FOR EACH ROW EXECUTE FUNCTION public.update_display_name_for_file();


--
-- Name: queue insert_leading_qitem; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER insert_leading_qitem BEFORE INSERT ON public.queue FOR EACH ROW EXECUTE FUNCTION public.set_leading_qitem();


--
-- Name: queue_20210101 insert_leading_qitem; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER insert_leading_qitem BEFORE INSERT ON public.queue_20210101 FOR EACH ROW EXECUTE FUNCTION public.set_leading_qitem();


--
-- Name: queue_20210201 insert_leading_qitem; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER insert_leading_qitem BEFORE INSERT ON public.queue_20210201 FOR EACH ROW EXECUTE FUNCTION public.set_leading_qitem();


--
-- Name: queue_20210301 insert_leading_qitem; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER insert_leading_qitem BEFORE INSERT ON public.queue_20210301 FOR EACH ROW EXECUTE FUNCTION public.set_leading_qitem();


--
-- Name: queue_20210401 insert_leading_qitem; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER insert_leading_qitem BEFORE INSERT ON public.queue_20210401 FOR EACH ROW EXECUTE FUNCTION public.set_leading_qitem();


--
-- Name: queue_20210501 insert_leading_qitem; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER insert_leading_qitem BEFORE INSERT ON public.queue_20210501 FOR EACH ROW EXECUTE FUNCTION public.set_leading_qitem();


--
-- Name: queue_20210601 insert_leading_qitem; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER insert_leading_qitem BEFORE INSERT ON public.queue_20210601 FOR EACH ROW EXECUTE FUNCTION public.set_leading_qitem();


--
-- Name: queue_20210701 insert_leading_qitem; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER insert_leading_qitem BEFORE INSERT ON public.queue_20210701 FOR EACH ROW EXECUTE FUNCTION public.set_leading_qitem();


--
-- Name: queue_20210801 insert_leading_qitem; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER insert_leading_qitem BEFORE INSERT ON public.queue_20210801 FOR EACH ROW EXECUTE FUNCTION public.set_leading_qitem();


--
-- Name: queue_20210901 insert_leading_qitem; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER insert_leading_qitem BEFORE INSERT ON public.queue_20210901 FOR EACH ROW EXECUTE FUNCTION public.set_leading_qitem();


--
-- Name: queue_20211001 insert_leading_qitem; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER insert_leading_qitem BEFORE INSERT ON public.queue_20211001 FOR EACH ROW EXECUTE FUNCTION public.set_leading_qitem();


--
-- Name: queue_20211101 insert_leading_qitem; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER insert_leading_qitem BEFORE INSERT ON public.queue_20211101 FOR EACH ROW EXECUTE FUNCTION public.set_leading_qitem();


--
-- Name: queue_20211201 insert_leading_qitem; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER insert_leading_qitem BEFORE INSERT ON public.queue_20211201 FOR EACH ROW EXECUTE FUNCTION public.set_leading_qitem();


--
-- Name: logging insert_name_cache_for_logging_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER insert_name_cache_for_logging_trigger BEFORE INSERT ON public.logging FOR EACH ROW EXECUTE FUNCTION public.update_name_cache_for_logging();


--
-- Name: logging logging_insert_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER logging_insert_trigger BEFORE INSERT ON public.logging FOR EACH ROW EXECUTE FUNCTION public.insert_timestamps();


--
-- Name: logging logging_update_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER logging_update_trigger BEFORE UPDATE ON public.logging FOR EACH ROW EXECUTE FUNCTION public.update_timestamps();


--
-- Name: parkeergebied parkeergebied_insert_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER parkeergebied_insert_trigger BEFORE INSERT ON public.parkeergebied FOR EACH ROW EXECUTE FUNCTION public.insert_timestamps();


--
-- Name: parkeergebied parkeergebied_update_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER parkeergebied_update_trigger BEFORE UPDATE ON public.parkeergebied FOR EACH ROW EXECUTE FUNCTION public.update_timestamps();


--
-- Name: zaaktype refresh_casetype_end_status; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER refresh_casetype_end_status AFTER UPDATE OF version ON public.zaaktype FOR EACH STATEMENT EXECUTE FUNCTION public.refresh_casetype_document_id_map();


--
-- Name: zaaktype_node refresh_casetype_end_status; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER refresh_casetype_end_status AFTER UPDATE OF logging_id ON public.zaaktype_node FOR EACH STATEMENT EXECUTE FUNCTION public.refresh_casetype_document_id_map();


--
-- Name: zaaktype_status refresh_casetype_end_status; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER refresh_casetype_end_status AFTER INSERT OR DELETE OR UPDATE OR TRUNCATE ON public.zaaktype_status FOR EACH STATEMENT EXECUTE FUNCTION public.refresh_casetype_end_status();


--
-- Name: zaaktype refresh_casetype_v1_reference; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER refresh_casetype_v1_reference AFTER INSERT OR DELETE OR UPDATE OR TRUNCATE ON public.zaaktype FOR EACH STATEMENT EXECUTE FUNCTION public.refresh_casetype_v1_reference();


--
-- Name: groups refresh_subject_position_matrix; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER refresh_subject_position_matrix AFTER INSERT OR DELETE OR UPDATE OR TRUNCATE ON public.groups FOR EACH STATEMENT EXECUTE FUNCTION public.refresh_subject_position_matrix();


--
-- Name: roles refresh_subject_position_matrix; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER refresh_subject_position_matrix AFTER INSERT OR DELETE OR UPDATE OR TRUNCATE ON public.roles FOR EACH STATEMENT EXECUTE FUNCTION public.refresh_subject_position_matrix();


--
-- Name: subject refresh_subject_position_matrix; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER refresh_subject_position_matrix AFTER INSERT OR DELETE OR UPDATE OF role_ids, group_ids, subject_type, uuid, username OR TRUNCATE ON public.subject FOR EACH STATEMENT EXECUTE FUNCTION public.refresh_subject_position_matrix();


--
-- Name: scheduled_jobs scheduled_jobs_insert_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER scheduled_jobs_insert_trigger BEFORE INSERT ON public.scheduled_jobs FOR EACH ROW EXECUTE FUNCTION public.insert_timestamps();


--
-- Name: scheduled_jobs scheduled_jobs_update_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER scheduled_jobs_update_trigger BEFORE UPDATE ON public.scheduled_jobs FOR EACH ROW EXECUTE FUNCTION public.update_timestamps();


--
-- Name: zaak set_confidential; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER set_confidential BEFORE INSERT OR UPDATE ON public.zaak FOR EACH ROW EXECUTE FUNCTION public.set_confidential_on_case();


--
-- Name: thread thread_insert_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER thread_insert_trigger BEFORE INSERT ON public.thread FOR EACH ROW EXECUTE FUNCTION public.insert_timestamps();


--
-- Name: thread_message thread_message_insert_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER thread_message_insert_trigger BEFORE INSERT ON public.thread_message FOR EACH ROW EXECUTE FUNCTION public.insert_timestamps();


--
-- Name: thread_message thread_message_update_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER thread_message_update_trigger BEFORE UPDATE ON public.thread_message FOR EACH ROW EXECUTE FUNCTION public.update_timestamps();


--
-- Name: thread thread_update_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER thread_update_trigger BEFORE UPDATE ON public.thread FOR EACH ROW EXECUTE FUNCTION public.update_timestamps();


--
-- Name: file update_display_name_for_file_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER update_display_name_for_file_trigger BEFORE UPDATE ON public.file FOR EACH ROW EXECUTE FUNCTION public.update_display_name_for_file();


--
-- Name: logging update_name_cache_for_logging_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER update_name_cache_for_logging_trigger BEFORE UPDATE ON public.logging FOR EACH ROW EXECUTE FUNCTION public.update_name_cache_for_logging();


--
-- Name: zaak update_subject_v1_json; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER update_subject_v1_json BEFORE INSERT OR UPDATE ON public.zaak FOR EACH ROW EXECUTE FUNCTION public.update_subject_json();


--
-- Name: zaaktype_node update_v0_json; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER update_v0_json BEFORE INSERT OR UPDATE ON public.zaaktype_node FOR EACH ROW EXECUTE FUNCTION public.zaaktype_node_v0_json();


--
-- Name: groups update_v1_json; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER update_v1_json BEFORE INSERT OR UPDATE ON public.groups FOR EACH ROW EXECUTE FUNCTION public.update_group_json();


--
-- Name: roles update_v1_json; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER update_v1_json BEFORE INSERT OR UPDATE ON public.roles FOR EACH ROW EXECUTE FUNCTION public.update_role_json();


--
-- Name: zaak_betrokkenen update_zaak_betrokkenen_cache; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER update_zaak_betrokkenen_cache AFTER INSERT OR DELETE OR UPDATE ON public.zaak_betrokkenen FOR EACH ROW EXECUTE FUNCTION public.sync_zaak_betrokkenen_cache();


--
-- Name: thread update_zaak_meta_count; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER update_zaak_meta_count AFTER INSERT OR UPDATE OF case_id, unread_employee_count ON public.thread FOR EACH ROW WHEN ((new.case_id IS NOT NULL)) EXECUTE FUNCTION public.zaak_meta_update_unread_count();


--
-- Name: zaak update_zaak_percentage; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER update_zaak_percentage BEFORE INSERT OR UPDATE OF milestone, zaaktype_node_id ON public.zaak FOR EACH ROW EXECUTE FUNCTION public.update_zaak_percentage();


--
-- Name: zaak zaak_aanvrager_update; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER zaak_aanvrager_update BEFORE INSERT OR UPDATE OF aanvrager_gm_id ON public.zaak FOR EACH ROW EXECUTE FUNCTION public.update_aanvrager_type();


--
-- Name: zaaktype_authorisation zaaktype_authorisation_insert_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER zaaktype_authorisation_insert_trigger BEFORE INSERT ON public.zaaktype_authorisation FOR EACH ROW EXECUTE FUNCTION public.insert_timestamps();


--
-- Name: zaaktype_authorisation zaaktype_authorisation_update_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER zaaktype_authorisation_update_trigger BEFORE UPDATE ON public.zaaktype_authorisation FOR EACH ROW EXECUTE FUNCTION public.update_timestamps();


--
-- Name: zaaktype_betrokkenen zaaktype_betrokkenen_insert_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER zaaktype_betrokkenen_insert_trigger BEFORE INSERT ON public.zaaktype_betrokkenen FOR EACH ROW EXECUTE FUNCTION public.insert_timestamps();


--
-- Name: zaaktype_betrokkenen zaaktype_betrokkenen_update_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER zaaktype_betrokkenen_update_trigger BEFORE UPDATE ON public.zaaktype_betrokkenen FOR EACH ROW EXECUTE FUNCTION public.update_timestamps();


--
-- Name: zaaktype zaaktype_insert_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER zaaktype_insert_trigger BEFORE INSERT ON public.zaaktype FOR EACH ROW EXECUTE FUNCTION public.insert_timestamps();


--
-- Name: zaaktype_kenmerken zaaktype_kenmerken_insert_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER zaaktype_kenmerken_insert_trigger BEFORE INSERT ON public.zaaktype_kenmerken FOR EACH ROW EXECUTE FUNCTION public.insert_timestamps();


--
-- Name: zaaktype_kenmerken zaaktype_kenmerken_update_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER zaaktype_kenmerken_update_trigger BEFORE UPDATE ON public.zaaktype_kenmerken FOR EACH ROW EXECUTE FUNCTION public.update_timestamps();


--
-- Name: zaaktype_node zaaktype_node_insert_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER zaaktype_node_insert_trigger BEFORE INSERT ON public.zaaktype_node FOR EACH ROW EXECUTE FUNCTION public.insert_timestamps();


--
-- Name: zaaktype_node zaaktype_node_update_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER zaaktype_node_update_trigger BEFORE UPDATE ON public.zaaktype_node FOR EACH ROW EXECUTE FUNCTION public.update_timestamps();


--
-- Name: zaaktype_notificatie zaaktype_notificatie_insert_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER zaaktype_notificatie_insert_trigger BEFORE INSERT ON public.zaaktype_notificatie FOR EACH ROW EXECUTE FUNCTION public.insert_timestamps();


--
-- Name: zaaktype_notificatie zaaktype_notificatie_update_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER zaaktype_notificatie_update_trigger BEFORE UPDATE ON public.zaaktype_notificatie FOR EACH ROW EXECUTE FUNCTION public.update_timestamps();


--
-- Name: zaaktype_regel zaaktype_regel_insert_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER zaaktype_regel_insert_trigger BEFORE INSERT ON public.zaaktype_regel FOR EACH ROW EXECUTE FUNCTION public.insert_timestamps();


--
-- Name: zaaktype_regel zaaktype_regel_update_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER zaaktype_regel_update_trigger BEFORE UPDATE ON public.zaaktype_regel FOR EACH ROW EXECUTE FUNCTION public.update_timestamps();


--
-- Name: zaaktype_relatie zaaktype_relatie_insert_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER zaaktype_relatie_insert_trigger BEFORE INSERT ON public.zaaktype_relatie FOR EACH ROW EXECUTE FUNCTION public.insert_timestamps();


--
-- Name: zaaktype_relatie zaaktype_relatie_update_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER zaaktype_relatie_update_trigger BEFORE UPDATE ON public.zaaktype_relatie FOR EACH ROW EXECUTE FUNCTION public.update_timestamps();


--
-- Name: zaaktype_resultaten zaaktype_resultaten_insert_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER zaaktype_resultaten_insert_trigger BEFORE INSERT ON public.zaaktype_resultaten FOR EACH ROW EXECUTE FUNCTION public.insert_timestamps();


--
-- Name: zaaktype_resultaten zaaktype_resultaten_update_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER zaaktype_resultaten_update_trigger BEFORE UPDATE ON public.zaaktype_resultaten FOR EACH ROW EXECUTE FUNCTION public.update_timestamps();


--
-- Name: zaaktype_sjablonen zaaktype_sjablonen_insert_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER zaaktype_sjablonen_insert_trigger BEFORE INSERT ON public.zaaktype_sjablonen FOR EACH ROW EXECUTE FUNCTION public.insert_timestamps();


--
-- Name: zaaktype_sjablonen zaaktype_sjablonen_update_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER zaaktype_sjablonen_update_trigger BEFORE UPDATE ON public.zaaktype_sjablonen FOR EACH ROW EXECUTE FUNCTION public.update_timestamps();


--
-- Name: zaaktype_status zaaktype_status_insert_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER zaaktype_status_insert_trigger BEFORE INSERT ON public.zaaktype_status FOR EACH ROW EXECUTE FUNCTION public.insert_timestamps();


--
-- Name: zaaktype_status zaaktype_status_update_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER zaaktype_status_update_trigger BEFORE UPDATE ON public.zaaktype_status FOR EACH ROW EXECUTE FUNCTION public.update_timestamps();


--
-- Name: zaaktype zaaktype_update_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER zaaktype_update_trigger BEFORE UPDATE ON public.zaaktype FOR EACH ROW EXECUTE FUNCTION public.update_timestamps();


--
-- Name: adres adres_natuurlijk_persoon_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.adres
    ADD CONSTRAINT adres_natuurlijk_persoon_id_fkey FOREIGN KEY (natuurlijk_persoon_id) REFERENCES public.natuurlijk_persoon(id);


--
-- Name: alternative_authentication_activation_link alternative_authentication_activation_link_subject_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.alternative_authentication_activation_link
    ADD CONSTRAINT alternative_authentication_activation_link_subject_id_fkey FOREIGN KEY (subject_id) REFERENCES public.subject(uuid);


--
-- Name: bedrijf bedrijf_related_custom_object_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bedrijf
    ADD CONSTRAINT bedrijf_related_custom_object_id_fkey FOREIGN KEY (related_custom_object_id) REFERENCES public.custom_object(id) ON DELETE SET NULL;


--
-- Name: beheer_import_log beheer_import_log_import_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.beheer_import_log
    ADD CONSTRAINT beheer_import_log_import_id_fkey FOREIGN KEY (import_id) REFERENCES public.beheer_import(id);


--
-- Name: object_bibliotheek_entry bibliotheek_categorie_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.object_bibliotheek_entry
    ADD CONSTRAINT bibliotheek_categorie_fkey FOREIGN KEY (bibliotheek_categorie_id) REFERENCES public.bibliotheek_categorie(id) ON DELETE CASCADE;


--
-- Name: bibliotheek_categorie bibliotheek_categorie_pid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bibliotheek_categorie
    ADD CONSTRAINT bibliotheek_categorie_pid_fkey FOREIGN KEY (pid) REFERENCES public.bibliotheek_categorie(id);


--
-- Name: bibliotheek_kenmerken bibliotheek_kenmerken_bibliotheek_categorie_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bibliotheek_kenmerken
    ADD CONSTRAINT bibliotheek_kenmerken_bibliotheek_categorie_id_fkey FOREIGN KEY (bibliotheek_categorie_id) REFERENCES public.bibliotheek_categorie(id);


--
-- Name: bibliotheek_kenmerken bibliotheek_kenmerken_file_metadata_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bibliotheek_kenmerken
    ADD CONSTRAINT bibliotheek_kenmerken_file_metadata_id_fkey FOREIGN KEY (file_metadata_id) REFERENCES public.file_metadata(id);


--
-- Name: bibliotheek_kenmerken_values bibliotheek_kenmerken_values_bibliotheek_kenmerken_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bibliotheek_kenmerken_values
    ADD CONSTRAINT bibliotheek_kenmerken_values_bibliotheek_kenmerken_id_fkey FOREIGN KEY (bibliotheek_kenmerken_id) REFERENCES public.bibliotheek_kenmerken(id) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE;


--
-- Name: bibliotheek_notificatie_kenmerk bibliotheek_notificatie_kenmerk_bibliotheek_kenmerken_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bibliotheek_notificatie_kenmerk
    ADD CONSTRAINT bibliotheek_notificatie_kenmerk_bibliotheek_kenmerken_id_fkey FOREIGN KEY (bibliotheek_kenmerken_id) REFERENCES public.bibliotheek_kenmerken(id);


--
-- Name: bibliotheek_notificatie_kenmerk bibliotheek_notificatie_kenmerk_bibliotheek_notificatie_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bibliotheek_notificatie_kenmerk
    ADD CONSTRAINT bibliotheek_notificatie_kenmerk_bibliotheek_notificatie_id_fkey FOREIGN KEY (bibliotheek_notificatie_id) REFERENCES public.bibliotheek_notificaties(id);


--
-- Name: bibliotheek_notificaties bibliotheek_notificaties_bibliotheek_categorie_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bibliotheek_notificaties
    ADD CONSTRAINT bibliotheek_notificaties_bibliotheek_categorie_id_fkey FOREIGN KEY (bibliotheek_categorie_id) REFERENCES public.bibliotheek_categorie(id);


--
-- Name: bibliotheek_sjablonen bibliotheek_sjablonen_bibliotheek_categorie_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bibliotheek_sjablonen
    ADD CONSTRAINT bibliotheek_sjablonen_bibliotheek_categorie_id_fkey FOREIGN KEY (bibliotheek_categorie_id) REFERENCES public.bibliotheek_categorie(id);


--
-- Name: bibliotheek_sjablonen bibliotheek_sjablonen_filestore_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bibliotheek_sjablonen
    ADD CONSTRAINT bibliotheek_sjablonen_filestore_id_fkey FOREIGN KEY (filestore_id) REFERENCES public.filestore(id);


--
-- Name: bibliotheek_sjablonen bibliotheek_sjablonen_interface_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bibliotheek_sjablonen
    ADD CONSTRAINT bibliotheek_sjablonen_interface_id_fkey FOREIGN KEY (interface_id) REFERENCES public.interface(id);


--
-- Name: bibliotheek_sjablonen_magic_string bibliotheek_sjablonen_magic_strin_bibliotheek_sjablonen_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bibliotheek_sjablonen_magic_string
    ADD CONSTRAINT bibliotheek_sjablonen_magic_strin_bibliotheek_sjablonen_id_fkey FOREIGN KEY (bibliotheek_sjablonen_id) REFERENCES public.bibliotheek_sjablonen(id);


--
-- Name: case_action case_actions_case_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.case_action
    ADD CONSTRAINT case_actions_case_id_fkey FOREIGN KEY (case_id) REFERENCES public.zaak(id);


--
-- Name: case_action case_actions_casetype_status_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.case_action
    ADD CONSTRAINT case_actions_casetype_status_id_fkey FOREIGN KEY (casetype_status_id) REFERENCES public.zaaktype_status(id);


--
-- Name: case_property case_property_case_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.case_property
    ADD CONSTRAINT case_property_case_id_fkey FOREIGN KEY (case_id) REFERENCES public.zaak(id) ON DELETE CASCADE;


--
-- Name: case_property case_property_object_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.case_property
    ADD CONSTRAINT case_property_object_id_fkey FOREIGN KEY (object_id) REFERENCES public.object_data(uuid) ON DELETE CASCADE;


--
-- Name: case_relation case_relation_case_id_a_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.case_relation
    ADD CONSTRAINT case_relation_case_id_a_fkey FOREIGN KEY (case_id_a) REFERENCES public.zaak(id);


--
-- Name: case_relation case_relation_case_id_b_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.case_relation
    ADD CONSTRAINT case_relation_case_id_b_fkey FOREIGN KEY (case_id_b) REFERENCES public.zaak(id);


--
-- Name: zaaktype_node casetype_node_logging_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_node
    ADD CONSTRAINT casetype_node_logging_id_fkey FOREIGN KEY (logging_id) REFERENCES public.logging(id);


--
-- Name: checklist checklist_case_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.checklist
    ADD CONSTRAINT checklist_case_id_fkey FOREIGN KEY (case_id) REFERENCES public.zaak(id);


--
-- Name: checklist_item checklist_item_assignee_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.checklist_item
    ADD CONSTRAINT checklist_item_assignee_id_fkey FOREIGN KEY (assignee_id) REFERENCES public.subject(id) ON DELETE SET NULL;


--
-- Name: checklist_item checklist_item_checklist_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.checklist_item
    ADD CONSTRAINT checklist_item_checklist_id_fkey FOREIGN KEY (checklist_id) REFERENCES public.checklist(id);


--
-- Name: contactmoment contactmoment_case_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.contactmoment
    ADD CONSTRAINT contactmoment_case_id_fkey FOREIGN KEY (case_id) REFERENCES public.zaak(id);


--
-- Name: contactmoment_email contactmoment_email_contactmoment_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.contactmoment_email
    ADD CONSTRAINT contactmoment_email_contactmoment_id_fkey FOREIGN KEY (contactmoment_id) REFERENCES public.contactmoment(id);


--
-- Name: contactmoment_email contactmoment_email_filestore_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.contactmoment_email
    ADD CONSTRAINT contactmoment_email_filestore_id_fkey FOREIGN KEY (filestore_id) REFERENCES public.filestore(id);


--
-- Name: contactmoment_note contactmoment_note_contactmoment_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.contactmoment_note
    ADD CONSTRAINT contactmoment_note_contactmoment_id_fkey FOREIGN KEY (contactmoment_id) REFERENCES public.contactmoment(id);


--
-- Name: custom_object custom_object_custom_object_version_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.custom_object
    ADD CONSTRAINT custom_object_custom_object_version_id_fkey FOREIGN KEY (custom_object_version_id) REFERENCES public.custom_object_version(id);


--
-- Name: custom_object_relationship custom_object_relationship_custom_object_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.custom_object_relationship
    ADD CONSTRAINT custom_object_relationship_custom_object_id_fkey FOREIGN KEY (custom_object_id) REFERENCES public.custom_object(id) ON DELETE CASCADE;


--
-- Name: custom_object_relationship custom_object_relationship_custom_object_version_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.custom_object_relationship
    ADD CONSTRAINT custom_object_relationship_custom_object_version_id_fkey FOREIGN KEY (custom_object_version_id) REFERENCES public.custom_object_version(id);


--
-- Name: custom_object_relationship custom_object_relationship_related_case_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.custom_object_relationship
    ADD CONSTRAINT custom_object_relationship_related_case_id_fkey FOREIGN KEY (related_case_id) REFERENCES public.zaak(id);


--
-- Name: custom_object_relationship custom_object_relationship_related_custom_object_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.custom_object_relationship
    ADD CONSTRAINT custom_object_relationship_related_custom_object_id_fkey FOREIGN KEY (related_custom_object_id) REFERENCES public.custom_object(id) ON DELETE CASCADE;


--
-- Name: custom_object_relationship custom_object_relationship_related_document_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.custom_object_relationship
    ADD CONSTRAINT custom_object_relationship_related_document_id_fkey FOREIGN KEY (related_document_id) REFERENCES public.file(id);


--
-- Name: custom_object_relationship custom_object_relationship_related_employee_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.custom_object_relationship
    ADD CONSTRAINT custom_object_relationship_related_employee_id_fkey FOREIGN KEY (related_employee_id) REFERENCES public.subject(id);


--
-- Name: custom_object_relationship custom_object_relationship_related_organization_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.custom_object_relationship
    ADD CONSTRAINT custom_object_relationship_related_organization_id_fkey FOREIGN KEY (related_organization_id) REFERENCES public.bedrijf(id);


--
-- Name: custom_object_relationship custom_object_relationship_related_person_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.custom_object_relationship
    ADD CONSTRAINT custom_object_relationship_related_person_id_fkey FOREIGN KEY (related_person_id) REFERENCES public.natuurlijk_persoon(id);


--
-- Name: custom_object_relationship custom_object_relationship_source_custom_field_type_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.custom_object_relationship
    ADD CONSTRAINT custom_object_relationship_source_custom_field_type_id_fkey FOREIGN KEY (source_custom_field_type_id) REFERENCES public.bibliotheek_kenmerken(id);


--
-- Name: custom_object_type custom_object_type_catalog_folder_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.custom_object_type
    ADD CONSTRAINT custom_object_type_catalog_folder_id_fkey FOREIGN KEY (catalog_folder_id) REFERENCES public.bibliotheek_categorie(id);


--
-- Name: custom_object_type custom_object_type_custom_object_type_version_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.custom_object_type
    ADD CONSTRAINT custom_object_type_custom_object_type_version_id_fkey FOREIGN KEY (custom_object_type_version_id) REFERENCES public.custom_object_type_version(id);


--
-- Name: custom_object_type_version custom_object_type_version_custom_object_type_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.custom_object_type_version
    ADD CONSTRAINT custom_object_type_version_custom_object_type_id_fkey FOREIGN KEY (custom_object_type_id) REFERENCES public.custom_object_type(id);


--
-- Name: custom_object_version custom_object_version_custom_object_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.custom_object_version
    ADD CONSTRAINT custom_object_version_custom_object_id_fkey FOREIGN KEY (custom_object_id) REFERENCES public.custom_object(id) ON DELETE CASCADE;


--
-- Name: custom_object_version custom_object_version_custom_object_type_version_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.custom_object_version
    ADD CONSTRAINT custom_object_version_custom_object_type_version_id_fkey FOREIGN KEY (custom_object_type_version_id) REFERENCES public.custom_object_type_version(id);


--
-- Name: custom_object_version custom_object_version_custom_object_version_content_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.custom_object_version
    ADD CONSTRAINT custom_object_version_custom_object_version_content_id_fkey FOREIGN KEY (custom_object_version_content_id) REFERENCES public.custom_object_version_content(id) ON DELETE CASCADE;


--
-- Name: directory directory_case_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.directory
    ADD CONSTRAINT directory_case_id_fkey FOREIGN KEY (case_id) REFERENCES public.zaak(id);


--
-- Name: export_queue export_queue_filestore_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.export_queue
    ADD CONSTRAINT export_queue_filestore_id_fkey FOREIGN KEY (filestore_id) REFERENCES public.filestore(id);


--
-- Name: export_queue export_queue_subject_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.export_queue
    ADD CONSTRAINT export_queue_subject_id_fkey FOREIGN KEY (subject_id) REFERENCES public.subject(id);


--
-- Name: export_queue export_queue_subject_uuid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.export_queue
    ADD CONSTRAINT export_queue_subject_uuid_fkey FOREIGN KEY (subject_uuid) REFERENCES public.subject(uuid);


--
-- Name: file_annotation file_annotation_file_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.file_annotation
    ADD CONSTRAINT file_annotation_file_id_fkey FOREIGN KEY (file_id) REFERENCES public.file(id);


--
-- Name: file_case_document file_case_document_bibliotheek_kenmerken_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.file_case_document
    ADD CONSTRAINT file_case_document_bibliotheek_kenmerken_id_fkey FOREIGN KEY (bibliotheek_kenmerken_id) REFERENCES public.bibliotheek_kenmerken(id);


--
-- Name: file_case_document file_case_document_case_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.file_case_document
    ADD CONSTRAINT file_case_document_case_id_fkey FOREIGN KEY (case_id) REFERENCES public.zaak(id);


--
-- Name: file_case_document file_case_document_file_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.file_case_document
    ADD CONSTRAINT file_case_document_file_id_fkey FOREIGN KEY (file_id) REFERENCES public.file(id);


--
-- Name: file file_case_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.file
    ADD CONSTRAINT file_case_id_fkey FOREIGN KEY (case_id) REFERENCES public.zaak(id);


--
-- Name: file file_directory_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.file
    ADD CONSTRAINT file_directory_id_fkey FOREIGN KEY (directory_id) REFERENCES public.directory(id);


--
-- Name: file file_filestore_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.file
    ADD CONSTRAINT file_filestore_id_fkey FOREIGN KEY (filestore_id) REFERENCES public.filestore(id);


--
-- Name: file file_intake_group_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.file
    ADD CONSTRAINT file_intake_group_id_fk FOREIGN KEY (intake_group_id) REFERENCES public.groups(id) ON DELETE SET NULL;


--
-- Name: file file_intake_role_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.file
    ADD CONSTRAINT file_intake_role_id_fk FOREIGN KEY (intake_role_id) REFERENCES public.roles(id) ON DELETE SET NULL;


--
-- Name: file file_is_duplicate_of_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.file
    ADD CONSTRAINT file_is_duplicate_of_fkey FOREIGN KEY (is_duplicate_of) REFERENCES public.file(id) ON DELETE SET NULL;


--
-- Name: file file_metadata_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.file
    ADD CONSTRAINT file_metadata_id_fkey FOREIGN KEY (metadata_id) REFERENCES public.file_metadata(id);


--
-- Name: file file_root_file_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.file
    ADD CONSTRAINT file_root_file_id_fkey FOREIGN KEY (root_file_id) REFERENCES public.file(id);


--
-- Name: file file_scheduled_jobs_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.file
    ADD CONSTRAINT file_scheduled_jobs_id_fkey FOREIGN KEY (scheduled_jobs_id) REFERENCES public.scheduled_jobs(id);


--
-- Name: file_derivative file_thumbnail_file_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.file_derivative
    ADD CONSTRAINT file_thumbnail_file_id_fkey FOREIGN KEY (file_id) REFERENCES public.file(id);


--
-- Name: file_derivative file_thumbnail_filestore_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.file_derivative
    ADD CONSTRAINT file_thumbnail_filestore_id_fkey FOREIGN KEY (filestore_id) REFERENCES public.filestore(id);


--
-- Name: gegevensmagazijn_subjecten gegevensmagazijn_subjecten_nnp_uuid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.gegevensmagazijn_subjecten
    ADD CONSTRAINT gegevensmagazijn_subjecten_nnp_uuid_fkey FOREIGN KEY (nnp_uuid) REFERENCES public.bedrijf(uuid);


--
-- Name: gegevensmagazijn_subjecten gegevensmagazijn_subjecten_subject_uuid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.gegevensmagazijn_subjecten
    ADD CONSTRAINT gegevensmagazijn_subjecten_subject_uuid_fkey FOREIGN KEY (subject_uuid) REFERENCES public.subject(uuid);


--
-- Name: gm_adres gm_adres_natuurlijk_persoon_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.gm_adres
    ADD CONSTRAINT gm_adres_natuurlijk_persoon_id_fkey FOREIGN KEY (natuurlijk_persoon_id) REFERENCES public.gm_natuurlijk_persoon(id);


--
-- Name: gm_natuurlijk_persoon gm_natuurlijk_persoon_adres_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.gm_natuurlijk_persoon
    ADD CONSTRAINT gm_natuurlijk_persoon_adres_id_fkey FOREIGN KEY (adres_id) REFERENCES public.gm_adres(id) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE;


--
-- Name: interface interface_case_type_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.interface
    ADD CONSTRAINT interface_case_type_id_fkey FOREIGN KEY (case_type_id) REFERENCES public.zaaktype(id);


--
-- Name: interface interface_objecttype_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.interface
    ADD CONSTRAINT interface_objecttype_id_fkey FOREIGN KEY (objecttype_id) REFERENCES public.object_data(uuid) ON DELETE SET NULL;


--
-- Name: logging logging_object_uuid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.logging
    ADD CONSTRAINT logging_object_uuid_fkey FOREIGN KEY (object_uuid) REFERENCES public.object_data(uuid) ON DELETE SET NULL;


--
-- Name: message message_logging_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.message
    ADD CONSTRAINT message_logging_id_fkey FOREIGN KEY (logging_id) REFERENCES public.logging(id);


--
-- Name: natuurlijk_persoon natuurlijk_persoon_adres_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.natuurlijk_persoon
    ADD CONSTRAINT natuurlijk_persoon_adres_id_fk FOREIGN KEY (adres_id) REFERENCES public.adres(id);


--
-- Name: natuurlijk_persoon natuurlijk_persoon_related_custom_object_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.natuurlijk_persoon
    ADD CONSTRAINT natuurlijk_persoon_related_custom_object_id_fkey FOREIGN KEY (related_custom_object_id) REFERENCES public.custom_object(id) ON DELETE SET NULL;


--
-- Name: object_bibliotheek_entry object_data; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.object_bibliotheek_entry
    ADD CONSTRAINT object_data FOREIGN KEY (object_uuid) REFERENCES public.object_data(uuid) ON DELETE CASCADE;


--
-- Name: object_data object_data_class_uuid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.object_data
    ADD CONSTRAINT object_data_class_uuid_fkey FOREIGN KEY (class_uuid) REFERENCES public.object_data(uuid);


--
-- Name: object_acl_entry object_data_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.object_acl_entry
    ADD CONSTRAINT object_data_fkey FOREIGN KEY (object_uuid) REFERENCES public.object_data(uuid) ON DELETE CASCADE;


--
-- Name: object_mutation object_mutation_lock_object_uuid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.object_mutation
    ADD CONSTRAINT object_mutation_lock_object_uuid_fkey FOREIGN KEY (lock_object_uuid) REFERENCES public.object_data(uuid);


--
-- Name: object_mutation object_mutation_object_uuid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.object_mutation
    ADD CONSTRAINT object_mutation_object_uuid_fkey FOREIGN KEY (object_uuid) REFERENCES public.object_data(uuid) ON DELETE CASCADE;


--
-- Name: object_mutation object_mutation_subject_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.object_mutation
    ADD CONSTRAINT object_mutation_subject_id_fkey FOREIGN KEY (subject_id) REFERENCES public.subject(id);


--
-- Name: object_relation object_relation_object_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.object_relation
    ADD CONSTRAINT object_relation_object_id_fkey FOREIGN KEY (object_id) REFERENCES public.object_data(uuid) ON DELETE CASCADE DEFERRABLE;


--
-- Name: object_relation object_relation_object_uuid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.object_relation
    ADD CONSTRAINT object_relation_object_uuid_fkey FOREIGN KEY (object_uuid) REFERENCES public.object_data(uuid) ON DELETE CASCADE DEFERRABLE;


--
-- Name: object_relationships object_relationships_object1_uuid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.object_relationships
    ADD CONSTRAINT object_relationships_object1_uuid_fkey FOREIGN KEY (object1_uuid) REFERENCES public.object_data(uuid) ON DELETE CASCADE;


--
-- Name: object_relationships object_relationships_object2_uuid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.object_relationships
    ADD CONSTRAINT object_relationships_object2_uuid_fkey FOREIGN KEY (object2_uuid) REFERENCES public.object_data(uuid) ON DELETE CASCADE;


--
-- Name: object_subscription object_subscription_config_interface_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.object_subscription
    ADD CONSTRAINT object_subscription_config_interface_id_fkey FOREIGN KEY (config_interface_id) REFERENCES public.interface(id);


--
-- Name: object_subscription object_subscription_interface_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.object_subscription
    ADD CONSTRAINT object_subscription_interface_id_fkey FOREIGN KEY (interface_id) REFERENCES public.interface(id);


--
-- Name: queue queue_object_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.queue
    ADD CONSTRAINT queue_object_id_fkey FOREIGN KEY (object_id) REFERENCES public.object_data(uuid) ON DELETE CASCADE DEFERRABLE;


--
-- Name: queue queue_parent_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.queue
    ADD CONSTRAINT queue_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES public.queue(id) ON DELETE SET NULL;


--
-- Name: role_rights role_rights_rights_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.role_rights
    ADD CONSTRAINT role_rights_rights_name_fkey FOREIGN KEY (rights_name) REFERENCES public.rights(name);


--
-- Name: role_rights role_rights_role_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.role_rights
    ADD CONSTRAINT role_rights_role_id_fkey FOREIGN KEY (role_id) REFERENCES public.roles(id);


--
-- Name: roles roles_parent_group_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.roles
    ADD CONSTRAINT roles_parent_group_id_fkey FOREIGN KEY (parent_group_id) REFERENCES public.groups(id);


--
-- Name: sbus_logging sbus_logging_pid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sbus_logging
    ADD CONSTRAINT sbus_logging_pid_fkey FOREIGN KEY (pid) REFERENCES public.sbus_logging(id);


--
-- Name: sbus_logging sbus_logging_sbus_traffic_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sbus_logging
    ADD CONSTRAINT sbus_logging_sbus_traffic_id_fkey FOREIGN KEY (sbus_traffic_id) REFERENCES public.sbus_traffic(id);


--
-- Name: scheduled_jobs scheduled_jobs_case_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.scheduled_jobs
    ADD CONSTRAINT scheduled_jobs_case_id_fkey FOREIGN KEY (case_id) REFERENCES public.zaak(id);


--
-- Name: search_query_delen search_query_delen_search_query_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.search_query_delen
    ADD CONSTRAINT search_query_delen_search_query_id_fkey FOREIGN KEY (search_query_id) REFERENCES public.search_query(id);


--
-- Name: service_geojson_relationship service_geojson_relationship_service_geojson_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.service_geojson_relationship
    ADD CONSTRAINT service_geojson_relationship_service_geojson_id_fkey FOREIGN KEY (service_geojson_id) REFERENCES public.service_geojson(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: session_invitation session_invitation_subject_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.session_invitation
    ADD CONSTRAINT session_invitation_subject_id_fkey FOREIGN KEY (subject_id) REFERENCES public.subject(uuid) ON DELETE CASCADE;


--
-- Name: subject_login_history subject_login_history_subject_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.subject_login_history
    ADD CONSTRAINT subject_login_history_subject_id_fkey FOREIGN KEY (subject_id) REFERENCES public.subject(id);


--
-- Name: subject_login_history subject_login_history_subject_uuid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.subject_login_history
    ADD CONSTRAINT subject_login_history_subject_uuid_fkey FOREIGN KEY (subject_uuid) REFERENCES public.subject(uuid);


--
-- Name: subject subject_related_custom_object_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.subject
    ADD CONSTRAINT subject_related_custom_object_id_fkey FOREIGN KEY (related_custom_object_id) REFERENCES public.custom_object(id) ON DELETE SET NULL;


--
-- Name: thread thread_case_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.thread
    ADD CONSTRAINT thread_case_id_fkey FOREIGN KEY (case_id) REFERENCES public.zaak(id);


--
-- Name: thread_message_attachment_derivative thread_message_attachment_der_thread_message_attachment_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.thread_message_attachment_derivative
    ADD CONSTRAINT thread_message_attachment_der_thread_message_attachment_id_fkey FOREIGN KEY (thread_message_attachment_id) REFERENCES public.thread_message_attachment(id) ON DELETE CASCADE;


--
-- Name: thread_message_attachment_derivative thread_message_attachment_derivative_filestore_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.thread_message_attachment_derivative
    ADD CONSTRAINT thread_message_attachment_derivative_filestore_id_fkey FOREIGN KEY (filestore_id) REFERENCES public.filestore(id);


--
-- Name: thread_message_attachment thread_message_attachment_filestore_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.thread_message_attachment
    ADD CONSTRAINT thread_message_attachment_filestore_id_fkey FOREIGN KEY (filestore_id) REFERENCES public.filestore(id);


--
-- Name: thread_message_attachment thread_message_attachment_thread_message_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.thread_message_attachment
    ADD CONSTRAINT thread_message_attachment_thread_message_id_fkey FOREIGN KEY (thread_message_id) REFERENCES public.thread_message(id);


--
-- Name: thread_message_external thread_message_external_source_file_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.thread_message_external
    ADD CONSTRAINT thread_message_external_source_file_id_fkey FOREIGN KEY (source_file_id) REFERENCES public.filestore(id);


--
-- Name: thread_message thread_message_thread_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.thread_message
    ADD CONSTRAINT thread_message_thread_id_fkey FOREIGN KEY (thread_id) REFERENCES public.thread(id);


--
-- Name: thread_message thread_message_thread_message_contact_moment_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.thread_message
    ADD CONSTRAINT thread_message_thread_message_contact_moment_id_fkey FOREIGN KEY (thread_message_contact_moment_id) REFERENCES public.thread_message_contact_moment(id);


--
-- Name: thread_message thread_message_thread_message_external_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.thread_message
    ADD CONSTRAINT thread_message_thread_message_external_id_fkey FOREIGN KEY (thread_message_external_id) REFERENCES public.thread_message_external(id);


--
-- Name: thread_message thread_message_thread_message_note_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.thread_message
    ADD CONSTRAINT thread_message_thread_message_note_id_fkey FOREIGN KEY (thread_message_note_id) REFERENCES public.thread_message_note(id);


--
-- Name: transaction transaction_input_file_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.transaction
    ADD CONSTRAINT transaction_input_file_fkey FOREIGN KEY (input_file) REFERENCES public.filestore(id);


--
-- Name: transaction transaction_interface_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.transaction
    ADD CONSTRAINT transaction_interface_id_fkey FOREIGN KEY (interface_id) REFERENCES public.interface(id);


--
-- Name: transaction_record_to_object transaction_record_to_object_transaction_record_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.transaction_record_to_object
    ADD CONSTRAINT transaction_record_to_object_transaction_record_id_fkey FOREIGN KEY (transaction_record_id) REFERENCES public.transaction_record(id) ON DELETE CASCADE DEFERRABLE;


--
-- Name: transaction_record transaction_record_transaction_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.transaction_record
    ADD CONSTRAINT transaction_record_transaction_id_fkey FOREIGN KEY (transaction_id) REFERENCES public.transaction(id) ON DELETE CASCADE DEFERRABLE;


--
-- Name: user_entity user_entity_source_interface_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.user_entity
    ADD CONSTRAINT user_entity_source_interface_id_fkey FOREIGN KEY (source_interface_id) REFERENCES public.interface(id);


--
-- Name: user_entity user_entity_subject_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.user_entity
    ADD CONSTRAINT user_entity_subject_id_fkey FOREIGN KEY (subject_id) REFERENCES public.subject(id);


--
-- Name: zaak zaak_aanvrager_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak
    ADD CONSTRAINT zaak_aanvrager_fkey FOREIGN KEY (aanvrager) REFERENCES public.zaak_betrokkenen(id);


--
-- Name: zaak_authorisation zaak_authorisation_zaak_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak_authorisation
    ADD CONSTRAINT zaak_authorisation_zaak_id_fkey FOREIGN KEY (zaak_id) REFERENCES public.zaak(id);


--
-- Name: zaak_bag zaak_bag_pid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak_bag
    ADD CONSTRAINT zaak_bag_pid_fkey FOREIGN KEY (pid) REFERENCES public.zaak_bag(id);


--
-- Name: zaak_bag zaak_bag_zaak_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak_bag
    ADD CONSTRAINT zaak_bag_zaak_id_fkey FOREIGN KEY (zaak_id) REFERENCES public.zaak(id);


--
-- Name: zaak zaak_behandelaar_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak
    ADD CONSTRAINT zaak_behandelaar_fkey FOREIGN KEY (behandelaar) REFERENCES public.zaak_betrokkenen(id);


--
-- Name: zaak_betrokkenen zaak_betrokkenen_bibliotheek_kenmerken_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak_betrokkenen
    ADD CONSTRAINT zaak_betrokkenen_bibliotheek_kenmerken_id_fkey FOREIGN KEY (bibliotheek_kenmerken_id) REFERENCES public.bibliotheek_kenmerken(id) ON DELETE SET NULL;


--
-- Name: zaak_betrokkenen zaak_betrokkenen_zaak_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak_betrokkenen
    ADD CONSTRAINT zaak_betrokkenen_zaak_id_fkey FOREIGN KEY (zaak_id) REFERENCES public.zaak(id);


--
-- Name: zaak zaak_coordinator_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak
    ADD CONSTRAINT zaak_coordinator_fkey FOREIGN KEY (coordinator) REFERENCES public.zaak_betrokkenen(id);


--
-- Name: logging zaak_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.logging
    ADD CONSTRAINT zaak_id_fkey FOREIGN KEY (zaak_id) REFERENCES public.zaak(id);


--
-- Name: zaak_kenmerk zaak_kenmerk_bibliotheek_kenmerken_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak_kenmerk
    ADD CONSTRAINT zaak_kenmerk_bibliotheek_kenmerken_id_fkey FOREIGN KEY (bibliotheek_kenmerken_id) REFERENCES public.bibliotheek_kenmerken(id);


--
-- Name: zaak_kenmerk zaak_kenmerk_zaak_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak_kenmerk
    ADD CONSTRAINT zaak_kenmerk_zaak_id_fkey FOREIGN KEY (zaak_id) REFERENCES public.zaak(id) ON DELETE CASCADE;


--
-- Name: zaak zaak_locatie_correspondentie_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak
    ADD CONSTRAINT zaak_locatie_correspondentie_fkey FOREIGN KEY (locatie_correspondentie) REFERENCES public.zaak_bag(id);


--
-- Name: zaak zaak_locatie_zaak_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak
    ADD CONSTRAINT zaak_locatie_zaak_fkey FOREIGN KEY (locatie_zaak) REFERENCES public.zaak_bag(id);


--
-- Name: zaak_meta zaak_meta_zaak_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak_meta
    ADD CONSTRAINT zaak_meta_zaak_id_fkey FOREIGN KEY (zaak_id) REFERENCES public.zaak(id);


--
-- Name: zaak zaak_number_master_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak
    ADD CONSTRAINT zaak_number_master_fk FOREIGN KEY (number_master) REFERENCES public.zaak(id);


--
-- Name: zaak zaak_object_data_uuid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak
    ADD CONSTRAINT zaak_object_data_uuid_fkey FOREIGN KEY (uuid) REFERENCES public.object_data(uuid) ON DELETE SET NULL;


--
-- Name: zaak zaak_pid_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak
    ADD CONSTRAINT zaak_pid_fk FOREIGN KEY (pid) REFERENCES public.zaak(id);


--
-- Name: zaak zaak_pid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak
    ADD CONSTRAINT zaak_pid_fkey FOREIGN KEY (pid) REFERENCES public.zaak(id);


--
-- Name: zaak zaak_relates_to_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak
    ADD CONSTRAINT zaak_relates_to_fkey FOREIGN KEY (relates_to) REFERENCES public.zaak(id);


--
-- Name: zaak zaak_resultaat_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak
    ADD CONSTRAINT zaak_resultaat_id_fkey FOREIGN KEY (resultaat_id) REFERENCES public.zaaktype_resultaten(id);


--
-- Name: zaak_subcase zaak_subcase_relation_zaak_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak_subcase
    ADD CONSTRAINT zaak_subcase_relation_zaak_id_fkey FOREIGN KEY (relation_zaak_id) REFERENCES public.zaak(id);


--
-- Name: zaak_subcase zaak_subcase_zaak_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak_subcase
    ADD CONSTRAINT zaak_subcase_zaak_id_fkey FOREIGN KEY (zaak_id) REFERENCES public.zaak(id);


--
-- Name: zaak zaak_vervolg_van_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak
    ADD CONSTRAINT zaak_vervolg_van_fkey FOREIGN KEY (vervolg_van) REFERENCES public.zaak(id);


--
-- Name: zaak zaak_zaaktype_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak
    ADD CONSTRAINT zaak_zaaktype_id_fkey FOREIGN KEY (zaaktype_id) REFERENCES public.zaaktype(id);


--
-- Name: zaak_onafgerond zaak_zaaktype_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak_onafgerond
    ADD CONSTRAINT zaak_zaaktype_id_fkey FOREIGN KEY (zaaktype_id) REFERENCES public.zaaktype(id);


--
-- Name: zaak zaak_zaaktype_node_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak
    ADD CONSTRAINT zaak_zaaktype_node_id_fkey FOREIGN KEY (zaaktype_node_id) REFERENCES public.zaaktype_node(id);


--
-- Name: zaaktype_authorisation zaaktype_authorisation_zaaktype_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_authorisation
    ADD CONSTRAINT zaaktype_authorisation_zaaktype_id_fkey FOREIGN KEY (zaaktype_id) REFERENCES public.zaaktype(id);


--
-- Name: zaaktype_authorisation zaaktype_authorisation_zaaktype_node_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_authorisation
    ADD CONSTRAINT zaaktype_authorisation_zaaktype_node_id_fkey FOREIGN KEY (zaaktype_node_id) REFERENCES public.zaaktype_node(id) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE;


--
-- Name: zaaktype_betrokkenen zaaktype_betrokkenen_zaaktype_node_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_betrokkenen
    ADD CONSTRAINT zaaktype_betrokkenen_zaaktype_node_id_fkey FOREIGN KEY (zaaktype_node_id) REFERENCES public.zaaktype_node(id) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE;


--
-- Name: zaaktype zaaktype_bibliotheek_categorie_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype
    ADD CONSTRAINT zaaktype_bibliotheek_categorie_id_fkey FOREIGN KEY (bibliotheek_categorie_id) REFERENCES public.bibliotheek_categorie(id) DEFERRABLE;


--
-- Name: zaaktype_kenmerken zaaktype_kenmerken_bibliotheek_kenmerken_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_kenmerken
    ADD CONSTRAINT zaaktype_kenmerken_bibliotheek_kenmerken_id_fkey FOREIGN KEY (bibliotheek_kenmerken_id) REFERENCES public.bibliotheek_kenmerken(id);


--
-- Name: zaaktype_kenmerken zaaktype_kenmerken_custom_object_uuid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_kenmerken
    ADD CONSTRAINT zaaktype_kenmerken_custom_object_uuid_fkey FOREIGN KEY (custom_object_uuid) REFERENCES public.custom_object_type(uuid);


--
-- Name: zaaktype_kenmerken zaaktype_kenmerken_object_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_kenmerken
    ADD CONSTRAINT zaaktype_kenmerken_object_id_fkey FOREIGN KEY (object_id) REFERENCES public.object_data(uuid) ON DELETE RESTRICT;


--
-- Name: zaaktype_kenmerken zaaktype_kenmerken_zaak_status_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_kenmerken
    ADD CONSTRAINT zaaktype_kenmerken_zaak_status_id_fkey FOREIGN KEY (zaak_status_id) REFERENCES public.zaaktype_status(id);


--
-- Name: zaaktype_kenmerken zaaktype_kenmerken_zaaktype_node_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_kenmerken
    ADD CONSTRAINT zaaktype_kenmerken_zaaktype_node_id_fkey FOREIGN KEY (zaaktype_node_id) REFERENCES public.zaaktype_node(id);


--
-- Name: zaaktype_node zaaktype_node_moeder_zaaktype_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_node
    ADD CONSTRAINT zaaktype_node_moeder_zaaktype_id_fkey FOREIGN KEY (moeder_zaaktype_id) REFERENCES public.zaaktype(id) ON DELETE SET NULL;


--
-- Name: zaaktype_node zaaktype_node_zaaktype_definitie_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_node
    ADD CONSTRAINT zaaktype_node_zaaktype_definitie_id_fkey FOREIGN KEY (zaaktype_definitie_id) REFERENCES public.zaaktype_definitie(id) DEFERRABLE;


--
-- Name: zaaktype_node zaaktype_node_zaaktype_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_node
    ADD CONSTRAINT zaaktype_node_zaaktype_id_fkey FOREIGN KEY (zaaktype_id) REFERENCES public.zaaktype(id) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE;


--
-- Name: zaaktype_notificatie zaaktype_notificatie_bibliotheek_notificaties_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_notificatie
    ADD CONSTRAINT zaaktype_notificatie_bibliotheek_notificaties_id_fkey FOREIGN KEY (bibliotheek_notificaties_id) REFERENCES public.bibliotheek_notificaties(id);


--
-- Name: zaaktype_notificatie zaaktype_notificatie_zaak_status_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_notificatie
    ADD CONSTRAINT zaaktype_notificatie_zaak_status_id_fkey FOREIGN KEY (zaak_status_id) REFERENCES public.zaaktype_status(id);


--
-- Name: zaaktype_notificatie zaaktype_notificatie_zaaktype_node_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_notificatie
    ADD CONSTRAINT zaaktype_notificatie_zaaktype_node_id_fkey FOREIGN KEY (zaaktype_node_id) REFERENCES public.zaaktype_node(id);


--
-- Name: zaaktype_regel zaaktype_regel_zaak_status_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_regel
    ADD CONSTRAINT zaaktype_regel_zaak_status_id_fkey FOREIGN KEY (zaak_status_id) REFERENCES public.zaaktype_status(id);


--
-- Name: zaaktype_regel zaaktype_regel_zaaktype_node_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_regel
    ADD CONSTRAINT zaaktype_regel_zaaktype_node_id_fkey FOREIGN KEY (zaaktype_node_id) REFERENCES public.zaaktype_node(id);


--
-- Name: zaaktype_relatie zaaktype_relatie_relatie_zaaktype_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_relatie
    ADD CONSTRAINT zaaktype_relatie_relatie_zaaktype_id_fkey FOREIGN KEY (relatie_zaaktype_id) REFERENCES public.zaaktype(id) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE;


--
-- Name: zaaktype_relatie zaaktype_relatie_zaaktype_node_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_relatie
    ADD CONSTRAINT zaaktype_relatie_zaaktype_node_id_fkey FOREIGN KEY (zaaktype_node_id) REFERENCES public.zaaktype_node(id) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE;


--
-- Name: zaaktype_relatie zaaktype_relatie_zaaktype_status_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_relatie
    ADD CONSTRAINT zaaktype_relatie_zaaktype_status_id_fkey FOREIGN KEY (zaaktype_status_id) REFERENCES public.zaaktype_status(id) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE;


--
-- Name: zaaktype_resultaten zaaktype_resultaten_zaaktype_node_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_resultaten
    ADD CONSTRAINT zaaktype_resultaten_zaaktype_node_id_fkey FOREIGN KEY (zaaktype_node_id) REFERENCES public.zaaktype_node(id) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE;


--
-- Name: zaaktype_resultaten zaaktype_resultaten_zaaktype_status_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_resultaten
    ADD CONSTRAINT zaaktype_resultaten_zaaktype_status_id_fkey FOREIGN KEY (zaaktype_status_id) REFERENCES public.zaaktype_status(id) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE;


--
-- Name: zaaktype_sjablonen zaaktype_sjablonen_bibliotheek_kenmerken_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_sjablonen
    ADD CONSTRAINT zaaktype_sjablonen_bibliotheek_kenmerken_id_fkey FOREIGN KEY (bibliotheek_kenmerken_id) REFERENCES public.bibliotheek_kenmerken(id) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE;


--
-- Name: zaaktype_sjablonen zaaktype_sjablonen_bibliotheek_sjablonen_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_sjablonen
    ADD CONSTRAINT zaaktype_sjablonen_bibliotheek_sjablonen_id_fkey FOREIGN KEY (bibliotheek_sjablonen_id) REFERENCES public.bibliotheek_sjablonen(id);


--
-- Name: zaaktype_sjablonen zaaktype_sjablonen_zaak_status_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_sjablonen
    ADD CONSTRAINT zaaktype_sjablonen_zaak_status_id_fkey FOREIGN KEY (zaak_status_id) REFERENCES public.zaaktype_status(id);


--
-- Name: zaaktype_sjablonen zaaktype_sjablonen_zaaktype_node_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_sjablonen
    ADD CONSTRAINT zaaktype_sjablonen_zaaktype_node_id_fkey FOREIGN KEY (zaaktype_node_id) REFERENCES public.zaaktype_node(id);


--
-- Name: zaaktype_standaard_betrokkenen zaaktype_standaard_betrokkenen_zaaktype_node_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_standaard_betrokkenen
    ADD CONSTRAINT zaaktype_standaard_betrokkenen_zaaktype_node_id_fkey FOREIGN KEY (zaaktype_node_id) REFERENCES public.zaaktype_node(id);


--
-- Name: zaaktype_standaard_betrokkenen zaaktype_standaard_betrokkenen_zaaktype_status_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_standaard_betrokkenen
    ADD CONSTRAINT zaaktype_standaard_betrokkenen_zaaktype_status_id_fkey FOREIGN KEY (zaak_status_id) REFERENCES public.zaaktype_status(id);


--
-- Name: zaaktype_status_checklist_item zaaktype_status_checklist_item_zaaktype_status_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_status_checklist_item
    ADD CONSTRAINT zaaktype_status_checklist_item_zaaktype_status_id_fkey FOREIGN KEY (casetype_status_id) REFERENCES public.zaaktype_status(id);


--
-- Name: zaaktype_status zaaktype_status_zaaktype_node_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_status
    ADD CONSTRAINT zaaktype_status_zaaktype_node_id_fkey FOREIGN KEY (zaaktype_node_id) REFERENCES public.zaaktype_node(id) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE;


--
-- Name: zaaktype zaaktype_zaaktype_node_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype
    ADD CONSTRAINT zaaktype_zaaktype_node_id_fkey FOREIGN KEY (zaaktype_node_id) REFERENCES public.zaaktype_node(id) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE;


--
-- PostgreSQL database dump complete
--

