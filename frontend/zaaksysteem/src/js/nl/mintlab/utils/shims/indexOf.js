// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

(function () {
  window.zsDefine('nl.mintlab.utils.shims.indexOf', function () {
    var func,
      isArray = angular.isArray;

    if (Array.prototype.indexOf !== undefined) {
      func = function (array, searchElement, fromIndex) {
        if (isArray(array)) {
          return array.indexOf(searchElement, fromIndex);
        } else {
          return Array.prototype.indexOf.call(array, searchElement, fromIndex);
        }
      };
    } else {
      func = function (array, searchElement /*, fromIndex */) {
        var t = array;
        var len = t.length >>> 0;
        if (len === 0) {
          return -1;
        }
        var n = 0;
        if (arguments.length > 1) {
          n = Number(arguments[1]);
          if (n != n) {
            // shortcut for verifying if it's NaN
            n = 0;
          } else if (n != 0 && n != Infinity && n != -Infinity) {
            n = (n > 0 || -1) * Math.floor(Math.abs(n));
          }
        }
        if (n >= len) {
          return -1;
        }
        var k = n >= 0 ? n : Math.max(len - Math.abs(n), 0);
        for (; k < len; k++) {
          if (k in t && t[k] === searchElement) {
            return k;
          }
        }
        return -1;
      };
    }

    return func;
  });
})();
