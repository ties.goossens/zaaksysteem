import {
    openPage,
    openPageAs
} from './../../../../functions/common/navigate';
import {
    mouseOverCreateButton,
    mouseOut
} from './../../../../functions/common/mouse';
import getContactUrl from './../../../../functions/common/getValue/getContactUrl';

describe('when logging in as behandelaar', () => {
    beforeAll(() => {
        openPageAs('behandelaar');
    });

    describe('and opening the plus button', () => {
        beforeAll(() => {
            mouseOverCreateButton();
        });

        it('should not have the create contact button present', () => {
            expect($('li[data-name="contact"]').isPresent()).toBe(false);
        });

        afterAll(() => {
            mouseOut();
        });
    });

    describe('and opening a contact overview', () => {
        beforeAll(() => {
            browser.ignoreSynchronization = true;
            browser.get(getContactUrl('citizen', '1'));
        });

        it('should not have the edit contact button present', () => {
            expect($('[title="Bewerken"]').isPresent()).toBe(false);
        });

        afterAll(() => {
            browser.ignoreSynchronization = false;
            openPage();
        });
    });
});

describe('when logging in as contactbeheerder', () => {
    beforeAll(() => {
        openPageAs('contactbeheerder');
    });

    describe('and opening the plus button', () => {
        beforeAll(() => {
            mouseOverCreateButton();
        });

        it('should have the create contact button present', () => {
            expect($('li[data-name="contact"]').isPresent()).toBe(true);
        });

        afterAll(() => {
            mouseOut();
        });
    });

    describe('and opening a contact overview', () => {
        beforeAll(() => {
            browser.ignoreSynchronization = true;
            browser.get(getContactUrl('citizen', '1'));
        });

        it('should have the edit contact button present', () => {
            expect($('[title="Bewerken"]').isPresent()).toBe(true);
        });

        afterAll(() => {
            browser.ignoreSynchronization = false;
            openPage();
        });
    });
});

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
