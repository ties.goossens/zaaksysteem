BEGIN;

  CREATE OR REPLACE FUNCTION update_display_name_for_file() RETURNS TRIGGER
  LANGUAGE 'plpgsql' AS $$
  DECLARE
    subject_uuid uuid;
    dn text;
  BEGIN
      IF NEW.created_by IS NOT NULL
      THEN

        IF NEW.created_by NOT LIKE 'betrokkene-%'
        THEN
            NEW.created_by_display_name := NEW.created_by;
            RETURN NEW;
        END IF;

        SELECT INTO subject_uuid get_subject_by_legacy_id(NEW.created_by);

        IF subject_uuid IS NULL
        THEN
            NEW.created_by_display_name := '<Onbekend>';
            RETURN NEW;
        END IF;

        SELECT INTO dn get_subject_display_name_by_uuid(subject_uuid);
        IF dn IS NOT NULL
        THEN
            NEW.created_by_display_name := dn;
          RETURN NEW;
        END IF;
      END IF;

      RETURN NEW;
  END $$;

  DROP TRIGGER IF EXISTS update_display_name_for_file_trigger ON file;
  CREATE TRIGGER update_display_name_for_file_trigger
  BEFORE UPDATE ON file
  FOR EACH ROW
  EXECUTE PROCEDURE update_display_name_for_file();

  DROP TRIGGER IF EXISTS insert_display_name_for_file_trigger ON file;
  CREATE TRIGGER insert_display_name_for_file_trigger
  BEFORE INSERT ON file
  FOR EACH ROW
  EXECUTE PROCEDURE update_display_name_for_file();

  UPDATE file SET created_by = created_by;

COMMIT;
