// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import assign from 'lodash/assign';
import isEmpty from 'lodash/isEmpty';
import isPlainObject from 'lodash/isPlainObject';
import seamlessImmutable from 'seamless-immutable';
import createConfigurationReducer from '../createConfigurationReducer';
import {
  APPOINTMENT_READ_PENDING,
  APPOINTMENT_REMOVE_ERROR,
  APPOINTMENT_REMOVE_PENDING,
  appointmentCreateSuccess,
} from '../messages';

const EXCEPTION = 'exception';
const NOT_FOUND = 'object/not_found';

export default class Controller {
  constructor(
    scope,
    http,
    resource,
    composedReducer,
    dateFilter,
    snackbarService
  ) {
    const ctrl = this;
    // default to true because once this directive is displayed,
    // it will immediately start to retrieve the appointment information
    let loading = true;

    const providerConfigurationReducer = createConfigurationReducer(
      composedReducer,
      scope,
      ctrl.provider()
    );

    const getResourceOptions = () => {
      const value = ctrl.value();

      if (value) {
        return {
          url: `/api/v1/appointment/${value}`,
        };
      }
    };

    const appointmentResource = resource(getResourceOptions, {
      scope,
      cache: {
        disabled: true,
      },
    }).reduce((requestOptions, data) => {
      const responseObject = this.getResponseObject(data);

      if (responseObject) {
        const { instance, type } = responseObject;

        if (this.appointmentNotFound(type, instance.type)) {
          ctrl.onRemove();
        }

        return instance;
      }

      return seamlessImmutable({});
    });

    const appointmentReducer = composedReducer(
      {
        scope,
        waitUntilResolved: false,
      },
      appointmentResource,
      () => loading
    ).reduce((appointment) => {
      if (isEmpty(appointment)) {
        return APPOINTMENT_READ_PENDING;
      } else {
        // eslint-disable-line no-else-return
        // ZS-TODO: update eslint rule, ES2015 blockscope is useful
        const date = dateFilter(appointment.start_time, 'dd-MM-yyyy');
        const timeStart = dateFilter(appointment.start_time, 'HH:mm');
        const timeEnd = dateFilter(appointment.end_time, 'HH:mm');

        loading = false;

        return appointmentCreateSuccess(date, timeStart, timeEnd);
      }
    });

    // Need this reducer because a simple function returning
    // the loading variable caused the spinner to never stop spinning
    const loadingReducer = composedReducer({ scope }, () => loading).reduce(
      (isLoading) => isLoading
    );

    ctrl.isLoading = loadingReducer.data;

    ctrl.getLabel = appointmentReducer.data;

    ctrl.handleRemoveClick = () => {
      loading = true;

      snackbarService
        .wait(APPOINTMENT_REMOVE_PENDING, {
          collapse: 0,
          promise: http({
            method: 'POST',
            url: `/api/v1/sysin/interface/${
              providerConfigurationReducer.data().appointmentInterfaceUuid
            }/trigger/delete_appointment`,
            data: {
              appointment: assign(
                {},
                {
                  type: 'appointment',
                  reference: ctrl.value(),
                  instance: null,
                }
              ),
            },
          }).then(() => {
            ctrl.onRemove();
          }),
          catch: () => APPOINTMENT_REMOVE_ERROR,
        })
        .catch(() => {
          loading = false;
        });
    };
  }

  getResponseObject(data) {
    if (
      Array.isArray(data) &&
      isPlainObject(data[0]) &&
      isPlainObject(data[0].instance)
    ) {
      return data[0];
    }

    return null;
  }

  appointmentNotFound(type, subType) {
    return type === EXCEPTION && subType === NOT_FOUND;
  }
}
