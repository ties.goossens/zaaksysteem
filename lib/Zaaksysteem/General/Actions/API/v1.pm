package Zaaksysteem::General::Actions::API::v1;
use Zaaksysteem::Moose;

use Hash::Flatten qw(unflatten);
use Zaaksysteem::Search::Object;
use Zaaksysteem::API::v1::ResultSet;
use Zaaksysteem::API::v1::Set;
use Zaaksysteem::Constants qw(BASE_RELATION_ROLES);
use Zaaksysteem::Search::ESQuery;
use Zaaksysteem::Search::ZQL;
use Zaaksysteem::Tools::EsQuery qw(parse_es_query);
use Zaaksysteem::Types qw(UUID);

=head1 NAME

Zaaksysteem::General::Actions::API::v1 - API v1 action calls

=head1 SYNOPSIS

=head1 METHODS

=head2 has_client_type

Returns true if the current request has the C<X-Client-Type> header.

=cut

sub has_client_type {
    my $c = shift;
    if(my $type = $c->req->header('X-Client-Type')) {
        $c->log->debug("Client reported X-Client-Type of '$type'");
        return 1;
    }
    return 0;

}

=head2 parse_es_query_params

Parse ES Query parameters to a query data structure.

C<query:match:foo=bar> will be parsed to an datastrucure like
C<{query => { match => { foo => bar }}}>

=cut

sub parse_es_query_params {
    my $self = shift;
    return parse_es_query($self->req->params);
}

=head2 assert_uuid

Assert if the given ID is in fact a UUID

=cut

sub assert_uuid {
    my ($c, $uuid) = @_;

    if (UUID->check($uuid)) {
        return 1;
    }
    throw('general/uuid/invalid', "Invalid UUID for '$uuid'");
}

=head2 parse_search_query

Parse an ZQL and/or ESQuery

=cut

=head2 get_base_rs

Get the base resultset from an API/v1 search

=cut

define_profile get_base_rs => (
    optional => {
        interface => 'Zaaksysteem::Model::DB::Interface',
        model     => 'Defined',
        base_rs   => 'Defined',
    }
);

sub get_base_rs {
    my $self = shift;
    my $opts = assert_profile({@_})->valid;

    my $model       = $opts->{model}   // $self->model('Object');
    my $base_rs     = $opts->{base_rs} // $model->rs;

    my $interface   = $opts->{interface};
    return $base_rs unless $interface;

    my $query = $self->get_query_constraint_from_interface($interface, $model);
    return $query ? $query->zql->apply_to_resultset($base_rs) : $base_rs;
}

sub get_query_constraint_from_interface {
    my ($self, $interface, $model) = @_;

    my $query_constraint = eval { $interface->jpath('$.query_constraint') };
    return unless $query_constraint;

    $model //= $self->model('Object');

    # Ignore user permissions for finding the query constraint
    my $object_data_saved_search_row = $model->new_resultset->find(
        $query_constraint->{ id }
    );

    unless (defined $object_data_saved_search_row) {
        throw('general/search/query_constraint_not_found', sprintf(
            'This API is configured with a constraint query, but the query object could not be found'
        ));
    }

    return try {
        return $model->inflate_from_row($object_data_saved_search_row);
    } catch {
        throw('api/v1/constraint_query/inflation_failed', sprintf(
            'Inflation of saved search "%s" (id %s) failed: %s',
            $query_constraint->{ label },
            $query_constraint->{ id },
            $_
        ));
    };

}

=head2 parse_search_query

Parse the search query and apply it on the resultset

=cut

define_profile parse_search_query => (
    required => { object_type => 'Str', },
    optional => {
        model     => 'Defined',
        base_rs   => 'Defined',
        interface => 'Zaaksysteem::Model::DB::Interface',
    }
);

sub parse_search_query {
    my ($self, %opts) = @_;

    my $model       = $opts{model} // $self->model('Object');
    my $object_type = $opts{object_type};
    my $interface   = $opts{interface};

    my $base_rs = $self->get_base_rs(
        base_rs => $opts{base_rs} // $model->rs,
        model   => $model,
        $interface ? (interface => $interface) : (),
    );

    my $rs;
    if ($self->req->params->{es_query}) {
        my $search = Zaaksysteem::Search::ESQuery->new(
            query       => $self->parse_es_query_params,
            object_type => $object_type,
        );
        $rs = $search->apply_to_resultset($base_rs);
    }
    elsif ($self->req->params->{zql}) {

        my $model = Zaaksysteem::Search::Object->new(
            user         => $self->user,
            schema       => $self->model("DB")->schema,
            object_model => $self->model('Object'),
            query        => $self->req->params->{zql},
            base_rs      => $base_rs,
        );

        unless ($model->check_zql_command_for($object_type)) {
            throw('general/search/zql/query_fault', sprintf(
                "Cannot parse ZQL, only objecttype '$object_type' is allowed"
            ));
        }
        $rs = $model->search_intake;
    }
    else {
        $rs = $base_rs->search_rs({object_class => $object_type});
    }

    return Zaaksysteem::Object::Iterator->new(
        rs       => $rs,
        inflator => sub { $model->inflate_from_row(shift) },
    );
}

sig iterator_to_api_v1_response => 'Zaaksysteem::Object::Iterator';

sub iterator_to_api_v1_response {
    my ($self, $iterator) = @_;

    my $set = Zaaksysteem::API::v1::Set->new(
        iterator => $iterator,
    )->init_paging($self->req);

    return Zaaksysteem::API::v1::ResultSet->new(
          iterator => $set->build_iterator->rs
    )->init_paging($self->req);
}

sub get_user_roles {
    my $self = shift;

    my @roles;
    for my $role (@{ BASE_RELATION_ROLES() }) {
        push @roles, Zaaksysteem::Object::Types::Subject::Role->new(
            label   => $role,
            is_builtin => 1,
        );
    }

    for my $role (@{ $self->model('DB::Config')->get_value('custom_relation_roles') || [] }) {
        push @roles, Zaaksysteem::Object::Types::Subject::Role->new(
            label   => $role,
            is_builtin => 0,
        );
    }
    return \@roles;
}
1;

__END__

=head1 SEE ALSO

L<Zaaksysteem::General>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
