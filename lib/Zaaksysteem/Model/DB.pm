package Zaaksysteem::Model::DB;

use Moose;
use Scalar::Util qw();

use Zaaksysteem::Zaken::DelayedTouch;


extends 'Catalyst::Model::DBIC::Schema';

with 'Catalyst::Component::InstancePerContext';

__PACKAGE__->config(
    schema_class => 'Zaaksysteem::Schema',
);

=head2 build_per_context_instance

See L<Catalyst::Component::InstancePerContext>.

=cut

sub build_per_context_instance {
    my $self    = shift;
    my $c       = shift;

    my $new = (ref($self) ? $self->new(%$self) : $self->new);

    my $customer_instance = $c->customer_instance;

    my $ro;
    # Database connection per request:
    unless ($c->stash->{__dbh}) {

        $ro = $c->is_db_read_only_action;
        my $config;
        if ($ro && $customer_instance->{'Model::DB'}{slave}) {
            $config = $customer_instance->{'Model::DB'}{slave};
        }
        else {
            $ro = 0;
            $config = $customer_instance->{'Model::DB'}{connect_info};
        }

        $c->{stash}->{__dbh} = $self->_connect_db($config, $ro);

        if (!$ro && $c->can_db_replicated && $customer_instance->{'Model::DB'}{slave}) {
            my $config = $customer_instance->{'Model::DB'}{slave};

            my $storage = $c->stash->{__dbh}->storage;
            $storage->connect_replicants($config);
        }

        $new->schema->default_resultset_attributes->{delayed_touch}
            = Zaaksysteem::Zaken::DelayedTouch->new;
    }

    $new->schema($c->stash->{__dbh});

    $new->schema->default_resultset_attributes->{ log } = $c->log;
    $new->schema->default_resultset_attributes->{ config } = $c->config;
    $new->schema->default_resultset_attributes->{ cache } = $c->zs_cache;
    $new->schema->default_resultset_attributes->{ queue_items } = [];
    $new->schema->default_resultset_attributes->{ events_to_publish } = [];

    ### Accessors on schema object
    $new->schema->catalyst_config($c->config);
    $new->schema->customer_instance($customer_instance);
    $new->schema->cache($c->zs_cache);

    my $betrokkene_stash    = {};
    $betrokkene_stash->{$_} = $c->stash->{$_} for qw/
        order
        order_direction
        paging_rows
        paging_page
        paging_total
        paging_lastpage
        paging_total
        paging_lastpage
    /;

    $new->schema->betrokkene_pager($betrokkene_stash);

    foreach my $attribute (qw/log config cache/) {
        if(!Scalar::Util::isweak($new->schema->default_resultset_attributes->{$attribute})) {
            Scalar::Util::weaken($new->schema->default_resultset_attributes->{$attribute});
        }
    }

    return $new;
}

sub _connect_db {
    my ($self, $connect_info, $ro) = @_;

    my $t0 = Zaaksysteem::StatsD->statsd->start;
    my $schema = $self->schema->connect(
        {
            %{$connect_info},
            auto_savepoint => 1,
        }
    );
    my $statsd = $ro ? 'database_ro.connect.time' : 'database.connect.time';
    Zaaksysteem::StatsD->statsd->end($statsd, $t0);
    $schema->read_only($ro) if $ro;
    return $schema;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2016, 2019 Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
