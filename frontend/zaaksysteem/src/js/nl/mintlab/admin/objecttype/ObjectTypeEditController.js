// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular
    .module('Zaaksysteem.admin.objecttype')
    .controller('nl.mintlab.admin.objecttype.ObjectTypeEditController', [
      '$scope',
      '$http',
      '$window',
      '$location',
      'formService',
      'translationService',
      'safeApply',
      function (
        $scope,
        $http,
        $window,
        $location,
        formService,
        translationService,
        safeApply
      ) {
        var advancedTo,
          metadataLoaded = false;

        function setPhase(phase, options) {
          var phaseObj = _.find($scope.phases, { id: phase });

          options = _.defaults(
            {
              setHash: true,
            },
            options
          );

          $scope.activePhase = phaseObj;

          if (phaseObj && options.setHash) {
            $window.location.hash = '#/' + phase;
          }

          if (
            _.findIndex($scope.phases, { id: phase }) >
            _.findIndex($scope.phases, { id: advancedTo })
          ) {
            advancedTo = phase;
          }
        }

        function setItemValues(values) {
          _.each(values, function (value, key) {
            if (
              key === 'modified_sections' ||
              key === 'modification_rationale'
            ) {
              return;
            }
            _.every($scope.phases, function (phase) {
              return _.every(phase.form.fields, function (field) {
                if (field.name === key) {
                  field.value = value;
                  return false;
                }
                return true;
              });
            });
          });
        }

        $scope.isNewObject = function () {
          return !$scope.objectTypeId || $scope.objectTypeId === '0';
        };

        $scope.handlePhaseSelect = function ($value) {
          $window.location.hash = '#/' + $value;
        };

        $scope.getForm = function (phaseId) {
          var phase = _.find($scope.phases, { id: phaseId });
          return phase ? phase.form : null;
        };

        $scope.prevPhase = function () {
          var prevIndex = _.indexOf($scope.phases, $scope.activePhase) - 1,
            prevPhase = $scope.phases[prevIndex];

          if (prevPhase) {
            setPhase(prevPhase.id);
          }
        };

        $scope.nextPhase = function () {
          var nextIndex = _.indexOf($scope.phases, $scope.activePhase) + 1,
            nextPhase = $scope.phases[nextIndex];

          if (nextPhase) {
            setPhase(nextPhase.id);
          }
        };

        $scope.isLastPhase = function () {
          var index = _.indexOf($scope.phases, $scope.activePhase);
          return $scope.phases && $scope.phases.length === index + 1;
        };

        $scope.isFirstPhase = function () {
          return _.indexOf($scope.phases, $scope.activePhase) === 0;
        };

        $scope.isActiveFormValid = function () {
          var phaseId = $scope.activePhase ? $scope.activePhase.id : '';
          return $scope.isFormValid(phaseId);
        };

        $scope.isFormValid = function (phaseId) {
          var form = formService.get('form_' + phaseId);
          return form && form.isFormValid();
        };

        $scope.isAvailable = function (phaseId) {
          var isNewObject = $scope.isNewObject(),
            hasPassed =
              _.findIndex($scope.phases, { id: phaseId }) <=
              _.findIndex($scope.phases, { id: advancedTo });

          return !isNewObject || hasPassed;
        };

        $scope.isAllFormsValid = function () {
          return _.every($scope.phases, function (phase) {
            return $scope.isFormValid(phase.id);
          });
        };

        $scope.handleFinishClick = function () {
          if ($scope.isLastPhase()) {
            $scope.finishEditing();
          } else {
            $scope.toLastPhase();
          }
        };

        $scope.toLastPhase = function () {
          var lastPhase = $scope.phases
            ? $scope.phases[$scope.phases.length - 1]
            : null;
          if (lastPhase) {
            setPhase(lastPhase.id);
          }
        };

        $scope.getObjectValues = function () {
          var values = {
              object_class: 'type',
              category_id: $scope.categoryId,
            },
            forms;

          forms = _.map($scope.phases, function (phase) {
            return formService.get('form_' + phase.id);
          });

          _.each(forms, function (form) {
            var vals = form.getValues();
            values = _.merge(values, vals);
          });

          // remove $$hashKey values
          values = angular.copy(values);

          /**
           * values.id = $scope.objectTypeId || undefined
           * Above fails, because objectTypeId is true-ish when value
           * is stringified "0"
           *
           * Test using non-strict false comparison so type distinction
           * does not matter explicitly.
           */
          if ($scope.objectTypeId == false) {
            values.id = undefined;
          } else {
            values.id = $scope.objectTypeId;
          }

          delete values.objecttype_name_display;

          if (!values.related_objecttypes) {
            values.related_objecttypes = [];
          }

          return values;
        };

        $scope.isActivePhase = function (phaseId) {
          var active = false;

          if ($scope.activePhase && $scope.activePhase.id === phaseId) {
            active = true;
          }

          return active;
        };

        $scope.finishEditing = function () {
          var values = $scope.getObjectValues(),
            isNew = $scope.isNewObject();

          $scope.submitting = true;

          $http({
            method: 'POST',
            url: '/api/object/save/',
            data: {
              object: values,
            },
          })
            .success(function (response) {
              var obj = response.result[0];

              $scope.objectTypeId = obj.id;

              $scope.$emit('systemMessage', {
                type: 'info',
                content: translationService.get(
                  isNew ? 'Objecttype aangemaakt' : 'Wijzigingen opgeslagen'
                ),
              });

              $window.location = '/admin/catalogus';
            })
            .error(function (/*response*/) {
              $scope.$emit('systemMessage', {
                type: 'error',
                content: translationService.get(
                  'Er ging iets fout bij het opslaan van de wijzigingen. Probeer het later opnieuw.'
                ),
              });
            })
            ['finally'](function () {
              $scope.submitting = false;
            });
        };

        $scope.init = function () {
          if (!$scope.isNewObject()) {
            $http({
              method: 'GET',
              url: '/api/object/' + $scope.objectTypeId,
            })
              .success(function (response) {
                var item = response.result[0];
                setItemValues(item.values);
                metadataLoaded = true;
              })
              .error(function (/*response*/) {
                $scope.$emit('systemMessage', {
                  type: 'error',
                  content: translationService.get('Object niet gevonden'),
                });
              });
          }
        };

        $scope.isMetadataLoaded = function () {
          return $scope.isNewObject() || metadataLoaded;
        };

        $scope.getObjectName = function () {
          var firstPhase = $scope.phases[0],
            form,
            name = '';

          if (firstPhase) {
            form = formService.get('form_' + firstPhase.id);
            name = form.getValue('name');
          }

          return name;
        };

        function getPhaseFromHash() {
          var match = $window.location.hash.match(/^#\/?(\w+)$/),
            phaseId = match ? match[1] : null;

          if ($scope.isAvailable(phaseId)) {
            safeApply($scope, function () {
              setPhase(phaseId, {
                setHash: false,
              });
            });
          }
        }

        $scope.$on(
          'zs.scope.data.apply',
          function (/*event, parentScope, data*/) {
            var firstPhase = $scope.phases[0];

            if (firstPhase) {
              advancedTo = firstPhase.id;
              getPhaseFromHash();
              if (!$scope.activePhase) {
                setPhase(firstPhase.id);
              }
            }
          }
        );

        angular.element($window).bind('hashchange', function () {
          getPhaseFromHash();
        });
      },
    ]);
})();
