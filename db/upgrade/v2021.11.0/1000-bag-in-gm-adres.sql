BEGIN;

  ALTER TABLE gm_adres ADD COLUMN bag_id text;
  ALTER TABLE gm_adres ADD COLUMN geo_lat_long point;

COMMIT;
