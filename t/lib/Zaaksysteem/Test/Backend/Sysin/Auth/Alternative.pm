package Zaaksysteem::Test::Backend::Sysin::Auth::Alternative;

=head1 NAME

Zaaksysteem::Test::Backend::Sysin::Auth::Alternative - Test OverheidIO model

=head2 SYNOPSIS

    prove -l :: Zaaksysteem::Test::Backend::Sysin::Auth::Alternative

=cut

use Zaaksysteem::Backend::Sysin::Auth::Alternative;
use Zaaksysteem::Test;

sub test_setup {
    my $test = shift;

    my $test_method = $test->test_report->current_method->name;

    if ('test_alt_auth_live' eq $test_method
        && !$ENV{ZS_ALT_AUTH_API_KEY})
    {
        $test->test_skip("Skipping $test_method: set ZS_ALT_AUTH_API_KEY");
    }
}

sub _create_alt_auth_ok {
    my $options = {@_};
    my $model   = Zaaksysteem::Backend::Sysin::Auth::Alternative->new(
        schema       => mock_dbix_schema,
        sms_endpoint => $options->{sms_endpoint}
            // 'https://sendsms.zaaksysteem.nl/api/sms',
        sms_product_token => $options->{sms_product_token} // 'zs-testsuite',
        sms_sender        => $options->{sms_sender} // 'zs-testsuite',
        interface         => mock_interface,
    );
    isa_ok($model, 'Zaaksysteem::Backend::Sysin::Auth::Alternative');
    return $model;
}

sub test_assert_password {

    my $model = _create_alt_auth_ok();

    throws_ok(
        sub {
            $model->assert_password(
                email          => 'foo@bar.nl',
                password       => 'foo',
                password_check => 'foo'
            );
        },
        qr#auth/alternative/password/username#,
        "password matches beginning of username"
    );

    throws_ok(
        sub {
            $model->assert_password(
                email          => 'foo@bar.nl',
                password       => 'bar',
                password_check => 'bar'
            );
        },
        qr#auth/alternative/password/username#,
        "password matches domain of username"
    );

    throws_ok(
        sub {
            $model->assert_password(
                email          => 'foo@da.bar.nl',
                password       => 'dad',
                password_check => 'dad'
            );
        },
        qr#auth/alternative/password/username#,
        "password matches domain of username"
    );

    throws_ok(
        sub {
            $model->assert_password(
                email          => 'foo@bar.nl',
                password       => 'nope',
                password_check => 'nopeddd'
            );
        },
        qr#^auth/alternative/password/invalid#,
        "Passwords do not match"
    );

    lives_ok(
        sub {
            $model->assert_password(
                email          => 'foo@bar.nl',
                password       => 'nope',
                password_check => 'nope'
            );
        },
        "Password asserts ok"
    );
}

sub test_register_account {
    my $model = _create_alt_auth_ok();

    my $override = override(
        "Zaaksysteem::Backend::Sysin::Auth::Alternative::assert_username" =>
            sub {
            return 1;
        }
    );

    $override->override(
        "Zaaksysteem::Backend::Sysin::Auth::Alternative::_assert_unique_account"
            => sub { return 1 }
    );

    lives_ok(
        sub {
            $model->register_account(
                email          => 'foo@example.com',
                password       => 'nocando',
                password_check => 'nocando',
                phone          => '+31612345678',
                subject_type   => 'person',
            );
        },
        "Create a NP account without BSN"
    );
}

sub test_create_sms_xml {

    my $oio = _create_alt_auth_ok();
    my $xml = $oio->create_sms_xml(
        phone_number => '0610094296',
        message      => 'This is the testsuite',
        reference    => 'Reference',
    );

    ok($xml, "create_sms_xml");

}

sub test_send_sms {
    my $oio = _create_alt_auth_ok();

    my $output;
    my $input;
    my $record = mock_transaction_record(
        output => sub { $output = shift },
        input => sub { $input = shift },
    );
    my $xml    = "<foo>bar</foo>";

    my %args = (
        xml    => $xml,
        record => $record,
    );

    my $record_input = sprintf("POST %s\nContent-Type: application/xml\n\n%s\n", $oio->sms_endpoint, $xml);

    {
        use HTTP::Response;
        my $answer = "Send sms";
        my $override = override("LWP::UserAgent::request" => sub {
            my $self = shift;
            return HTTP::Response->new(200, undef, undef, $answer);
        });

        ok($oio->send_sms(%args), "send_sms with 200");

        is($output, "200 OK\n\n$answer\n", "We have output in our transaction record");
        is($input, $record_input, "We have input in our transaction record");

    }

    {
        use HTTP::Response;
        my $answer = "This is failure";
        my $override = override("LWP::UserAgent::request" => sub {
            my $self = shift;
            return HTTP::Response->new(403, undef, undef, $answer);
        });

        throws_ok(
            sub {
                $oio->send_sms(%args);
            },
            qr#^twofactor/sms_sending_failed: \d+#,
            "Failure with sending SMS",
        );

        is($output, "403 Forbidden\n\n$answer\n", "We have output in our transaction record when failing");
        is($input, $record_input, "We have input in our transaction record when failing");
    }

}

sub test_sms_token_generation {

    my $oio = _create_alt_auth_ok();

    my $token = $oio->create_auth_challenge();
    is(length($token), 8, "We have 8 chars");
    ok(int($token), "... and consists out of integers");

}

1;

__END__

=head1 NAME

Zaaksysteem::Test::Backend::Sysin::Auth::Alternative - A OverheidIO test package

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
