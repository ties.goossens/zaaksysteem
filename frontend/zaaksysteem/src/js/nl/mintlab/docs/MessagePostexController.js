// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
angular
  .module('Zaaksysteem.docs')
  .controller('nl.mintlab.docs.MessagePostexController', [
    '$scope',
    '$rootScope',
    '$q',
    '$http',
    'smartHttp',
    'translationService',
    'systemMessageService',
    'snackbarService',
    'messagePostexService',
    function (
      $scope,
      $rootScope,
      $q,
      $http,
      smartHttp,
      translationService,
      systemMessageService,
      snackbarService,
      messagePostexService
    ) {
      var loading = false;
      let postexRecipientTypes;
      let availableSubjectRoles;

      /* Copy/pasta from https://www.npmjs.com/package/@opndev/util
       * MIT license, Wesley Schwengle <wesley@opndev.io>
       * See https://gitlab.com/opndev/opndev-js-util/-/blob/master/lib/defined.js
       */
      function defined(what) {
        if (what === null) return false;

        if (typeof what === 'undefined') return false;

        /* This will probably never be hit:
         * https://stackoverflow.com/questions/4725603/variable-undefined-vs-typeof-variable-undefined
         */
        if (what === undefined) return false;

        return true;
      }
      /* End of MIT license stuff */

      function getRolesFromAPI() {
        return $http.get('/api/v1/subject/role').then(function (response) {
          return response.data.result.instance.rows.map(function (item) {
            var label = item.instance.label;

            return {
              label: label,
              value: label,
            };
          });
        });
      }

      function hasOnlyAllowedFiles(files) {
        return !Boolean(
          _.find(files, function (file, i) {
            return file.extension_dotless != 'pdf';
          })
        );
      }

      $scope.init = function () {
        postexRecipientTypes = [
          {
            value: 'requestor',
            label: 'Aanvrager',
          },
          {
            value: 'authorized',
            label: 'Gemachtigde',
          },
          {
            value: 'betrokkene',
            label: 'Betrokkene',
          },
          {
            value: 'ocr',
            label: 'OCR (gegevens uit bestand)',
          },
        ];

        if (!$scope.typeRecipient)
          $scope.typeRecipient = postexRecipientTypes[0].value;

        getRolesFromAPI().then(function (data) {
          availableSubjectRoles = data;
          if (!$scope.roleRecipient)
            $scope.roleRecipient = availableSubjectRoles[0].value;
        });
      };

      $scope.maySubmit = function () {
        if ($scope.loading) return false;

        if (!hasOnlyAllowedFiles($scope.attachments)) return false;

        const recipientType = $scope.typeRecipient;
        const employeeType = 'medewerker';

        /* Easy check, employees are never authorized. Roles are a
         * different beast but we need more information from the case.
         * We cannot see who are authorized as well. Additional calls
         * maybe?
         *
         * First make it work, than make a propper check.
         */
        if (
          recipientType === 'requestor' &&
          $scope.requestor.subject_type === employeeType
        )
          return false;

        return true;
      };

      $scope.handleTypeRecipientChange = function () {
        $scope.recipientAddress = '';
      };

      $scope.getRoles = function () {
        return availableSubjectRoles;
      };

      $scope.getRecipients = function () {
        return postexRecipientTypes;
      };

      $scope.submit = function () {
        $scope.loading = true;

        snackbarService
          .wait('Bezig met versturen...', {
            promise: messagePostexService.submit(
              $scope.postexInterfaceUUID,
              $scope.requestor.uuid,
              {
                sender_uuid: $scope.subjectUUID,

                recipient_type: $scope.typeRecipient,
                recipient_role:
                  $scope.typeRecipient === 'betrokkene'
                    ? $scope.roleRecipient
                    : null,

                case_id: $scope.case.instance.number,

                subject: $scope.subject,
                body: $scope.content,

                file_attachments: _.map($scope.attachments, function (file, i) {
                  return file.id;
                }),
              }
            ),
            then: function (response) {
              return 'Het bericht is afgeleverd bij Postex.';
            },
            catch: function (error) {
              if (_.get(error, 'data.result.instance.message')) {
                return _.get(error, 'data.result.instance.message');
              } else if (_.get(error, 'data.result.preview')) {
                return _.get(error, 'data.result.preview');
              } else if (typeof error === 'string') {
                return error;
              } else {
                return 'Er is iets fout gegaan bij het versturen van het bericht. Neem contact op met de beheerder';
              }
            },
            finally: function () {
              $scope.loading = false;
            },
          })
          .then(function () {
            $scope.closePopup();
          })
          .finally(function () {
            $scope.loading = false;
          });
      };
    },
  ]);
