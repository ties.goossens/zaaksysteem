package Zaaksysteem::Zaken::Roles::DocumentenObjecten;

use BTTW::Tools;
use Clone qw/clone/;
use Data::Dumper;
use Moose::Role;

requires qw(
    log
);

#
# new sleeker version. avoid excessive rule execution
#
sub is_documents_complete {
    my ($self) = @_;

    my $statusses = $self->get_rule_statusses;

    my $required_documents = $self->zaaktype_node_id->zaaktype_kenmerken({
        'zaak_status_id'                         => { '-in' => $statusses->get_column('id')->as_query },
        'bibliotheek_kenmerken_id.value_type'    => 'file',
        'value_mandatory'                        => 1,
    },
    {
        'join'      => 'bibliotheek_kenmerken_id',
        'prefetch'  => 'zaak_status_id'
    });

    my $given_kenmerken ||= $self->field_values();

    my $rules_result;

    my $required_fields_per_phase = {};
    my $required_fields = $self->visible_fields({
        mandatory       => 1,
        field_values    => clone($given_kenmerken),
        result          => $rules_result,
        include_documents => 1,
    });

    while (my $required_document = $required_documents->next) {
        my $status                  = $required_document->zaak_status_id->status;
        my $bibliotheek_kenmerken_id = $required_document->get_column('bibliotheek_kenmerken_id');

        if (!$required_fields_per_phase->{$status}) {
            $required_fields_per_phase->{$status} = $self->visible_fields({
                mandatory       => 1,
                field_values    => clone($given_kenmerken),
                result          => $rules_result,
                include_documents => 1,
                phase           => $required_document->zaak_status_id
            });
        }

        next unless $required_fields_per_phase->{$status}->{$bibliotheek_kenmerken_id};

        my $documents = $self->file_field_documents($bibliotheek_kenmerken_id);

        unless (@$documents) {
            $self->log->info(sprintf(
                "Missing document; magic_string=%s, zaaktype-kenmerken_id=%d",
                $required_document->bibliotheek_kenmerken_id->magic_string,
                $required_document->id,
            ));
            return 0;
        }
    }

    return 1;
}



sub is_documenten_compleet {
    my ($self, $args) = @_;

    tombstone('20190412', 'Patrick Spek');

    ### Check if every kenmerk is filled in this fase and the fase before
    my $goto_status = (
        $self->volgende_fase
            ? $self->volgende_fase->status
            : $self->huidige_fase->status
        );

    my $statusses = $self->zaaktype_node_id->zaaktype_statussen->search(
        {
            status  => { '<=' => $goto_status }
        },
        {
            order_by    => { '-asc' => 'id' }
        }
    );

    my $incomplete;
    while (my $status = $statusses->next) {
        unless (
            $self->_check_phase_documents({
                status => $status->status
            })
        ) {
            $incomplete = $status->status
        }
    }

    if ($incomplete) {
        return {
            phase   => $incomplete,
            result  => 0,
        };
    } else {
        return {
            result  => 1,
        };

    }
}



sub _check_phase_documents {
    my ($self, $args) = @_;


    my $fasen   = $self->zaaktype_node_id
        ->zaaktype_statussen
        ->search({
            status => $args->{status},
        });

    my $fase    = $fasen->first or die('Er is geen volgende fase');

    my $documenten  = $fase->zaaktype_kenmerken->search(
            {
                'bibliotheek_kenmerken_id.value_type'   => 'file',
                'me.value_mandatory'                    => 1,
            },
            {
                'join'  => 'bibliotheek_kenmerken_id'
            }
        );

    my $error = 0;

    ### Get list of phase_fields
    my $required_fields;
    {
        my $given_kenmerken ||= $self->field_values({ fase => $fase->status });

        my $kenmerken       = { %{ $given_kenmerken } };

        my $rules_result;
        $required_fields = $self->visible_fields({
            phase           => $fase,
            mandatory       => 1,
            field_values    => $given_kenmerken,
            result          => $rules_result,
            include_documents => 1,
        });
    }


    while (my $document = $documenten->next) {
        next unless (
            defined(
                $required_fields->{ $document->get_column('bibliotheek_kenmerken_id') }
            ) &&
            $required_fields->{ $document->get_column('bibliotheek_kenmerken_id') }
        );

        if (!$self->documents->search({
                'zaaktype_kenmerken_id' => $document->id,
                'deleted_on'            => undef
            })->count
        ) {
            $error = 1;
        }
    }

    return 1 unless $error;
    return 0;
}

sub is_document_queue_empty {
    my $self        = shift;

    return $self->files->search(
        {
            accepted     => 0,
            date_deleted => undef,
            destroyed    => { '!=' => 1 }
        }
    )->count ? 0 : 1;
}


around can_volgende_fase => sub {
    my $orig    = shift;
    my $self    = shift;

    my $advance_result = $self->$orig(@_);

    if (not $self->is_documents_complete) {
        $advance_result->fail('documents_complete', 'Missing documents');
    } elsif (not $self->is_document_queue_empty) {
        $advance_result->fail('documents_complete', 'Documents in queue');
    } else {
        $advance_result->ok('documents_complete');
    }

    return $advance_result;
};


sub _publishable_fields {
    my ($self) = @_;

    return $self->zaaktype_id->zaaktype_node_id->zaaktype_kenmerkens->search({
        'publish_public' => 1,
        'bibliotheek_kenmerken_id.value_type' => 'file',
    }, {
        join => ['bibliotheek_kenmerken_id'],
    });
}


sub publishable_documents {
    my ($self) = @_;

    my $documents = $self->documents->search({
        zaaktype_kenmerken_id => {
            '-in' => $self->_publishable_fields->get_column('id')->as_query
        }
    }, {
        order_by => 'id',
    });

    return $documents;
}

sub non_publishable_documents {
    my ($self) = @_;

    my $documents = $self->documents->search({
        zaaktype_kenmerken_id => {
            '-not_in' => $self->_publishable_fields->get_column('id')->as_query
        }
    }, {
        order_by => 'id',
    });

    return $documents;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 is_document_queue_empty

TODO: Fix the POD

=cut

=head2 is_documenten_compleet

TODO: Fix the POD

=cut

=head2 is_documents_complete

TODO: Fix the POD

=cut

=head2 non_publishable_documents

TODO: Fix the POD

=cut

=head2 publishable_documents

TODO: Fix the POD

=cut

