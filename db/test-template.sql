--
-- PostgreSQL database dump
--

-- Dumped from database version 12.8 (Debian 12.8-1.pgdg100+1)
-- Dumped by pg_dump version 12.8 (Debian 12.8-1.pgdg100+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: hstore; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS hstore WITH SCHEMA public;


--
-- Name: EXTENSION hstore; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION hstore IS 'data type for storing sets of (key, value) pairs';


--
-- Name: pg_trgm; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS pg_trgm WITH SCHEMA public;


--
-- Name: EXTENSION pg_trgm; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION pg_trgm IS 'text similarity measurement and index searching based on trigrams';


--
-- Name: uuid-ossp; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS "uuid-ossp" WITH SCHEMA public;


--
-- Name: EXTENSION "uuid-ossp"; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION "uuid-ossp" IS 'generate universally unique identifiers (UUIDs)';


--
-- Name: confidentiality; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.confidentiality AS ENUM (
    'public',
    'internal',
    'confidential'
);


--
-- Name: contactmoment_medium; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.contactmoment_medium AS ENUM (
    'behandelaar',
    'balie',
    'telefoon',
    'post',
    'email',
    'webformulier'
);


--
-- Name: contactmoment_type; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.contactmoment_type AS ENUM (
    'email',
    'note'
);


--
-- Name: custom_object_type_status; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.custom_object_type_status AS ENUM (
    'active',
    'inactive'
);


--
-- Name: custom_object_version_content_archive_status; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.custom_object_version_content_archive_status AS ENUM (
    'archived',
    'to destroy',
    'to preserve'
);


--
-- Name: custom_object_version_status; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.custom_object_version_status AS ENUM (
    'active',
    'inactive',
    'draft'
);


--
-- Name: documentstatus; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.documentstatus AS ENUM (
    'original',
    'copy',
    'replaced',
    'converted'
);


--
-- Name: preferred_contact_channel; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.preferred_contact_channel AS ENUM (
    'email',
    'mail',
    'pip',
    'phone'
);


--
-- Name: zaaksysteem_bag_types; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.zaaksysteem_bag_types AS ENUM (
    'nummeraanduiding',
    'verblijfsobject',
    'pand',
    'openbareruimte'
);


--
-- Name: zaaksysteem_status; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.zaaksysteem_status AS ENUM (
    'new',
    'open',
    'resolved',
    'stalled',
    'deleted',
    'overdragen'
);


--
-- Name: zaaksysteem_trigger; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.zaaksysteem_trigger AS ENUM (
    'extern',
    'intern'
);


--
-- Name: appointment_attribute_value_to_jsonb(text, uuid); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.appointment_attribute_value_to_jsonb(value text, reference uuid, OUT value_json jsonb) RETURNS jsonb
    LANGUAGE plpgsql
    AS $$
  DECLARE
    appointment jsonb;
    preview text;
    start_time timestamp with time zone;
    end_time timestamp with time zone;
  BEGIN

      IF value = '{}'
      THEN
        RETURN;
      END IF;

      appointment := (value)::jsonb->'values';

      start_time := (appointment->'start_time'->'value'->>'__DateTime__')::timestamp with time zone;
      end_time := (appointment->'end_time'->'value'->>'__DateTime__')::timestamp with time zone;

      preview := concat(
        to_char(timezone('Europe/Amsterdam', start_time), 'YYYY-MM-DD HH:MI'),
        ' - ',
        to_char(timezone('Europe/Amsterdam', end_time), 'YYYY-MM-DD HH:MI')
      );

      SELECT INTO value_json jsonb_build_object(
        'type', 'appointment',
        'reference', reference,
        'preview', preview,
        'instance', jsonb_build_object(
          'date_created', (appointment->'date_created'->'value'->>'__DateTime__')::timestamp with time zone,
          'date_modified', (appointment->'date_modified'->'value'->>'__DateTime__')::timestamp with time zone,
          'start_time', start_time,
          'end_time', end_time,
          'plugin_type', appointment->'plugin_type'->>'value',
          'plugin_data', appointment->'plugin_data'->'value'
        )
      );
      SELECT INTO value_json to_jsonb(ARRAY[value_json]);
      RETURN;
  END;
  $$;


--
-- Name: attribute_date_value_to_text(text); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.attribute_date_value_to_text(value text, OUT datestamp text) RETURNS text
    LANGUAGE plpgsql
    AS $$
  DECLARE
    dd text;
    mm text;
    yy text;
    dt timestamp;
  BEGIN
    dd := split_part(value, '-', 1);
    mm := split_part(value, '-', 2);
    yy := split_part(value, '-', 3);

    -- We have attributes which are switched around :((
    IF length(yy) = 2 AND length(dd) = 4
    THEN
      -- 2019-10-21
      dt := make_date(dd::int, mm::int, yy::int);
    ELSIF length(yy) > 2 and length(dd) = 4
    THEN
      -- and we have actual timestamps
      dt := value::date;
    ELSE
      -- 25-10-2019
      dt := make_date(yy::int, mm::int, dd::int);
    END IF;

    SELECT INTO datestamp timestamp_to_perl_datetime(dt::timestamp with time zone at time zone 'Europe/Amsterdam');
  EXCEPTION WHEN OTHERS THEN
    RETURN;

  END;
  $$;


--
-- Name: attribute_file_value_to_v0_jsonb(text[]); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.attribute_file_value_to_v0_jsonb(value text[], OUT value_json jsonb) RETURNS jsonb
    LANGUAGE plpgsql
    AS $$
  DECLARE
    length int;
    m text;

    r record;

  BEGIN

    value_json := '[]'::jsonb;

    FOR r IN
      SELECT
        f.accepted as accepted,
        f.confidential as confidential,
        CONCAT(f.name, f.extension) as filename,
        f.id as id,
        fs.uuid as uuid,
        fs.size as filesize,
        fs.original_name as original_name,
        fs.mimetype as mimetype,
        fs.md5 as md5,
        fs.is_archivable as is_archivable
      FROM
        filestore fs
      JOIN
        file f
      ON
        (f.filestore_id = fs.id)
      WHERE
        fs.uuid = ANY(value::uuid[])
      ORDER by f.id
    LOOP

      select into value_json value_json || jsonb_build_object(
        'accepted', CASE WHEN r.accepted = true THEN 1 ELSE 0 END,
        'confidential', r.confidential,
        'file_id', r.id,
        'filename', r.filename,
        'is_archivable', CASE WHEN r.is_archivable = true THEN 1 ELSE 0 END,
        'md5', r.md5,
        'mimetype', r.mimetype,
        'original_name', r.original_name,
        'size', r.filesize,
        'thumbnail_uuid', null,
        'uuid', r.uuid
      );
    END LOOP;

    RETURN;


  END;
  $$;


--
-- Name: attribute_value_to_jsonb(text[], text); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.attribute_value_to_jsonb(value text[], value_type text, OUT value_json jsonb) RETURNS jsonb
    LANGUAGE plpgsql
    AS $$
  DECLARE
    length int;
    m text;

  BEGIN

      value_json := '[]'::jsonb;
      SELECT INTO length cardinality(COALESCE(value, '{}'::text[]));

      IF length = 0 and value_type = 'file'
      THEN
        value_json := '[]'::jsonb;
        RETURN;
      ELSIF length = 0
      THEN
        value_json := '[null]'::jsonb;
        RETURN;
      END IF;

      IF value_type = 'date'
      THEN
        SELECT INTO value array_agg(attribute_date_value_to_text(value[1]));
      END IF;

      IF value_type LIKE 'bag_%'
      THEN
        SELECT INTO value_json bag_attribute_value_to_jsonb(value, value_type);
        RETURN;
      END IF;

      IF value_type IN ('geojson', 'address_v2', 'appointment_v2', 'relationship')
      THEN

          FOREACH m IN ARRAY value
          LOOP
            SELECT INTO value_json value_json || jsonb_build_array((CONCAT('[', m::text, ']')::json ->> 0)::json);
          END LOOP;

      ELSIF value_type IN ('checkbox', 'select')
      THEN
        SELECT INTO value_json to_jsonb(ARRAY[value]);
      ELSE
        SELECT INTO value_json to_jsonb(value);
      END IF;
      RETURN;
  END;
  $$;


--
-- Name: attribute_value_to_v0(text[], text, boolean); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.attribute_value_to_v0(value text[], value_type text, value_mvp boolean, OUT value_json jsonb) RETURNS jsonb
    LANGUAGE plpgsql
    AS $$
  DECLARE
    length int;
    m text;

  BEGIN

    value_json := null;
    SELECT INTO length cardinality(COALESCE(value, '{}'::text[]));

    IF length = 0 AND value_type = 'file'
    THEN
      value_json := '[]'::jsonb;
      RETURN;
    ELSIF length = 0
    THEN
      RETURN;
    END IF;

    IF value_type = 'date'
    THEN
      SELECT INTO value_json to_jsonb(attribute_date_value_to_text(value[1]));
      RETURN;
    END IF;


    IF value_type LIKE 'bag_%'
    THEN

      SELECT INTO value_json bag_attribute_value_to_jsonb(value, value_type);
      RETURN;

    END IF;

    IF value_type = 'file'
    THEN

      SELECT INTO value_json attribute_file_value_to_v0_jsonb(value);
      RETURN;

    END IF;


    IF value_type IN ('geojson', 'address_v2', 'appointment_v2', 'relationship')
    THEN

        IF value_mvp = true
        THEN
          value_json := '[]'::jsonb;
          FOREACH m IN ARRAY value
          LOOP
            SELECT INTO value_json value_json || to_jsonb(m::jsonb);
          END LOOP;
        ELSE
          SELECT INTO value_json to_jsonb(value[1]::jsonb);
        END IF;
        RETURN;
    END IF;

    IF value_type IN ('checkbox', 'select')
    THEN
      SELECT INTO value_json to_jsonb(value);
    END IF;


    IF value_type = 'valuta'
    THEN
      value_json := value[1]::numeric;
      RETURN;
    END IF;

    IF value_mvp = true
    THEN
      SELECT INTO value_json to_jsonb(value);
    ELSE
      SELECT INTO value_json to_jsonb(value[1]);
    END IF;


    RETURN;
  END;
  $$;


--
-- Name: bag_attribute_value_to_jsonb(text[], text); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.bag_attribute_value_to_jsonb(value text[], value_type text, OUT value_json jsonb) RETURNS jsonb
    LANGUAGE plpgsql
    AS $$
  DECLARE
    length int;
    val text;

    btype text;
    bid text;

  BEGIN

      value_json := '[]'::jsonb;
      SELECT INTO length cardinality(COALESCE(value, '{}'::text[]));
      IF length = 0
      THEN
        value_json := '[null]'::jsonb;
        RETURN;
      END IF;

      FOREACH val IN ARRAY value
      LOOP

        btype := split_part(val, '-', 1);
        bid   := split_part(val, '-', 2);

        SELECT INTO value_json value_json || bag_type_id_to_json(btype, bid);

      END LOOP;
      RETURN;
  END;
  $$;


--
-- Name: bag_attribute_value_to_jsonb(text[], text, integer); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.bag_attribute_value_to_jsonb(value text[], value_type text, case_id integer, OUT value_json jsonb) RETURNS jsonb
    LANGUAGE plpgsql
    AS $$
  DECLARE
    length int;
    val text;

    btype text;
    bid text;

  BEGIN

      value_json := '[]'::jsonb;
      SELECT INTO length cardinality(COALESCE(value, '{}'::text[]));
      IF length = 0
      THEN
        value_json := '[null]'::jsonb;
        RETURN;
      END IF;

      FOREACH val IN ARRAY value
      LOOP

        btype := split_part(val, '-', 1);
        bid   := split_part(val, '-', 2);

        SELECT INTO value_json value_json || bag_type_id_to_json(btype, bid);

      END LOOP;
      RETURN;
  END;
  $$;


--
-- Name: bag_type_id_to_json(text, text); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.bag_type_id_to_json(btype text, bid text, OUT value_json jsonb) RETURNS jsonb
    LANGUAGE plpgsql
    AS $$
  DECLARE
    val text;

    bag_record jsonb;
    bag_json jsonb;

    display_name text;
    lat_long text;
  BEGIN

    bid := lpad(bid, 16, '0');

    SELECT INTO bag_record bag_data FROM bag_cache bc WHERE
      bc.bag_type = btype and bc.bag_id = bid;

    SELECT INTO lat_long REGEXP_REPLACE(bag_record->>'centroide_ll','POINT\((.*) (.*)\)','\2,\1');

    SELECT INTO bag_json json_build_object(
        'identification', bid
    );

    IF btype = 'nummeraanduiding'
    THEN

      SELECT INTO bag_json bag_json || jsonb_build_object(
        'straat', bag_record->'straatnaam',
        'gps_lat_lon', lat_long,
        'huisnummer', bag_record->'huisnummer',
        'postcode', bag_record->'postcode',
        'huisletter', bag_record->'huisletter',
        'huisnummertoevoeging', bag_record->'huisnummertoevoeging',
        'woonplaats', bag_record->'woonplaatsnaam',

        -- We don't have the data in the DB, if there.. fill them.
        -- This is just here to keep the API the same.
        'end_date', null,
        'start_date', null,
        'under_investigation', null,
        'document_date', null,
        'document_number', null
      );

    ELSIF btype = 'openbareruimte'
    THEN

      SELECT INTO bag_json bag_json || jsonb_build_object(
        'straat', bag_record->'straatnaam',
        'gps_lat_lon', lat_long
      );

    ELSIF btype = 'verblijfsobject'
    THEN

      SELECT INTO bag_json bag_json || jsonb_build_object(
        'surface_area', bag_record->'wfs_data'->'oppervlakte',
        'year_of_construction', bag_record->'wfs_data'->'bouwjaar',
        'gebruiksdoel', bag_record->'wfs_data'->'gebruiksdoel'
      );

    END IF;

    display_name := bag_record->>'weergavenaam';

    select into value_json jsonb_build_object(
      'human_identifier', display_name,
      'bag_id', CONCAT(btype, '-', bid),
      'address_data', bag_json
    );

  RETURN;
  END;
  $$;


--
-- Name: case_location_as_v0_json(public.hstore); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.case_location_as_v0_json(location public.hstore, OUT value_json jsonb) RETURNS jsonb
    LANGUAGE plpgsql
    AS $$
  DECLARE
    btype text;
    bid text;

    types text[];
    key text;
    brecord jsonb;

  BEGIN

    SELECT INTO value_json jsonb_build_object();

    types := ARRAY[
      'ligplaats',
      'nummeraanduiding',
      'openbareruimte',
      'pand',
      'standplaats',
      'verblijfsobject',
      'woonplaats'
    ];

    FOREACH btype in ARRAY types
    LOOP

      IF btype = 'ligplaats'
      THEN
          bid := location->'bag_ligplaats_id';
      ELSIF btype = 'nummeraanduiding'
      THEN
          bid := location->'bag_nummeraanduiding_id';
      ELSIF btype = 'openbareruimte'
      THEN
          bid := location->'bag_openbareruimte_id';
      ELSIF btype = 'pand'
      THEN
          bid := location->'bag_pand_id';
      ELSIF btype = 'verblijfsobject'
      THEN
          bid := location->'bag_verblijfsobject_id';
      ELSIF btype = 'woonplaats'
      THEN
          bid := location->'bag_woonplaats_id';
      ELSIF btype = 'standplaats'
      THEN
          bid := location->'bag_standplaats_id';
      END IF;

      -- we don't store it, so we just fill it here for now
      SELECT INTO value_json value_json || jsonb_build_object(
        CONCAT('case.correspondence_location.', btype), null
      );

      key := CONCAT('case.case_location.', btype);

      IF bid IS NULL
      THEN
        SELECT INTO value_json value_json || jsonb_build_object(
          key, null
        );
        CONTINUE;
      END IF;

      SELECT INTO brecord bag_type_id_to_json(btype, bid);

      SELECT INTO value_json value_json || jsonb_build_object(
        key, brecord
      );

    END LOOP;



  END;
  $$;


--
-- Name: case_subject_as_v0_json(public.hstore, text, boolean); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.case_subject_as_v0_json(zb public.hstore, type text, historic boolean, OUT json jsonb) RETURNS jsonb
    LANGUAGE plpgsql
    AS $$
  DECLARE
    btype text;

    gm_id int;
    b_id int;

    subject record;
    s_hstore hstore;
    subject_json json;

    contactdata_person int;
    contactdata_company int;

  BEGIN

    contactdata_person  := 1;
    contactdata_company := 2;

    btype := zb->'betrokkene_type';
    gm_id := (zb->'gegevens_magazijn_id')::int;
    b_id  := (zb->'betrokkene_id')::int;

    IF historic = true
    THEN
      type := CONCAT(type, '_snapshot');
    END IF;

    IF btype IS NULL THEN
      SELECT INTO json empty_subject_as_v0_json(type);
      RETURN;
    END IF;

    IF btype NOT IN ('medewerker', 'natuurlijk_persoon', 'bedrijf') THEN
      RAISE EXCEPTION 'Unknown betrokkene type %', btype;
    END IF;

    IF btype = 'medewerker' THEN

      -- There is no difference between historic data for employees

      SELECT INTO
        subject
        s.*,
        gr.name as department

      FROM
        subject s
      LEFT JOIN
        groups gr
      ON
        s.group_ids[1] = gr.id
      WHERE
        s.subject_type = 'employee'
      AND
        s.id = gm_id;

      s_hstore := hstore(subject);

      SELECT INTO json employee_as_v0_json(s_hstore, type);

    ELSIF btype = 'natuurlijk_persoon' THEN

      IF historic = false
      THEN

        SELECT INTO
          subject
          np.*,
          a.straatnaam as street,
          a.huisnummer as street_number,
          a.huisletter as street_number_letter,
          a.huisnummertoevoeging as street_number_suffix,
          a.postcode as zipcode,
          a.woonplaats as city,
          a.functie_adres as address_type,
          mc.json as municipality,
          cc.label as country,
          a.adres_buitenland1 as foreign_address_line_1,
          a.adres_buitenland2 as foreign_address_line_2,
          a.adres_buitenland3 as foreign_address_line_3,
          a.bag_id as bag_id,
          a.geo_lat_long[0] as latitude,
          a.geo_lat_long[1] as longitude,
          cd.mobiel as mobile,
          cd.telefoonnummer as phone_number,
          cd.email as email_address
        FROM
          natuurlijk_persoon np
        LEFT JOIN
          adres a
        ON
          np.adres_id = a.id
        LEFT JOIN
          country_code cc
        ON
          cc.dutch_code = a.landcode
        LEFT JOIN
          municipality_code_v1_view mc
        ON
          mc.dutch_code = a.gemeente_code
        LEFT JOIN
          contact_data cd
        ON
          (cd.gegevens_magazijn_id = np.id and cd.betrokkene_type = contactdata_person)

        WHERE
          np.id = gm_id;
      ELSE

        SELECT INTO
          subject
          np.*,
          a.straatnaam as street,
          a.huisnummer as street_number,
          a.huisletter as street_number_letter,
          a.huisnummertoevoeging as street_number_suffix,
          a.postcode as zipcode,
          a.woonplaats as city,
          a.functie_adres as address_type,
          mc.json as municipality,
          cc.label as country,
          a.adres_buitenland1 as foreign_address_line_1,
          a.adres_buitenland2 as foreign_address_line_2,
          a.adres_buitenland3 as foreign_address_line_3,
          a.bag_id as bag_id,
          a.geo_lat_long[0] as latitude,
          a.geo_lat_long[1] as longitude,
          cd.mobiel as mobile,
          cd.telefoonnummer as phone_number,
          cd.email as email_address
        FROM
          gm_natuurlijk_persoon np
        LEFT JOIN
          gm_adres a
        ON
          np.adres_id = a.id
        LEFT JOIN
          country_code cc
        ON
          cc.dutch_code = a.landcode
        LEFT JOIN
          municipality_code_v1_view mc
        ON
          mc.dutch_code = a.gemeente_code
        LEFT JOIN
          contact_data cd
        ON
          (cd.gegevens_magazijn_id = np.id and cd.betrokkene_type = contactdata_person)

        WHERE
          np.gegevens_magazijn_id = gm_id and np.id = b_id;
      END IF;

      s_hstore := hstore(subject);

      SELECT INTO json person_as_v0_json(s_hstore, type);

    ELSIF btype = 'bedrijf' THEN

      IF historic = false
      THEN

        SELECT INTO subject
        b.*,
        let.label as company_type,
        cc.label as country_of_residence,
        cd.mobiel as mobile,
        cd.telefoonnummer as phone_number,
        cd.email as email_address
        FROM
          bedrijf b
        LEFT JOIN
          legal_entity_type let
        ON
          let.code = b.rechtsvorm
        LEFT JOIN
          country_code cc
        ON
          cc.dutch_code = b.vestiging_landcode
        LEFT JOIN
          contact_data cd
        ON
          (cd.gegevens_magazijn_id = b.id and cd.betrokkene_type = contactdata_company)
        WHERE
          b.id = gm_id;
      ELSE

        SELECT INTO subject
        b.*,
        let.label as company_type,
        cc.label as country_of_residence,
        cd.mobiel as mobile,
        cd.telefoonnummer as phone_number,
        cd.email as email_address
        FROM
          gm_bedrijf b
        LEFT JOIN
          legal_entity_type let
        ON
          let.code = b.rechtsvorm
        LEFT JOIN
          country_code cc
        ON
          cc.dutch_code = b.vestiging_landcode
        LEFT JOIN
          contact_data cd
        ON
          (cd.gegevens_magazijn_id = b.id and cd.betrokkene_type = contactdata_company)
        WHERE
          b.gegevens_magazijn_id = gm_id and b.id = b_id;

      END IF;

      s_hstore := hstore(subject);

      SELECT INTO json company_as_v0_json(s_hstore, type);
      RETURN;

    END IF;

  END;
  $$;


--
-- Name: case_subject_json(public.hstore); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.case_subject_json(zb public.hstore, OUT json jsonb) RETURNS jsonb
    LANGUAGE plpgsql
    AS $$
  DECLARE
    btype text;
    gm_id int;
    old_id text;

    subject record;
    s_hstore hstore;
    subject_json json;

    subject_uuid uuid;
    subject_type text;
    display_name text;

  BEGIN

    btype := zb->'betrokkene_type';

    IF btype IS NULL THEN
      json := null;
      RETURN;
    END IF;

    IF btype NOT IN ('medewerker', 'natuurlijk_persoon', 'bedrijf') THEN
      RAISE EXCEPTION 'Unknown betrokkene type %', btype;
    END IF;


    gm_id := (zb->'gegevens_magazijn_id')::int;
    old_id := 'betrokkene-' || btype || '-' || gm_id;

    IF btype = 'medewerker' THEN
      subject_type := 'employee';

      SELECT INTO subject * FROM subject s WHERE s.subject_type = 'employee'
        AND id = gm_id;

      s_hstore := hstore(subject);
      SELECT INTO subject_json subject_employee_json(s_hstore);
      SELECT INTO display_name get_display_name_for_employee(s_hstore);

    ELSIF btype = 'natuurlijk_persoon' THEN

      subject_type := 'person';

      SELECT INTO
        subject
        np.*,
        a.straatnaam as street,
        a.huisnummer as street_number,
        a.huisletter as street_number_letter,
        a.huisnummertoevoeging as street_number_suffix,
        a.postcode as zipcode,
        a.woonplaats as city,
        a.functie_adres as address_type,
        mc.json as municipality,
        cc.json as country,
        a.adres_buitenland1 as foreign_address_line_1,
        a.adres_buitenland2 as foreign_address_line_2,
        a.adres_buitenland3 as foreign_address_line_3,
        a.bag_id as bag_id,
        a.geo_lat_long[0] as latitude,
        a.geo_lat_long[1] as longitude,
        cd.mobiel as mobile,
        cd.telefoonnummer as phone_number,
        cd.email as email_address
      FROM
        natuurlijk_persoon np
      LEFT JOIN
        adres a
      ON
        np.adres_id = a.id
      LEFT JOIN
        country_code_v1_view cc
      ON
        cc.dutch_code = a.landcode
      LEFT JOIN
        municipality_code_v1_view mc
      ON
        mc.dutch_code = a.gemeente_code
      LEFT JOIN
        contact_data cd
      ON
        -- 1 or 2 is much clearer than natuurlijk_persoon or bedrijf ey
        -- :/
        (cd.gegevens_magazijn_id = np.id and cd.betrokkene_type = 1)

      WHERE
        np.id = gm_id;

      s_hstore := hstore(subject);

      SELECT INTO subject_json subject_person_json(s_hstore);
      SELECT INTO display_name get_display_name_for_person(s_hstore);

    ELSIF btype = 'bedrijf' THEN

      subject_type := 'company';

      SELECT INTO subject
      b.*,
      cc_vestiging.json as vestiging_country,
      cc_correspondentie.json as correspondentie_country,
      let.json as company_type
      FROM
        bedrijf b
      LEFT JOIN
        country_code_v1_view cc_vestiging
      ON
        cc_vestiging.dutch_code = b.vestiging_landcode
      LEFT JOIN
        country_code_v1_view cc_correspondentie
      ON
        cc_correspondentie.dutch_code = b.correspondentie_landcode
      LEFT JOIN
        legal_entity_v1_view let
      ON
        let.code = b.rechtsvorm
      WHERE
        b.id = gm_id
      ;

      s_hstore := hstore(subject);

      SELECT INTO subject_json subject_company_json(s_hstore);
      SELECT INTO display_name get_display_name_for_company(s_hstore);

    END IF;

    SELECT INTO json json_build_object(
        'preview', display_name,
        'type', 'subject',
        'reference', subject.uuid,
        'instance', json_build_object(
          'date_created', timestamp_to_perl_datetime(NOW()),
          'date_modified', timestamp_to_perl_datetime(NOW()),
          'display_name', display_name,
          'external_subscription', null,
          'old_subject_identifier', old_id,
          'subject_type', subject_type,
          'subject', subject_json
        )
    );

  END;
  $$;


--
-- Name: check_queue_object_id_constraint(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.check_queue_object_id_constraint() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
  BEGIN

    IF NEW.object_id IS NOT NULL
    THEN
      IF NOT EXISTS (SELECT uuid FROM object_data WHERE uuid = NEW.object_id)
      THEN
        RAISE EXCEPTION 'Nonexistent UUID --> %', NEW.object_id
          USING HINT = 'Please check your object_id';
      END IF;
    END IF;

    return NEW;

  END $$;


--
-- Name: check_queue_parent_id_constraint(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.check_queue_parent_id_constraint() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
  BEGIN

    IF NEW.parent_id IS NOT NULL
    THEN
      IF NOT EXISTS (SELECT id FROM queue WHERE id = NEW.parent_id)
      THEN
        RAISE EXCEPTION 'Nonexistent UUID --> %', NEW.parent_id
          USING HINT = 'Please check your parent_id';
      END IF;
    END IF;

    return NEW;

  END $$;


--
-- Name: company_as_v0_json(public.hstore, text); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.company_as_v0_json(subject public.hstore, type text, OUT json jsonb) RETURNS jsonb
    LANGUAGE plpgsql
    AS $$
  DECLARE
    key text;
    is_correspondence boolean;
    tmp text;

    active boolean;
    bid text;

  BEGIN

    key := CONCAT('case.', type, '.');

    is_correspondence := false;

    FOREACH tmp IN ARRAY ARRAY['woonplaats', 'straatnaam', 'postcode', 'adres_buitenland1', 'adres_buitenland2', 'adres_buitenland3']
    LOOP
      tmp := CONCAT('correspondentie_', tmp);
      IF subject->tmp IS NOT NULL AND LENGTH(subject->tmp) > 0 THEN
        is_correspondence := true;
        EXIT;
      END IF;
    END LOOP;


    SELECT INTO json jsonb_build_object(

      CONCAT(key, 'foreign_residence_address_line1'), subject->'vestiging_adres_buitenland1',
      CONCAT(key, 'foreign_residence_address_line2'), subject->'vestiging_adres_buitenland2',
      CONCAT(key, 'foreign_residence_address_line3'), subject->'vestiging_adres_buitenland3',

      CONCAT(key, 'correspondence_house_number'), complete_house_number(
        subject->'correspondentie_huisnummer', subject->'correspondentie_huisletter', subject->'correspondentie_huisnummertoevoeging'
      ),
      CONCAT(key, 'correspondence_place_of_residence'), subject->'correspondentie_woonplaats',
      CONCAT(key, 'correspondence_street'), subject->'correspondentie_straatnaam',
      CONCAT(key, 'correspondence_zipcode'), subject->'correspondentie_postcode',

      CONCAT(key, 'residence_house_number'), complete_house_number(
        subject->'vestiging_huisnummer', subject->'vestiging_huisletter', subject->'vestiging_huisnummertoevoeging'
      ),
      CONCAT(key, 'residence_place_of_residence'), subject->'vestiging_woonplaats',
      CONCAT(key, 'residence_street'), subject->'vestiging_straatnaam',
      CONCAT(key, 'residence_zipcode'), subject->'vestiging_postcode'

    );

    IF is_correspondence = true THEN
      SELECT INTO json json || jsonb_build_object(
        CONCAT(key, 'street'),  subject->'correspondentie_straatnaam',
        CONCAT(key, 'zipcode'), subject->'correspondentie_postcode',
        CONCAT(key, 'place_of_residence'), subject->'correspondentie_woonplaats',
        CONCAT(key, 'house_number'), complete_house_number(
          subject->'correspondentie_huisnummer', subject->'correspondentie_huisletter', subject->'correspondentie_huisnummertoevoeging'
        )
      );
    ELSE
      SELECT INTO json json || jsonb_build_object(
        CONCAT(key, 'street'),  subject->'vestiging_straatnaam',
        CONCAT(key, 'zipcode'), subject->'vestiging_postcode',
        CONCAT(key, 'place_of_residence'), subject->'vestiging_woonplaats',
        CONCAT(key, 'house_number'), complete_house_number(
          subject->'vestiging_huisnummer', subject->'vestiging_huisletter', subject->'vestiging_huisnummertoevoeging'
        )
      );
    END IF;

    -- active does not exists in the gm_ table of bedrijf
    -- and is always active, since well, you can't create cases with
    -- inactive people
    active := true;

    IF subject->'active' IS NOT NULL
    THEN
      active := (subject->'active')::boolean;
    END IF;

    IF subject->'gegevens_magazijn_id' IS NOT NULL
    THEN
      bid := CONCAT('betrokkene-bedrijf-', subject->'gegevens_magazijn_id');
    ELSE
      bid := CONCAT('betrokkene-bedrijf-', subject->'id');
    END IF;

    SELECT INTO json json || jsonb_build_object(
      type, subject->'uuid',
      CONCAT(key, 'type'), 'Niet natuurlijk persoon',
      CONCAT(key, 'subject_type'), 'bedrijf',

      CONCAT(key, 'status'), CASE WHEN active = true THEN 'Actief' ELSE 'Inactief' END,

      CONCAT(key, 'id'), bid,

      CONCAT(key, 'uuid'), subject->'uuid',
      CONCAT(key, 'preferred_contact_channel'), subject->'preferred_contact_channel',
      CONCAT(key, 'country_of_residence'), COALESCE(
        subject->'country_of_residence',
        CONCAT('Onbekende landcode: ', subject->'vestiging_landcode')
      ),

      CONCAT(key, 'name'), subject->'handelsnaam',
      CONCAT(key, 'display_name'), subject->'handelsnaam',
      CONCAT(key, 'coc'), subject->'dossiernummer',
      CONCAT(key, 'establishment_number'), subject->'vestigingsnummer',
      -- we have the actual data?
      CONCAT(key, 'type_of_business_entity'), (subject->'rechtsvorm')::int,
      CONCAT(key, 'trade_name'), subject->'handelsnaam',
      CONCAT(key, 'has_correspondence_address'), is_correspondence,

      CONCAT(key, 'email'), subject->'email_address',
      CONCAT(key, 'mobile_number'), subject->'mobile',
      CONCAT(key, 'phone_number'), subject->'phone_number',

      -- Not relevant for a company
      CONCAT(key, 'investigation'), null,
      CONCAT(key, 'a_number'), null,
      CONCAT(key, 'bsn'), null,
      CONCAT(key, 'family_name'), null,
      CONCAT(key, 'first_names'), null,
      CONCAT(key, 'surname'), null,
      CONCAT(key, 'full_name'), null,
      CONCAT(key, 'gender'), null,
      CONCAT(key, 'salutation'), null,
      CONCAT(key, 'salutation1'), null,
      CONCAT(key, 'salutation2'), null,
      CONCAT(key, 'initials'), null,
      CONCAT(key, 'is_secret'), null,
      CONCAT(key, 'naamgebruik'), null,
      CONCAT(key, 'noble_title'), null,
      CONCAT(key, 'place_of_birth'), null,
      CONCAT(key, 'surname_prefix'), null,
      CONCAT(key, 'country_of_birth'), null,
      CONCAT(key, 'date_of_birth'), null,
      CONCAT(key, 'date_of_death'), null,
      CONCAT(key, 'date_of_divorce'), null,
      CONCAT(key, 'date_of_marriage'), null,
      CONCAT(key, 'login'), null,
      CONCAT(key, 'department'), null,
      CONCAT(key, 'title'), null,
      CONCAT(key, 'password'), null

    );

  END;
  $$;


--
-- Name: complete_house_number(text, text, text); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.complete_house_number(house_number text, letter text, postfix text, OUT complete text) RETURNS text
    LANGUAGE plpgsql
    AS $$
  DECLARE

  BEGIN

    IF house_number IS NULL
    THEN
      complete = '';
      RETURN;
    END IF;

    complete := house_number::text;

    IF letter IS NOT NULL AND length(letter) > 0
    THEN
      complete := CONCAT(complete, ' ', letter);
    END IF;


    IF postfix IS NOT NULL AND length(postfix) > 0
    THEN
      complete := CONCAT(complete, ' - ', postfix);
    END IF;

    RETURN;

  END;
  $$;


--
-- Name: country_code_to_json(public.hstore); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.country_code_to_json(unit public.hstore, OUT unit_json jsonb) RETURNS jsonb
    LANGUAGE plpgsql
    AS $$
  BEGIN
      SELECT INTO unit_json json_build_object(
        'reference', unit->'uuid',
        'preview', unit->'label',
        'type', 'country_code',
        'instance', json_build_object(
          'label', unit->'label',
          'alpha_one', null,
          'alpha_two', null,
          'alpha_three', null,
          'code', null,
          'dutch_code', lpad(unit->'dutch_code', 4, '0')
        )
      );
      RETURN;
  END;
  $$;


--
-- Name: create_queue_partition(date); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.create_queue_partition(t_stamp date) RETURNS void
    LANGUAGE plpgsql
    AS $$
  DECLARE
    t_name text;
    t_end date;
    t_start date;
  BEGIN

    t_start := t_stamp;
    t_end := t_stamp + interval '1 month';

    SELECT INTO t_name REGEXP_REPLACE('queue_' || t_stamp::text, '-', '', 'g');

    RAISE NOTICE '%', t_name;

    EXECUTE format('
       CREATE TABLE %I PARTITION OF queue_part
         FOR VALUES FROM (%L) TO (%L)
    ', t_name, t_start, t_end);

    EXECUTE format('
      CREATE TRIGGER insert_leading_qitem
      BEFORE INSERT ON %I
      FOR EACH ROW
      EXECUTE PROCEDURE set_leading_qitem();
    ' , t_name);

    EXECUTE format('
      CREATE TRIGGER check_queue_object_id_constraint
      BEFORE INSERT ON %I
      FOR EACH ROW
      EXECUTE PROCEDURE check_queue_object_id_constraint();
    ' , t_name);

    EXECUTE format('
      CREATE TRIGGER check_queue_parent_id_constraint
      BEFORE INSERT ON %I
      FOR EACH ROW
      EXECUTE PROCEDURE check_queue_parent_id_constraint();
    ' , t_name);

    EXECUTE format('
      CREATE INDEX ON %I((data::jsonb->%L->>%L)) WHERE type = %L
    ' , t_name, 'create-args', 'duplicate_prevention_token', 'create_case_form');

  END $$;


--
-- Name: employee_as_v0_json(public.hstore, text); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.employee_as_v0_json(subject public.hstore, type text, OUT json jsonb) RETURNS jsonb
    LANGUAGE plpgsql
    AS $$
  DECLARE
    key text;

    properties jsonb;
    config_row jsonb;

    ue record;
    is_active boolean;

  BEGIN

    key := CONCAT('case.', type, '.');

    properties := (subject->'properties')::jsonb;

    SELECT INTO config_row jsonb_object_agg(config.parameter, config.value) from config where parameter like 'customer_info%';

    is_active := false;
    FOR ue IN
      select distinct(active) as active from user_entity where subject_id = (subject->'id')::int and date_deleted is null
    LOOP
      IF ue.active = true THEN
        is_active := true;
        EXIT;
      END IF;
    END LOOP;

    SELECT INTO json jsonb_build_object(

      type, subject->'uuid',
      CONCAT(key, 'type'), 'Behandelaar',
      CONCAT(key, 'subject_type'), 'medewerker',

      CONCAT(key, 'status'), CASE WHEN is_active = true THEN 'Actief' ELSE 'Inactief' END,

      CONCAT(key, 'uuid'), subject->'uuid',
      CONCAT(key, 'id'), CONCAT('betrokkene-medewerker-', subject->'id'),
      CONCAT(key, 'preferred_contact_channel'), null,
      CONCAT(key, 'country_of_residence'), null,

      CONCAT(key, 'display_name'), properties->'displayname',
      CONCAT(key, 'name'), properties->'displayname',
      CONCAT(key, 'full_name'), properties->'displayname',
      CONCAT(key, 'initials'), properties->'initials',
      CONCAT(key, 'naamgebruik'), properties->'sn',
      CONCAT(key, 'surname'), properties->'sn',

      CONCAT(key, 'department'), subject->'department',

      CONCAT(key, 'email'), properties->'mail',
      CONCAT(key, 'phone_number'), properties->'telephonenumber',

      -- blub
      CONCAT(key, 'mobile_number'), null,
      CONCAT(key, 'login'), null,
      CONCAT(key, 'title'), null,
      CONCAT(key, 'password'), null,

      -- In use...
      CONCAT(key, 'correspondence_house_number'), config_row->'customer_info_huisnummer',
      CONCAT(key, 'correspondence_place_of_residence'), config_row->'customer_info_woonplaats',
      CONCAT(key, 'correspondence_street'), config_row->'customer_info_straatnaam',
      CONCAT(key, 'correspondence_zipcode'), config_row->'customer_info_postcode',

      CONCAT(key, 'residence_house_number'), config_row->'customer_info_huisnummer',
      CONCAT(key, 'residence_place_of_residence'), config_row->'customer_info_woonplaats',
      CONCAT(key, 'residence_street'), config_row->'customer_info_straatnaam',
      CONCAT(key, 'residence_zipcode'), config_row->'customer_info_postcode',

      CONCAT(key, 'house_number'), config_row->'customer_info_huisnummer',
      CONCAT(key, 'place_of_residence'), config_row->'customer_info_woonplaats',
      CONCAT(key, 'street'), config_row->'customer_info_straatnaam',
      CONCAT(key, 'zipcode'), config_row->'customer_info_postcode'

    ) -- work around >100 keys
    || jsonb_build_object(
      -- Not relevant for an employee
      CONCAT(key, 'coc'), null,
      CONCAT(key, 'establishment_number'), null,
      CONCAT(key, 'type_of_business_entity'), null,
      CONCAT(key, 'trade_name'), null,
      CONCAT(key, 'has_correspondence_address'), null,
      CONCAT(key, 'foreign_residence_address_line1'), null,
      CONCAT(key, 'foreign_residence_address_line2'), null,
      CONCAT(key, 'foreign_residence_address_line3'), null,
      CONCAT(key, 'investigation'), null,
      CONCAT(key, 'a_number'), null,
      CONCAT(key, 'bsn'), null,
      CONCAT(key, 'family_name'), null,
      CONCAT(key, 'first_names'), null,
      CONCAT(key, 'gender'), null,
      CONCAT(key, 'salutation'), null,
      CONCAT(key, 'salutation1'), null,
      CONCAT(key, 'salutation2'), null,
      CONCAT(key, 'is_secret'), null,
      CONCAT(key, 'naamgebruik'), null,
      CONCAT(key, 'noble_title'), null,
      CONCAT(key, 'place_of_birth'), null,
      CONCAT(key, 'surname_prefix'), null,
      CONCAT(key, 'country_of_birth'), null,
      CONCAT(key, 'date_of_birth'), null,
      CONCAT(key, 'date_of_death'), null,
      CONCAT(key, 'date_of_divorce'), null,
      CONCAT(key, 'date_of_marriage'), null

    );

  END;
  $$;


--
-- Name: empty_subject_as_v0_json(text); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.empty_subject_as_v0_json(type text, OUT json jsonb) RETURNS jsonb
    LANGUAGE plpgsql
    AS $$
  DECLARE
    key text;

    field text;
    fields text[];

  BEGIN

    key := CONCAT('case.', type, '.');

    fields := ARRAY[
      'a_number',
      'bsn',
      'coc',
      'correspondence_house_number',
      'correspondence_place_of_residence',
      'correspondence_street',
      'correspondence_zipcode',
      'country_of_birth',
      'country_of_residence',
      'date_of_birth',
      'date_of_death',
      'date_of_divorce',
      'date_of_marriage',
      'department',
      'display_name',
      'email',
      'establishment_number',
      'family_name',
      'first_names',
      'foreign_residence_address_line1',
      'foreign_residence_address_line2',
      'foreign_residence_address_line3',
      'full_name',
      'gender',
      'has_correspondence_address',
      'house_number',
      'id',
      'initials',
      'investigation',
      'is_secret',
      'login',
      'mobile_number',
      'naamgebruik',
      'name',
      'noble_title',
      'password',
      'phone_number',
      'place_of_birth',
      'place_of_residence',
      'preferred_contact_channel',
      'residence_house_number',
      'residence_place_of_residence',
      'residence_street',
      'residence_zipcode',
      'salutation',
      'salutation1',
      'salutation2',
      'status',
      'street',
      'subject_type',
      'surname',
      'surname_prefix',
      'title',
      'trade_name',
      'type',
      'type_of_business_entity',
      'uuid',
      'zipcode'
    ];

    json := '{}'::jsonb;
    FOREACH field IN ARRAY fields
    LOOP
      SELECT INTO json json || jsonb_build_object(
        CONCAT(key, field), null
      );
    END LOOP;

  END;
  $$;


--
-- Name: generate_intials_for_np(text); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.generate_intials_for_np(name text, OUT initials text) RETURNS text
    LANGUAGE plpgsql
    AS $$
    DECLARE
      names TEXT[];
      i int;
    BEGIN

      IF name is NULL or LENGTH(name) <1
      THEN
        initials := '';
        RETURN;
      END IF;

      names := regexp_split_to_array(name, '\s+');

      initials := substr(names[1], 1, 1);
      IF array_upper(names, 1) > 2
      THEN
        FOR i in 2 ..array_length(names, 1)
        LOOP
          initials := concat(initials, '.', substr(names[i], 1, 1));
        END LOOP;
      END IF;

      initials := UPPER(concat(initials, '.', ''));
      RETURN;

    END $$;


--
-- Name: get_case_status_perc(integer, integer); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.get_case_status_perc(milestone integer, node integer, OUT percentage numeric) RETURNS numeric
    LANGUAGE plpgsql
    AS $$
  DECLARE
    status_count int;
  BEGIN

    select into status_count count(id) from zaaktype_status where zaaktype_node_id = node;

    IF status_count = 0
    THEN
      percentage := '0';
      RETURN;
    END IF;

    percentage := 100 * milestone / status_count;

  RETURN;
  END;
  $$;


--
-- Name: get_confidential_mapping(public.confidentiality); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.get_confidential_mapping(type public.confidentiality, OUT mapping jsonb) RETURNS jsonb
    LANGUAGE plpgsql
    AS $$
  DECLARE
    mapped text;
  BEGIN

    IF type = 'public'
    THEN
      mapped := 'Openbaar';
    ELSIF type = 'internal'
    THEN
      mapped := 'Intern';
    ELSIF type = 'confidential'
    THEN
      mapped := 'Geheim';
    ELSE
      RAISE EXCEPTION 'Unknown confidentiality type %', type;
    END IF;


    mapping := jsonb_build_object(
      'original', type,
      'mapped', mapped
    );

  END;
  $$;


--
-- Name: get_date_progress_from_case(public.hstore); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.get_date_progress_from_case(zaak public.hstore, OUT percentage text) RETURNS text
    LANGUAGE plpgsql
    AS $$
  DECLARE
    completion_date timestamp with time zone;
    completion_epoch int;
    start_epoch int;
    target_epoch int;

    current_difference int;
    max_difference int;

    perc int;
  BEGIN

    percentage := '';
    IF zaak->'status' = 'stalled'
    THEN
      RETURN;
    END IF;

    IF zaak->'afhandeldatum' IS NOT NULL
    THEN
      completion_date := (zaak->'afhandeldatum')::timestamp without time zone;
    ELSE
      SELECT INTO completion_date NOW()::timestamp without time zone;
    END IF;

    SELECT INTO completion_epoch date_part('epoch', completion_date);
    SELECT INTO start_epoch date_part('epoch', (zaak->'registratiedatum')::timestamp without time zone);

    current_difference := completion_epoch - start_epoch;

    SELECT INTO target_epoch date_part('epoch', (zaak->'streefafhandeldatum')::timestamp without time zone);

    max_difference := target_epoch - start_epoch;
    max_difference := GREATEST(max_difference, 1);

    perc := ROUND(100 * ( current_difference::numeric / max_difference::numeric ));
    percentage := perc::text;

    RETURN;
  END;
  $$;


--
-- Name: get_display_name_for_company(public.hstore); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.get_display_name_for_company(nnp public.hstore, OUT display_name text) RETURNS text
    LANGUAGE plpgsql
    AS $$
  DECLARE
  BEGIN

    display_name := nnp->'handelsnaam';
    RETURN;
  END;
  $$;


--
-- Name: get_display_name_for_employee(public.hstore); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.get_display_name_for_employee(emp public.hstore, OUT display_name text) RETURNS text
    LANGUAGE plpgsql
    AS $$
  DECLARE
  BEGIN

    display_name := (emp->'properties')::jsonb->>'displayname';
    RETURN;
  END;
  $$;


--
-- Name: get_display_name_for_person(public.hstore); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.get_display_name_for_person(np public.hstore, OUT display_name text) RETURNS text
    LANGUAGE plpgsql
    AS $$
  DECLARE
    initials text;
    surname text;
  BEGIN

    initials := np->'voorletters';
    surname := np->'naamgebruik';

    IF surname IS NULL
    THEN
        surname := np->'geslachtsnaam';
    END IF;

    IF initials = '' OR initials IS NULL
    THEN
      display_name := surname;
    ELSE
      display_name := concat(initials, ' ', surname);
    END IF;

  END;
  $$;


--
-- Name: get_payment_status_mapping(text); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.get_payment_status_mapping(type text, OUT mapping jsonb) RETURNS jsonb
    LANGUAGE plpgsql
    AS $$
  DECLARE
    mapped text;
  BEGIN

    IF type IS NULL
    THEN
      RETURN;
    ELSIF type = 'pending'
    THEN
      mapped := 'Wachten op bevestiging';
    ELSIF type = 'offline'
    THEN
      mapped := 'Later betalen';
    ELSIF type = 'failed'
    THEN
      mapped := 'Niet geslaagd';
    ELSIF type = 'success'
    THEN
      mapped := 'Geslaagd';
    ELSE
      RAISE EXCEPTION 'Unknown payment status type %', type;
    END IF;

    mapping := jsonb_build_object(
      'original', type,
      'mapped', mapped
    );

  END;
  $$;


--
-- Name: get_subject_by_legacy_id(text); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.get_subject_by_legacy_id(legacy_id text, OUT subject_uuid uuid) RETURNS uuid
    LANGUAGE plpgsql
    AS $$
    DECLARE
      betrokkene_id TEXT[];
      subject_type TEXT;
      subject_id INT;

    BEGIN
        betrokkene_id = string_to_array(legacy_id, '-');

        -- on our dev we have some weirdness going on
        -- unsure about live data
        IF array_length(betrokkene_id, 1) = 3
        THEN
          subject_type = betrokkene_id[2]::text;
          subject_id = betrokkene_id[3]::int;
        ELSE
          subject_type = betrokkene_id[4]::text;
          subject_id = betrokkene_id[5]::int;
        END IF;

        /* Employee or medewerker, work around a bug we have */
        IF subject_type = 'medewerker' OR subject_type = 'employee'
        THEN
          SELECT INTO subject_uuid uuid FROM subject where id = subject_id;
        ELSIF subject_type = 'natuurlijk_persoon'
        THEN
          SELECT INTO subject_uuid uuid FROM natuurlijk_persoon
          where id = subject_id;
        ELSIF subject_type = 'bedrijf'
        THEN
          SELECT INTO subject_uuid uuid FROM bedrijf where id = subject_id;
        END IF;

        RETURN;
    END $$;


--
-- Name: get_subject_by_uuid(uuid); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.get_subject_by_uuid(subject_uuid uuid, OUT subject_type text) RETURNS text
    LANGUAGE plpgsql
    AS $$
    DECLARE
      found uuid;
    BEGIN
          SELECT INTO found uuid FROM natuurlijk_persoon where uuid = subject_uuid;

          IF found IS NOT NULL
          THEN
            subject_type := 'person';
          END IF;

          SELECT INTO found uuid FROM bedrijf where uuid = subject_uuid;
          IF found is not null THEN
            subject_type := 'company';
          END IF;

          SELECT INTO found uuid FROM subject where uuid = subject_uuid
            AND subject.subject_type = 'employee';
          IF found is not null THEN
            subject_type := 'employee';
          END IF;

        RETURN;
    END $$;


--
-- Name: get_subject_display_name_by_uuid(uuid); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.get_subject_display_name_by_uuid(subject_uuid uuid, OUT display_name text) RETURNS text
    LANGUAGE plpgsql
    AS $$
    DECLARE
      found record;
      initials text;
      surname text;
    BEGIN
      SELECT INTO found * FROM natuurlijk_persoon where uuid = subject_uuid;

      IF found.uuid IS NOT NULL
      THEN

        SELECT INTO display_name get_display_name_for_person(hstore(found));
        RETURN;
      END IF;

      SELECT INTO found * FROM bedrijf where uuid = subject_uuid;
      IF found.uuid IS NOT NULL THEN
        SELECT INTO display_name get_display_name_for_company(hstore(found));
        RETURN;
      END IF;

      SELECT INTO found * FROM subject where uuid = subject_uuid
        AND subject.subject_type = 'employee';

      IF found.uuid IS NOT NULL THEN
        SELECT INTO display_name get_display_name_for_employee(hstore(found));
        RETURN;
      END IF;

      RETURN;
    END $$;


--
-- Name: hstore_to_timestamp(character varying); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.hstore_to_timestamp(date_field character varying) RETURNS timestamp without time zone
    LANGUAGE sql IMMUTABLE
    AS $_$















































  SELECT to_timestamp($1, 'YYYY-MM-DD"T"HH24:MI:SS')::TIMESTAMP;















































$_$;


--
-- Name: insert_file_timestamps(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.insert_file_timestamps() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
        BEGIN
            NEW.date_created = NOW() AT TIME ZONE 'UTC';
            NEW.date_modified = NOW() AT TIME ZONE 'UTC';
            RETURN NEW;
        END;
    $$;


--
-- Name: insert_timestamps(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.insert_timestamps() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
        BEGIN
            IF NEW.created is NULL
            THEN
                NEW.created = NOW();
            END IF;
            IF NEW.last_modified is NULL
            THEN
                NEW.last_modified = NOW();
            END IF;
            RETURN NEW;
        END;
    $$;


--
-- Name: is_destructable(timestamp with time zone); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.is_destructable(destruction_date timestamp with time zone, OUT destruction boolean) RETURNS boolean
    LANGUAGE plpgsql
    AS $$
  BEGIN


    IF destruction_date IS NOT NULL AND destruction_date < NOW()
    THEN
      destruction := true;
    ELSE
      destruction := false;
    END IF;
  END;
  $$;


--
-- Name: legal_entity_code_to_json(public.hstore); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.legal_entity_code_to_json(unit public.hstore, OUT unit_json jsonb) RETURNS jsonb
    LANGUAGE plpgsql
    AS $$
  BEGIN
      SELECT INTO unit_json json_build_object(
        'reference', unit->'uuid',
        'preview', unit->'label',
        'type', 'legal_entity_type',
        'instance', json_build_object(
          'label', unit->'label',
          'code', unit->'code',
          'active', (unit->'active')::boolean,
          'description', null
        )
      );
      RETURN;
  END;
  $$;


--
-- Name: municipality_code_to_json(public.hstore); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.municipality_code_to_json(unit public.hstore, OUT unit_json jsonb) RETURNS jsonb
    LANGUAGE plpgsql
    AS $$
  BEGIN
      SELECT INTO unit_json json_build_object(
        'reference', unit->'uuid',
        'preview', unit->'label',
        'type', 'municipality_code',
        'instance', json_build_object(
          'label', unit->'label',
          'dutch_code', lpad(unit->'dutch_code', 4, '0'),
          'alternative_name', unit->'alternative_name',
          'date_created', NOW(),
          'date_modified', NOW()
        )
      );
      RETURN;
  END;
  $$;


--
-- Name: org_unit(public.hstore, text); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.org_unit(org public.hstore, type text, OUT org_json jsonb) RETURNS jsonb
    LANGUAGE plpgsql
    AS $$
  DECLARE
    org_instance jsonb;
    org_id text;
  BEGIN

    IF type = 'role'
    THEN
      org_id := 'role_id';
    ELSE
      org_id := 'group_id';
    END IF;


      SELECT INTO org_instance jsonb_build_object(
        'name', org->'name',
        'description', org->'description',
        'date_modified', timestamp_to_perl_datetime((org->'date_modified')::timestamp with time zone),
        'date_created', timestamp_to_perl_datetime((org->'date_created')::timestamp with time zone)
      );

      IF type = 'role' THEN
        SELECT INTO org_instance org_instance || jsonb_build_object(
          'system_role', (org->'system_role')::boolean
        );
        SELECT INTO org_instance org_instance || jsonb_build_object(
          'role_id', (org->'id')::int
        );
      ELSE
        SELECT INTO org_instance org_instance || jsonb_build_object(
          'group_id', (org->'id')::int
        );
      END IF;

      SELECT INTO org_json json_build_object(
        'reference', org->'uuid',
        'preview', org->'name',
        'type', type,
        'instance', org_instance
      );

      RETURN;
  END;
  $$;


--
-- Name: person_as_v0_json(public.hstore, text); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.person_as_v0_json(np public.hstore, type text, OUT json jsonb) RETURNS jsonb
    LANGUAGE plpgsql
    AS $$
  DECLARE
    key text;
    gender text;
    salutation text;
    salutation1 text;
    salutation2 text;

    address_inactive text;
    address_active text;
    house_number text;

    investigation boolean;
    active boolean;
    tmp text;
    bid text;

  BEGIN

    key := CONCAT('case.', type, '.');

    investigation := false;

    FOREACH tmp IN ARRAY ARRAY['persoon', 'huwelijk', 'overlijden', 'verblijfplaats']
    LOOP
      tmp := CONCAT('onderzoek_', tmp);
      IF (np->tmp)::boolean = true THEN
        investigation := true;
        EXIT;
      END IF;
    END LOOP;

    gender := LOWER(np->'geslachtsaanduiding');

    IF gender = 'm'
    THEN
      gender := 'man';
      salutation := 'meneer';
      salutation1 := 'heer';
      salutation2 := 'de heer';
    ELSIF gender = 'v'
    THEN
      gender := 'vrouw';
      salutation := 'mevrouw';
      salutation1 := 'heer';
      salutation2 := 'de heer';
    ELSE
      gender := '';
      salutation := '';
      salutation1 := '';
      salutation2 := '';
    END IF;

    house_number := complete_house_number(
      np->'street_number',
      np->'street_number_letter',
      np->'street_number_suffix'
    );

    SELECT INTO json jsonb_build_object(

      /* This is so wrong on so many levels:
       *
       * This needs to be NULL if the country_code = 6030 This can only
       * be used when address_type = 'W'. Yet, we allow 'B' and
       * incorrect data. For now, respect the old incorrect API.
       */
      CONCAT(key, 'foreign_residence_address_line1'), np->'foreign_address_line_1',
      CONCAT(key, 'foreign_residence_address_line2'), np->'foreign_address_line_2',
      CONCAT(key, 'foreign_residence_address_line3'), np->'foreign_address_line_3',

      /* This is also wrong.
       *
       * This needs to be NULL if the country_code != 6030
       */
      CONCAT(key, 'street'), np->'street',
      CONCAT(key, 'zipcode'), np->'zipcode',
      CONCAT(key, 'place_of_residence'), np->'city',
      CONCAT(key, 'house_number'), house_number
    );

    IF np->'address_type' = 'W' THEN
      address_active = 'residence';
      address_inactive = 'correspondence';
    ELSE
      address_inactive = 'residence';
      address_active = 'correspondence';
    END IF;

    SELECT INTO json json || jsonb_build_object(
      CONCAT(key, address_active, '_house_number'), house_number,
      CONCAT(key, address_active, '_place_of_residence'), np->'city',
      CONCAT(key, address_active, '_street'), np->'street',
      CONCAT(key, address_active, '_zipcode'), np->'zipcode',

      CONCAT(key, address_inactive, '_house_number'), '',
      CONCAT(key, address_inactive, '_place_of_residence'), null,
      CONCAT(key, address_inactive, '_street'), null,
      CONCAT(key, address_inactive, '_zipcode'), null
    );

    -- active does not exists in the gm_ table of NP
    -- and is always active, since well, you can't create cases with
    -- inactive people
    active := true;
    IF np->'active' IS NOT NULL
    THEN
      active := (np->'active')::boolean;
    END IF;

    IF np->'gegevens_magazijn_id' IS NOT NULL
    THEN
      bid := CONCAT('betrokkene-natuurlijk_persoon-', np->'gegevens_magazijn_id');
    ELSE
      bid := CONCAT('betrokkene-natuurlijk_persoon-', np->'id');
    END IF;

    SELECT INTO json json || jsonb_build_object(
      type, np->'uuid',
      CONCAT(key, 'type'), 'Natuurlijk persoon',
      CONCAT(key, 'subject_type'), 'natuurlijk_persoon',

      CONCAT(key, 'status'), CASE WHEN active = true THEN 'Actief' ELSE 'Inactief' END,
      CONCAT(key, 'id'), bid,

      CONCAT(key, 'uuid'), np->'uuid',
      CONCAT(key, 'a_number'), np->'a_nummer',
      CONCAT(key, 'bsn'), np->'burgerservicenummer',

      CONCAT(key, 'family_name'), np->'geslachtsnaam',
      CONCAT(key, 'first_names'), np->'voornamen',
      CONCAT(key, 'surname'), np->'naamgebruik',
      CONCAT(key, 'full_name'), CONCAT(np->'voorletters', ' ', np->'naamgebruik'),
      CONCAT(key, 'display_name'), CONCAT(np->'voorletters', ' ', np->'naamgebruik'),
      CONCAT(key, 'gender'), gender,
      CONCAT(key, 'salutation'), salutation,
      CONCAT(key, 'salutation1'), salutation1,
      CONCAT(key, 'salutation2'), salutation2,

      CONCAT(key, 'initials'), np->'voorletters',
      CONCAT(key, 'is_secret'), np->'indicatie_geheim',
      CONCAT(key, 'naamgebruik'), np->'naamgebruik',
      CONCAT(key, 'name'), CONCAT(np->'voorletters', ' ', np->'naamgebruik'),
      CONCAT(key, 'noble_title'), np->'adellijke_titel',
      CONCAT(key, 'place_of_birth'), np->'geboorteplaats',
      CONCAT(key, 'preferred_contact_channel'), np->'preferred_contact_channel',

      CONCAT(key, 'investigation'), investigation,
      CONCAT(key, 'surname_prefix'), np->'voorvoegsel',

      CONCAT(key, 'country_of_birth'), np->'geboorteland',
      CONCAT(key, 'country_of_residence'), np->'country',
      CONCAT(key, 'date_of_birth'), (np->'geboortedatum')::date,
      CONCAT(key, 'date_of_death'), (np->'overlijdensdatum')::date,
      CONCAT(key, 'date_of_divorce'), (np->'datum_huwelijk_ontbinding')::date,
      CONCAT(key, 'date_of_marriage'), (np->'datum_huwelijk')::date,

      CONCAT(key, 'email'), np->'email_address',
      CONCAT(key, 'mobile_number'), np->'mobile',
      CONCAT(key, 'phone_number'), np->'phone_number',
      CONCAT(key, 'has_correspondence_address'), CASE WHEN np->'address_type' = 'W' THEN false ELSE true END,

      -- Not relevant for NP
      CONCAT(key, 'coc'), null,
      CONCAT(key, 'department'), null,
      CONCAT(key, 'establishment_number'), null,
      CONCAT(key, 'login'), null,
      CONCAT(key, 'title'), null,
      CONCAT(key, 'trade_name'), null,
      CONCAT(key, 'type_of_business_entity'), null,
      CONCAT(key, 'password'), null
    );

  END;
  $$;


--
-- Name: position_matrix(integer[], integer[]); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.position_matrix(groups integer[], roles integer[], OUT json jsonb) RETURNS jsonb
    LANGUAGE plpgsql
    AS $$
  DECLARE
    role_id int;
    group_id int;

    gr jsonb;
    role json;

    pos jsonb;
  BEGIN

      pos := '[]'::jsonb; -- || does nothing when value is NULL

      FOREACH group_id IN ARRAY groups
      LOOP
        SELECT INTO gr v1_json FROM groups WHERE id = group_id;
        FOREACH role_id IN ARRAY roles
        LOOP
          SELECT INTO role v1_json FROM roles WHERE id = role_id;
          SELECT INTO pos pos || jsonb_build_object(
            'preview', 'position(unsynched)',
            'reference', NULL,
            'type', 'position',
            'instance', json_build_object(
              'date_modified', timestamp_to_perl_datetime(NOW()),
              'date_created', timestamp_to_perl_datetime(NOW()),
              'group', gr,
              'role', role
            )
          );
        END LOOP;
      END LOOP;

      SELECT INTO json json_build_object(
        'type', 'set',
        'reference', null,
        'instance', json_build_object(
          'pager', null,
          'rows', pos
        )
      );
  END;
  $$;


--
-- Name: refresh_casetype_document_id_map(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.refresh_casetype_document_id_map() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
        BEGIN
            REFRESH MATERIALIZED VIEW zaaktype_document_kenmerken_map;
            RETURN NULL;
        END
    $$;


--
-- Name: refresh_casetype_end_status(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.refresh_casetype_end_status() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
  BEGIN
    REFRESH MATERIALIZED VIEW casetype_end_status;
    RETURN NULL;
  END
  $$;


--
-- Name: refresh_casetype_v1_reference(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.refresh_casetype_v1_reference() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
  BEGIN
    REFRESH MATERIALIZED VIEW casetype_v1_reference;
    RETURN NULL;
  END
  $$;


--
-- Name: refresh_subject_position_matrix(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.refresh_subject_position_matrix() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    BEGIN
        REFRESH MATERIALIZED VIEW CONCURRENTLY subject_position_matrix;
        RETURN NULL;
    END
    $$;


--
-- Name: set_confidential_on_case(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.set_confidential_on_case() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
  BEGIN
    IF NEW.confidentiality IS NOT NULL
    THEN
      IF NEW.confidentiality = 'confidential'
      THEN
        NEW.confidential = true;
      ELSE
        NEW.confidential = false;
      END IF;
    END IF;
    RETURN NEW;
  END
  $$;


--
-- Name: set_leading_qitem(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.set_leading_qitem() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
  DECLARE
    counter int;
    case_uuid uuid;
    case_id int;
    json_no jsonb;
    json_uuid jsonb;
  BEGIN

    IF NEW.type = 'touch_case'
    THEN
      case_uuid := NEW.data::jsonb->>'case_object_id';
      case_id := NEW.data::jsonb->>'case_number';
      json_no := '{}'::jsonb;
      json_uuid := '{}'::jsonb;

      if case_id is not null
      then
        select into case_uuid uuid from zaak where id = case_id;
      else
        select into case_id id from zaak where uuid = case_uuid;
      end if;

      json_no   := json_build_object('case_number',    case_id::text);
      json_uuid := json_build_object('case_object_id', case_uuid);

      UPDATE queue set status = 'cancelled' WHERE type = 'touch_case'
        AND priority < NEW.priority
        AND status in ('pending', 'waiting')
        AND (
          data::jsonb @> json_no
          OR
          data::jsonb @> json_uuid
        );

      SELECT INTO counter count(id) FROM queue WHERE type = 'touch_case'
        AND priority >= NEW.priority
        AND status in ('pending', 'waiting')
        AND (
          data::jsonb @> json_no
          OR
          data::jsonb @> json_uuid
        );

      IF counter > 0
      THEN
        NEW.status := 'cancelled';
      END IF;

    END IF;

    return NEW;

  END $$;


--
-- Name: subject_company_json(public.hstore); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.subject_company_json(subject public.hstore, OUT json jsonb) RETURNS jsonb
    LANGUAGE plpgsql
    AS $$
  DECLARE

    address_cor json;
    address_res json;

    latitude float;
    longitude float;

  BEGIN

    SELECT INTO json subject_json(subject, 'company');

    latitude := ((subject->'vestiging_latlong')::point)[0];
    longitude := ((subject->'vestiging_latlong')::point)[1];

    IF (
        subject->'vestiging_adres_buitenland1' IS NOT NULL
        AND
        subject->'vestiging_adres_buitenland2' IS NOT NULL
        AND
        subject->'vestiging_adres_buitenland3' IS NOT NULL
        )
      OR (subject->'vestiging_straatnaam' IS NOT NULL
          AND subject->'vestiging_postcode' IS NOT NULL
      )
    THEN
      address_res := json_build_object(
        'preview', 'address(unsynched)',
        'reference', null,
        'type', 'address',
        'instance', json_build_object(
          'bag_id', (subject->'vestiging_bag_id')::bigint,
          'city', subject->'vestiging_woonplaats',
          'street', subject->'vestiging_straatnaam',
          'street_number', (subject->'vestiging_huisnummer')::int,
          'street_number_suffix', subject->'vestiging_huisnummertoevoeging',
          'street_number_letter', subject->'vestiging_huisletter',
          'foreign_address_line1', subject->'vestiging_adres_buitenland1',
          'foreign_address_line2', subject->'vestiging_adres_buitenland2',
          'foreign_address_line3', subject->'vestiging_adres_buitenland3',
          'country', (subject->'vestiging_country')::jsonb,
          'zipcode', subject->'vestiging_postcode',
          'latitude', latitude,
          'longitude', longitude,
          -- non-existent data
          'municipality', null,
          'date_created', timestamp_to_perl_datetime(NOW()),
          'date_modified', timestamp_to_perl_datetime(NOW())
        )
      );
    END IF;

    IF (
        subject->'correspondentie_adres_buitenland1' IS NOT NULL
        AND
        subject->'correspondentie_adres_buitenland2' IS NOT NULL
        AND
        subject->'correspondentie_adres_buitenland3' IS NOT NULL
        )
      OR (subject->'correspondentie_straatnaam' IS NOT NULL
          AND subject->'correspondentie_postcode' IS NOT NULL
      )
    THEN
      address_cor := json_build_object(
        'preview', 'address(unsynched)',
        'reference', null,
        'type', 'address',
        'instance', json_build_object(
          'bag_id', (subject->'correspondentie_bag_id')::bigint,
          'city', subject->'correspondentie_woonplaats',
          'street', subject->'correspondentie_straatnaam',
          'street_number', (subject->'correspondentie_huisnummer')::int,
          'street_number_suffix', subject->'correspondentie_huisnummertoevoeging',
          'street_number_letter', subject->'correspondentie_huisletter',
          'foreign_address_line1', subject->'correspondentie_adres_buitenland1',
          'foreign_address_line2', subject->'correspondentie_adres_buitenland2',
          'foreign_address_line3', subject->'correspondentie_adres_buitenland3',
          'zipcode', subject->'correspondentie_postcode',
          'country', (subject->'correspondentie_country')::jsonb,
          'latitude', null,
          'longitude', null,
          -- non-existent data
          'municipality', null,
          'date_created', timestamp_to_perl_datetime(NOW()),
          'date_modified', timestamp_to_perl_datetime(NOW())
        )
      );
    END IF;

    IF subject->'main_activity' = '{}'
    THEN
      SELECT INTO subject subject || hstore('main_activity', null);
    END IF;

    IF subject->'secondairy_activities' = '[]'
    THEN
      SELECT INTO subject subject || hstore('secondairy_activities', null);
    END IF;

    SELECT INTO json json || jsonb_build_object(
      'instance', json_build_object(
        'address_residence', address_res,
        'address_correspondence', address_cor,
        'email_address', subject->'email',
        'phone_number', subject->'phone_number',
        'mobile_phone_number', subject->'mobile',
        'coc_number', lpad(subject->'dossiernummer', 8, '0'),
        'coc_location_number', lpad(subject->'vestigingsnummer', 12, '0'),
        'date_ceased', (subject->'date_ceased')::date,
        'date_founded', (subject->'date_founded')::date,
        'date_registration', (subject->'date_registration')::date,
        'main_activity', (subject->'main_activity')::jsonb,
        'secondairy_activities', (subject->'secondairy_activities')::jsonb,
        'company_type', (subject->'company_type')::jsonb,
        'oin', (subject->'oin')::bigint,
        'rsin', (subject->'rsin')::bigint,
        'company', (subject->'handelsnaam')::text,
        -- non-existent data
        'date_created', timestamp_to_perl_datetime(NOW()),
        'date_modified', timestamp_to_perl_datetime(NOW())
      )
    );

    RETURN;
  END;
  $$;


--
-- Name: subject_employee_json(public.hstore); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.subject_employee_json(subject public.hstore, OUT json jsonb) RETURNS jsonb
    LANGUAGE plpgsql
    AS $$
  DECLARE
    properties jsonb;
    preview text;
    positions jsonb;

  BEGIN
      properties := (subject->'properties')::jsonb;

      SELECT INTO json subject_json(subject, 'employee');

      SELECT INTO positions position_matrix(
        COALESCE((subject->'group_ids')::int[], '{}'::int[]),
        COALESCE((subject->'role_ids')::int[], '{}'::int[])
      );

      SELECT INTO json json || jsonb_build_object(
        'instance', json_build_object(
          'username', subject->'username',
          'initials', properties->'initials',
          'first_names', properties->'givenname',
          'surname', properties->'sn',
          'display_name', properties->'displayname',
          'email_address', properties->'mail',
          'phone_number', properties->'telephonenumber',
          'settings', (subject->'settings')::jsonb,
          'positions', positions,
          'date_modified', timestamp_to_perl_datetime((subject->'last_modified')::timestamp with time zone),
          'date_created', timestamp_to_perl_datetime((subject->'last_modified')::timestamp with time zone)
        )
      );
      RETURN;
  END;
  $$;


--
-- Name: subject_json(public.hstore, text); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.subject_json(subject public.hstore, s_type text, OUT json jsonb) RETURNS jsonb
    LANGUAGE plpgsql
    AS $$
  DECLARE
    preview text;
  BEGIN
      -- uuid == 36 long, need last 6 chars, 31, 36
      SELECT INTO preview substring(subject->'uuid', 31, 36);

      SELECT INTO json jsonb_build_object(
        'reference', subject->'uuid',
        'preview', s_type || '(...' || preview || ')',
        'type', s_type
      );
      RETURN;
  END;
  $$;


--
-- Name: subject_person_json(public.hstore); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.subject_person_json(subject public.hstore, OUT json jsonb) RETURNS jsonb
    LANGUAGE plpgsql
    AS $$
  DECLARE

    address json;
    address_cor json;
    address_res json;

    partner json;
    surname text;

  BEGIN

    SELECT INTO json subject_json(subject, 'person');

    address := json_build_object(
      'preview', 'address(unsynched)',
      'reference', null,
      'type', 'address',
      'instance', json_build_object(
        'bag_id', subject->'bag_id',
        'city', subject->'city',
        'street', subject->'street',
        'street_number', (subject->'street_number')::int,
        'street_number_suffix', subject->'street_number_suffix',
        'street_number_letter', subject->'street_number_letter',
        'foreign_address_line1', subject->'foreign_address_line1',
        'foreign_address_line2', subject->'foreign_address_line2',
        'foreign_address_line3', subject->'foreign_address_line3',
        'zipcode', subject->'zipcode',
        'country', (subject->'country')::jsonb,
        'municipality', (subject->'municipality')::jsonb,
        'latitude', (subject->'latitude')::float,
        'longitude', (subject->'longitude')::float,
        'date_created', timestamp_to_perl_datetime(NOW()),
        'date_modified', timestamp_to_perl_datetime(NOW())
      )
    );

    IF subject->'address_type' = 'W' THEN
          address_res := address;
    ELSE
          address_cor := address;
    END IF;

    IF   subject->'partner_a_nummer' IS NOT NULL
      OR subject->'partner_burgerservicenummer' IS NOT NULL
      OR subject->'partner_geslachtsnaam' IS NOT NULL
      OR subject->'partner_voorvoegsel' IS NOT NULL
    THEN

      IF subject->'partner_voorvoegsel' IS NOT NULL
      THEN
        surname := CONCAT(subject->'partner_voorvoegsel', ' ', subject->'partner_geslachtsnaam');
      ELSE
        surname := subject->'partner_geslachtsnaam';
      END IF;

      SELECT INTO partner json_build_object(
        'instance', json_build_object(
          'surname', surname,
          'family_name' , subject->'partner_geslachtsnaam',
          'prefix', subject->'partner_voorvoegsel',
          'personal_number', lpad(subject->'partner_burgerservicenummer', 9, '0'),
          'personal_number_a', lpad(subject->'partner_a_nummer', 10, '0'),
          -- We have no information on these items
          'first_names' , null,
          'use_of_name', null,
          'is_local_resident', false,
          'gender', null,
          'noble_title', null,
          'place_of_birth', null,
          'initials', null,
          'date_of_birth', null,
          'is_secret', false,
          'email_address', null,
          'mobile_phone_number', null,
          'address_correspondence', null,
          'address_residence', null,
          'date_of_death', null,
          'phone_number', null,
          'partner', null,
          'date_created', timestamp_to_perl_datetime(NOW()),
          'date_modified', timestamp_to_perl_datetime(NOW())
        ),
        'type', 'person',
        'preview', 'person(unsynched)',
        'reference', null
      );
    END IF;

    SELECT INTO json json || jsonb_build_object(
      'instance', json_build_object(
        'address_residence', address_res,
        'address_correspondence', address_cor,
        'email_address', subject->'email_address',
        'phone_number', subject->'phone_number',
        'mobile_phone_number', subject->'mobile',
        'partner', partner,
        -- non-existent data
        'date_created', timestamp_to_perl_datetime(NOW()), -- np table?
        'date_modified', timestamp_to_perl_datetime(NOW()), -- np table?
        -- we have it
        'date_of_birth', (subject->'geboortedatum')::date,
        'date_of_death', (subject->'datum_overlijden')::date,
        'family_name', subject->'geslachtsnaam',
        'first_names', subject->'voornamen',
        'gender', subject->'geslachtsaanduiding',
        'initials', subject->'voorletters',
        'is_local_resident', (subject->'in_gemeente')::boolean,
        'is_secret', CASE WHEN subject->'indicatie_geheim' = '0' THEN false ELSE true END,
        'noble_title', subject->'adellijke_titel',
        'personal_number', lpad(subject->'burgerservicenummer', 9, '0'),
        'personal_number_a', lpad(subject->'a_nummer', 10, '0'),
        'place_of_birth', subject->'geboorteplaats',
        'prefix', subject->'aanhef_aanschrijving',
        'surname', COALESCE(subject->'naamgebruik', subject->'geslachtsnaam'),
        'use_of_name', subject->'aanduiding_naamgebruik'
      )
    );

    RETURN;
  END;
  $$;


--
-- Name: sync_zaak_betrokkenen_cache(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.sync_zaak_betrokkenen_cache() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
    affected RECORD;
BEGIN
    IF TG_OP = 'INSERT' OR TG_OP = 'UPDATE' THEN
        affected := NEW;
    ELSIF TG_OP = 'DELETE' THEN
        affected := OLD;
    END IF;

    IF affected.zaak_id IS NULL THEN
        -- Betrokkene without a case. Weird situation.
        RETURN affected;
    END IF;

    UPDATE
        zaak
    SET
        betrokkenen_cache = q.betrokkenen
    FROM (
        SELECT
            z.id AS zaak_id,
            json_object_agg(r.uuid, jsonb_build_object('roles', r.roles, 'pip_authorized', r.authorized)) AS betrokkenen
        FROM
            zaak z,
            (
                SELECT
                    zb.zaak_id,
                    zb.subject_id AS uuid,
                    json_object_agg(zb.id, jsonb_build_object('pip_authorized', zb.pip_authorized, 'authorisation', zb.authorisation, 'role', coalesce(zb.rol, CASE WHEN zb.id = z.aanvrager THEN
                                    'Aanvrager'
                                WHEN zb.id = z.behandelaar THEN
                                    'Behandelaar'
                                WHEN zb.id = z.coordinator THEN
                                    'Coordinator'
                                ELSE
                                    'Onbekende rol'
                                END))) AS roles,
                    sum(
                        CASE WHEN zb.pip_authorized THEN
                            1
                        ELSE
                            0
                        END)::int::boolean AS authorized
                FROM
                    zaak_betrokkenen zb
                    JOIN zaak z ON zb.zaak_id = z.id
                WHERE
                    zb.deleted IS NULL
                GROUP BY
                    zb.subject_id,
                    zb.zaak_id) AS r
            WHERE
                r.zaak_id = z.id
                AND r.uuid IS NOT NULL
            GROUP BY
                z.id) AS q
        WHERE (q.zaak_id = zaak.id AND zaak.id = affected.zaak_id);
    RETURN affected;
END;
$$;


--
-- Name: timestamp_to_perl_datetime(timestamp with time zone); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.timestamp_to_perl_datetime(dt timestamp with time zone, OUT tt text) RETURNS text
    LANGUAGE plpgsql
    AS $$
  BEGIN
    SELECT INTO tt to_char(dt::timestamp with time zone at time zone 'UTC', 'YYYY-MM-DD"T"HH24:MI:SS"Z"');
  END;
  $$;


--
-- Name: update_aanvrager_type(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.update_aanvrager_type() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
    IF NEW.aanvrager IS NOT NULL THEN
        SELECT
            betrokkene_type
        FROM
            zaak_betrokkenen
        WHERE
            id = NEW.aanvrager INTO NEW.aanvrager_type;
    ELSE
        NEW.aanvrager_type := NULL;
    END IF;
    RETURN NEW;
END;
$$;


--
-- Name: update_display_name_for_file(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.update_display_name_for_file() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
  DECLARE
    subject_uuid uuid;
    dn text;
  BEGIN
      IF NEW.created_by IS NOT NULL
      THEN

        IF NEW.created_by NOT LIKE 'betrokkene-%'
        THEN
            NEW.created_by_display_name := NEW.created_by;
            RETURN NEW;
        END IF;

        SELECT INTO subject_uuid get_subject_by_legacy_id(NEW.created_by);

        IF subject_uuid IS NULL
        THEN
            NEW.created_by_display_name := '<Onbekend>';
            RETURN NEW;
        END IF;

        SELECT INTO dn get_subject_display_name_by_uuid(subject_uuid);
        IF dn IS NOT NULL
        THEN
            NEW.created_by_display_name := dn;
          RETURN NEW;
        END IF;
      END IF;

      RETURN NEW;
  END $$;


--
-- Name: update_file_timestamps(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.update_file_timestamps() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
        BEGIN
            NEW.date_modified = NOW() AT TIME ZONE 'UTC';
            RETURN NEW;
        END;
    $$;


--
-- Name: update_group_json(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.update_group_json() RETURNS trigger
    LANGUAGE plpgsql
    AS $$

    DECLARE
      j_group JSONB;
    BEGIN

      SELECT INTO j_group org_unit(hstore(NEW), 'group');
      NEW.v1_json := j_group;
      RETURN NEW;
    END;
  $$;


--
-- Name: update_name_cache_for_logging(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.update_name_cache_for_logging() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
  DECLARE
    subject_uuid uuid;
    dn text;
  BEGIN
      IF NEW.created_by IS NOT NULL AND NEW.created_by_name_cache IS NULL
      THEN

        IF NEW.created_by NOT LIKE 'betrokkene-%'
        THEN
            NEW.created_by_name_cache := NEW.created_by;
            NEW.created_by := NULL;
            RETURN NEW;
        END IF;

        SELECT INTO subject_uuid get_subject_by_legacy_id(NEW.created_by);

        IF subject_uuid IS NULL
        THEN
            NEW.created_by_name_cache := '<onbekend>';
            RETURN NEW;
        END IF;

        SELECT INTO dn get_subject_display_name_by_uuid(subject_uuid);
        IF dn IS NOT NULL
        THEN
            NEW.created_by_name_cache := dn;
          RETURN NEW;
        END IF;
      END IF;

      RETURN NEW;
  END $$;


--
-- Name: update_requestor_json(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.update_requestor_json() RETURNS trigger
    LANGUAGE plpgsql
    AS $$

    BEGIN

      IF TG_OP = 'INSERT' OR (TG_OP = 'UPDATE' AND NEW.aanvrager != OLD.aanvrager) THEN
        SELECT INTO NEW.requestor_v1_json v1_json FROM zaak_betrokkenen zb WHERE zb.id = NEW.aanvrager AND zb.zaak_id = NEW.id;
      END IF;

      IF TG_OP = 'INSERT' OR (TG_OP = 'UPDATE' AND NEW.behandelaar != OLD.behandelaar) THEN

        IF NEW.behandelaar IS NOT NULL
        THEN
          SELECT INTO NEW.assignee_v1_json v1_json FROM zaak_betrokkenen zb WHERE zb.id = NEW.behandelaar AND zb.zaak_id = NEW.id;
        ELSE
          NEW.assignee_v1_json := '{}';
        END IF;
      END IF;

      IF TG_OP = 'INSERT' OR (TG_OP = 'UPDATE' AND NEW.coordinator != OLD.coordinator) THEN

        IF NEW.coordinator IS NOT NULL
        THEN
          SELECT INTO NEW.coordinator_v1_json v1_json FROM zaak_betrokkenen zb WHERE zb.id = NEW.coordinator AND zb.zaak_id = NEW.id;
        ELSE
          NEW.coordinator_v1_json := '{}';
        END IF;
      END IF;
      RETURN NEW;

    END;

  $$;


--
-- Name: update_role_json(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.update_role_json() RETURNS trigger
    LANGUAGE plpgsql
    AS $$

    DECLARE
      j_role JSONB;
    BEGIN

      SELECT INTO j_role org_unit(hstore(NEW), 'role');
      NEW.v1_json := j_role;
      RETURN NEW;
    END;
  $$;


--
-- Name: update_subject_json(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.update_subject_json() RETURNS trigger
    LANGUAGE plpgsql
    AS $$

    BEGIN

      IF TG_OP = 'INSERT'
      THEN
        SELECT INTO
          NEW.requestor_v1_json
          case_subject_json(hstore(zb))
        FROM
          zaak_betrokkenen zb
        WHERE
          -- zb does not have the zaak id yet on create..
          zb.id = NEW.aanvrager;

      ELSIF TG_OP = 'UPDATE' AND (NEW.requestor_v1_json IS NULL OR NEW.aanvrager != OLD.aanvrager)
      THEN
        SELECT INTO
          NEW.requestor_v1_json
          case_subject_json(hstore(zb))
        FROM
          zaak_betrokkenen zb
        WHERE
          -- zb does not have the zaak id yet on update, this is handled
          -- later on..
           zb.id = NEW.aanvrager;

      END IF;

      IF NEW.behandelaar IS NOT NULL
      THEN
        IF TG_OP = 'INSERT' OR (TG_OP = 'UPDATE' AND ( OLD.behandelaar IS NULL OR NEW.behandelaar != OLD.behandelaar))
        THEN
          SELECT INTO
            NEW.assignee_v1_json
            case_subject_json(hstore(zb))
          FROM
            zaak_betrokkenen zb
          WHERE
            zb.id = NEW.behandelaar AND zb.zaak_id = NEW.id;
        END IF;
      ELSE
        NEW.assignee_v1_json := NULL;
      END IF;


      IF NEW.coordinator IS NOT NULL
      THEN
        IF TG_OP = 'INSERT' OR (TG_OP = 'UPDATE' AND ( OLD.coordinator IS NULL OR NEW.coordinator != OLD.coordinator))
        THEN
          SELECT INTO
            NEW.coordinator_v1_json
            case_subject_json(hstore(zb))
          FROM
            zaak_betrokkenen zb
          WHERE
            zb.id = NEW.coordinator AND zb.zaak_id = NEW.id;
        END IF;
      ELSE
        NEW.coordinator_v1_json := NULL;
      END IF;

      RETURN NEW;

    END;

  $$;


--
-- Name: update_timestamps(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.update_timestamps() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
        BEGIN
            IF NEW.last_modified is NULL
            THEN
                NEW.last_modified = NOW();
            END IF;
            RETURN NEW;
        END;
    $$;


--
-- Name: update_zaak_percentage(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.update_zaak_percentage() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
  BEGIN

    SELECT INTO NEW.status_percentage get_case_status_perc(
      NEW.milestone,
      NEW.zaaktype_node_id
    );
    RETURN NEW;

  END
  $$;


--
-- Name: zaak_meta_update_unread_count(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.zaak_meta_update_unread_count() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    BEGIN
        UPDATE zaak_meta SET unread_communication_count = (SELECT GREATEST(SUM(unread_employee_count), 0) FROM thread WHERE case_id = NEW.case_id) WHERE zaak_id = NEW.case_id;

        RETURN NEW;
    END
$$;


--
-- Name: zaaktype_node_as_v0(public.hstore); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.zaaktype_node_as_v0(ztn public.hstore, OUT unit_json jsonb) RETURNS jsonb
    LANGUAGE plpgsql
    AS $$
  DECLARE
    properties JSONB;
  BEGIN

    properties := (ztn->'properties')::jsonb;

    SELECT INTO unit_json json_build_object(
      -- properties mapping
      'case.casetype.adjourn_period', properties->'verdagingstermijn',
      'case.casetype.archive_classification_code', properties->'archiefclassicatiecode',
      'case.casetype.designation_of_confidentiality', properties->'vertrouwelijkheidsaanduiding',
      'case.casetype.eform', properties->'e_formulier',
      'case.casetype.extension', properties->'verlenging_mogelijk',
      'case.casetype.extension_period', properties->'verlengingstermijn',
      'case.casetype.goal', properties->'doel',
      'case.casetype.lex_silencio_positivo', properties->'lex_silencio_positivo',
      'case.casetype.motivation', properties->'aanleiding',
      'case.casetype.objection_and_appeal', properties->'beroep_mogelijk',
      'case.casetype.penalty', properties->'wet_dwangsom',
      'case.casetype.price.counter', properties->'pdc_tarief_balie',
      'case.casetype.price.email', properties->'pdc_tarief_email',
      'case.casetype.price.employee', properties->'pdc_tarief_behandelaar',
      'case.casetype.price.post', properties->'pdc_tarief_post',
      'case.casetype.price.telephone', properties->'pdc_tarief_telefoon',
      'case.casetype.principle_local', properties->'lokale_grondslag',
      'case.casetype.publication', properties->'publicatie',
      'case.casetype.registration_bag', properties->'bag',
      'case.casetype.supervisor', properties->'verantwoordelijke',
      'case.casetype.supervisor_relation', properties->'verantwoordingsrelatie',
      'case.casetype.suspension', properties->'opschorten_mogelijk',
      'case.casetype.text_for_publication', properties->'publicatietekst',
      'case.casetype.wkpb', properties->'wkpb',
      -- ztn itself
      'case.casetype.version', (ztn->'version')::int,
      'case.casetype.description', ztn->'zaaktype_omschrijving',
      'case.casetype.name', ztn->'titel',
      'case.casetype.node.id', (ztn->'id')::int,
      'case.casetype.id', (ztn->'zaaktype_id')::int,
      'case.casetype.keywords', ztn->'zaaktype_trefwoorden',
      'case.casetype.identification', ztn->'code',
      'case.casetype.initiator_source', ztn->'trigger',
      'case.casetype.version_date_of_creation', (ztn->'created')::timestamp with time zone,
      -- how is this even possible?
      'case.casetype.version_date_of_expiration', (ztn->'deleted')::timestamp with time zone
    );

    RETURN;
  END;
  $$;


--
-- Name: zaaktype_node_v0_json(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.zaaktype_node_v0_json() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    BEGIN
      SELECT INTO NEW.v0_json zaaktype_node_as_v0(hstore(NEW));
      RETURN NEW;
    END;
  $$;


SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: adres; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.adres (
    id integer NOT NULL,
    straatnaam text,
    huisnummer bigint,
    huisletter character(1),
    huisnummertoevoeging text,
    nadere_aanduiding character varying(35),
    postcode character varying(6),
    woonplaats text,
    gemeentedeel text,
    functie_adres character(1) NOT NULL,
    datum_aanvang_bewoning date,
    woonplaats_id character varying(32),
    gemeente_code smallint,
    hash character varying(32),
    import_datum timestamp(6) without time zone,
    deleted_on timestamp(6) without time zone,
    adres_buitenland1 text,
    adres_buitenland2 text,
    adres_buitenland3 text,
    landcode integer DEFAULT 6030,
    natuurlijk_persoon_id integer,
    bag_id text,
    geo_lat_long point
);


--
-- Name: adres_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.adres_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: adres_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.adres_id_seq OWNED BY public.adres.id;


--
-- Name: alternative_authentication_activation_link; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.alternative_authentication_activation_link (
    token text NOT NULL,
    subject_id uuid NOT NULL,
    expires timestamp without time zone NOT NULL
);


--
-- Name: bag_cache; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.bag_cache (
    id integer NOT NULL,
    bag_type character varying NOT NULL,
    bag_id character(16) NOT NULL,
    bag_data jsonb NOT NULL,
    date_created timestamp without time zone DEFAULT timezone('UTC'::text, CURRENT_TIMESTAMP) NOT NULL,
    last_modified timestamp without time zone DEFAULT timezone('UTC'::text, CURRENT_TIMESTAMP) NOT NULL,
    refresh_after timestamp without time zone DEFAULT (timezone('UTC'::text, CURRENT_TIMESTAMP) + '24:00:00'::interval) NOT NULL,
    CONSTRAINT bag_cache_bag_type_check CHECK (((bag_type)::text = ANY (ARRAY[('ligplaats'::character varying)::text, ('nummeraanduiding'::character varying)::text, ('openbareruimte'::character varying)::text, ('pand'::character varying)::text, ('standplaats'::character varying)::text, ('verblijfsobject'::character varying)::text, ('woonplaats'::character varying)::text])))
);


--
-- Name: bag_cache_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

ALTER TABLE public.bag_cache ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.bag_cache_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: bag_ligplaats; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.bag_ligplaats (
    identificatie character varying(16) NOT NULL,
    begindatum character varying(14) NOT NULL,
    einddatum character varying(14),
    officieel character varying(1),
    status character varying(80) NOT NULL,
    hoofdadres character varying(16) NOT NULL,
    inonderzoek character varying(1) NOT NULL,
    documentdatum character varying(14),
    documentnummer character varying(20),
    correctie character varying(1),
    id integer NOT NULL
);


--
-- Name: TABLE bag_ligplaats; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.bag_ligplaats IS '55 : een ligplaats is een formeel door de gemeenteraad als zodanig aangewezen plaats in het water, al dan niet aangevuld met een op de oever aanwezig terrein of een gedeelte daarvan, dat bestemd is voor het permanent afmeren van een voor woon-, bedrijfsmatige- of recreatieve doeleinden geschikt vaartuig.';


--
-- Name: COLUMN bag_ligplaats.identificatie; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_ligplaats.identificatie IS '58.01 : de unieke aanduiding van een ligplaats.';


--
-- Name: COLUMN bag_ligplaats.begindatum; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_ligplaats.begindatum IS '58.91 : de begindatum van een periode waarin een of meer gegevens die worden bijgehouden over een ligplaats een wijziging hebben ondergaan.';


--
-- Name: COLUMN bag_ligplaats.einddatum; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_ligplaats.einddatum IS '58.92 : de einddatum van een periode waarin er geen wijzigingen hebben plaatsgevonden in de gegevens die worden bijgehouden over een ligplaats.';


--
-- Name: COLUMN bag_ligplaats.officieel; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_ligplaats.officieel IS '58.02 : een aanduiding waarmee kan worden aangegeven dat een object in de registratie is opgenomen als gevolg van een feitelijke constatering, zonder dat er op het moment van opname sprake is van een formele grondslag voor deze opname.';


--
-- Name: COLUMN bag_ligplaats.status; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_ligplaats.status IS '58.03 : de fase van de levenscyclus van een ligplaats, waarin de betreffende ligplaats zich bevindt.';


--
-- Name: COLUMN bag_ligplaats.hoofdadres; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_ligplaats.hoofdadres IS '58:10 : de identificatiecode nummeraanduiding waaronder het hoofdadres van een ligplaats, dat in het kader van de basis gebouwen registratie als zodanig is aangemerkt, is opgenomen in de basis registratie adressen.';


--
-- Name: COLUMN bag_ligplaats.inonderzoek; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_ligplaats.inonderzoek IS '58.93 : een aanduiding waarmee wordt aangegeven dat een onderzoek wordt uitgevoerd naar de juistheid van een of meerdere gegevens van het betreffende object.';


--
-- Name: COLUMN bag_ligplaats.documentdatum; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_ligplaats.documentdatum IS '58.97 : de datum waarop het brondocument is vastgesteld, op basis waarvan een opname, mutatie of een in de historie plaatsen van gegevens ten aanzien van een ligplaats heeft plaatsgevonden.';


--
-- Name: COLUMN bag_ligplaats.documentnummer; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_ligplaats.documentnummer IS '58.98 : de unieke aanduiding van het brondocument op basis waarvan een opname, mutatie of een verwijdering van gegevens ten aanzien van een ligplaats heeft plaatsgevonden, binnen een gemeente.';


--
-- Name: COLUMN bag_ligplaats.correctie; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_ligplaats.correctie IS 'het gegeven is gecorrigeerd.';


--
-- Name: bag_ligplaats_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.bag_ligplaats_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: bag_ligplaats_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.bag_ligplaats_id_seq OWNED BY public.bag_ligplaats.id;


--
-- Name: bag_ligplaats_nevenadres; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.bag_ligplaats_nevenadres (
    identificatie character varying(16) NOT NULL,
    begindatum character varying(14) NOT NULL,
    nevenadres character varying(16) NOT NULL,
    correctie character varying(1) NOT NULL
);


--
-- Name: TABLE bag_ligplaats_nevenadres; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.bag_ligplaats_nevenadres IS 'koppeltabel voor nevenadressen bij ligplaats';


--
-- Name: COLUMN bag_ligplaats_nevenadres.identificatie; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_ligplaats_nevenadres.identificatie IS '58.01 : de unieke aanduiding van een ligplaats.';


--
-- Name: COLUMN bag_ligplaats_nevenadres.begindatum; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_ligplaats_nevenadres.begindatum IS '58.91 : de begindatum van een periode waarin een of meer gegevens die worden bijgehouden over een ligplaats een wijziging hebben ondergaan.';


--
-- Name: COLUMN bag_ligplaats_nevenadres.nevenadres; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_ligplaats_nevenadres.nevenadres IS '58.11 : de identificatiecodes nummeraanduiding waaronder nevenadressen van een ligplaats, die in het kader van de basis gebouwen registratie als zodanig zijn aangemerkt, zijn opgenomen in de basis registratie adressen.';


--
-- Name: COLUMN bag_ligplaats_nevenadres.correctie; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_ligplaats_nevenadres.correctie IS 'het gegeven is gecorrigeerd.';


--
-- Name: bag_nummeraanduiding; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.bag_nummeraanduiding (
    identificatie character varying(16) NOT NULL,
    begindatum character varying(14) NOT NULL,
    einddatum character varying(14),
    huisnummer bigint NOT NULL,
    officieel character varying(1),
    huisletter character varying(1),
    huisnummertoevoeging text,
    postcode character varying(6),
    woonplaats character varying(4),
    inonderzoek character varying(1) NOT NULL,
    openbareruimte character varying(16) NOT NULL,
    type character varying(20) NOT NULL,
    documentdatum character varying(14),
    documentnummer character varying(20),
    status character varying(80) NOT NULL,
    correctie character varying(1),
    id integer NOT NULL,
    gps_lat_lon point
);


--
-- Name: TABLE bag_nummeraanduiding; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.bag_nummeraanduiding IS '11.2 : een nummeraanduiding is een door de gemeenteraad als zodanig toegekende aanduiding van een adresseerbaar object.';


--
-- Name: COLUMN bag_nummeraanduiding.identificatie; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_nummeraanduiding.identificatie IS '11.02 : de unieke aanduiding van een nummeraanduiding.';


--
-- Name: COLUMN bag_nummeraanduiding.begindatum; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_nummeraanduiding.begindatum IS '11.62 : de begindatum van een periode waarin een of meer gegevens die worden bijgehouden over een nummeraanduiding een wijziging hebben ondergaan.';


--
-- Name: COLUMN bag_nummeraanduiding.einddatum; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_nummeraanduiding.einddatum IS '11.63 : de einddatum van een periode waarin er geen wijzigingen hebben plaatsgevonden in de gegevens die worden bijgehouden over een nummeraanduiding.';


--
-- Name: COLUMN bag_nummeraanduiding.huisnummer; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_nummeraanduiding.huisnummer IS '11.20 : een door of namens het gemeentebestuur ten aanzien van een adresseerbaar object toegekende nummering.';


--
-- Name: COLUMN bag_nummeraanduiding.officieel; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_nummeraanduiding.officieel IS '11.21 : een aanduiding waarmee kan worden aangegeven dat een object in de registratie is opgenomen als gevolg van een feitelijke constatering, zonder dat er op het moment van opname sprake is van een formele grondslag voor deze opname.';


--
-- Name: COLUMN bag_nummeraanduiding.huisletter; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_nummeraanduiding.huisletter IS '11.30 : een door of namens het gemeentebestuur ten aanzien van  een adresseerbaar object toegekende toevoeging aan een huisnummer in de vorm van een alfanumeriek teken.';


--
-- Name: COLUMN bag_nummeraanduiding.huisnummertoevoeging; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_nummeraanduiding.huisnummertoevoeging IS '11.40 : een door of namens het gemeentebestuur ten aanzien van  een adresseerbaar object toegekende nadere toevoeging aan een huisnummer of een combinatie van huisnummer en huisletter.';


--
-- Name: COLUMN bag_nummeraanduiding.postcode; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_nummeraanduiding.postcode IS '11.60 : de door tnt post vastgestelde code behorende bij een bepaalde combinatie van een straatnaam en een huisnummer.';


--
-- Name: COLUMN bag_nummeraanduiding.woonplaats; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_nummeraanduiding.woonplaats IS '11.61 : unieke aanduiding van de woonplaats waarbinnen het object waaraan de nummeraanduiding is toegekend is gelegen.';


--
-- Name: COLUMN bag_nummeraanduiding.inonderzoek; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_nummeraanduiding.inonderzoek IS '11.64 : een aanduiding waarmee wordt aangegeven dat een onderzoek wordt uitgevoerd naar de juistheid van een of meerdere gegevens van het betreffende object.';


--
-- Name: COLUMN bag_nummeraanduiding.openbareruimte; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_nummeraanduiding.openbareruimte IS '11.65 : de unieke aanduiding van een openbare ruimte waaraan een adresseerbaar object is gelegen.';


--
-- Name: COLUMN bag_nummeraanduiding.type; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_nummeraanduiding.type IS '11.66 : de aard van een als zodanig benoemde nummeraanduiding.';


--
-- Name: COLUMN bag_nummeraanduiding.documentdatum; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_nummeraanduiding.documentdatum IS '11.67 : de datum waarop het brondocument is vastgesteld, op basis waarvan een opname, mutatie of een verwijdering van gegevens ten aanzien van een nummeraanduiding heeft plaatsgevonden.';


--
-- Name: COLUMN bag_nummeraanduiding.documentnummer; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_nummeraanduiding.documentnummer IS '11.68 : de unieke aanduiding van het brondocument op basis waarvan een opname, mutatie of een verwijdering van gegevens ten aanzien van een nummeraanduiding heeft plaatsgevonden, binnen een gemeente.';


--
-- Name: COLUMN bag_nummeraanduiding.status; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_nummeraanduiding.status IS '11.69 : de fase van de levenscyclus van een nummeraanduiding, waarin de betreffende nummeraanduiding zich bevindt.';


--
-- Name: COLUMN bag_nummeraanduiding.correctie; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_nummeraanduiding.correctie IS 'het gegeven is gecorrigeerd.';


--
-- Name: bag_nummeraanduiding_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.bag_nummeraanduiding_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: bag_nummeraanduiding_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.bag_nummeraanduiding_id_seq OWNED BY public.bag_nummeraanduiding.id;


--
-- Name: bag_openbareruimte; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.bag_openbareruimte (
    identificatie character varying(16) NOT NULL,
    begindatum character varying(14) NOT NULL,
    einddatum character varying(14),
    naam character varying(80) NOT NULL,
    officieel character varying(1),
    woonplaats character varying(4),
    type character varying(40) NOT NULL,
    inonderzoek character varying(1) NOT NULL,
    documentdatum character varying(14),
    documentnummer character varying(20),
    status character varying(80) NOT NULL,
    correctie character varying(1),
    id integer NOT NULL,
    gps_lat_lon point
);


--
-- Name: TABLE bag_openbareruimte; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.bag_openbareruimte IS '11.1 : een openbare ruimte is een door de gemeenteraad als zodanig aangewezen benaming van een binnen een woonplaats gelegen buitenruimte.';


--
-- Name: COLUMN bag_openbareruimte.identificatie; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_openbareruimte.identificatie IS '11.01 : de unieke aanduiding van een openbare ruimte.';


--
-- Name: COLUMN bag_openbareruimte.begindatum; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_openbareruimte.begindatum IS '11.12 : de begindatum van een periode waarin een of meer gegevens die worden bijgehouden over een openbare ruimte een wijziging hebben ondergaan.';


--
-- Name: COLUMN bag_openbareruimte.einddatum; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_openbareruimte.einddatum IS '11.13 : de einddatum van een periode waarin er geen wijzigingen hebben plaatsgevonden in de gegevens die worden bijgehouden over een openbare ruimte.';


--
-- Name: COLUMN bag_openbareruimte.naam; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_openbareruimte.naam IS '11.10 : een naam die aan een openbare ruimte is toegekend in een daartoe strekkend formeel gemeentelijk besluit.';


--
-- Name: COLUMN bag_openbareruimte.officieel; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_openbareruimte.officieel IS '11.11 : een aanduiding waarmee kan worden aangegeven dat een object in de registratie is opgenomen als gevolg van een feitelijke constatering, zonder dat er op het moment van opname sprake is van een formele grondslag voor deze opname.';


--
-- Name: COLUMN bag_openbareruimte.woonplaats; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_openbareruimte.woonplaats IS '11.15 : unieke aanduiding van de woonplaats waarbinnen een openbare ruimte is gelegen.';


--
-- Name: COLUMN bag_openbareruimte.type; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_openbareruimte.type IS '11.16 : de aard van de als zodanig benoemde openbare ruimte.';


--
-- Name: COLUMN bag_openbareruimte.inonderzoek; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_openbareruimte.inonderzoek IS '11.14 : een aanduiding waarmee wordt aangegeven dat een onderzoek wordt uitgevoerd naar de juistheid van een of meerdere gegevens van het betreffende object.';


--
-- Name: COLUMN bag_openbareruimte.documentdatum; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_openbareruimte.documentdatum IS '11.17 : de datum waarop het brondocument is vastgesteld, op basis waarvan een opname, mutatie of een verwijdering van gegevens ten aanzien van een openbare ruimte heeft plaatsgevonden.';


--
-- Name: COLUMN bag_openbareruimte.documentnummer; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_openbareruimte.documentnummer IS '11.18 : de unieke aanduiding van het brondocument op basis waarvan een opname, mutatie of een verwijdering van gegevens ten aanzien van een openbare ruimte heeft plaatsgevonden, binnen een gemeente.';


--
-- Name: COLUMN bag_openbareruimte.status; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_openbareruimte.status IS '11.19 : de fase van de levenscyclus van een openbare ruimte, waarin de betreffende openbare ruimte zich bevindt.';


--
-- Name: COLUMN bag_openbareruimte.correctie; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_openbareruimte.correctie IS 'het gegeven is gecorrigeerd.';


--
-- Name: bag_openbareruimte_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.bag_openbareruimte_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: bag_openbareruimte_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.bag_openbareruimte_id_seq OWNED BY public.bag_openbareruimte.id;


--
-- Name: bag_pand; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.bag_pand (
    identificatie character varying(16) NOT NULL,
    begindatum character varying(14) NOT NULL,
    einddatum character varying(14),
    officieel character varying(1),
    bouwjaar integer,
    status character varying(80) NOT NULL,
    inonderzoek character varying(1) NOT NULL,
    documentdatum character varying(14),
    documentnummer character varying(20),
    correctie character varying(1),
    id integer NOT NULL
);


--
-- Name: TABLE bag_pand; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.bag_pand IS '55 : een pand is de kleinste, bij de totstandkoming functioneel en bouwkundig constructief zelfstandige eenheid, die direct en duurzaam met de aarde is verbonden.';


--
-- Name: COLUMN bag_pand.identificatie; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_pand.identificatie IS '55.01 : de unieke aanduiding van een pand';


--
-- Name: COLUMN bag_pand.begindatum; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_pand.begindatum IS '55.91 : de begindatum van een periode waarin een of meer gegevens die worden bijgehouden over een pand een wijziging hebben ondergaan.';


--
-- Name: COLUMN bag_pand.einddatum; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_pand.einddatum IS '55.92 : de einddatum van een periode waarin er geen wijzigingen hebben plaatsgevonden in de gegevens die worden bijgehouden over een pand.';


--
-- Name: COLUMN bag_pand.officieel; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_pand.officieel IS '55.02 : een aanduiding waarmee kan worden aangegeven dat een object in de registratie is opgenomen als gevolg van een feitelijke constatering, zonder dat er op het moment van opname sprake is van een formele grondslag voor deze opname';


--
-- Name: COLUMN bag_pand.bouwjaar; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_pand.bouwjaar IS '55.30 : de aanduiding van het jaar waarin een pand oorspronkelijk als bouwkundig gereed is opgeleverd.';


--
-- Name: COLUMN bag_pand.status; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_pand.status IS '55.31 : de fase van de levenscyclus van een pand, waarin het betreffende pand zich bevindt.';


--
-- Name: COLUMN bag_pand.inonderzoek; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_pand.inonderzoek IS '55.93 : een aanduiding waarmee wordt aangegeven dat een onderzoek wordt uitgevoerd naar de juistheid van een of meerdere gegevens van het betreffende object.';


--
-- Name: COLUMN bag_pand.documentdatum; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_pand.documentdatum IS '55.97 : de datum waarop het brondocument is vastgesteld, op basis waarvan een opname, mutatie of een verwijdering van gegevens ten aanzien van een pand heeft plaatsgevonden.';


--
-- Name: COLUMN bag_pand.documentnummer; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_pand.documentnummer IS '55.98 : de unieke aanduiding van het brondocument op basis waarvan een opname, mutatie of een verwijdering van gegevens ten aanzien van een pand heeft plaatsgevonden, binnen een gemeente.';


--
-- Name: COLUMN bag_pand.correctie; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_pand.correctie IS 'het gegeven is gecorrigeerd.';


--
-- Name: bag_pand_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.bag_pand_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: bag_pand_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.bag_pand_id_seq OWNED BY public.bag_pand.id;


--
-- Name: bag_standplaats; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.bag_standplaats (
    identificatie character varying(16) NOT NULL,
    begindatum character varying(14) NOT NULL,
    einddatum character varying(14),
    officieel character varying(1),
    status character varying(80) NOT NULL,
    hoofdadres character varying(16) NOT NULL,
    inonderzoek character varying(1) NOT NULL,
    documentdatum character varying(14),
    documentnummer character varying(20),
    correctie character varying(1),
    id integer NOT NULL
);


--
-- Name: TABLE bag_standplaats; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.bag_standplaats IS '57 : een standplaats is een formeel door de gemeenteraad als zodanig aangewezen terrein of een gedeelte daarvan, dat bestemd is voor het permanent plaatsen van een niet direct en duurzaam met de aarde verbonden en voor woon -, bedrijfsmatige - of recreatieve doeleinden geschikte ruimte.';


--
-- Name: COLUMN bag_standplaats.identificatie; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_standplaats.identificatie IS '57.01 : de unieke aanduiding van een standplaats.';


--
-- Name: COLUMN bag_standplaats.begindatum; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_standplaats.begindatum IS '57.91 : de begindatum van een periode waarin een of meer gegevens die worden bijgehouden over een standplaats een wijziging hebben ondergaan.';


--
-- Name: COLUMN bag_standplaats.einddatum; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_standplaats.einddatum IS '57.92 : de einddatum van een periode waarin er geen wijzigingen hebben plaatsgevonden in de gegevens die worden bijgehouden over een standplaats.';


--
-- Name: COLUMN bag_standplaats.officieel; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_standplaats.officieel IS '57.02 : een aanduiding waarmee kan worden aangegeven dat een object in de registratie is opgenomen als gevolg van een feitelijke constatering, zonder dat er op het moment van opname sprake is van een formele grondslag voor deze opname.';


--
-- Name: COLUMN bag_standplaats.status; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_standplaats.status IS '57.03 : de fase van de levenscyclus van een standplaats, waarin de betreffende standplaats zich bevindt.';


--
-- Name: COLUMN bag_standplaats.hoofdadres; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_standplaats.hoofdadres IS '57:10 : de identificatiecode nummeraanduiding waaronder het hoofdadres van een standplaats, dat in het kader van de basis gebouwen registratie als zodanig is aangemerkt, is opgenomen in de basis registratie adressen.';


--
-- Name: COLUMN bag_standplaats.inonderzoek; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_standplaats.inonderzoek IS '57.93 : een aanduiding waarmee wordt aangegeven dat een onderzoek wordt uitgevoerd naar de juistheid van een of meerdere gegevens van het betreffende object.';


--
-- Name: COLUMN bag_standplaats.documentdatum; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_standplaats.documentdatum IS '57.97 : de datum waarop het brondocument is vastgesteld, op basis waarvan een opname, mutatie of een in de historie plaatsen van gegevens ten aanzien van een standplaats heeft plaatsgevonden.';


--
-- Name: COLUMN bag_standplaats.documentnummer; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_standplaats.documentnummer IS '57.98 : de unieke aanduiding van het brondocument op basis waarvan een opname, mutatie of een verwijdering van gegevens ten aanzien van een standplaats heeft plaatsgevonden, binnen een gemeente.';


--
-- Name: COLUMN bag_standplaats.correctie; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_standplaats.correctie IS 'het gegeven is gecorrigeerd.';


--
-- Name: bag_standplaats_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.bag_standplaats_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: bag_standplaats_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.bag_standplaats_id_seq OWNED BY public.bag_standplaats.id;


--
-- Name: bag_verblijfsobject; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.bag_verblijfsobject (
    identificatie character varying(16) NOT NULL,
    begindatum character varying(14) NOT NULL,
    einddatum character varying(14),
    officieel character varying(1),
    hoofdadres character varying(16) NOT NULL,
    oppervlakte integer,
    status character varying(80) NOT NULL,
    inonderzoek character varying(1) NOT NULL,
    documentdatum character varying(14),
    documentnummer character varying(20),
    correctie character varying(1),
    id integer NOT NULL
);


--
-- Name: TABLE bag_verblijfsobject; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.bag_verblijfsobject IS '56 : een verblijfsobject is de kleinste binnen een of meerdere panden gelegen en voor woon -, bedrijfsmatige - of recreatieve doeleinden geschikte eenheid van gebruik, die ontsloten wordt via een eigen toegang vanaf de openbare weg, een erf of een gedeelde verkeersruimte en die onderwerp kan zijn van rechtshandelingen.';


--
-- Name: COLUMN bag_verblijfsobject.identificatie; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_verblijfsobject.identificatie IS '56.01 : de unieke aanduiding van een verblijfsobject';


--
-- Name: COLUMN bag_verblijfsobject.begindatum; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_verblijfsobject.begindatum IS '56.91 : de begindatum van een periode waarin een of meer gegevens die worden bijgehouden over een verblijfsobject een wijziging hebben ondergaan.';


--
-- Name: COLUMN bag_verblijfsobject.einddatum; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_verblijfsobject.einddatum IS '56.92 : de einddatum van een periode waarin er geen wijzigingen hebben plaatsgevonden in de gegevens die worden bijgehouden over een verblijfsobject.';


--
-- Name: COLUMN bag_verblijfsobject.officieel; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_verblijfsobject.officieel IS '56.02 : een aanduiding waarmee kan worden aangegeven dat een object in de registratie is opgenomen als gevolg van een feitelijke constatering, zonder dat er op het moment van opname sprake is van een formele grondslag voor deze opname';


--
-- Name: COLUMN bag_verblijfsobject.hoofdadres; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_verblijfsobject.hoofdadres IS '56:10 : de identificatiecode nummeraanduiding waaronder het hoofdadres van een verblijfsobject, dat in het kader van de basis gebouwen registratie als zodanig is aangemerkt, is opgenomen in de basis registratie adressen.';


--
-- Name: COLUMN bag_verblijfsobject.oppervlakte; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_verblijfsobject.oppervlakte IS '56.31 : de gebruiksoppervlakte van een verblijfsobject in gehele vierkante meters.';


--
-- Name: COLUMN bag_verblijfsobject.status; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_verblijfsobject.status IS '56.32 : de fase van de levenscyclus van een verblijfsobject, waarin het betreffende verblijfsobject zich bevindt.';


--
-- Name: COLUMN bag_verblijfsobject.inonderzoek; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_verblijfsobject.inonderzoek IS '56.93 : een aanduiding waarmee wordt aangegeven dat een onderzoek wordt uitgevoerd naar de juistheid van een of meerdere gegevens van het betreffende object.';


--
-- Name: COLUMN bag_verblijfsobject.documentdatum; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_verblijfsobject.documentdatum IS '56.97 : de datum waarop het brondocument is vastgesteld, op basis waarvan een opname, mutatie of een verwijdering van gegevens ten aanzien van een verblijfsobject heeft plaatsgevonden.';


--
-- Name: COLUMN bag_verblijfsobject.documentnummer; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_verblijfsobject.documentnummer IS '56.98 : de unieke aanduiding van het brondocument op basis waarvan een opname, mutatie of een verwijdering van gegevens ten aanzien van een verblijfsobject heeft plaatsgevonden, binnen een gemeente.';


--
-- Name: COLUMN bag_verblijfsobject.correctie; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_verblijfsobject.correctie IS 'het gegeven is gecorrigeerd.';


--
-- Name: bag_verblijfsobject_gebruiksdoel; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.bag_verblijfsobject_gebruiksdoel (
    identificatie character varying(16) NOT NULL,
    begindatum character varying(14),
    gebruiksdoel character varying(80) NOT NULL,
    correctie character varying(1),
    id integer NOT NULL
);


--
-- Name: TABLE bag_verblijfsobject_gebruiksdoel; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.bag_verblijfsobject_gebruiksdoel IS 'koppeltabel voor gebruiksdoelen bij verblijfsobject';


--
-- Name: COLUMN bag_verblijfsobject_gebruiksdoel.identificatie; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_verblijfsobject_gebruiksdoel.identificatie IS '56.01 : de unieke aanduiding van een verblijfsobject';


--
-- Name: COLUMN bag_verblijfsobject_gebruiksdoel.begindatum; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_verblijfsobject_gebruiksdoel.begindatum IS '56.91 : de begindatum van een periode waarin een of meer gegevens die worden bijgehouden over een verblijfsobject een wijziging hebben ondergaan.';


--
-- Name: COLUMN bag_verblijfsobject_gebruiksdoel.gebruiksdoel; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_verblijfsobject_gebruiksdoel.gebruiksdoel IS '56.30 : een categorisering van de gebruiksdoelen van het betreffende verblijfsobject, zoals dit  formeel door de overheid als zodanig is toegestaan.';


--
-- Name: COLUMN bag_verblijfsobject_gebruiksdoel.correctie; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_verblijfsobject_gebruiksdoel.correctie IS 'het gegeven is gecorrigeerd.';


--
-- Name: bag_verblijfsobject_gebruiksdoel_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.bag_verblijfsobject_gebruiksdoel_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: bag_verblijfsobject_gebruiksdoel_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.bag_verblijfsobject_gebruiksdoel_id_seq OWNED BY public.bag_verblijfsobject_gebruiksdoel.id;


--
-- Name: bag_verblijfsobject_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.bag_verblijfsobject_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: bag_verblijfsobject_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.bag_verblijfsobject_id_seq OWNED BY public.bag_verblijfsobject.id;


--
-- Name: bag_verblijfsobject_pand; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.bag_verblijfsobject_pand (
    identificatie character varying(16) NOT NULL,
    begindatum character varying(14),
    pand character varying(16) NOT NULL,
    correctie character varying(1),
    id integer NOT NULL
);


--
-- Name: TABLE bag_verblijfsobject_pand; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.bag_verblijfsobject_pand IS 'koppeltabel voor panden bij verblijfsobject';


--
-- Name: COLUMN bag_verblijfsobject_pand.identificatie; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_verblijfsobject_pand.identificatie IS '56.01 : de unieke aanduiding van een verblijfsobject';


--
-- Name: COLUMN bag_verblijfsobject_pand.begindatum; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_verblijfsobject_pand.begindatum IS '56.91 : de begindatum van een periode waarin een of meer gegevens die worden bijgehouden over een verblijfsobject een wijziging hebben ondergaan.';


--
-- Name: COLUMN bag_verblijfsobject_pand.pand; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_verblijfsobject_pand.pand IS '56.90 : de unieke aanduidingen van de panden waarvan het verblijfsobject onderdeel uitmaakt.';


--
-- Name: COLUMN bag_verblijfsobject_pand.correctie; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_verblijfsobject_pand.correctie IS 'het gegeven is gecorrigeerd.';


--
-- Name: bag_verblijfsobject_pand_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.bag_verblijfsobject_pand_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: bag_verblijfsobject_pand_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.bag_verblijfsobject_pand_id_seq OWNED BY public.bag_verblijfsobject_pand.id;


--
-- Name: bag_woonplaats; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.bag_woonplaats (
    identificatie character varying(16) NOT NULL,
    begindatum character varying(14) NOT NULL,
    einddatum character varying(14),
    officieel character varying(1),
    naam character varying(80) NOT NULL,
    status character varying(80) NOT NULL,
    inonderzoek character varying(1) NOT NULL,
    documentdatum character varying(14),
    documentnummer character varying(20),
    correctie character varying(1),
    id integer NOT NULL
);


--
-- Name: TABLE bag_woonplaats; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.bag_woonplaats IS '11.7 : een woonplaats is een door de gemeenteraad als zodanig aangewezen gedeelte van het gemeentelijk grondgebied.';


--
-- Name: COLUMN bag_woonplaats.identificatie; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_woonplaats.identificatie IS '11.03 : de landelijk unieke aanduiding van een woonplaats, zoals vastgesteld door de beheerder van de landelijke tabel voor woonplaatsen.';


--
-- Name: COLUMN bag_woonplaats.begindatum; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_woonplaats.begindatum IS '11.73 : de begindatum van een periode waarin een of meer gegevens die worden bijgehouden over een woonplaats een wijziging hebben ondergaan.';


--
-- Name: COLUMN bag_woonplaats.einddatum; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_woonplaats.einddatum IS '11.74 : de einddatum van een periode waarin er geen wijzigingen hebben plaatsgevonden in de gegevens die worden bijgehouden over een woonplaats.';


--
-- Name: COLUMN bag_woonplaats.officieel; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_woonplaats.officieel IS '11.72 : een aanduiding waarmee kan worden aangegeven dat een object in de registratie is opgenomen als gevolg van een feitelijke constatering, zonder dat er op het moment van opname sprake is van een formele grondslag voor deze opname.';


--
-- Name: COLUMN bag_woonplaats.naam; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_woonplaats.naam IS '11.70 : de benaming van een door het gemeentebestuur aangewezen woonplaats.';


--
-- Name: COLUMN bag_woonplaats.status; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_woonplaats.status IS '11.79 : de fase van de levenscyclus van een woonplaats, waarin de betreffende woonplaats zich bevindt.';


--
-- Name: COLUMN bag_woonplaats.inonderzoek; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_woonplaats.inonderzoek IS '11.75 : een aanduiding waarmee wordt aangegeven dat een onderzoek wordt uitgevoerd naar de juistheid van een of meerdere gegevens van het betreffende object.';


--
-- Name: COLUMN bag_woonplaats.documentdatum; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_woonplaats.documentdatum IS '11.77 : de datum waarop het brondocument is vastgesteld, op basis waarvan een opname, mutatie of een verwijdering van gegevens ten aanzien van een woonplaats heeft plaatsgevonden.';


--
-- Name: COLUMN bag_woonplaats.documentnummer; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_woonplaats.documentnummer IS '11.78 : de unieke aanduiding van het brondocument op basis waarvan een opname, mutatie of een verwijdering van gegevens ten aanzien van een woonplaats heeft plaatsgevonden, binnen een gemeente.';


--
-- Name: COLUMN bag_woonplaats.correctie; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_woonplaats.correctie IS 'het gegeven is gecorrigeerd.';


--
-- Name: bag_woonplaats_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.bag_woonplaats_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: bag_woonplaats_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.bag_woonplaats_id_seq OWNED BY public.bag_woonplaats.id;


--
-- Name: searchable; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.searchable (
    search_index tsvector,
    search_term text,
    object_type text,
    searchable_id integer NOT NULL,
    search_order text
);


--
-- Name: bedrijf; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.bedrijf (
    search_index tsvector,
    search_term text,
    object_type text DEFAULT 'bedrijf'::character varying,
    id integer NOT NULL,
    dossiernummer character varying(8),
    subdossiernummer character varying(4),
    hoofdvestiging_dossiernummer character varying(8),
    hoofdvestiging_subdossiernummer character varying(4),
    vorig_dossiernummer character varying(8),
    vorig_subdossiernummer character varying(4),
    handelsnaam text,
    rechtsvorm smallint,
    kamernummer smallint,
    faillisement smallint,
    surseance smallint,
    telefoonnummer character varying(10),
    email character varying(128),
    vestiging_adres text,
    vestiging_straatnaam text,
    vestiging_huisnummer bigint,
    vestiging_huisnummertoevoeging text,
    vestiging_postcodewoonplaats text,
    vestiging_postcode character varying(6),
    vestiging_woonplaats text,
    correspondentie_adres text,
    correspondentie_straatnaam text,
    correspondentie_huisnummer bigint,
    correspondentie_huisnummertoevoeging text,
    correspondentie_postcodewoonplaats text,
    correspondentie_postcode character varying(6),
    correspondentie_woonplaats text,
    hoofdactiviteitencode integer,
    nevenactiviteitencode1 integer,
    nevenactiviteitencode2 integer,
    werkzamepersonen integer,
    contact_naam character varying(64),
    contact_aanspreektitel character varying(45),
    contact_voorletters character varying(19),
    contact_voorvoegsel character varying(8),
    contact_geslachtsnaam character varying(95),
    contact_geslachtsaanduiding character varying(1),
    authenticated smallint,
    authenticatedby text,
    fulldossiernummer text,
    import_datum timestamp(6) without time zone,
    deleted_on timestamp(6) without time zone,
    verblijfsobject_id character varying(16),
    system_of_record character varying(32),
    system_of_record_id bigint,
    vestigingsnummer bigint,
    vestiging_huisletter text,
    correspondentie_huisletter text,
    vestiging_adres_buitenland1 text,
    vestiging_adres_buitenland2 text,
    vestiging_adres_buitenland3 text,
    vestiging_landcode integer DEFAULT 6030,
    correspondentie_adres_buitenland1 text,
    correspondentie_adres_buitenland2 text,
    correspondentie_adres_buitenland3 text,
    correspondentie_landcode integer DEFAULT 6030,
    uuid uuid DEFAULT public.uuid_generate_v4(),
    rsin bigint,
    oin bigint,
    main_activity jsonb DEFAULT '{}'::jsonb NOT NULL,
    secondairy_activities jsonb DEFAULT '[]'::jsonb NOT NULL,
    date_founded date,
    date_registration date,
    date_ceased date,
    vestiging_latlong point,
    vestiging_bag_id bigint,
    preferred_contact_channel public.preferred_contact_channel DEFAULT 'pip'::public.preferred_contact_channel,
    active boolean DEFAULT true NOT NULL,
    pending boolean DEFAULT false NOT NULL,
    related_custom_object_id integer
)
INHERITS (public.searchable);


--
-- Name: bedrijf_authenticatie; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.bedrijf_authenticatie (
    id integer NOT NULL,
    gegevens_magazijn_id integer,
    login integer,
    password character varying(255),
    created timestamp without time zone,
    last_modified timestamp without time zone
);


--
-- Name: bedrijf_authenticatie_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.bedrijf_authenticatie_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: bedrijf_authenticatie_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.bedrijf_authenticatie_id_seq OWNED BY public.bedrijf_authenticatie.id;


--
-- Name: bedrijf_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.bedrijf_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: bedrijf_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.bedrijf_id_seq OWNED BY public.bedrijf.id;


--
-- Name: beheer_import; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.beheer_import (
    id integer NOT NULL,
    importtype character varying(256),
    succesvol integer,
    finished timestamp without time zone,
    import_create integer,
    import_update integer,
    error integer,
    error_message text,
    entries integer,
    created timestamp without time zone,
    last_modified timestamp without time zone
);


--
-- Name: beheer_import_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.beheer_import_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: beheer_import_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.beheer_import_id_seq OWNED BY public.beheer_import.id;


--
-- Name: beheer_import_log; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.beheer_import_log (
    id integer NOT NULL,
    import_id integer,
    old_data text,
    new_data text,
    created timestamp without time zone,
    last_modified timestamp without time zone,
    kolom text,
    identifier text,
    action character varying(255)
);


--
-- Name: beheer_import_log_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.beheer_import_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: beheer_import_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.beheer_import_log_id_seq OWNED BY public.beheer_import_log.id;


--
-- Name: betrokkene_notes; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.betrokkene_notes (
    id integer NOT NULL,
    betrokkene_exid integer,
    betrokkene_type text,
    betrokkene_from text,
    ntype text,
    subject text,
    message text,
    created timestamp without time zone,
    last_modified timestamp without time zone
);


--
-- Name: betrokkene_notes_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.betrokkene_notes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: betrokkene_notes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.betrokkene_notes_id_seq OWNED BY public.betrokkene_notes.id;


--
-- Name: betrokkenen; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.betrokkenen (
    id integer NOT NULL,
    btype integer,
    gm_natuurlijk_persoon_id integer,
    naam text
);


--
-- Name: betrokkenen_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.betrokkenen_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: betrokkenen_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.betrokkenen_id_seq OWNED BY public.betrokkenen.id;


--
-- Name: bibliotheek_categorie; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.bibliotheek_categorie (
    search_index tsvector,
    search_term text,
    object_type text DEFAULT 'bibliotheek_categorie'::character varying,
    id integer NOT NULL,
    naam character varying(256),
    label text,
    description text,
    help text,
    created timestamp without time zone,
    last_modified timestamp without time zone,
    system integer,
    pid integer,
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL
)
INHERITS (public.searchable);


--
-- Name: bibliotheek_categorie_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.bibliotheek_categorie_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: bibliotheek_categorie_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.bibliotheek_categorie_id_seq OWNED BY public.bibliotheek_categorie.id;


--
-- Name: bibliotheek_kenmerken; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.bibliotheek_kenmerken (
    search_index tsvector,
    search_term text,
    object_type text DEFAULT 'bibliotheek_kenmerken'::character varying,
    id integer NOT NULL,
    naam character varying(256),
    value_type text,
    value_default text DEFAULT ''::text NOT NULL,
    label text,
    description text,
    help text DEFAULT ''::text NOT NULL,
    magic_string text,
    created timestamp without time zone,
    last_modified timestamp without time zone,
    bibliotheek_categorie_id integer,
    document_categorie text,
    system integer,
    deleted timestamp without time zone,
    file_metadata_id integer,
    version integer DEFAULT 1 NOT NULL,
    properties text DEFAULT '{}'::text,
    naam_public text DEFAULT ''::text NOT NULL,
    type_multiple boolean DEFAULT false NOT NULL,
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    relationship_type text,
    relationship_name text,
    relationship_uuid uuid,
    CONSTRAINT bibliotheek_kenmerken_value_type_check CHECK ((value_type = ANY (ARRAY['text_uc'::text, 'checkbox'::text, 'richtext'::text, 'date'::text, 'file'::text, 'bag_straat_adres'::text, 'email'::text, 'valutaex'::text, 'bag_openbareruimte'::text, 'text'::text, 'bag_openbareruimtes'::text, 'url'::text, 'valuta'::text, 'option'::text, 'bag_adres'::text, 'select'::text, 'valutain6'::text, 'valutaex6'::text, 'valutaex21'::text, 'image_from_url'::text, 'bag_adressen'::text, 'valutain'::text, 'calendar'::text, 'calendar_supersaas'::text, 'bag_straat_adressen'::text, 'googlemaps'::text, 'numeric'::text, 'valutain21'::text, 'textarea'::text, 'bankaccount'::text, 'subject'::text, 'geolatlon'::text, 'appointment'::text, 'geojson'::text, 'relationship'::text, 'address_v2'::text, 'appointment_v2'::text])))
)
INHERITS (public.searchable);


--
-- Name: bibliotheek_kenmerken_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.bibliotheek_kenmerken_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: bibliotheek_kenmerken_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.bibliotheek_kenmerken_id_seq OWNED BY public.bibliotheek_kenmerken.id;


--
-- Name: bibliotheek_kenmerken_values; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.bibliotheek_kenmerken_values (
    id integer NOT NULL,
    bibliotheek_kenmerken_id integer,
    value text,
    active boolean DEFAULT true NOT NULL,
    sort_order integer NOT NULL
);


--
-- Name: bibliotheek_kenmerken_values_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.bibliotheek_kenmerken_values_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: bibliotheek_kenmerken_values_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.bibliotheek_kenmerken_values_id_seq OWNED BY public.bibliotheek_kenmerken_values.id;


--
-- Name: bibliotheek_kenmerken_values_sort_order_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.bibliotheek_kenmerken_values_sort_order_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: bibliotheek_kenmerken_values_sort_order_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.bibliotheek_kenmerken_values_sort_order_seq OWNED BY public.bibliotheek_kenmerken_values.sort_order;


--
-- Name: bibliotheek_notificatie_kenmerk; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.bibliotheek_notificatie_kenmerk (
    id integer NOT NULL,
    bibliotheek_notificatie_id integer NOT NULL,
    bibliotheek_kenmerken_id integer NOT NULL
);


--
-- Name: bibliotheek_notificatie_kenmerk_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.bibliotheek_notificatie_kenmerk_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: bibliotheek_notificatie_kenmerk_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.bibliotheek_notificatie_kenmerk_id_seq OWNED BY public.bibliotheek_notificatie_kenmerk.id;


--
-- Name: bibliotheek_notificaties; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.bibliotheek_notificaties (
    search_index tsvector,
    search_term text,
    object_type text DEFAULT 'bibliotheek_notificaties'::character varying,
    id integer NOT NULL,
    bibliotheek_categorie_id integer,
    label text,
    subject text,
    message text,
    created timestamp without time zone,
    last_modified timestamp without time zone,
    deleted timestamp without time zone,
    sender text,
    sender_address text,
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL
)
INHERITS (public.searchable);


--
-- Name: bibliotheek_notificaties_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.bibliotheek_notificaties_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: bibliotheek_notificaties_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.bibliotheek_notificaties_id_seq OWNED BY public.bibliotheek_notificaties.id;


--
-- Name: bibliotheek_sjablonen; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.bibliotheek_sjablonen (
    search_index tsvector,
    search_term text,
    object_type text DEFAULT 'bibliotheek_sjablonen'::character varying,
    id integer NOT NULL,
    bibliotheek_categorie_id integer,
    naam character varying(256),
    label text,
    description text,
    help text,
    created timestamp without time zone,
    last_modified timestamp without time zone,
    filestore_id integer,
    deleted timestamp without time zone,
    interface_id integer,
    template_external_name text,
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    CONSTRAINT filestore_or_interface CHECK (((filestore_id IS NOT NULL) OR (interface_id IS NOT NULL)))
)
INHERITS (public.searchable);


--
-- Name: bibliotheek_sjablonen_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.bibliotheek_sjablonen_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: bibliotheek_sjablonen_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.bibliotheek_sjablonen_id_seq OWNED BY public.bibliotheek_sjablonen.id;


--
-- Name: bibliotheek_sjablonen_magic_string; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.bibliotheek_sjablonen_magic_string (
    id integer NOT NULL,
    bibliotheek_sjablonen_id integer,
    value text
);


--
-- Name: bibliotheek_sjablonen_magic_string_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.bibliotheek_sjablonen_magic_string_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: bibliotheek_sjablonen_magic_string_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.bibliotheek_sjablonen_magic_string_id_seq OWNED BY public.bibliotheek_sjablonen_magic_string.id;


--
-- Name: case_authorisation_map; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.case_authorisation_map (
    key text NOT NULL,
    legacy_key text NOT NULL
);


--
-- Name: groups; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.groups (
    id integer NOT NULL,
    path integer[] NOT NULL,
    name text,
    description text,
    date_created timestamp without time zone,
    date_modified timestamp without time zone,
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    v1_json jsonb DEFAULT '{}'::jsonb NOT NULL
);


--
-- Name: roles; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.roles (
    id integer NOT NULL,
    parent_group_id integer,
    name text,
    description text,
    system_role boolean,
    date_created timestamp without time zone,
    date_modified timestamp without time zone,
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    v1_json jsonb DEFAULT '{}'::jsonb NOT NULL
);


--
-- Name: subject; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.subject (
    id integer NOT NULL,
    uuid uuid DEFAULT public.uuid_generate_v4(),
    subject_type text NOT NULL,
    properties text DEFAULT '{}'::text NOT NULL,
    settings text DEFAULT '{}'::text NOT NULL,
    username character varying NOT NULL,
    last_modified timestamp without time zone DEFAULT now(),
    role_ids integer[] DEFAULT '{}'::integer[] NOT NULL,
    group_ids integer[] DEFAULT '{}'::integer[] NOT NULL,
    nobody boolean DEFAULT false NOT NULL,
    system boolean DEFAULT false NOT NULL,
    related_custom_object_id integer,
    CONSTRAINT subject_subject_type_check CHECK ((subject_type = ANY (ARRAY['person'::text, 'company'::text, 'employee'::text])))
);


--
-- Name: subject_position_matrix; Type: MATERIALIZED VIEW; Schema: public; Owner: -
--

CREATE MATERIALIZED VIEW public.subject_position_matrix AS
 SELECT s.id AS subject_id,
    unrolled_groups.gid AS group_id,
    unrolled_roles.rid AS role_id,
    ((((unrolled_groups.gid)::character varying)::text || '|'::text) || ((unrolled_roles.rid)::character varying)::text) AS "position"
   FROM public.subject s,
    ( SELECT s_1.id AS subject_id,
            unnest(s_1.role_ids) AS rid
           FROM public.subject s_1) unrolled_roles,
    ( SELECT s_1.id AS subject_id,
            unnest(g.path) AS gid
           FROM (public.subject s_1
             JOIN public.groups g ON ((s_1.group_ids[1] = g.id)))) unrolled_groups,
    public.roles ro,
    public.groups gr
  WHERE ((s.id = unrolled_roles.subject_id) AND (s.id = unrolled_groups.subject_id) AND (ro.id = unrolled_roles.rid) AND (gr.id = unrolled_groups.gid))
  GROUP BY s.id, unrolled_groups.gid, unrolled_roles.rid
  WITH NO DATA;


--
-- Name: zaak; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.zaak (
    search_index tsvector,
    search_term text,
    object_type text DEFAULT 'zaak'::character varying,
    id integer NOT NULL,
    pid integer,
    relates_to integer,
    zaaktype_id integer NOT NULL,
    zaaktype_node_id integer NOT NULL,
    milestone integer NOT NULL,
    contactkanaal character varying(128) NOT NULL,
    aanvraag_trigger public.zaaksysteem_trigger NOT NULL,
    onderwerp text,
    resultaat text,
    besluit text,
    coordinator integer,
    behandelaar integer,
    aanvrager integer NOT NULL,
    route_ou integer,
    route_role integer,
    locatie_zaak integer,
    locatie_correspondentie integer,
    streefafhandeldatum timestamp(6) without time zone,
    registratiedatum timestamp(6) without time zone NOT NULL,
    afhandeldatum timestamp(6) without time zone,
    vernietigingsdatum timestamp(6) without time zone,
    created timestamp(6) without time zone NOT NULL,
    last_modified timestamp(6) without time zone NOT NULL,
    deleted timestamp(6) without time zone,
    vervolg_van integer,
    aanvrager_gm_id integer,
    behandelaar_gm_id integer,
    coordinator_gm_id integer,
    uuid uuid,
    payment_status text,
    payment_amount numeric(100,2),
    confidentiality public.confidentiality DEFAULT 'public'::public.confidentiality NOT NULL,
    stalled_until timestamp without time zone,
    onderwerp_extern text,
    archival_state text,
    status text NOT NULL,
    duplicate_prevention_token uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    resultaat_id integer,
    urgency text,
    preset_client boolean DEFAULT false NOT NULL,
    prefix text DEFAULT ''::text NOT NULL,
    confidential boolean DEFAULT false NOT NULL,
    html_email_template character varying,
    number_master integer,
    requestor_v1_json jsonb NOT NULL,
    assignee_v1_json jsonb,
    coordinator_v1_json jsonb,
    aanvrager_type character varying,
    dutch_price text GENERATED ALWAYS AS (COALESCE(replace((payment_amount)::text, '.'::text, ','::text), '-'::text)) STORED,
    betrokkenen_cache jsonb DEFAULT '{}'::jsonb NOT NULL,
    status_percentage numeric(100,2) DEFAULT 0 NOT NULL,
    leadtime integer GENERATED ALWAYS AS (
CASE
    WHEN (afhandeldatum IS NOT NULL) THEN date_part('day'::text, (afhandeldatum - registratiedatum))
    ELSE NULL::double precision
END) STORED,
    CONSTRAINT archival_state_value CHECK ((archival_state = ANY (ARRAY['overdragen'::text, 'vernietigen'::text]))),
    CONSTRAINT status_value CHECK ((status = ANY (ARRAY['new'::text, 'open'::text, 'stalled'::text, 'resolved'::text, 'deleted'::text])))
)
INHERITS (public.searchable);


--
-- Name: zaak_authorisation; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.zaak_authorisation (
    id integer NOT NULL,
    zaak_id integer NOT NULL,
    capability text NOT NULL,
    entity_id text NOT NULL,
    entity_type text NOT NULL,
    scope text NOT NULL
);


--
-- Name: zaaktype_authorisation; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.zaaktype_authorisation (
    id integer NOT NULL,
    zaaktype_node_id integer,
    recht text NOT NULL,
    created timestamp without time zone,
    last_modified timestamp without time zone,
    deleted timestamp without time zone,
    role_id integer,
    ou_id integer,
    zaaktype_id integer,
    confidential boolean DEFAULT false NOT NULL,
    CONSTRAINT zaaktype_authorisation_recht CHECK ((recht = ANY (ARRAY['zaak_beheer'::text, 'zaak_edit'::text, 'zaak_read'::text, 'zaak_search'::text])))
);


--
-- Name: case_acl; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW public.case_acl AS
 SELECT z.id AS case_id,
    z.uuid AS case_uuid,
    cam.key AS permission,
    s.id AS subject_id,
    s.uuid AS subject_uuid,
    z.zaaktype_id AS casetype_id
   FROM (((public.zaak z
     JOIN public.zaaktype_authorisation za ON (((za.zaaktype_id = z.zaaktype_id) AND (za.confidential = z.confidential))))
     JOIN (public.subject_position_matrix spm
     JOIN public.subject s ON ((spm.subject_id = s.id))) ON (((spm.role_id = za.role_id) AND (spm.group_id = za.ou_id))))
     JOIN public.case_authorisation_map cam ON ((za.recht = cam.legacy_key)))
UNION ALL
 SELECT z.id AS case_id,
    z.uuid AS case_uuid,
    za.capability AS permission,
    s.id AS subject_id,
    s.uuid AS subject_uuid,
    z.zaaktype_id AS casetype_id
   FROM ((public.zaak z
     JOIN public.zaak_authorisation za ON (((za.zaak_id = z.id) AND (za.entity_type = 'position'::text))))
     JOIN (public.subject_position_matrix spm
     JOIN public.subject s ON ((spm.subject_id = s.id))) ON ((spm."position" = za.entity_id)))
UNION ALL
 SELECT z.id AS case_id,
    z.uuid AS case_uuid,
    za.capability AS permission,
    s.id AS subject_id,
    s.uuid AS subject_uuid,
    z.zaaktype_id AS casetype_id
   FROM ((public.zaak z
     JOIN public.zaak_authorisation za ON (((za.zaak_id = z.id) AND (za.entity_type = 'user'::text))))
     JOIN public.subject s ON (((s.username)::text = za.entity_id)));


--
-- Name: case_action; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.case_action (
    id integer NOT NULL,
    case_id integer NOT NULL,
    casetype_status_id integer,
    type character varying(64),
    label character varying(255),
    automatic boolean,
    data text,
    state_tainted boolean DEFAULT false,
    data_tainted boolean DEFAULT false
);


--
-- Name: case_action_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.case_action_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: case_action_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.case_action_id_seq OWNED BY public.case_action.id;


--
-- Name: case_attributes; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW public.case_attributes AS
SELECT
    NULL::integer AS case_id,
    NULL::text[] AS value,
    NULL::text AS magic_string,
    NULL::integer AS library_id,
    NULL::text AS value_type,
    NULL::boolean AS mvp;


--
-- Name: object_data; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.object_data (
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    object_id integer,
    object_class text NOT NULL,
    properties text DEFAULT '{}'::text NOT NULL,
    index_hstore public.hstore,
    date_created timestamp without time zone DEFAULT now() NOT NULL,
    date_modified timestamp without time zone,
    text_vector tsvector,
    class_uuid uuid,
    acl_groupname text,
    invalid boolean DEFAULT false,
    CONSTRAINT object_data_acl_groupname_check CHECK (((acl_groupname IS NULL) OR (character_length(acl_groupname) > 0)))
);


--
-- Name: zaak_kenmerk; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.zaak_kenmerk (
    zaak_id integer NOT NULL,
    bibliotheek_kenmerken_id integer NOT NULL,
    id integer NOT NULL,
    value text[] NOT NULL,
    magic_string text NOT NULL
);


--
-- Name: zaaktype_kenmerken; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.zaaktype_kenmerken (
    id integer NOT NULL,
    bibliotheek_kenmerken_id integer,
    label text,
    help text,
    created timestamp without time zone,
    last_modified timestamp without time zone,
    zaaktype_node_id integer,
    zaak_status_id integer,
    pip integer,
    zaakinformatie_view integer DEFAULT 1,
    bag_zaakadres integer,
    value_default text,
    pip_can_change boolean,
    publish_public integer,
    is_systeemkenmerk boolean DEFAULT false,
    required_permissions text,
    version integer,
    help_extern text,
    object_id uuid,
    object_metadata text DEFAULT '{}'::text NOT NULL,
    label_multiple text,
    properties text DEFAULT '{}'::text,
    is_group boolean DEFAULT false NOT NULL,
    referential boolean DEFAULT false NOT NULL,
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    custom_object_uuid uuid,
    value_mandatory boolean DEFAULT false NOT NULL
);


--
-- Name: case_attributes_appointments; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW public.case_attributes_appointments AS
 SELECT z.id AS case_id,
    COALESCE(od.properties, '{}'::text) AS value,
    bk.magic_string,
    bk.id AS library_id,
    od.uuid AS reference
   FROM ((((public.zaak z
     JOIN public.zaaktype_kenmerken ztk ON ((z.zaaktype_node_id = ztk.zaaktype_node_id)))
     JOIN public.bibliotheek_kenmerken bk ON (((bk.id = ztk.bibliotheek_kenmerken_id) AND (bk.value_type = 'appointment'::text))))
     LEFT JOIN public.zaak_kenmerk zk ON (((z.id = zk.zaak_id) AND (zk.bibliotheek_kenmerken_id = ztk.bibliotheek_kenmerken_id))))
     LEFT JOIN public.object_data od ON (((zk.value[1])::uuid = od.uuid)))
  GROUP BY z.id, COALESCE(od.properties, '{}'::text), bk.magic_string, bk.id, od.uuid;


--
-- Name: searchable_searchable_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.searchable_searchable_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: searchable_searchable_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.searchable_searchable_id_seq OWNED BY public.searchable.searchable_id;


--
-- Name: file; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.file (
    search_index tsvector,
    search_term text,
    object_type text DEFAULT 'file'::character varying,
    searchable_id integer DEFAULT nextval('public.searchable_searchable_id_seq'::regclass),
    id integer NOT NULL,
    filestore_id integer NOT NULL,
    name text NOT NULL,
    extension character varying(10) NOT NULL,
    root_file_id integer,
    version integer DEFAULT 1,
    case_id integer,
    metadata_id integer,
    subject_id character varying(100),
    directory_id integer,
    creation_reason text NOT NULL,
    accepted boolean DEFAULT false NOT NULL,
    rejection_reason text,
    reject_to_queue boolean DEFAULT false,
    is_duplicate_name boolean DEFAULT false NOT NULL,
    publish_pip boolean DEFAULT false NOT NULL,
    publish_website boolean DEFAULT false NOT NULL,
    date_created timestamp without time zone DEFAULT now() NOT NULL,
    created_by character varying(100) NOT NULL,
    date_modified timestamp without time zone,
    modified_by character varying(100),
    date_deleted timestamp without time zone,
    deleted_by character varying(100),
    destroyed boolean DEFAULT false,
    scheduled_jobs_id integer,
    intake_owner character varying,
    active_version boolean DEFAULT false NOT NULL,
    is_duplicate_of integer,
    queue boolean DEFAULT true NOT NULL,
    document_status public.documentstatus DEFAULT 'original'::public.documentstatus NOT NULL,
    generator text,
    lock_timestamp timestamp without time zone,
    lock_subject_id uuid,
    lock_subject_name text,
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    confidential boolean DEFAULT false NOT NULL,
    rejected_by_display_name text,
    intake_group_id integer,
    intake_role_id integer,
    skip_intake boolean DEFAULT false NOT NULL,
    created_by_display_name text,
    CONSTRAINT lock_fields_check CHECK (((lock_timestamp IS NULL) OR ((lock_subject_id IS NOT NULL) AND (lock_subject_name IS NOT NULL))))
)
INHERITS (public.searchable);


--
-- Name: file_case_document; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.file_case_document (
    id integer NOT NULL,
    file_id integer NOT NULL,
    magic_string text NOT NULL,
    bibliotheek_kenmerken_id integer NOT NULL,
    case_id integer NOT NULL
);


--
-- Name: filestore; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.filestore (
    id integer NOT NULL,
    uuid uuid NOT NULL,
    thumbnail_uuid uuid,
    original_name text NOT NULL,
    size bigint NOT NULL,
    mimetype character varying(160) NOT NULL,
    md5 character varying(100) NOT NULL,
    date_created timestamp without time zone DEFAULT now() NOT NULL,
    storage_location text[],
    is_archivable boolean DEFAULT false NOT NULL,
    virus_scan_status text DEFAULT 'pending'::text NOT NULL,
    CONSTRAINT filestore_virus_scan_status_check CHECK ((virus_scan_status ~ '^(pending|ok|found(:.*)?)$'::text))
);


--
-- Name: case_documents; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW public.case_documents AS
 SELECT z.id AS case_id,
    (COALESCE(array_agg(fs.uuid) FILTER (WHERE (fs.uuid IS NOT NULL)), '{}'::uuid[]))::text[] AS value,
    bk.magic_string,
    bk.id AS library_id
   FROM (((public.zaak z
     JOIN public.zaaktype_kenmerken ztk ON ((z.zaaktype_node_id = ztk.zaaktype_node_id)))
     JOIN public.bibliotheek_kenmerken bk ON (((ztk.bibliotheek_kenmerken_id = bk.id) AND (bk.value_type = 'file'::text))))
     LEFT JOIN (public.file_case_document fcd
     JOIN (public.file f
     JOIN public.filestore fs ON ((f.filestore_id = fs.id))) ON ((fcd.file_id = f.id))) ON (((z.id = fcd.case_id) AND (bk.id = fcd.bibliotheek_kenmerken_id))))
  GROUP BY z.id, bk.magic_string, bk.id;


--
-- Name: case_attributes_v0; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW public.case_attributes_v0 AS
 SELECT case_attributes.case_id,
    case_attributes.magic_string,
    case_attributes.library_id,
    public.attribute_value_to_v0(case_attributes.value, case_attributes.value_type, case_attributes.mvp) AS value
   FROM public.case_attributes
UNION ALL
 SELECT case_attributes_appointments.case_id,
    case_attributes_appointments.magic_string,
    case_attributes_appointments.library_id,
    to_jsonb((case_attributes_appointments.reference)::text) AS value
   FROM public.case_attributes_appointments
UNION ALL
 SELECT case_documents.case_id,
    case_documents.magic_string,
    case_documents.library_id,
    public.attribute_value_to_v0(case_documents.value, 'file'::text, true) AS value
   FROM public.case_documents;


--
-- Name: case_attributes_v1; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW public.case_attributes_v1 AS
 SELECT case_attributes.case_id,
    case_attributes.magic_string,
    case_attributes.library_id,
    public.attribute_value_to_jsonb(case_attributes.value, case_attributes.value_type) AS value
   FROM public.case_attributes
UNION ALL
 SELECT case_attributes_appointments.case_id,
    case_attributes_appointments.magic_string,
    case_attributes_appointments.library_id,
    public.appointment_attribute_value_to_jsonb(case_attributes_appointments.value, case_attributes_appointments.reference) AS value
   FROM public.case_attributes_appointments
UNION ALL
 SELECT case_documents.case_id,
    case_documents.magic_string,
    case_documents.library_id,
    public.attribute_value_to_jsonb(case_documents.value, 'file'::text) AS value
   FROM public.case_documents;


--
-- Name: case_property; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.case_property (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    name text NOT NULL,
    type text NOT NULL,
    namespace text NOT NULL,
    value jsonb,
    value_v0 jsonb,
    case_id integer NOT NULL,
    object_id uuid NOT NULL,
    CONSTRAINT case_property_min_value_count CHECK (((value IS NOT NULL) OR (value_v0 IS NOT NULL)))
);


--
-- Name: COLUMN case_property.name; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.case_property.name IS 'unified magic_string/v1 attribute/v0 property';


--
-- Name: COLUMN case_property.type; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.case_property.type IS 'de-normalized type of the /value/ of the property in the context of the referent object';


--
-- Name: COLUMN case_property.value; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.case_property.value IS 'syzygy-style value blob; {"value_type_name":"text","value":"myval","meta":"data"}';


--
-- Name: COLUMN case_property.value_v0; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.case_property.value_v0 IS 'object_data-style value blob; {"human_label":"Aanvrager KvK-nummer","human_value":"123456789","value":":123456789","name":"case.requestor.coc","attribute_type":"text"}';


--
-- Name: case_relation; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.case_relation (
    id integer NOT NULL,
    case_id_a integer,
    case_id_b integer,
    order_seq_a integer,
    order_seq_b integer,
    type_a character varying(64),
    type_b character varying(64),
    uuid uuid DEFAULT public.uuid_generate_v4()
);


--
-- Name: case_relation_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.case_relation_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: case_relation_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.case_relation_id_seq OWNED BY public.case_relation.id;


--
-- Name: case_relationship_view; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW public.case_relationship_view AS
 SELECT cr.case_id_a AS case_id,
    cr.case_id_b AS relation_id,
    cr.type_a AS type,
    z.uuid AS relation_uuid
   FROM (public.case_relation cr
     JOIN public.zaak z ON (((z.id = cr.case_id_b) AND (z.deleted IS NULL))))
UNION
 SELECT cr.case_id_b AS case_id,
    cr.case_id_a AS relation_id,
    cr.type_b AS type,
    z.uuid AS relation_uuid
   FROM (public.case_relation cr
     JOIN public.zaak z ON (((z.id = cr.case_id_a) AND (z.deleted IS NULL))))
UNION
 SELECT parent.id AS case_id,
    child.id AS relation_id,
    'parent'::character varying AS type,
    child.uuid AS relation_uuid
   FROM (public.zaak parent
     JOIN public.zaak child ON ((child.pid = parent.id)))
  WHERE ((child.deleted IS NULL) AND (parent.deleted IS NULL));


--
-- Name: case_relationship_json_view; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW public.case_relationship_json_view AS
 SELECT case_relationship_view.case_id,
    case_relationship_view.type,
    jsonb_agg(json_build_object('type', 'case', 'reference', case_relationship_view.relation_uuid)) AS relationship
   FROM public.case_relationship_view
  GROUP BY case_relationship_view.case_id, case_relationship_view.type;


--
-- Name: result_preservation_terms; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.result_preservation_terms (
    id integer NOT NULL,
    code integer NOT NULL,
    label text NOT NULL,
    unit text NOT NULL,
    unit_amount numeric NOT NULL
);


--
-- Name: zaak_bag; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.zaak_bag (
    id integer NOT NULL,
    pid integer,
    zaak_id integer,
    bag_type public.zaaksysteem_bag_types,
    bag_id character varying(255),
    bag_verblijfsobject_id character varying(255),
    bag_openbareruimte_id character varying(255),
    bag_nummeraanduiding_id character varying(255),
    bag_pand_id character varying(255),
    bag_standplaats_id character varying(255),
    bag_ligplaats_id character varying(255),
    bag_coordinates_wsg point
);


--
-- Name: zaak_betrokkenen; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.zaak_betrokkenen (
    id integer NOT NULL,
    zaak_id integer,
    betrokkene_type character varying(128),
    betrokkene_id integer,
    gegevens_magazijn_id integer,
    verificatie character varying(128),
    naam character varying(255),
    rol text,
    magic_string_prefix text,
    deleted timestamp without time zone,
    uuid uuid DEFAULT public.uuid_generate_v4(),
    pip_authorized boolean DEFAULT false NOT NULL,
    subject_id uuid,
    authorisation text,
    bibliotheek_kenmerken_id integer
);


--
-- Name: zaak_meta; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.zaak_meta (
    id integer NOT NULL,
    zaak_id integer,
    verlenging character varying(255),
    opschorten character varying(255),
    deel character varying(255),
    gerelateerd character varying(255),
    vervolg character varying(255),
    afhandeling character varying(255),
    stalled_since timestamp without time zone,
    current_deadline jsonb DEFAULT '{}'::jsonb NOT NULL,
    deadline_timeline jsonb DEFAULT '[]'::jsonb NOT NULL,
    last_modified timestamp without time zone DEFAULT now() NOT NULL,
    index_hstore public.hstore,
    text_vector tsvector,
    unread_communication_count integer DEFAULT 0 NOT NULL,
    unaccepted_attribute_update_count integer DEFAULT 0 NOT NULL,
    pending_changes jsonb DEFAULT '{}'::jsonb NOT NULL,
    unaccepted_files_count integer DEFAULT 0 NOT NULL,
    CONSTRAINT unread_minimum CHECK ((unread_communication_count >= 0))
);


--
-- Name: zaaktype; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.zaaktype (
    search_index tsvector,
    search_term text,
    object_type text DEFAULT 'zaaktype'::character varying,
    id integer NOT NULL,
    zaaktype_node_id integer,
    version integer,
    created timestamp without time zone,
    last_modified timestamp without time zone,
    deleted timestamp without time zone,
    bibliotheek_categorie_id integer,
    active boolean DEFAULT true NOT NULL,
    uuid uuid DEFAULT public.uuid_generate_v4()
)
INHERITS (public.searchable);


--
-- Name: zaaktype_definitie; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.zaaktype_definitie (
    id integer NOT NULL,
    openbaarheid character varying(255),
    handelingsinitiator character varying(255),
    grondslag text,
    procesbeschrijving character varying(255),
    afhandeltermijn character varying(255),
    afhandeltermijn_type character varying(255),
    selectielijst character varying(255),
    servicenorm character varying(255),
    servicenorm_type character varying(255),
    pdc_voorwaarden text,
    pdc_description text,
    pdc_meenemen text,
    pdc_tarief text,
    omschrijving_upl character varying(255),
    aard character varying(255),
    extra_informatie text,
    preset_client character varying(255),
    extra_informatie_extern text
);


--
-- Name: zaaktype_node; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.zaaktype_node (
    id integer NOT NULL,
    zaaktype_id integer,
    zaaktype_rt_queue text,
    code text,
    trigger text,
    titel character varying(128),
    version integer,
    active integer,
    created timestamp without time zone,
    last_modified timestamp without time zone,
    deleted timestamp without time zone,
    webform_toegang integer,
    webform_authenticatie text,
    adres_relatie text,
    aanvrager_hergebruik integer,
    automatisch_aanvragen integer,
    automatisch_behandelen integer,
    toewijzing_zaakintake integer,
    toelichting character varying(128),
    online_betaling integer,
    zaaktype_definitie_id integer,
    adres_andere_locatie integer,
    adres_aanvrager integer,
    bedrijfid_wijzigen integer,
    zaaktype_vertrouwelijk integer,
    zaaktype_trefwoorden text,
    zaaktype_omschrijving text,
    extra_relaties_in_aanvraag boolean,
    properties text DEFAULT '{}'::text NOT NULL,
    contact_info_intake boolean DEFAULT false NOT NULL,
    is_public boolean DEFAULT false,
    prevent_pip boolean DEFAULT false NOT NULL,
    contact_info_email_required boolean DEFAULT false,
    contact_info_phone_required boolean DEFAULT false,
    contact_info_mobile_phone_required boolean DEFAULT false,
    moeder_zaaktype_id integer,
    logging_id integer,
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    adres_geojson boolean DEFAULT false,
    v0_json jsonb DEFAULT '{}'::jsonb NOT NULL
);


--
-- Name: zaaktype_resultaten; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.zaaktype_resultaten (
    id integer NOT NULL,
    zaaktype_node_id integer,
    zaaktype_status_id integer,
    resultaat text,
    ingang text,
    bewaartermijn integer,
    created timestamp without time zone,
    last_modified timestamp without time zone,
    dossiertype character varying(50),
    label text,
    selectielijst text,
    archiefnominatie text,
    comments text,
    external_reference text,
    trigger_archival boolean DEFAULT true NOT NULL,
    selectielijst_brondatum date,
    selectielijst_einddatum date,
    properties text DEFAULT '{}'::text NOT NULL,
    standaard_keuze boolean DEFAULT false NOT NULL
);


--
-- Name: zaaktype_status; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.zaaktype_status (
    id integer NOT NULL,
    zaaktype_node_id integer,
    status integer,
    status_type text,
    naam text,
    created timestamp without time zone,
    last_modified timestamp without time zone,
    ou_id integer,
    role_id integer,
    checklist integer,
    fase character varying(255),
    role_set integer,
    termijn integer DEFAULT 0
);


--
-- Name: case_v0; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW public.case_v0 AS
 SELECT z.uuid AS id,
    z.id AS object_id,
    jsonb_build_object('class_uuid', zt.uuid, 'pending_changes', zm.pending_changes) AS "case",
    (((((((((((((((jsonb_build_object('case.route_ou', z.route_ou, 'case.route_role', z.route_role, 'case.channel_of_contact', z.contactkanaal, 'case.html_email_template', z.html_email_template, 'case.current_deadline', zm.current_deadline, 'case.date_destruction', public.timestamp_to_perl_datetime((z.vernietigingsdatum)::timestamp with time zone), 'case.date_of_completion', public.timestamp_to_perl_datetime((z.afhandeldatum)::timestamp with time zone), 'case.date_of_registration', public.timestamp_to_perl_datetime((z.registratiedatum)::timestamp with time zone), 'case.date_target',
        CASE
            WHEN (z.status = 'stalled'::text) THEN 'Opgeschort'::text
            ELSE public.timestamp_to_perl_datetime((z.streefafhandeldatum)::timestamp with time zone)
        END, 'case.deadline_timeline', zm.deadline_timeline, 'case.stalled_since', public.timestamp_to_perl_datetime((zm.stalled_since)::timestamp with time zone), 'case.stalled_until', public.timestamp_to_perl_datetime((z.stalled_until)::timestamp with time zone), 'case.suspension_rationale', zm.opschorten, 'date_created', public.timestamp_to_perl_datetime((z.created)::timestamp with time zone), 'date_modified', public.timestamp_to_perl_datetime((z.last_modified)::timestamp with time zone), 'case.urgency', z.urgency, 'case.startdate', to_char(timezone('Europe/Amsterdam'::text, (z.registratiedatum)::timestamp with time zone), 'DD-MM-YYYY'::text), 'case.progress_days', public.get_date_progress_from_case(public.hstore(z.*)), 'case.progress_status', z.status_percentage, 'case.days_left', ((z.streefafhandeldatum)::date - COALESCE((z.afhandeldatum)::date, (now())::date)), 'case.lead_time_real', z.leadtime, 'case.destructable', public.is_destructable((z.vernietigingsdatum)::timestamp with time zone), 'case.number_status', z.milestone, 'case.milestone',
        CASE
            WHEN (zts_next.id IS NOT NULL) THEN zts.naam
            ELSE NULL::text
        END, 'case.phase',
        CASE
            WHEN (zts_next.id IS NOT NULL) THEN zts_next.fase
            ELSE NULL::character varying
        END, 'case.status', z.status, 'case.subject', z.onderwerp, 'case.subject_external', z.onderwerp_extern, 'case.archival_state', z.archival_state, 'case.confidentiality', public.get_confidential_mapping(z.confidentiality), 'case.payment_status', public.get_payment_status_mapping(z.payment_status), 'case.price', z.dutch_price, 'case.documents', COALESCE(( SELECT string_agg(concat(f.name, f.extension, ' (', f.document_status, ')'), ', '::text) AS string_agg
           FROM public.file f
          WHERE ((f.case_id = z.id) AND (f.date_deleted IS NULL) AND (f.active_version = true))), ''::text), 'case.case_documents', COALESCE(( SELECT string_agg(concat(case_documents.name, case_documents.extension, ' (', case_documents.document_status, ')'), ', '::text) AS string_agg
           FROM (public.file case_documents
             JOIN public.file_case_document fcd ON (((fcd.case_id = case_documents.case_id) AND (case_documents.id = fcd.file_id))))
          WHERE (case_documents.case_id = z.id)), ''::text), 'case.num_unaccepted_updates', zm.unaccepted_attribute_update_count, 'case.num_unread_communication', zm.unread_communication_count, 'case.num_unaccepted_files', zm.unaccepted_files_count, 'case.aggregation_scope', 'Dossier') || jsonb_build_object('case.result', z.resultaat, 'case.result_description', ztr.label, 'case.result_explanation', ztr.comments, 'case.result_id', ztr.id, 'case.result_origin', ((ztr.properties)::jsonb -> 'herkomst'::text), 'case.result_process_term', ((ztr.properties)::jsonb -> 'procestermijn'::text), 'case.result_process_type_description', ((ztr.properties)::jsonb -> 'procestype_omschrijving'::text), 'case.result_process_type_explanation', ((ztr.properties)::jsonb -> 'procestype_toelichting'::text), 'case.result_process_type_generic', ((ztr.properties)::jsonb -> 'procestype_generiek'::text), 'case.result_process_type_name', ((ztr.properties)::jsonb -> 'procestype_naam'::text), 'case.result_process_type_number', ((ztr.properties)::jsonb -> 'procestype_nummer'::text), 'case.result_process_type_object', ((ztr.properties)::jsonb -> 'procestype_object'::text), 'case.result_selection_list_number', ((ztr.properties)::jsonb -> 'selectielijst_nummer'::text), 'case.retention_period_source_date', ztr.ingang, 'case.type_of_archiving', ztr.archiefnominatie, 'case.period_of_preservation', rpt.label, 'case.period_of_preservation_active',
        CASE
            WHEN (ztr.trigger_archival = true) THEN 'Ja'::text
            ELSE 'Nee'::text
        END, 'case.type_of_archiving', ztr.archiefnominatie, 'case.selection_list', ztr.selectielijst, 'case.active_selection_list', ztr.selectielijst)) || jsonb_build_object('case.lead_time_legal', ztd.afhandeltermijn, 'case.lead_time_service', ztd.servicenorm, 'case.principle_national', ztd.grondslag)) || jsonb_build_object('case.custom_number',
        CASE
            WHEN (char_length(z.prefix) > 0) THEN concat(z.prefix, '-', z.id)
            ELSE (z.id)::text
        END, 'case.number', z.id, 'case.number_master', z.number_master)) || jsonb_build_object('case.casetype', zt.uuid, 'case.casetype.id', zt.id, 'case.casetype.generic_category', bc.naam, 'case.casetype.initiator_type', ztd.handelingsinitiator, 'case.casetype.price.web', ztd.pdc_tarief, 'case.casetype.process_description', ztd.procesbeschrijving, 'case.casetype.publicity', ztd.openbaarheid, 'case.casetype.department', gr.name, 'case.casetype.route_role', ro.name)) || ztn.v0_json) || jsonb_build_object('case.parent_uuid', COALESCE((parent.uuid)::text, ''::text))) || jsonb_build_object('assignee', (z.assignee_v1_json ->> 'reference'::text), 'case.assignee', (z.assignee_v1_json ->> 'preview'::text), 'case.assignee.uuid', (z.assignee_v1_json ->> 'reference'::text), 'case.assignee.email', ((((z.assignee_v1_json -> 'instance'::text) -> 'subject'::text) -> 'instance'::text) ->> 'email_address'::text), 'case.assignee.initials', ((((z.assignee_v1_json -> 'instance'::text) -> 'subject'::text) -> 'instance'::text) ->> 'initials'::text), 'case.assignee.last_name', ((((z.assignee_v1_json -> 'instance'::text) -> 'subject'::text) -> 'instance'::text) ->> 'surname'::text), 'case.assignee.first_names', ((((z.assignee_v1_json -> 'instance'::text) -> 'subject'::text) -> 'instance'::text) ->> 'first_names'::text), 'case.assignee.phone_number', ((((z.assignee_v1_json -> 'instance'::text) -> 'subject'::text) -> 'instance'::text) ->> 'phone_number'::text), 'case.assignee.id', (split_part(((z.assignee_v1_json -> 'instance'::text) ->> 'old_subject_identifier'::text), '-'::text, 3))::integer, 'case.assignee.department', gassign.name, 'case.assignee.title', '')) || jsonb_build_object('coordinator', (z.coordinator_v1_json -> 'reference'::text), 'case.coordinator', (z.coordinator_v1_json -> 'preview'::text), 'case.coordinator.uuid', (z.coordinator_v1_json -> 'reference'::text), 'case.coordinator.email', ((((z.coordinator_v1_json -> 'instance'::text) -> 'subject'::text) -> 'instance'::text) ->> 'email_address'::text), 'case.coordinator.phone_number', ((((z.coordinator_v1_json -> 'instance'::text) -> 'subject'::text) -> 'instance'::text) ->> 'phone_number'::text), 'case.coordinator.id', (split_part(((z.coordinator_v1_json -> 'instance'::text) ->> 'old_subject_identifier'::text), '-'::text, 3))::integer, 'case.coordinator.title', '')) || public.case_subject_as_v0_json(public.hstore(requestor.*), 'requestor'::text, false)) || public.case_subject_as_v0_json(public.hstore(requestor.*), 'requestor'::text, true)) || jsonb_build_object('case.requestor.preset_client',
        CASE
            WHEN (z.preset_client = true) THEN 'Ja'::text
            ELSE 'Nee'::text
        END)) || public.case_subject_as_v0_json(public.hstore(recipient.*), 'recipient'::text, false)) || public.case_subject_as_v0_json(public.hstore(recipient.*), 'recipient'::text, true)) || public.case_location_as_v0_json(public.hstore(case_location.*))) || ( SELECT COALESCE(jsonb_object_agg(concat('attribute.', ca.magic_string), ca.value) FILTER (WHERE (ca.magic_string IS NOT NULL)), '{}'::jsonb) AS attributes
           FROM public.case_attributes_v0 ca
          WHERE (ca.case_id = z.id))) AS "values",
    'case'::text AS object_type,
    '{}'::text[] AS related_objects
   FROM ((((((((((((((((((public.zaak z
     JOIN public.zaak_meta zm ON ((z.id = zm.zaak_id)))
     LEFT JOIN public.zaaktype zt ON ((z.zaaktype_id = zt.id)))
     LEFT JOIN public.zaaktype_node ztn ON (((z.zaaktype_node_id = ztn.id) AND (zt.id = ztn.zaaktype_id))))
     LEFT JOIN public.zaaktype_resultaten ztr ON ((z.resultaat_id = ztr.id)))
     LEFT JOIN public.bibliotheek_categorie bc ON ((zt.bibliotheek_categorie_id = bc.id)))
     LEFT JOIN public.zaaktype_definitie ztd ON ((ztn.zaaktype_definitie_id = ztd.id)))
     LEFT JOIN public.groups gr ON ((z.route_ou = gr.id)))
     LEFT JOIN public.subject assignee ON ((((z.assignee_v1_json ->> 'reference'::text))::uuid = assignee.uuid)))
     LEFT JOIN public.groups gassign ON ((assignee.group_ids[1] = gassign.id)))
     LEFT JOIN public.subject coordinator ON ((((z.coordinator_v1_json ->> 'reference'::text))::uuid = coordinator.uuid)))
     LEFT JOIN public.roles ro ON ((z.route_role = ro.id)))
     LEFT JOIN public.zaak parent ON ((z.pid = parent.id)))
     LEFT JOIN public.zaak_betrokkenen requestor ON (((z.aanvrager = requestor.id) AND (z.id = requestor.zaak_id))))
     LEFT JOIN public.zaak_betrokkenen recipient ON (((z.id = recipient.zaak_id) AND (recipient.rol = 'Ontvanger'::text))))
     LEFT JOIN public.result_preservation_terms rpt ON ((ztr.bewaartermijn = rpt.code)))
     LEFT JOIN public.zaak_bag case_location ON (((case_location.id = z.locatie_zaak) AND (z.id = case_location.zaak_id))))
     LEFT JOIN public.zaaktype_status zts_next ON (((z.zaaktype_node_id = zts_next.zaaktype_node_id) AND (zts_next.status = (z.milestone + 1)))))
     LEFT JOIN public.zaaktype_status zts ON (((z.zaaktype_node_id = zts.zaaktype_node_id) AND (zts.status = z.milestone))));


--
-- Name: casetype_end_status; Type: MATERIALIZED VIEW; Schema: public; Owner: -
--

CREATE MATERIALIZED VIEW public.casetype_end_status AS
 SELECT l1.id,
    l1.zaaktype_node_id,
    l1.status,
    l1.status_type,
    l1.naam,
    l1.created,
    l1.last_modified,
    l1.ou_id,
    l1.role_id,
    l1.checklist,
    l1.fase,
    l1.role_set,
    l1.termijn
   FROM (public.zaaktype_status l1
     JOIN ( SELECT zaaktype_status.zaaktype_node_id,
            max(zaaktype_status.status) AS phase
           FROM public.zaaktype_status
          GROUP BY zaaktype_status.zaaktype_node_id) l2 ON (((l1.status = l2.phase) AND (l1.zaaktype_node_id = l2.zaaktype_node_id))))
  WITH NO DATA;


--
-- Name: casetype_v1_reference; Type: MATERIALIZED VIEW; Schema: public; Owner: -
--

CREATE MATERIALIZED VIEW public.casetype_v1_reference AS
 SELECT zt.id,
    ztn.titel AS title,
    zt.uuid,
    ztn.version,
    ztn.id AS casetype_node_id
   FROM (public.zaaktype zt
     JOIN public.zaaktype_node ztn ON ((ztn.zaaktype_id = zt.id)))
  WITH NO DATA;


--
-- Name: case_v1; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW public.case_v1 AS
 SELECT c.number,
    c.id,
    c.number_parent,
    c.number_master,
    c.number_previous,
    c.subject,
    c.subject_external,
    c.status,
    c.date_created,
    c.date_modified,
    c.date_destruction,
    c.date_of_completion,
    c.date_of_registration,
    c.date_target,
    c.html_email_template,
    c.payment_status,
    c.price,
    c.channel_of_contact,
    c.archival_state,
    c.confidentiality,
    c.stalled_since,
    c.stalled_until,
    c.current_deadline,
    c.deadline_timeline,
    c.result_id,
    c.result,
    c.active_selection_list,
    c.outcome,
    c.casetype,
    c.route,
    c.suspension_rationale,
    c.premature_completion_rationale,
    (zts.fase)::text AS phase,
    c.relations,
    c.case_relationships,
    c.requestor,
    c.assignee,
    c.coordinator,
    c.aggregation_scope,
    c.case_location,
    c.correspondence_location,
    ( SELECT COALESCE(jsonb_object_agg(ca.magic_string, ca.value) FILTER (WHERE (ca.magic_string IS NOT NULL)), '{}'::jsonb) AS "coalesce"
           FROM public.case_attributes_v1 ca
          WHERE (ca.case_id = c.number)) AS attributes,
    ( SELECT json_build_object('preview', zts.fase, 'reference', NULL::unknown, 'type', 'case/milestone', 'instance', json_build_object('date_created', public.timestamp_to_perl_datetime(now()), 'date_modified', public.timestamp_to_perl_datetime(now()), 'phase_label',
                CASE
                    WHEN (zts.id IS NOT NULL) THEN zts.fase
                    ELSE zts_end.fase
                END, 'phase_sequence_number',
                CASE
                    WHEN (zts.id IS NOT NULL) THEN zts.status
                    ELSE zts_end.status
                END, 'milestone_label', zts_previous.naam, 'milestone_sequence_number', zts_previous.status, 'last_sequence_number', zts_end.status)) AS json_build_object
           FROM public.casetype_end_status zts_end
          WHERE (c.zaaktype_node_id = zts_end.zaaktype_node_id)) AS milestone
   FROM ((( SELECT z.id AS number,
            z.uuid AS id,
            z.pid AS number_parent,
            z.number_master,
            z.vervolg_van AS number_previous,
            z.onderwerp AS subject,
            z.onderwerp_extern AS subject_external,
            z.status,
            z.created AS date_created,
            z.last_modified AS date_modified,
            z.vernietigingsdatum AS date_destruction,
            z.afhandeldatum AS date_of_completion,
            z.registratiedatum AS date_of_registration,
            z.streefafhandeldatum AS date_target,
            z.html_email_template,
            z.payment_status,
            z.dutch_price AS price,
            z.contactkanaal AS channel_of_contact,
            z.archival_state,
            z.zaaktype_node_id,
            z.milestone AS raw_milestone,
            public.get_confidential_mapping(z.confidentiality) AS confidentiality,
                CASE
                    WHEN (z.status = 'stalled'::text) THEN zm.stalled_since
                    ELSE NULL::timestamp without time zone
                END AS stalled_since,
                CASE
                    WHEN (z.status = 'stalled'::text) THEN z.stalled_until
                    ELSE NULL::timestamp without time zone
                END AS stalled_until,
            zm.current_deadline,
            zm.deadline_timeline,
            ztr.id AS result_id,
            ztr.resultaat AS result,
            ztr.selectielijst AS active_selection_list,
                CASE
                    WHEN (ztr.id IS NOT NULL) THEN json_build_object('reference', NULL::unknown, 'type', 'case/result', 'preview',
                    CASE
                        WHEN (ztr.label IS NOT NULL) THEN ztr.label
                        ELSE ztr.resultaat
                    END, 'instance', json_build_object('date_created', public.timestamp_to_perl_datetime((ztr.created)::timestamp with time zone), 'date_modified', public.timestamp_to_perl_datetime((ztr.last_modified)::timestamp with time zone), 'archival_type', ztr.archiefnominatie, 'dossier_type', ztr.dossiertype, 'name',
                    CASE
                        WHEN (ztr.label IS NOT NULL) THEN ztr.label
                        ELSE ztr.resultaat
                    END, 'result', ztr.resultaat, 'retention_period', ztr.bewaartermijn, 'selection_list',
                    CASE
                        WHEN (ztr.selectielijst = ''::text) THEN NULL::text
                        ELSE ztr.selectielijst
                    END, 'selection_list_start', ztr.selectielijst_brondatum, 'selection_list_end', ztr.selectielijst_einddatum))
                    ELSE NULL::json
                END AS outcome,
            json_build_object('preview', ct_ref.title, 'reference', ct_ref.uuid, 'instance', json_build_object('version', ct_ref.version, 'name', ct_ref.title), 'type', 'casetype') AS casetype,
            json_build_object('preview', ((gr.name || ', '::text) || role.name), 'reference', NULL::unknown, 'type', 'case/route', 'instance', json_build_object('date_created', public.timestamp_to_perl_datetime(now()), 'date_modified', public.timestamp_to_perl_datetime(now()), 'group', gr.v1_json, 'role', role.v1_json)) AS route,
                CASE
                    WHEN (z.status = 'stalled'::text) THEN zm.opschorten
                    ELSE NULL::character varying
                END AS suspension_rationale,
                CASE
                    WHEN (z.status = 'resolved'::text) THEN zm.afhandeling
                    ELSE NULL::character varying
                END AS premature_completion_rationale,
            json_build_object('type', 'set', 'instance', json_build_object('rows', COALESCE(crp.relationship, '[]'::jsonb))) AS relations,
            json_build_object('parent', z.pid, 'continuation', json_build_object('type', 'set', 'instance', json_build_object('rows', COALESCE(continuation.relationship, '[]'::jsonb))), 'child', json_build_object('type', 'set', 'instance', json_build_object('rows', COALESCE(children.relationship, '[]'::jsonb))), 'plain', json_build_object('type', 'set', 'instance', json_build_object('rows', COALESCE(crp.relationship, '[]'::jsonb)))) AS case_relationships,
            z.requestor_v1_json AS requestor,
            z.assignee_v1_json AS assignee,
            z.coordinator_v1_json AS coordinator,
            'Dossier'::text AS aggregation_scope,
            NULL::text AS case_location,
            NULL::text AS correspondence_location
           FROM ((((((((public.zaak z
             LEFT JOIN public.zaak_meta zm ON ((zm.zaak_id = z.id)))
             LEFT JOIN public.casetype_v1_reference ct_ref ON ((z.zaaktype_node_id = ct_ref.casetype_node_id)))
             LEFT JOIN public.zaaktype_resultaten ztr ON ((z.resultaat_id = ztr.id)))
             LEFT JOIN public.groups gr ON ((z.route_ou = gr.id)))
             LEFT JOIN public.roles role ON ((z.route_role = role.id)))
             LEFT JOIN public.case_relationship_json_view crp ON (((z.id = crp.case_id) AND ((crp.type)::text = 'plain'::text))))
             LEFT JOIN public.case_relationship_json_view continuation ON (((z.id = continuation.case_id) AND ((continuation.type)::text = 'initiator'::text))))
             LEFT JOIN public.case_relationship_json_view children ON (((z.id = children.case_id) AND ((children.type)::text = 'parent'::text))))
          WHERE (z.deleted IS NULL)) c
     LEFT JOIN public.zaaktype_status zts ON (((c.zaaktype_node_id = zts.zaaktype_node_id) AND (zts.status = (c.raw_milestone + 1)))))
     LEFT JOIN public.zaaktype_status zts_previous ON (((c.zaaktype_node_id = zts_previous.zaaktype_node_id) AND (zts_previous.status = c.raw_milestone))));


--
-- Name: checklist; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.checklist (
    id integer NOT NULL,
    case_id integer NOT NULL,
    case_milestone integer
);


--
-- Name: checklist_antwoord_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.checklist_antwoord_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: checklist_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.checklist_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: checklist_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.checklist_id_seq OWNED BY public.checklist.id;


--
-- Name: checklist_item; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.checklist_item (
    id integer NOT NULL,
    checklist_id integer NOT NULL,
    label text,
    state boolean DEFAULT false NOT NULL,
    sequence integer,
    user_defined boolean DEFAULT true NOT NULL,
    deprecated_answer character varying(8),
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    due_date date,
    description text,
    assignee_id integer
);


--
-- Name: checklist_item_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.checklist_item_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: checklist_item_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.checklist_item_id_seq OWNED BY public.checklist_item.id;


--
-- Name: config; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.config (
    id integer NOT NULL,
    parameter character varying(128),
    value text,
    advanced boolean DEFAULT true NOT NULL,
    uuid uuid DEFAULT public.uuid_generate_v4(),
    definition_id uuid NOT NULL
);


--
-- Name: config_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.config_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: config_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.config_id_seq OWNED BY public.config.id;


--
-- Name: contact_data; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.contact_data (
    id integer NOT NULL,
    gegevens_magazijn_id integer,
    betrokkene_type integer,
    mobiel character varying(255),
    telefoonnummer character varying(255),
    email character varying(255),
    created timestamp without time zone,
    last_modified timestamp without time zone,
    note text
);


--
-- Name: contact_data_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.contact_data_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: contact_data_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.contact_data_id_seq OWNED BY public.contact_data.id;


--
-- Name: contact_relationship; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.contact_relationship (
    id integer NOT NULL,
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    contact integer NOT NULL,
    contact_uuid uuid NOT NULL,
    contact_type text NOT NULL,
    relation integer NOT NULL,
    relation_uuid uuid NOT NULL,
    relation_type text NOT NULL
);


--
-- Name: contact_relationship_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.contact_relationship_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: contact_relationship_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.contact_relationship_id_seq OWNED BY public.contact_relationship.id;


--
-- Name: custom_object; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.custom_object (
    id integer NOT NULL,
    uuid uuid NOT NULL,
    custom_object_version_id integer
);


--
-- Name: custom_object_relationship; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.custom_object_relationship (
    id integer NOT NULL,
    relationship_type text NOT NULL,
    relationship_magic_string_prefix text,
    custom_object_id integer NOT NULL,
    custom_object_version_id integer,
    related_document_id integer,
    related_case_id integer,
    related_custom_object_id integer,
    related_uuid uuid NOT NULL,
    related_person_id integer,
    related_organization_id integer,
    related_employee_id integer,
    source_custom_field_type_id integer,
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    CONSTRAINT custom_object_relationship_at_least_one_relationship CHECK ((num_nonnulls(related_document_id, related_case_id, related_custom_object_id, related_person_id, related_employee_id, related_organization_id) = 1)),
    CONSTRAINT source_custom_field_reference CHECK ((((source_custom_field_type_id IS NOT NULL) AND (related_case_id IS NOT NULL)) OR (source_custom_field_type_id IS NULL)))
);


--
-- Name: contact_relationship_view; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW public.contact_relationship_view AS
 SELECT contact_relationship.contact,
    contact_relationship.contact_uuid,
    contact_relationship.contact_type,
    contact_relationship.relation,
    contact_relationship.relation_uuid,
    contact_relationship.relation_type
   FROM public.contact_relationship
UNION ALL
 SELECT custom_object_relationship.related_person_id AS contact,
    custom_object_relationship.related_uuid AS contact_uuid,
    'person'::text AS contact_type,
    custom_object_relationship.custom_object_id AS relation,
    co.uuid AS relation_uuid,
    'custom_object'::text AS relation_type
   FROM (public.custom_object_relationship
     JOIN public.custom_object co ON ((custom_object_relationship.custom_object_id = co.id)))
  WHERE (custom_object_relationship.related_person_id IS NOT NULL)
UNION ALL
 SELECT custom_object_relationship.related_organization_id AS contact,
    custom_object_relationship.related_uuid AS contact_uuid,
    'company'::text AS contact_type,
    custom_object_relationship.custom_object_id AS relation,
    co.uuid AS relation_uuid,
    'custom_object'::text AS relation_type
   FROM (public.custom_object_relationship
     JOIN public.custom_object co ON ((custom_object_relationship.custom_object_id = co.id)))
  WHERE (custom_object_relationship.related_organization_id IS NOT NULL)
UNION ALL
 SELECT custom_object_relationship.related_employee_id AS contact,
    custom_object_relationship.related_uuid AS contact_uuid,
    'employee'::text AS contact_type,
    custom_object_relationship.custom_object_id AS relation,
    co.uuid AS relation_uuid,
    'custom_object'::text AS relation_type
   FROM (public.custom_object_relationship
     JOIN public.custom_object co ON ((custom_object_relationship.custom_object_id = co.id)))
  WHERE (custom_object_relationship.related_employee_id IS NOT NULL);


--
-- Name: contactmoment; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.contactmoment (
    id integer NOT NULL,
    subject_id character varying(100),
    case_id integer,
    type public.contactmoment_type NOT NULL,
    medium public.contactmoment_medium NOT NULL,
    date_created timestamp without time zone DEFAULT now() NOT NULL,
    created_by text NOT NULL,
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL
);


--
-- Name: contactmoment_email; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.contactmoment_email (
    id integer NOT NULL,
    filestore_id integer NOT NULL,
    contactmoment_id integer NOT NULL,
    body text NOT NULL,
    subject text NOT NULL,
    recipient text NOT NULL,
    cc text,
    bcc text
);


--
-- Name: contactmoment_email_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.contactmoment_email_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: contactmoment_email_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.contactmoment_email_id_seq OWNED BY public.contactmoment_email.id;


--
-- Name: contactmoment_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.contactmoment_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: contactmoment_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.contactmoment_id_seq OWNED BY public.contactmoment.id;


--
-- Name: contactmoment_note; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.contactmoment_note (
    id integer NOT NULL,
    message character varying,
    contactmoment_id integer NOT NULL
);


--
-- Name: contactmoment_note_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.contactmoment_note_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: contactmoment_note_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.contactmoment_note_id_seq OWNED BY public.contactmoment_note.id;


--
-- Name: country_code; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.country_code (
    id integer NOT NULL,
    dutch_code integer NOT NULL,
    label text NOT NULL,
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    historical boolean DEFAULT false NOT NULL
);


--
-- Name: country_code_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.country_code_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: country_code_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.country_code_id_seq OWNED BY public.country_code.id;


--
-- Name: country_code_v1_view; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW public.country_code_v1_view AS
 SELECT r.id,
    r.dutch_code,
    r.uuid,
    r.label,
    f.unit_json AS json
   FROM public.country_code r,
    LATERAL public.country_code_to_json(public.hstore(r.*)) f(unit_json);


--
-- Name: custom_object_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.custom_object_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: custom_object_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.custom_object_id_seq OWNED BY public.custom_object.id;


--
-- Name: custom_object_relationship_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.custom_object_relationship_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: custom_object_relationship_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.custom_object_relationship_id_seq OWNED BY public.custom_object_relationship.id;


--
-- Name: custom_object_type; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.custom_object_type (
    id integer NOT NULL,
    uuid uuid NOT NULL,
    catalog_folder_id integer,
    custom_object_type_version_id integer,
    authorization_definition jsonb
);


--
-- Name: custom_object_type_acl; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW public.custom_object_type_acl AS
 SELECT authorization_subquery.custom_object_type_id,
    roles.id AS role_id,
    groups.id AS group_id,
    authorization_subquery."authorization"
   FROM ((( SELECT custom_object_type.id AS custom_object_type_id,
            (t.role ->> 'uuid'::text) AS role_uuid,
            (t.department ->> 'uuid'::text) AS group_uuid,
            t."authorization"
           FROM (public.custom_object_type
             CROSS JOIN LATERAL jsonb_to_recordset((custom_object_type.authorization_definition -> 'authorizations'::text)) t("authorization" text, role json, department json))) authorization_subquery
     JOIN public.roles ON (((roles.uuid)::text = authorization_subquery.role_uuid)))
     JOIN public.groups ON (((groups.uuid)::text = authorization_subquery.group_uuid)));


--
-- Name: custom_object_type_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.custom_object_type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: custom_object_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.custom_object_type_id_seq OWNED BY public.custom_object_type.id;


--
-- Name: custom_object_type_version; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.custom_object_type_version (
    id integer NOT NULL,
    uuid uuid NOT NULL,
    name text,
    title text,
    status public.custom_object_type_status DEFAULT 'active'::public.custom_object_type_status NOT NULL,
    version integer NOT NULL,
    custom_field_definition jsonb,
    authorizations jsonb,
    relationship_definition jsonb,
    date_created timestamp without time zone DEFAULT now(),
    last_modified timestamp without time zone DEFAULT now(),
    date_deleted timestamp without time zone,
    custom_object_type_id integer,
    audit_log jsonb DEFAULT '{}'::jsonb NOT NULL,
    external_reference text,
    subtitle text
);


--
-- Name: custom_object_type_version_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.custom_object_type_version_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: custom_object_type_version_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.custom_object_type_version_id_seq OWNED BY public.custom_object_type_version.id;


--
-- Name: custom_object_version; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.custom_object_version (
    id integer NOT NULL,
    uuid uuid NOT NULL,
    title text,
    status public.custom_object_version_status DEFAULT 'active'::public.custom_object_version_status NOT NULL,
    version integer NOT NULL,
    custom_object_version_content_id integer NOT NULL,
    custom_object_type_version_id integer NOT NULL,
    date_created timestamp without time zone DEFAULT now(),
    last_modified timestamp without time zone DEFAULT now(),
    date_deleted timestamp without time zone,
    custom_object_id integer,
    external_reference text,
    subtitle text
);


--
-- Name: custom_object_version_content; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.custom_object_version_content (
    id integer NOT NULL,
    archive_status public.custom_object_version_content_archive_status,
    archive_ground text,
    archive_retention integer,
    custom_fields jsonb
);


--
-- Name: custom_object_version_content_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.custom_object_version_content_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: custom_object_version_content_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.custom_object_version_content_id_seq OWNED BY public.custom_object_version_content.id;


--
-- Name: custom_object_version_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.custom_object_version_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: custom_object_version_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.custom_object_version_id_seq OWNED BY public.custom_object_version.id;


--
-- Name: directory; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.directory (
    id integer NOT NULL,
    name text NOT NULL,
    case_id integer NOT NULL,
    original_name text NOT NULL,
    path integer[] DEFAULT ARRAY[]::integer[] NOT NULL,
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    CONSTRAINT directory_no_self_parent CHECK ((NOT (ARRAY[id] <@ path)))
);


--
-- Name: directory_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.directory_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: directory_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.directory_id_seq OWNED BY public.directory.id;


--
-- Name: export_queue; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.export_queue (
    id integer NOT NULL,
    uuid uuid DEFAULT public.uuid_generate_v4(),
    subject_id integer NOT NULL,
    subject_uuid uuid NOT NULL,
    expires timestamp with time zone DEFAULT (now() + '3 days'::interval) NOT NULL,
    token text NOT NULL,
    filestore_id integer NOT NULL,
    filestore_uuid uuid NOT NULL,
    downloaded integer DEFAULT 0 NOT NULL
);


--
-- Name: export_queue_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.export_queue_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: export_queue_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.export_queue_id_seq OWNED BY public.export_queue.id;


--
-- Name: file_annotation; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.file_annotation (
    id uuid NOT NULL,
    file_id integer NOT NULL,
    subject character varying(255) NOT NULL,
    properties text DEFAULT '{}'::text NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL,
    modified timestamp without time zone
);


--
-- Name: file_case_document_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.file_case_document_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: file_case_document_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.file_case_document_id_seq OWNED BY public.file_case_document.id;


--
-- Name: file_derivative; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.file_derivative (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    file_id integer NOT NULL,
    filestore_id integer NOT NULL,
    max_width integer NOT NULL,
    max_height integer NOT NULL,
    date_generated timestamp without time zone DEFAULT now() NOT NULL,
    type text NOT NULL,
    CONSTRAINT file_derivative_type CHECK ((type = ANY (ARRAY['pdf'::text, 'thumbnail'::text, 'doc'::text, 'docx'::text])))
);


--
-- Name: file_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.file_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: file_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.file_id_seq OWNED BY public.file.id;


--
-- Name: file_metadata; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.file_metadata (
    id integer NOT NULL,
    description text,
    trust_level text DEFAULT 'Zaakvertrouwelijk'::character varying NOT NULL,
    origin text,
    document_category text,
    origin_date date,
    pronom_format text,
    appearance text,
    structure text,
    creation_date date,
    CONSTRAINT file_metadata_origin_check CHECK ((origin ~ '(Inkomend|Uitgaand|Intern)'::text)),
    CONSTRAINT file_metadata_trust_level_check CHECK ((trust_level ~ '(Openbaar|Beperkt openbaar|Intern|Zaakvertrouwelijk|Vertrouwelijk|Confidentieel|Geheim|Zeer geheim)'::text))
);


--
-- Name: file_metadata_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.file_metadata_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: file_metadata_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.file_metadata_id_seq OWNED BY public.file_metadata.id;


--
-- Name: filestore_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.filestore_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: filestore_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.filestore_id_seq OWNED BY public.filestore.id;


--
-- Name: gegevensmagazijn_subjecten; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.gegevensmagazijn_subjecten (
    subject_uuid uuid NOT NULL,
    nnp_uuid uuid NOT NULL
);


--
-- Name: gm_adres; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.gm_adres (
    id integer NOT NULL,
    straatnaam character varying(80),
    huisnummer bigint,
    huisletter character(1),
    huisnummertoevoeging text,
    nadere_aanduiding character varying(35),
    postcode character varying(6),
    woonplaats character varying(75),
    gemeentedeel character varying(75),
    functie_adres character(1) NOT NULL,
    datum_aanvang_bewoning date,
    woonplaats_id character varying(32),
    gemeente_code smallint,
    hash character varying(32),
    import_datum timestamp(6) without time zone,
    adres_buitenland1 text,
    adres_buitenland2 text,
    adres_buitenland3 text,
    landcode integer DEFAULT 6030,
    natuurlijk_persoon_id integer,
    deleted_on timestamp(6) without time zone,
    bag_id text,
    geo_lat_long point
);


--
-- Name: gm_adres_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.gm_adres_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: gm_adres_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.gm_adres_id_seq OWNED BY public.gm_adres.id;


--
-- Name: gm_bedrijf; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.gm_bedrijf (
    id integer NOT NULL,
    gegevens_magazijn_id integer,
    dossiernummer character varying(8),
    subdossiernummer character varying(4),
    hoofdvestiging_dossiernummer character varying(8),
    hoofdvestiging_subdossiernummer character varying(4),
    vorig_dossiernummer character varying(8),
    vorig_subdossiernummer character varying(4),
    handelsnaam text,
    rechtsvorm smallint,
    kamernummer smallint,
    faillisement smallint,
    surseance smallint,
    telefoonnummer character varying(15),
    email character varying(128),
    vestiging_adres text,
    vestiging_straatnaam text,
    vestiging_huisnummer bigint,
    vestiging_huisnummertoevoeging text,
    vestiging_postcodewoonplaats text,
    vestiging_postcode character varying(6),
    vestiging_woonplaats text,
    correspondentie_adres text,
    correspondentie_straatnaam text,
    correspondentie_huisnummer bigint,
    correspondentie_huisnummertoevoeging text,
    correspondentie_postcodewoonplaats text,
    correspondentie_postcode character varying(6),
    correspondentie_woonplaats text,
    hoofdactiviteitencode integer,
    nevenactiviteitencode1 integer,
    nevenactiviteitencode2 integer,
    werkzamepersonen integer,
    contact_naam character varying(64),
    contact_aanspreektitel character varying(45),
    contact_voorletters character varying(19),
    contact_voorvoegsel character varying(8),
    contact_geslachtsnaam character varying(95),
    contact_geslachtsaanduiding character varying(1),
    authenticated smallint,
    authenticatedby text,
    import_datum timestamp(6) without time zone,
    verblijfsobject_id character varying(16),
    vestigingsnummer bigint,
    vestiging_huisletter text,
    correspondentie_huisletter text,
    vestiging_adres_buitenland1 text,
    vestiging_adres_buitenland2 text,
    vestiging_adres_buitenland3 text,
    vestiging_landcode integer DEFAULT 6030,
    correspondentie_adres_buitenland1 text,
    correspondentie_adres_buitenland2 text,
    correspondentie_adres_buitenland3 text,
    correspondentie_landcode integer DEFAULT 6030,
    rsin bigint,
    oin bigint,
    main_activity jsonb DEFAULT '{}'::jsonb NOT NULL,
    secondairy_activities jsonb DEFAULT '[]'::jsonb NOT NULL,
    date_founded date,
    date_registration date,
    date_ceased date,
    vestiging_latlong point,
    vestiging_bag_id bigint,
    preferred_contact_channel public.preferred_contact_channel DEFAULT 'pip'::public.preferred_contact_channel
);


--
-- Name: gm_bedrijf_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.gm_bedrijf_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: gm_bedrijf_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.gm_bedrijf_id_seq OWNED BY public.gm_bedrijf.id;


--
-- Name: gm_natuurlijk_persoon; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.gm_natuurlijk_persoon (
    id integer NOT NULL,
    gegevens_magazijn_id integer,
    betrokkene_type integer,
    burgerservicenummer character varying(9),
    a_nummer character varying(10),
    voorletters character varying(50),
    voornamen character varying(200),
    geslachtsnaam character varying(200),
    voorvoegsel character varying(50),
    geslachtsaanduiding character varying(3),
    nationaliteitscode1 smallint,
    nationaliteitscode2 smallint,
    nationaliteitscode3 smallint,
    geboorteplaats character varying(75),
    geboorteland character varying(75),
    geboortedatum timestamp without time zone,
    aanhef_aanschrijving character varying(10),
    voorletters_aanschrijving character varying(20),
    voornamen_aanschrijving character varying(200),
    naam_aanschrijving character varying(200),
    voorvoegsel_aanschrijving character varying(50),
    burgerlijke_staat character(1),
    indicatie_geheim character(1),
    import_datum timestamp(6) without time zone,
    adres_id integer,
    authenticatedby text,
    authenticated smallint,
    datum_overlijden timestamp(6) without time zone,
    verblijfsobject_id character varying(16),
    aanduiding_naamgebruik character varying(1),
    onderzoek_persoon boolean,
    onderzoek_huwelijk boolean,
    onderzoek_overlijden boolean,
    onderzoek_verblijfplaats boolean,
    partner_a_nummer character varying(50),
    partner_burgerservicenummer character varying(50),
    partner_voorvoegsel character varying(50),
    partner_geslachtsnaam character varying(50),
    datum_huwelijk timestamp without time zone,
    datum_huwelijk_ontbinding timestamp without time zone,
    landcode integer DEFAULT 6030 NOT NULL,
    naamgebruik text,
    adellijke_titel text,
    preferred_contact_channel public.preferred_contact_channel DEFAULT 'pip'::public.preferred_contact_channel,
    surname text GENERATED ALWAYS AS (COALESCE(naamgebruik, (geslachtsnaam)::text)) STORED
);


--
-- Name: gm_natuurlijk_persoon_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.gm_natuurlijk_persoon_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: gm_natuurlijk_persoon_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.gm_natuurlijk_persoon_id_seq OWNED BY public.gm_natuurlijk_persoon.id;


--
-- Name: groups_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.groups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.groups_id_seq OWNED BY public.groups.id;


--
-- Name: interface; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.interface (
    id integer NOT NULL,
    name text NOT NULL,
    active boolean NOT NULL,
    case_type_id integer,
    max_retries integer NOT NULL,
    interface_config text NOT NULL,
    multiple boolean DEFAULT false NOT NULL,
    module text NOT NULL,
    date_deleted timestamp without time zone,
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    objecttype_id uuid
);


--
-- Name: interface_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.interface_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: interface_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.interface_id_seq OWNED BY public.interface.id;


--
-- Name: legal_entity_type; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.legal_entity_type (
    id integer NOT NULL,
    code integer NOT NULL,
    label text NOT NULL,
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    active boolean DEFAULT true NOT NULL
);


--
-- Name: legal_entity_type_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.legal_entity_type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: legal_entity_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.legal_entity_type_id_seq OWNED BY public.legal_entity_type.id;


--
-- Name: legal_entity_v1_view; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW public.legal_entity_v1_view AS
 SELECT r.id,
    r.code,
    r.uuid,
    r.label,
    f.unit_json AS json
   FROM public.legal_entity_type r,
    LATERAL public.legal_entity_code_to_json(public.hstore(r.*)) f(unit_json);


--
-- Name: logging; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.logging (
    id integer NOT NULL,
    zaak_id integer,
    betrokkene_id character varying(128),
    aanvrager_id character varying(128),
    is_bericht integer,
    component character varying(64),
    component_id integer,
    seen integer,
    onderwerp text,
    bericht text,
    created timestamp without time zone,
    last_modified timestamp without time zone,
    deleted_on timestamp without time zone,
    event_type text,
    event_data text,
    created_by text,
    modified_by text,
    deleted_by text,
    created_for text,
    created_by_name_cache character varying,
    object_uuid uuid,
    restricted boolean DEFAULT false NOT NULL,
    uuid uuid DEFAULT public.uuid_generate_v4()
);


--
-- Name: logging_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.logging_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: logging_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.logging_id_seq OWNED BY public.logging.id;


--
-- Name: message; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.message (
    id integer NOT NULL,
    message text NOT NULL,
    subject_id character varying,
    logging_id integer NOT NULL,
    is_read boolean DEFAULT false,
    is_archived boolean DEFAULT false NOT NULL,
    created timestamp without time zone DEFAULT timezone('UTC'::text, CURRENT_TIMESTAMP) NOT NULL
);


--
-- Name: message_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.message_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: message_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.message_id_seq OWNED BY public.message.id;


--
-- Name: municipality_code; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.municipality_code (
    id integer NOT NULL,
    dutch_code integer NOT NULL,
    label text NOT NULL,
    alternative_name text,
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    historical boolean DEFAULT false NOT NULL
);


--
-- Name: municipality_code_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.municipality_code_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: municipality_code_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.municipality_code_id_seq OWNED BY public.municipality_code.id;


--
-- Name: municipality_code_v1_view; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW public.municipality_code_v1_view AS
 SELECT r.id,
    r.dutch_code,
    r.uuid,
    r.label,
    f.unit_json AS json
   FROM public.municipality_code r,
    LATERAL public.municipality_code_to_json(public.hstore(r.*)) f(unit_json);


--
-- Name: natuurlijk_persoon; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.natuurlijk_persoon (
    search_index tsvector,
    search_term text,
    object_type text DEFAULT 'natuurlijk_persoon'::character varying,
    id integer NOT NULL,
    burgerservicenummer character varying(9),
    a_nummer character varying(10),
    voorletters character varying(50),
    voornamen text,
    geslachtsnaam text,
    voorvoegsel character varying(50),
    geslachtsaanduiding character varying(3),
    nationaliteitscode1 smallint,
    nationaliteitscode2 smallint,
    nationaliteitscode3 smallint,
    geboorteplaats text,
    geboorteland text,
    geboortedatum timestamp without time zone,
    aanhef_aanschrijving character varying(10),
    voorletters_aanschrijving character varying(20),
    voornamen_aanschrijving character varying(200),
    naam_aanschrijving text,
    voorvoegsel_aanschrijving character varying(50),
    burgerlijke_staat character(1),
    indicatie_geheim character(1),
    land_waarnaar_vertrokken smallint,
    import_datum timestamp(6) without time zone,
    adres_id integer,
    authenticated boolean DEFAULT false NOT NULL,
    authenticatedby text,
    deleted_on timestamp(6) without time zone,
    verblijfsobject_id character varying(16),
    datum_overlijden timestamp without time zone,
    aanduiding_naamgebruik character varying(1),
    onderzoek_persoon boolean,
    onderzoek_huwelijk boolean,
    onderzoek_overlijden boolean,
    onderzoek_verblijfplaats boolean,
    partner_a_nummer character varying(50),
    partner_burgerservicenummer character varying(50),
    partner_voorvoegsel character varying(50),
    partner_geslachtsnaam character varying(50),
    datum_huwelijk timestamp without time zone,
    datum_huwelijk_ontbinding timestamp without time zone,
    in_gemeente boolean DEFAULT false,
    landcode integer DEFAULT 6030 NOT NULL,
    naamgebruik text,
    uuid uuid DEFAULT public.uuid_generate_v4(),
    active boolean DEFAULT true NOT NULL,
    adellijke_titel text,
    preferred_contact_channel public.preferred_contact_channel DEFAULT 'pip'::public.preferred_contact_channel,
    pending boolean DEFAULT false NOT NULL,
    related_custom_object_id integer,
    gemeentecode smallint,
    surname text GENERATED ALWAYS AS (COALESCE(naamgebruik, geslachtsnaam)) STORED
)
INHERITS (public.searchable);


--
-- Name: natuurlijk_persoon_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.natuurlijk_persoon_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: natuurlijk_persoon_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.natuurlijk_persoon_id_seq OWNED BY public.natuurlijk_persoon.id;


--
-- Name: object_acl_entry; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.object_acl_entry (
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    object_uuid uuid NOT NULL,
    entity_type text NOT NULL,
    entity_id text NOT NULL,
    capability text NOT NULL,
    scope text DEFAULT 'instance'::text NOT NULL,
    groupname text,
    CONSTRAINT object_acl_entry_groupname_check CHECK (((groupname IS NULL) OR (character_length(groupname) > 0))),
    CONSTRAINT object_acl_entry_scope_check CHECK ((scope = ANY (ARRAY['instance'::text, 'type'::text])))
);


--
-- Name: object_bibliotheek_entry; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.object_bibliotheek_entry (
    search_index tsvector,
    search_term text NOT NULL,
    object_type text NOT NULL,
    searchable_id integer DEFAULT nextval('public.searchable_searchable_id_seq'::regclass),
    id integer NOT NULL,
    bibliotheek_categorie_id integer,
    object_uuid uuid NOT NULL,
    name text NOT NULL
)
INHERITS (public.searchable);


--
-- Name: object_bibliotheek_entry_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.object_bibliotheek_entry_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: object_bibliotheek_entry_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.object_bibliotheek_entry_id_seq OWNED BY public.object_bibliotheek_entry.id;


--
-- Name: object_mutation; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.object_mutation (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    object_uuid uuid,
    object_type text DEFAULT 'object'::text,
    lock_object_uuid uuid,
    type text NOT NULL,
    "values" text DEFAULT '{}'::text NOT NULL,
    date_created timestamp without time zone DEFAULT now(),
    subject_id integer NOT NULL,
    executed boolean DEFAULT false NOT NULL,
    CONSTRAINT object_mutation_type_check CHECK ((type = ANY (ARRAY['create'::text, 'update'::text, 'delete'::text, 'relate'::text])))
);


--
-- Name: object_relation; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.object_relation (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    name text NOT NULL,
    object_type text NOT NULL,
    object_uuid uuid,
    object_embedding text,
    object_id uuid,
    object_preview text,
    CONSTRAINT object_relation_ref_xor_embed CHECK (((object_uuid IS NULL) <> (object_embedding IS NULL)))
);


--
-- Name: object_relationships; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.object_relationships (
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    object1_uuid uuid NOT NULL,
    object2_uuid uuid NOT NULL,
    type1 text NOT NULL,
    type2 text NOT NULL,
    object1_type text NOT NULL,
    object2_type text NOT NULL,
    blocks_deletion boolean DEFAULT false NOT NULL,
    title1 text,
    title2 text,
    owner_object_uuid uuid
);


--
-- Name: object_subscription; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.object_subscription (
    id integer NOT NULL,
    interface_id integer NOT NULL,
    external_id character varying(255) NOT NULL,
    local_table character varying(100) NOT NULL,
    local_id character varying(255) NOT NULL,
    date_created timestamp without time zone DEFAULT now(),
    date_deleted timestamp without time zone,
    object_preview text,
    config_interface_id integer
);


--
-- Name: object_subscription_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.object_subscription_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: object_subscription_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.object_subscription_id_seq OWNED BY public.object_subscription.id;


--
-- Name: parkeergebied; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.parkeergebied (
    id integer NOT NULL,
    bag_hoofdadres bigint,
    postcode character varying(6) NOT NULL,
    straatnaam character varying(255) NOT NULL,
    huisnummer integer,
    huisletter character varying(8),
    huisnummertoevoeging character varying(4),
    parkeergebied_id integer,
    parkeergebied character varying(255),
    created timestamp without time zone,
    last_modified timestamp without time zone,
    woonplaats character varying(255)
);


--
-- Name: parkeergebied_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.parkeergebied_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: parkeergebied_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.parkeergebied_id_seq OWNED BY public.parkeergebied.id;


--
-- Name: queue; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.queue (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    object_id uuid,
    status text DEFAULT 'pending'::text NOT NULL,
    type text NOT NULL,
    label text NOT NULL,
    data text DEFAULT '{}'::text NOT NULL,
    date_created timestamp without time zone DEFAULT statement_timestamp() NOT NULL,
    date_started timestamp without time zone,
    date_finished timestamp without time zone,
    parent_id uuid,
    priority integer DEFAULT 1000 NOT NULL,
    metadata text DEFAULT '{}'::text NOT NULL,
    CONSTRAINT queue_status_check1 CHECK ((status = ANY (ARRAY['pending'::text, 'running'::text, 'finished'::text, 'failed'::text, 'waiting'::text, 'cancelled'::text, 'postponed'::text])))
);


--
-- Name: queue_partitioned; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.queue_partitioned (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    object_id uuid,
    status text DEFAULT 'pending'::text NOT NULL,
    type text NOT NULL,
    label text NOT NULL,
    data text DEFAULT '{}'::text NOT NULL,
    date_created timestamp without time zone DEFAULT statement_timestamp() NOT NULL,
    date_started timestamp without time zone,
    date_finished timestamp without time zone,
    parent_id uuid,
    priority integer DEFAULT 1000 NOT NULL,
    metadata text DEFAULT '{}'::text NOT NULL,
    CONSTRAINT queue_status_check CHECK ((status = ANY (ARRAY['pending'::text, 'running'::text, 'finished'::text, 'failed'::text, 'waiting'::text, 'cancelled'::text, 'postponed'::text])))
)
PARTITION BY RANGE (date_created);


--
-- Name: queue_20210101; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.queue_20210101 (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    object_id uuid,
    status text DEFAULT 'pending'::text NOT NULL,
    type text NOT NULL,
    label text NOT NULL,
    data text DEFAULT '{}'::text NOT NULL,
    date_created timestamp without time zone DEFAULT statement_timestamp() NOT NULL,
    date_started timestamp without time zone,
    date_finished timestamp without time zone,
    parent_id uuid,
    priority integer DEFAULT 1000 NOT NULL,
    metadata text DEFAULT '{}'::text NOT NULL,
    CONSTRAINT queue_status_check CHECK ((status = ANY (ARRAY['pending'::text, 'running'::text, 'finished'::text, 'failed'::text, 'waiting'::text, 'cancelled'::text, 'postponed'::text])))
);
ALTER TABLE ONLY public.queue_partitioned ATTACH PARTITION public.queue_20210101 FOR VALUES FROM ('2021-01-01 00:00:00') TO ('2021-02-01 00:00:00');


--
-- Name: queue_20210201; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.queue_20210201 (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    object_id uuid,
    status text DEFAULT 'pending'::text NOT NULL,
    type text NOT NULL,
    label text NOT NULL,
    data text DEFAULT '{}'::text NOT NULL,
    date_created timestamp without time zone DEFAULT statement_timestamp() NOT NULL,
    date_started timestamp without time zone,
    date_finished timestamp without time zone,
    parent_id uuid,
    priority integer DEFAULT 1000 NOT NULL,
    metadata text DEFAULT '{}'::text NOT NULL,
    CONSTRAINT queue_status_check CHECK ((status = ANY (ARRAY['pending'::text, 'running'::text, 'finished'::text, 'failed'::text, 'waiting'::text, 'cancelled'::text, 'postponed'::text])))
);
ALTER TABLE ONLY public.queue_partitioned ATTACH PARTITION public.queue_20210201 FOR VALUES FROM ('2021-02-01 00:00:00') TO ('2021-03-01 00:00:00');


--
-- Name: queue_20210301; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.queue_20210301 (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    object_id uuid,
    status text DEFAULT 'pending'::text NOT NULL,
    type text NOT NULL,
    label text NOT NULL,
    data text DEFAULT '{}'::text NOT NULL,
    date_created timestamp without time zone DEFAULT statement_timestamp() NOT NULL,
    date_started timestamp without time zone,
    date_finished timestamp without time zone,
    parent_id uuid,
    priority integer DEFAULT 1000 NOT NULL,
    metadata text DEFAULT '{}'::text NOT NULL,
    CONSTRAINT queue_status_check CHECK ((status = ANY (ARRAY['pending'::text, 'running'::text, 'finished'::text, 'failed'::text, 'waiting'::text, 'cancelled'::text, 'postponed'::text])))
);
ALTER TABLE ONLY public.queue_partitioned ATTACH PARTITION public.queue_20210301 FOR VALUES FROM ('2021-03-01 00:00:00') TO ('2021-04-01 00:00:00');


--
-- Name: queue_20210401; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.queue_20210401 (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    object_id uuid,
    status text DEFAULT 'pending'::text NOT NULL,
    type text NOT NULL,
    label text NOT NULL,
    data text DEFAULT '{}'::text NOT NULL,
    date_created timestamp without time zone DEFAULT statement_timestamp() NOT NULL,
    date_started timestamp without time zone,
    date_finished timestamp without time zone,
    parent_id uuid,
    priority integer DEFAULT 1000 NOT NULL,
    metadata text DEFAULT '{}'::text NOT NULL,
    CONSTRAINT queue_status_check CHECK ((status = ANY (ARRAY['pending'::text, 'running'::text, 'finished'::text, 'failed'::text, 'waiting'::text, 'cancelled'::text, 'postponed'::text])))
);
ALTER TABLE ONLY public.queue_partitioned ATTACH PARTITION public.queue_20210401 FOR VALUES FROM ('2021-04-01 00:00:00') TO ('2021-05-01 00:00:00');


--
-- Name: queue_20210501; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.queue_20210501 (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    object_id uuid,
    status text DEFAULT 'pending'::text NOT NULL,
    type text NOT NULL,
    label text NOT NULL,
    data text DEFAULT '{}'::text NOT NULL,
    date_created timestamp without time zone DEFAULT statement_timestamp() NOT NULL,
    date_started timestamp without time zone,
    date_finished timestamp without time zone,
    parent_id uuid,
    priority integer DEFAULT 1000 NOT NULL,
    metadata text DEFAULT '{}'::text NOT NULL,
    CONSTRAINT queue_status_check CHECK ((status = ANY (ARRAY['pending'::text, 'running'::text, 'finished'::text, 'failed'::text, 'waiting'::text, 'cancelled'::text, 'postponed'::text])))
);
ALTER TABLE ONLY public.queue_partitioned ATTACH PARTITION public.queue_20210501 FOR VALUES FROM ('2021-05-01 00:00:00') TO ('2021-06-01 00:00:00');


--
-- Name: queue_20210601; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.queue_20210601 (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    object_id uuid,
    status text DEFAULT 'pending'::text NOT NULL,
    type text NOT NULL,
    label text NOT NULL,
    data text DEFAULT '{}'::text NOT NULL,
    date_created timestamp without time zone DEFAULT statement_timestamp() NOT NULL,
    date_started timestamp without time zone,
    date_finished timestamp without time zone,
    parent_id uuid,
    priority integer DEFAULT 1000 NOT NULL,
    metadata text DEFAULT '{}'::text NOT NULL,
    CONSTRAINT queue_status_check CHECK ((status = ANY (ARRAY['pending'::text, 'running'::text, 'finished'::text, 'failed'::text, 'waiting'::text, 'cancelled'::text, 'postponed'::text])))
);
ALTER TABLE ONLY public.queue_partitioned ATTACH PARTITION public.queue_20210601 FOR VALUES FROM ('2021-06-01 00:00:00') TO ('2021-07-01 00:00:00');


--
-- Name: queue_20210701; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.queue_20210701 (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    object_id uuid,
    status text DEFAULT 'pending'::text NOT NULL,
    type text NOT NULL,
    label text NOT NULL,
    data text DEFAULT '{}'::text NOT NULL,
    date_created timestamp without time zone DEFAULT statement_timestamp() NOT NULL,
    date_started timestamp without time zone,
    date_finished timestamp without time zone,
    parent_id uuid,
    priority integer DEFAULT 1000 NOT NULL,
    metadata text DEFAULT '{}'::text NOT NULL,
    CONSTRAINT queue_status_check CHECK ((status = ANY (ARRAY['pending'::text, 'running'::text, 'finished'::text, 'failed'::text, 'waiting'::text, 'cancelled'::text, 'postponed'::text])))
);
ALTER TABLE ONLY public.queue_partitioned ATTACH PARTITION public.queue_20210701 FOR VALUES FROM ('2021-07-01 00:00:00') TO ('2021-08-01 00:00:00');


--
-- Name: queue_20210801; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.queue_20210801 (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    object_id uuid,
    status text DEFAULT 'pending'::text NOT NULL,
    type text NOT NULL,
    label text NOT NULL,
    data text DEFAULT '{}'::text NOT NULL,
    date_created timestamp without time zone DEFAULT statement_timestamp() NOT NULL,
    date_started timestamp without time zone,
    date_finished timestamp without time zone,
    parent_id uuid,
    priority integer DEFAULT 1000 NOT NULL,
    metadata text DEFAULT '{}'::text NOT NULL,
    CONSTRAINT queue_status_check CHECK ((status = ANY (ARRAY['pending'::text, 'running'::text, 'finished'::text, 'failed'::text, 'waiting'::text, 'cancelled'::text, 'postponed'::text])))
);
ALTER TABLE ONLY public.queue_partitioned ATTACH PARTITION public.queue_20210801 FOR VALUES FROM ('2021-08-01 00:00:00') TO ('2021-09-01 00:00:00');


--
-- Name: queue_20210901; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.queue_20210901 (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    object_id uuid,
    status text DEFAULT 'pending'::text NOT NULL,
    type text NOT NULL,
    label text NOT NULL,
    data text DEFAULT '{}'::text NOT NULL,
    date_created timestamp without time zone DEFAULT statement_timestamp() NOT NULL,
    date_started timestamp without time zone,
    date_finished timestamp without time zone,
    parent_id uuid,
    priority integer DEFAULT 1000 NOT NULL,
    metadata text DEFAULT '{}'::text NOT NULL,
    CONSTRAINT queue_status_check CHECK ((status = ANY (ARRAY['pending'::text, 'running'::text, 'finished'::text, 'failed'::text, 'waiting'::text, 'cancelled'::text, 'postponed'::text])))
);
ALTER TABLE ONLY public.queue_partitioned ATTACH PARTITION public.queue_20210901 FOR VALUES FROM ('2021-09-01 00:00:00') TO ('2021-10-01 00:00:00');


--
-- Name: queue_20211001; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.queue_20211001 (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    object_id uuid,
    status text DEFAULT 'pending'::text NOT NULL,
    type text NOT NULL,
    label text NOT NULL,
    data text DEFAULT '{}'::text NOT NULL,
    date_created timestamp without time zone DEFAULT statement_timestamp() NOT NULL,
    date_started timestamp without time zone,
    date_finished timestamp without time zone,
    parent_id uuid,
    priority integer DEFAULT 1000 NOT NULL,
    metadata text DEFAULT '{}'::text NOT NULL,
    CONSTRAINT queue_status_check CHECK ((status = ANY (ARRAY['pending'::text, 'running'::text, 'finished'::text, 'failed'::text, 'waiting'::text, 'cancelled'::text, 'postponed'::text])))
);
ALTER TABLE ONLY public.queue_partitioned ATTACH PARTITION public.queue_20211001 FOR VALUES FROM ('2021-10-01 00:00:00') TO ('2021-11-01 00:00:00');


--
-- Name: queue_20211101; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.queue_20211101 (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    object_id uuid,
    status text DEFAULT 'pending'::text NOT NULL,
    type text NOT NULL,
    label text NOT NULL,
    data text DEFAULT '{}'::text NOT NULL,
    date_created timestamp without time zone DEFAULT statement_timestamp() NOT NULL,
    date_started timestamp without time zone,
    date_finished timestamp without time zone,
    parent_id uuid,
    priority integer DEFAULT 1000 NOT NULL,
    metadata text DEFAULT '{}'::text NOT NULL,
    CONSTRAINT queue_status_check CHECK ((status = ANY (ARRAY['pending'::text, 'running'::text, 'finished'::text, 'failed'::text, 'waiting'::text, 'cancelled'::text, 'postponed'::text])))
);
ALTER TABLE ONLY public.queue_partitioned ATTACH PARTITION public.queue_20211101 FOR VALUES FROM ('2021-11-01 00:00:00') TO ('2021-12-01 00:00:00');


--
-- Name: queue_20211201; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.queue_20211201 (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    object_id uuid,
    status text DEFAULT 'pending'::text NOT NULL,
    type text NOT NULL,
    label text NOT NULL,
    data text DEFAULT '{}'::text NOT NULL,
    date_created timestamp without time zone DEFAULT statement_timestamp() NOT NULL,
    date_started timestamp without time zone,
    date_finished timestamp without time zone,
    parent_id uuid,
    priority integer DEFAULT 1000 NOT NULL,
    metadata text DEFAULT '{}'::text NOT NULL,
    CONSTRAINT queue_status_check CHECK ((status = ANY (ARRAY['pending'::text, 'running'::text, 'finished'::text, 'failed'::text, 'waiting'::text, 'cancelled'::text, 'postponed'::text])))
);
ALTER TABLE ONLY public.queue_partitioned ATTACH PARTITION public.queue_20211201 FOR VALUES FROM ('2021-12-01 00:00:00') TO ('2022-01-01 00:00:00');


--
-- Name: queue_20xx; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.queue_20xx (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    object_id uuid,
    status text DEFAULT 'pending'::text NOT NULL,
    type text NOT NULL,
    label text NOT NULL,
    data text DEFAULT '{}'::text NOT NULL,
    date_created timestamp without time zone DEFAULT statement_timestamp() NOT NULL,
    date_started timestamp without time zone,
    date_finished timestamp without time zone,
    parent_id uuid,
    priority integer DEFAULT 1000 NOT NULL,
    metadata text DEFAULT '{}'::text NOT NULL,
    CONSTRAINT queue_status_check CHECK ((status = ANY (ARRAY['pending'::text, 'running'::text, 'finished'::text, 'failed'::text, 'waiting'::text, 'cancelled'::text, 'postponed'::text])))
);
ALTER TABLE ONLY public.queue_partitioned ATTACH PARTITION public.queue_20xx FOR VALUES FROM ('1970-01-01 00:00:00') TO ('2021-01-01 00:00:00');


--
-- Name: remote_api_keys; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.remote_api_keys (
    id integer NOT NULL,
    key character varying(60) NOT NULL,
    permissions text NOT NULL
);


--
-- Name: remote_api_keys_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.remote_api_keys_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: remote_api_keys_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.remote_api_keys_id_seq OWNED BY public.remote_api_keys.id;


--
-- Name: result_preservation_terms_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.result_preservation_terms_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: result_preservation_terms_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.result_preservation_terms_id_seq OWNED BY public.result_preservation_terms.id;


--
-- Name: rights; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.rights (
    name text NOT NULL,
    description text NOT NULL
);


--
-- Name: role_rights; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.role_rights (
    rights_name text NOT NULL,
    role_id integer NOT NULL
);


--
-- Name: roles_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.roles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: roles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.roles_id_seq OWNED BY public.roles.id;


--
-- Name: sbus_logging; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.sbus_logging (
    id integer NOT NULL,
    sbus_traffic_id integer,
    pid integer,
    mutatie_type text,
    object text,
    params text,
    kerngegeven text,
    label text,
    changes text,
    error boolean,
    error_message text,
    created timestamp without time zone,
    modified timestamp without time zone
);


--
-- Name: sbus_logging_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.sbus_logging_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: sbus_logging_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.sbus_logging_id_seq OWNED BY public.sbus_logging.id;


--
-- Name: sbus_traffic; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.sbus_traffic (
    id integer NOT NULL,
    sbus_type text,
    object text,
    operation text,
    input text,
    input_raw text,
    output text,
    output_raw text,
    error boolean,
    error_message text,
    created timestamp without time zone,
    modified timestamp without time zone
);


--
-- Name: sbus_traffic_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.sbus_traffic_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: sbus_traffic_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.sbus_traffic_id_seq OWNED BY public.sbus_traffic.id;


--
-- Name: scheduled_jobs; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.scheduled_jobs (
    id integer NOT NULL,
    task character varying(255) NOT NULL,
    scheduled_for timestamp without time zone,
    parameters text,
    created timestamp without time zone,
    last_modified timestamp without time zone,
    deleted timestamp without time zone,
    schedule_type character varying(16),
    case_id integer,
    uuid uuid DEFAULT public.uuid_generate_v4(),
    CONSTRAINT scheduled_jobs_schedule_type_check CHECK (((schedule_type)::text ~ '(manual|time)'::text))
);


--
-- Name: scheduled_jobs_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.scheduled_jobs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: scheduled_jobs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.scheduled_jobs_id_seq OWNED BY public.scheduled_jobs.id;


--
-- Name: search_query; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.search_query (
    id integer NOT NULL,
    settings text,
    ldap_id integer,
    name character varying(256),
    sort_index integer
);


--
-- Name: search_query_delen; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.search_query_delen (
    id integer NOT NULL,
    search_query_id integer,
    ou_id integer,
    role_id integer
);


--
-- Name: search_query_delen_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.search_query_delen_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: search_query_delen_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.search_query_delen_id_seq OWNED BY public.search_query_delen.id;


--
-- Name: search_query_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.search_query_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: search_query_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.search_query_id_seq OWNED BY public.search_query.id;


--
-- Name: service_geojson; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.service_geojson (
    id integer NOT NULL,
    uuid uuid NOT NULL,
    geo_json jsonb NOT NULL
);


--
-- Name: service_geojson_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

ALTER TABLE public.service_geojson ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.service_geojson_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: service_geojson_relationship; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.service_geojson_relationship (
    id integer NOT NULL,
    service_geojson_id integer NOT NULL,
    related_uuid uuid NOT NULL,
    uuid uuid NOT NULL
);


--
-- Name: service_geojson_relationship_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

ALTER TABLE public.service_geojson_relationship ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.service_geojson_relationship_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: session_invitation; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.session_invitation (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    subject_id uuid NOT NULL,
    object_id uuid,
    object_type text,
    date_created timestamp without time zone DEFAULT now() NOT NULL,
    date_expires timestamp without time zone NOT NULL,
    token text NOT NULL,
    action_path text,
    CONSTRAINT object_reference CHECK ((((object_id IS NOT NULL) AND (object_type IS NOT NULL)) OR ((object_id IS NULL) AND (object_type IS NULL))))
);


--
-- Name: settings; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.settings (
    id integer NOT NULL,
    key character varying(255),
    value text
);


--
-- Name: settings_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.settings_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: settings_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.settings_id_seq OWNED BY public.settings.id;


--
-- Name: subject_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.subject_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: subject_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.subject_id_seq OWNED BY public.subject.id;


--
-- Name: subject_login_history; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.subject_login_history (
    id integer NOT NULL,
    ip inet NOT NULL,
    subject_id integer NOT NULL,
    subject_uuid uuid NOT NULL,
    success boolean DEFAULT true NOT NULL,
    method text NOT NULL,
    date_attempt timestamp with time zone DEFAULT now() NOT NULL
);


--
-- Name: subject_login_history_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.subject_login_history_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: subject_login_history_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.subject_login_history_id_seq OWNED BY public.subject_login_history.id;


--
-- Name: thread; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.thread (
    id integer NOT NULL,
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    contact_uuid uuid,
    contact_displayname text,
    case_id integer,
    created timestamp without time zone NOT NULL,
    last_modified timestamp without time zone,
    thread_type text NOT NULL,
    last_message_cache text NOT NULL,
    message_count integer DEFAULT 0 NOT NULL,
    unread_pip_count integer DEFAULT 0 NOT NULL,
    unread_employee_count integer DEFAULT 0 NOT NULL,
    attachment_count integer DEFAULT 0 NOT NULL,
    CONSTRAINT message_count_positive CHECK ((message_count >= 0)),
    CONSTRAINT message_type_check CHECK ((thread_type = ANY (ARRAY['note'::text, 'contact_moment'::text, 'external'::text])))
);


--
-- Name: thread_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.thread_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: thread_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.thread_id_seq OWNED BY public.thread.id;


--
-- Name: thread_message; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.thread_message (
    id integer NOT NULL,
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    thread_id integer NOT NULL,
    type text NOT NULL,
    message_slug text NOT NULL,
    created_by_uuid uuid,
    created_by_displayname text,
    created timestamp without time zone NOT NULL,
    last_modified timestamp without time zone,
    thread_message_note_id integer,
    thread_message_contact_moment_id integer,
    thread_message_external_id integer,
    message_date timestamp without time zone,
    CONSTRAINT check_exactly_one_column CHECK ((((
CASE
    WHEN (thread_message_note_id IS NULL) THEN 0
    ELSE 1
END +
CASE
    WHEN (thread_message_contact_moment_id IS NULL) THEN 0
    ELSE 1
END) +
CASE
    WHEN (thread_message_external_id IS NULL) THEN 0
    ELSE 1
END) = 1)),
    CONSTRAINT message_type_check CHECK ((type = ANY (ARRAY['note'::text, 'contact_moment'::text, 'external'::text])))
);


--
-- Name: thread_message_attachment; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.thread_message_attachment (
    id integer NOT NULL,
    filestore_id integer NOT NULL,
    thread_message_id integer NOT NULL,
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    filename character varying NOT NULL
);


--
-- Name: thread_message_attachment_derivative; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.thread_message_attachment_derivative (
    id integer NOT NULL,
    thread_message_attachment_id integer NOT NULL,
    filestore_id integer NOT NULL,
    max_width integer NOT NULL,
    max_height integer NOT NULL,
    date_generated timestamp without time zone DEFAULT now() NOT NULL,
    type text NOT NULL,
    CONSTRAINT file_derivative_type CHECK ((type = ANY (ARRAY['pdf'::text, 'thumbnail'::text, 'doc'::text, 'docx'::text])))
);


--
-- Name: thread_message_attachment_derivative_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.thread_message_attachment_derivative_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: thread_message_attachment_derivative_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.thread_message_attachment_derivative_id_seq OWNED BY public.thread_message_attachment_derivative.id;


--
-- Name: thread_message_attachment_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.thread_message_attachment_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: thread_message_attachment_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.thread_message_attachment_id_seq OWNED BY public.thread_message_attachment.id;


--
-- Name: thread_message_contact_moment; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.thread_message_contact_moment (
    id integer NOT NULL,
    content text NOT NULL,
    contact_channel text NOT NULL,
    direction text,
    recipient_uuid uuid NOT NULL,
    recipient_displayname text NOT NULL,
    CONSTRAINT contact_moment_channels CHECK ((contact_channel = ANY (ARRAY['assignee'::text, 'frontdesk'::text, 'phone'::text, 'mail'::text, 'email'::text, 'webform'::text, 'social_media'::text, 'external_application'::text]))),
    CONSTRAINT message_direction_check CHECK ((direction = ANY (ARRAY['incoming'::text, 'outgoing'::text])))
);


--
-- Name: thread_message_contact_moment_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.thread_message_contact_moment_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: thread_message_contact_moment_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.thread_message_contact_moment_id_seq OWNED BY public.thread_message_contact_moment.id;


--
-- Name: thread_message_external; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.thread_message_external (
    id integer NOT NULL,
    type text NOT NULL,
    content text NOT NULL,
    subject text NOT NULL,
    participants text DEFAULT '[]'::text NOT NULL,
    direction text NOT NULL,
    source_file_id integer,
    read_pip timestamp without time zone,
    read_employee timestamp without time zone,
    attachment_count integer DEFAULT 0 NOT NULL,
    failure_reason text,
    CONSTRAINT external_message_type CHECK ((type = ANY (ARRAY['pip'::text, 'email'::text, 'postex'::text]))),
    CONSTRAINT thread_message_external_direction_constraint CHECK ((direction = ANY (ARRAY['incoming'::text, 'outgoing'::text, 'unspecified'::text])))
);


--
-- Name: thread_message_external_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.thread_message_external_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: thread_message_external_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.thread_message_external_id_seq OWNED BY public.thread_message_external.id;


--
-- Name: thread_message_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.thread_message_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: thread_message_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.thread_message_id_seq OWNED BY public.thread_message.id;


--
-- Name: thread_message_note; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.thread_message_note (
    id integer NOT NULL,
    content text NOT NULL
);


--
-- Name: thread_message_note_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.thread_message_note_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: thread_message_note_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.thread_message_note_id_seq OWNED BY public.thread_message_note.id;


--
-- Name: transaction; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.transaction (
    id integer NOT NULL,
    interface_id integer NOT NULL,
    external_transaction_id character varying(250),
    input_data text,
    input_file integer,
    automated_retry_count integer,
    date_created timestamp without time zone DEFAULT now() NOT NULL,
    date_last_retry timestamp without time zone,
    date_next_retry timestamp without time zone,
    processed boolean DEFAULT false,
    date_deleted timestamp without time zone,
    error_count integer DEFAULT 0,
    direction character varying(255) DEFAULT 'incoming'::character varying NOT NULL,
    success_count integer DEFAULT 0,
    total_count integer DEFAULT 0,
    processor_params text,
    error_fatal boolean,
    preview_data text DEFAULT '{}'::text NOT NULL,
    error_message text,
    text_vector tsvector,
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    CONSTRAINT input_data_or_input_file CHECK (((input_data IS NOT NULL) OR (input_file IS NOT NULL))),
    CONSTRAINT transaction_direction_check CHECK ((((direction)::text = 'incoming'::text) OR ((direction)::text = 'outgoing'::text)))
);


--
-- Name: transaction_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.transaction_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: transaction_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.transaction_id_seq OWNED BY public.transaction.id;


--
-- Name: transaction_record; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.transaction_record (
    id integer NOT NULL,
    transaction_id integer,
    input text NOT NULL,
    output text NOT NULL,
    is_error boolean DEFAULT false NOT NULL,
    date_executed timestamp without time zone DEFAULT now() NOT NULL,
    date_deleted timestamp without time zone,
    preview_string text,
    last_error text,
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL
);


--
-- Name: transaction_record_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.transaction_record_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: transaction_record_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.transaction_record_id_seq OWNED BY public.transaction_record.id;


--
-- Name: transaction_record_to_object; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.transaction_record_to_object (
    id integer NOT NULL,
    transaction_record_id integer,
    local_table character varying(100),
    local_id character varying(255),
    mutations text,
    date_deleted timestamp without time zone,
    mutation_type character varying(100)
);


--
-- Name: transaction_record_to_object_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.transaction_record_to_object_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: transaction_record_to_object_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.transaction_record_to_object_id_seq OWNED BY public.transaction_record_to_object.id;


--
-- Name: user_app_lock; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.user_app_lock (
    type character(40) NOT NULL,
    type_id character(20) NOT NULL,
    create_unixtime integer NOT NULL,
    session_id character(40) NOT NULL,
    uidnumber integer NOT NULL
);


--
-- Name: user_entity; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.user_entity (
    id integer NOT NULL,
    uuid uuid DEFAULT public.uuid_generate_v4(),
    source_interface_id integer,
    source_identifier text NOT NULL,
    subject_id integer,
    date_created timestamp without time zone,
    date_deleted timestamp without time zone,
    properties text DEFAULT '{}'::text,
    password character varying(255),
    active boolean DEFAULT true NOT NULL
);


--
-- Name: user_entity_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.user_entity_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: user_entity_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.user_entity_id_seq OWNED BY public.user_entity.id;


--
-- Name: woz_objects; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.woz_objects (
    id integer NOT NULL,
    object_data text,
    owner character varying(255) NOT NULL,
    object_id character varying(32) NOT NULL
);


--
-- Name: woz_objects_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.woz_objects_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: woz_objects_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.woz_objects_id_seq OWNED BY public.woz_objects.id;


--
-- Name: zaak_authorisation_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.zaak_authorisation_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: zaak_authorisation_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.zaak_authorisation_id_seq OWNED BY public.zaak_authorisation.id;


--
-- Name: zaak_bag_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.zaak_bag_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: zaak_bag_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.zaak_bag_id_seq OWNED BY public.zaak_bag.id;


--
-- Name: zaak_betrokkenen_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.zaak_betrokkenen_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: zaak_betrokkenen_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.zaak_betrokkenen_id_seq OWNED BY public.zaak_betrokkenen.id;


--
-- Name: zaak_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.zaak_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: zaak_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.zaak_id_seq OWNED BY public.zaak.id;


--
-- Name: zaak_kenmerk_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.zaak_kenmerk_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: zaak_kenmerk_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.zaak_kenmerk_id_seq OWNED BY public.zaak_kenmerk.id;


--
-- Name: zaak_meta_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.zaak_meta_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: zaak_meta_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.zaak_meta_id_seq OWNED BY public.zaak_meta.id;


--
-- Name: zaak_onafgerond; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.zaak_onafgerond (
    zaaktype_id integer NOT NULL,
    betrokkene character(50) NOT NULL,
    json_string text NOT NULL,
    afronden boolean,
    create_unixtime integer
);


--
-- Name: zaak_subcase; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.zaak_subcase (
    id integer NOT NULL,
    zaak_id integer NOT NULL,
    relation_zaak_id integer,
    required integer
);


--
-- Name: zaak_subcase_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.zaak_subcase_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: zaak_subcase_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.zaak_subcase_id_seq OWNED BY public.zaak_subcase.id;


--
-- Name: zaaktype_authorisation_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.zaaktype_authorisation_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: zaaktype_authorisation_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.zaaktype_authorisation_id_seq OWNED BY public.zaaktype_authorisation.id;


--
-- Name: zaaktype_betrokkenen; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.zaaktype_betrokkenen (
    id integer NOT NULL,
    zaaktype_node_id integer,
    betrokkene_type text,
    created timestamp without time zone,
    last_modified timestamp without time zone
);


--
-- Name: zaaktype_betrokkenen_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.zaaktype_betrokkenen_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: zaaktype_betrokkenen_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.zaaktype_betrokkenen_id_seq OWNED BY public.zaaktype_betrokkenen.id;


--
-- Name: zaaktype_definitie_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.zaaktype_definitie_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: zaaktype_definitie_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.zaaktype_definitie_id_seq OWNED BY public.zaaktype_definitie.id;


--
-- Name: zaaktype_document_kenmerken_map; Type: MATERIALIZED VIEW; Schema: public; Owner: -
--

CREATE MATERIALIZED VIEW public.zaaktype_document_kenmerken_map AS
 SELECT t.zaaktype_node_id,
    t.referential,
    t.bibliotheek_kenmerken_id,
    t.magic_string,
    t.name,
    t.public_name,
    t.case_document_uuid,
    t.case_document_id,
    t.show_on_pip,
    t.show_on_website
   FROM ( SELECT ztk.zaaktype_node_id,
            ztk.referential,
            ztk.bibliotheek_kenmerken_id,
            (COALESCE(ztk.pip, 0))::boolean AS show_on_pip,
            (COALESCE(ztk.publish_public, 0))::boolean AS show_on_website,
            bk.magic_string,
            bk.naam AS name,
            COALESCE(NULLIF(ztk.label, ''::text), (bk.naam)::text) AS public_name,
            ztk.uuid AS case_document_uuid,
            ztk.id AS case_document_id,
            row_number() OVER (PARTITION BY ztk.bibliotheek_kenmerken_id, ztk.zaaktype_node_id ORDER BY zs.status, ztk.id) AS row_num
           FROM ((public.zaaktype_kenmerken ztk
             JOIN public.zaaktype_status zs ON ((zs.id = ztk.zaak_status_id)))
             JOIN public.bibliotheek_kenmerken bk ON (((bk.id = ztk.bibliotheek_kenmerken_id) AND (bk.value_type = 'file'::text))))) t
  WHERE (t.row_num = 1)
  WITH NO DATA;


--
-- Name: zaaktype_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.zaaktype_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: zaaktype_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.zaaktype_id_seq OWNED BY public.zaaktype.id;


--
-- Name: zaaktype_kenmerken_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.zaaktype_kenmerken_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: zaaktype_kenmerken_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.zaaktype_kenmerken_id_seq OWNED BY public.zaaktype_kenmerken.id;


--
-- Name: zaaktype_node_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.zaaktype_node_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: zaaktype_node_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.zaaktype_node_id_seq OWNED BY public.zaaktype_node.id;


--
-- Name: zaaktype_notificatie; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.zaaktype_notificatie (
    id integer NOT NULL,
    zaaktype_node_id integer,
    zaak_status_id integer,
    label text,
    rcpt text,
    onderwerp text,
    bericht text,
    created timestamp without time zone,
    last_modified timestamp without time zone,
    intern_block integer,
    email text,
    bibliotheek_notificaties_id integer,
    behandelaar character varying(255),
    automatic integer,
    cc text,
    bcc text,
    betrokkene_role text
);


--
-- Name: zaaktype_notificatie_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.zaaktype_notificatie_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: zaaktype_notificatie_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.zaaktype_notificatie_id_seq OWNED BY public.zaaktype_notificatie.id;


--
-- Name: zaaktype_regel; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.zaaktype_regel (
    id integer NOT NULL,
    zaaktype_node_id integer,
    zaak_status_id integer,
    naam text,
    created timestamp without time zone,
    last_modified timestamp without time zone,
    settings text,
    active boolean DEFAULT false,
    is_group boolean DEFAULT false NOT NULL
);


--
-- Name: zaaktype_regel_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.zaaktype_regel_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: zaaktype_regel_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.zaaktype_regel_id_seq OWNED BY public.zaaktype_regel.id;


--
-- Name: zaaktype_relatie; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.zaaktype_relatie (
    id integer NOT NULL,
    zaaktype_node_id integer,
    relatie_zaaktype_id integer,
    zaaktype_status_id integer,
    relatie_type text,
    eigenaar_type text DEFAULT 'aanvrager'::text NOT NULL,
    start_delay character varying(255),
    created timestamp without time zone,
    last_modified timestamp without time zone,
    kopieren_kenmerken integer,
    ou_id integer,
    role_id integer,
    automatisch_behandelen boolean,
    required character varying(12),
    betrokkene_authorized boolean,
    betrokkene_notify boolean,
    betrokkene_id text,
    betrokkene_role text,
    betrokkene_role_set text,
    betrokkene_prefix text,
    eigenaar_id text,
    eigenaar_role text,
    show_in_pip boolean DEFAULT false NOT NULL,
    pip_label text,
    subject_role text[],
    copy_subject_role boolean DEFAULT false,
    copy_related_cases boolean DEFAULT false NOT NULL,
    copy_related_objects boolean DEFAULT false NOT NULL,
    copy_selected_attributes public.hstore,
    creation_style character varying DEFAULT 'background'::character varying NOT NULL,
    status boolean DEFAULT false NOT NULL,
    CONSTRAINT zaaktype_relatie_creation_style_check CHECK (((creation_style)::text = ANY (ARRAY[('background'::character varying)::text, ('form'::character varying)::text])))
);


--
-- Name: zaaktype_relatie_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.zaaktype_relatie_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: zaaktype_relatie_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.zaaktype_relatie_id_seq OWNED BY public.zaaktype_relatie.id;


--
-- Name: zaaktype_resultaten_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.zaaktype_resultaten_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: zaaktype_resultaten_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.zaaktype_resultaten_id_seq OWNED BY public.zaaktype_resultaten.id;


--
-- Name: zaaktype_sjablonen; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.zaaktype_sjablonen (
    id integer NOT NULL,
    zaaktype_node_id integer,
    bibliotheek_sjablonen_id integer,
    help text,
    zaak_status_id integer,
    created timestamp without time zone,
    last_modified timestamp without time zone,
    automatisch_genereren integer,
    bibliotheek_kenmerken_id integer,
    target_format character varying(5),
    custom_filename text,
    label text
);


--
-- Name: zaaktype_sjablonen_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.zaaktype_sjablonen_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: zaaktype_sjablonen_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.zaaktype_sjablonen_id_seq OWNED BY public.zaaktype_sjablonen.id;


--
-- Name: zaaktype_standaard_betrokkenen; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.zaaktype_standaard_betrokkenen (
    id integer NOT NULL,
    zaaktype_node_id integer NOT NULL,
    zaak_status_id integer NOT NULL,
    betrokkene_type text NOT NULL,
    betrokkene_identifier text NOT NULL,
    naam text NOT NULL,
    rol text NOT NULL,
    magic_string_prefix text,
    gemachtigd boolean DEFAULT false NOT NULL,
    notify boolean DEFAULT false NOT NULL,
    uuid uuid DEFAULT public.uuid_generate_v4()
);


--
-- Name: zaaktype_standaard_betrokkenen_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.zaaktype_standaard_betrokkenen_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: zaaktype_standaard_betrokkenen_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.zaaktype_standaard_betrokkenen_id_seq OWNED BY public.zaaktype_standaard_betrokkenen.id;


--
-- Name: zaaktype_status_checklist_item; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.zaaktype_status_checklist_item (
    id integer NOT NULL,
    casetype_status_id integer NOT NULL,
    label text,
    external_reference text
);


--
-- Name: zaaktype_status_checklist_item_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.zaaktype_status_checklist_item_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: zaaktype_status_checklist_item_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.zaaktype_status_checklist_item_id_seq OWNED BY public.zaaktype_status_checklist_item.id;


--
-- Name: zaaktype_status_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.zaaktype_status_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: zaaktype_status_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.zaaktype_status_id_seq OWNED BY public.zaaktype_status.id;


--
-- Name: zorginstituut_identificatie_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.zorginstituut_identificatie_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: adres id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.adres ALTER COLUMN id SET DEFAULT nextval('public.adres_id_seq'::regclass);


--
-- Name: bag_ligplaats id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bag_ligplaats ALTER COLUMN id SET DEFAULT nextval('public.bag_ligplaats_id_seq'::regclass);


--
-- Name: bag_nummeraanduiding id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bag_nummeraanduiding ALTER COLUMN id SET DEFAULT nextval('public.bag_nummeraanduiding_id_seq'::regclass);


--
-- Name: bag_openbareruimte id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bag_openbareruimte ALTER COLUMN id SET DEFAULT nextval('public.bag_openbareruimte_id_seq'::regclass);


--
-- Name: bag_pand id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bag_pand ALTER COLUMN id SET DEFAULT nextval('public.bag_pand_id_seq'::regclass);


--
-- Name: bag_standplaats id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bag_standplaats ALTER COLUMN id SET DEFAULT nextval('public.bag_standplaats_id_seq'::regclass);


--
-- Name: bag_verblijfsobject id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bag_verblijfsobject ALTER COLUMN id SET DEFAULT nextval('public.bag_verblijfsobject_id_seq'::regclass);


--
-- Name: bag_verblijfsobject_gebruiksdoel id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bag_verblijfsobject_gebruiksdoel ALTER COLUMN id SET DEFAULT nextval('public.bag_verblijfsobject_gebruiksdoel_id_seq'::regclass);


--
-- Name: bag_verblijfsobject_pand id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bag_verblijfsobject_pand ALTER COLUMN id SET DEFAULT nextval('public.bag_verblijfsobject_pand_id_seq'::regclass);


--
-- Name: bag_woonplaats id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bag_woonplaats ALTER COLUMN id SET DEFAULT nextval('public.bag_woonplaats_id_seq'::regclass);


--
-- Name: bedrijf searchable_id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bedrijf ALTER COLUMN searchable_id SET DEFAULT nextval('public.searchable_searchable_id_seq'::regclass);


--
-- Name: bedrijf id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bedrijf ALTER COLUMN id SET DEFAULT nextval('public.bedrijf_id_seq'::regclass);


--
-- Name: bedrijf_authenticatie id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bedrijf_authenticatie ALTER COLUMN id SET DEFAULT nextval('public.bedrijf_authenticatie_id_seq'::regclass);


--
-- Name: beheer_import id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.beheer_import ALTER COLUMN id SET DEFAULT nextval('public.beheer_import_id_seq'::regclass);


--
-- Name: beheer_import_log id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.beheer_import_log ALTER COLUMN id SET DEFAULT nextval('public.beheer_import_log_id_seq'::regclass);


--
-- Name: betrokkene_notes id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.betrokkene_notes ALTER COLUMN id SET DEFAULT nextval('public.betrokkene_notes_id_seq'::regclass);


--
-- Name: betrokkenen id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.betrokkenen ALTER COLUMN id SET DEFAULT nextval('public.betrokkenen_id_seq'::regclass);


--
-- Name: bibliotheek_categorie searchable_id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bibliotheek_categorie ALTER COLUMN searchable_id SET DEFAULT nextval('public.searchable_searchable_id_seq'::regclass);


--
-- Name: bibliotheek_categorie id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bibliotheek_categorie ALTER COLUMN id SET DEFAULT nextval('public.bibliotheek_categorie_id_seq'::regclass);


--
-- Name: bibliotheek_kenmerken searchable_id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bibliotheek_kenmerken ALTER COLUMN searchable_id SET DEFAULT nextval('public.searchable_searchable_id_seq'::regclass);


--
-- Name: bibliotheek_kenmerken id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bibliotheek_kenmerken ALTER COLUMN id SET DEFAULT nextval('public.bibliotheek_kenmerken_id_seq'::regclass);


--
-- Name: bibliotheek_kenmerken_values id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bibliotheek_kenmerken_values ALTER COLUMN id SET DEFAULT nextval('public.bibliotheek_kenmerken_values_id_seq'::regclass);


--
-- Name: bibliotheek_kenmerken_values sort_order; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bibliotheek_kenmerken_values ALTER COLUMN sort_order SET DEFAULT nextval('public.bibliotheek_kenmerken_values_sort_order_seq'::regclass);


--
-- Name: bibliotheek_notificatie_kenmerk id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bibliotheek_notificatie_kenmerk ALTER COLUMN id SET DEFAULT nextval('public.bibliotheek_notificatie_kenmerk_id_seq'::regclass);


--
-- Name: bibliotheek_notificaties searchable_id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bibliotheek_notificaties ALTER COLUMN searchable_id SET DEFAULT nextval('public.searchable_searchable_id_seq'::regclass);


--
-- Name: bibliotheek_notificaties id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bibliotheek_notificaties ALTER COLUMN id SET DEFAULT nextval('public.bibliotheek_notificaties_id_seq'::regclass);


--
-- Name: bibliotheek_sjablonen searchable_id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bibliotheek_sjablonen ALTER COLUMN searchable_id SET DEFAULT nextval('public.searchable_searchable_id_seq'::regclass);


--
-- Name: bibliotheek_sjablonen id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bibliotheek_sjablonen ALTER COLUMN id SET DEFAULT nextval('public.bibliotheek_sjablonen_id_seq'::regclass);


--
-- Name: bibliotheek_sjablonen_magic_string id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bibliotheek_sjablonen_magic_string ALTER COLUMN id SET DEFAULT nextval('public.bibliotheek_sjablonen_magic_string_id_seq'::regclass);


--
-- Name: case_action id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.case_action ALTER COLUMN id SET DEFAULT nextval('public.case_action_id_seq'::regclass);


--
-- Name: case_relation id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.case_relation ALTER COLUMN id SET DEFAULT nextval('public.case_relation_id_seq'::regclass);


--
-- Name: checklist id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.checklist ALTER COLUMN id SET DEFAULT nextval('public.checklist_id_seq'::regclass);


--
-- Name: checklist_item id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.checklist_item ALTER COLUMN id SET DEFAULT nextval('public.checklist_item_id_seq'::regclass);


--
-- Name: config id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.config ALTER COLUMN id SET DEFAULT nextval('public.config_id_seq'::regclass);


--
-- Name: contact_data id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.contact_data ALTER COLUMN id SET DEFAULT nextval('public.contact_data_id_seq'::regclass);


--
-- Name: contact_relationship id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.contact_relationship ALTER COLUMN id SET DEFAULT nextval('public.contact_relationship_id_seq'::regclass);


--
-- Name: contactmoment id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.contactmoment ALTER COLUMN id SET DEFAULT nextval('public.contactmoment_id_seq'::regclass);


--
-- Name: contactmoment_email id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.contactmoment_email ALTER COLUMN id SET DEFAULT nextval('public.contactmoment_email_id_seq'::regclass);


--
-- Name: contactmoment_note id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.contactmoment_note ALTER COLUMN id SET DEFAULT nextval('public.contactmoment_note_id_seq'::regclass);


--
-- Name: country_code id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.country_code ALTER COLUMN id SET DEFAULT nextval('public.country_code_id_seq'::regclass);


--
-- Name: custom_object id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.custom_object ALTER COLUMN id SET DEFAULT nextval('public.custom_object_id_seq'::regclass);


--
-- Name: custom_object_relationship id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.custom_object_relationship ALTER COLUMN id SET DEFAULT nextval('public.custom_object_relationship_id_seq'::regclass);


--
-- Name: custom_object_type id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.custom_object_type ALTER COLUMN id SET DEFAULT nextval('public.custom_object_type_id_seq'::regclass);


--
-- Name: custom_object_type_version id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.custom_object_type_version ALTER COLUMN id SET DEFAULT nextval('public.custom_object_type_version_id_seq'::regclass);


--
-- Name: custom_object_version id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.custom_object_version ALTER COLUMN id SET DEFAULT nextval('public.custom_object_version_id_seq'::regclass);


--
-- Name: custom_object_version_content id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.custom_object_version_content ALTER COLUMN id SET DEFAULT nextval('public.custom_object_version_content_id_seq'::regclass);


--
-- Name: directory id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.directory ALTER COLUMN id SET DEFAULT nextval('public.directory_id_seq'::regclass);


--
-- Name: export_queue id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.export_queue ALTER COLUMN id SET DEFAULT nextval('public.export_queue_id_seq'::regclass);


--
-- Name: file id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.file ALTER COLUMN id SET DEFAULT nextval('public.file_id_seq'::regclass);


--
-- Name: file_case_document id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.file_case_document ALTER COLUMN id SET DEFAULT nextval('public.file_case_document_id_seq'::regclass);


--
-- Name: file_metadata id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.file_metadata ALTER COLUMN id SET DEFAULT nextval('public.file_metadata_id_seq'::regclass);


--
-- Name: filestore id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.filestore ALTER COLUMN id SET DEFAULT nextval('public.filestore_id_seq'::regclass);


--
-- Name: gm_adres id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.gm_adres ALTER COLUMN id SET DEFAULT nextval('public.gm_adres_id_seq'::regclass);


--
-- Name: gm_bedrijf id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.gm_bedrijf ALTER COLUMN id SET DEFAULT nextval('public.gm_bedrijf_id_seq'::regclass);


--
-- Name: gm_natuurlijk_persoon id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.gm_natuurlijk_persoon ALTER COLUMN id SET DEFAULT nextval('public.gm_natuurlijk_persoon_id_seq'::regclass);


--
-- Name: groups id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.groups ALTER COLUMN id SET DEFAULT nextval('public.groups_id_seq'::regclass);


--
-- Name: interface id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.interface ALTER COLUMN id SET DEFAULT nextval('public.interface_id_seq'::regclass);


--
-- Name: legal_entity_type id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.legal_entity_type ALTER COLUMN id SET DEFAULT nextval('public.legal_entity_type_id_seq'::regclass);


--
-- Name: logging id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.logging ALTER COLUMN id SET DEFAULT nextval('public.logging_id_seq'::regclass);


--
-- Name: message id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.message ALTER COLUMN id SET DEFAULT nextval('public.message_id_seq'::regclass);


--
-- Name: municipality_code id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.municipality_code ALTER COLUMN id SET DEFAULT nextval('public.municipality_code_id_seq'::regclass);


--
-- Name: natuurlijk_persoon searchable_id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.natuurlijk_persoon ALTER COLUMN searchable_id SET DEFAULT nextval('public.searchable_searchable_id_seq'::regclass);


--
-- Name: natuurlijk_persoon id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.natuurlijk_persoon ALTER COLUMN id SET DEFAULT nextval('public.natuurlijk_persoon_id_seq'::regclass);


--
-- Name: object_bibliotheek_entry id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.object_bibliotheek_entry ALTER COLUMN id SET DEFAULT nextval('public.object_bibliotheek_entry_id_seq'::regclass);


--
-- Name: object_subscription id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.object_subscription ALTER COLUMN id SET DEFAULT nextval('public.object_subscription_id_seq'::regclass);


--
-- Name: parkeergebied id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.parkeergebied ALTER COLUMN id SET DEFAULT nextval('public.parkeergebied_id_seq'::regclass);


--
-- Name: remote_api_keys id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.remote_api_keys ALTER COLUMN id SET DEFAULT nextval('public.remote_api_keys_id_seq'::regclass);


--
-- Name: result_preservation_terms id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.result_preservation_terms ALTER COLUMN id SET DEFAULT nextval('public.result_preservation_terms_id_seq'::regclass);


--
-- Name: roles id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.roles ALTER COLUMN id SET DEFAULT nextval('public.roles_id_seq'::regclass);


--
-- Name: sbus_logging id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sbus_logging ALTER COLUMN id SET DEFAULT nextval('public.sbus_logging_id_seq'::regclass);


--
-- Name: sbus_traffic id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sbus_traffic ALTER COLUMN id SET DEFAULT nextval('public.sbus_traffic_id_seq'::regclass);


--
-- Name: scheduled_jobs id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.scheduled_jobs ALTER COLUMN id SET DEFAULT nextval('public.scheduled_jobs_id_seq'::regclass);


--
-- Name: search_query id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.search_query ALTER COLUMN id SET DEFAULT nextval('public.search_query_id_seq'::regclass);


--
-- Name: search_query_delen id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.search_query_delen ALTER COLUMN id SET DEFAULT nextval('public.search_query_delen_id_seq'::regclass);


--
-- Name: searchable searchable_id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.searchable ALTER COLUMN searchable_id SET DEFAULT nextval('public.searchable_searchable_id_seq'::regclass);


--
-- Name: settings id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.settings ALTER COLUMN id SET DEFAULT nextval('public.settings_id_seq'::regclass);


--
-- Name: subject id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.subject ALTER COLUMN id SET DEFAULT nextval('public.subject_id_seq'::regclass);


--
-- Name: subject_login_history id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.subject_login_history ALTER COLUMN id SET DEFAULT nextval('public.subject_login_history_id_seq'::regclass);


--
-- Name: thread id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.thread ALTER COLUMN id SET DEFAULT nextval('public.thread_id_seq'::regclass);


--
-- Name: thread_message id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.thread_message ALTER COLUMN id SET DEFAULT nextval('public.thread_message_id_seq'::regclass);


--
-- Name: thread_message_attachment id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.thread_message_attachment ALTER COLUMN id SET DEFAULT nextval('public.thread_message_attachment_id_seq'::regclass);


--
-- Name: thread_message_attachment_derivative id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.thread_message_attachment_derivative ALTER COLUMN id SET DEFAULT nextval('public.thread_message_attachment_derivative_id_seq'::regclass);


--
-- Name: thread_message_contact_moment id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.thread_message_contact_moment ALTER COLUMN id SET DEFAULT nextval('public.thread_message_contact_moment_id_seq'::regclass);


--
-- Name: thread_message_external id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.thread_message_external ALTER COLUMN id SET DEFAULT nextval('public.thread_message_external_id_seq'::regclass);


--
-- Name: thread_message_note id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.thread_message_note ALTER COLUMN id SET DEFAULT nextval('public.thread_message_note_id_seq'::regclass);


--
-- Name: transaction id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.transaction ALTER COLUMN id SET DEFAULT nextval('public.transaction_id_seq'::regclass);


--
-- Name: transaction_record id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.transaction_record ALTER COLUMN id SET DEFAULT nextval('public.transaction_record_id_seq'::regclass);


--
-- Name: transaction_record_to_object id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.transaction_record_to_object ALTER COLUMN id SET DEFAULT nextval('public.transaction_record_to_object_id_seq'::regclass);


--
-- Name: user_entity id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.user_entity ALTER COLUMN id SET DEFAULT nextval('public.user_entity_id_seq'::regclass);


--
-- Name: woz_objects id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.woz_objects ALTER COLUMN id SET DEFAULT nextval('public.woz_objects_id_seq'::regclass);


--
-- Name: zaak searchable_id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak ALTER COLUMN searchable_id SET DEFAULT nextval('public.searchable_searchable_id_seq'::regclass);


--
-- Name: zaak id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak ALTER COLUMN id SET DEFAULT nextval('public.zaak_id_seq'::regclass);


--
-- Name: zaak_authorisation id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak_authorisation ALTER COLUMN id SET DEFAULT nextval('public.zaak_authorisation_id_seq'::regclass);


--
-- Name: zaak_bag id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak_bag ALTER COLUMN id SET DEFAULT nextval('public.zaak_bag_id_seq'::regclass);


--
-- Name: zaak_betrokkenen id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak_betrokkenen ALTER COLUMN id SET DEFAULT nextval('public.zaak_betrokkenen_id_seq'::regclass);


--
-- Name: zaak_kenmerk id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak_kenmerk ALTER COLUMN id SET DEFAULT nextval('public.zaak_kenmerk_id_seq'::regclass);


--
-- Name: zaak_meta id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak_meta ALTER COLUMN id SET DEFAULT nextval('public.zaak_meta_id_seq'::regclass);


--
-- Name: zaak_subcase id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak_subcase ALTER COLUMN id SET DEFAULT nextval('public.zaak_subcase_id_seq'::regclass);


--
-- Name: zaaktype searchable_id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype ALTER COLUMN searchable_id SET DEFAULT nextval('public.searchable_searchable_id_seq'::regclass);


--
-- Name: zaaktype id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype ALTER COLUMN id SET DEFAULT nextval('public.zaaktype_id_seq'::regclass);


--
-- Name: zaaktype_authorisation id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_authorisation ALTER COLUMN id SET DEFAULT nextval('public.zaaktype_authorisation_id_seq'::regclass);


--
-- Name: zaaktype_betrokkenen id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_betrokkenen ALTER COLUMN id SET DEFAULT nextval('public.zaaktype_betrokkenen_id_seq'::regclass);


--
-- Name: zaaktype_definitie id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_definitie ALTER COLUMN id SET DEFAULT nextval('public.zaaktype_definitie_id_seq'::regclass);


--
-- Name: zaaktype_kenmerken id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_kenmerken ALTER COLUMN id SET DEFAULT nextval('public.zaaktype_kenmerken_id_seq'::regclass);


--
-- Name: zaaktype_node id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_node ALTER COLUMN id SET DEFAULT nextval('public.zaaktype_node_id_seq'::regclass);


--
-- Name: zaaktype_notificatie id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_notificatie ALTER COLUMN id SET DEFAULT nextval('public.zaaktype_notificatie_id_seq'::regclass);


--
-- Name: zaaktype_regel id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_regel ALTER COLUMN id SET DEFAULT nextval('public.zaaktype_regel_id_seq'::regclass);


--
-- Name: zaaktype_relatie id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_relatie ALTER COLUMN id SET DEFAULT nextval('public.zaaktype_relatie_id_seq'::regclass);


--
-- Name: zaaktype_resultaten id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_resultaten ALTER COLUMN id SET DEFAULT nextval('public.zaaktype_resultaten_id_seq'::regclass);


--
-- Name: zaaktype_sjablonen id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_sjablonen ALTER COLUMN id SET DEFAULT nextval('public.zaaktype_sjablonen_id_seq'::regclass);


--
-- Name: zaaktype_standaard_betrokkenen id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_standaard_betrokkenen ALTER COLUMN id SET DEFAULT nextval('public.zaaktype_standaard_betrokkenen_id_seq'::regclass);


--
-- Name: zaaktype_status id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_status ALTER COLUMN id SET DEFAULT nextval('public.zaaktype_status_id_seq'::regclass);


--
-- Name: zaaktype_status_checklist_item id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_status_checklist_item ALTER COLUMN id SET DEFAULT nextval('public.zaaktype_status_checklist_item_id_seq'::regclass);


--
-- Data for Name: adres; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.adres (id, straatnaam, huisnummer, huisletter, huisnummertoevoeging, nadere_aanduiding, postcode, woonplaats, gemeentedeel, functie_adres, datum_aanvang_bewoning, woonplaats_id, gemeente_code, hash, import_datum, deleted_on, adres_buitenland1, adres_buitenland2, adres_buitenland3, landcode, natuurlijk_persoon_id, bag_id, geo_lat_long) FROM stdin;
\.


--
-- Data for Name: alternative_authentication_activation_link; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.alternative_authentication_activation_link (token, subject_id, expires) FROM stdin;
\.


--
-- Data for Name: bag_cache; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.bag_cache (id, bag_type, bag_id, bag_data, date_created, last_modified, refresh_after) FROM stdin;
\.


--
-- Data for Name: bag_ligplaats; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.bag_ligplaats (identificatie, begindatum, einddatum, officieel, status, hoofdadres, inonderzoek, documentdatum, documentnummer, correctie, id) FROM stdin;
\.


--
-- Data for Name: bag_ligplaats_nevenadres; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.bag_ligplaats_nevenadres (identificatie, begindatum, nevenadres, correctie) FROM stdin;
\.


--
-- Data for Name: bag_nummeraanduiding; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.bag_nummeraanduiding (identificatie, begindatum, einddatum, huisnummer, officieel, huisletter, huisnummertoevoeging, postcode, woonplaats, inonderzoek, openbareruimte, type, documentdatum, documentnummer, status, correctie, id, gps_lat_lon) FROM stdin;
\.


--
-- Data for Name: bag_openbareruimte; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.bag_openbareruimte (identificatie, begindatum, einddatum, naam, officieel, woonplaats, type, inonderzoek, documentdatum, documentnummer, status, correctie, id, gps_lat_lon) FROM stdin;
\.


--
-- Data for Name: bag_pand; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.bag_pand (identificatie, begindatum, einddatum, officieel, bouwjaar, status, inonderzoek, documentdatum, documentnummer, correctie, id) FROM stdin;
\.


--
-- Data for Name: bag_standplaats; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.bag_standplaats (identificatie, begindatum, einddatum, officieel, status, hoofdadres, inonderzoek, documentdatum, documentnummer, correctie, id) FROM stdin;
\.


--
-- Data for Name: bag_verblijfsobject; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.bag_verblijfsobject (identificatie, begindatum, einddatum, officieel, hoofdadres, oppervlakte, status, inonderzoek, documentdatum, documentnummer, correctie, id) FROM stdin;
\.


--
-- Data for Name: bag_verblijfsobject_gebruiksdoel; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.bag_verblijfsobject_gebruiksdoel (identificatie, begindatum, gebruiksdoel, correctie, id) FROM stdin;
\.


--
-- Data for Name: bag_verblijfsobject_pand; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.bag_verblijfsobject_pand (identificatie, begindatum, pand, correctie, id) FROM stdin;
\.


--
-- Data for Name: bag_woonplaats; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.bag_woonplaats (identificatie, begindatum, einddatum, officieel, naam, status, inonderzoek, documentdatum, documentnummer, correctie, id) FROM stdin;
\.


--
-- Data for Name: bedrijf; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.bedrijf (search_index, search_term, object_type, searchable_id, search_order, id, dossiernummer, subdossiernummer, hoofdvestiging_dossiernummer, hoofdvestiging_subdossiernummer, vorig_dossiernummer, vorig_subdossiernummer, handelsnaam, rechtsvorm, kamernummer, faillisement, surseance, telefoonnummer, email, vestiging_adres, vestiging_straatnaam, vestiging_huisnummer, vestiging_huisnummertoevoeging, vestiging_postcodewoonplaats, vestiging_postcode, vestiging_woonplaats, correspondentie_adres, correspondentie_straatnaam, correspondentie_huisnummer, correspondentie_huisnummertoevoeging, correspondentie_postcodewoonplaats, correspondentie_postcode, correspondentie_woonplaats, hoofdactiviteitencode, nevenactiviteitencode1, nevenactiviteitencode2, werkzamepersonen, contact_naam, contact_aanspreektitel, contact_voorletters, contact_voorvoegsel, contact_geslachtsnaam, contact_geslachtsaanduiding, authenticated, authenticatedby, fulldossiernummer, import_datum, deleted_on, verblijfsobject_id, system_of_record, system_of_record_id, vestigingsnummer, vestiging_huisletter, correspondentie_huisletter, vestiging_adres_buitenland1, vestiging_adres_buitenland2, vestiging_adres_buitenland3, vestiging_landcode, correspondentie_adres_buitenland1, correspondentie_adres_buitenland2, correspondentie_adres_buitenland3, correspondentie_landcode, uuid, rsin, oin, main_activity, secondairy_activities, date_founded, date_registration, date_ceased, vestiging_latlong, vestiging_bag_id, preferred_contact_channel, active, pending, related_custom_object_id) FROM stdin;
\.


--
-- Data for Name: bedrijf_authenticatie; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.bedrijf_authenticatie (id, gegevens_magazijn_id, login, password, created, last_modified) FROM stdin;
\.


--
-- Data for Name: beheer_import; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.beheer_import (id, importtype, succesvol, finished, import_create, import_update, error, error_message, entries, created, last_modified) FROM stdin;
\.


--
-- Data for Name: beheer_import_log; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.beheer_import_log (id, import_id, old_data, new_data, created, last_modified, kolom, identifier, action) FROM stdin;
\.


--
-- Data for Name: betrokkene_notes; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.betrokkene_notes (id, betrokkene_exid, betrokkene_type, betrokkene_from, ntype, subject, message, created, last_modified) FROM stdin;
\.


--
-- Data for Name: betrokkenen; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.betrokkenen (id, btype, gm_natuurlijk_persoon_id, naam) FROM stdin;
\.


--
-- Data for Name: bibliotheek_categorie; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.bibliotheek_categorie (search_index, search_term, object_type, searchable_id, search_order, id, naam, label, description, help, created, last_modified, system, pid, uuid) FROM stdin;
\.


--
-- Data for Name: bibliotheek_kenmerken; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.bibliotheek_kenmerken (search_index, search_term, object_type, searchable_id, search_order, id, naam, value_type, value_default, label, description, help, magic_string, created, last_modified, bibliotheek_categorie_id, document_categorie, system, deleted, file_metadata_id, version, properties, naam_public, type_multiple, uuid, relationship_type, relationship_name, relationship_uuid) FROM stdin;
\.


--
-- Data for Name: bibliotheek_kenmerken_values; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.bibliotheek_kenmerken_values (id, bibliotheek_kenmerken_id, value, active, sort_order) FROM stdin;
\.


--
-- Data for Name: bibliotheek_notificatie_kenmerk; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.bibliotheek_notificatie_kenmerk (id, bibliotheek_notificatie_id, bibliotheek_kenmerken_id) FROM stdin;
\.


--
-- Data for Name: bibliotheek_notificaties; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.bibliotheek_notificaties (search_index, search_term, object_type, searchable_id, search_order, id, bibliotheek_categorie_id, label, subject, message, created, last_modified, deleted, sender, sender_address, uuid) FROM stdin;
\.


--
-- Data for Name: bibliotheek_sjablonen; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.bibliotheek_sjablonen (search_index, search_term, object_type, searchable_id, search_order, id, bibliotheek_categorie_id, naam, label, description, help, created, last_modified, filestore_id, deleted, interface_id, template_external_name, uuid) FROM stdin;
\.


--
-- Data for Name: bibliotheek_sjablonen_magic_string; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.bibliotheek_sjablonen_magic_string (id, bibliotheek_sjablonen_id, value) FROM stdin;
\.


--
-- Data for Name: case_action; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.case_action (id, case_id, casetype_status_id, type, label, automatic, data, state_tainted, data_tainted) FROM stdin;
\.


--
-- Data for Name: case_authorisation_map; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.case_authorisation_map (key, legacy_key) FROM stdin;
read	zaak_read
write	zaak_edit
manage	zaak_beheer
search	zaak_search
\.


--
-- Data for Name: case_property; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.case_property (id, name, type, namespace, value, value_v0, case_id, object_id) FROM stdin;
\.


--
-- Data for Name: case_relation; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.case_relation (id, case_id_a, case_id_b, order_seq_a, order_seq_b, type_a, type_b, uuid) FROM stdin;
\.


--
-- Data for Name: checklist; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.checklist (id, case_id, case_milestone) FROM stdin;
\.


--
-- Data for Name: checklist_item; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.checklist_item (id, checklist_id, label, state, sequence, user_defined, deprecated_answer, uuid, due_date, description, assignee_id) FROM stdin;
\.


--
-- Data for Name: config; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.config (id, parameter, value, advanced, uuid, definition_id) FROM stdin;
1	allocation_notification_template_id		t	db6e2a5b-471c-4833-90d4-cdf9b0463525	8c3f8850-435d-4b64-8489-4285cdeedf7e
3	app_enabled_meeting	0	t	7da6dc79-00f9-40a4-8070-5b09c7417fab	287fd068-0999-4c44-a504-a9538f19a763
4	app_enabled_wordapp	0	t	d0f8e667-09dd-46c4-952e-4b4798894011	2c3ed588-6770-41df-8c95-2987b9bd6c61
5	case_distributor_group		t	d9a8a167-7563-4d41-b227-20bd69738d43	82f270d2-498e-4ce9-adcd-cb23331daf69
6	case_distributor_role		t	a01df9be-d64f-4715-8aa5-04af9786d676	7362679d-b8f6-493b-bcb1-b8d9350b443e
7	customer_info_adres	H.J.E. Wenckbachweg 90	t	5381b2a5-ca0f-4906-ad70-69958a0e6c92	33e32331-e6d6-4031-85c7-59c7eb1163bc
8	customer_info_email	servicedesk@mintlab.nl	t	0f8d34a7-6394-4481-b3a6-d232d10c2cac	239bbdee-0811-42c3-8cc2-10d20b8b1e30
9	customer_info_faxnummer		t	57932d20-81e3-4ed6-afbd-cccb2ae31125	3499c848-0903-47aa-9d86-f3bc4190e7e5
10	customer_info_gemeente_id_url	https://zaaksysteem.nl	t	89545cb4-1e3b-44f2-9e54-b5b54a5dba72	424ff3bc-bcf6-4772-872a-b9a93a2f3c34
11	customer_info_gemeente_portal	https://zaaksysteem.nl	t	540ad9a6-e54f-4edd-a563-8a6f61a7f550	11fa98a7-4fd4-4dbc-a929-c2e6ab25f49b
12	customer_info_huisnummer	90	t	129c4b4b-9b3f-4541-b243-3e898301c37c	4bd449ae-d82d-43ed-aa84-72fe01b7462d
13	customer_info_latitude	52.3375433	t	41ae7529-cfa7-45d2-9630-957362c52f82	d81f8648-2a36-4ea0-814e-2af227a5da8a
14	customer_info_longitude	4.9259539	t	8c91da44-646a-4a82-944f-094fbd76f1fe	165a4192-8c28-4e74-904b-2317b00e2e23
15	customer_info_naam	Ontwikkelomgeving	t	d8fa4831-8b1b-430c-8143-0b74a903d622	5f2fef2a-4833-4847-9b1a-236039700fb0
16	customer_info_naam_kort	dev.zs.nl	t	fca3dda7-c45c-4d51-a419-af8116bcb0af	fadcf16c-4bbe-4157-9771-e156ca0ae205
17	customer_info_naam_lang	Ontwikkelomgeving dev.zaaksysteem.nl	t	fe2111e3-96e0-47ce-bbff-105082c70457	945bd84a-d3cc-412f-a801-e89787deaf29
18	customer_info_postbus		t	1a16873b-9d53-46ab-bddd-8902a25e7700	eece146f-97e3-4315-a4dd-e6751360059b
19	customer_info_postbus_postcode		t	52b65d68-c5fe-44ef-aad6-79c3d82ee788	02ec24a4-b511-4596-ad22-1b2a381f26ee
20	customer_info_postcode	1114AD	t	ad7d6c3b-b3f6-4470-9c24-80e288fbfd5d	f3e9df2b-61cc-4d38-9feb-35dda8a5d22a
21	customer_info_straatnaam	H.J.E. Wenckbachweg	t	5219d99a-64e4-4710-bd44-78b7bb4a3062	2e35ebfd-e6e0-4124-8e4f-7c28cfffeefd
22	customer_info_telefoonnummer	020 - 737 000 5	t	67d38edf-1aa1-4465-96dc-99543f332f10	4eea2794-28ac-4d24-a07e-e13d75528d1e
23	customer_info_website	https://zaaksysteem.nl	t	596ed72c-7e5d-48b1-813d-0ccee0b9e6ff	e9cf0c02-f64f-44a2-b7d7-ac6004218084
24	customer_info_woonplaats	Amsterdam-Duivendrecht	t	842fbb0b-02e0-41a2-b34a-9b5dd88c48aa	086512fa-4b43-48c6-94da-7c9d1ccc895e
25	customer_info_zaak_email	no-reply@zaaksysteem.nl	t	2d26795b-b486-4b5a-abd9-8e5140a9289e	a21cbc94-b624-4a6a-8e10-3fa73c6148c8
26	custom_relation_roles	[]	t	6bbd3c57-52fc-49fb-9b53-a247086f8ce0	e73874c6-e81a-49cb-9b87-05deb68e1118
27	disable_dashboard_customization	0	t	7d2b0229-9631-43e2-ac79-ab3fafaa583c	1c7b45b7-f101-4b7e-8514-f412063cc0c8
28	document_intake_user		t	36ddb2bb-6ed7-429a-bae5-c9c000868b25	7561d210-b9be-4e29-9b65-c4f8a2de1fd4
29	enable_mintlab_id	1	t	1eea05fa-db90-480a-87c6-bfd3eacd2858	e3d6909a-134b-4317-b61f-1b7dca1569b5
30	enable_stufzkn_simulator	1	t	14e748fb-6140-4767-a366-ac19eea7a1f4	42f9b366-e324-4b79-abcc-a5d699290da6
31	feedback_email_template_id		t	42ecc76a-ca64-41cf-ac42-299c6ba3503c	1acf71e1-aab1-4951-8e7a-db7cdaaf6a2b
32	files_locally_editable	0	t	527f22a9-a99a-4856-9200-faa8b7216d9e	92236ee6-eb05-44f3-bd0a-5bca376749b4
33	file_username_seperator	-	t	fcc939eb-e137-4e31-a2ef-b99efddcbd78	98a664ba-e652-4ac0-8bd5-a6962389c6c6
34	first_login_confirmation		t	f195fe5b-2879-4ccf-a194-a5f053c39fc3	f4ebe619-933c-4ead-b664-506a9c6e1ea0
35	first_login_intro		t	709b5978-c684-4b22-ac3e-e2497e3d11a3	a34e8a0c-6427-4e97-a14b-db0f12783a21
36	new_user_template		t	c0594f9d-e0c4-41bc-9cb2-071d2b8c8cda	6271cb81-e5fb-40fd-9385-d8544fee75f0
37	pdf_annotations_public	0	t	ebd50ab1-b232-451e-b9da-5b191d3b55b3	608f73b1-f15f-4985-8dc6-06ce2d9c257a
38	pip_login_intro		t	31175318-f2ee-4339-9c70-363c79bbbf90	2f9d7541-8e22-4dfc-9934-1374c55175b4
39	public_manpage	1	t	5b60db5a-b5ae-4349-8f99-a7c64530eeb3	2ea16761-23b7-4964-9548-2d54861cc78d
40	requestor_search_extension_active		t	47728923-1dd5-4923-9e94-d27924dc13c3	348a00a6-4163-49f4-9279-eef1ccb19ae6
43	signature_upload_role		t	9b9acb00-6de5-41cc-8dca-34f0573bb18e	09c5e460-2fd3-4153-84db-7f9159fe4f07
41	requestor_search_extension_href		t	f2ccd590-eaea-4b67-a4b6-6cd8918335a1	7fcfb2ea-8f46-475c-8f89-e09b2c28c828
42	requestor_search_extension_name		t	8d798c6f-2962-4f9a-b5b7-5db98c993eff	836c7e4b-a896-4c25-a7a4-6f138f75d41e
44	subject_pip_authorization_confirmation_template_id		t	58f62b5e-d9e8-4db4-8256-80e2bb8b87d9	2cd00952-d6a0-4096-93c9-7515108c798d
45	users_can_change_password	0	t	ce23834e-5121-4968-8485-7dbb24dde529	49999eee-96b4-46c4-babc-8fa6bb5978cb
46	bag_spoof_mode	1	t	1d7f5b08-345e-4e49-96e9-d6552957f320	9b0956d2-c9a6-4e84-8c49-0f53d17551a1
47	bag_local_only	0	t	f9da20ab-fba0-4d57-a44f-be6d9219af42	399be534-a816-42b9-bae8-f2899405b7b8
48	bag_priority_gemeentes	[]	t	734a59dd-306c-429f-bdab-42af3ad0550f	55482181-7e7d-4bc2-b653-356762a91ade
49	edit_document_online	0	t	0f0640d7-02ac-48be-b637-fd14e309a6b6	8d4a83c1-dff9-48d0-8481-10eba0baa129
2	allowed_templates	[]	t	23a12dc1-dc5a-45ca-a365-86521dd1e152	33aeed61-95bb-4dd7-9022-f2ba5aa2b718
50	case_number_prefix	\N	t	de86d295-c027-4e4a-bfb5-e7dc19a55066	dbb124d7-9e96-4a8a-9252-fb367ef8562a
51	dashboard_ui_xss_uri	[]	t	11874c68-87ab-493d-8aba-315100da8406	270be974-6390-4eea-9db7-74a7aed5ed6f
52	contact_channel_enabled	0	t	eda6ceb5-8fac-4499-9a56-1e497547b070	50de4bd5-0a71-4606-bdca-4f4d42080283
53	edit_document_msonline	0	t	892d35c3-6c67-439b-95c3-10ba56eadb17	1bab86db-09d7-4062-a7c6-64e11fdc7959
54	export_queue_email_template_id	\N	t	a412b40a-8db7-4e77-9001-6b7fd8b7d879	204fe879-a7c5-4f6c-8a85-ecb739cddff7
55	case_suspension_term_exceeded_notification_template_id		t	5305c727-4b2e-45d3-b3d9-d2b418c29ec1	dd992c38-bb0b-4b3f-b24f-002afd6d93e7
56	case_term_exceeded_notification_template_id		t	4a42f743-7673-4656-a371-f6a367932071	471e6099-dd5c-4b43-858e-59c694f2b13d
57	new_assigned_case_notification_template_id		t	eadf811e-3868-4c9f-a38e-9c8c01d6beca	985941b9-f37e-4361-9c9e-5b17da591a3c
58	new_attribute_proposal_notification_template_id		t	9c8c6281-2ac3-4efc-a7b8-9f91a0b55850	7423f0f0-87ae-4551-ae1a-3502885eac9d
59	new_document_notification_template_id		t	79b95c75-f629-4097-9ce9-ea699218cf10	fbc5158d-8edf-44bf-b215-99b7b94a12c2
60	new_ext_pip_message_notification_template_id		t	7d00ed73-aedd-401a-863e-dda6ea3276ed	5d9c371f-a203-49a6-8531-67986a0f2d3f
61	new_int_pip_message_notification_template_id		t	48dc9b9b-eb6b-4506-8b7b-547689c0f841	c41cedb5-3415-498d-9774-d89e95a23b20
62	show_object_v1	1	t	0102a46f-8b6f-413e-a62d-a88ffaa412f4	d1532d86-536e-4254-a60a-8f30fcf09903
63	show_object_v2	1	t	d5bd4917-5068-406f-866b-907738410b65	96f00720-2db7-416b-96f0-0552851c4012
\.


--
-- Data for Name: contact_data; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.contact_data (id, gegevens_magazijn_id, betrokkene_type, mobiel, telefoonnummer, email, created, last_modified, note) FROM stdin;
\.


--
-- Data for Name: contact_relationship; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.contact_relationship (id, uuid, contact, contact_uuid, contact_type, relation, relation_uuid, relation_type) FROM stdin;
\.


--
-- Data for Name: contactmoment; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.contactmoment (id, subject_id, case_id, type, medium, date_created, created_by, uuid) FROM stdin;
\.


--
-- Data for Name: contactmoment_email; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.contactmoment_email (id, filestore_id, contactmoment_id, body, subject, recipient, cc, bcc) FROM stdin;
\.


--
-- Data for Name: contactmoment_note; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.contactmoment_note (id, message, contactmoment_id) FROM stdin;
\.


--
-- Data for Name: country_code; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.country_code (id, dutch_code, label, uuid, historical) FROM stdin;
1	0	Onbekend	8c4d9e76-4098-46ee-9879-4503c102de07	f
2	5001	Canada	8adf58c4-da89-4271-adaa-915f34145ca2	f
3	5002	Frankrijk	4d0247d5-58dc-40cb-94ae-786dfe07eef2	f
4	5003	Zwitserland	26b94232-d7e9-4e4f-91c6-9535c3e4cc48	f
5	5004	Rhodesië	1161a3b8-ba49-4f34-9baf-5c52a78e143b	f
6	5005	Malawi	8d8de9fa-bf0f-4a0e-a61c-3763ab67932f	f
7	5006	Cuba	bbd83925-4abd-4db3-b531-b15bf5eb9e4e	f
8	5007	Suriname	58eaa052-9cb4-4983-a3cb-b06f627ad9dd	f
9	5008	Tunesië	39fe4557-44d3-4428-a842-4447b2e0a078	f
10	5009	Oostenrijk	bbb7d007-8147-4106-8028-ddc8cd8141e1	f
11	5010	België	cc3ca75d-44fc-45be-bd09-2ade8d56c903	f
12	5011	Botswana	5fd4820d-8d63-48a6-9704-8d16ad0a4104	f
13	5012	Iran	fc650929-463b-49ea-94a9-a779414a1667	f
14	5013	Nieuwzeeland	f40662e1-003d-49b0-a84b-ff8e59e577a2	f
15	5014	Zuidafrika	db2c06ec-226d-4bb5-9050-a772767f0b34	f
16	5015	Denemarken	4b1015c9-eacf-452f-88e8-1ac387dff42d	f
17	5016	Noordjemen	56acb7ae-cf92-424c-9627-40974dd34076	f
18	5017	Hongarije	94a19427-cd1a-4c64-b79e-2451841f4aee	f
19	5018	Saoediarabië	533e9055-8eaa-4a64-bd5a-be9b52fbdf46	f
20	5019	Liberia	18cc2ab8-60fd-4b41-b004-a63f48075c9d	f
21	5020	Etiopië	c0e9cc8e-a61a-436c-a1e9-75878774e094	f
22	5021	Chili	4a5109ff-d001-4b83-8cc9-521305d96d41	f
23	5022	Marokko	a968a228-6ca4-450e-a3ea-8e70b13b4131	f
24	5023	Togo	a8a8049e-1665-4669-a311-2ae65612647e	f
25	5024	Ghana	b39e31a0-a263-4744-85df-6572c2188c32	f
26	5025	Laos	441169f1-4ab4-4a73-b0bd-7be91cd4fb68	f
27	5026	Angola	7ba530ca-046d-4822-9096-f5538d5505ce	f
28	5027	Filipijnen	6860355f-330b-4598-b5a7-366482ac6a5d	f
29	5028	Zambia	6f4bb2ec-e112-4535-98fc-369fd8c3fa39	f
30	5029	Mali	31c22b77-60b9-4264-bb8e-cf879c851907	f
31	5030	Ivoorkust	4d39afd5-8ad8-4846-8e75-5bae2fd4cdac	f
32	5031	Burma	6641dfdb-6d5f-4c59-9247-d2d575e3ed60	f
33	5032	Monaco	6d993e10-dd01-4c29-8fd2-41efccdd6a7c	f
34	5033	Colombia	71373604-2785-4f9f-a7e3-6a6ff368f222	f
35	5034	Albanië	12e8c61b-55b6-4bc5-a707-15e774938c36	f
36	5035	Kameroen	ccd550fe-a36d-45cc-a085-cbf14dbed29c	f
37	5036	Zuidviëtnam	b18e1fde-471b-44f1-8cde-a3d02c054fbd	f
38	5037	Singapore	db77f9ed-4383-4d97-9c7b-97d228be619f	f
39	5038	Paraguay	1cb92ee8-18a9-40d2-b740-65398e8da2fd	f
40	5039	Zweden	ffaff274-be90-4de0-a59f-17a8c7a1b599	f
41	5040	Cyprus	05d2b497-6420-47f7-ba9c-f2de7befca87	f
42	5041	Australisch Nieuwguinea	02feaad1-880a-43fb-a036-2a6b78078492	f
43	5042	Brunei	35f96565-5bcd-4499-8a73-ceeb5d0e17d5	f
44	5043	Irak	e626b869-235d-40a5-9906-0a35b99885bd	f
45	5044	Mauritius	bcf923b4-ac4d-45af-975e-222380e28dc1	f
46	5045	Vaticaanstad	45409407-2a9f-47ef-a9fa-a1af588710e4	f
47	5046	Kashmir	c07ef39d-aa05-4c24-b809-a4783369bb46	f
48	5047	Myanmar	b5373d85-db45-45c1-9932-4b3bc59988fc	f
49	5048	Jemen	114283d5-53b9-45f1-80c5-14c4215f79e4	f
50	5049	Slovenië	b3dbf73f-5497-4869-aa8b-66cf70cac342	f
51	5050	Zaïre	ff496536-da1b-47a1-908c-6faaa9c598fc	f
52	5051	Kroatië	e227c2b6-d84c-4617-81a8-020bb2c38bfe	f
53	5052	Taiwan	85098eac-18bf-4c56-9346-03ea22fb1fc3	f
54	5053	Rusland	4b426a6e-9f0e-4008-b2ab-efa404fe1950	f
55	5054	Armenië	fc0793f5-6cb0-49a4-aba3-4a7302beaa77	f
56	5055	Ascension	e91e301f-43c9-459f-8bee-efb7b5c4235f	f
57	5056	Azoren	146c312e-75a3-4d24-adbc-c2a3f54b0ecc	f
58	5057	Bahrein	95397498-d604-436b-8be5-60dd56aed51c	f
59	5058	Bhutan	2b09fa1b-8256-4682-a908-dd23c1bd6656	f
60	5059	Britse Antillen	2d14788d-4ee0-45d7-83e5-68d8b49a65f8	f
61	5060	Comoren	b9c2ec7a-7aa8-428b-a16f-517d05967d04	f
62	5061	Falklandeilanden	5e809664-1ad1-4bdb-90dc-912c505f7363	f
63	5062	Frans Guyana	805de983-4657-4121-8ba3-ea3be534f959	f
64	5063	Frans Somaliland	7e6a503c-9b1b-4c04-b1db-581d51a51c8d	f
65	5064	Gilbert- en Ellice-eilanden	58cef3fe-9ec9-47b1-99dc-06117bc99f6f	f
66	5065	Groenland	a00bec36-5107-4bf1-9dc0-a83f3b641ff0	f
67	5066	Guadeloupe	60f8455e-6960-4fc9-b506-ef29a71756ea	f
68	5067	Kaapverdische Eilanden	eeb3064c-173d-436f-9d66-b43b2a483f91	f
69	5068	Macau	22995326-b921-4db7-8c40-92d2832e3cbf	f
70	5069	Martinique	6fc85e28-1fcd-4767-a97d-960b6d0ec5ee	f
71	5070	Mozambique	d9844782-12aa-4e47-9c3e-2d0a7f177495	f
72	5071	Pitcairneilanden	486ceb15-1ea9-41ab-adf9-ae0e7f8a51ef	f
73	5072	Guinee Bissau	8179f736-20eb-4dc1-9b2a-878b529b1386	f
74	5073	Réunion	47ff206a-48ed-42dc-97ae-5986e9eab649	f
75	5074	Saint Pierre en Miquelon	a144f97f-9a8b-467c-b6a8-cd71ddc68162	f
76	5075	Seychellen en Amiranten	8241d12a-f3a7-47b0-ab8f-3539e28f8f89	f
77	5076	Tonga	7984b38e-655c-4295-906f-01e5b8178916	f
78	5077	Wallis en Futuna	7c21d2f5-3c54-4925-8edc-9203bafd0f6a	f
79	5078	Zuidwest Afrika	bbc69a03-9f7d-4d05-b4c7-4f9ee5721a1b	f
80	5079	Frans Indië	ede0f32d-da81-4cf3-899b-6ddb88b71fa4	f
81	5080	Johnston	44241712-c420-4aae-8197-21d01d89ee78	f
82	5081	Kedah	fa99cc4f-5b0f-400f-ad7b-0001bc6394fb	f
83	5082	Kelantan	2daf0fe5-ccc0-4755-986e-9d17855d7a85	f
84	5083	Malakka	28a741e5-28c9-4d86-a9c9-b4fc42761a15	f
85	5084	Mayotte	5dee1c45-3c90-4076-ade4-5a244d3c8a87	f
86	5085	Negri Sembilan	75c73f4f-eb12-4474-8049-f351f42f3ae0	f
87	5086	Pahang	2d188b18-5845-4b0f-b747-316fdb47ce67	f
88	5087	Perak	e921f615-fe47-4f4a-8e76-d551d56cbb77	f
89	5088	Perlis	617dfeb2-57f0-4f5e-9b22-fc0f15d9274b	f
90	5089	Portugees Indië	731bbdf5-5593-4e73-a066-42a83c50c0a2	f
91	5090	Selangor	e2a26870-a634-4291-bdf2-69220eba6d28	f
92	5091	Sikkim	2bcb8a79-0d63-4cb9-aab3-c3a839989be4	f
93	5092	Sint Vincent en de Grenadinen	e50a4910-a4ce-4406-bac3-4ed83700cf80	f
94	5093	Spitsbergen	dfb4e564-e134-4f5a-81d7-2786eedbb9fd	f
95	5094	Trengganu	9148b728-9ea8-4398-8fc4-d7d5f887e701	f
96	5095	Aruba	3d9d9e62-5a03-475b-925b-2a59f1cbacdc	f
97	5096	Burkina Faso	e26dbdf8-6a4d-43f7-aa6d-62580d8d2045	f
98	5097	Azerbajdsjan	f425bc25-87e4-4521-8724-30151be3c023	f
99	5098	Belarus (Wit-Rusland)	46b75766-555f-4ef1-a373-854f30c771d4	f
100	5099	Kazachstan	015c4e28-782b-4a6a-94f5-2033c297c3c7	f
101	5100	Macedonië	630a06fa-b2c7-4f8d-a891-99055b26e51e	f
102	5101	Timor Leste	c90e86c1-cc61-43c2-93ed-4ed0ab8b9e66	f
103	5102	Servië en Montenegro	4e3ac244-24a0-498f-a3a9-fa7247c3435f	f
104	5103	Servië	a915e82a-4f84-4218-bf9a-cd7f1c21ee75	f
105	5104	Montenegro	c0e95934-fe5a-4864-850a-418b757a6fa3	f
106	5105	Kosovo	88a681be-3675-460f-aa6c-688a4ff2e9f5	f
107	5106	Bonaire	c3702979-ab74-4da3-875b-f66d77989b5a	f
108	5107	Curaçao	51496db9-9af0-44dc-ac20-0c9e530d8ed1	f
109	5108	Saba	d9f2984e-c6e2-438e-9ec9-acbbb879c316	f
110	5109	Sint Eustatius	97459cd6-b765-4bcd-a507-65dc91ee5a1a	f
111	5110	Sint Maarten	da0037bd-2b3b-43ee-9d69-0a41e86c62c9	f
112	5111	Zuid-Soedan	93088821-1894-4277-8ccf-3008d4f9e4a2	f
113	6000	Moldavië	aa8bde64-d1a9-4fff-b6b7-a57b8858fd18	f
114	6001	Burundi	c3eb674e-3542-4468-a563-57cdcc8148ab	f
115	6002	Finland	259da240-86ff-4e11-ac10-87b74abb006e	f
116	6003	Griekenland	746442e7-32de-4688-8b88-b0061fa25346	f
117	6004	Guatemala	9d697682-7e15-420a-8706-adc0e28942d8	f
118	6005	Nigeria	dae91f34-825c-4df0-b733-da9069d030e0	f
119	6006	Libië	0129250d-b384-497f-bd43-4ce0d814033e	f
120	6007	Ierland	adb95688-9e5d-4c11-8691-425f2b67ac97	f
121	6008	Brazilië	2c8391fc-b141-4c64-acb6-92bbc8b22282	f
122	6009	Rwanda	a9183af4-c876-4776-8a80-03afd1dbcda8	f
123	6010	Venezuela	36473bd7-62a0-4d29-9119-391e93db51fb	f
124	6011	IJsland	caa48d0e-ea55-46e6-98c6-b79bbaa3071e	f
125	6012	Liechtenstein	9786d7d7-a7c6-4b83-ac7f-cd678bdc5a1b	f
126	6013	Somalia	cb8a68a6-d05e-4a6c-99ea-066664ef59d3	f
127	6014	Verenigde Staten van Amerika	9721d22d-3fe4-4faa-a189-46707b35eda7	f
128	6015	Bolivia	7d9f0a76-d158-4f26-bed4-ff9727846522	f
129	6016	Australië	4759081e-2307-44ac-a764-438797d7078b	f
130	6017	Jamaica	693e5f3f-4687-41ac-8689-fb7d0a46d70e	f
131	6018	Luxemburg	25460c43-16be-4bcb-bd03-5a584c113ecc	f
132	6019	Tsjaad	6080c4c5-31e8-4fd9-a7c9-299ae87c361a	f
133	6020	Mauritanië	b5f4ac43-afc4-4063-81a8-4aeacebb6cd8	f
134	6021	Kyrgyzstan	c6949f47-52e4-44cd-ba66-6a11863d1e42	f
135	6022	China	db660873-7ee9-4921-9922-bc5cf585841b	f
136	6023	Afganistan	349931d6-6ed9-472f-aa7b-6bf3e5ecc4e9	f
137	6024	Indonesië	dfdfc950-311f-4832-a196-02b3596978f6	f
138	6025	Guyana	cb28a2c0-3106-4637-b03e-8d1ec80cfe81	f
139	6026	Noordviëtnam	44094964-7355-40da-a8fe-7672a8ccbfff	f
140	6027	Noorwegen	4c649f77-8857-4fd8-8bfe-25cd1fcc011e	f
141	6028	San Marino	38f5c18d-3c4b-4ec0-bf09-50fc48bb7d99	f
142	6029	Duitsland	968b623e-183b-4e4c-8580-233499d0164b	f
143	6030	Nederland	7139d886-5bd8-4bd1-b2ae-78d5822163aa	f
144	6031	Kambodja	6ecb7173-799f-4194-86f2-6a6926a2529a	f
145	6032	Fiji	e8b462ac-b0cd-4d32-ba34-ba73ddb96b6c	f
146	6033	Bahama-eilanden	6f2d5d65-1ec7-48ec-a05e-22baf04445d3	f
147	6034	Israël	dd789db4-7491-437a-bc68-87529abf954f	f
148	6035	Nepal	7aae4ca0-ca2f-42ee-b2a2-7e9debdbcb3d	f
149	6036	Zuidkorea	a5ab3b45-8dee-4d0a-84ac-6218cae04bcb	f
150	6037	Spanje	4758bc4c-ec92-49b0-b6c6-af5e05f00fa2	f
151	6038	Oekraine	bd59b067-780e-4c4d-bfb9-6ab48db669c5	f
152	6039	Grootbrittannië	9bc9dedb-5cfd-4c24-a650-cddc4edd0c4d	f
153	6040	Niger	09fac1fc-5204-4f86-b7f1-399f6bed0793	f
154	6041	Haïti	d597dbdf-1740-4f53-a543-a10b13b85edf	f
155	6042	Jordanië	0884ddac-89a6-46aa-9ffc-e4a78453ec72	f
156	6043	Turkije	d2cc7dea-01fc-42d1-a68b-982f31c26ecb	f
157	6044	Trinidad en Tobago	3ad069b6-32eb-49c5-a10b-6c266c5e2f71	f
158	6045	Joegoslavië	09db526b-e012-4311-8a90-430cbb4b846f	f
159	6046	Bovenvolta	cee5a033-05ec-4211-9f5b-8d759181878b	f
160	6047	Algerije	a1a63cbf-bd03-4b24-b13d-91a64b7007d2	f
161	6048	Gabon	1e05a11a-52cc-40c2-a173-e6453c76fc81	f
162	6049	Noordkorea	0dc8ac02-694e-4c4e-9fc8-b7b958c62c91	f
163	6050	Oezbekistan	bfff608f-4e2b-4933-ac0f-3c4f070765fe	f
164	6051	Sierra Leone	83132fc4-911e-4d88-bcdb-c1e9d7bb7526	f
165	6052	Brits Honduras	0d0c070b-399a-4db2-a24f-6ce5d0c6cd4f	f
166	6053	Canarische eilanden	ce798fd1-264e-42ef-8e1c-ef34076f8897	f
167	6054	Frans Polynesië	d8b26ecc-a839-449f-a611-59b4501967a4	f
168	6055	Gibraltar	35a56bc8-5068-47e6-8543-76b937ad8535	f
169	6056	Portugees Timor	cde9cad7-4cb8-4f39-89eb-7b65dcdd7768	f
170	6057	Tadzjikistan	ccd305ee-6d89-4c34-afae-978773cbff51	f
171	6058	Britse Salomons-eilanden	b1e94303-00b6-4220-96ae-85cfd7dba581	f
172	6059	São Tomé en Principe	88b207b1-5f59-45b8-8fa6-5aeb31da28ab	f
173	6060	Sint Helena	dd70b434-3db1-4b73-8ee8-34e358ee3a20	f
174	6061	Tristan Da Cunha	8c6841de-2128-433e-a3f0-b6103830d670	f
175	6062	Westsamoa	a18618db-9088-4ae9-abab-6c33efa9e8ac	f
176	6063	Toerkmenistan	5b7b18f2-a614-4c28-847f-df8b7289fde4	f
177	6064	Georgië	077c6260-c5f2-4a14-a2a8-1e1ae702f9e3	f
178	6065	Bosnië-Herzegovina	49724f62-ba06-4f68-90f9-8e8a0021e0a9	f
179	6066	Tsjechië	5307797a-16c8-4dc9-9dfc-e188fdf266ff	f
180	6067	Slowakije	1318b978-2b2f-440b-aebe-26a45f3ed99d	f
181	6068	Federale Republiek Joegoslavië	cc7cd0b7-9535-4e89-b3a1-c1ce76a11fd8	f
182	6069	Democratische Republiek Congo	be1a854f-3706-4dec-a5d6-d8e565104498	f
183	7001	Uganda	655024f0-9b42-4aed-8624-e70e97554ff9	f
184	7002	Kenya	52b88ba2-ccd2-4d14-a228-d5c3a1a4a406	f
185	7003	Malta	73228ccb-7e83-4adf-8bda-7c552fc76944	f
186	7004	Barbados	faf8efa0-ad65-4ec4-a834-6cb7fc643419	f
187	7005	Andorra	e3e09124-992f-41d2-b581-50961898ff17	f
188	7006	Mexico	ae54f982-f25f-4079-8da2-867346836e06	f
189	7007	Costa Rica	8f63aa17-1317-4e2c-81f8-8a83e9949884	f
190	7008	Gambia	7482e043-727c-472d-803f-1325b286711e	f
191	7009	Syrië	06c5524e-1b7f-4de6-8807-b3db0aff7ee9	f
192	7011	Nederlandse Antillen	41bc6e4b-159c-41f3-b2eb-446ca449424e	f
193	7012	Zuidjemen	63b94fef-6c8d-42bc-b410-82c08d776ffa	f
194	7014	Egypte	39db2d68-8742-48b1-8dfc-586364af73a0	f
195	7015	Argentinië	671e9025-0664-4419-bf1b-b554cdea0be7	f
196	7016	Lesotho	c9b53645-5188-4b4e-8d13-f9adb1bcefe7	f
197	7017	Honduras	70ca7335-40e1-4e0b-adad-451625b7f4ab	f
198	7018	Nicaragua	07c9624c-0ca2-45e7-8ecd-443b6cd2987f	f
199	7020	Pakistan	d62d8919-70bf-40da-92d2-746788d2d7e9	f
200	7021	Senegal	d38ada9f-55be-4fc5-b819-7e9359727f10	f
201	7023	Dahomey	d81adfa9-df43-4685-9b66-17464992eda4	f
202	7024	Bulgarije	75d8e7a7-19f1-4956-8344-4adb0be2c4ec	f
203	7026	Maleisië	2f4653e3-9629-4768-884d-3d64cab9deff	f
204	7027	Dominicaanse Republiek	bd6bc111-31ad-441e-ad9b-90bf7f8fee04	f
205	7028	Polen	d786ae5b-a541-4ee3-a5c9-1e640497588c	f
206	7029	Rusland (oud)	450e154a-cb09-4001-ab24-9dfd7d831bf5	f
207	7030	Britse Maagdeneilanden	ff9a54a2-0f05-4e56-8128-d875fb96a4ef	f
208	7031	Tanzania	86c8ddaf-0232-467f-b49d-d9049e76c71f	f
209	7032	El Salvador	8008566e-7243-40c2-9bbe-83606f0add9e	f
210	7033	Sri Lanka	d45dd2d2-1ad7-4d25-a159-1a212d97ad2f	f
211	7034	Soedan	1233c50d-b3f1-4b28-a37b-903fc161304a	f
212	7035	Japan	1dbb3cac-c11d-4a0d-b33e-877e440a68d6	f
213	7036	Hongkong	3ecc0327-037b-4689-95d1-bb71aa60a85e	f
214	7037	Panama	b1fed16a-c7a3-4fcb-90f9-901c8fa54461	f
215	7038	Uruguay	7c8418b0-872e-45eb-94ea-5de597b49641	f
216	7039	Ecuador	2a20c63d-5ac1-44ec-b825-17f02dcc609b	f
217	7040	Guinee	5efced5c-e1b6-46bb-acbf-ddd0428085f0	f
218	7041	Maldiven	a3b58073-ce44-4c8a-9b65-9e20dcd48c6f	f
219	7042	Thailand	2f220e29-4e88-4c46-b56e-a2e6fa82c9fd	f
220	7043	Libanon	1450d655-5e68-4a50-b404-572148fe5b8d	f
221	7044	Italië	a5ac9d79-04df-4a5a-8345-bf63a0632af5	f
222	7045	Koeweit	c6c7bd22-8f86-4bcf-8631-5085e3c8c397	f
223	7046	India	2314933b-acb8-44d8-b1d7-a71a0d75314d	f
224	7047	Roemenië	e13aa2d1-2abc-408e-ae53-2519ea881d97	f
225	7048	Tsjechoslowakije	a5ad94ce-0960-4322-89e9-ec731cee6587	f
226	7049	Peru	3e61bfa4-b7ef-47b2-9f44-bc26faba9175	f
227	7050	Portugal	0f564722-86ad-4113-952d-a91c0b1e2a9e	f
228	7051	Oman	901b2edf-0379-48fd-83a3-3cd45c2437d1	f
229	7052	Mongolië	f0433d2b-9eac-4305-ad10-51fe848ffdbe	f
230	7054	Verenigde Arabische Emiraten	fc88817a-90ea-4eaf-9496-cae7671b13a1	f
231	7055	Tibet	b8e5e916-0a42-4bbc-b109-0c717d48b1fa	f
232	7057	Nauru	15185d83-950e-475a-a917-405fa06075e1	f
233	7058	Nederlands Nieuwguinea	6ce7416d-e13d-4359-b65a-e517a741d1c2	f
234	7059	Tanganyika	93556942-ca91-40a2-ac1d-7c2f4c78c0c3	f
235	7060	Palestina	d9ecff10-d1b4-4c02-9a5f-46df85811cf9	f
236	7062	Brits Westindië	dea2b0eb-0043-403b-93d4-fd38d1b47f8b	f
237	7063	Portugees Afrika	53ca81d1-4207-40be-b5e8-ddaaa50fd8df	f
238	7064	Letland	68083864-4e84-4cba-992b-b3ab72fdad00	f
239	7065	Estland	544740b9-a9ca-47cc-bce2-663390da3511	f
240	7066	Litouwen	15607076-de33-4e2a-b19c-b977d21a5471	f
241	7067	Brits Afrika	062b3889-efa0-4d8b-ab7c-8ab5a1049648	f
242	7068	Belgisch Congo	77067ff3-0c12-4de2-a106-6b388f68d2d7	f
243	7070	Brits Indië	1253a4c5-8f8c-4edf-847f-bca0ae78644f	f
244	7071	Noordrhodesië	821f283f-54e0-4e2c-bfcf-25e966aefaa7	f
245	7072	Zuidrhodesië	fa2b779e-2ab2-4b9e-bdc7-b8ff5e20cfef	f
246	7073	Saarland	24ccd504-dab9-409b-9ce1-4fd8eeb84712	f
247	7074	Frans Indo China	166fe0c3-6a25-40b8-ad35-318bf4aaa823	f
248	7075	Brits Westborneo	5dc98a4c-3388-404b-9d0d-3e2cee2d4416	f
249	7076	Goudkust	201a03d3-7040-4e1d-ab2e-9fc6247d9bff	f
250	7077	Ras-El-Cheima	92bc5370-cdbf-49ac-8523-e9ff22fa42e1	f
251	7079	Frans Congo	6bee2ab5-ec83-4147-b867-7dad693238fc	f
252	7080	Siam	02b22e8b-54dd-4942-bb0d-a08cf200881e	f
253	7082	Brits Oostafrika	af4ce11a-fa05-4b13-b40d-73048d107876	f
254	7083	Brits Noordborneo	0423ad04-a2a8-432d-be33-2b7e7429cf7d	f
255	7084	Bangladesh	67b43859-1767-4733-ad2d-589d3f78d8d3	f
256	7085	Duitse Democratische Republiek	f1d4d8c8-0025-4c7a-a8e0-9d38f1bc34d0	f
257	7087	Madeira-eilanden	ca1fa6c7-03ea-4235-8667-829a92448cc1	f
258	7088	Amerikaanse Maagdeneilanden	60d7c96e-f3f3-4936-a1f5-a4e2be456a25	f
259	7089	Australische Salomonseilanden	f9863a02-5f1f-462b-a802-31a79d547ee0	f
260	7091	Spaanse Sahara	1a0f4afd-bc8d-49b1-b2d9-328c35d38a79	f
261	7092	Caymaneilanden	8a62bc62-1d6f-432c-a866-f4e57a53b9c3	f
262	7093	Caicoseilanden	b4c73c5f-e53b-4101-afd1-8eb8810cfe29	f
263	7094	Turkseilanden	927d9285-5710-4c3d-8b43-ca5a435fbfeb	f
264	7095	Brits Territorium in Antarctica	bee125e2-1d9d-447b-8bde-4e2366b3551b	f
265	7096	Brits Territorium in de Indische Oceaan	faa62abc-fec3-4594-83d8-4b7585a94586	f
266	7097	Cookeilanden	8e3a8da6-d79c-4413-9e0a-7eb1a90f595a	f
267	7098	Tokelau-eilanden	f98cd51a-5e33-455e-9111-3891377fa8cf	f
268	7099	Nieuwcaledonië	9c2f11b7-e118-4bce-85a1-e94f92b94d2d	f
269	8000	Hawaii-eilanden	57bf4be2-f820-4303-80b7-a4d250cd371a	f
270	8001	Guam	e39f94bb-7c2a-4471-bb51-972539cb2337	f
271	8002	Amerikaans Samoa	15a43e89-e057-4d82-8a33-5646544c13cb	f
272	8003	Midway	93b607f9-4594-4e8d-9fee-355338ee8f16	f
273	8004	Ryukyueilanden	04105bce-d192-47cc-99b3-87abdd1a73a2	f
274	8005	Wake	73573ced-075c-45c0-8a47-fecb83f77ffc	f
275	8006	Pacific eilanden	9c95658d-9789-4aff-bb0f-a9a2539837a3	f
276	8008	Grenada	a0abb52a-64c4-4166-b80c-502a63c2a717	f
277	8009	Marianen	76eb1759-70cd-4383-8535-945ca156617e	f
278	8010	Cabinda	be94ae0a-f07e-4ee1-951d-75544abad18c	f
279	8011	Canton en Enderbury	e903a2b6-8434-4e71-b8cf-6d90fc211075	f
280	8012	Christmaseiland	614a5466-4774-464e-87c6-481f5bbe3776	f
281	8013	Cocoseilanden	77edcf3a-47ed-4562-ab0b-6dd3571d75a8	f
282	8014	Faeröer	35ec0828-4829-489b-944b-afabec555ae0	f
283	8015	Montserrat	10853987-4dd1-4ae6-a388-4e9f163c4736	f
284	8016	Norfolk	08fc5d42-aeef-4b44-9c3a-3eb6a74ecca0	f
285	8017	Belize	b7f2c849-88a7-4972-87eb-c3a1c071d5d8	f
286	8018	Tasmanië	24446b25-8484-4124-91c7-9d1268104ec8	f
287	8019	Turks- en Caicoseilanden	4a5e6707-64e0-4539-8910-5a93594607ec	f
288	8020	Puerto Rico	7fe6caad-b96a-478b-a4e2-63090d781da6	f
289	8021	Papua-Nieuwguinea	06fd57ac-3696-4994-a670-f9b18876f80e	f
290	8022	Solomoneilanden	71887499-d7fd-44a9-b53e-d8b96901d09d	f
291	8023	Benin	4c4f4464-8a8b-4af3-98ff-8aaa4a0ba91b	f
292	8024	Viëtnam	00af179c-c58d-4892-b87f-42c2c74eee39	f
293	8025	Kaapverdië	e31c4b20-fe5f-4864-958e-98df3b685aab	f
294	8026	Seychellen	0226318d-f3ce-44a4-9b4d-d8e88d4048b9	f
295	8027	Kiribati	3112a681-6a66-4a6a-9dde-980594ff81e0	f
296	8028	Tuvalu	eba96c80-d2c8-4a8a-8b40-a9460f5ca0b7	f
297	8029	Sint Lucia	17bd2118-5b3d-4724-87fc-205f21b046e3	f
298	8030	Dominica	73ead23b-d804-4984-9fa8-f3f320e744b3	f
299	8031	Zimbabwe	661a10f9-931f-4bc9-b3bf-97d3e642e7ec	f
300	8032	Doebai	6683759c-f7d2-41b9-8bdf-ab9879b9c41e	f
301	8033	Nieuwehebriden	68ce232f-cf0d-41d5-90ce-9ae2d75d6344	f
302	8034	Kanaaleilanden	ccd85a43-9aeb-4852-be4e-0d3192a3194d	f
303	8035	Man	020ee313-4860-466d-9d7b-7721b23721cc	f
304	8036	Anguilla	62c706a7-8187-487c-b0fd-e5261d246509	f
305	8037	Saint Kitts-Nevis	f73ea327-eec7-4587-a6d8-1c9cf1ab1664	f
306	8038	Antigua	d1916f6e-ed77-4539-acf2-125188c32a82	f
307	8039	Sint Vincent	2680234b-6b07-4b32-b1aa-5c8ea883879d	f
308	8040	Gilberteilanden	371b7a84-2ca3-4b23-85de-397d2a92f3a7	f
309	8041	Panamakanaalzone	cc142aa8-8b28-44d2-8d08-2b21e0b67538	f
310	8042	Saint Kitts-Nevis-Anguilla	ce8eb788-f324-49af-8149-6b062650a8c8	f
311	8043	Belau	4cd268d9-f416-4ebf-a111-f14965af891a	f
312	8044	Republiek van Palau	df7204f2-f97f-41ee-ac5d-2328b8a8ccae	f
313	8045	Antigua en Barbuda	b3ea09c2-74dc-44fe-9698-df0b3f7b4742	f
314	9000	Newfoundland	e04fef14-9e6f-48dd-90e9-a6fba8dc0e6d	f
315	9001	Nyasaland	4f516597-ecfc-4d5c-a896-4d5c498ff337	f
316	9003	Eritrea	9049d172-44a9-4ba5-9c96-2d5c4bd2ed88	f
317	9005	Ifni	6c264d6b-bdfc-49d9-a454-7a102a70ef7e	f
318	9006	Brits Kameroen	cc9a03d5-3465-4a2c-9d96-a7e40fdae8ea	f
319	9007	Kaiser Wilhelmsland	d2b6e78a-aeb8-43fa-9780-88ed3e6e23c0	f
320	9008	Kongo	3bb4c8bf-4ddd-469f-b755-bf8c71f5ec4c	f
321	9009	Kongo Kinshasa	b4ecf2c8-8cab-425c-b180-9aa071d4c30a	f
322	9010	Madagaskar	eeb59ff5-306a-46b8-b076-e49192ac3f33	f
323	9013	Kongo Brazzaville	f0ebe912-bc1f-4ef1-97d8-9889a3e03fbf	f
324	9014	Leewardeilanden	6dc5cd3b-e62c-4490-885d-80c671ac766d	f
325	9015	Windwardeilanden	e28ca37f-5663-46df-8ad0-33a5d03f3d40	f
326	9016	Frans gebied van Afars en Issa's	c312feb8-edb7-4e07-9b45-f55f1de8497a	f
327	9017	Phoenixeilanden	41f834a7-3d67-4219-ae4e-9ee3fc6aa74d	f
328	9020	Portugees Guinee	028ce053-5f08-47e1-a82a-d8eb1a283a56	f
329	9022	Duits Zuidwestafrika	046b2d28-d356-488e-834a-bfe78cab611c	f
330	9023	Namibië	e0ce9839-8885-4272-b91a-6222360fc06c	f
331	9027	Brits Somaliland	0624db74-24d5-4443-a497-5287b5898b1c	f
332	9028	Italiaans Somaliland	cc698900-2ebd-4511-9e2d-d7f6947dcf95	f
333	9030	Nederlands Indië	a23db607-f297-4601-aed8-0ca6b1635918	f
334	9031	Brits Guyana	7f053f21-230d-4e28-86fd-317c06d69851	f
335	9036	Swaziland	cd9ca617-4069-4e70-b616-4e6094c4fbf0	f
336	9037	Qatar	6af8ee03-40dc-4618-b544-0014d6028074	f
337	9041	Aden	d33e458b-5307-4155-8b28-b3a084793030	f
338	9042	Zuidarabische Federatie	40f75506-88fb-4ef0-af95-c9343bd1921d	f
339	9043	Equatoriaalguinee	058b8779-b7da-45ac-819e-da95732f5ed5	f
340	9044	Spaans Guinee	34c277ba-9e80-4d72-90a9-36f56a576c9b	f
341	9047	Verenigde Arabische Republiek	52e9de2c-b6ba-43d5-8211-1bf924ea9125	f
342	9048	Bermuda	59099e78-63bd-46ab-8d1c-76f82338e1e8	f
343	9049	Sovjetunie	d4596a1e-66a4-42bb-a1ae-3df092ed8658	f
344	9050	Duits Oostafrika	6078bdb0-ff47-4599-a606-2a0c67426167	f
345	9051	Zanzibar	9b2a1615-a0f8-485a-be91-97eb87fd5143	f
346	9052	Ceylon	246cec1d-4293-4647-b0cc-f0086468ece2	f
347	9053	Muscat en Oman	0458bf24-7165-40bf-8054-3021ee0452da	f
348	9054	Trucial Oman	c98f006c-f8c2-4acc-bd42-cf050622e6b0	f
349	9055	Indo China	9999b199-151a-4b8e-86d3-960807c4f537	f
350	9056	Marshalleilanden	b8ac54cd-167f-4abc-9d4c-b3c93bcc9a4f	f
351	9057	Sarawak	e2702fd0-fcff-4ab5-b16c-78f1542d6a89	f
352	9058	Brits Borneo	30c8b430-32a2-4e4a-a8ca-3391c56a148b	f
353	9060	Sabah	e02a904e-e316-4c83-bd62-b93b563d40b9	f
354	9061	Aboe Dhabi	6f6b1133-7cd8-4c26-87a3-dc51388d8e32	f
355	9062	Adjman	fa95ef05-e436-482a-96c3-71afe37a7802	f
356	9063	Basoetoland	1310f6be-6968-4ba0-96c8-1d1543b9bc21	f
357	9064	Bechuanaland	d8f1e5f2-d68b-4e95-8738-d54fc4f7737a	f
358	9065	Foedjaira	15fc93de-b739-4e2c-9eee-92cde707a80b	f
359	9066	Frans Kameroen	71aabd2d-a130-480c-93b3-8bbaa824b11f	f
360	9067	Johore	a27e0de3-9a4a-46c1-a4b0-92d332d6854b	f
361	9068	Korea	3544feec-8dba-4533-9529-a9405dd01652	f
362	9069	Labuan	cc8a06ff-1c55-43cc-b411-06902ff94b23	f
363	9070	Oem el Koewein	6ad55c27-5d2f-428b-8efb-518f5bd4f471	f
364	9071	Oostenrijk-Hongarije	19c0e3d9-056c-45db-b5b2-e111a668ed27	f
365	9072	Portugees Oost Afrika	4f86bce1-f029-4f20-bb8f-e9b1f77f3424	f
366	9073	Portugees West Afrika	803fc072-c65d-4d12-a82b-d117a585b011	f
367	9074	Sjardja	cb36a420-f08e-4817-ac71-87c72899e9d9	f
368	9075	Straits Settlements	37f3ecf6-2006-43b5-aa7b-79a7252e0e57	f
369	9076	Abessinië	ed56eea4-addd-49a1-8d84-34b9f111d5c4	f
370	9077	Frans West Afrika	3f8fe1b9-90dd-4f53-837e-3c7d28c3f12d	f
371	9078	Frans Equatoriaal Afrika	c8256eea-a4c4-4cde-9bf6-57422fe0ef1c	f
372	9081	Oeroendi	3af27e09-3486-4aba-b7ca-c80584fce533	f
373	9082	Roeanda-Oeroendi	061be605-f8cb-46e6-94fa-23c8f5959fe5	f
374	9084	Goa	2814320b-71f2-4f1e-a71f-a43f7004c134	f
375	9085	Dantzig	573445be-8cbc-4f35-af3d-de4cf2022cab	f
376	9086	Centrafrika	a1f4fec9-4fe7-4423-86e5-ba93fe303c17	f
377	9087	Djibouti	8aa0feeb-3636-4214-b262-ac2008589982	f
378	9088	Transjordanië	c824d624-22d5-4ae6-a2dc-f94b6957a3b4	f
379	9089	Bondsrepubliek Duitsland	4d1d4b1a-358b-471b-9563-bb1aecb22e0b	f
380	9090	Vanuatu	2de71db7-de99-4ce0-9b3a-1f56b0d17899	f
381	9091	Niue	cde07dc7-db88-49b4-b420-adcc58e0b418	f
382	9092	Spaans Noordafrika	baa94555-80fe-4242-a00e-6d2e1a2c4598	f
383	9093	Westelijke Sahara	669b1dd2-0337-4ea9-88c4-67e0a010cd6b	f
384	9094	Micronesia	68509783-7365-4d5a-af98-56dfbb3d09f3	f
385	9095	Svalbardeilanden	000d569f-6cdc-4d28-8572-5f7bd45e2cbc	f
386	9999	Internationaal gebied	86222636-84db-4768-b339-bb6f0b1eadbe	f
\.


--
-- Data for Name: custom_object; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.custom_object (id, uuid, custom_object_version_id) FROM stdin;
\.


--
-- Data for Name: custom_object_relationship; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.custom_object_relationship (id, relationship_type, relationship_magic_string_prefix, custom_object_id, custom_object_version_id, related_document_id, related_case_id, related_custom_object_id, related_uuid, related_person_id, related_organization_id, related_employee_id, source_custom_field_type_id, uuid) FROM stdin;
\.


--
-- Data for Name: custom_object_type; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.custom_object_type (id, uuid, catalog_folder_id, custom_object_type_version_id, authorization_definition) FROM stdin;
\.


--
-- Data for Name: custom_object_type_version; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.custom_object_type_version (id, uuid, name, title, status, version, custom_field_definition, authorizations, relationship_definition, date_created, last_modified, date_deleted, custom_object_type_id, audit_log, external_reference, subtitle) FROM stdin;
\.


--
-- Data for Name: custom_object_version; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.custom_object_version (id, uuid, title, status, version, custom_object_version_content_id, custom_object_type_version_id, date_created, last_modified, date_deleted, custom_object_id, external_reference, subtitle) FROM stdin;
\.


--
-- Data for Name: custom_object_version_content; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.custom_object_version_content (id, archive_status, archive_ground, archive_retention, custom_fields) FROM stdin;
\.


--
-- Data for Name: directory; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.directory (id, name, case_id, original_name, path, uuid) FROM stdin;
\.


--
-- Data for Name: export_queue; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.export_queue (id, uuid, subject_id, subject_uuid, expires, token, filestore_id, filestore_uuid, downloaded) FROM stdin;
\.


--
-- Data for Name: file; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.file (search_index, search_term, object_type, searchable_id, search_order, id, filestore_id, name, extension, root_file_id, version, case_id, metadata_id, subject_id, directory_id, creation_reason, accepted, rejection_reason, reject_to_queue, is_duplicate_name, publish_pip, publish_website, date_created, created_by, date_modified, modified_by, date_deleted, deleted_by, destroyed, scheduled_jobs_id, intake_owner, active_version, is_duplicate_of, queue, document_status, generator, lock_timestamp, lock_subject_id, lock_subject_name, uuid, confidential, rejected_by_display_name, intake_group_id, intake_role_id, skip_intake, created_by_display_name) FROM stdin;
\.


--
-- Data for Name: file_annotation; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.file_annotation (id, file_id, subject, properties, created, modified) FROM stdin;
\.


--
-- Data for Name: file_case_document; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.file_case_document (id, file_id, magic_string, bibliotheek_kenmerken_id, case_id) FROM stdin;
\.


--
-- Data for Name: file_derivative; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.file_derivative (id, file_id, filestore_id, max_width, max_height, date_generated, type) FROM stdin;
\.


--
-- Data for Name: file_metadata; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.file_metadata (id, description, trust_level, origin, document_category, origin_date, pronom_format, appearance, structure, creation_date) FROM stdin;
\.


--
-- Data for Name: filestore; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.filestore (id, uuid, thumbnail_uuid, original_name, size, mimetype, md5, date_created, storage_location, is_archivable, virus_scan_status) FROM stdin;
\.


--
-- Data for Name: gegevensmagazijn_subjecten; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.gegevensmagazijn_subjecten (subject_uuid, nnp_uuid) FROM stdin;
\.


--
-- Data for Name: gm_adres; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.gm_adres (id, straatnaam, huisnummer, huisletter, huisnummertoevoeging, nadere_aanduiding, postcode, woonplaats, gemeentedeel, functie_adres, datum_aanvang_bewoning, woonplaats_id, gemeente_code, hash, import_datum, adres_buitenland1, adres_buitenland2, adres_buitenland3, landcode, natuurlijk_persoon_id, deleted_on, bag_id, geo_lat_long) FROM stdin;
\.


--
-- Data for Name: gm_bedrijf; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.gm_bedrijf (id, gegevens_magazijn_id, dossiernummer, subdossiernummer, hoofdvestiging_dossiernummer, hoofdvestiging_subdossiernummer, vorig_dossiernummer, vorig_subdossiernummer, handelsnaam, rechtsvorm, kamernummer, faillisement, surseance, telefoonnummer, email, vestiging_adres, vestiging_straatnaam, vestiging_huisnummer, vestiging_huisnummertoevoeging, vestiging_postcodewoonplaats, vestiging_postcode, vestiging_woonplaats, correspondentie_adres, correspondentie_straatnaam, correspondentie_huisnummer, correspondentie_huisnummertoevoeging, correspondentie_postcodewoonplaats, correspondentie_postcode, correspondentie_woonplaats, hoofdactiviteitencode, nevenactiviteitencode1, nevenactiviteitencode2, werkzamepersonen, contact_naam, contact_aanspreektitel, contact_voorletters, contact_voorvoegsel, contact_geslachtsnaam, contact_geslachtsaanduiding, authenticated, authenticatedby, import_datum, verblijfsobject_id, vestigingsnummer, vestiging_huisletter, correspondentie_huisletter, vestiging_adres_buitenland1, vestiging_adres_buitenland2, vestiging_adres_buitenland3, vestiging_landcode, correspondentie_adres_buitenland1, correspondentie_adres_buitenland2, correspondentie_adres_buitenland3, correspondentie_landcode, rsin, oin, main_activity, secondairy_activities, date_founded, date_registration, date_ceased, vestiging_latlong, vestiging_bag_id, preferred_contact_channel) FROM stdin;
\.


--
-- Data for Name: gm_natuurlijk_persoon; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.gm_natuurlijk_persoon (id, gegevens_magazijn_id, betrokkene_type, burgerservicenummer, a_nummer, voorletters, voornamen, geslachtsnaam, voorvoegsel, geslachtsaanduiding, nationaliteitscode1, nationaliteitscode2, nationaliteitscode3, geboorteplaats, geboorteland, geboortedatum, aanhef_aanschrijving, voorletters_aanschrijving, voornamen_aanschrijving, naam_aanschrijving, voorvoegsel_aanschrijving, burgerlijke_staat, indicatie_geheim, import_datum, adres_id, authenticatedby, authenticated, datum_overlijden, verblijfsobject_id, aanduiding_naamgebruik, onderzoek_persoon, onderzoek_huwelijk, onderzoek_overlijden, onderzoek_verblijfplaats, partner_a_nummer, partner_burgerservicenummer, partner_voorvoegsel, partner_geslachtsnaam, datum_huwelijk, datum_huwelijk_ontbinding, landcode, naamgebruik, adellijke_titel, preferred_contact_channel) FROM stdin;
\.


--
-- Data for Name: groups; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.groups (id, path, name, description, date_created, date_modified, uuid, v1_json) FROM stdin;
1	{1}	Development	Development root	2021-08-20 11:10:37.423386	2021-08-20 11:10:37.423386	432555e5-2a37-4380-8571-5f51df8c07ab	{"type": "group", "preview": "Development", "instance": {"name": "Development", "group_id": 1, "description": "Development root", "date_created": "2021-08-20T11:10:37Z", "date_modified": "2021-08-20T11:10:37Z"}, "reference": "432555e5-2a37-4380-8571-5f51df8c07ab"}
2	{1,2}	Backoffice	Default backoffice	2021-08-20 11:10:37.423386	2021-08-20 11:10:37.423386	923996fc-f8f8-4745-9886-a8b00bce1bbe	{"type": "group", "preview": "Backoffice", "instance": {"name": "Backoffice", "group_id": 2, "description": "Default backoffice", "date_created": "2021-08-20T11:10:37Z", "date_modified": "2021-08-20T11:10:37Z"}, "reference": "923996fc-f8f8-4745-9886-a8b00bce1bbe"}
3	{1,3}	Frontoffice	Default frontoffice	2021-08-20 11:10:37.423386	2021-08-20 11:10:37.423386	e7a6b730-b98a-4757-bbfd-07c2edda5180	{"type": "group", "preview": "Frontoffice", "instance": {"name": "Frontoffice", "group_id": 3, "description": "Default frontoffice", "date_created": "2021-08-20T11:10:37Z", "date_modified": "2021-08-20T11:10:37Z"}, "reference": "e7a6b730-b98a-4757-bbfd-07c2edda5180"}
\.


--
-- Data for Name: interface; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.interface (id, name, active, case_type_id, max_retries, interface_config, multiple, module, date_deleted, uuid, objecttype_id) FROM stdin;
1	Authenticatie	t	\N	10	{"enable_user_login":"1"}	t	authldap	\N	90dc861c-aac7-4a2e-9370-1ce0a353e5cd	\N
\.


--
-- Data for Name: legal_entity_type; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.legal_entity_type (id, code, label, uuid, active) FROM stdin;
1	1	Eenmanszaak	645cf805-6ef9-42e4-bf32-ede66aecc885	t
2	2	Eenmanszaak met meer dan één eigenaar	1873346f-927c-4d23-9322-186e8aea758a	f
3	3	N.V./B.V. in oprichting op A-formulier	9f0d0a7d-6ece-4a54-823e-8b5b93e048b6	f
4	5	Rederij	ce7f249e-d996-4987-b660-b66067af0c7d	f
5	7	Maatschap	8171d3a2-0576-459f-b940-88e5a0d875d1	t
6	11	Vennootschap onder firma	284c6cef-b3d5-47e9-986f-23476d732825	t
7	12	N.V/B.V. in oprichting op B-formulier	0053b5a3-f2da-43c8-a736-125a5cad5a62	f
8	21	Commanditaire vennootschap met een beherend vennoot	c0e2868d-ba56-4d4f-bbbe-4bf53120dd43	t
9	22	Commanditaire vennootschap met meer dan één beherende vennoot	5d5bbe2c-c74b-4427-9f55-62470737e7f4	f
10	23	N.V./B.V. in oprichting op D-formulier	62fc1e5f-f04b-4bcd-a4db-0dd8a298a5d0	f
11	40	Rechtspersoon in oprichting	4344ce11-6cb7-433e-9eb1-8526ff9a4a55	f
12	41	Besloten vennootschap met gewone structuur	6ed6a0de-bbb9-432a-8e03-4f3294f6e10a	t
13	42	Besloten vennootschap blijkens statuten structuurvennootschap	4ffaf90c-db27-42c2-9e4a-400424bad6e5	f
14	51	Naamloze vennootschap met gewone structuur	5db79ef3-651a-4fd0-9211-ae7f4bb39ce7	t
15	52	Naamloze vennootschap blijkens statuten structuurvennootschap	0973fbd2-ed83-4dff-9d34-51f745b391c5	f
16	53	Naamloze vennootschap beleggingsmaatschappij met veranderlijk kapitaal	1ede5f01-6d76-49aa-ae26-75f2f4b20ce0	f
17	54	Naamloze vennootschap beleggingsmaatschappij met veranderlijk kapitaal blijkens statuten structuurvennootschap	e7fe016f-37cc-4e34-947e-ebfd49877952	f
18	55	Europese naamloze vennootschap (SE) met gewone structuur	912835bf-9f98-40d2-aba1-b8866e2b98ee	t
19	56	Europese naamloze vennootschap (SE) blijkens statuten structuurvennootschap	230077a3-b0a9-445f-885f-5890c3bfe133	f
20	61	Coöperatie U.A. met gewone structuur	686e0b53-91e7-4a5b-af08-5c512de94ba9	f
21	62	Coöperatie U.A. blijkens statuten structuurcoöperatie	f24d328e-1122-434f-b40b-dc4f5fe38c82	f
22	63	Coöperatie W.A. met gewone structuur	e04e5ee9-d712-43c7-8e3a-1bd8c84fa39d	f
23	64	Coöperatie W.A. blijkens statuten structuurcoöperatie	5bbc2951-5f0e-4203-8be6-259912272ead	f
24	65	Coöperatie B.A. met gewone structuur	27662752-6f20-4cf1-b081-662def3503c6	f
25	66	Coöperatie B.A. blijkens statuten structuurcoöperatie	bf276571-9380-4ad2-ab52-14b84ff133c6	f
26	70	Vereniging van eigenaars	edc1477f-2fc5-4e1f-bb61-233b9823509a	t
27	71	Vereniging met volledige rechtsbevoegdheid	3918d3ab-e11d-43ff-ae1c-51f5f523338f	f
28	72	Vereniging met beperkte rechtsbevoegdheid	fc7f7654-b23c-4ecd-b719-39d80f6c98c9	f
29	73	Kerkgenootschap	759b76bf-bceb-49d0-948b-4bbe17992326	t
30	74	Stichting	c91215d9-14b5-43e0-ba2b-1b8487e2b889	t
31	81	Onderlinge waarborgmaatschappij U.A. met gewone structuur	fc01a26d-1ec9-419a-b05d-c3c0a53dcab6	f
32	82	Onderlinge waarborgmaatschappij U.A. blijkens statuten structuuronderlinge	3c718f04-77e3-4ec7-87fc-6a896c16c8a4	f
33	83	Onderlinge waarborgmaatschappij W.A. met gewone structuur	649dba84-8c72-4a94-aa91-617db9dab51c	f
34	84	Onderlinge waarborgmaatschappij W.A. blijkens statuten structuuronderlinge	755f456d-a94f-44bb-bfe8-47277ee360ab	f
35	85	Onderlinge waarborgmaatschappij B.A. met gewone structuur	71580ddf-7ba8-4ee1-a383-fc20d562ed20	f
36	86	Onderlinge waarborgmaatschappij B.A. blijkens statuten structuuronderlinge	38fc7147-724a-4c30-9d3b-204aa99b859f	f
37	88	Publiekrechtelijke rechtspersoon	423a8ddf-81d3-4499-a0f1-33fad764839c	t
38	89	Privaatrechtelijke rechtspersoon	292840a2-4e03-47f8-b00b-d5d1ffe0392a	f
39	91	Buitenlandse rechtsvorm met hoofdvestiging in Nederland	7861a128-8419-4d09-9647-e31f3498faf0	f
40	92	Nevenvest. met hoofdvest. in buitenl.	c164718f-fd53-43bb-8b99-b403cf5cff43	f
41	93	Europees economisch samenwerkingsverband	0d566451-9f52-40f7-95e6-79033101d13f	f
42	94	Buitenl. EG-venn. met onderneming in Nederland	86641b3f-3e1f-45c7-87dc-9ba24b92c10d	f
43	95	Buitenl. EG-venn. met hoofdnederzetting in Nederland	454fc546-0d05-404a-80f4-8f2c3e2e9355	f
44	96	Buitenl. op EG-venn. lijkende venn. met onderneming in Nederland	47c9ab5c-1900-4e9c-8a2a-26ded7105a82	f
45	97	Buitenl. op EG-venn. lijkende venn. met hoofdnederzetting in Nederland	4c8a1012-8f38-4b41-8583-79f48f98c581	f
46	201	Coöperatie	8a5398b1-4b27-4395-9a52-568dff425aae	t
47	202	Vereniging	4e038ac3-8f8e-4391-b8e6-1f8ffb1a23fe	t
\.


--
-- Data for Name: logging; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.logging (id, zaak_id, betrokkene_id, aanvrager_id, is_bericht, component, component_id, seen, onderwerp, bericht, created, last_modified, deleted_on, event_type, event_data, created_by, modified_by, deleted_by, created_for, created_by_name_cache, object_uuid, restricted, uuid) FROM stdin;
\.


--
-- Data for Name: message; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.message (id, message, subject_id, logging_id, is_read, is_archived, created) FROM stdin;
\.


--
-- Data for Name: municipality_code; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.municipality_code (id, dutch_code, label, alternative_name, uuid, historical) FROM stdin;
1	9999	Testgemeente	\N	02dbb0de-ca03-473b-9f7d-1ca80360e878	f
2	1332	Testgemeente (historisch)	\N	02dbb0de-ca03-473b-9f7d-1ca80360e878	t
3	1999	RNI / Registratie Niet Ingezetenen	\N	b4edc05c-0f29-45d6-b12a-81b986a882fd	f
4	3	Appingedam	\N	0f44329e-ccb9-4bae-8c54-fef95b4b7328	t
5	5	Bedum	\N	26f48bd5-9247-45ad-936a-363a358f40e5	t
6	7	Bellingwedde	\N	7bb9e5f7-b594-42c6-a4c0-5694d9df89e9	t
7	9	Ten Boer	\N	831b4209-8c06-412d-ae91-44a59f324f3e	t
8	10	Delfzijl	\N	987667cd-f38f-49ad-8385-d8e56c023234	t
9	14	Groningen	\N	109e16db-d984-4f4b-9a23-87fe0718636a	f
10	15	Grootegast	\N	c812f63d-a7a9-49db-81b4-5ba00cda5d6b	t
11	17	Haren	\N	38d2429a-5095-4197-aec1-697e48b1552c	t
12	18	Hoogezand-Sappemeer	\N	998f2f43-c434-42e7-a72b-214f7dc83eff	t
13	22	Leek	\N	c0eebf0c-ccf7-4900-9968-6b43bfeed25a	t
14	24	Loppersum	\N	5039632c-3330-4acf-9b9b-4f4c44f561f8	t
15	25	Marum	\N	d84f6efe-3705-479d-bd0d-c5425466f88a	t
16	34	Almere	\N	17ceec51-4d87-4ac0-ac8e-29eeeda9e5d9	f
17	37	Stadskanaal	\N	7bba996b-b884-4e36-a7e6-96986416c320	f
18	39	Scheemda	\N	4e444792-9fc2-4750-8e37-730011f985de	t
19	40	Slochteren	\N	0e27c360-d2b1-4d7c-a0dc-a7a761046c46	t
20	47	Veendam	\N	3db65516-da22-40a8-8e45-003cbc1f74bf	f
21	48	Vlagtwedde	\N	25ed5668-3787-413c-a788-50685f69f64d	t
22	50	Zeewolde	\N	455865b0-a817-491a-a5f6-d9d6cb3a96e2	f
23	51	Skarsterlân	\N	4db9aa8c-4167-4bd2-b4f8-f044bffbc063	t
24	52	Winschoten	\N	e84c5221-7385-4b21-91d2-df82cbff34bf	t
25	53	Winsum	\N	dacc2f85-dd19-40eb-bd30-11c7b75a3625	t
26	55	Boarnsterhim	\N	d36988b5-9853-4315-876a-9c5644d0b32c	t
27	56	Zuidhorn	\N	60cbae0c-de29-4646-b6de-ceb8be152bba	t
28	58	Dongeradeel	\N	903a98f7-d4b4-4314-830a-0b6941a58608	t
29	59	Achtkarspelen	\N	04333305-e4c0-4ff1-b316-eb4e31569dbd	f
30	60	Ameland	\N	38236167-f86f-4aad-aec2-5258be11729a	f
31	63	het Bildt	\N	294196e0-fb9c-436f-acd8-98d6c90f1298	t
32	64	Bolsward	\N	a5e661ab-4eac-4221-bd35-ea8d51a3cffa	t
33	70	Franekeradeel	\N	1b3d05ff-24bf-47c5-abea-7e6ae3f63284	t
34	72	Harlingen	\N	0dc4eb94-53ff-4a6c-9687-7eb16da1d4cb	f
35	74	Heerenveen	\N	93707cf4-10f9-4564-af90-0d0b17a7f02e	f
36	79	Kollumerland en Nieuwkruisland	\N	f44c90c4-2faf-485b-bf60-63cc4e920462	t
37	80	Leeuwarden	\N	61ca9ea2-43c6-409b-8828-f643534de85c	f
38	81	Leeuwarderadeel	\N	23e6e34e-70f4-4ee1-8b0b-263f7af79933	t
39	82	Lemsterland	\N	fde85358-7b39-44cb-b30a-719f9ab4625b	t
40	83	Menaldumadeel	\N	5c3771d9-239c-4dfc-a317-06f7acfe7933	t
41	85	Ooststellingwerf	\N	5e70afbe-b617-4ed5-89d3-f9ad9f80cd90	f
42	86	Opsterland	\N	0429b2b6-8e4d-4102-837a-05d9b3bde6fe	f
43	88	Schiermonnikoog	\N	ac482447-79dd-492b-8f85-c3a497c147e3	f
44	90	Smallingerland	\N	09c3c4a4-2121-4f8b-97a8-c7bb1aa5262e	f
45	91	Sneek	\N	60c0021f-ec93-414e-a7db-5267eda7e6ed	t
46	93	Terschelling	\N	6c5ebe67-e7b9-41b5-bf13-e37f43ae63e8	f
47	96	Vlieland	\N	464ca01d-d868-4bed-a1ee-1c9b37362876	f
48	98	Weststellingwerf	\N	0101b3cb-2411-49c0-a282-e2e048776efe	f
49	104	Nijefurd	\N	2dc89308-e768-4d80-9287-252303b793b8	t
50	106	Assen	\N	ed386208-789a-4f1b-a0c6-9d4260f890fe	f
51	109	Coevorden	\N	64abd9cb-fa26-46d8-8fc5-d61f4ea4476f	f
52	114	Emmen	\N	d4c29c6f-35ca-4c35-a7de-5fda7a1adc51	f
53	118	Hoogeveen	\N	9258bd87-99ef-4ea0-aae9-23aad1898554	f
54	119	Meppel	\N	b61a52c4-7c52-44c3-984d-f7fb3298e9c6	f
55	140	Littenseradiel	\N	71cc8847-0a46-4e99-9400-b07ab49cc850	t
56	141	Almelo	\N	d34852f9-2cec-4a3e-85c1-f27d12cd994a	f
57	147	Borne	\N	7434dfbb-2c2b-42b4-9ace-0b18822c350d	f
58	148	Dalfsen	\N	b7c063b6-7cb4-4d0c-ae54-090bd8b85e82	f
59	150	Deventer	\N	d02401c0-475d-4dc2-ab29-a1e0cacc33ed	f
60	153	Enschede	\N	10f1b5da-a44b-45a2-b675-80efcdae1892	f
61	158	Haaksbergen	\N	b52804a0-7239-4507-9c94-27d33680bb5f	f
62	160	Hardenberg	\N	33bb37e6-303b-42d6-b602-b748d8e10645	f
63	163	Hellendoorn	\N	98d3a574-ff92-4f6e-ac77-f940eb2192f5	f
64	164	Hengelo	\N	501912d8-d21b-4a21-bf18-f44f6bcadb24	f
65	166	Kampen	\N	6dcea5f9-2048-406f-94a5-3a330a347703	f
66	168	Losser	\N	7063ae7d-b874-40cd-b217-ba795882fd67	f
67	171	Noordoostpolder	\N	3e01f991-6560-4489-bdc4-804ef2610c59	f
68	173	Oldenzaal	\N	889de30f-9f6e-4afe-9615-b641fbc11a26	f
69	175	Ommen	\N	9f5ae936-33cf-4e3b-829d-1e39abcacd56	f
70	177	Raalte	\N	f292f45e-c732-4c26-90fc-bbb1359a722a	f
71	180	Staphorst	\N	7b7fe3e9-98b7-4259-9077-61fe1dfb100a	f
72	183	Tubbergen	\N	5a7b179b-38d5-40f9-ad19-a3d01349374a	f
73	184	Urk	\N	a6aa39b8-e9b5-4e9f-aee2-4a582216d16a	f
74	189	Wierden	\N	ef51400c-436a-47f3-8cac-18dc780df97a	f
75	193	Zwolle	\N	259dfcd5-3f21-417f-b2d6-51ffdf8538cd	f
76	196	Rijnwaarden	\N	90a40fa9-0c9d-44d2-a5b5-05a62e566913	t
77	197	Aalten	\N	e28937f2-3c4a-4526-9175-61e16bb533d9	f
78	200	Apeldoorn	\N	fe8dd89e-7145-4f13-933d-8b0ff09ef146	f
79	202	Arnhem	\N	d35b15b9-3f3f-40ef-bdcd-9634f41bff84	f
80	203	Barneveld	\N	0fbb9c60-907d-46fa-82a5-ee36466610ff	f
81	209	Beuningen	\N	89b378e6-e3f6-49f1-90f3-6a6d86881d86	f
82	213	Brummen	\N	8161d3d1-e2ed-4369-a4de-2cc00212046e	f
83	214	Buren	\N	3cc70992-0b36-4c64-bfdf-7321ef1a82f4	f
84	216	Culemborg	\N	db1299c8-acad-417a-bae7-a437d0470a19	f
85	221	Doesburg	\N	822b4dd1-1699-4dcc-8624-92083280c9fb	f
86	222	Doetinchem	\N	13a6ef8f-ec49-40bd-8d56-5adb0d213f50	f
87	225	Druten	\N	b132892d-97e2-44b1-b71c-963e3f102f25	f
88	226	Duiven	\N	875f6c86-4e38-4179-9d36-71dc68977d4a	f
89	228	Ede	\N	29b1cc3f-ef72-4afd-97b9-4faff745db3b	f
90	230	Elburg	\N	66e73e10-de81-4b6b-a527-0fd651644443	f
91	232	Epe	\N	23b7a4bb-21d4-4d58-b004-4a67fa8ed167	f
92	233	Ermelo	\N	f6b36933-7f2a-4179-95fc-3066ae3b958f	f
93	236	Geldermalsen	\N	6a40ec8b-f64e-4d36-bf24-73d8a9b8b6e9	t
94	241	Groesbeek	\N	abe54589-e9a4-465f-b9aa-d9e9d7891abe	t
95	243	Harderwijk	\N	946ee42b-aa6d-4e7e-991b-2b16275f300d	f
96	244	Hattem	\N	2b731ec5-e97c-4712-847b-ec575139eef8	f
97	246	Heerde	\N	444bb582-4426-4020-8bb9-9326cd49af12	f
98	252	Heumen	\N	f7d01400-f3af-4bd3-996a-225a426ec29e	f
99	262	Lochem	\N	6ecbd429-52ca-4654-a6be-a5ec18c6ba3c	f
100	263	Maasdriel	\N	b3f5e4e1-830b-4d7f-a079-e32e436802ba	f
101	265	Millingen aan de Rijn	\N	e6feb3e1-8e82-4ac4-8756-81d6818d4451	t
102	267	Nijkerk	\N	973ad63a-0d66-495f-b398-9f65e80b33e2	f
103	268	Nijmegen	\N	26f44294-2301-4521-9bc5-e1f1e4072bdb	f
104	269	Oldebroek	\N	fa1e9d61-804c-459d-8e45-5b6fadbe75c5	f
105	273	Putten	\N	e4e1e25c-bc56-4d48-aa56-f37e5b1c467a	f
106	274	Renkum	\N	28f18caf-8aa1-4aa9-a097-dc88f3e51e0b	f
107	275	Rheden	\N	a9820528-da10-4cc1-bc5a-bfd5ad5cbeac	f
108	277	Rozendaal	\N	3e509dd2-5d9a-4be7-b94c-2f0bcc309880	f
109	279	Scherpenzeel	\N	901aad79-8048-4c3b-85f8-b4524483e59f	f
110	281	Tiel	\N	2928c71f-0e67-488f-b052-72b2912dfe9c	f
111	282	Ubbergen	\N	c21f6be1-944e-4ded-aae7-5804ec748915	t
112	285	Voorst	\N	0d265d53-fc98-4a6e-8584-d3d07d0f030e	f
113	289	Wageningen	\N	e1496898-f919-4701-be2d-d67996741f67	f
114	293	Westervoort	\N	3b6704ac-fcd5-4c40-842b-f99665a4662b	f
115	294	Winterswijk	\N	cb11f22f-d954-4537-b7c9-88603bd4f82d	f
116	296	Wijchen	\N	0ad26d1d-3711-43fa-9134-38daeae48466	f
117	297	Zaltbommel	\N	0f83e48e-ff21-421d-93cc-86da4e243555	f
118	299	Zevenaar	\N	313a3d0e-fde5-4c8c-905c-9986a746c3e6	f
119	301	Zutphen	\N	47c25787-89b3-41c0-a52e-103b48a874a9	f
120	302	Nunspeet	\N	b439b4f8-7a22-4bc2-a914-3ef5c999499c	f
121	303	Dronten	\N	c3272eb6-1479-4b8f-af09-948e301f68b9	f
122	304	Neerijnen	\N	3da5f7a4-27be-4219-be89-2ff428cb6bb6	t
123	305	Abcoude	\N	17a7cf54-fa09-4790-9290-a51ae34408c9	t
124	307	Amersfoort	\N	e01f24c7-1eed-4fd2-b656-032bfac31f57	f
125	308	Baarn	\N	8b3b81aa-7aa9-4bdb-ab1b-cdf380a9da6c	f
126	310	De Bilt	\N	51deb750-89de-4aba-9e59-bd7d45f1c254	f
127	311	Breukelen	\N	b974392a-b026-4753-b27c-5831bb77ab7c	t
128	312	Bunnik	\N	ae518c9f-4451-4781-b78f-3f3b6e822846	f
129	313	Bunschoten	\N	8cc3d752-76a6-4580-9d8c-50a9a2c31664	f
130	317	Eemnes	\N	6537421b-aca9-44c5-b158-b3385aaf6673	f
131	321	Houten	\N	f51f03ea-341a-447b-af55-e54cd23e5d7d	f
132	327	Leusden	\N	9b48c7e4-b3e2-4bd6-832f-b1c6d248fdfa	f
133	329	Loenen	\N	87cc0937-5313-4183-86d1-0066c9679510	t
134	331	Lopik	\N	8f7ee7e3-bbcd-42f8-88d4-08c4c09a0d1f	f
135	333	Maarssen	\N	7911eb6f-5bd9-4eb8-beb4-17bda9cd20d3	t
136	335	Montfoort	\N	e3d13bc1-d593-44b5-8cb3-ad6bc85e7d79	f
137	339	Renswoude	\N	3087a42a-c236-43ae-bab2-f7ca216ed815	f
138	340	Rhenen	\N	12d9210e-82c9-475b-af79-ab2b3ef92bcd	f
139	342	Soest	\N	2a943b87-5bf3-4a4e-8b8a-4eb56bb7a90d	f
140	344	Utrecht	\N	0425bb80-d193-4082-8b6e-9ff954a33e50	f
141	345	Veenendaal	\N	6b01d2ef-3316-44c9-8479-022a73ac366c	f
142	351	Woudenberg	\N	ead6ed60-3d18-45cc-914d-61c2c9203ff4	f
143	352	Wijk bij Duurstede	\N	c1c955e1-0f90-448c-a89c-fad73d060ca6	f
144	353	IJsselstein	\N	2c55c36a-0785-4f3c-966f-a804df513d8d	f
145	355	Zeist	\N	8a5ac601-2c81-417f-b801-0591ee57b662	f
146	356	Nieuwegein	\N	8e536667-446c-4ecc-bcda-00d5a98fd5cf	f
147	358	Aalsmeer	\N	0b8187cf-7a81-4b10-98f7-f79d42f01053	f
148	361	Alkmaar	\N	698befda-698a-478b-98c3-e81a00db5b8c	f
149	362	Amstelveen	\N	ddd0ce3c-67ab-4665-b7ab-b2eab9fd0925	f
150	363	Amsterdam	\N	0ad5aae2-a609-4a7e-b87e-3b44c7f2c663	f
151	364	Andijk	\N	35dce8cd-04dc-48e8-8982-06dcd8398609	t
152	365	Graft-De Rijp	\N	151a0d57-a7ce-4e70-8448-463ffe4c0690	t
153	366	Anna Paulowna	\N	f757570f-3ab0-4b78-bb1e-53e58dae3d60	t
154	370	Beemster	\N	6c465ac6-55bd-4b9d-93f7-36a9e5e578de	f
155	373	Bergen (NH.)	\N	846067e9-05b5-4136-a8cf-2b2c8a0163a9	f
156	375	Beverwijk	\N	78446ef9-8079-4c4c-83db-8a7cd872fcf5	f
157	376	Blaricum	\N	723ed6d4-2f1b-4a42-bc73-67ccbb1ecce6	f
158	377	Bloemendaal	\N	c11989a3-e8ef-4472-9376-7d08e680234e	f
159	381	Bussum	\N	64c95133-d47b-47d0-b44a-645f01437331	t
160	383	Castricum	\N	c56c2510-6865-40b7-bfff-fe592a538f36	f
161	384	Diemen	\N	a9e2bf1a-2421-4c2d-b6cf-88903154851d	f
162	385	Edam-Volendam	\N	08eb5815-d023-4733-84dc-0111c2ab42cb	f
163	388	Enkhuizen	\N	e2eb572e-0cdd-47fc-8a66-34ca76157b41	f
164	392	Haarlem	\N	c834a1a8-6d2a-46f1-be2a-988eb80ca8ea	f
165	393	Haarlemmerliede en Spaarnwoude	\N	b51e3906-2c7f-4e10-9487-db2e6fb1e3b2	t
166	394	Haarlemmermeer	\N	68d82b04-5159-4f81-a5be-2083475e0cc4	f
167	395	Harenkarspel	\N	d97336c4-a49f-4183-894b-da5edfd60c91	t
168	396	Heemskerk	\N	dfccd608-bc72-4b43-8d6f-dfe53c5fee81	f
169	397	Heemstede	\N	25441e7f-b63b-43ea-aa2c-8157c1edbb3a	f
170	398	Heerhugowaard	\N	f731c7d8-ff0d-4551-8851-ada4399afc8c	f
171	399	Heiloo	\N	587e1440-1c83-43d5-8107-71fa1ceacc22	f
172	400	Den Helder	\N	29a8d264-009c-443f-b431-d74c12a08fc9	f
173	402	Hilversum	\N	0219a31e-b399-42a7-ba4f-7734da2edb05	f
174	405	Hoorn	\N	e87c513b-0ed2-492c-afd2-e3407a117424	f
175	406	Huizen	\N	74384372-e49d-4459-99a6-830a7a5776c0	f
176	412	Niedorp	\N	25b6f43c-1685-406c-be0b-bd06cee583c5	t
177	415	Landsmeer	\N	d091eb7e-b1e9-464f-8633-c4fa5e1032a2	f
178	416	Langedijk	\N	a7038427-2adb-4f04-9c52-99c353197b27	f
179	417	Laren	\N	8e9c9202-84cc-45f0-8eb3-11577152cdd6	f
180	420	Medemblik	\N	a75de19b-fb17-4d9c-9e30-7425bc4335f0	f
181	424	Muiden	\N	c7c3f468-1296-4721-9d70-ac64d35188c3	t
182	425	Naarden	\N	ff14e4dd-610e-4c37-8199-28a0772e4f79	t
183	431	Oostzaan	\N	afbd1ec8-4087-47ca-bc97-84aef54a7a13	f
184	432	Opmeer	\N	f42e388e-dfcf-4de6-bc4f-094f67f6d734	f
185	437	Ouder-Amstel	\N	e379661e-0c9b-45e6-aefa-52d334a25b75	f
186	439	Purmerend	\N	8aef286a-7015-46ab-a7e7-db96435698c5	f
187	441	Schagen	\N	e8c4aaf9-d2f1-40f5-948b-72d9eca1d905	f
188	448	Texel	\N	a837b704-0913-4fb0-847f-ec998f362b12	f
189	450	Uitgeest	\N	233ca9e5-e17e-46fd-bbe3-6dce90592616	f
190	451	Uithoorn	\N	cc3a5dc6-c937-4c74-a0e0-387842fdc6e6	f
191	453	Velsen	\N	9070e114-e08b-417c-885b-8b2599f32436	f
192	457	Weesp	\N	6750f363-0962-4d08-9486-fb813aa458fc	f
193	458	Schermer	\N	180ea3e0-4986-46ba-bafc-507e999c799c	t
194	459	Wervershoof	\N	955f8f21-aad8-42ff-8891-f8423f5042a3	t
195	462	Wieringen	\N	aeb5767e-659b-4160-b07d-d33f3e214355	t
196	463	Wieringermeer	\N	a3f4be3f-5622-4e10-b150-798ee0b5391a	t
197	473	Zandvoort	\N	abfdeaf0-9b7a-40f4-9403-5502aed0565e	f
198	476	Zijpe	\N	ce0e7fde-db94-401d-be43-42cc2a41bc3f	t
199	478	Zeevang	\N	d3c769cd-27f7-46d9-8abf-58107f93b1f2	t
200	479	Zaanstad	\N	2e4c23f0-3328-4214-930f-f2ac66db911a	f
201	482	Alblasserdam	\N	6ed63920-6a9f-46b4-bafd-0925480ac923	f
202	484	Alphen aan den Rijn	\N	a46ef81d-38b8-459d-902e-57187034441b	f
203	489	Barendrecht	\N	9a4cef81-e701-4587-9c54-bd5e4387a68e	f
204	491	Bergambacht	\N	f2e89f65-8a50-4587-a7b4-c62464e48d41	t
205	497	Bodegraven	\N	8fe05876-a042-4973-b6f1-9ef7d1541cd6	t
206	498	Drechterland	\N	9fe8c6a0-2f2c-4526-b59b-1d7985024f61	f
207	499	Boskoop	\N	fb4dcf45-9279-4060-b34b-49d1cb21650d	t
208	501	Brielle	\N	7421af3c-880f-4418-8571-d3f39cb93c97	f
209	502	Capelle aan den IJssel	\N	ce03f8d7-8c60-48a8-a67a-c278916e576e	f
210	503	Delft	\N	c2def0fb-f825-481a-8a02-086d9adda546	f
211	504	Dirksland	\N	ba6769eb-8379-4e4f-8210-ae08aa2628ec	t
212	505	Dordrecht	\N	6e06cf5b-14ad-4915-bb2b-924affc13685	f
213	511	Goedereede	\N	2f8eae40-a9f6-4cd5-8956-6a31db2a238a	t
214	512	Gorinchem	\N	cafd931a-5200-44b2-9223-8f53908e80bb	f
215	513	Gouda	\N	cd5085d8-f50a-41f4-ad8e-65ea86762a39	f
216	518	's-Gravenhage	Den Haag	0c48f9ea-a528-4c61-a76e-306653334565	f
217	523	Hardinxveld-Giessendam	\N	8078a749-57bf-4f7d-9753-b60b2c80f981	f
218	530	Hellevoetsluis	\N	c6045df2-b3a7-4fef-a40f-70b7330b4939	f
219	531	Hendrik-Ido-Ambacht	\N	ca5ccf42-70a7-45cc-9ab1-49cd9abcea60	t
220	532	Stede Broec	\N	62f93422-b2dc-46de-bab0-2556e6ca7188	f
221	534	Hillegom	\N	3a91ff81-220f-4d46-97e1-996fac7e9c16	f
222	537	Katwijk	\N	65163c78-06f2-4fea-a3a5-38e6db2f6ff3	f
223	542	Krimpen aan den IJssel	\N	6b05ef47-2b79-4d81-a0e1-c7a204957db0	f
224	545	Leerdam	\N	77f858eb-a6ca-4e15-b585-aa1655259b41	t
225	546	Leiden	\N	b3b98f00-b132-4848-8ff9-20f24957f3e6	f
226	547	Leiderdorp	\N	5f49a31d-0682-4dc1-aecd-d8e64e4da3ef	f
227	553	Lisse	\N	fdd42de4-e896-483d-b52e-d1297633b409	f
228	556	Maassluis	\N	25cd9bf1-1f9a-4a9b-822d-b4f8f10de74d	f
229	559	Middelharnis	\N	8fc7028b-c1e2-4cda-a01f-c3784b593010	t
230	563	Moordrecht	\N	ecd5191b-be80-4a93-944a-832c4ce3856f	t
231	567	Nieuwerkerk aan den IJssel	\N	4063ede9-fa7c-4cc4-8bae-3d443f7e86be	t
232	568	Bernisse	\N	d4cef9f9-7641-434c-865f-2041405b6177	t
233	569	Nieuwkoop	\N	b3602387-443b-488e-921d-33920d7f2897	f
234	571	Nieuw-Lekkerland	\N	3c68346f-7baf-480e-ae03-52ed9a824490	t
235	575	Noordwijk	\N	9151ab4b-669b-4cca-9334-8b8fc10e8998	f
236	576	Noordwijkerhout	\N	65476ede-5654-43d7-b99f-dc9812b953d4	t
237	579	Oegstgeest	\N	4fe3497d-de82-4d1b-b715-1e62cb9d63c4	f
238	580	Oostflakkee	\N	fb52517f-c21c-48f3-8031-9279ade94a79	t
239	584	Oud-Beijerland	\N	007183f4-b7e8-49f0-a9e1-93c655d316d8	t
240	585	Binnenmaas	\N	fdb7b845-7d52-41b3-bde3-31c34ed6ff32	t
241	588	Korendijk	\N	80209b34-9ba6-4b72-9e7a-e73cbe9fe211	t
242	589	Oudewater	\N	593d1050-9b22-4933-b153-5912454f8a10	f
243	590	Papendrecht	\N	f8996c38-6660-4ae6-b1b5-429f48b79159	f
244	595	Reeuwijk	\N	b261c8d3-42c8-47be-8c20-6129706c8c14	t
245	597	Ridderkerk	\N	77ad13d5-1e9e-491d-8fac-18e64ae0edd2	f
246	599	Rotterdam	\N	5cfba98e-69bf-4ac8-8c5f-194b935e7b0e	f
247	600	Rozenburg	\N	009852f0-9e3e-4579-8ee0-d87a9551c8ed	t
248	603	Rijswijk	\N	341fe6f2-f27a-4020-9bcb-e52858067909	f
249	606	Schiedam	\N	d7d4bcd1-1f81-4907-9cf1-78205e7c41ff	f
250	608	Schoonhoven	\N	e1e0cb83-f905-436b-b831-3f175af0697f	t
251	610	Sliedrecht	\N	8405e045-a005-4280-be9d-368816f7d705	f
252	611	Cromstrijen	\N	9bb51188-f5f5-43f4-afed-f301c49a1bd9	t
253	612	Spijkenisse	\N	8b16d64c-96c6-4987-a76b-23e2adc32694	t
254	613	Albrandswaard	\N	a9d775dc-9c30-4e67-80f9-c8bf3a37ecb6	f
255	614	Westvoorne	\N	415afef4-4f65-4ae1-8657-2706dcc6937f	f
256	617	Strijen	\N	96e8dfba-b748-48e7-8b16-250fc50714bf	t
257	620	Vianen	\N	d62ad226-1b51-4941-b2a1-152a67c9f42b	t
258	622	Vlaardingen	\N	3e9c2b5d-c84a-4686-b174-15474460b953	f
259	623	Vlist	\N	aa90d2bf-d7d1-450d-a561-ba5f3f6c7858	t
260	626	Voorschoten	\N	f5d9c20c-78b7-4f57-bb2f-aaa643b98cc5	f
261	627	Waddinxveen	\N	d5772984-eaec-40cf-8919-4010f593e434	f
262	629	Wassenaar	\N	802e4785-6e0b-4d1f-b48b-930b08257dfc	f
263	632	Woerden	\N	bfdfbcc2-7740-4924-9fa8-04b4dc171bf2	f
264	637	Zoetermeer	\N	148f3522-3b65-492f-99a4-08d986a5221b	f
265	638	Zoeterwoude	\N	98d966ab-afd0-4ee1-b173-1f574b06a10b	f
266	642	Zwijndrecht	\N	b9671f2c-231c-48da-ac4e-bb92b2457810	f
267	643	Nederlek	\N	a95c9a13-d2d1-48a3-877f-5a92e03fb8b0	t
268	644	Ouderkerk	\N	4a4b2cfe-8dea-4cdf-a48d-fb3a612586c1	t
269	653	Gaasterlân-Sleat	\N	6a951aae-0027-43f5-a211-cc4013592ed8	t
270	654	Borsele	\N	9824623a-f700-4319-aea5-c353655b23e3	f
271	664	Goes	\N	15592935-c639-4e53-b49f-e52bfd8fe3f4	f
272	668	West Maas en Waal	\N	d99bbefd-d989-4b60-8ae8-87a2fc718f44	f
273	677	Hulst	\N	3ade6e87-aab5-4418-8438-107cc160ce28	f
274	678	Kapelle	\N	3f74552d-e7b5-4ed0-9b4d-a255bdf2853e	f
275	683	Wymbritseradiel	\N	6488d30f-4db4-4bb9-83b4-50180bfec315	t
276	687	Middelburg	\N	80a55b1c-1dda-4565-b916-520d823cc64d	f
277	689	Giessenlanden	\N	87da38ba-75f3-41de-bac2-321bae23316e	t
278	693	Graafstroom	\N	cfc9b87c-4768-4a0c-bac1-c59eeb498248	t
279	694	Liesveld	\N	66b3556c-1787-410c-a6b2-f7aa6186b340	t
280	703	Reimerswaal	\N	d111d90b-c374-465e-b2ee-f314686b387b	f
281	707	Zederik	\N	d422524d-72c5-4b2b-afb1-e85e433c5911	t
282	710	Wûnseradiel	\N	819ed43e-e504-4647-9a52-c854ea3dd8b1	t
283	715	Terneuzen	\N	6d2448c2-65df-481b-b69c-c8a42c6cc53f	f
284	716	Tholen	\N	e8184c8e-fcc9-4630-b11e-b787640a29cd	f
285	717	Veere	\N	6187d65b-e09c-4756-8f01-9c915bbd5518	f
286	718	Vlissingen	\N	9bea9e08-79db-40ac-86bb-e5e538ca9094	f
287	733	Lingewaal	\N	57349338-26d8-4ec0-8243-26f1d7e1d422	t
288	736	De Ronde Venen	\N	d24f71e4-2955-4a2a-bd25-be21591c80dd	f
289	737	Tytsjerksteradiel	\N	eea75c09-0fe8-417a-b66a-d98b4b005a78	f
290	738	Aalburg	\N	a9505342-0efe-480a-8935-8466eeb7c5ef	t
291	743	Asten	\N	ef45785b-0e93-49b9-9b10-21f3078f27c3	f
292	744	Baarle-Nassau	\N	2df0fee1-d44b-4a48-9e53-bd236c8558a2	f
293	748	Bergen op Zoom	\N	5d884613-3e08-460a-9ebf-09315e890817	f
294	753	Best	\N	9589e10b-2864-48ad-8f02-6b8585c88832	f
295	755	Boekel	\N	f757909a-4090-4cc6-b654-c7bcaf6be2be	f
296	756	Boxmeer	\N	ae90e90a-87dc-4c2b-bd33-cd848a08c9c3	f
297	757	Boxtel	\N	fe1ffe66-7e82-4a27-bcce-7f5beaec1b13	f
298	758	Breda	\N	2c29dd15-db81-4866-8a44-fea415ceb76f	f
299	762	Deurne	\N	2d3d9a83-1736-4b50-a172-cf900a39bbab	f
300	765	Pekela	\N	d0290a9f-f269-4322-8a42-bbfae06b0536	f
301	766	Dongen	\N	50acfeb5-9f16-4de4-bf79-610b3570e2a5	f
302	770	Eersel	\N	569f660a-169d-4a05-b257-c5209a3f65dd	f
303	1979	Eemsdelta	\N	2e694b7e-cc66-48d6-bdb0-4c7e5a33c7cb	f
304	772	Eindhoven	\N	a3efff87-36fb-4845-aeb4-a4eb81bf2158	f
305	777	Etten-Leur	\N	1655e5ac-9eb2-4f7e-8dce-60d372bf16e4	f
306	779	Geertruidenberg	\N	dcd3a5d5-4aab-440f-b303-920fe885e4ff	f
307	784	Gilze en Rijen	\N	23ea6063-ef7f-4aa7-acfd-9f9706115a06	f
308	785	Goirle	\N	8bf37214-8fa4-40f2-874e-2a18cacf989d	f
309	786	Grave	\N	fa6d7583-f21a-401d-980a-85c460b5f4ee	f
310	788	Haaren	\N	11208a44-9d46-4761-b721-a22b869c12d3	t
311	794	Helmond	\N	6f611717-9d9e-442a-8ab6-53c410134c3f	f
312	796	's-Hertogenbosch	Den Bosch	51f2d486-a443-4e55-b43a-09dc8f305633	f
313	797	Heusden	\N	0fb684b2-b729-4fdf-9fda-b8fc2c0abc22	f
314	798	Hilvarenbeek	\N	df1f0d37-4cac-4e3e-96b9-6570e5efbf96	f
315	808	Lith	\N	47beedaf-8132-48af-83f3-2f8301afdb1e	t
316	809	Loon op Zand	\N	04fe984f-9629-400e-8e23-05f25f9e5402	f
317	815	Mill en Sint Hubert	\N	790e66b5-8a8d-4934-b1ac-f3a8b48bf40c	f
318	820	Nuenen, Gerwen en Nederwetten	\N	4ddf4ac1-c6d3-477b-ae3d-7262a397c9a5	f
319	823	Oirschot	\N	31ba5cd6-53cd-45c4-8dd6-e7bb1e8286a9	f
320	824	Oisterwijk	\N	7bdb09b4-4f8f-4043-a9cf-05cab828ba96	f
321	826	Oosterhout	\N	1165f0c4-c7fd-4627-9308-5c4fc55d0170	f
322	828	Oss	\N	baf3a2d0-d78a-49dc-9b85-d4d7230b53ef	f
323	840	Rucphen	\N	e37ecc95-70c4-4a78-bb91-1605c689b19b	f
324	844	Schijndel	\N	e519e259-2200-49ac-8a7b-b8c7264b6d01	t
325	845	Sint-Michielsgestel	\N	bb896dee-15cb-4f5a-a15b-442b866a726e	f
326	846	Sint-Oedenrode	\N	59b5a32f-2495-4fee-aa68-fc9fa4602c0c	t
327	847	Someren	\N	2b767bf2-04cb-4894-9d5e-e8a43020e36c	f
328	848	Son en Breugel	\N	8e6ebdf3-8525-4851-b90b-605be3be4959	f
329	851	Steenbergen	\N	7fdf92ad-1bed-43c6-a0f4-1aa987324132	f
330	852	Waterland	\N	d0789349-ee66-4608-8a5b-dfa47dd20ca3	f
331	855	Tilburg	\N	b46f50a9-b3a2-4864-8b62-e857fa826f60	f
332	856	Uden	\N	5f647cf2-85fa-4fb7-8f6c-a090784d8555	f
333	858	Valkenswaard	\N	80bd5746-4499-4665-a9f0-a4c56318e825	f
334	860	Veghel	\N	6ba49c47-10cf-4737-837e-521913be92d3	t
335	861	Veldhoven	\N	c3503a95-8be2-4dae-bc14-277b212c5d87	f
336	865	Vught	\N	d0684e7d-c051-4f90-ad63-e288a826160a	f
337	866	Waalre	\N	8515998e-a5bc-4faa-bf42-215ca6c1ceec	f
338	867	Waalwijk	\N	4040f3dc-0b45-4025-a3a2-b5678f48a7b9	f
339	870	Werkendam	\N	e8a32fd6-686c-4461-85e2-94672f25c2e5	t
340	873	Woensdrecht	\N	3dae1ad0-366a-4960-b951-f2077d9222a9	f
341	874	Woudrichem	\N	5a0cea87-394e-41a5-8d52-0cecba1f787d	t
342	879	Zundert	\N	bc5fd894-0e45-442a-a331-fff56ae5b727	f
343	880	Wormerland	\N	b7023a50-19ea-46ca-a198-9cb29b104d5a	f
344	881	Onderbanken	\N	0b9a1010-ebdb-4a88-a912-aae361fc1c31	t
345	882	Landgraaf	\N	0a385dbc-e22a-4c1b-9d51-ffdb28325c5a	f
346	885	Arcen en Velden	\N	f1563a97-c35f-4f99-ac5d-edd7d9152dc5	f
347	888	Beek	\N	f92adb54-e7f2-459a-95b3-a07f40deda2f	f
348	889	Beesel	\N	87b8bee2-3d21-412c-965c-684beb8dcb8f	f
349	893	Bergen (L.)	\N	b0b96a6d-28bc-432e-aea8-c4ba209b8432	f
350	899	Brunssum	\N	7052ca24-ca2f-4ff3-a899-b246080e6401	f
351	905	Eijsden	\N	ca65e142-fb41-4a45-9358-aa6df86581b2	t
352	907	Gennep	\N	4a42bcf5-cebe-4c7f-948b-e22d7ec48545	f
353	917	Heerlen	\N	385394d9-f003-472f-aa68-668336e5cb1b	f
354	918	Helden	\N	b68d92bf-06d9-47e6-a222-85e4b1cc503e	t
355	928	Kerkrade	\N	7adc4ec3-b3b4-4138-9aaf-e1984157be90	f
356	929	Kessel	\N	da4a8d06-f9cb-4409-aa19-ecfc5493c620	t
357	934	Maasbree	\N	eb511d2f-b386-4298-90cf-0ddf11df24d3	t
358	935	Maastricht	\N	c53f2ddf-c2e5-4b0a-ba8f-73efdce14836	f
359	936	Margraten	\N	6636f9de-1a82-48a4-984b-011cf807fdbb	t
360	938	Meerssen	\N	9e58ff19-7145-4b8e-85ed-185e7161e4c9	f
361	941	Meijel	\N	5f80bbf2-6a2c-4bff-b084-c8e22be843b4	t
362	944	Mook en Middelaar	\N	709b3ad6-18c5-4ad6-9073-207183b49532	f
363	946	Nederweert	\N	f26a2ba9-64a4-4b35-80c2-1a8f0cc2e325	f
364	951	Nuth	\N	5b260f6a-1ca3-480a-98ae-60cb63b2031f	t
365	957	Roermond	\N	6b61d113-7b5c-46f5-9f9b-3027883c9fec	f
366	962	Schinnen	\N	a22bfd75-ad84-425c-b0a4-afe83ccbf036	t
367	964	Sevenum	\N	574e31de-3bec-474b-b4fa-e16e36beae45	t
368	965	Simpelveld	\N	02a718c6-ba48-425a-a401-e0141c2da6c9	f
369	971	Stein	\N	1259fc3d-dd12-4cac-aa59-a3c59b176db8	f
370	981	Vaals	\N	066b779b-567f-4f80-bd7a-e69d06cbdbcf	f
371	983	Venlo	\N	a366c12e-4d87-49c4-8f77-b97dd6a70fe4	f
372	984	Venray	\N	23022112-49e0-4750-9047-d23f973256c1	f
373	986	Voerendaal	\N	d09dc573-04ac-4b80-8961-03a060f670b3	f
374	988	Weert	\N	3b493f4a-abfe-4f71-b279-f26a8eda791d	f
375	993	Meerlo-Wanssum	\N	bebbd6d2-c29b-476f-8073-c55c1d4aa4f4	t
376	994	Valkenburg aan de Geul	\N	a6d3fa57-d49a-4f90-b847-513c7a07191c	f
377	995	Lelystad	\N	872c0f65-25a4-4a45-98b7-2bd6ca69e3e5	f
378	1507	Horst aan de Maas	\N	91a02378-7cee-42eb-9ff8-8e5241f0c4b8	f
379	1509	Oude IJsselstreek	\N	e4e8df47-8f4b-4143-a278-142d0d62fa86	f
380	1525	Teylingen	\N	8d9ada86-f4c2-4ff6-8cb3-74cc46a0e98d	f
381	1581	Utrechtse Heuvelrug	\N	749c54df-d2a3-4850-ae01-22f35c0fd249	f
382	1586	Oost Gelre	\N	c686fbf6-c777-44f7-b751-9ca0e6cb1589	f
383	1598	Koggenland	\N	d2eb46d7-73f4-417e-959a-85c1becb6550	f
384	1621	Lansingerland	\N	75a8bc60-4847-4071-bad7-6078f4b1e801	f
385	1640	Leudal	\N	49a3c925-960c-4921-8817-96f4292e35b8	f
386	1641	Maasgouw	\N	b84d85b5-92c6-4f3a-858c-aaf79ccf2416	f
387	1651	Eemsmond	\N	a52b5eaf-4210-40c1-a098-6457e6cbb1d8	t
388	1652	Gemert-Bakel	\N	ea91138b-681c-4cd9-9d7c-84c9d5c9c652	f
389	1655	Halderberge	\N	dad265ec-8beb-4b5f-b25a-17fe7a04c9f3	f
390	1658	Heeze-Leende	\N	8bdff903-ffba-438e-82e3-337feb22d183	f
391	1659	Laarbeek	\N	78944c52-becd-4615-be71-0df792c77374	f
392	1661	Reiderland	\N	12148dba-0c0b-4ef7-8b5d-9f20989f26a5	t
393	1663	De Marne	\N	e1483840-82a8-4dda-a66b-2a696f171181	t
394	1666	Zevenhuizen-Moerkapelle	\N	42039b7e-b468-440f-9869-0704d8f72cc4	t
395	1667	Reusel-De Mierden	\N	59bb51a2-7eb7-4e8f-8aa0-15e9099811c9	f
396	1669	Roerdalen	\N	831d14b0-76b5-41d7-958b-97e2509b41dd	f
397	1671	Maasdonk	\N	e4bf5a34-9a61-4cc2-af9b-ba4f471e8cf4	t
398	1672	Rijnwoude	\N	2d3127ff-6f89-4db5-8fed-eb681b588010	t
399	1674	Roosendaal	\N	a5a02f86-fb0e-4ede-ba7a-a7f2df13d56c	f
400	1676	Schouwen-Duiveland	\N	4ddb6441-4461-4b56-87e8-720f696c171d	f
401	1680	Aa en Hunze	\N	b5f083d4-c9d9-4f18-a165-51bed73eb5b5	f
402	1681	Borger-Odoorn	\N	f75be851-74c7-4ded-975d-7ab4e795d7f8	f
403	1684	Cuijk	\N	8d2773f4-a3f3-424b-9789-54e0282abbfd	f
404	1685	Landerd	\N	ce61e258-b319-4cf6-b892-8663a1870024	f
405	1690	De Wolden	\N	a8b1c1e0-5782-4dbd-9a46-6f55c0b6a809	f
406	1695	Noord-Beveland	\N	70dc9728-7fd6-4f21-a00b-684da3990a40	f
407	1696	Wijdemeren	\N	ae9fda03-87df-43d4-9fbb-8cce5341eaba	f
408	1699	Noordenveld	\N	2fcaf178-f421-4ce6-a5ba-f541c55c2a7c	f
409	1700	Twenterand	\N	012197bc-9dce-4d1b-a932-e7e24fc9dd58	f
410	1701	Westerveld	\N	b8568581-018b-48a1-9c30-871aa3b65cd0	f
411	1702	Sint Anthonis	\N	bb53f60b-5ffa-4f10-b30b-e63ad02d82aa	f
412	1705	Lingewaard	\N	b5fc725f-7147-4324-b846-f7ade6ff0c60	f
413	1706	Cranendonck	\N	42bd3c3d-055e-4487-8802-8d5cb8722cac	f
414	1708	Steenwijkerland	\N	b65b4285-c898-45f4-9cd8-b0e85f5100df	f
415	1709	Moerdijk	\N	0167c404-ab3a-4239-97b5-5dd772098345	f
416	1711	Echt-Susteren	\N	c1c9a6a6-56cb-4686-9715-71b9d37c8c38	f
417	1714	Sluis	\N	50b9ca62-ba02-47ad-b2d2-1fde068aaa32	f
418	1719	Drimmelen	\N	d4861114-e193-4a95-9d1b-028cd1540e9e	f
419	1721	Bernheze	\N	44158269-694d-4145-aca3-011ecd5bcc96	f
420	1722	Ferwerderadiel	\N	edff92ed-e242-4544-b99b-6906147a9fa9	t
421	1723	Alphen-Chaam	\N	977708a2-d798-4974-b7dc-900a82d87f2c	f
422	1724	Bergeijk	\N	c4dedfb1-c3c7-4427-a024-4def2091fd2d	f
423	1728	Bladel	\N	bd0831f5-bcd5-46ef-befc-7242e12753d6	f
424	1729	Gulpen-Wittem	\N	22d2ad45-c29a-455c-908b-c83ba9db8310	f
425	1730	Tynaarlo	\N	302ef79e-41aa-406c-ab13-73712154a6fa	f
426	1731	Midden-Drenthe	\N	8a89e60c-d874-458f-bd75-1175105ee5c8	f
427	1734	Overbetuwe	\N	debbe4de-3b3c-4c00-aa24-2963b0eb3cf3	f
428	1735	Hof van Twente	\N	4f91209c-5974-47cb-88a5-4d93137604cc	f
429	1740	Neder-Betuwe	\N	4fea7a0c-18af-4895-b2c7-d195a553b41d	f
430	1742	Rijssen-Holten	\N	1e8efe2e-953b-427d-ae98-c5a8d58e9391	f
431	1771	Geldrop-Mierlo	\N	c96ef787-2263-4671-957e-586cf3db3db3	f
432	1773	Olst-Wijhe	\N	c7de1e8d-9eef-49c0-a1a3-5cd955fc5a8f	f
433	1774	Dinkelland	\N	e5b51d51-b69e-4308-92b7-a8306813426a	f
434	1783	Westland	\N	eb62c259-8700-4ed6-9d4c-53f452ad7a94	f
435	1842	Midden-Delfland	\N	ef958472-5820-4fab-b56b-9e04abae240b	f
436	1859	Berkelland	\N	cc3f1846-e3be-4ecc-ac85-efc7fdceb1f3	f
437	1876	Bronckhorst	\N	ef471ee9-7b22-4271-b148-1c52b4630ea8	f
438	1883	Sittard-Geleen	\N	49f6715a-49cd-48c0-b3b6-73f2e9cbe327	f
439	1884	Kaag en Braassem	\N	dc331066-029a-4a64-be15-d513bd136876	f
440	1891	Dantumadiel	\N	f04e2d6f-e181-485d-af97-08edecca6b4d	f
441	1892	Zuidplas	\N	95e5f9a3-550c-4657-902c-f2f9f1fe6211	f
442	1894	Peel en Maas	\N	de18c0b4-96bf-4abf-9fcc-a9358b0fc902	f
443	1895	Oldambt	\N	380eedfc-59f0-4c37-b7e0-021b559ab0ae	f
444	1896	Zwartewaterland	\N	8506c310-b71b-45ed-be2f-48524593afb6	f
445	1900	Súdwest-Fryslân	\N	7cfdec38-b8cb-405f-9053-318fbce5ec12	f
446	1901	Bodegraven-Reeuwijk	\N	7fcc65f2-d98f-40e2-9d67-773abfcf9c12	f
447	1903	Eijsden-Margraten	\N	56e7366d-8009-41cb-84d2-bf4bec7b988a	f
448	1904	Stichtse Vecht	\N	a9e96779-5c02-497a-9d34-98e8df2939c4	f
449	1908	Menameradiel	\N	e2934162-dd43-4294-80f7-6ac0e2c220c2	t
450	1911	Hollands Kroon	\N	57820002-5119-4bef-ab79-185fd022fad2	f
451	1916	Leidschendam-Voorburg	\N	1ae14241-893f-4182-bbfa-7796f1011fca	f
452	1924	Goeree-Overflakkee	\N	6dc42793-60e2-43ac-bcdc-1f737c2087b9	f
453	1926	Pijnacker-Nootdorp	\N	27469b04-c6f4-466e-a1ee-00982788e741	f
454	1927	Molenwaard	\N	7f19a8ad-f86c-4752-9df7-e545b354781a	t
455	1930	Nissewaard	\N	3a7e8a5b-dad1-465a-9a1c-1be6a87572a5	f
456	1931	Krimpenerwaard	\N	06547322-c619-4d6e-8c3c-5bb41dd75107	f
457	1940	De Fryske Marren	\N	3ac6da20-2b5f-4698-b63a-811044478250	f
458	1942	Gooise Meren	\N	3726385e-6a5b-49de-ac6e-e2692e88c2f5	f
459	1945	Berg en Dal	\N	e857f0f5-c464-45e3-b448-4bb088b2f22d	f
460	1948	Meierijstad	\N	3a985cae-26c4-45d1-98ec-6910ebb310b4	f
461	1949	Waadhoeke	\N	54f983ea-c843-4491-a10e-e4f1de353271	f
462	1950	Westerwolde	\N	06ad4cd3-b422-44ab-8142-4f8d5beeadf8	f
463	1952	Midden-Groningen	\N	d2535d71-2571-4279-b06c-1046a7b9c0e8	f
464	1954	Beekdaelen	\N	0a10191e-7cb4-4953-b03d-01511cc56303	f
465	1955	Montferland	\N	f25fe4c5-556f-443f-97ab-a541743367b6	f
466	1959	Altena	\N	8a193685-0ac1-4e7c-a3c6-b75973a0db2c	f
467	1960	West Betuwe	\N	77282143-19cd-4aaa-9304-97b16abb7ff2	f
468	1961	Vijfheerenlanden	\N	3e9a0754-cb85-418a-80b0-9ca9c2e7980e	f
469	1963	Hoeksche Waard	\N	8f6bf683-9da8-4bd0-b645-386617752e9f	f
470	1966	Het Hogeland	\N	f2099bb7-49de-4c32-8e22-0020c95500b0	f
471	1969	Westerkwartier	\N	c6f02ed0-1085-4704-a482-1fec6629b035	f
472	1970	Noardeast-Fryslân	\N	5a8d21fd-260a-4899-a6f8-5e47e4bb2f3b	f
473	1978	Molenlanden	\N	ac67a8a2-a4d8-4b56-866c-8cc40826cbcb	f
474	1987	Menterwolde	\N	e055230a-8a06-4e7c-9f58-0594b3d38fe1	t
\.


--
-- Data for Name: natuurlijk_persoon; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.natuurlijk_persoon (search_index, search_term, object_type, searchable_id, search_order, id, burgerservicenummer, a_nummer, voorletters, voornamen, geslachtsnaam, voorvoegsel, geslachtsaanduiding, nationaliteitscode1, nationaliteitscode2, nationaliteitscode3, geboorteplaats, geboorteland, geboortedatum, aanhef_aanschrijving, voorletters_aanschrijving, voornamen_aanschrijving, naam_aanschrijving, voorvoegsel_aanschrijving, burgerlijke_staat, indicatie_geheim, land_waarnaar_vertrokken, import_datum, adres_id, authenticated, authenticatedby, deleted_on, verblijfsobject_id, datum_overlijden, aanduiding_naamgebruik, onderzoek_persoon, onderzoek_huwelijk, onderzoek_overlijden, onderzoek_verblijfplaats, partner_a_nummer, partner_burgerservicenummer, partner_voorvoegsel, partner_geslachtsnaam, datum_huwelijk, datum_huwelijk_ontbinding, in_gemeente, landcode, naamgebruik, uuid, active, adellijke_titel, preferred_contact_channel, pending, related_custom_object_id, gemeentecode) FROM stdin;
\.


--
-- Data for Name: object_acl_entry; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.object_acl_entry (uuid, object_uuid, entity_type, entity_id, capability, scope, groupname) FROM stdin;
\.


--
-- Data for Name: object_bibliotheek_entry; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.object_bibliotheek_entry (search_index, search_term, object_type, searchable_id, search_order, id, bibliotheek_categorie_id, object_uuid, name) FROM stdin;
\.


--
-- Data for Name: object_data; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.object_data (uuid, object_id, object_class, properties, index_hstore, date_created, date_modified, text_vector, class_uuid, acl_groupname, invalid) FROM stdin;
\.


--
-- Data for Name: object_mutation; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.object_mutation (id, object_uuid, object_type, lock_object_uuid, type, "values", date_created, subject_id, executed) FROM stdin;
\.


--
-- Data for Name: object_relation; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.object_relation (id, name, object_type, object_uuid, object_embedding, object_id, object_preview) FROM stdin;
\.


--
-- Data for Name: object_relationships; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.object_relationships (uuid, object1_uuid, object2_uuid, type1, type2, object1_type, object2_type, blocks_deletion, title1, title2, owner_object_uuid) FROM stdin;
\.


--
-- Data for Name: object_subscription; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.object_subscription (id, interface_id, external_id, local_table, local_id, date_created, date_deleted, object_preview, config_interface_id) FROM stdin;
\.


--
-- Data for Name: parkeergebied; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.parkeergebied (id, bag_hoofdadres, postcode, straatnaam, huisnummer, huisletter, huisnummertoevoeging, parkeergebied_id, parkeergebied, created, last_modified, woonplaats) FROM stdin;
\.


--
-- Data for Name: queue; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.queue (id, object_id, status, type, label, data, date_created, date_started, date_finished, parent_id, priority, metadata) FROM stdin;
\.


--
-- Data for Name: queue_20210101; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.queue_20210101 (id, object_id, status, type, label, data, date_created, date_started, date_finished, parent_id, priority, metadata) FROM stdin;
\.


--
-- Data for Name: queue_20210201; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.queue_20210201 (id, object_id, status, type, label, data, date_created, date_started, date_finished, parent_id, priority, metadata) FROM stdin;
\.


--
-- Data for Name: queue_20210301; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.queue_20210301 (id, object_id, status, type, label, data, date_created, date_started, date_finished, parent_id, priority, metadata) FROM stdin;
\.


--
-- Data for Name: queue_20210401; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.queue_20210401 (id, object_id, status, type, label, data, date_created, date_started, date_finished, parent_id, priority, metadata) FROM stdin;
\.


--
-- Data for Name: queue_20210501; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.queue_20210501 (id, object_id, status, type, label, data, date_created, date_started, date_finished, parent_id, priority, metadata) FROM stdin;
\.


--
-- Data for Name: queue_20210601; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.queue_20210601 (id, object_id, status, type, label, data, date_created, date_started, date_finished, parent_id, priority, metadata) FROM stdin;
\.


--
-- Data for Name: queue_20210701; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.queue_20210701 (id, object_id, status, type, label, data, date_created, date_started, date_finished, parent_id, priority, metadata) FROM stdin;
\.


--
-- Data for Name: queue_20210801; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.queue_20210801 (id, object_id, status, type, label, data, date_created, date_started, date_finished, parent_id, priority, metadata) FROM stdin;
\.


--
-- Data for Name: queue_20210901; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.queue_20210901 (id, object_id, status, type, label, data, date_created, date_started, date_finished, parent_id, priority, metadata) FROM stdin;
\.


--
-- Data for Name: queue_20211001; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.queue_20211001 (id, object_id, status, type, label, data, date_created, date_started, date_finished, parent_id, priority, metadata) FROM stdin;
\.


--
-- Data for Name: queue_20211101; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.queue_20211101 (id, object_id, status, type, label, data, date_created, date_started, date_finished, parent_id, priority, metadata) FROM stdin;
\.


--
-- Data for Name: queue_20211201; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.queue_20211201 (id, object_id, status, type, label, data, date_created, date_started, date_finished, parent_id, priority, metadata) FROM stdin;
\.


--
-- Data for Name: queue_20xx; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.queue_20xx (id, object_id, status, type, label, data, date_created, date_started, date_finished, parent_id, priority, metadata) FROM stdin;
\.


--
-- Data for Name: remote_api_keys; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.remote_api_keys (id, key, permissions) FROM stdin;
\.


--
-- Data for Name: result_preservation_terms; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.result_preservation_terms (id, code, label, unit, unit_amount) FROM stdin;
1	28	4 weken	week	4
2	42	6 weken	week	6
3	93	3 maanden	month	3
4	186	6 maanden	month	6
5	279	9 maanden	month	9
6	365	1 jaar	year	1
7	548	1,5 jaar	year	1.5
8	730	2 jaar	year	2
9	1095	3 jaar	year	3
10	1460	4 jaar	year	4
11	1825	5 jaar	year	5
12	2190	6 jaar	year	6
13	2555	7 jaar	year	7
14	2920	8 jaar	year	8
15	3285	9 jaar	year	9
16	3650	10 jaar	year	10
17	4015	11 jaar	year	11
18	4380	12 jaar	year	12
19	4745	13 jaar	year	13
20	5110	14 jaar	year	14
21	5475	15 jaar	year	15
22	5840	16 jaar	year	16
23	6935	19 jaar	year	19
24	7300	20 jaar	year	20
25	7665	21 jaar	year	21
26	9125	25 jaar	year	25
27	10950	30 jaar	year	30
28	14600	40 jaar	year	40
29	18250	50 jaar	year	50
30	40150	110 jaar	year	110
31	99999	Bewaren	year	NaN
\.


--
-- Data for Name: rights; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.rights (name, description) FROM stdin;
user	Be a user in the system
dashboard	Be able to view a dashboard
case_read	Be able to read cases
cases_read_own	Be able to view cases that are created by oneself
case_add	Be able to add cases
case_edit	Be able to edit cases
case_manage	Be able to manage cases
view_sensitive_contact_data	Be able to view GDPR datas
\.


--
-- Data for Name: role_rights; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.role_rights (rights_name, role_id) FROM stdin;
\.


--
-- Data for Name: roles; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.roles (id, parent_group_id, name, description, system_role, date_created, date_modified, uuid, v1_json) FROM stdin;
1	1	Administrator	Systeemrol: Administrator	t	2021-08-20 11:10:37.423386	2021-08-20 11:10:37.423386	e63eea98-1862-401d-bdfa-643f91102688	{"type": "role", "preview": "Administrator", "instance": {"name": "Administrator", "role_id": 1, "description": "Systeemrol: Administrator", "system_role": true, "date_created": "2021-08-20T11:10:37Z", "date_modified": "2021-08-20T11:10:37Z"}, "reference": "e63eea98-1862-401d-bdfa-643f91102688"}
2	1	Zaaksysteembeheerder	Systeemrol: Zaaksysteembeheerder	t	2021-08-20 11:10:37.423386	2021-08-20 11:10:37.423386	0306be91-96ac-4d4b-9d70-3d8f23e9495b	{"type": "role", "preview": "Zaaksysteembeheerder", "instance": {"name": "Zaaksysteembeheerder", "role_id": 2, "description": "Systeemrol: Zaaksysteembeheerder", "system_role": true, "date_created": "2021-08-20T11:10:37Z", "date_modified": "2021-08-20T11:10:37Z"}, "reference": "0306be91-96ac-4d4b-9d70-3d8f23e9495b"}
3	1	Zaaktypebeheerder	Systeemrol: Zaaktypebeheerder	t	2021-08-20 11:10:37.423386	2021-08-20 11:10:37.423386	059760ff-ef7b-449d-9ac3-07d28991f4df	{"type": "role", "preview": "Zaaktypebeheerder", "instance": {"name": "Zaaktypebeheerder", "role_id": 3, "description": "Systeemrol: Zaaktypebeheerder", "system_role": true, "date_created": "2021-08-20T11:10:37Z", "date_modified": "2021-08-20T11:10:37Z"}, "reference": "059760ff-ef7b-449d-9ac3-07d28991f4df"}
4	1	Zaakbeheerder	Systeemrol: Zaakbeheerder	t	2021-08-20 11:10:37.423386	2021-08-20 11:10:37.423386	5deaebfd-a07f-4586-aad8-0d62df2faba4	{"type": "role", "preview": "Zaakbeheerder", "instance": {"name": "Zaakbeheerder", "role_id": 4, "description": "Systeemrol: Zaakbeheerder", "system_role": true, "date_created": "2021-08-20T11:10:37Z", "date_modified": "2021-08-20T11:10:37Z"}, "reference": "5deaebfd-a07f-4586-aad8-0d62df2faba4"}
5	1	Contactbeheerder	Systeemrol: Contactbeheerder	t	2021-08-20 11:10:37.423386	2021-08-20 11:10:37.423386	eca7f9c1-76f0-45b9-8a08-154a41fcb8fc	{"type": "role", "preview": "Contactbeheerder", "instance": {"name": "Contactbeheerder", "role_id": 5, "description": "Systeemrol: Contactbeheerder", "system_role": true, "date_created": "2021-08-20T11:10:37Z", "date_modified": "2021-08-20T11:10:37Z"}, "reference": "eca7f9c1-76f0-45b9-8a08-154a41fcb8fc"}
6	1	Basisregistratiebeheerder	Systeemrol: Basisregistratiebeheerder	t	2021-08-20 11:10:37.423386	2021-08-20 11:10:37.423386	bf0ada0e-b9f7-4d6b-83fd-2a750189072b	{"type": "role", "preview": "Basisregistratiebeheerder", "instance": {"name": "Basisregistratiebeheerder", "role_id": 6, "description": "Systeemrol: Basisregistratiebeheerder", "system_role": true, "date_created": "2021-08-20T11:10:37Z", "date_modified": "2021-08-20T11:10:37Z"}, "reference": "bf0ada0e-b9f7-4d6b-83fd-2a750189072b"}
7	1	Wethouder	Systeemrol: Wethouder	t	2021-08-20 11:10:37.423386	2021-08-20 11:10:37.423386	fccf4239-827f-42a9-9ac4-f4bc9ed4d6d4	{"type": "role", "preview": "Wethouder", "instance": {"name": "Wethouder", "role_id": 7, "description": "Systeemrol: Wethouder", "system_role": true, "date_created": "2021-08-20T11:10:37Z", "date_modified": "2021-08-20T11:10:37Z"}, "reference": "fccf4239-827f-42a9-9ac4-f4bc9ed4d6d4"}
8	1	Directielid	Systeemrol: Directielid	t	2021-08-20 11:10:37.423386	2021-08-20 11:10:37.423386	8efdac0e-2e5d-43c8-9965-42ec864137ab	{"type": "role", "preview": "Directielid", "instance": {"name": "Directielid", "role_id": 8, "description": "Systeemrol: Directielid", "system_role": true, "date_created": "2021-08-20T11:10:37Z", "date_modified": "2021-08-20T11:10:37Z"}, "reference": "8efdac0e-2e5d-43c8-9965-42ec864137ab"}
9	1	Afdelingshoofd	Systeemrol: Afdelingshoofd	t	2021-08-20 11:10:37.423386	2021-08-20 11:10:37.423386	79a38581-9648-469f-8ea3-d34a99f9e158	{"type": "role", "preview": "Afdelingshoofd", "instance": {"name": "Afdelingshoofd", "role_id": 9, "description": "Systeemrol: Afdelingshoofd", "system_role": true, "date_created": "2021-08-20T11:10:37Z", "date_modified": "2021-08-20T11:10:37Z"}, "reference": "79a38581-9648-469f-8ea3-d34a99f9e158"}
10	1	Kcc-medewerker	Systeemrol: Kcc-medewerker	t	2021-08-20 11:10:37.423386	2021-08-20 11:10:37.423386	b175e939-a5bb-41a7-8bda-d38bc84216ff	{"type": "role", "preview": "Kcc-medewerker", "instance": {"name": "Kcc-medewerker", "role_id": 10, "description": "Systeemrol: Kcc-medewerker", "system_role": true, "date_created": "2021-08-20T11:10:37Z", "date_modified": "2021-08-20T11:10:37Z"}, "reference": "b175e939-a5bb-41a7-8bda-d38bc84216ff"}
11	1	Zaakverdeler	Systeemrol: Zaakverdeler	t	2021-08-20 11:10:37.423386	2021-08-20 11:10:37.423386	2ad0489b-eb38-40b8-87c6-2b26c7485d9f	{"type": "role", "preview": "Zaakverdeler", "instance": {"name": "Zaakverdeler", "role_id": 11, "description": "Systeemrol: Zaakverdeler", "system_role": true, "date_created": "2021-08-20T11:10:37Z", "date_modified": "2021-08-20T11:10:37Z"}, "reference": "2ad0489b-eb38-40b8-87c6-2b26c7485d9f"}
12	1	Behandelaar	Systeemrol: Behandelaar	t	2021-08-20 11:10:37.423386	2021-08-20 11:10:37.423386	8b07fe5d-7685-4a58-b851-3d9692f90c62	{"type": "role", "preview": "Behandelaar", "instance": {"name": "Behandelaar", "role_id": 12, "description": "Systeemrol: Behandelaar", "system_role": true, "date_created": "2021-08-20T11:10:37Z", "date_modified": "2021-08-20T11:10:37Z"}, "reference": "8b07fe5d-7685-4a58-b851-3d9692f90c62"}
13	1	Gebruikersbeheerder	Systeemrol: Gebruikersbeheerder	t	2021-08-20 11:10:37.423386	2021-08-20 11:10:37.423386	22a6f674-0fd9-4c34-b7f0-cfc612cd57c4	{"type": "role", "preview": "Gebruikersbeheerder", "instance": {"name": "Gebruikersbeheerder", "role_id": 13, "description": "Systeemrol: Gebruikersbeheerder", "system_role": true, "date_created": "2021-08-20T11:10:37Z", "date_modified": "2021-08-20T11:10:37Z"}, "reference": "22a6f674-0fd9-4c34-b7f0-cfc612cd57c4"}
14	1	Documentintaker	Systeemrol: Documentintaker	t	2021-08-20 11:10:37.423386	2021-08-20 11:10:37.423386	22f6daa5-0f08-4821-b5df-1e1847f935ff	{"type": "role", "preview": "Documentintaker", "instance": {"name": "Documentintaker", "role_id": 14, "description": "Systeemrol: Documentintaker", "system_role": true, "date_created": "2021-08-20T11:10:37Z", "date_modified": "2021-08-20T11:10:37Z"}, "reference": "22f6daa5-0f08-4821-b5df-1e1847f935ff"}
15	1	BRP externe bevrager	Systeemrol: BRP externe bevrager	t	2021-08-20 11:10:37.423386	2021-08-20 11:10:37.423386	e878ae93-b62e-4673-b105-c08e898e3428	{"type": "role", "preview": "BRP externe bevrager", "instance": {"name": "BRP externe bevrager", "role_id": 15, "description": "Systeemrol: BRP externe bevrager", "system_role": true, "date_created": "2021-08-20T11:10:37Z", "date_modified": "2021-08-20T11:10:37Z"}, "reference": "e878ae93-b62e-4673-b105-c08e898e3428"}
16	1	App gebruiker	Systeemrol: Verleent enkel toegang tot apps	t	2021-08-20 11:10:37.423386	2021-08-20 11:10:37.423386	c59cce6d-d78a-41a3-9582-48b4c2c599a6	{"type": "role", "preview": "App gebruiker", "instance": {"name": "App gebruiker", "role_id": 16, "description": "Systeemrol: Verleent enkel toegang tot apps", "system_role": true, "date_created": "2021-08-20T11:10:37Z", "date_modified": "2021-08-20T11:10:37Z"}, "reference": "c59cce6d-d78a-41a3-9582-48b4c2c599a6"}
17	1	Persoonsverwerker	Systeemrol: Persoonsverwerker	t	2021-08-20 11:10:37.423386	2021-08-20 11:10:37.423386	a95e4082-67ab-4969-be66-63c5b27bfb79	{"type": "role", "preview": "Persoonsverwerker", "instance": {"name": "Persoonsverwerker", "role_id": 17, "description": "Systeemrol: Persoonsverwerker", "system_role": true, "date_created": "2021-08-20T11:10:37Z", "date_modified": "2021-08-20T11:10:37Z"}, "reference": "a95e4082-67ab-4969-be66-63c5b27bfb79"}
18	1	Klantcontacter	Systeemrol: Klantcontacter	t	2021-08-20 11:10:37.423386	2021-08-20 11:10:37.423386	623971d4-b6c9-4105-b56f-aa55459b57cc	{"type": "role", "preview": "Klantcontacter", "instance": {"name": "Klantcontacter", "role_id": 18, "description": "Systeemrol: Klantcontacter", "system_role": true, "date_created": "2021-08-20T11:10:37Z", "date_modified": "2021-08-20T11:10:37Z"}, "reference": "623971d4-b6c9-4105-b56f-aa55459b57cc"}
\.


--
-- Data for Name: sbus_logging; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.sbus_logging (id, sbus_traffic_id, pid, mutatie_type, object, params, kerngegeven, label, changes, error, error_message, created, modified) FROM stdin;
\.


--
-- Data for Name: sbus_traffic; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.sbus_traffic (id, sbus_type, object, operation, input, input_raw, output, output_raw, error, error_message, created, modified) FROM stdin;
\.


--
-- Data for Name: scheduled_jobs; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.scheduled_jobs (id, task, scheduled_for, parameters, created, last_modified, deleted, schedule_type, case_id, uuid) FROM stdin;
\.


--
-- Data for Name: search_query; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.search_query (id, settings, ldap_id, name, sort_index) FROM stdin;
\.


--
-- Data for Name: search_query_delen; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.search_query_delen (id, search_query_id, ou_id, role_id) FROM stdin;
\.


--
-- Data for Name: searchable; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.searchable (search_index, search_term, object_type, searchable_id, search_order) FROM stdin;
\.


--
-- Data for Name: service_geojson; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.service_geojson (id, uuid, geo_json) FROM stdin;
\.


--
-- Data for Name: service_geojson_relationship; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.service_geojson_relationship (id, service_geojson_id, related_uuid, uuid) FROM stdin;
\.


--
-- Data for Name: session_invitation; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.session_invitation (id, subject_id, object_id, object_type, date_created, date_expires, token, action_path) FROM stdin;
\.


--
-- Data for Name: settings; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.settings (id, key, value) FROM stdin;
\.


--
-- Data for Name: subject; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.subject (id, uuid, subject_type, properties, settings, username, last_modified, role_ids, group_ids, nobody, system, related_custom_object_id) FROM stdin;
1	7145766b-472c-4316-9634-6470ea51c4c9	employee	{"givenname":"Ad","initials":"A.","cn":"admin","telephonenumber":"0612345678","sn":"Mïn","displayname":"Ad Mïn","mail":"devnull@zaaksysteem.nl"}	{}	admin	2021-08-20 11:10:37.423386	{1,12}	{2}	f	t	\N
2	48dfb907-8f67-4795-891c-8184225466de	employee	{"initials":"beheerder","givenname":"beheerder","telephonenumber":"0612345678","cn":"beheerder","sn":"beheerder","mail":"devnull@zaaksysteem.nl","displayname":"beheerder"}	{}	beheerder	2021-08-20 11:10:37.423386	{1,12}	{2}	f	t	\N
3	e4396de0-7a83-4deb-b1de-78cb4ea6ec23	employee	{"givenname":"gebruiker","initials":"gebruiker","cn":"gebruiker","telephonenumber":"0612345678","sn":"gebruiker","displayname":"gebruiker","mail":"devnull@zaaksysteem.nl"}	{}	gebruiker	2021-08-20 11:10:37.423386	{12}	{3}	f	t	\N
\.


--
-- Data for Name: subject_login_history; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.subject_login_history (id, ip, subject_id, subject_uuid, success, method, date_attempt) FROM stdin;
\.


--
-- Data for Name: thread; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.thread (id, uuid, contact_uuid, contact_displayname, case_id, created, last_modified, thread_type, last_message_cache, message_count, unread_pip_count, unread_employee_count, attachment_count) FROM stdin;
\.


--
-- Data for Name: thread_message; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.thread_message (id, uuid, thread_id, type, message_slug, created_by_uuid, created_by_displayname, created, last_modified, thread_message_note_id, thread_message_contact_moment_id, thread_message_external_id, message_date) FROM stdin;
\.


--
-- Data for Name: thread_message_attachment; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.thread_message_attachment (id, filestore_id, thread_message_id, uuid, filename) FROM stdin;
\.


--
-- Data for Name: thread_message_attachment_derivative; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.thread_message_attachment_derivative (id, thread_message_attachment_id, filestore_id, max_width, max_height, date_generated, type) FROM stdin;
\.


--
-- Data for Name: thread_message_contact_moment; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.thread_message_contact_moment (id, content, contact_channel, direction, recipient_uuid, recipient_displayname) FROM stdin;
\.


--
-- Data for Name: thread_message_external; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.thread_message_external (id, type, content, subject, participants, direction, source_file_id, read_pip, read_employee, attachment_count, failure_reason) FROM stdin;
\.


--
-- Data for Name: thread_message_note; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.thread_message_note (id, content) FROM stdin;
\.


--
-- Data for Name: transaction; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.transaction (id, interface_id, external_transaction_id, input_data, input_file, automated_retry_count, date_created, date_last_retry, date_next_retry, processed, date_deleted, error_count, direction, success_count, total_count, processor_params, error_fatal, preview_data, error_message, text_vector, uuid) FROM stdin;
\.


--
-- Data for Name: transaction_record; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.transaction_record (id, transaction_id, input, output, is_error, date_executed, date_deleted, preview_string, last_error, uuid) FROM stdin;
\.


--
-- Data for Name: transaction_record_to_object; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.transaction_record_to_object (id, transaction_record_id, local_table, local_id, mutations, date_deleted, mutation_type) FROM stdin;
\.


--
-- Data for Name: user_app_lock; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.user_app_lock (type, type_id, create_unixtime, session_id, uidnumber) FROM stdin;
\.


--
-- Data for Name: user_entity; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.user_entity (id, uuid, source_interface_id, source_identifier, subject_id, date_created, date_deleted, properties, password, active) FROM stdin;
1	9c11e4f6-7e7c-42de-a675-078e32a16717	1	admin	1	2021-08-20 11:10:37.423386	\N	{}	{CLEARTEXT}admin	t
2	8ed71efd-a699-4948-9b68-6906d54877de	1	beheerder	2	2021-08-20 11:10:37.423386	\N	{}	{CLEARTEXT}beheerder	t
3	2f21712b-38a8-40e7-a62f-4a8025f9c63d	1	gebruiker	3	2021-08-20 11:10:37.423386	\N	{}	{CLEARTEXT}gebruiker	t
\.


--
-- Data for Name: woz_objects; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.woz_objects (id, object_data, owner, object_id) FROM stdin;
\.


--
-- Data for Name: zaak; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.zaak (search_index, search_term, object_type, searchable_id, search_order, id, pid, relates_to, zaaktype_id, zaaktype_node_id, milestone, contactkanaal, aanvraag_trigger, onderwerp, resultaat, besluit, coordinator, behandelaar, aanvrager, route_ou, route_role, locatie_zaak, locatie_correspondentie, streefafhandeldatum, registratiedatum, afhandeldatum, vernietigingsdatum, created, last_modified, deleted, vervolg_van, aanvrager_gm_id, behandelaar_gm_id, coordinator_gm_id, uuid, payment_status, payment_amount, confidentiality, stalled_until, onderwerp_extern, archival_state, status, duplicate_prevention_token, resultaat_id, urgency, preset_client, prefix, confidential, html_email_template, number_master, requestor_v1_json, assignee_v1_json, coordinator_v1_json, aanvrager_type, betrokkenen_cache, status_percentage) FROM stdin;
\.


--
-- Data for Name: zaak_authorisation; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.zaak_authorisation (id, zaak_id, capability, entity_id, entity_type, scope) FROM stdin;
\.


--
-- Data for Name: zaak_bag; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.zaak_bag (id, pid, zaak_id, bag_type, bag_id, bag_verblijfsobject_id, bag_openbareruimte_id, bag_nummeraanduiding_id, bag_pand_id, bag_standplaats_id, bag_ligplaats_id, bag_coordinates_wsg) FROM stdin;
\.


--
-- Data for Name: zaak_betrokkenen; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.zaak_betrokkenen (id, zaak_id, betrokkene_type, betrokkene_id, gegevens_magazijn_id, verificatie, naam, rol, magic_string_prefix, deleted, uuid, pip_authorized, subject_id, authorisation, bibliotheek_kenmerken_id) FROM stdin;
\.


--
-- Data for Name: zaak_kenmerk; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.zaak_kenmerk (zaak_id, bibliotheek_kenmerken_id, id, value, magic_string) FROM stdin;
\.


--
-- Data for Name: zaak_meta; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.zaak_meta (id, zaak_id, verlenging, opschorten, deel, gerelateerd, vervolg, afhandeling, stalled_since, current_deadline, deadline_timeline, last_modified, index_hstore, text_vector, unread_communication_count, unaccepted_attribute_update_count, pending_changes, unaccepted_files_count) FROM stdin;
\.


--
-- Data for Name: zaak_onafgerond; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.zaak_onafgerond (zaaktype_id, betrokkene, json_string, afronden, create_unixtime) FROM stdin;
\.


--
-- Data for Name: zaak_subcase; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.zaak_subcase (id, zaak_id, relation_zaak_id, required) FROM stdin;
\.


--
-- Data for Name: zaaktype; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.zaaktype (search_index, search_term, object_type, searchable_id, search_order, id, zaaktype_node_id, version, created, last_modified, deleted, bibliotheek_categorie_id, active, uuid) FROM stdin;
\.


--
-- Data for Name: zaaktype_authorisation; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.zaaktype_authorisation (id, zaaktype_node_id, recht, created, last_modified, deleted, role_id, ou_id, zaaktype_id, confidential) FROM stdin;
\.


--
-- Data for Name: zaaktype_betrokkenen; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.zaaktype_betrokkenen (id, zaaktype_node_id, betrokkene_type, created, last_modified) FROM stdin;
\.


--
-- Data for Name: zaaktype_definitie; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.zaaktype_definitie (id, openbaarheid, handelingsinitiator, grondslag, procesbeschrijving, afhandeltermijn, afhandeltermijn_type, selectielijst, servicenorm, servicenorm_type, pdc_voorwaarden, pdc_description, pdc_meenemen, pdc_tarief, omschrijving_upl, aard, extra_informatie, preset_client, extra_informatie_extern) FROM stdin;
\.


--
-- Data for Name: zaaktype_kenmerken; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.zaaktype_kenmerken (id, bibliotheek_kenmerken_id, label, help, created, last_modified, zaaktype_node_id, zaak_status_id, pip, zaakinformatie_view, bag_zaakadres, value_default, pip_can_change, publish_public, is_systeemkenmerk, required_permissions, version, help_extern, object_id, object_metadata, label_multiple, properties, is_group, referential, uuid, custom_object_uuid, value_mandatory) FROM stdin;
\.


--
-- Data for Name: zaaktype_node; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.zaaktype_node (id, zaaktype_id, zaaktype_rt_queue, code, trigger, titel, version, active, created, last_modified, deleted, webform_toegang, webform_authenticatie, adres_relatie, aanvrager_hergebruik, automatisch_aanvragen, automatisch_behandelen, toewijzing_zaakintake, toelichting, online_betaling, zaaktype_definitie_id, adres_andere_locatie, adres_aanvrager, bedrijfid_wijzigen, zaaktype_vertrouwelijk, zaaktype_trefwoorden, zaaktype_omschrijving, extra_relaties_in_aanvraag, properties, contact_info_intake, is_public, prevent_pip, contact_info_email_required, contact_info_phone_required, contact_info_mobile_phone_required, moeder_zaaktype_id, logging_id, uuid, adres_geojson, v0_json) FROM stdin;
\.


--
-- Data for Name: zaaktype_notificatie; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.zaaktype_notificatie (id, zaaktype_node_id, zaak_status_id, label, rcpt, onderwerp, bericht, created, last_modified, intern_block, email, bibliotheek_notificaties_id, behandelaar, automatic, cc, bcc, betrokkene_role) FROM stdin;
\.


--
-- Data for Name: zaaktype_regel; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.zaaktype_regel (id, zaaktype_node_id, zaak_status_id, naam, created, last_modified, settings, active, is_group) FROM stdin;
\.


--
-- Data for Name: zaaktype_relatie; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.zaaktype_relatie (id, zaaktype_node_id, relatie_zaaktype_id, zaaktype_status_id, relatie_type, eigenaar_type, start_delay, created, last_modified, kopieren_kenmerken, ou_id, role_id, automatisch_behandelen, required, betrokkene_authorized, betrokkene_notify, betrokkene_id, betrokkene_role, betrokkene_role_set, betrokkene_prefix, eigenaar_id, eigenaar_role, show_in_pip, pip_label, subject_role, copy_subject_role, copy_related_cases, copy_related_objects, copy_selected_attributes, creation_style, status) FROM stdin;
\.


--
-- Data for Name: zaaktype_resultaten; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.zaaktype_resultaten (id, zaaktype_node_id, zaaktype_status_id, resultaat, ingang, bewaartermijn, created, last_modified, dossiertype, label, selectielijst, archiefnominatie, comments, external_reference, trigger_archival, selectielijst_brondatum, selectielijst_einddatum, properties, standaard_keuze) FROM stdin;
\.


--
-- Data for Name: zaaktype_sjablonen; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.zaaktype_sjablonen (id, zaaktype_node_id, bibliotheek_sjablonen_id, help, zaak_status_id, created, last_modified, automatisch_genereren, bibliotheek_kenmerken_id, target_format, custom_filename, label) FROM stdin;
\.


--
-- Data for Name: zaaktype_standaard_betrokkenen; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.zaaktype_standaard_betrokkenen (id, zaaktype_node_id, zaak_status_id, betrokkene_type, betrokkene_identifier, naam, rol, magic_string_prefix, gemachtigd, notify, uuid) FROM stdin;
\.


--
-- Data for Name: zaaktype_status; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.zaaktype_status (id, zaaktype_node_id, status, status_type, naam, created, last_modified, ou_id, role_id, checklist, fase, role_set, termijn) FROM stdin;
\.


--
-- Data for Name: zaaktype_status_checklist_item; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.zaaktype_status_checklist_item (id, casetype_status_id, label, external_reference) FROM stdin;
\.


--
-- Name: adres_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.adres_id_seq', 1, false);


--
-- Name: bag_cache_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.bag_cache_id_seq', 1, false);


--
-- Name: bag_ligplaats_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.bag_ligplaats_id_seq', 1, false);


--
-- Name: bag_nummeraanduiding_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.bag_nummeraanduiding_id_seq', 1, false);


--
-- Name: bag_openbareruimte_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.bag_openbareruimte_id_seq', 1, false);


--
-- Name: bag_pand_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.bag_pand_id_seq', 1, false);


--
-- Name: bag_standplaats_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.bag_standplaats_id_seq', 1, false);


--
-- Name: bag_verblijfsobject_gebruiksdoel_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.bag_verblijfsobject_gebruiksdoel_id_seq', 1, false);


--
-- Name: bag_verblijfsobject_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.bag_verblijfsobject_id_seq', 1, false);


--
-- Name: bag_verblijfsobject_pand_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.bag_verblijfsobject_pand_id_seq', 1, false);


--
-- Name: bag_woonplaats_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.bag_woonplaats_id_seq', 1, false);


--
-- Name: bedrijf_authenticatie_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.bedrijf_authenticatie_id_seq', 1, false);


--
-- Name: bedrijf_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.bedrijf_id_seq', 1, false);


--
-- Name: beheer_import_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.beheer_import_id_seq', 1, false);


--
-- Name: beheer_import_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.beheer_import_log_id_seq', 1, false);


--
-- Name: betrokkene_notes_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.betrokkene_notes_id_seq', 1, false);


--
-- Name: betrokkenen_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.betrokkenen_id_seq', 1, false);


--
-- Name: bibliotheek_categorie_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.bibliotheek_categorie_id_seq', 1, false);


--
-- Name: bibliotheek_kenmerken_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.bibliotheek_kenmerken_id_seq', 1, false);


--
-- Name: bibliotheek_kenmerken_values_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.bibliotheek_kenmerken_values_id_seq', 1, false);


--
-- Name: bibliotheek_kenmerken_values_sort_order_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.bibliotheek_kenmerken_values_sort_order_seq', 1, false);


--
-- Name: bibliotheek_notificatie_kenmerk_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.bibliotheek_notificatie_kenmerk_id_seq', 1, false);


--
-- Name: bibliotheek_notificaties_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.bibliotheek_notificaties_id_seq', 1, false);


--
-- Name: bibliotheek_sjablonen_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.bibliotheek_sjablonen_id_seq', 1, false);


--
-- Name: bibliotheek_sjablonen_magic_string_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.bibliotheek_sjablonen_magic_string_id_seq', 1, false);


--
-- Name: case_action_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.case_action_id_seq', 1, false);


--
-- Name: case_relation_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.case_relation_id_seq', 1, false);


--
-- Name: checklist_antwoord_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.checklist_antwoord_id_seq', 1, false);


--
-- Name: checklist_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.checklist_id_seq', 1, false);


--
-- Name: checklist_item_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.checklist_item_id_seq', 1, false);


--
-- Name: config_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.config_id_seq', 63, true);


--
-- Name: contact_data_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.contact_data_id_seq', 1, false);


--
-- Name: contact_relationship_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.contact_relationship_id_seq', 1, false);


--
-- Name: contactmoment_email_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.contactmoment_email_id_seq', 1, false);


--
-- Name: contactmoment_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.contactmoment_id_seq', 1, false);


--
-- Name: contactmoment_note_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.contactmoment_note_id_seq', 1, false);


--
-- Name: country_code_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.country_code_id_seq', 386, true);


--
-- Name: custom_object_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.custom_object_id_seq', 1, false);


--
-- Name: custom_object_relationship_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.custom_object_relationship_id_seq', 1, false);


--
-- Name: custom_object_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.custom_object_type_id_seq', 1, false);


--
-- Name: custom_object_type_version_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.custom_object_type_version_id_seq', 1, false);


--
-- Name: custom_object_version_content_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.custom_object_version_content_id_seq', 1, false);


--
-- Name: custom_object_version_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.custom_object_version_id_seq', 1, false);


--
-- Name: directory_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.directory_id_seq', 1, false);


--
-- Name: export_queue_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.export_queue_id_seq', 1, false);


--
-- Name: file_case_document_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.file_case_document_id_seq', 1, false);


--
-- Name: file_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.file_id_seq', 1, false);


--
-- Name: file_metadata_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.file_metadata_id_seq', 1, false);


--
-- Name: filestore_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.filestore_id_seq', 1, false);


--
-- Name: gm_adres_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.gm_adres_id_seq', 1, false);


--
-- Name: gm_bedrijf_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.gm_bedrijf_id_seq', 1, false);


--
-- Name: gm_natuurlijk_persoon_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.gm_natuurlijk_persoon_id_seq', 1, false);


--
-- Name: groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.groups_id_seq', 3, true);


--
-- Name: interface_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.interface_id_seq', 1, true);


--
-- Name: legal_entity_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.legal_entity_type_id_seq', 47, true);


--
-- Name: logging_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.logging_id_seq', 1, false);


--
-- Name: message_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.message_id_seq', 1, false);


--
-- Name: municipality_code_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.municipality_code_id_seq', 474, true);


--
-- Name: natuurlijk_persoon_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.natuurlijk_persoon_id_seq', 1, false);


--
-- Name: object_bibliotheek_entry_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.object_bibliotheek_entry_id_seq', 1, false);


--
-- Name: object_subscription_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.object_subscription_id_seq', 1, false);


--
-- Name: parkeergebied_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.parkeergebied_id_seq', 1, false);


--
-- Name: remote_api_keys_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.remote_api_keys_id_seq', 1, false);


--
-- Name: result_preservation_terms_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.result_preservation_terms_id_seq', 31, true);


--
-- Name: roles_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.roles_id_seq', 18, true);


--
-- Name: sbus_logging_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.sbus_logging_id_seq', 1, false);


--
-- Name: sbus_traffic_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.sbus_traffic_id_seq', 1, false);


--
-- Name: scheduled_jobs_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.scheduled_jobs_id_seq', 1, false);


--
-- Name: search_query_delen_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.search_query_delen_id_seq', 1, false);


--
-- Name: search_query_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.search_query_id_seq', 1, false);


--
-- Name: searchable_searchable_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.searchable_searchable_id_seq', 1, false);


--
-- Name: service_geojson_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.service_geojson_id_seq', 1, false);


--
-- Name: service_geojson_relationship_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.service_geojson_relationship_id_seq', 1, false);


--
-- Name: settings_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.settings_id_seq', 1, false);


--
-- Name: subject_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.subject_id_seq', 3, true);


--
-- Name: subject_login_history_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.subject_login_history_id_seq', 1, false);


--
-- Name: thread_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.thread_id_seq', 1, false);


--
-- Name: thread_message_attachment_derivative_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.thread_message_attachment_derivative_id_seq', 1, false);


--
-- Name: thread_message_attachment_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.thread_message_attachment_id_seq', 1, false);


--
-- Name: thread_message_contact_moment_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.thread_message_contact_moment_id_seq', 1, false);


--
-- Name: thread_message_external_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.thread_message_external_id_seq', 1, false);


--
-- Name: thread_message_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.thread_message_id_seq', 1, false);


--
-- Name: thread_message_note_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.thread_message_note_id_seq', 1, false);


--
-- Name: transaction_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.transaction_id_seq', 1, false);


--
-- Name: transaction_record_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.transaction_record_id_seq', 1, false);


--
-- Name: transaction_record_to_object_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.transaction_record_to_object_id_seq', 1, false);


--
-- Name: user_entity_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.user_entity_id_seq', 3, true);


--
-- Name: woz_objects_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.woz_objects_id_seq', 1, false);


--
-- Name: zaak_authorisation_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.zaak_authorisation_id_seq', 1, false);


--
-- Name: zaak_bag_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.zaak_bag_id_seq', 1, false);


--
-- Name: zaak_betrokkenen_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.zaak_betrokkenen_id_seq', 1, false);


--
-- Name: zaak_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.zaak_id_seq', 1, false);


--
-- Name: zaak_kenmerk_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.zaak_kenmerk_id_seq', 1, false);


--
-- Name: zaak_meta_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.zaak_meta_id_seq', 1, false);


--
-- Name: zaak_subcase_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.zaak_subcase_id_seq', 1, false);


--
-- Name: zaaktype_authorisation_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.zaaktype_authorisation_id_seq', 1, false);


--
-- Name: zaaktype_betrokkenen_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.zaaktype_betrokkenen_id_seq', 1, false);


--
-- Name: zaaktype_definitie_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.zaaktype_definitie_id_seq', 1, false);


--
-- Name: zaaktype_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.zaaktype_id_seq', 1, false);


--
-- Name: zaaktype_kenmerken_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.zaaktype_kenmerken_id_seq', 1, false);


--
-- Name: zaaktype_node_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.zaaktype_node_id_seq', 1, false);


--
-- Name: zaaktype_notificatie_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.zaaktype_notificatie_id_seq', 1, false);


--
-- Name: zaaktype_regel_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.zaaktype_regel_id_seq', 1, false);


--
-- Name: zaaktype_relatie_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.zaaktype_relatie_id_seq', 1, false);


--
-- Name: zaaktype_resultaten_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.zaaktype_resultaten_id_seq', 1, false);


--
-- Name: zaaktype_sjablonen_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.zaaktype_sjablonen_id_seq', 1, false);


--
-- Name: zaaktype_standaard_betrokkenen_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.zaaktype_standaard_betrokkenen_id_seq', 1, false);


--
-- Name: zaaktype_status_checklist_item_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.zaaktype_status_checklist_item_id_seq', 1, false);


--
-- Name: zaaktype_status_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.zaaktype_status_id_seq', 1, false);


--
-- Name: zorginstituut_identificatie_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.zorginstituut_identificatie_seq', 1, false);


--
-- Name: natuurlijk_persoon adres_id; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.natuurlijk_persoon
    ADD CONSTRAINT adres_id UNIQUE (id);


--
-- Name: adres adres_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.adres
    ADD CONSTRAINT adres_pkey PRIMARY KEY (id);


--
-- Name: alternative_authentication_activation_link alternative_authentication_activation_link_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.alternative_authentication_activation_link
    ADD CONSTRAINT alternative_authentication_activation_link_pkey PRIMARY KEY (token);


--
-- Name: bag_cache bag_cache_bag_type_bag_id_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bag_cache
    ADD CONSTRAINT bag_cache_bag_type_bag_id_key UNIQUE (bag_type, bag_id);


--
-- Name: bag_cache bag_cache_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bag_cache
    ADD CONSTRAINT bag_cache_pkey PRIMARY KEY (id);


--
-- Name: bag_ligplaats bag_ligplaats_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bag_ligplaats
    ADD CONSTRAINT bag_ligplaats_pkey PRIMARY KEY (id);


--
-- Name: bag_nummeraanduiding bag_nummeraanduiding_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bag_nummeraanduiding
    ADD CONSTRAINT bag_nummeraanduiding_pkey PRIMARY KEY (id);


--
-- Name: bag_openbareruimte bag_openbareruimte_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bag_openbareruimte
    ADD CONSTRAINT bag_openbareruimte_pkey PRIMARY KEY (id);


--
-- Name: bag_pand bag_pand_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bag_pand
    ADD CONSTRAINT bag_pand_pkey PRIMARY KEY (id);


--
-- Name: bag_standplaats bag_standplaats_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bag_standplaats
    ADD CONSTRAINT bag_standplaats_pkey PRIMARY KEY (id);


--
-- Name: bag_verblijfsobject_gebruiksdoel bag_verblijfsobject_gebruiksdoel_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bag_verblijfsobject_gebruiksdoel
    ADD CONSTRAINT bag_verblijfsobject_gebruiksdoel_pkey PRIMARY KEY (id);


--
-- Name: bag_verblijfsobject_pand bag_verblijfsobject_pand_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bag_verblijfsobject_pand
    ADD CONSTRAINT bag_verblijfsobject_pand_pkey PRIMARY KEY (id);


--
-- Name: bag_verblijfsobject bag_verblijfsobject_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bag_verblijfsobject
    ADD CONSTRAINT bag_verblijfsobject_pkey PRIMARY KEY (id);


--
-- Name: bag_woonplaats bag_woonplaats_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bag_woonplaats
    ADD CONSTRAINT bag_woonplaats_pkey PRIMARY KEY (id);


--
-- Name: bedrijf_authenticatie bedrijf_authenticatie_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bedrijf_authenticatie
    ADD CONSTRAINT bedrijf_authenticatie_pkey PRIMARY KEY (id);


--
-- Name: bedrijf bedrijf_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bedrijf
    ADD CONSTRAINT bedrijf_pkey PRIMARY KEY (id);


--
-- Name: bedrijf bedrijf_uuid_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bedrijf
    ADD CONSTRAINT bedrijf_uuid_key UNIQUE (uuid);


--
-- Name: beheer_import_log beheer_import_log_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.beheer_import_log
    ADD CONSTRAINT beheer_import_log_pkey PRIMARY KEY (id);


--
-- Name: beheer_import beheer_import_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.beheer_import
    ADD CONSTRAINT beheer_import_pkey PRIMARY KEY (id);


--
-- Name: betrokkene_notes betrokkene_notes_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.betrokkene_notes
    ADD CONSTRAINT betrokkene_notes_pkey PRIMARY KEY (id);


--
-- Name: betrokkenen betrokkenen_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.betrokkenen
    ADD CONSTRAINT betrokkenen_pkey PRIMARY KEY (id);


--
-- Name: bibliotheek_categorie bibliotheek_categorie_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bibliotheek_categorie
    ADD CONSTRAINT bibliotheek_categorie_pkey PRIMARY KEY (id);


--
-- Name: bibliotheek_kenmerken bibliotheek_kenmerken_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bibliotheek_kenmerken
    ADD CONSTRAINT bibliotheek_kenmerken_pkey PRIMARY KEY (id);


--
-- Name: bibliotheek_kenmerken_values bibliotheek_kenmerken_values_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bibliotheek_kenmerken_values
    ADD CONSTRAINT bibliotheek_kenmerken_values_pkey PRIMARY KEY (id);


--
-- Name: bibliotheek_notificatie_kenmerk bibliotheek_notificatie_kenmerk_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bibliotheek_notificatie_kenmerk
    ADD CONSTRAINT bibliotheek_notificatie_kenmerk_pkey PRIMARY KEY (id);


--
-- Name: bibliotheek_notificaties bibliotheek_notificaties_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bibliotheek_notificaties
    ADD CONSTRAINT bibliotheek_notificaties_pkey PRIMARY KEY (id);


--
-- Name: bibliotheek_sjablonen_magic_string bibliotheek_sjablonen_magic_string_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bibliotheek_sjablonen_magic_string
    ADD CONSTRAINT bibliotheek_sjablonen_magic_string_pkey PRIMARY KEY (id);


--
-- Name: bibliotheek_sjablonen bibliotheek_sjablonen_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bibliotheek_sjablonen
    ADD CONSTRAINT bibliotheek_sjablonen_pkey PRIMARY KEY (id);


--
-- Name: case_action case_actions_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.case_action
    ADD CONSTRAINT case_actions_pkey PRIMARY KEY (id);


--
-- Name: case_authorisation_map case_authorisation_map_key_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.case_authorisation_map
    ADD CONSTRAINT case_authorisation_map_key_key UNIQUE (key);


--
-- Name: case_authorisation_map case_authorisation_map_legacy_key_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.case_authorisation_map
    ADD CONSTRAINT case_authorisation_map_legacy_key_key UNIQUE (legacy_key);


--
-- Name: case_authorisation_map case_authorisation_map_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.case_authorisation_map
    ADD CONSTRAINT case_authorisation_map_pkey PRIMARY KEY (key, legacy_key);


--
-- Name: case_property case_property_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.case_property
    ADD CONSTRAINT case_property_pkey PRIMARY KEY (id);


--
-- Name: case_relation case_relation_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.case_relation
    ADD CONSTRAINT case_relation_pkey PRIMARY KEY (id);


--
-- Name: checklist_item checklist_item_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.checklist_item
    ADD CONSTRAINT checklist_item_pkey PRIMARY KEY (id);


--
-- Name: checklist_item checklist_item_uuid_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.checklist_item
    ADD CONSTRAINT checklist_item_uuid_key UNIQUE (uuid);


--
-- Name: checklist checklist_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.checklist
    ADD CONSTRAINT checklist_pkey PRIMARY KEY (id);


--
-- Name: config config_definition_id_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.config
    ADD CONSTRAINT config_definition_id_key UNIQUE (definition_id);


--
-- Name: config config_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.config
    ADD CONSTRAINT config_pkey PRIMARY KEY (id);


--
-- Name: contact_data contact_data_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.contact_data
    ADD CONSTRAINT contact_data_pkey PRIMARY KEY (id);


--
-- Name: contact_relationship contact_relationship_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.contact_relationship
    ADD CONSTRAINT contact_relationship_pkey PRIMARY KEY (id);


--
-- Name: contactmoment_email contactmoment_email_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.contactmoment_email
    ADD CONSTRAINT contactmoment_email_pkey PRIMARY KEY (id);


--
-- Name: contactmoment_note contactmoment_note_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.contactmoment_note
    ADD CONSTRAINT contactmoment_note_pkey PRIMARY KEY (id);


--
-- Name: contactmoment contactmoment_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.contactmoment
    ADD CONSTRAINT contactmoment_pkey PRIMARY KEY (id);


--
-- Name: country_code country_code_dutch_code_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.country_code
    ADD CONSTRAINT country_code_dutch_code_key UNIQUE (dutch_code);


--
-- Name: custom_object custom_object_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.custom_object
    ADD CONSTRAINT custom_object_pkey PRIMARY KEY (id);


--
-- Name: custom_object_relationship custom_object_relationship_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.custom_object_relationship
    ADD CONSTRAINT custom_object_relationship_pkey PRIMARY KEY (id);


--
-- Name: custom_object_relationship custom_object_relationship_uuid_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.custom_object_relationship
    ADD CONSTRAINT custom_object_relationship_uuid_key UNIQUE (uuid);


--
-- Name: custom_object_type custom_object_type_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.custom_object_type
    ADD CONSTRAINT custom_object_type_pkey PRIMARY KEY (id);


--
-- Name: custom_object_type custom_object_type_uuid_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.custom_object_type
    ADD CONSTRAINT custom_object_type_uuid_key UNIQUE (uuid);


--
-- Name: custom_object_type_version custom_object_type_version_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.custom_object_type_version
    ADD CONSTRAINT custom_object_type_version_pkey PRIMARY KEY (id);


--
-- Name: custom_object_type_version custom_object_type_version_uuid_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.custom_object_type_version
    ADD CONSTRAINT custom_object_type_version_uuid_key UNIQUE (uuid);


--
-- Name: custom_object custom_object_uuid_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.custom_object
    ADD CONSTRAINT custom_object_uuid_key UNIQUE (uuid);


--
-- Name: custom_object_version_content custom_object_version_content_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.custom_object_version_content
    ADD CONSTRAINT custom_object_version_content_pkey PRIMARY KEY (id);


--
-- Name: custom_object_version custom_object_version_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.custom_object_version
    ADD CONSTRAINT custom_object_version_key UNIQUE (id, custom_object_id);


--
-- Name: custom_object_version custom_object_version_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.custom_object_version
    ADD CONSTRAINT custom_object_version_pkey PRIMARY KEY (id);


--
-- Name: custom_object_version custom_object_version_uuid_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.custom_object_version
    ADD CONSTRAINT custom_object_version_uuid_key UNIQUE (uuid);


--
-- Name: directory directory_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.directory
    ADD CONSTRAINT directory_pkey PRIMARY KEY (id);


--
-- Name: directory directory_uuid_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.directory
    ADD CONSTRAINT directory_uuid_key UNIQUE (uuid);


--
-- Name: export_queue export_queue_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.export_queue
    ADD CONSTRAINT export_queue_pkey PRIMARY KEY (id);


--
-- Name: file_annotation file_annotation_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.file_annotation
    ADD CONSTRAINT file_annotation_pkey PRIMARY KEY (id);


--
-- Name: file_case_document file_case_document_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.file_case_document
    ADD CONSTRAINT file_case_document_pkey PRIMARY KEY (id);


--
-- Name: file_metadata file_metadata_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.file_metadata
    ADD CONSTRAINT file_metadata_pkey PRIMARY KEY (id);


--
-- Name: file file_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.file
    ADD CONSTRAINT file_pkey PRIMARY KEY (id);


--
-- Name: file_derivative file_thumbnail_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.file_derivative
    ADD CONSTRAINT file_thumbnail_pkey PRIMARY KEY (id);


--
-- Name: filestore filestore_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.filestore
    ADD CONSTRAINT filestore_pkey PRIMARY KEY (id);


--
-- Name: gegevensmagazijn_subjecten gegevensmagazijn_subjecten_subject_uuid_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.gegevensmagazijn_subjecten
    ADD CONSTRAINT gegevensmagazijn_subjecten_subject_uuid_key UNIQUE (subject_uuid);


--
-- Name: gm_adres gm_adres_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.gm_adres
    ADD CONSTRAINT gm_adres_pkey PRIMARY KEY (id);


--
-- Name: gm_bedrijf gm_bedrijf_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.gm_bedrijf
    ADD CONSTRAINT gm_bedrijf_pkey PRIMARY KEY (id);


--
-- Name: gm_natuurlijk_persoon gm_natuurlijk_persoon_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.gm_natuurlijk_persoon
    ADD CONSTRAINT gm_natuurlijk_persoon_pkey PRIMARY KEY (id);


--
-- Name: groups groups_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.groups
    ADD CONSTRAINT groups_pkey PRIMARY KEY (id);


--
-- Name: interface interface_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.interface
    ADD CONSTRAINT interface_pkey PRIMARY KEY (id);


--
-- Name: legal_entity_type legal_entity_type_code_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.legal_entity_type
    ADD CONSTRAINT legal_entity_type_code_key UNIQUE (code);


--
-- Name: logging logging_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.logging
    ADD CONSTRAINT logging_pkey PRIMARY KEY (id);


--
-- Name: logging logging_uuid_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.logging
    ADD CONSTRAINT logging_uuid_key UNIQUE (uuid);


--
-- Name: message message_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.message
    ADD CONSTRAINT message_pkey PRIMARY KEY (id);


--
-- Name: municipality_code municipality_code_dutch_code_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.municipality_code
    ADD CONSTRAINT municipality_code_dutch_code_key UNIQUE (dutch_code);


--
-- Name: directory name_case_id; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.directory
    ADD CONSTRAINT name_case_id UNIQUE (name, case_id);


--
-- Name: natuurlijk_persoon natuurlijk_persoon_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.natuurlijk_persoon
    ADD CONSTRAINT natuurlijk_persoon_pkey PRIMARY KEY (id);


--
-- Name: natuurlijk_persoon natuurlijk_persoon_uuid_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.natuurlijk_persoon
    ADD CONSTRAINT natuurlijk_persoon_uuid_key UNIQUE (uuid);


--
-- Name: object_acl_entry object_acl_entry_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.object_acl_entry
    ADD CONSTRAINT object_acl_entry_pkey PRIMARY KEY (uuid);


--
-- Name: object_data object_data_object_class_object_id_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.object_data
    ADD CONSTRAINT object_data_object_class_object_id_key UNIQUE (object_class, object_id);


--
-- Name: object_data object_data_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.object_data
    ADD CONSTRAINT object_data_pkey PRIMARY KEY (uuid);


--
-- Name: object_mutation object_mutation_object_uuid_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.object_mutation
    ADD CONSTRAINT object_mutation_object_uuid_key UNIQUE (object_uuid);


--
-- Name: object_mutation object_mutation_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.object_mutation
    ADD CONSTRAINT object_mutation_pkey PRIMARY KEY (id);


--
-- Name: object_relation object_relation_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.object_relation
    ADD CONSTRAINT object_relation_pkey PRIMARY KEY (id);


--
-- Name: object_relationships object_relationships_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.object_relationships
    ADD CONSTRAINT object_relationships_pkey PRIMARY KEY (uuid);


--
-- Name: object_subscription object_subscription_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.object_subscription
    ADD CONSTRAINT object_subscription_pkey PRIMARY KEY (id);


--
-- Name: object_bibliotheek_entry object_type_bibliotheek_entry_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.object_bibliotheek_entry
    ADD CONSTRAINT object_type_bibliotheek_entry_pkey PRIMARY KEY (id);


--
-- Name: config parameter_unique; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.config
    ADD CONSTRAINT parameter_unique UNIQUE (parameter);


--
-- Name: parkeergebied parkeergebied_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.parkeergebied
    ADD CONSTRAINT parkeergebied_pkey PRIMARY KEY (id);


--
-- Name: bag_ligplaats_nevenadres pk_ligplaats_nevenadres; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bag_ligplaats_nevenadres
    ADD CONSTRAINT pk_ligplaats_nevenadres PRIMARY KEY (identificatie, begindatum, correctie, nevenadres);


--
-- Name: queue queue_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.queue
    ADD CONSTRAINT queue_pkey PRIMARY KEY (id);


--
-- Name: remote_api_keys remote_api_keys_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.remote_api_keys
    ADD CONSTRAINT remote_api_keys_pkey PRIMARY KEY (id);


--
-- Name: result_preservation_terms result_preservation_terms_code_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.result_preservation_terms
    ADD CONSTRAINT result_preservation_terms_code_key UNIQUE (code);


--
-- Name: result_preservation_terms result_preservation_terms_label_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.result_preservation_terms
    ADD CONSTRAINT result_preservation_terms_label_key UNIQUE (label);


--
-- Name: rights rights_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.rights
    ADD CONSTRAINT rights_pkey PRIMARY KEY (name);


--
-- Name: role_rights role_rights_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.role_rights
    ADD CONSTRAINT role_rights_pkey PRIMARY KEY (rights_name, role_id);


--
-- Name: roles roles_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.roles
    ADD CONSTRAINT roles_pkey PRIMARY KEY (id);


--
-- Name: sbus_logging sbus_logging_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sbus_logging
    ADD CONSTRAINT sbus_logging_pkey PRIMARY KEY (id);


--
-- Name: sbus_traffic sbus_traffic_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sbus_traffic
    ADD CONSTRAINT sbus_traffic_pkey PRIMARY KEY (id);


--
-- Name: scheduled_jobs scheduled_jobs_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.scheduled_jobs
    ADD CONSTRAINT scheduled_jobs_pkey PRIMARY KEY (id);


--
-- Name: scheduled_jobs scheduled_jobs_uuid_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.scheduled_jobs
    ADD CONSTRAINT scheduled_jobs_uuid_key UNIQUE (uuid);


--
-- Name: search_query_delen search_query_delen_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.search_query_delen
    ADD CONSTRAINT search_query_delen_pkey PRIMARY KEY (id);


--
-- Name: search_query search_query_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.search_query
    ADD CONSTRAINT search_query_pkey PRIMARY KEY (id);


--
-- Name: service_geojson service_geojson_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.service_geojson
    ADD CONSTRAINT service_geojson_pkey PRIMARY KEY (id);


--
-- Name: service_geojson_relationship service_geojson_relationship_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.service_geojson_relationship
    ADD CONSTRAINT service_geojson_relationship_pkey PRIMARY KEY (id);


--
-- Name: service_geojson_relationship service_geojson_relationship_related_uuid_service_geojson_i_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.service_geojson_relationship
    ADD CONSTRAINT service_geojson_relationship_related_uuid_service_geojson_i_key UNIQUE (related_uuid, service_geojson_id);


--
-- Name: service_geojson_relationship service_geojson_relationship_uuid_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.service_geojson_relationship
    ADD CONSTRAINT service_geojson_relationship_uuid_key UNIQUE (uuid);


--
-- Name: service_geojson service_geojson_uuid_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.service_geojson
    ADD CONSTRAINT service_geojson_uuid_key UNIQUE (uuid);


--
-- Name: session_invitation session_invitation_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.session_invitation
    ADD CONSTRAINT session_invitation_pkey PRIMARY KEY (id);


--
-- Name: settings settings_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.settings
    ADD CONSTRAINT settings_pkey PRIMARY KEY (id);


--
-- Name: bibliotheek_kenmerken_values sort_order_unique; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bibliotheek_kenmerken_values
    ADD CONSTRAINT sort_order_unique UNIQUE (sort_order, bibliotheek_kenmerken_id);


--
-- Name: subject_login_history subject_login_history_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.subject_login_history
    ADD CONSTRAINT subject_login_history_pkey PRIMARY KEY (id);


--
-- Name: subject subject_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.subject
    ADD CONSTRAINT subject_pkey PRIMARY KEY (id);


--
-- Name: subject subject_uuid_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.subject
    ADD CONSTRAINT subject_uuid_key UNIQUE (uuid);


--
-- Name: thread_message_attachment_derivative thread_message_attachment_derivative_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.thread_message_attachment_derivative
    ADD CONSTRAINT thread_message_attachment_derivative_pkey PRIMARY KEY (id);


--
-- Name: thread_message_attachment thread_message_attachment_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.thread_message_attachment
    ADD CONSTRAINT thread_message_attachment_pkey PRIMARY KEY (id);


--
-- Name: thread_message_attachment thread_message_attachment_uuid_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.thread_message_attachment
    ADD CONSTRAINT thread_message_attachment_uuid_key UNIQUE (uuid);


--
-- Name: thread_message_contact_moment thread_message_contact_moment_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.thread_message_contact_moment
    ADD CONSTRAINT thread_message_contact_moment_pkey PRIMARY KEY (id);


--
-- Name: thread_message_external thread_message_external_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.thread_message_external
    ADD CONSTRAINT thread_message_external_pkey PRIMARY KEY (id);


--
-- Name: thread_message_note thread_message_note_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.thread_message_note
    ADD CONSTRAINT thread_message_note_pkey PRIMARY KEY (id);


--
-- Name: thread_message thread_message_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.thread_message
    ADD CONSTRAINT thread_message_pkey PRIMARY KEY (id);


--
-- Name: thread_message thread_message_thread_message_contact_moment_id_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.thread_message
    ADD CONSTRAINT thread_message_thread_message_contact_moment_id_key UNIQUE (thread_message_contact_moment_id);


--
-- Name: thread_message thread_message_thread_message_external_id_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.thread_message
    ADD CONSTRAINT thread_message_thread_message_external_id_key UNIQUE (thread_message_external_id);


--
-- Name: thread_message thread_message_thread_message_note_id_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.thread_message
    ADD CONSTRAINT thread_message_thread_message_note_id_key UNIQUE (thread_message_note_id);


--
-- Name: thread_message thread_message_uuid_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.thread_message
    ADD CONSTRAINT thread_message_uuid_key UNIQUE (uuid);


--
-- Name: thread thread_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.thread
    ADD CONSTRAINT thread_pkey PRIMARY KEY (id);


--
-- Name: thread thread_uuid_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.thread
    ADD CONSTRAINT thread_uuid_key UNIQUE (uuid);


--
-- Name: transaction transaction_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.transaction
    ADD CONSTRAINT transaction_pkey PRIMARY KEY (id);


--
-- Name: transaction_record transaction_record_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.transaction_record
    ADD CONSTRAINT transaction_record_pkey PRIMARY KEY (id);


--
-- Name: transaction_record_to_object transaction_record_to_object_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.transaction_record_to_object
    ADD CONSTRAINT transaction_record_to_object_pkey PRIMARY KEY (id);


--
-- Name: user_app_lock user_app_lock_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.user_app_lock
    ADD CONSTRAINT user_app_lock_pkey PRIMARY KEY (uidnumber, type, type_id);


--
-- Name: user_entity user_entity_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.user_entity
    ADD CONSTRAINT user_entity_pkey PRIMARY KEY (id);


--
-- Name: woz_objects woz_objects_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.woz_objects
    ADD CONSTRAINT woz_objects_pkey PRIMARY KEY (id);


--
-- Name: zaak_authorisation zaak_authorisation_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak_authorisation
    ADD CONSTRAINT zaak_authorisation_pkey PRIMARY KEY (id);


--
-- Name: zaak_bag zaak_bag_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak_bag
    ADD CONSTRAINT zaak_bag_pkey PRIMARY KEY (id);


--
-- Name: zaak_betrokkenen zaak_betrokkenen_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak_betrokkenen
    ADD CONSTRAINT zaak_betrokkenen_pkey PRIMARY KEY (id);


--
-- Name: zaak zaak_duplicate_prevention_token_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak
    ADD CONSTRAINT zaak_duplicate_prevention_token_key UNIQUE (duplicate_prevention_token);


--
-- Name: zaak_kenmerk zaak_kenmerk_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak_kenmerk
    ADD CONSTRAINT zaak_kenmerk_pkey PRIMARY KEY (id);


--
-- Name: zaak_meta zaak_meta_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak_meta
    ADD CONSTRAINT zaak_meta_pkey PRIMARY KEY (id);


--
-- Name: zaak_meta zaak_meta_uniq_zaak_id; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak_meta
    ADD CONSTRAINT zaak_meta_uniq_zaak_id UNIQUE (zaak_id);


--
-- Name: zaak_onafgerond zaak_onafgerond_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak_onafgerond
    ADD CONSTRAINT zaak_onafgerond_pkey PRIMARY KEY (zaaktype_id, betrokkene);


--
-- Name: zaak zaak_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak
    ADD CONSTRAINT zaak_pkey PRIMARY KEY (id);


--
-- Name: zaak_subcase zaak_subcase_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak_subcase
    ADD CONSTRAINT zaak_subcase_pkey PRIMARY KEY (id);


--
-- Name: zaaktype_authorisation zaaktype_authorisation_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_authorisation
    ADD CONSTRAINT zaaktype_authorisation_pkey PRIMARY KEY (id);


--
-- Name: zaaktype_betrokkenen zaaktype_betrokkenen_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_betrokkenen
    ADD CONSTRAINT zaaktype_betrokkenen_pkey PRIMARY KEY (id);


--
-- Name: zaaktype_status_checklist_item zaaktype_checklist_item_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_status_checklist_item
    ADD CONSTRAINT zaaktype_checklist_item_pkey PRIMARY KEY (id);


--
-- Name: zaaktype_definitie zaaktype_definitie_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_definitie
    ADD CONSTRAINT zaaktype_definitie_pkey PRIMARY KEY (id);


--
-- Name: zaaktype_kenmerken zaaktype_kenmerken_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_kenmerken
    ADD CONSTRAINT zaaktype_kenmerken_pkey PRIMARY KEY (id);


--
-- Name: zaaktype_kenmerken zaaktype_kenmerken_uuid_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_kenmerken
    ADD CONSTRAINT zaaktype_kenmerken_uuid_key UNIQUE (uuid);


--
-- Name: zaaktype_node zaaktype_node_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_node
    ADD CONSTRAINT zaaktype_node_pkey PRIMARY KEY (id);


--
-- Name: zaaktype_notificatie zaaktype_notificatie_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_notificatie
    ADD CONSTRAINT zaaktype_notificatie_pkey PRIMARY KEY (id);


--
-- Name: zaaktype zaaktype_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype
    ADD CONSTRAINT zaaktype_pkey PRIMARY KEY (id);


--
-- Name: zaaktype_regel zaaktype_regel_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_regel
    ADD CONSTRAINT zaaktype_regel_pkey PRIMARY KEY (id);


--
-- Name: zaaktype_relatie zaaktype_relatie_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_relatie
    ADD CONSTRAINT zaaktype_relatie_pkey PRIMARY KEY (id);


--
-- Name: zaaktype_resultaten zaaktype_resultaten_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_resultaten
    ADD CONSTRAINT zaaktype_resultaten_pkey PRIMARY KEY (id);


--
-- Name: zaaktype_sjablonen zaaktype_sjablonen_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_sjablonen
    ADD CONSTRAINT zaaktype_sjablonen_pkey PRIMARY KEY (id);


--
-- Name: zaaktype_standaard_betrokkenen zaaktype_standaard_betrokkenen_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_standaard_betrokkenen
    ADD CONSTRAINT zaaktype_standaard_betrokkenen_pkey PRIMARY KEY (id);


--
-- Name: zaaktype_status zaaktype_status_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_status
    ADD CONSTRAINT zaaktype_status_pkey PRIMARY KEY (id);


--
-- Name: zaaktype zaaktype_uuid_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype
    ADD CONSTRAINT zaaktype_uuid_key UNIQUE (uuid);


--
-- Name: alternative_authentication_activation_link_subject_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX alternative_authentication_activation_link_subject_id_idx ON public.alternative_authentication_activation_link USING btree (subject_id);


--
-- Name: beheer_import_log_idx_import_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX beheer_import_log_idx_import_id ON public.beheer_import_log USING btree (import_id);


--
-- Name: bibliotheek_categorie_pid_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX bibliotheek_categorie_pid_idx ON public.bibliotheek_categorie USING btree (pid);


--
-- Name: bibliotheek_categorie_uuid_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX bibliotheek_categorie_uuid_idx ON public.bibliotheek_categorie USING btree (uuid);


--
-- Name: bibliotheek_category_unique_name; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX bibliotheek_category_unique_name ON public.bibliotheek_categorie USING btree (naam, COALESCE(pid, '-1'::integer));


--
-- Name: bibliotheek_kenmerken_bibliotheek_categorie_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX bibliotheek_kenmerken_bibliotheek_categorie_id_idx ON public.bibliotheek_kenmerken USING btree (bibliotheek_categorie_id);


--
-- Name: bibliotheek_kenmerken_deleted_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX bibliotheek_kenmerken_deleted_idx ON public.bibliotheek_kenmerken USING btree (deleted);


--
-- Name: bibliotheek_kenmerken_magic_string; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX bibliotheek_kenmerken_magic_string ON public.bibliotheek_kenmerken USING btree (magic_string) WHERE (deleted IS NULL);


--
-- Name: bibliotheek_kenmerken_magic_string_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX bibliotheek_kenmerken_magic_string_idx ON public.bibliotheek_kenmerken USING btree (magic_string);


--
-- Name: bibliotheek_kenmerken_magic_string_unique_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX bibliotheek_kenmerken_magic_string_unique_id ON public.bibliotheek_kenmerken USING btree (magic_string) WHERE (deleted IS NULL);


--
-- Name: bibliotheek_kenmerken_uuid_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX bibliotheek_kenmerken_uuid_idx ON public.bibliotheek_kenmerken USING btree (uuid);


--
-- Name: bibliotheek_kenmerken_value_type_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX bibliotheek_kenmerken_value_type_idx ON public.bibliotheek_kenmerken USING btree (value_type);


--
-- Name: bibliotheek_kenmerken_values_idx_bibliotheek_kenmerken_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX bibliotheek_kenmerken_values_idx_bibliotheek_kenmerken_id ON public.bibliotheek_kenmerken_values USING btree (bibliotheek_kenmerken_id);


--
-- Name: bibliotheek_notificaties_bibliotheek_categorie_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX bibliotheek_notificaties_bibliotheek_categorie_id_idx ON public.bibliotheek_notificaties USING btree (bibliotheek_categorie_id);


--
-- Name: bibliotheek_notificaties_deleted_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX bibliotheek_notificaties_deleted_idx ON public.bibliotheek_notificaties USING btree (deleted);


--
-- Name: bibliotheek_notificaties_uuid_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX bibliotheek_notificaties_uuid_idx ON public.bibliotheek_notificaties USING btree (uuid);


--
-- Name: bibliotheek_sjablonen_bibliotheek_categorie_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX bibliotheek_sjablonen_bibliotheek_categorie_id_idx ON public.bibliotheek_sjablonen USING btree (bibliotheek_categorie_id);


--
-- Name: bibliotheek_sjablonen_deleted_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX bibliotheek_sjablonen_deleted_idx ON public.bibliotheek_sjablonen USING btree (deleted);


--
-- Name: bibliotheek_sjablonen_interface_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX bibliotheek_sjablonen_interface_id_idx ON public.bibliotheek_sjablonen USING btree (interface_id);


--
-- Name: bibliotheek_sjablonen_template_uuid_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX bibliotheek_sjablonen_template_uuid_idx ON public.bibliotheek_sjablonen USING btree (template_external_name);


--
-- Name: bibliotheek_sjablonen_uuid_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX bibliotheek_sjablonen_uuid_idx ON public.bibliotheek_sjablonen USING btree (uuid);


--
-- Name: burgerservicenummer; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX burgerservicenummer ON public.natuurlijk_persoon USING btree (burgerservicenummer, authenticated) WHERE (deleted_on IS NULL);


--
-- Name: case_property_case_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX case_property_case_id_idx ON public.case_property USING btree (case_id);


--
-- Name: case_property_name_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX case_property_name_idx ON public.case_property USING btree (name);


--
-- Name: case_property_name_ref_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX case_property_name_ref_idx ON public.case_property USING btree (name, namespace, object_id, case_id);


--
-- Name: INDEX case_property_name_ref_idx; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON INDEX public.case_property_name_ref_idx IS 'enforce uniqueness of param<->object rows, multiple values not allowed';


--
-- Name: case_property_object_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX case_property_object_id_idx ON public.case_property USING btree (object_id);


--
-- Name: case_property_type_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX case_property_type_idx ON public.case_property USING btree (type);


--
-- Name: case_relation_case_id_a_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX case_relation_case_id_a_idx ON public.case_relation USING btree (case_id_a);


--
-- Name: case_relation_case_id_b_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX case_relation_case_id_b_idx ON public.case_relation USING btree (case_id_b);


--
-- Name: casetype_end_status_casetype_node_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX casetype_end_status_casetype_node_idx ON public.casetype_end_status USING btree (zaaktype_node_id);


--
-- Name: casetype_v1_reference_casetype_node_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX casetype_v1_reference_casetype_node_idx ON public.casetype_v1_reference USING btree (casetype_node_id);


--
-- Name: checklist_item_assignee_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX checklist_item_assignee_id_idx ON public.checklist_item USING btree (assignee_id);


--
-- Name: checklist_item_assignee_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX checklist_item_assignee_idx ON public.checklist_item USING btree (assignee_id);


--
-- Name: checklist_item_due_date_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX checklist_item_due_date_idx ON public.checklist_item USING btree (due_date);


--
-- Name: checklist_item_text_search_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX checklist_item_text_search_idx ON public.checklist_item USING gin (label public.gin_trgm_ops, description public.gin_trgm_ops);


--
-- Name: contact_data_betrokkene_type_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX contact_data_betrokkene_type_idx ON public.contact_data USING btree (betrokkene_type);


--
-- Name: contact_data_gegevens_magazijn_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX contact_data_gegevens_magazijn_id_idx ON public.contact_data USING btree (gegevens_magazijn_id);


--
-- Name: contact_relationship_unique_link_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX contact_relationship_unique_link_idx ON public.contact_relationship USING btree (LEAST((contact || contact_type), (relation || relation_type)), GREATEST((contact || contact_type), (relation || relation_type)));


--
-- Name: contact_relationship_uuid_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX contact_relationship_uuid_idx ON public.contact_relationship USING btree (uuid);


--
-- Name: contactmoment_uuid_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX contactmoment_uuid_idx ON public.contactmoment USING btree (uuid);


--
-- Name: custom_object_relationship_related_employee_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX custom_object_relationship_related_employee_id_idx ON public.custom_object_relationship USING btree (related_employee_id);


--
-- Name: custom_object_type_uuid_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX custom_object_type_uuid_idx ON public.custom_object_type USING btree (uuid);


--
-- Name: custom_object_type_version_date_deleted_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX custom_object_type_version_date_deleted_idx ON public.custom_object_type_version USING btree (date_deleted);


--
-- Name: custom_object_type_version_uuid_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX custom_object_type_version_uuid_idx ON public.custom_object_type_version USING btree (uuid);


--
-- Name: custom_object_uuid_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX custom_object_uuid_idx ON public.custom_object USING btree (uuid);


--
-- Name: custom_object_version_external_reference_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX custom_object_version_external_reference_idx ON public.custom_object_version USING gin (external_reference public.gin_trgm_ops);


--
-- Name: custom_object_version_subtitle_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX custom_object_version_subtitle_idx ON public.custom_object_version USING gin (subtitle public.gin_trgm_ops);


--
-- Name: custom_object_version_title_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX custom_object_version_title_idx ON public.custom_object_version USING gin (title public.gin_trgm_ops);


--
-- Name: custom_object_version_uuid_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX custom_object_version_uuid_idx ON public.custom_object_version USING btree (uuid);


--
-- Name: export_queue_subject_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX export_queue_subject_id_idx ON public.export_queue USING btree (subject_id);


--
-- Name: file_accepted_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX file_accepted_idx ON public.file USING btree (accepted);


--
-- Name: file_case_document_magic_string_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX file_case_document_magic_string_idx ON public.file_case_document USING btree (magic_string);


--
-- Name: file_case_document_unique_labels_per_file; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX file_case_document_unique_labels_per_file ON public.file_case_document USING btree (file_id, bibliotheek_kenmerken_id, case_id);


--
-- Name: file_case_documents_case_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX file_case_documents_case_idx ON public.file_case_document USING btree (case_id);


--
-- Name: file_case_documents_case_library_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX file_case_documents_case_library_idx ON public.file_case_document USING btree (case_id);


--
-- Name: file_case_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX file_case_id_idx ON public.file USING btree (case_id);


--
-- Name: file_date_deleted; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX file_date_deleted ON public.file USING btree (date_deleted);


--
-- Name: file_derivative_file_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX file_derivative_file_id_idx ON public.file_derivative USING btree (file_id);


--
-- Name: file_derivative_type_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX file_derivative_type_idx ON public.file_derivative USING btree (type);


--
-- Name: file_destroyed_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX file_destroyed_idx ON public.file USING btree (destroyed);


--
-- Name: file_search_index_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX file_search_index_idx ON public.file USING gin (search_index);


--
-- Name: filestore_storage_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX filestore_storage_idx ON public.filestore USING gin (storage_location);


--
-- Name: gegevensmagazijn_subjecten_subject_uuid_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX gegevensmagazijn_subjecten_subject_uuid_idx ON public.gegevensmagazijn_subjecten USING btree (subject_uuid);


--
-- Name: gm_natuurlijk_persoon_idx_adres_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX gm_natuurlijk_persoon_idx_adres_id ON public.gm_natuurlijk_persoon USING btree (adres_id);


--
-- Name: groups_uuid_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX groups_uuid_idx ON public.groups USING btree (uuid);


--
-- Name: idx_case_action_casetype_status_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_case_action_casetype_status_id ON public.case_action USING btree (casetype_status_id);


--
-- Name: idx_zaaktype_kenmerken_zaak_status_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_zaaktype_kenmerken_zaak_status_id ON public.zaaktype_kenmerken USING btree (zaak_status_id);


--
-- Name: idx_zaaktype_notificatie_zaak_status_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_zaaktype_notificatie_zaak_status_id ON public.zaaktype_notificatie USING btree (zaak_status_id);


--
-- Name: idx_zaaktype_regel_zaak_status_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_zaaktype_regel_zaak_status_id ON public.zaaktype_regel USING btree (zaak_status_id);


--
-- Name: idx_zaaktype_sjablonen_zaak_status_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_zaaktype_sjablonen_zaak_status_id ON public.zaaktype_sjablonen USING btree (zaak_status_id);


--
-- Name: interface_uuid_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX interface_uuid_idx ON public.interface USING btree (uuid);


--
-- Name: logging_component_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX logging_component_id_idx ON public.logging USING btree (component_id) WHERE (component_id IS NOT NULL);


--
-- Name: logging_component_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX logging_component_idx ON public.logging USING btree (component) WHERE (component IS NOT NULL);


--
-- Name: logging_created_by_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX logging_created_by_idx ON public.logging USING btree (created_by);


--
-- Name: logging_created_for_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX logging_created_for_idx ON public.logging USING btree (created_for);


--
-- Name: logging_created_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX logging_created_idx ON public.logging USING btree (created);


--
-- Name: logging_custom_object_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX logging_custom_object_idx ON public.logging USING btree ((((event_data)::json ->> 'custom_object_uuid'::text))) WHERE (((component)::text = 'custom_object'::text) AND (((event_data)::json ->> 'custom_object_uuid'::text) IS NOT NULL));


--
-- Name: logging_event_type_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX logging_event_type_idx ON public.logging USING btree (event_type);


--
-- Name: logging_restricted_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX logging_restricted_idx ON public.logging USING btree (restricted);


--
-- Name: logging_zaak_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX logging_zaak_id_idx ON public.logging USING btree (zaak_id);


--
-- Name: message_created_desc_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX message_created_desc_idx ON public.message USING btree (subject_id, is_archived, created DESC);


--
-- Name: message_subject_archived_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX message_subject_archived_idx ON public.message USING btree (subject_id, is_archived);


--
-- Name: message_subject_read_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX message_subject_read_idx ON public.message USING btree (subject_id, is_read);


--
-- Name: natuurlijk_persoon_burgerservicenummer; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX natuurlijk_persoon_burgerservicenummer ON public.natuurlijk_persoon USING btree (burgerservicenummer);


--
-- Name: natuurlijk_persoon_idx_adres_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX natuurlijk_persoon_idx_adres_id ON public.natuurlijk_persoon USING btree (adres_id);


--
-- Name: object_acl_entity_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX object_acl_entity_idx ON public.object_acl_entry USING btree (entity_type, entity_id);


--
-- Name: object_acl_entry_groupname_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX object_acl_entry_groupname_idx ON public.object_acl_entry USING btree (groupname);


--
-- Name: object_acl_entry_permission_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX object_acl_entry_permission_idx ON public.object_acl_entry USING btree (capability);


--
-- Name: object_acl_entry_unique_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX object_acl_entry_unique_idx ON public.object_acl_entry USING btree (object_uuid, entity_type, entity_id, capability, scope, groupname);


--
-- Name: object_acl_object_uuid_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX object_acl_object_uuid_idx ON public.object_acl_entry USING btree (object_uuid);


--
-- Name: object_bibliotheek_entry_bibliotheek_categorie_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX object_bibliotheek_entry_bibliotheek_categorie_id_idx ON public.object_bibliotheek_entry USING btree (bibliotheek_categorie_id);


--
-- Name: object_data_assignee_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX object_data_assignee_idx ON public.object_data USING btree (((index_hstore OPERATOR(public.->) 'case.assignee'::text)));


--
-- Name: object_data_case_assignee_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX object_data_case_assignee_id_idx ON public.object_data USING btree ((((index_hstore OPERATOR(public.->) 'case.assignee.id'::text))::numeric)) WHERE ((object_class = 'case'::text) AND (invalid IS FALSE));


--
-- Name: object_data_case_casetype_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX object_data_case_casetype_id_idx ON public.object_data USING btree ((((index_hstore OPERATOR(public.->) 'case.casetype.id'::text))::numeric)) WHERE ((object_class = 'case'::text) AND (invalid IS FALSE));


--
-- Name: object_data_case_date_current_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX object_data_case_date_current_idx ON public.object_data USING btree (((public.hstore_to_timestamp(((index_hstore OPERATOR(public.->) 'case.date_current'::text))::character varying))::date));


--
-- Name: object_data_case_number_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX object_data_case_number_idx ON public.object_data USING btree (((index_hstore OPERATOR(public.->) 'case.number'::text)));


--
-- Name: object_data_case_phase_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX object_data_case_phase_idx ON public.object_data USING btree (((index_hstore OPERATOR(public.->) 'case.phase'::text)));


--
-- Name: object_data_case_requestor_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX object_data_case_requestor_id_idx ON public.object_data USING btree (((index_hstore OPERATOR(public.->) 'case.requestor.id'::text))) WHERE ((object_class = 'case'::text) AND (invalid IS FALSE));


--
-- Name: object_data_case_route_role_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX object_data_case_route_role_idx ON public.object_data USING btree ((((index_hstore OPERATOR(public.->) 'case.route_ou'::text))::numeric), (((index_hstore OPERATOR(public.->) 'case.route_ou'::text))::numeric)) WHERE ((object_class = 'case'::text) AND (invalid IS FALSE));


--
-- Name: object_data_case_status_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX object_data_case_status_id_idx ON public.object_data USING btree (((index_hstore OPERATOR(public.->) 'case.status'::text))) WHERE ((object_class = 'case'::text) AND (invalid IS FALSE));


--
-- Name: object_data_casetype_description_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX object_data_casetype_description_idx ON public.object_data USING btree (((index_hstore OPERATOR(public.->) 'casetype.description'::text)));


--
-- Name: object_data_casetype_name_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX object_data_casetype_name_idx ON public.object_data USING btree (((index_hstore OPERATOR(public.->) 'casetype.name'::text)));


--
-- Name: object_data_completion_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX object_data_completion_idx ON public.object_data USING btree (((public.hstore_to_timestamp(((index_hstore OPERATOR(public.->) 'case.date_of_completion'::text))::character varying))::date)) WHERE ((object_class = 'case'::text) AND (invalid IS FALSE));


--
-- Name: object_data_hstore_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX object_data_hstore_idx ON public.object_data USING gin (index_hstore);


--
-- Name: object_data_object_class_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX object_data_object_class_idx ON public.object_data USING btree (object_class);


--
-- Name: object_data_object_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX object_data_object_id_idx ON public.object_data USING btree (object_id);


--
-- Name: object_data_recipient_name_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX object_data_recipient_name_idx ON public.object_data USING btree (((index_hstore OPERATOR(public.->) 'recipient.full_name'::text)));


--
-- Name: object_data_regdate_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX object_data_regdate_idx ON public.object_data USING btree (((public.hstore_to_timestamp(((index_hstore OPERATOR(public.->) 'case.date_of_registration'::text))::character varying))::date)) WHERE ((object_class = 'case'::text) AND (invalid IS FALSE));


--
-- Name: object_data_status_casetype_assignee_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX object_data_status_casetype_assignee_idx ON public.object_data USING btree (((index_hstore OPERATOR(public.->) 'case.status'::text)), (((index_hstore OPERATOR(public.->) 'case.casetype.id'::text))::numeric), (((index_hstore OPERATOR(public.->) 'case.assignee.id'::text))::numeric)) WHERE ((object_class = 'case'::text) AND (invalid IS FALSE));


--
-- Name: object_data_target_date_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX object_data_target_date_idx ON public.object_data USING btree (((public.hstore_to_timestamp(((index_hstore OPERATOR(public.->) 'case.case.date_target'::text))::character varying))::date)) WHERE ((object_class = 'case'::text) AND (invalid IS FALSE));


--
-- Name: object_mutation_subject_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX object_mutation_subject_id_idx ON public.object_mutation USING btree (subject_id);


--
-- Name: object_relation_name_and_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX object_relation_name_and_id_idx ON public.object_relation USING btree (name, object_id);


--
-- Name: object_relation_name_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX object_relation_name_idx ON public.object_relation USING btree (name);


--
-- Name: object_text_vector_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX object_text_vector_idx ON public.object_data USING gin (text_vector);


--
-- Name: queue_20210101_expr_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX queue_20210101_expr_idx ON public.queue_20210101 USING btree (((((data)::jsonb -> 'create-args'::text) ->> 'duplicate_prevention_token'::text))) WHERE (type = 'create_case_form'::text);


--
-- Name: queue_20210201_expr_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX queue_20210201_expr_idx ON public.queue_20210201 USING btree (((((data)::jsonb -> 'create-args'::text) ->> 'duplicate_prevention_token'::text))) WHERE (type = 'create_case_form'::text);


--
-- Name: queue_20210301_expr_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX queue_20210301_expr_idx ON public.queue_20210301 USING btree (((((data)::jsonb -> 'create-args'::text) ->> 'duplicate_prevention_token'::text))) WHERE (type = 'create_case_form'::text);


--
-- Name: queue_20210401_expr_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX queue_20210401_expr_idx ON public.queue_20210401 USING btree (((((data)::jsonb -> 'create-args'::text) ->> 'duplicate_prevention_token'::text))) WHERE (type = 'create_case_form'::text);


--
-- Name: queue_20210501_expr_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX queue_20210501_expr_idx ON public.queue_20210501 USING btree (((((data)::jsonb -> 'create-args'::text) ->> 'duplicate_prevention_token'::text))) WHERE (type = 'create_case_form'::text);


--
-- Name: queue_20210601_expr_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX queue_20210601_expr_idx ON public.queue_20210601 USING btree (((((data)::jsonb -> 'create-args'::text) ->> 'duplicate_prevention_token'::text))) WHERE (type = 'create_case_form'::text);


--
-- Name: queue_20210701_expr_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX queue_20210701_expr_idx ON public.queue_20210701 USING btree (((((data)::jsonb -> 'create-args'::text) ->> 'duplicate_prevention_token'::text))) WHERE (type = 'create_case_form'::text);


--
-- Name: queue_20210801_expr_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX queue_20210801_expr_idx ON public.queue_20210801 USING btree (((((data)::jsonb -> 'create-args'::text) ->> 'duplicate_prevention_token'::text))) WHERE (type = 'create_case_form'::text);


--
-- Name: queue_20210901_expr_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX queue_20210901_expr_idx ON public.queue_20210901 USING btree (((((data)::jsonb -> 'create-args'::text) ->> 'duplicate_prevention_token'::text))) WHERE (type = 'create_case_form'::text);


--
-- Name: queue_20211001_expr_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX queue_20211001_expr_idx ON public.queue_20211001 USING btree (((((data)::jsonb -> 'create-args'::text) ->> 'duplicate_prevention_token'::text))) WHERE (type = 'create_case_form'::text);


--
-- Name: queue_20211101_expr_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX queue_20211101_expr_idx ON public.queue_20211101 USING btree (((((data)::jsonb -> 'create-args'::text) ->> 'duplicate_prevention_token'::text))) WHERE (type = 'create_case_form'::text);


--
-- Name: queue_20211201_expr_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX queue_20211201_expr_idx ON public.queue_20211201 USING btree (((((data)::jsonb -> 'create-args'::text) ->> 'duplicate_prevention_token'::text))) WHERE (type = 'create_case_form'::text);


--
-- Name: queue_date_created_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX queue_date_created_idx ON public.queue USING btree (date_created);


--
-- Name: queue_date_finished_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX queue_date_finished_idx ON public.queue USING btree (date_finished);


--
-- Name: queue_object_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX queue_object_id_idx ON public.queue USING btree (object_id);


--
-- Name: queue_priority_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX queue_priority_idx ON public.queue USING btree (priority);


--
-- Name: queue_status_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX queue_status_idx ON public.queue USING btree (status);


--
-- Name: roles_uuid_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX roles_uuid_idx ON public.roles USING btree (uuid);


--
-- Name: scheduled_jobs_case_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX scheduled_jobs_case_id_idx ON public.scheduled_jobs USING btree (case_id);


--
-- Name: scheduled_jobs_task_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX scheduled_jobs_task_idx ON public.scheduled_jobs USING btree (task);


--
-- Name: session_invitation_subject_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX session_invitation_subject_id_idx ON public.session_invitation USING btree (subject_id);


--
-- Name: subject_id_username_covering_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX subject_id_username_covering_idx ON public.subject USING btree (id, username);


--
-- Name: subject_login_history_subject_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX subject_login_history_subject_id_idx ON public.subject_login_history USING btree (subject_id);


--
-- Name: subject_position_matrix_group_role_subject_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX subject_position_matrix_group_role_subject_idx ON public.subject_position_matrix USING btree (subject_id, role_id, group_id);


--
-- Name: subject_position_matrix_position_subject_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX subject_position_matrix_position_subject_idx ON public.subject_position_matrix USING btree (subject_id, "position");


--
-- Name: thread_case_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX thread_case_id_idx ON public.thread USING btree (case_id);


--
-- Name: thread_contact_uuid_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX thread_contact_uuid_idx ON public.thread USING btree (contact_uuid);


--
-- Name: thread_message_contact_moment_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX thread_message_contact_moment_id_idx ON public.thread_message USING btree (thread_message_contact_moment_id);


--
-- Name: thread_message_external_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX thread_message_external_id_idx ON public.thread_message USING btree (thread_message_external_id);


--
-- Name: thread_message_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX thread_message_id_idx ON public.thread_message USING btree (thread_id);


--
-- Name: thread_message_note_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX thread_message_note_id_idx ON public.thread_message USING btree (thread_message_note_id);


--
-- Name: thread_uuid_reply_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX thread_uuid_reply_idx ON public.thread USING btree ((((('x'::text || translate("right"((uuid)::text, 12), '-'::text, ''::text)))::bit(48))::bigint));


--
-- Name: tmp_queue_no_parent; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX tmp_queue_no_parent ON public.queue USING btree (id) WHERE (parent_id IS NULL);


--
-- Name: tmp_queue_parent; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX tmp_queue_parent ON public.queue USING btree (id) WHERE (parent_id IS NOT NULL);


--
-- Name: transaction_date_deleted_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX transaction_date_deleted_idx ON public.transaction USING btree (date_deleted);


--
-- Name: transaction_date_next_retry_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX transaction_date_next_retry_idx ON public.transaction USING btree (date_next_retry);


--
-- Name: transaction_error_fatal_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX transaction_error_fatal_idx ON public.transaction USING btree (error_fatal);


--
-- Name: transaction_external_transaction_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX transaction_external_transaction_id_idx ON public.transaction USING btree (external_transaction_id);


--
-- Name: transaction_interface_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX transaction_interface_id_idx ON public.transaction USING btree (interface_id);


--
-- Name: transaction_record_transaction_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX transaction_record_transaction_id_idx ON public.transaction_record USING btree (transaction_id);


--
-- Name: transaction_record_uuid_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX transaction_record_uuid_idx ON public.transaction_record USING btree (uuid);


--
-- Name: transaction_text_vector_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX transaction_text_vector_idx ON public.transaction USING gist (text_vector);


--
-- Name: transaction_uuid_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX transaction_uuid_idx ON public.transaction USING btree (uuid);


--
-- Name: user_entity_source_interface_id_source_identifier_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX user_entity_source_interface_id_source_identifier_idx ON public.user_entity USING btree (source_interface_id, source_identifier);


--
-- Name: user_entity_subject_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX user_entity_subject_id_idx ON public.user_entity USING btree (subject_id);


--
-- Name: zaak_aanvrager_covering_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaak_aanvrager_covering_idx ON public.zaak USING btree (aanvrager_type, aanvrager_gm_id, id, uuid);


--
-- Name: zaak_aanvrager_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaak_aanvrager_idx ON public.zaak USING btree (aanvrager);


--
-- Name: zaak_authorisation_all_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaak_authorisation_all_idx ON public.zaak_authorisation USING btree (entity_type, capability, zaak_id);


--
-- Name: zaak_authorisation_all_without_case_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaak_authorisation_all_without_case_idx ON public.zaak_authorisation USING btree (entity_type, capability);


--
-- Name: zaak_authorisation_entity_type_capability_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaak_authorisation_entity_type_capability_idx ON public.zaak_authorisation USING btree (entity_type, capability) WHERE (entity_type = 'position'::text);


--
-- Name: zaak_authorisation_entity_type_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaak_authorisation_entity_type_idx ON public.zaak_authorisation USING btree (entity_type);


--
-- Name: zaak_bag_type_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaak_bag_type_id_idx ON public.zaak_bag USING btree (bag_type, bag_id);


--
-- Name: zaak_behandelaar_covering_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaak_behandelaar_covering_idx ON public.zaak USING btree (behandelaar_gm_id, id, uuid);


--
-- Name: zaak_behandelaar_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaak_behandelaar_idx ON public.zaak USING btree (behandelaar);


--
-- Name: zaak_betrokkenen_betrokkene_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaak_betrokkenen_betrokkene_id_idx ON public.zaak_betrokkenen USING btree (betrokkene_id);


--
-- Name: zaak_betrokkenen_betrokkene_type_betrokkene_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaak_betrokkenen_betrokkene_type_betrokkene_id_idx ON public.zaak_betrokkenen USING btree (betrokkene_type, betrokkene_id);


--
-- Name: zaak_betrokkenen_cache_uuid; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaak_betrokkenen_cache_uuid ON public.zaak USING gin (betrokkenen_cache);


--
-- Name: zaak_betrokkenen_deleted_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaak_betrokkenen_deleted_idx ON public.zaak_betrokkenen USING btree (deleted);


--
-- Name: zaak_betrokkenen_gegevens_magazijn_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaak_betrokkenen_gegevens_magazijn_index ON public.zaak_betrokkenen USING btree (gegevens_magazijn_id);


--
-- Name: zaak_betrokkenen_magic_string_prefix_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaak_betrokkenen_magic_string_prefix_idx ON public.zaak_betrokkenen USING btree (magic_string_prefix);


--
-- Name: zaak_betrokkenen_subject_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaak_betrokkenen_subject_id_idx ON public.zaak_betrokkenen USING btree (subject_id);


--
-- Name: zaak_betrokkenen_zaak_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaak_betrokkenen_zaak_id_idx ON public.zaak_betrokkenen USING btree (zaak_id);


--
-- Name: zaak_confidential_covering_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaak_confidential_covering_idx ON public.zaak USING btree (confidential, zaaktype_id, id);


--
-- Name: zaak_confidential_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaak_confidential_idx ON public.zaak USING btree (confidential);


--
-- Name: zaak_coordinator_covering_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaak_coordinator_covering_idx ON public.zaak USING btree (coordinator_gm_id, id, uuid);


--
-- Name: zaak_coordinator_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaak_coordinator_idx ON public.zaak USING btree (coordinator);


--
-- Name: zaak_id_confidential_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaak_id_confidential_idx ON public.zaak USING btree (id, confidential);


--
-- Name: zaak_kenmerk_bibliotheek_kenmerken_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX zaak_kenmerk_bibliotheek_kenmerken_id ON public.zaak_kenmerk USING btree (zaak_id, bibliotheek_kenmerken_id);


--
-- Name: zaak_kenmerk_zaak_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaak_kenmerk_zaak_id_idx ON public.zaak_kenmerk USING btree (zaak_id);


--
-- Name: zaak_last_modified; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaak_last_modified ON public.zaak USING btree (last_modified);


--
-- Name: zaak_meta_index_hstore_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaak_meta_index_hstore_idx ON public.zaak_meta USING gin (index_hstore) WITH (gin_pending_list_limit='128');


--
-- Name: zaak_meta_text_vector_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaak_meta_text_vector_idx ON public.zaak_meta USING gin (text_vector) WITH (gin_pending_list_limit='128');


--
-- Name: zaak_meta_unread_communications_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaak_meta_unread_communications_idx ON public.zaak_meta USING btree (unread_communication_count);


--
-- Name: zaak_onderwerp_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaak_onderwerp_idx ON public.zaak USING gin (onderwerp public.gin_trgm_ops);


--
-- Name: zaak_open_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaak_open_idx ON public.zaak USING btree (id) WHERE (deleted IS NULL);


--
-- Name: zaak_pid_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaak_pid_idx ON public.zaak USING btree (pid);


--
-- Name: zaak_uuid_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX zaak_uuid_idx ON public.zaak USING btree (uuid) WHERE (uuid IS NOT NULL);


--
-- Name: zaak_zaaktype_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaak_zaaktype_id_idx ON public.zaak USING btree (zaaktype_id);


--
-- Name: zaaktype_authorisation_idx_zaaktype_node_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaaktype_authorisation_idx_zaaktype_node_id ON public.zaaktype_authorisation USING btree (zaaktype_node_id);


--
-- Name: zaaktype_authorisation_recht_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaaktype_authorisation_recht_idx ON public.zaaktype_authorisation USING btree (recht);


--
-- Name: zaaktype_betrokkenen_idx_zaaktype_node_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaaktype_betrokkenen_idx_zaaktype_node_id ON public.zaaktype_betrokkenen USING btree (zaaktype_node_id);


--
-- Name: zaaktype_bibliotheek_categorie_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaaktype_bibliotheek_categorie_id_idx ON public.zaaktype USING btree (bibliotheek_categorie_id);


--
-- Name: zaaktype_deleted_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaaktype_deleted_idx ON public.zaaktype USING btree (deleted);


--
-- Name: zaaktype_document_kenmerken_map_by_case_document_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaaktype_document_kenmerken_map_by_case_document_id ON public.zaaktype_document_kenmerken_map USING btree (zaaktype_node_id, case_document_id);


--
-- Name: zaaktype_document_kenmerken_map_by_case_document_uuid; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaaktype_document_kenmerken_map_by_case_document_uuid ON public.zaaktype_document_kenmerken_map USING btree (case_document_uuid);


--
-- Name: zaaktype_document_kenmerken_map_by_casetype_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaaktype_document_kenmerken_map_by_casetype_idx ON public.zaaktype_document_kenmerken_map USING btree (zaaktype_node_id, bibliotheek_kenmerken_id);


--
-- Name: zaaktype_idx_zaaktype_node_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaaktype_idx_zaaktype_node_id ON public.zaaktype USING btree (zaaktype_node_id);


--
-- Name: zaaktype_kenmerken_bag_zaakadres_or_map_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaaktype_kenmerken_bag_zaakadres_or_map_idx ON public.zaaktype_kenmerken USING btree (bag_zaakadres) WHERE ((bag_zaakadres = 1) OR ((properties)::jsonb @> '{"map_case_location": "1"}'::jsonb));


--
-- Name: zaaktype_kenmerken_zaaktype_node_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaaktype_kenmerken_zaaktype_node_idx ON public.zaaktype_kenmerken USING btree (zaaktype_node_id);


--
-- Name: zaaktype_node_idx_zaaktype_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaaktype_node_idx_zaaktype_id ON public.zaaktype_node USING btree (zaaktype_id);


--
-- Name: zaaktype_node_titel_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaaktype_node_titel_idx ON public.zaaktype_node USING gin (titel public.gin_trgm_ops);


--
-- Name: zaaktype_node_uuid_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX zaaktype_node_uuid_idx ON public.zaaktype_node USING btree (uuid);


--
-- Name: zaaktype_node_zaaktype_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaaktype_node_zaaktype_id_idx ON public.zaaktype_node USING btree (zaaktype_id);


--
-- Name: zaaktype_relatie_idx_relatie_zaaktype_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaaktype_relatie_idx_relatie_zaaktype_id ON public.zaaktype_relatie USING btree (relatie_zaaktype_id);


--
-- Name: zaaktype_relatie_idx_zaaktype_node_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaaktype_relatie_idx_zaaktype_node_id ON public.zaaktype_relatie USING btree (zaaktype_node_id);


--
-- Name: zaaktype_relatie_idx_zaaktype_status_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaaktype_relatie_idx_zaaktype_status_id ON public.zaaktype_relatie USING btree (zaaktype_status_id);


--
-- Name: zaaktype_resultaten_idx_zaaktype_node_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaaktype_resultaten_idx_zaaktype_node_id ON public.zaaktype_resultaten USING btree (zaaktype_node_id);


--
-- Name: zaaktype_resultaten_idx_zaaktype_status_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaaktype_resultaten_idx_zaaktype_status_id ON public.zaaktype_resultaten USING btree (zaaktype_status_id);


--
-- Name: zaaktype_status_idx_zaaktype_node_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaaktype_status_idx_zaaktype_node_id ON public.zaaktype_status USING btree (zaaktype_node_id);


--
-- Name: zaaktype_status_zaaktype_node_status_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaaktype_status_zaaktype_node_status_idx ON public.zaaktype_status USING btree (zaaktype_node_id, status);


--
-- Name: zaaktype_zaaktype_node_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaaktype_zaaktype_node_id_idx ON public.zaaktype USING btree (zaaktype_node_id);


--
-- Name: case_attributes _RETURN; Type: RULE; Schema: public; Owner: -
--

CREATE OR REPLACE VIEW public.case_attributes AS
 SELECT z.id AS case_id,
    COALESCE(zk.value, '{}'::text[]) AS value,
    bk.magic_string,
    bk.id AS library_id,
    bk.value_type,
    bk.type_multiple AS mvp
   FROM (((public.zaak z
     JOIN public.zaaktype_kenmerken ztk ON ((z.zaaktype_node_id = ztk.zaaktype_node_id)))
     JOIN public.bibliotheek_kenmerken bk ON (((bk.id = ztk.bibliotheek_kenmerken_id) AND (bk.value_type <> ALL (ARRAY['file'::text, 'appointment'::text])))))
     LEFT JOIN public.zaak_kenmerk zk ON (((z.id = zk.zaak_id) AND (zk.bibliotheek_kenmerken_id = ztk.bibliotheek_kenmerken_id))))
  GROUP BY z.id, COALESCE(zk.value, '{}'::text[]), bk.magic_string, bk.id, bk.value_type;


--
-- Name: bedrijf_authenticatie bedrijf_authenticatie_insert_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER bedrijf_authenticatie_insert_trigger BEFORE INSERT ON public.bedrijf_authenticatie FOR EACH ROW EXECUTE FUNCTION public.insert_timestamps();


--
-- Name: bedrijf_authenticatie bedrijf_authenticatie_update_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER bedrijf_authenticatie_update_trigger BEFORE UPDATE ON public.bedrijf_authenticatie FOR EACH ROW EXECUTE FUNCTION public.update_timestamps();


--
-- Name: beheer_import beheer_import_insert_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER beheer_import_insert_trigger BEFORE INSERT ON public.beheer_import FOR EACH ROW EXECUTE FUNCTION public.insert_timestamps();


--
-- Name: beheer_import_log beheer_import_log_insert_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER beheer_import_log_insert_trigger BEFORE INSERT ON public.beheer_import_log FOR EACH ROW EXECUTE FUNCTION public.insert_timestamps();


--
-- Name: beheer_import_log beheer_import_log_update_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER beheer_import_log_update_trigger BEFORE UPDATE ON public.beheer_import_log FOR EACH ROW EXECUTE FUNCTION public.update_timestamps();


--
-- Name: beheer_import beheer_import_update_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER beheer_import_update_trigger BEFORE UPDATE ON public.beheer_import FOR EACH ROW EXECUTE FUNCTION public.update_timestamps();


--
-- Name: betrokkene_notes betrokkene_notes_insert_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER betrokkene_notes_insert_trigger BEFORE INSERT ON public.betrokkene_notes FOR EACH ROW EXECUTE FUNCTION public.insert_timestamps();


--
-- Name: betrokkene_notes betrokkene_notes_update_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER betrokkene_notes_update_trigger BEFORE UPDATE ON public.betrokkene_notes FOR EACH ROW EXECUTE FUNCTION public.update_timestamps();


--
-- Name: bibliotheek_categorie bibliotheek_categorie_insert_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER bibliotheek_categorie_insert_trigger BEFORE INSERT ON public.bibliotheek_categorie FOR EACH ROW EXECUTE FUNCTION public.insert_timestamps();


--
-- Name: bibliotheek_categorie bibliotheek_categorie_update_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER bibliotheek_categorie_update_trigger BEFORE UPDATE ON public.bibliotheek_categorie FOR EACH ROW EXECUTE FUNCTION public.update_timestamps();


--
-- Name: bibliotheek_kenmerken bibliotheek_kenmerken_insert_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER bibliotheek_kenmerken_insert_trigger BEFORE INSERT ON public.bibliotheek_kenmerken FOR EACH ROW EXECUTE FUNCTION public.insert_timestamps();


--
-- Name: bibliotheek_kenmerken bibliotheek_kenmerken_update_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER bibliotheek_kenmerken_update_trigger BEFORE UPDATE ON public.bibliotheek_kenmerken FOR EACH ROW EXECUTE FUNCTION public.update_timestamps();


--
-- Name: bibliotheek_notificaties bibliotheek_notificaties_insert_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER bibliotheek_notificaties_insert_trigger BEFORE INSERT ON public.bibliotheek_notificaties FOR EACH ROW EXECUTE FUNCTION public.insert_timestamps();


--
-- Name: bibliotheek_notificaties bibliotheek_notificaties_update_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER bibliotheek_notificaties_update_trigger BEFORE UPDATE ON public.bibliotheek_notificaties FOR EACH ROW EXECUTE FUNCTION public.update_timestamps();


--
-- Name: bibliotheek_sjablonen bibliotheek_sjablonen_insert_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER bibliotheek_sjablonen_insert_trigger BEFORE INSERT ON public.bibliotheek_sjablonen FOR EACH ROW EXECUTE FUNCTION public.insert_timestamps();


--
-- Name: bibliotheek_sjablonen bibliotheek_sjablonen_update_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER bibliotheek_sjablonen_update_trigger BEFORE UPDATE ON public.bibliotheek_sjablonen FOR EACH ROW EXECUTE FUNCTION public.update_timestamps();


--
-- Name: queue_20210101 check_queue_object_id_constraint; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER check_queue_object_id_constraint BEFORE INSERT ON public.queue_20210101 FOR EACH ROW EXECUTE FUNCTION public.check_queue_object_id_constraint();


--
-- Name: queue_20210201 check_queue_object_id_constraint; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER check_queue_object_id_constraint BEFORE INSERT ON public.queue_20210201 FOR EACH ROW EXECUTE FUNCTION public.check_queue_object_id_constraint();


--
-- Name: queue_20210301 check_queue_object_id_constraint; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER check_queue_object_id_constraint BEFORE INSERT ON public.queue_20210301 FOR EACH ROW EXECUTE FUNCTION public.check_queue_object_id_constraint();


--
-- Name: queue_20210401 check_queue_object_id_constraint; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER check_queue_object_id_constraint BEFORE INSERT ON public.queue_20210401 FOR EACH ROW EXECUTE FUNCTION public.check_queue_object_id_constraint();


--
-- Name: queue_20210501 check_queue_object_id_constraint; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER check_queue_object_id_constraint BEFORE INSERT ON public.queue_20210501 FOR EACH ROW EXECUTE FUNCTION public.check_queue_object_id_constraint();


--
-- Name: queue_20210601 check_queue_object_id_constraint; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER check_queue_object_id_constraint BEFORE INSERT ON public.queue_20210601 FOR EACH ROW EXECUTE FUNCTION public.check_queue_object_id_constraint();


--
-- Name: queue_20210701 check_queue_object_id_constraint; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER check_queue_object_id_constraint BEFORE INSERT ON public.queue_20210701 FOR EACH ROW EXECUTE FUNCTION public.check_queue_object_id_constraint();


--
-- Name: queue_20210801 check_queue_object_id_constraint; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER check_queue_object_id_constraint BEFORE INSERT ON public.queue_20210801 FOR EACH ROW EXECUTE FUNCTION public.check_queue_object_id_constraint();


--
-- Name: queue_20210901 check_queue_object_id_constraint; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER check_queue_object_id_constraint BEFORE INSERT ON public.queue_20210901 FOR EACH ROW EXECUTE FUNCTION public.check_queue_object_id_constraint();


--
-- Name: queue_20211001 check_queue_object_id_constraint; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER check_queue_object_id_constraint BEFORE INSERT ON public.queue_20211001 FOR EACH ROW EXECUTE FUNCTION public.check_queue_object_id_constraint();


--
-- Name: queue_20211101 check_queue_object_id_constraint; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER check_queue_object_id_constraint BEFORE INSERT ON public.queue_20211101 FOR EACH ROW EXECUTE FUNCTION public.check_queue_object_id_constraint();


--
-- Name: queue_20211201 check_queue_object_id_constraint; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER check_queue_object_id_constraint BEFORE INSERT ON public.queue_20211201 FOR EACH ROW EXECUTE FUNCTION public.check_queue_object_id_constraint();


--
-- Name: queue_20210101 check_queue_parent_id_constraint; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER check_queue_parent_id_constraint BEFORE INSERT ON public.queue_20210101 FOR EACH ROW EXECUTE FUNCTION public.check_queue_parent_id_constraint();


--
-- Name: queue_20210201 check_queue_parent_id_constraint; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER check_queue_parent_id_constraint BEFORE INSERT ON public.queue_20210201 FOR EACH ROW EXECUTE FUNCTION public.check_queue_parent_id_constraint();


--
-- Name: queue_20210301 check_queue_parent_id_constraint; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER check_queue_parent_id_constraint BEFORE INSERT ON public.queue_20210301 FOR EACH ROW EXECUTE FUNCTION public.check_queue_parent_id_constraint();


--
-- Name: queue_20210401 check_queue_parent_id_constraint; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER check_queue_parent_id_constraint BEFORE INSERT ON public.queue_20210401 FOR EACH ROW EXECUTE FUNCTION public.check_queue_parent_id_constraint();


--
-- Name: queue_20210501 check_queue_parent_id_constraint; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER check_queue_parent_id_constraint BEFORE INSERT ON public.queue_20210501 FOR EACH ROW EXECUTE FUNCTION public.check_queue_parent_id_constraint();


--
-- Name: queue_20210601 check_queue_parent_id_constraint; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER check_queue_parent_id_constraint BEFORE INSERT ON public.queue_20210601 FOR EACH ROW EXECUTE FUNCTION public.check_queue_parent_id_constraint();


--
-- Name: queue_20210701 check_queue_parent_id_constraint; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER check_queue_parent_id_constraint BEFORE INSERT ON public.queue_20210701 FOR EACH ROW EXECUTE FUNCTION public.check_queue_parent_id_constraint();


--
-- Name: queue_20210801 check_queue_parent_id_constraint; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER check_queue_parent_id_constraint BEFORE INSERT ON public.queue_20210801 FOR EACH ROW EXECUTE FUNCTION public.check_queue_parent_id_constraint();


--
-- Name: queue_20210901 check_queue_parent_id_constraint; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER check_queue_parent_id_constraint BEFORE INSERT ON public.queue_20210901 FOR EACH ROW EXECUTE FUNCTION public.check_queue_parent_id_constraint();


--
-- Name: queue_20211001 check_queue_parent_id_constraint; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER check_queue_parent_id_constraint BEFORE INSERT ON public.queue_20211001 FOR EACH ROW EXECUTE FUNCTION public.check_queue_parent_id_constraint();


--
-- Name: queue_20211101 check_queue_parent_id_constraint; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER check_queue_parent_id_constraint BEFORE INSERT ON public.queue_20211101 FOR EACH ROW EXECUTE FUNCTION public.check_queue_parent_id_constraint();


--
-- Name: queue_20211201 check_queue_parent_id_constraint; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER check_queue_parent_id_constraint BEFORE INSERT ON public.queue_20211201 FOR EACH ROW EXECUTE FUNCTION public.check_queue_parent_id_constraint();


--
-- Name: contact_data contact_data_insert_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER contact_data_insert_trigger BEFORE INSERT ON public.contact_data FOR EACH ROW EXECUTE FUNCTION public.insert_timestamps();


--
-- Name: contact_data contact_data_update_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER contact_data_update_trigger BEFORE UPDATE ON public.contact_data FOR EACH ROW EXECUTE FUNCTION public.update_timestamps();


--
-- Name: custom_object_type_version custom_object_type_version_update_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER custom_object_type_version_update_trigger BEFORE UPDATE ON public.custom_object_type_version FOR EACH ROW EXECUTE FUNCTION public.update_timestamps();


--
-- Name: custom_object_version custom_object_version_update_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER custom_object_version_update_trigger BEFORE UPDATE ON public.custom_object_version FOR EACH ROW EXECUTE FUNCTION public.update_timestamps();


--
-- Name: file file_insert_timestamp_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER file_insert_timestamp_trigger BEFORE INSERT ON public.file FOR EACH ROW EXECUTE FUNCTION public.insert_file_timestamps();


--
-- Name: file insert_display_name_for_file_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER insert_display_name_for_file_trigger BEFORE INSERT ON public.file FOR EACH ROW EXECUTE FUNCTION public.update_display_name_for_file();


--
-- Name: queue insert_leading_qitem; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER insert_leading_qitem BEFORE INSERT ON public.queue FOR EACH ROW EXECUTE FUNCTION public.set_leading_qitem();


--
-- Name: queue_20210101 insert_leading_qitem; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER insert_leading_qitem BEFORE INSERT ON public.queue_20210101 FOR EACH ROW EXECUTE FUNCTION public.set_leading_qitem();


--
-- Name: queue_20210201 insert_leading_qitem; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER insert_leading_qitem BEFORE INSERT ON public.queue_20210201 FOR EACH ROW EXECUTE FUNCTION public.set_leading_qitem();


--
-- Name: queue_20210301 insert_leading_qitem; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER insert_leading_qitem BEFORE INSERT ON public.queue_20210301 FOR EACH ROW EXECUTE FUNCTION public.set_leading_qitem();


--
-- Name: queue_20210401 insert_leading_qitem; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER insert_leading_qitem BEFORE INSERT ON public.queue_20210401 FOR EACH ROW EXECUTE FUNCTION public.set_leading_qitem();


--
-- Name: queue_20210501 insert_leading_qitem; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER insert_leading_qitem BEFORE INSERT ON public.queue_20210501 FOR EACH ROW EXECUTE FUNCTION public.set_leading_qitem();


--
-- Name: queue_20210601 insert_leading_qitem; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER insert_leading_qitem BEFORE INSERT ON public.queue_20210601 FOR EACH ROW EXECUTE FUNCTION public.set_leading_qitem();


--
-- Name: queue_20210701 insert_leading_qitem; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER insert_leading_qitem BEFORE INSERT ON public.queue_20210701 FOR EACH ROW EXECUTE FUNCTION public.set_leading_qitem();


--
-- Name: queue_20210801 insert_leading_qitem; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER insert_leading_qitem BEFORE INSERT ON public.queue_20210801 FOR EACH ROW EXECUTE FUNCTION public.set_leading_qitem();


--
-- Name: queue_20210901 insert_leading_qitem; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER insert_leading_qitem BEFORE INSERT ON public.queue_20210901 FOR EACH ROW EXECUTE FUNCTION public.set_leading_qitem();


--
-- Name: queue_20211001 insert_leading_qitem; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER insert_leading_qitem BEFORE INSERT ON public.queue_20211001 FOR EACH ROW EXECUTE FUNCTION public.set_leading_qitem();


--
-- Name: queue_20211101 insert_leading_qitem; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER insert_leading_qitem BEFORE INSERT ON public.queue_20211101 FOR EACH ROW EXECUTE FUNCTION public.set_leading_qitem();


--
-- Name: queue_20211201 insert_leading_qitem; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER insert_leading_qitem BEFORE INSERT ON public.queue_20211201 FOR EACH ROW EXECUTE FUNCTION public.set_leading_qitem();


--
-- Name: logging insert_name_cache_for_logging_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER insert_name_cache_for_logging_trigger BEFORE INSERT ON public.logging FOR EACH ROW EXECUTE FUNCTION public.update_name_cache_for_logging();


--
-- Name: logging logging_insert_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER logging_insert_trigger BEFORE INSERT ON public.logging FOR EACH ROW EXECUTE FUNCTION public.insert_timestamps();


--
-- Name: logging logging_update_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER logging_update_trigger BEFORE UPDATE ON public.logging FOR EACH ROW EXECUTE FUNCTION public.update_timestamps();


--
-- Name: parkeergebied parkeergebied_insert_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER parkeergebied_insert_trigger BEFORE INSERT ON public.parkeergebied FOR EACH ROW EXECUTE FUNCTION public.insert_timestamps();


--
-- Name: parkeergebied parkeergebied_update_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER parkeergebied_update_trigger BEFORE UPDATE ON public.parkeergebied FOR EACH ROW EXECUTE FUNCTION public.update_timestamps();


--
-- Name: zaaktype refresh_casetype_end_status; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER refresh_casetype_end_status AFTER UPDATE OF version ON public.zaaktype FOR EACH STATEMENT EXECUTE FUNCTION public.refresh_casetype_document_id_map();


--
-- Name: zaaktype_node refresh_casetype_end_status; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER refresh_casetype_end_status AFTER UPDATE OF logging_id ON public.zaaktype_node FOR EACH STATEMENT EXECUTE FUNCTION public.refresh_casetype_document_id_map();


--
-- Name: zaaktype_status refresh_casetype_end_status; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER refresh_casetype_end_status AFTER INSERT OR DELETE OR UPDATE OR TRUNCATE ON public.zaaktype_status FOR EACH STATEMENT EXECUTE FUNCTION public.refresh_casetype_end_status();


--
-- Name: zaaktype refresh_casetype_v1_reference; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER refresh_casetype_v1_reference AFTER INSERT OR DELETE OR UPDATE OR TRUNCATE ON public.zaaktype FOR EACH STATEMENT EXECUTE FUNCTION public.refresh_casetype_v1_reference();


--
-- Name: groups refresh_subject_position_matrix; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER refresh_subject_position_matrix AFTER INSERT OR DELETE OR UPDATE OR TRUNCATE ON public.groups FOR EACH STATEMENT EXECUTE FUNCTION public.refresh_subject_position_matrix();


--
-- Name: roles refresh_subject_position_matrix; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER refresh_subject_position_matrix AFTER INSERT OR DELETE OR UPDATE OR TRUNCATE ON public.roles FOR EACH STATEMENT EXECUTE FUNCTION public.refresh_subject_position_matrix();


--
-- Name: subject refresh_subject_position_matrix; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER refresh_subject_position_matrix AFTER INSERT OR DELETE OR UPDATE OF role_ids, group_ids, subject_type, uuid, username OR TRUNCATE ON public.subject FOR EACH STATEMENT EXECUTE FUNCTION public.refresh_subject_position_matrix();


--
-- Name: scheduled_jobs scheduled_jobs_insert_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER scheduled_jobs_insert_trigger BEFORE INSERT ON public.scheduled_jobs FOR EACH ROW EXECUTE FUNCTION public.insert_timestamps();


--
-- Name: scheduled_jobs scheduled_jobs_update_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER scheduled_jobs_update_trigger BEFORE UPDATE ON public.scheduled_jobs FOR EACH ROW EXECUTE FUNCTION public.update_timestamps();


--
-- Name: zaak set_confidential; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER set_confidential BEFORE INSERT OR UPDATE ON public.zaak FOR EACH ROW EXECUTE FUNCTION public.set_confidential_on_case();


--
-- Name: thread thread_insert_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER thread_insert_trigger BEFORE INSERT ON public.thread FOR EACH ROW EXECUTE FUNCTION public.insert_timestamps();


--
-- Name: thread_message thread_message_insert_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER thread_message_insert_trigger BEFORE INSERT ON public.thread_message FOR EACH ROW EXECUTE FUNCTION public.insert_timestamps();


--
-- Name: thread_message thread_message_update_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER thread_message_update_trigger BEFORE UPDATE ON public.thread_message FOR EACH ROW EXECUTE FUNCTION public.update_timestamps();


--
-- Name: thread thread_update_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER thread_update_trigger BEFORE UPDATE ON public.thread FOR EACH ROW EXECUTE FUNCTION public.update_timestamps();


--
-- Name: file update_display_name_for_file_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER update_display_name_for_file_trigger BEFORE UPDATE ON public.file FOR EACH ROW EXECUTE FUNCTION public.update_display_name_for_file();


--
-- Name: logging update_name_cache_for_logging_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER update_name_cache_for_logging_trigger BEFORE UPDATE ON public.logging FOR EACH ROW EXECUTE FUNCTION public.update_name_cache_for_logging();


--
-- Name: zaak update_subject_v1_json; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER update_subject_v1_json BEFORE INSERT OR UPDATE ON public.zaak FOR EACH ROW EXECUTE FUNCTION public.update_subject_json();


--
-- Name: zaaktype_node update_v0_json; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER update_v0_json BEFORE INSERT OR UPDATE ON public.zaaktype_node FOR EACH ROW EXECUTE FUNCTION public.zaaktype_node_v0_json();


--
-- Name: groups update_v1_json; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER update_v1_json BEFORE INSERT OR UPDATE ON public.groups FOR EACH ROW EXECUTE FUNCTION public.update_group_json();


--
-- Name: roles update_v1_json; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER update_v1_json BEFORE INSERT OR UPDATE ON public.roles FOR EACH ROW EXECUTE FUNCTION public.update_role_json();


--
-- Name: zaak_betrokkenen update_zaak_betrokkenen_cache; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER update_zaak_betrokkenen_cache AFTER INSERT OR DELETE OR UPDATE ON public.zaak_betrokkenen FOR EACH ROW EXECUTE FUNCTION public.sync_zaak_betrokkenen_cache();


--
-- Name: thread update_zaak_meta_count; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER update_zaak_meta_count AFTER INSERT OR UPDATE OF case_id, unread_employee_count ON public.thread FOR EACH ROW WHEN ((new.case_id IS NOT NULL)) EXECUTE FUNCTION public.zaak_meta_update_unread_count();


--
-- Name: zaak update_zaak_percentage; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER update_zaak_percentage BEFORE INSERT OR UPDATE OF milestone, zaaktype_node_id ON public.zaak FOR EACH ROW EXECUTE FUNCTION public.update_zaak_percentage();


--
-- Name: zaak zaak_aanvrager_update; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER zaak_aanvrager_update BEFORE INSERT OR UPDATE OF aanvrager_gm_id ON public.zaak FOR EACH ROW EXECUTE FUNCTION public.update_aanvrager_type();


--
-- Name: zaaktype_authorisation zaaktype_authorisation_insert_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER zaaktype_authorisation_insert_trigger BEFORE INSERT ON public.zaaktype_authorisation FOR EACH ROW EXECUTE FUNCTION public.insert_timestamps();


--
-- Name: zaaktype_authorisation zaaktype_authorisation_update_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER zaaktype_authorisation_update_trigger BEFORE UPDATE ON public.zaaktype_authorisation FOR EACH ROW EXECUTE FUNCTION public.update_timestamps();


--
-- Name: zaaktype_betrokkenen zaaktype_betrokkenen_insert_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER zaaktype_betrokkenen_insert_trigger BEFORE INSERT ON public.zaaktype_betrokkenen FOR EACH ROW EXECUTE FUNCTION public.insert_timestamps();


--
-- Name: zaaktype_betrokkenen zaaktype_betrokkenen_update_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER zaaktype_betrokkenen_update_trigger BEFORE UPDATE ON public.zaaktype_betrokkenen FOR EACH ROW EXECUTE FUNCTION public.update_timestamps();


--
-- Name: zaaktype zaaktype_insert_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER zaaktype_insert_trigger BEFORE INSERT ON public.zaaktype FOR EACH ROW EXECUTE FUNCTION public.insert_timestamps();


--
-- Name: zaaktype_kenmerken zaaktype_kenmerken_insert_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER zaaktype_kenmerken_insert_trigger BEFORE INSERT ON public.zaaktype_kenmerken FOR EACH ROW EXECUTE FUNCTION public.insert_timestamps();


--
-- Name: zaaktype_kenmerken zaaktype_kenmerken_update_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER zaaktype_kenmerken_update_trigger BEFORE UPDATE ON public.zaaktype_kenmerken FOR EACH ROW EXECUTE FUNCTION public.update_timestamps();


--
-- Name: zaaktype_node zaaktype_node_insert_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER zaaktype_node_insert_trigger BEFORE INSERT ON public.zaaktype_node FOR EACH ROW EXECUTE FUNCTION public.insert_timestamps();


--
-- Name: zaaktype_node zaaktype_node_update_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER zaaktype_node_update_trigger BEFORE UPDATE ON public.zaaktype_node FOR EACH ROW EXECUTE FUNCTION public.update_timestamps();


--
-- Name: zaaktype_notificatie zaaktype_notificatie_insert_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER zaaktype_notificatie_insert_trigger BEFORE INSERT ON public.zaaktype_notificatie FOR EACH ROW EXECUTE FUNCTION public.insert_timestamps();


--
-- Name: zaaktype_notificatie zaaktype_notificatie_update_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER zaaktype_notificatie_update_trigger BEFORE UPDATE ON public.zaaktype_notificatie FOR EACH ROW EXECUTE FUNCTION public.update_timestamps();


--
-- Name: zaaktype_regel zaaktype_regel_insert_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER zaaktype_regel_insert_trigger BEFORE INSERT ON public.zaaktype_regel FOR EACH ROW EXECUTE FUNCTION public.insert_timestamps();


--
-- Name: zaaktype_regel zaaktype_regel_update_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER zaaktype_regel_update_trigger BEFORE UPDATE ON public.zaaktype_regel FOR EACH ROW EXECUTE FUNCTION public.update_timestamps();


--
-- Name: zaaktype_relatie zaaktype_relatie_insert_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER zaaktype_relatie_insert_trigger BEFORE INSERT ON public.zaaktype_relatie FOR EACH ROW EXECUTE FUNCTION public.insert_timestamps();


--
-- Name: zaaktype_relatie zaaktype_relatie_update_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER zaaktype_relatie_update_trigger BEFORE UPDATE ON public.zaaktype_relatie FOR EACH ROW EXECUTE FUNCTION public.update_timestamps();


--
-- Name: zaaktype_resultaten zaaktype_resultaten_insert_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER zaaktype_resultaten_insert_trigger BEFORE INSERT ON public.zaaktype_resultaten FOR EACH ROW EXECUTE FUNCTION public.insert_timestamps();


--
-- Name: zaaktype_resultaten zaaktype_resultaten_update_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER zaaktype_resultaten_update_trigger BEFORE UPDATE ON public.zaaktype_resultaten FOR EACH ROW EXECUTE FUNCTION public.update_timestamps();


--
-- Name: zaaktype_sjablonen zaaktype_sjablonen_insert_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER zaaktype_sjablonen_insert_trigger BEFORE INSERT ON public.zaaktype_sjablonen FOR EACH ROW EXECUTE FUNCTION public.insert_timestamps();


--
-- Name: zaaktype_sjablonen zaaktype_sjablonen_update_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER zaaktype_sjablonen_update_trigger BEFORE UPDATE ON public.zaaktype_sjablonen FOR EACH ROW EXECUTE FUNCTION public.update_timestamps();


--
-- Name: zaaktype_status zaaktype_status_insert_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER zaaktype_status_insert_trigger BEFORE INSERT ON public.zaaktype_status FOR EACH ROW EXECUTE FUNCTION public.insert_timestamps();


--
-- Name: zaaktype_status zaaktype_status_update_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER zaaktype_status_update_trigger BEFORE UPDATE ON public.zaaktype_status FOR EACH ROW EXECUTE FUNCTION public.update_timestamps();


--
-- Name: zaaktype zaaktype_update_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER zaaktype_update_trigger BEFORE UPDATE ON public.zaaktype FOR EACH ROW EXECUTE FUNCTION public.update_timestamps();


--
-- Name: adres adres_natuurlijk_persoon_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.adres
    ADD CONSTRAINT adres_natuurlijk_persoon_id_fkey FOREIGN KEY (natuurlijk_persoon_id) REFERENCES public.natuurlijk_persoon(id);


--
-- Name: alternative_authentication_activation_link alternative_authentication_activation_link_subject_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.alternative_authentication_activation_link
    ADD CONSTRAINT alternative_authentication_activation_link_subject_id_fkey FOREIGN KEY (subject_id) REFERENCES public.subject(uuid);


--
-- Name: bedrijf bedrijf_related_custom_object_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bedrijf
    ADD CONSTRAINT bedrijf_related_custom_object_id_fkey FOREIGN KEY (related_custom_object_id) REFERENCES public.custom_object(id) ON DELETE SET NULL;


--
-- Name: beheer_import_log beheer_import_log_import_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.beheer_import_log
    ADD CONSTRAINT beheer_import_log_import_id_fkey FOREIGN KEY (import_id) REFERENCES public.beheer_import(id);


--
-- Name: object_bibliotheek_entry bibliotheek_categorie_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.object_bibliotheek_entry
    ADD CONSTRAINT bibliotheek_categorie_fkey FOREIGN KEY (bibliotheek_categorie_id) REFERENCES public.bibliotheek_categorie(id) ON DELETE CASCADE;


--
-- Name: bibliotheek_categorie bibliotheek_categorie_pid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bibliotheek_categorie
    ADD CONSTRAINT bibliotheek_categorie_pid_fkey FOREIGN KEY (pid) REFERENCES public.bibliotheek_categorie(id);


--
-- Name: bibliotheek_kenmerken bibliotheek_kenmerken_bibliotheek_categorie_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bibliotheek_kenmerken
    ADD CONSTRAINT bibliotheek_kenmerken_bibliotheek_categorie_id_fkey FOREIGN KEY (bibliotheek_categorie_id) REFERENCES public.bibliotheek_categorie(id);


--
-- Name: bibliotheek_kenmerken bibliotheek_kenmerken_file_metadata_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bibliotheek_kenmerken
    ADD CONSTRAINT bibliotheek_kenmerken_file_metadata_id_fkey FOREIGN KEY (file_metadata_id) REFERENCES public.file_metadata(id);


--
-- Name: bibliotheek_kenmerken_values bibliotheek_kenmerken_values_bibliotheek_kenmerken_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bibliotheek_kenmerken_values
    ADD CONSTRAINT bibliotheek_kenmerken_values_bibliotheek_kenmerken_id_fkey FOREIGN KEY (bibliotheek_kenmerken_id) REFERENCES public.bibliotheek_kenmerken(id) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE;


--
-- Name: bibliotheek_notificatie_kenmerk bibliotheek_notificatie_kenmerk_bibliotheek_kenmerken_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bibliotheek_notificatie_kenmerk
    ADD CONSTRAINT bibliotheek_notificatie_kenmerk_bibliotheek_kenmerken_id_fkey FOREIGN KEY (bibliotheek_kenmerken_id) REFERENCES public.bibliotheek_kenmerken(id);


--
-- Name: bibliotheek_notificatie_kenmerk bibliotheek_notificatie_kenmerk_bibliotheek_notificatie_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bibliotheek_notificatie_kenmerk
    ADD CONSTRAINT bibliotheek_notificatie_kenmerk_bibliotheek_notificatie_id_fkey FOREIGN KEY (bibliotheek_notificatie_id) REFERENCES public.bibliotheek_notificaties(id);


--
-- Name: bibliotheek_notificaties bibliotheek_notificaties_bibliotheek_categorie_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bibliotheek_notificaties
    ADD CONSTRAINT bibliotheek_notificaties_bibliotheek_categorie_id_fkey FOREIGN KEY (bibliotheek_categorie_id) REFERENCES public.bibliotheek_categorie(id);


--
-- Name: bibliotheek_sjablonen bibliotheek_sjablonen_bibliotheek_categorie_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bibliotheek_sjablonen
    ADD CONSTRAINT bibliotheek_sjablonen_bibliotheek_categorie_id_fkey FOREIGN KEY (bibliotheek_categorie_id) REFERENCES public.bibliotheek_categorie(id);


--
-- Name: bibliotheek_sjablonen bibliotheek_sjablonen_filestore_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bibliotheek_sjablonen
    ADD CONSTRAINT bibliotheek_sjablonen_filestore_id_fkey FOREIGN KEY (filestore_id) REFERENCES public.filestore(id);


--
-- Name: bibliotheek_sjablonen bibliotheek_sjablonen_interface_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bibliotheek_sjablonen
    ADD CONSTRAINT bibliotheek_sjablonen_interface_id_fkey FOREIGN KEY (interface_id) REFERENCES public.interface(id);


--
-- Name: bibliotheek_sjablonen_magic_string bibliotheek_sjablonen_magic_strin_bibliotheek_sjablonen_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bibliotheek_sjablonen_magic_string
    ADD CONSTRAINT bibliotheek_sjablonen_magic_strin_bibliotheek_sjablonen_id_fkey FOREIGN KEY (bibliotheek_sjablonen_id) REFERENCES public.bibliotheek_sjablonen(id);


--
-- Name: case_action case_actions_case_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.case_action
    ADD CONSTRAINT case_actions_case_id_fkey FOREIGN KEY (case_id) REFERENCES public.zaak(id);


--
-- Name: case_action case_actions_casetype_status_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.case_action
    ADD CONSTRAINT case_actions_casetype_status_id_fkey FOREIGN KEY (casetype_status_id) REFERENCES public.zaaktype_status(id);


--
-- Name: case_property case_property_case_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.case_property
    ADD CONSTRAINT case_property_case_id_fkey FOREIGN KEY (case_id) REFERENCES public.zaak(id) ON DELETE CASCADE;


--
-- Name: case_property case_property_object_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.case_property
    ADD CONSTRAINT case_property_object_id_fkey FOREIGN KEY (object_id) REFERENCES public.object_data(uuid) ON DELETE CASCADE;


--
-- Name: case_relation case_relation_case_id_a_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.case_relation
    ADD CONSTRAINT case_relation_case_id_a_fkey FOREIGN KEY (case_id_a) REFERENCES public.zaak(id);


--
-- Name: case_relation case_relation_case_id_b_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.case_relation
    ADD CONSTRAINT case_relation_case_id_b_fkey FOREIGN KEY (case_id_b) REFERENCES public.zaak(id);


--
-- Name: zaaktype_node casetype_node_logging_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_node
    ADD CONSTRAINT casetype_node_logging_id_fkey FOREIGN KEY (logging_id) REFERENCES public.logging(id);


--
-- Name: checklist checklist_case_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.checklist
    ADD CONSTRAINT checklist_case_id_fkey FOREIGN KEY (case_id) REFERENCES public.zaak(id);


--
-- Name: checklist_item checklist_item_assignee_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.checklist_item
    ADD CONSTRAINT checklist_item_assignee_id_fkey FOREIGN KEY (assignee_id) REFERENCES public.subject(id) ON DELETE SET NULL;


--
-- Name: checklist_item checklist_item_checklist_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.checklist_item
    ADD CONSTRAINT checklist_item_checklist_id_fkey FOREIGN KEY (checklist_id) REFERENCES public.checklist(id);


--
-- Name: contactmoment contactmoment_case_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.contactmoment
    ADD CONSTRAINT contactmoment_case_id_fkey FOREIGN KEY (case_id) REFERENCES public.zaak(id);


--
-- Name: contactmoment_email contactmoment_email_contactmoment_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.contactmoment_email
    ADD CONSTRAINT contactmoment_email_contactmoment_id_fkey FOREIGN KEY (contactmoment_id) REFERENCES public.contactmoment(id);


--
-- Name: contactmoment_email contactmoment_email_filestore_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.contactmoment_email
    ADD CONSTRAINT contactmoment_email_filestore_id_fkey FOREIGN KEY (filestore_id) REFERENCES public.filestore(id);


--
-- Name: contactmoment_note contactmoment_note_contactmoment_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.contactmoment_note
    ADD CONSTRAINT contactmoment_note_contactmoment_id_fkey FOREIGN KEY (contactmoment_id) REFERENCES public.contactmoment(id);


--
-- Name: custom_object custom_object_custom_object_version_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.custom_object
    ADD CONSTRAINT custom_object_custom_object_version_id_fkey FOREIGN KEY (custom_object_version_id) REFERENCES public.custom_object_version(id);


--
-- Name: custom_object_relationship custom_object_relationship_custom_object_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.custom_object_relationship
    ADD CONSTRAINT custom_object_relationship_custom_object_id_fkey FOREIGN KEY (custom_object_id) REFERENCES public.custom_object(id) ON DELETE CASCADE;


--
-- Name: custom_object_relationship custom_object_relationship_custom_object_version_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.custom_object_relationship
    ADD CONSTRAINT custom_object_relationship_custom_object_version_id_fkey FOREIGN KEY (custom_object_version_id) REFERENCES public.custom_object_version(id);


--
-- Name: custom_object_relationship custom_object_relationship_related_case_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.custom_object_relationship
    ADD CONSTRAINT custom_object_relationship_related_case_id_fkey FOREIGN KEY (related_case_id) REFERENCES public.zaak(id);


--
-- Name: custom_object_relationship custom_object_relationship_related_custom_object_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.custom_object_relationship
    ADD CONSTRAINT custom_object_relationship_related_custom_object_id_fkey FOREIGN KEY (related_custom_object_id) REFERENCES public.custom_object(id) ON DELETE CASCADE;


--
-- Name: custom_object_relationship custom_object_relationship_related_document_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.custom_object_relationship
    ADD CONSTRAINT custom_object_relationship_related_document_id_fkey FOREIGN KEY (related_document_id) REFERENCES public.file(id);


--
-- Name: custom_object_relationship custom_object_relationship_related_employee_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.custom_object_relationship
    ADD CONSTRAINT custom_object_relationship_related_employee_id_fkey FOREIGN KEY (related_employee_id) REFERENCES public.subject(id);


--
-- Name: custom_object_relationship custom_object_relationship_related_organization_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.custom_object_relationship
    ADD CONSTRAINT custom_object_relationship_related_organization_id_fkey FOREIGN KEY (related_organization_id) REFERENCES public.bedrijf(id);


--
-- Name: custom_object_relationship custom_object_relationship_related_person_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.custom_object_relationship
    ADD CONSTRAINT custom_object_relationship_related_person_id_fkey FOREIGN KEY (related_person_id) REFERENCES public.natuurlijk_persoon(id);


--
-- Name: custom_object_relationship custom_object_relationship_source_custom_field_type_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.custom_object_relationship
    ADD CONSTRAINT custom_object_relationship_source_custom_field_type_id_fkey FOREIGN KEY (source_custom_field_type_id) REFERENCES public.bibliotheek_kenmerken(id);


--
-- Name: custom_object_type custom_object_type_catalog_folder_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.custom_object_type
    ADD CONSTRAINT custom_object_type_catalog_folder_id_fkey FOREIGN KEY (catalog_folder_id) REFERENCES public.bibliotheek_categorie(id);


--
-- Name: custom_object_type custom_object_type_custom_object_type_version_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.custom_object_type
    ADD CONSTRAINT custom_object_type_custom_object_type_version_id_fkey FOREIGN KEY (custom_object_type_version_id) REFERENCES public.custom_object_type_version(id);


--
-- Name: custom_object_type_version custom_object_type_version_custom_object_type_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.custom_object_type_version
    ADD CONSTRAINT custom_object_type_version_custom_object_type_id_fkey FOREIGN KEY (custom_object_type_id) REFERENCES public.custom_object_type(id);


--
-- Name: custom_object_version custom_object_version_custom_object_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.custom_object_version
    ADD CONSTRAINT custom_object_version_custom_object_id_fkey FOREIGN KEY (custom_object_id) REFERENCES public.custom_object(id) ON DELETE CASCADE;


--
-- Name: custom_object_version custom_object_version_custom_object_type_version_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.custom_object_version
    ADD CONSTRAINT custom_object_version_custom_object_type_version_id_fkey FOREIGN KEY (custom_object_type_version_id) REFERENCES public.custom_object_type_version(id);


--
-- Name: custom_object_version custom_object_version_custom_object_version_content_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.custom_object_version
    ADD CONSTRAINT custom_object_version_custom_object_version_content_id_fkey FOREIGN KEY (custom_object_version_content_id) REFERENCES public.custom_object_version_content(id) ON DELETE CASCADE;


--
-- Name: directory directory_case_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.directory
    ADD CONSTRAINT directory_case_id_fkey FOREIGN KEY (case_id) REFERENCES public.zaak(id);


--
-- Name: export_queue export_queue_filestore_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.export_queue
    ADD CONSTRAINT export_queue_filestore_id_fkey FOREIGN KEY (filestore_id) REFERENCES public.filestore(id);


--
-- Name: export_queue export_queue_subject_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.export_queue
    ADD CONSTRAINT export_queue_subject_id_fkey FOREIGN KEY (subject_id) REFERENCES public.subject(id);


--
-- Name: export_queue export_queue_subject_uuid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.export_queue
    ADD CONSTRAINT export_queue_subject_uuid_fkey FOREIGN KEY (subject_uuid) REFERENCES public.subject(uuid);


--
-- Name: file_annotation file_annotation_file_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.file_annotation
    ADD CONSTRAINT file_annotation_file_id_fkey FOREIGN KEY (file_id) REFERENCES public.file(id);


--
-- Name: file_case_document file_case_document_bibliotheek_kenmerken_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.file_case_document
    ADD CONSTRAINT file_case_document_bibliotheek_kenmerken_id_fkey FOREIGN KEY (bibliotheek_kenmerken_id) REFERENCES public.bibliotheek_kenmerken(id);


--
-- Name: file_case_document file_case_document_case_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.file_case_document
    ADD CONSTRAINT file_case_document_case_id_fkey FOREIGN KEY (case_id) REFERENCES public.zaak(id);


--
-- Name: file_case_document file_case_document_file_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.file_case_document
    ADD CONSTRAINT file_case_document_file_id_fkey FOREIGN KEY (file_id) REFERENCES public.file(id);


--
-- Name: file file_case_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.file
    ADD CONSTRAINT file_case_id_fkey FOREIGN KEY (case_id) REFERENCES public.zaak(id);


--
-- Name: file file_directory_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.file
    ADD CONSTRAINT file_directory_id_fkey FOREIGN KEY (directory_id) REFERENCES public.directory(id);


--
-- Name: file file_filestore_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.file
    ADD CONSTRAINT file_filestore_id_fkey FOREIGN KEY (filestore_id) REFERENCES public.filestore(id);


--
-- Name: file file_intake_group_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.file
    ADD CONSTRAINT file_intake_group_id_fk FOREIGN KEY (intake_group_id) REFERENCES public.groups(id) ON DELETE SET NULL;


--
-- Name: file file_intake_role_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.file
    ADD CONSTRAINT file_intake_role_id_fk FOREIGN KEY (intake_role_id) REFERENCES public.roles(id) ON DELETE SET NULL;


--
-- Name: file file_is_duplicate_of_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.file
    ADD CONSTRAINT file_is_duplicate_of_fkey FOREIGN KEY (is_duplicate_of) REFERENCES public.file(id) ON DELETE SET NULL;


--
-- Name: file file_metadata_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.file
    ADD CONSTRAINT file_metadata_id_fkey FOREIGN KEY (metadata_id) REFERENCES public.file_metadata(id);


--
-- Name: file file_root_file_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.file
    ADD CONSTRAINT file_root_file_id_fkey FOREIGN KEY (root_file_id) REFERENCES public.file(id);


--
-- Name: file file_scheduled_jobs_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.file
    ADD CONSTRAINT file_scheduled_jobs_id_fkey FOREIGN KEY (scheduled_jobs_id) REFERENCES public.scheduled_jobs(id);


--
-- Name: file_derivative file_thumbnail_file_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.file_derivative
    ADD CONSTRAINT file_thumbnail_file_id_fkey FOREIGN KEY (file_id) REFERENCES public.file(id);


--
-- Name: file_derivative file_thumbnail_filestore_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.file_derivative
    ADD CONSTRAINT file_thumbnail_filestore_id_fkey FOREIGN KEY (filestore_id) REFERENCES public.filestore(id);


--
-- Name: gegevensmagazijn_subjecten gegevensmagazijn_subjecten_nnp_uuid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.gegevensmagazijn_subjecten
    ADD CONSTRAINT gegevensmagazijn_subjecten_nnp_uuid_fkey FOREIGN KEY (nnp_uuid) REFERENCES public.bedrijf(uuid);


--
-- Name: gegevensmagazijn_subjecten gegevensmagazijn_subjecten_subject_uuid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.gegevensmagazijn_subjecten
    ADD CONSTRAINT gegevensmagazijn_subjecten_subject_uuid_fkey FOREIGN KEY (subject_uuid) REFERENCES public.subject(uuid);


--
-- Name: gm_adres gm_adres_natuurlijk_persoon_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.gm_adres
    ADD CONSTRAINT gm_adres_natuurlijk_persoon_id_fkey FOREIGN KEY (natuurlijk_persoon_id) REFERENCES public.gm_natuurlijk_persoon(id);


--
-- Name: gm_natuurlijk_persoon gm_natuurlijk_persoon_adres_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.gm_natuurlijk_persoon
    ADD CONSTRAINT gm_natuurlijk_persoon_adres_id_fkey FOREIGN KEY (adres_id) REFERENCES public.gm_adres(id) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE;


--
-- Name: interface interface_case_type_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.interface
    ADD CONSTRAINT interface_case_type_id_fkey FOREIGN KEY (case_type_id) REFERENCES public.zaaktype(id);


--
-- Name: interface interface_objecttype_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.interface
    ADD CONSTRAINT interface_objecttype_id_fkey FOREIGN KEY (objecttype_id) REFERENCES public.object_data(uuid) ON DELETE SET NULL;


--
-- Name: logging logging_object_uuid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.logging
    ADD CONSTRAINT logging_object_uuid_fkey FOREIGN KEY (object_uuid) REFERENCES public.object_data(uuid) ON DELETE SET NULL;


--
-- Name: message message_logging_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.message
    ADD CONSTRAINT message_logging_id_fkey FOREIGN KEY (logging_id) REFERENCES public.logging(id);


--
-- Name: natuurlijk_persoon natuurlijk_persoon_adres_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.natuurlijk_persoon
    ADD CONSTRAINT natuurlijk_persoon_adres_id_fk FOREIGN KEY (adres_id) REFERENCES public.adres(id);


--
-- Name: natuurlijk_persoon natuurlijk_persoon_related_custom_object_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.natuurlijk_persoon
    ADD CONSTRAINT natuurlijk_persoon_related_custom_object_id_fkey FOREIGN KEY (related_custom_object_id) REFERENCES public.custom_object(id) ON DELETE SET NULL;


--
-- Name: object_bibliotheek_entry object_data; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.object_bibliotheek_entry
    ADD CONSTRAINT object_data FOREIGN KEY (object_uuid) REFERENCES public.object_data(uuid) ON DELETE CASCADE;


--
-- Name: object_data object_data_class_uuid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.object_data
    ADD CONSTRAINT object_data_class_uuid_fkey FOREIGN KEY (class_uuid) REFERENCES public.object_data(uuid);


--
-- Name: object_acl_entry object_data_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.object_acl_entry
    ADD CONSTRAINT object_data_fkey FOREIGN KEY (object_uuid) REFERENCES public.object_data(uuid) ON DELETE CASCADE;


--
-- Name: object_mutation object_mutation_lock_object_uuid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.object_mutation
    ADD CONSTRAINT object_mutation_lock_object_uuid_fkey FOREIGN KEY (lock_object_uuid) REFERENCES public.object_data(uuid);


--
-- Name: object_mutation object_mutation_object_uuid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.object_mutation
    ADD CONSTRAINT object_mutation_object_uuid_fkey FOREIGN KEY (object_uuid) REFERENCES public.object_data(uuid) ON DELETE CASCADE;


--
-- Name: object_mutation object_mutation_subject_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.object_mutation
    ADD CONSTRAINT object_mutation_subject_id_fkey FOREIGN KEY (subject_id) REFERENCES public.subject(id);


--
-- Name: object_relation object_relation_object_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.object_relation
    ADD CONSTRAINT object_relation_object_id_fkey FOREIGN KEY (object_id) REFERENCES public.object_data(uuid) ON DELETE CASCADE DEFERRABLE;


--
-- Name: object_relation object_relation_object_uuid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.object_relation
    ADD CONSTRAINT object_relation_object_uuid_fkey FOREIGN KEY (object_uuid) REFERENCES public.object_data(uuid) ON DELETE CASCADE DEFERRABLE;


--
-- Name: object_relationships object_relationships_object1_uuid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.object_relationships
    ADD CONSTRAINT object_relationships_object1_uuid_fkey FOREIGN KEY (object1_uuid) REFERENCES public.object_data(uuid) ON DELETE CASCADE;


--
-- Name: object_relationships object_relationships_object2_uuid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.object_relationships
    ADD CONSTRAINT object_relationships_object2_uuid_fkey FOREIGN KEY (object2_uuid) REFERENCES public.object_data(uuid) ON DELETE CASCADE;


--
-- Name: object_subscription object_subscription_config_interface_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.object_subscription
    ADD CONSTRAINT object_subscription_config_interface_id_fkey FOREIGN KEY (config_interface_id) REFERENCES public.interface(id);


--
-- Name: object_subscription object_subscription_interface_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.object_subscription
    ADD CONSTRAINT object_subscription_interface_id_fkey FOREIGN KEY (interface_id) REFERENCES public.interface(id);


--
-- Name: queue queue_object_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.queue
    ADD CONSTRAINT queue_object_id_fkey FOREIGN KEY (object_id) REFERENCES public.object_data(uuid) ON DELETE CASCADE DEFERRABLE;


--
-- Name: queue queue_parent_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.queue
    ADD CONSTRAINT queue_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES public.queue(id) ON DELETE SET NULL;


--
-- Name: role_rights role_rights_rights_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.role_rights
    ADD CONSTRAINT role_rights_rights_name_fkey FOREIGN KEY (rights_name) REFERENCES public.rights(name);


--
-- Name: role_rights role_rights_role_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.role_rights
    ADD CONSTRAINT role_rights_role_id_fkey FOREIGN KEY (role_id) REFERENCES public.roles(id);


--
-- Name: roles roles_parent_group_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.roles
    ADD CONSTRAINT roles_parent_group_id_fkey FOREIGN KEY (parent_group_id) REFERENCES public.groups(id);


--
-- Name: sbus_logging sbus_logging_pid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sbus_logging
    ADD CONSTRAINT sbus_logging_pid_fkey FOREIGN KEY (pid) REFERENCES public.sbus_logging(id);


--
-- Name: sbus_logging sbus_logging_sbus_traffic_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sbus_logging
    ADD CONSTRAINT sbus_logging_sbus_traffic_id_fkey FOREIGN KEY (sbus_traffic_id) REFERENCES public.sbus_traffic(id);


--
-- Name: scheduled_jobs scheduled_jobs_case_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.scheduled_jobs
    ADD CONSTRAINT scheduled_jobs_case_id_fkey FOREIGN KEY (case_id) REFERENCES public.zaak(id);


--
-- Name: search_query_delen search_query_delen_search_query_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.search_query_delen
    ADD CONSTRAINT search_query_delen_search_query_id_fkey FOREIGN KEY (search_query_id) REFERENCES public.search_query(id);


--
-- Name: service_geojson_relationship service_geojson_relationship_service_geojson_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.service_geojson_relationship
    ADD CONSTRAINT service_geojson_relationship_service_geojson_id_fkey FOREIGN KEY (service_geojson_id) REFERENCES public.service_geojson(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: session_invitation session_invitation_subject_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.session_invitation
    ADD CONSTRAINT session_invitation_subject_id_fkey FOREIGN KEY (subject_id) REFERENCES public.subject(uuid) ON DELETE CASCADE;


--
-- Name: subject_login_history subject_login_history_subject_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.subject_login_history
    ADD CONSTRAINT subject_login_history_subject_id_fkey FOREIGN KEY (subject_id) REFERENCES public.subject(id);


--
-- Name: subject_login_history subject_login_history_subject_uuid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.subject_login_history
    ADD CONSTRAINT subject_login_history_subject_uuid_fkey FOREIGN KEY (subject_uuid) REFERENCES public.subject(uuid);


--
-- Name: subject subject_related_custom_object_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.subject
    ADD CONSTRAINT subject_related_custom_object_id_fkey FOREIGN KEY (related_custom_object_id) REFERENCES public.custom_object(id) ON DELETE SET NULL;


--
-- Name: thread thread_case_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.thread
    ADD CONSTRAINT thread_case_id_fkey FOREIGN KEY (case_id) REFERENCES public.zaak(id);


--
-- Name: thread_message_attachment_derivative thread_message_attachment_der_thread_message_attachment_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.thread_message_attachment_derivative
    ADD CONSTRAINT thread_message_attachment_der_thread_message_attachment_id_fkey FOREIGN KEY (thread_message_attachment_id) REFERENCES public.thread_message_attachment(id) ON DELETE CASCADE;


--
-- Name: thread_message_attachment_derivative thread_message_attachment_derivative_filestore_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.thread_message_attachment_derivative
    ADD CONSTRAINT thread_message_attachment_derivative_filestore_id_fkey FOREIGN KEY (filestore_id) REFERENCES public.filestore(id);


--
-- Name: thread_message_attachment thread_message_attachment_filestore_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.thread_message_attachment
    ADD CONSTRAINT thread_message_attachment_filestore_id_fkey FOREIGN KEY (filestore_id) REFERENCES public.filestore(id);


--
-- Name: thread_message_attachment thread_message_attachment_thread_message_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.thread_message_attachment
    ADD CONSTRAINT thread_message_attachment_thread_message_id_fkey FOREIGN KEY (thread_message_id) REFERENCES public.thread_message(id);


--
-- Name: thread_message_external thread_message_external_source_file_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.thread_message_external
    ADD CONSTRAINT thread_message_external_source_file_id_fkey FOREIGN KEY (source_file_id) REFERENCES public.filestore(id);


--
-- Name: thread_message thread_message_thread_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.thread_message
    ADD CONSTRAINT thread_message_thread_id_fkey FOREIGN KEY (thread_id) REFERENCES public.thread(id);


--
-- Name: thread_message thread_message_thread_message_contact_moment_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.thread_message
    ADD CONSTRAINT thread_message_thread_message_contact_moment_id_fkey FOREIGN KEY (thread_message_contact_moment_id) REFERENCES public.thread_message_contact_moment(id);


--
-- Name: thread_message thread_message_thread_message_external_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.thread_message
    ADD CONSTRAINT thread_message_thread_message_external_id_fkey FOREIGN KEY (thread_message_external_id) REFERENCES public.thread_message_external(id);


--
-- Name: thread_message thread_message_thread_message_note_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.thread_message
    ADD CONSTRAINT thread_message_thread_message_note_id_fkey FOREIGN KEY (thread_message_note_id) REFERENCES public.thread_message_note(id);


--
-- Name: transaction transaction_input_file_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.transaction
    ADD CONSTRAINT transaction_input_file_fkey FOREIGN KEY (input_file) REFERENCES public.filestore(id);


--
-- Name: transaction transaction_interface_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.transaction
    ADD CONSTRAINT transaction_interface_id_fkey FOREIGN KEY (interface_id) REFERENCES public.interface(id);


--
-- Name: transaction_record_to_object transaction_record_to_object_transaction_record_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.transaction_record_to_object
    ADD CONSTRAINT transaction_record_to_object_transaction_record_id_fkey FOREIGN KEY (transaction_record_id) REFERENCES public.transaction_record(id) ON DELETE CASCADE DEFERRABLE;


--
-- Name: transaction_record transaction_record_transaction_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.transaction_record
    ADD CONSTRAINT transaction_record_transaction_id_fkey FOREIGN KEY (transaction_id) REFERENCES public.transaction(id) ON DELETE CASCADE DEFERRABLE;


--
-- Name: user_entity user_entity_source_interface_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.user_entity
    ADD CONSTRAINT user_entity_source_interface_id_fkey FOREIGN KEY (source_interface_id) REFERENCES public.interface(id);


--
-- Name: user_entity user_entity_subject_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.user_entity
    ADD CONSTRAINT user_entity_subject_id_fkey FOREIGN KEY (subject_id) REFERENCES public.subject(id);


--
-- Name: zaak zaak_aanvrager_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak
    ADD CONSTRAINT zaak_aanvrager_fkey FOREIGN KEY (aanvrager) REFERENCES public.zaak_betrokkenen(id);


--
-- Name: zaak_authorisation zaak_authorisation_zaak_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak_authorisation
    ADD CONSTRAINT zaak_authorisation_zaak_id_fkey FOREIGN KEY (zaak_id) REFERENCES public.zaak(id);


--
-- Name: zaak_bag zaak_bag_pid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak_bag
    ADD CONSTRAINT zaak_bag_pid_fkey FOREIGN KEY (pid) REFERENCES public.zaak_bag(id);


--
-- Name: zaak_bag zaak_bag_zaak_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak_bag
    ADD CONSTRAINT zaak_bag_zaak_id_fkey FOREIGN KEY (zaak_id) REFERENCES public.zaak(id);


--
-- Name: zaak zaak_behandelaar_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak
    ADD CONSTRAINT zaak_behandelaar_fkey FOREIGN KEY (behandelaar) REFERENCES public.zaak_betrokkenen(id);


--
-- Name: zaak_betrokkenen zaak_betrokkenen_bibliotheek_kenmerken_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak_betrokkenen
    ADD CONSTRAINT zaak_betrokkenen_bibliotheek_kenmerken_id_fkey FOREIGN KEY (bibliotheek_kenmerken_id) REFERENCES public.bibliotheek_kenmerken(id) ON DELETE SET NULL;


--
-- Name: zaak_betrokkenen zaak_betrokkenen_zaak_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak_betrokkenen
    ADD CONSTRAINT zaak_betrokkenen_zaak_id_fkey FOREIGN KEY (zaak_id) REFERENCES public.zaak(id);


--
-- Name: zaak zaak_coordinator_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak
    ADD CONSTRAINT zaak_coordinator_fkey FOREIGN KEY (coordinator) REFERENCES public.zaak_betrokkenen(id);


--
-- Name: logging zaak_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.logging
    ADD CONSTRAINT zaak_id_fkey FOREIGN KEY (zaak_id) REFERENCES public.zaak(id);


--
-- Name: zaak_kenmerk zaak_kenmerk_bibliotheek_kenmerken_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak_kenmerk
    ADD CONSTRAINT zaak_kenmerk_bibliotheek_kenmerken_id_fkey FOREIGN KEY (bibliotheek_kenmerken_id) REFERENCES public.bibliotheek_kenmerken(id);


--
-- Name: zaak_kenmerk zaak_kenmerk_zaak_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak_kenmerk
    ADD CONSTRAINT zaak_kenmerk_zaak_id_fkey FOREIGN KEY (zaak_id) REFERENCES public.zaak(id) ON DELETE CASCADE;


--
-- Name: zaak zaak_locatie_correspondentie_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak
    ADD CONSTRAINT zaak_locatie_correspondentie_fkey FOREIGN KEY (locatie_correspondentie) REFERENCES public.zaak_bag(id);


--
-- Name: zaak zaak_locatie_zaak_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak
    ADD CONSTRAINT zaak_locatie_zaak_fkey FOREIGN KEY (locatie_zaak) REFERENCES public.zaak_bag(id);


--
-- Name: zaak_meta zaak_meta_zaak_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak_meta
    ADD CONSTRAINT zaak_meta_zaak_id_fkey FOREIGN KEY (zaak_id) REFERENCES public.zaak(id);


--
-- Name: zaak zaak_number_master_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak
    ADD CONSTRAINT zaak_number_master_fk FOREIGN KEY (number_master) REFERENCES public.zaak(id);


--
-- Name: zaak zaak_object_data_uuid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak
    ADD CONSTRAINT zaak_object_data_uuid_fkey FOREIGN KEY (uuid) REFERENCES public.object_data(uuid) ON DELETE SET NULL;


--
-- Name: zaak zaak_pid_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak
    ADD CONSTRAINT zaak_pid_fk FOREIGN KEY (pid) REFERENCES public.zaak(id);


--
-- Name: zaak zaak_pid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak
    ADD CONSTRAINT zaak_pid_fkey FOREIGN KEY (pid) REFERENCES public.zaak(id);


--
-- Name: zaak zaak_relates_to_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak
    ADD CONSTRAINT zaak_relates_to_fkey FOREIGN KEY (relates_to) REFERENCES public.zaak(id);


--
-- Name: zaak zaak_resultaat_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak
    ADD CONSTRAINT zaak_resultaat_id_fkey FOREIGN KEY (resultaat_id) REFERENCES public.zaaktype_resultaten(id);


--
-- Name: zaak_subcase zaak_subcase_relation_zaak_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak_subcase
    ADD CONSTRAINT zaak_subcase_relation_zaak_id_fkey FOREIGN KEY (relation_zaak_id) REFERENCES public.zaak(id);


--
-- Name: zaak_subcase zaak_subcase_zaak_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak_subcase
    ADD CONSTRAINT zaak_subcase_zaak_id_fkey FOREIGN KEY (zaak_id) REFERENCES public.zaak(id);


--
-- Name: zaak zaak_vervolg_van_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak
    ADD CONSTRAINT zaak_vervolg_van_fkey FOREIGN KEY (vervolg_van) REFERENCES public.zaak(id);


--
-- Name: zaak zaak_zaaktype_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak
    ADD CONSTRAINT zaak_zaaktype_id_fkey FOREIGN KEY (zaaktype_id) REFERENCES public.zaaktype(id);


--
-- Name: zaak_onafgerond zaak_zaaktype_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak_onafgerond
    ADD CONSTRAINT zaak_zaaktype_id_fkey FOREIGN KEY (zaaktype_id) REFERENCES public.zaaktype(id);


--
-- Name: zaak zaak_zaaktype_node_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak
    ADD CONSTRAINT zaak_zaaktype_node_id_fkey FOREIGN KEY (zaaktype_node_id) REFERENCES public.zaaktype_node(id);


--
-- Name: zaaktype_authorisation zaaktype_authorisation_zaaktype_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_authorisation
    ADD CONSTRAINT zaaktype_authorisation_zaaktype_id_fkey FOREIGN KEY (zaaktype_id) REFERENCES public.zaaktype(id);


--
-- Name: zaaktype_authorisation zaaktype_authorisation_zaaktype_node_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_authorisation
    ADD CONSTRAINT zaaktype_authorisation_zaaktype_node_id_fkey FOREIGN KEY (zaaktype_node_id) REFERENCES public.zaaktype_node(id) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE;


--
-- Name: zaaktype_betrokkenen zaaktype_betrokkenen_zaaktype_node_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_betrokkenen
    ADD CONSTRAINT zaaktype_betrokkenen_zaaktype_node_id_fkey FOREIGN KEY (zaaktype_node_id) REFERENCES public.zaaktype_node(id) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE;


--
-- Name: zaaktype zaaktype_bibliotheek_categorie_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype
    ADD CONSTRAINT zaaktype_bibliotheek_categorie_id_fkey FOREIGN KEY (bibliotheek_categorie_id) REFERENCES public.bibliotheek_categorie(id) DEFERRABLE;


--
-- Name: zaaktype_kenmerken zaaktype_kenmerken_bibliotheek_kenmerken_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_kenmerken
    ADD CONSTRAINT zaaktype_kenmerken_bibliotheek_kenmerken_id_fkey FOREIGN KEY (bibliotheek_kenmerken_id) REFERENCES public.bibliotheek_kenmerken(id);


--
-- Name: zaaktype_kenmerken zaaktype_kenmerken_custom_object_uuid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_kenmerken
    ADD CONSTRAINT zaaktype_kenmerken_custom_object_uuid_fkey FOREIGN KEY (custom_object_uuid) REFERENCES public.custom_object_type(uuid);


--
-- Name: zaaktype_kenmerken zaaktype_kenmerken_object_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_kenmerken
    ADD CONSTRAINT zaaktype_kenmerken_object_id_fkey FOREIGN KEY (object_id) REFERENCES public.object_data(uuid) ON DELETE RESTRICT;


--
-- Name: zaaktype_kenmerken zaaktype_kenmerken_zaak_status_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_kenmerken
    ADD CONSTRAINT zaaktype_kenmerken_zaak_status_id_fkey FOREIGN KEY (zaak_status_id) REFERENCES public.zaaktype_status(id);


--
-- Name: zaaktype_kenmerken zaaktype_kenmerken_zaaktype_node_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_kenmerken
    ADD CONSTRAINT zaaktype_kenmerken_zaaktype_node_id_fkey FOREIGN KEY (zaaktype_node_id) REFERENCES public.zaaktype_node(id);


--
-- Name: zaaktype_node zaaktype_node_moeder_zaaktype_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_node
    ADD CONSTRAINT zaaktype_node_moeder_zaaktype_id_fkey FOREIGN KEY (moeder_zaaktype_id) REFERENCES public.zaaktype(id) ON DELETE SET NULL;


--
-- Name: zaaktype_node zaaktype_node_zaaktype_definitie_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_node
    ADD CONSTRAINT zaaktype_node_zaaktype_definitie_id_fkey FOREIGN KEY (zaaktype_definitie_id) REFERENCES public.zaaktype_definitie(id) DEFERRABLE;


--
-- Name: zaaktype_node zaaktype_node_zaaktype_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_node
    ADD CONSTRAINT zaaktype_node_zaaktype_id_fkey FOREIGN KEY (zaaktype_id) REFERENCES public.zaaktype(id) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE;


--
-- Name: zaaktype_notificatie zaaktype_notificatie_bibliotheek_notificaties_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_notificatie
    ADD CONSTRAINT zaaktype_notificatie_bibliotheek_notificaties_id_fkey FOREIGN KEY (bibliotheek_notificaties_id) REFERENCES public.bibliotheek_notificaties(id);


--
-- Name: zaaktype_notificatie zaaktype_notificatie_zaak_status_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_notificatie
    ADD CONSTRAINT zaaktype_notificatie_zaak_status_id_fkey FOREIGN KEY (zaak_status_id) REFERENCES public.zaaktype_status(id);


--
-- Name: zaaktype_notificatie zaaktype_notificatie_zaaktype_node_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_notificatie
    ADD CONSTRAINT zaaktype_notificatie_zaaktype_node_id_fkey FOREIGN KEY (zaaktype_node_id) REFERENCES public.zaaktype_node(id);


--
-- Name: zaaktype_regel zaaktype_regel_zaak_status_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_regel
    ADD CONSTRAINT zaaktype_regel_zaak_status_id_fkey FOREIGN KEY (zaak_status_id) REFERENCES public.zaaktype_status(id);


--
-- Name: zaaktype_regel zaaktype_regel_zaaktype_node_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_regel
    ADD CONSTRAINT zaaktype_regel_zaaktype_node_id_fkey FOREIGN KEY (zaaktype_node_id) REFERENCES public.zaaktype_node(id);


--
-- Name: zaaktype_relatie zaaktype_relatie_relatie_zaaktype_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_relatie
    ADD CONSTRAINT zaaktype_relatie_relatie_zaaktype_id_fkey FOREIGN KEY (relatie_zaaktype_id) REFERENCES public.zaaktype(id) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE;


--
-- Name: zaaktype_relatie zaaktype_relatie_zaaktype_node_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_relatie
    ADD CONSTRAINT zaaktype_relatie_zaaktype_node_id_fkey FOREIGN KEY (zaaktype_node_id) REFERENCES public.zaaktype_node(id) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE;


--
-- Name: zaaktype_relatie zaaktype_relatie_zaaktype_status_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_relatie
    ADD CONSTRAINT zaaktype_relatie_zaaktype_status_id_fkey FOREIGN KEY (zaaktype_status_id) REFERENCES public.zaaktype_status(id) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE;


--
-- Name: zaaktype_resultaten zaaktype_resultaten_zaaktype_node_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_resultaten
    ADD CONSTRAINT zaaktype_resultaten_zaaktype_node_id_fkey FOREIGN KEY (zaaktype_node_id) REFERENCES public.zaaktype_node(id) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE;


--
-- Name: zaaktype_resultaten zaaktype_resultaten_zaaktype_status_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_resultaten
    ADD CONSTRAINT zaaktype_resultaten_zaaktype_status_id_fkey FOREIGN KEY (zaaktype_status_id) REFERENCES public.zaaktype_status(id) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE;


--
-- Name: zaaktype_sjablonen zaaktype_sjablonen_bibliotheek_kenmerken_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_sjablonen
    ADD CONSTRAINT zaaktype_sjablonen_bibliotheek_kenmerken_id_fkey FOREIGN KEY (bibliotheek_kenmerken_id) REFERENCES public.bibliotheek_kenmerken(id) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE;


--
-- Name: zaaktype_sjablonen zaaktype_sjablonen_bibliotheek_sjablonen_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_sjablonen
    ADD CONSTRAINT zaaktype_sjablonen_bibliotheek_sjablonen_id_fkey FOREIGN KEY (bibliotheek_sjablonen_id) REFERENCES public.bibliotheek_sjablonen(id);


--
-- Name: zaaktype_sjablonen zaaktype_sjablonen_zaak_status_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_sjablonen
    ADD CONSTRAINT zaaktype_sjablonen_zaak_status_id_fkey FOREIGN KEY (zaak_status_id) REFERENCES public.zaaktype_status(id);


--
-- Name: zaaktype_sjablonen zaaktype_sjablonen_zaaktype_node_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_sjablonen
    ADD CONSTRAINT zaaktype_sjablonen_zaaktype_node_id_fkey FOREIGN KEY (zaaktype_node_id) REFERENCES public.zaaktype_node(id);


--
-- Name: zaaktype_standaard_betrokkenen zaaktype_standaard_betrokkenen_zaaktype_node_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_standaard_betrokkenen
    ADD CONSTRAINT zaaktype_standaard_betrokkenen_zaaktype_node_id_fkey FOREIGN KEY (zaaktype_node_id) REFERENCES public.zaaktype_node(id);


--
-- Name: zaaktype_standaard_betrokkenen zaaktype_standaard_betrokkenen_zaaktype_status_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_standaard_betrokkenen
    ADD CONSTRAINT zaaktype_standaard_betrokkenen_zaaktype_status_id_fkey FOREIGN KEY (zaak_status_id) REFERENCES public.zaaktype_status(id);


--
-- Name: zaaktype_status_checklist_item zaaktype_status_checklist_item_zaaktype_status_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_status_checklist_item
    ADD CONSTRAINT zaaktype_status_checklist_item_zaaktype_status_id_fkey FOREIGN KEY (casetype_status_id) REFERENCES public.zaaktype_status(id);


--
-- Name: zaaktype_status zaaktype_status_zaaktype_node_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_status
    ADD CONSTRAINT zaaktype_status_zaaktype_node_id_fkey FOREIGN KEY (zaaktype_node_id) REFERENCES public.zaaktype_node(id) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE;


--
-- Name: zaaktype zaaktype_zaaktype_node_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype
    ADD CONSTRAINT zaaktype_zaaktype_node_id_fkey FOREIGN KEY (zaaktype_node_id) REFERENCES public.zaaktype_node(id) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE;


--
-- Name: casetype_end_status; Type: MATERIALIZED VIEW DATA; Schema: public; Owner: -
--

REFRESH MATERIALIZED VIEW public.casetype_end_status;


--
-- Name: casetype_v1_reference; Type: MATERIALIZED VIEW DATA; Schema: public; Owner: -
--

REFRESH MATERIALIZED VIEW public.casetype_v1_reference;


--
-- Name: subject_position_matrix; Type: MATERIALIZED VIEW DATA; Schema: public; Owner: -
--

REFRESH MATERIALIZED VIEW public.subject_position_matrix;


--
-- Name: zaaktype_document_kenmerken_map; Type: MATERIALIZED VIEW DATA; Schema: public; Owner: -
--

REFRESH MATERIALIZED VIEW public.zaaktype_document_kenmerken_map;


--
-- PostgreSQL database dump complete
--

