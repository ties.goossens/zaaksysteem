import {
    openPageAs
} from './../../../functions/common/navigate';
import {
    createCase
} from './../../../functions/intern/documentintake';
import {
    setDocumentintakeSettings,
    setDocumentintakeCasedocument,
    goNext
} from './../../../functions/common/form';
import {
    checkDocument,
    getDocumentDescription,
    getDocumentOrigin,
    getDocumentOriginDate,
    getDocumentLabels
} from './../../../functions/intern/caseView/caseDocuments';
import {
    openTab
} from './../../../functions/intern/caseView/caseNav';
import waitForElement from './../../../functions/common/waitForElement';

const testData = [
    {
        type: 'without settings',
        documentName: 'document zaak registreren 1.odt',
        documentNameExpected: 'document zaak registreren 1.odt'
    },
    {
        type: 'with settings',
        documentName: 'document zaak registreren 2.odt',
        setOnForm: false,
        settings: {
            newDocumentName: 'document zaak registreren 2 gewijzigd',
            description: 'description',
            origin: 'Uitgaand',
            originDate: '01-01-17'
        },
        documentNameExpected: 'document zaak registreren 2.odt'
    },
    {
        type: 'with changing the settings on form',
        documentName: 'document zaak registreren 3.odt',
        setOnForm: true,
        settings: {
            newDocumentName: 'document zaak registreren 3 gewijzigd',
            description: 'description',
            origin: 'Inkomend',
            originDate: '01-01-17'
        },
        documentNameExpected: 'document zaak registreren 3 gewijzigd.odt'
    },
    {
        type: 'with adding the document as casedocument',
        documentName: 'document zaak registreren 4.odt',
        caseDocument: 'Document',
        documentNameExpected: 'document zaak registreren 4.odt'
    }
];

const newCase = {
    casetype: 'documentintake zaak registreren',
    requestorType: 'natuurlijk_persoon',
    requestor: '111222333'
};

testData.forEach(data => {
    const { type, documentName, setOnForm, settings, caseDocument, documentNameExpected } = data;

    describe(`when registering a case from the documentintake ${type}`, () => {
        beforeAll(() => {

            openPageAs('admin', '/intern');
            openPageAs('admin', '/zaak/intake?scope=documents');

            createCase(documentName, newCase);

            if ( setOnForm ) {
                setDocumentintakeSettings(settings);
            }

            if ( caseDocument ) {
                setDocumentintakeCasedocument(caseDocument);
            }

            goNext(3);
            waitForElement('.case-view');
            openTab('docs');
        });

        it('the case should have the document', () => {
            expect(checkDocument(documentNameExpected)).toBe(true);
        });

        if ( settings ) {
            const { description, origin, originDate } = settings;

            it('the document should have the settings', () => {
                expect(getDocumentDescription(documentNameExpected)).toEqual(description);
                expect(getDocumentOrigin(documentNameExpected)).toEqual(origin);
                expect(getDocumentOriginDate(documentNameExpected)).toEqual(originDate);
            });
        }

        if ( caseDocument ) {
            it(`the document should be set as casedocument "${caseDocument}"`, () => {
                expect(getDocumentLabels(documentName)).toEqual(caseDocument);
            });
        }
    });
});
