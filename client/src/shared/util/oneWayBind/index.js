// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import propCheck from '../propCheck';

const defaultGetter = () => undefined;

export default () => {
  let getter = defaultGetter;

  return (fn) => {
    if (fn) {
      propCheck.throw(propCheck.func, fn);

      getter = fn;

      return () => {
        getter = defaultGetter;
      };
    }

    return getter();
  };
};
