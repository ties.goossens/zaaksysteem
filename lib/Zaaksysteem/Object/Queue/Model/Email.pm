package Zaaksysteem::Object::Queue::Model::Email;

use Moose::Role;

use BTTW::Tools;
use Zaaksysteem::Backend::Mailer;

=head1 NAME

Zaaksysteem::Object::Queue::Model::Email - Email queue item handler

=head1 DESCRIPTION

=head1 METHODS

=head2 send_email

=cut

sig send_email => 'Zaaksysteem::Backend::Object::Queue::Component';

sub send_email {
    my $self = shift;
    my $item = shift;

    my $case = $item->object_data->get_source_object;

    $case->mail_action({
        case => $case,
        action_data => $item->data,
    });

    return;
}

sig send_subject_relation_email => 'Zaaksysteem::Backend::Object::Queue::Component';

sub send_subject_relation_email {
    my $self = shift;
    my $item = shift;

    $self->log->info(sprintf(
        "Sending authorization notification email for subject '%s'",
        $item->data->{subject_uuid}
    ));

    my $case = $item->object_data->get_source_object;

    my %subject_types = (
        employee => 'employee',
        person => 'person',
        organization => 'company',
    );

    $self->case_model->send_pip_email(
        $case,
        {
            send_auth_confirmation => 1,
            pip_authorized => 1,
            subject => {
                reference => $item->data->{subject_uuid},
                type => $subject_types{ $item->data->{subject_type} },
            }
        }
    );

    return;
}

sig send_case_assignee_email => 'Zaaksysteem::Backend::Object::Queue::Component';

sub send_case_assignee_email {
    my $self = shift;
    my $item = shift;

    my $case = $item->object_data->get_source_object;

    $self->log->info(sprintf(
        "Sending assignment notification email for case '%s'",
        $case->id
    ));

    $self->case_model->send_case_assignment_mail($case);

    return;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016-2019, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
