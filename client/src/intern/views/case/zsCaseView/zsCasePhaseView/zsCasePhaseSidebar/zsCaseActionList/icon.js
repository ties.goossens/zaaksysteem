// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
export const iconMappings = {
  allocation: 'account-multiple',
  defaults: 'play-circle-outline',
  email: 'email',
  template: 'file-outline',

  case(action) {
    let icon;

    switch (action.data.relatie_type) {
      case 'deelzaak':
        icon = 'folder-download';
        break;

      case 'vervolgzaak':
      case 'vervolgzaak_datum':
        icon = 'folder-move';
        break;

      case 'gerelateerd':
        icon = 'folder-multiple-outline';
        break;
    }

    return icon;
  },

  subject(action) {
    let icon;

    switch (action.data.betrokkene_type) {
      case 'natuurlijk_persoon':
        icon = 'account';
        break;

      case 'bedrijf':
        icon = 'domain';
        break;
    }

    return icon;
  },
};

export default function getIcon(action) {
  let getter = iconMappings[action.type] || iconMappings.defaults,
    icon = getter;

  if (typeof getter === 'function') {
    icon = getter(action);
  }

  return icon;
}
