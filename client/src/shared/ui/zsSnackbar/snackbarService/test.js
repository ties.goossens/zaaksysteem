// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import 'angular-mocks';
import snackbarServiceModule from '.';

describe('snackbarService', () => {
  let $rootScope;
  let $timeout;
  let $q;
  let snackbarService;

  beforeEach(angular.mock.module(snackbarServiceModule));

  beforeEach(
    angular.mock.inject([
      '$rootScope',
      '$timeout',
      '$q',
      'snackbarService',
      (...rest) => {
        [$rootScope, $timeout, $q, snackbarService] = rest;
      },
    ])
  );

  test('should throw if message is undefined', () => {
    expect(() => {
      snackbarService.add();
    }).toThrow();
  });

  test('should add an info snack', () => {
    snackbarService.info('foo');

    expect(snackbarService.getSnacks().length).toBe(1);
    expect(snackbarService.getSnacks()[0].type).toBe('info');
  });

  test('should add an error snack', () => {
    snackbarService.error('err');

    expect(snackbarService.getSnacks().length).toBe(1);
    expect(snackbarService.getSnacks()[0].type).toBe('error');
  });

  test('should remove a snack', () => {
    let snack = snackbarService.info('foo');

    expect(snackbarService.getSnacks().length).toBe(1);
    snackbarService.remove(snack);
    expect(snackbarService.getSnacks().length).toBe(0);
  });

  test('should set a default timeout if undefined', () => {
    let snack = snackbarService.info('foo');

    expect(snack.timeout).toBeGreaterThan(0);
  });

  test('should persist error snacks', () => {
    let snack = snackbarService.error('foo', { timeout: 1 });

    expect(snack.timeout).toBe(0);
  });

  test('should remove a snack after a timeout', () => {
    snackbarService.info('foo');
    expect(snackbarService.getSnacks().length).toBe(1);
    $timeout.flush();
    expect(snackbarService.getSnacks().length).toBe(0);
  });

  test('should cancel the timeout if the snack is removed earlier', () => {
    let snack = snackbarService.info('foo');

    snackbarService.remove(snack);
    expect($timeout.flush).toThrow();
  });

  describe('when adding a wait snack', () => {
    let deferred;
    let label = 'foo';
    let then;
    let catchHandler;
    let resolved;
    let rejected;
    let returnValue;
    let data = { foo: 'bar' };
    let snack;

    const resolvePromise = () => {
      deferred.resolve(data);
      $rootScope.$digest();
    };

    const rejectPromise = () => {
      deferred.reject(data);
      $rootScope.$digest();
    };

    beforeEach(() => {
      deferred = $q.defer();
      then = jasmine.createSpy('then');
      catchHandler = jasmine.createSpy('catch');
      resolved = jasmine.createSpy('resolve');
      rejected = jasmine.createSpy('reject');
      returnValue = snackbarService
        .wait(label, {
          promise: deferred.promise,
          then,
          catch: catchHandler,
        })
        .then(resolved)
        .catch(rejected);

      snack = snackbarService.getSnacks()[0];
    });

    test('should return a promise', () => {
      expect(returnValue).toBeDefined();
      expect('then' in returnValue).toBe(true);
    });

    test('should add a wait snack', () => {
      expect(snackbarService.getSnacks().length).toBe(1);
    });

    describe('when returning a label', () => {
      const returnedLabel = 'bar';

      beforeEach(() => {
        then.and.returnValue(returnedLabel);
        catchHandler.and.returnValue(returnedLabel);
      });

      describe('and it resolves', () => {
        beforeEach(resolvePromise);

        test('should remove the snack when the promise is fulfilled', () => {
          expect(snackbarService.getSnacks()[0]).not.toBe(snack);
        });

        test('should call the then handler with the resolve value from the promise', () => {
          expect(then).toHaveBeenCalledWith(data);
        });

        test('should add an info snack with the label returned', () => {
          expect(snackbarService.getSnacks().length).toBe(1);
          expect(snackbarService.getSnacks()[0].message).toBe(returnedLabel);
          expect(snackbarService.getSnacks()[0].type).toBe('info');
        });

        test('should resolve the promise with the created snack', () => {
          expect(resolved).toHaveBeenCalledWith(snackbarService.getSnacks()[0]);
        });
      });

      describe('and it rejects', () => {
        beforeEach(rejectPromise);

        test('should remove the snack when the promise is fulfilled', () => {
          expect(snackbarService.getSnacks()[0]).not.toBe(snack);
        });

        test('should call the then handler with the resolve value from the promise', () => {
          expect(catchHandler).toHaveBeenCalledWith(data);
        });

        test('should add an info snack with the label returned', () => {
          expect(snackbarService.getSnacks().length).toBe(1);
          expect(snackbarService.getSnacks()[0].message).toBe(returnedLabel);
          expect(snackbarService.getSnacks()[0].type).toBe('error');
        });

        test('should resolve the promise with the created snack', () => {
          expect(rejected).toHaveBeenCalledWith(snackbarService.getSnacks()[0]);
        });
      });
    });

    describe('without returning a label', () => {
      beforeEach(resolvePromise);

      test('shouldn’t add a snack', () => {
        expect(snackbarService.getSnacks().length).toBe(0);
      });

      // ZS-FIXME: this fails because the promise is resolved with `data`
      // (see the last line of `snackbarService.wait`).
      // Without more research we don't know if
      // - the implementation has been changed on purpose
      //   in order to achieve a specific side-effect
      //   without updating the test OR
      // - the test has been added on purpose
      //   in order to prevent a specific side-effect
      //   without fixing the implementation
      xtest('should resolve the promise with undefined', () => {
        expect(resolved).toHaveBeenCalledWith(undefined);
      });
    });
  });
});
