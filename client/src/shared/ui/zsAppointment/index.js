// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import includes from 'lodash/includes';
import template from './template.html';
import './styles.scss';

export default angular.module('zsAppointment', []).directive('zsAppointment', [
  () => {
    return {
      restrict: 'E',
      template,
      scope: {
        calendarUrl: '&',
        onSubmit: '&',
        appointmentId: '&',
      },
      bindToController: true,
      controller: [
        '$sce',
        '$scope',
        '$element',
        function ($sce, scope, $element) {
          let ctrl = this;
          const url = ctrl.calendarUrl();

          window.top.addEventListener('message', (event) => {
            if (includes(url, event.origin)) {
              if (event.data && event.data.type === 'appointmentSubmit') {
                ctrl.onSubmit({ value: event.data.data });
              }
            }
          });

          let iframe = document.createElement('iframe');

          iframe.src = $sce.trustAsResourceUrl(url);
          iframe.style.width = '100%';
          iframe.style.height = '600px';
          iframe.title = 'zsAppointment';
          iframe.allow = 'fullscreen; geolocation';
          iframe.addEventListener('load', () => {
            iframe.contentWindow.postMessage(
              {
                type: 'appointmentInit',
                data: {
                  id: ctrl.appointmentId(),
                },
              },
              '*'
            );
          });

          $element.find('zs-appointment-content').replaceWith(iframe);
        },
      ],
      controllerAs: 'vm',
    };
  },
]).name;
