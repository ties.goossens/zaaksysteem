package Zaaksysteem::Admin::PermissionMatrix;
use Zaaksysteem::Moose;

use List::Util qw(first none);

with qw(
    Zaaksysteem::Moose::Role::Schema
    Zaaksysteem::Moose::Role::Environment
);

has type => (
    isa      => 'Str',
    required => 1,
);

has employee_rs => (
    isa     => 'Zaaksysteem::Backend::Subject::ResultSet',
    lazy    => 1,
    builder => '_build_employee_rs',
);

sub _build_employee_rs {
    my $self = shift;
    my $rs   = $self->build_resultset('Subject')->employees->search_active;
    return $rs->search_rs(
        {
            'user_entities.active' => 1,
            group_ids              => { '!=' => undef },
        },
        { prefetch => 'user_entities' },
    );
}

has role_rs => (
    isa     => 'Zaaksysteem::Backend::Roles::ResultSet',
    lazy    => 1,
    builder => '_build_role_rs',
);

sub _build_role_rs {
    my $self = shift;
    return $self->build_resultset('Roles');
}

has group_rs => (
    isa     => 'Zaaksysteem::Backend::Groups::ResultSet',
    lazy    => 1,
    builder => '_build_group_rs',
);

sub _build_group_rs {
    my $self = shift;
    return $self->build_resultset('Groups');
}

has casetype_authorisation => (
    isa     => 'Defined',
    lazy    => 1,
    builder => '_build_casetype_authorisation',
);

sub _build_casetype_authorisation {
    my $self = shift;
    return $self->build_resultset(
        'ZaaktypeAuthorisation',
        {
            'zaaktype_id.deleted' => { '=' => undef, },
            'zaaktype_id.active'  => 1
        },
        {
            prefetch => [qw(zaaktype_id zaaktype_node_id)],
            join     => 'zaaktype_id',
            # recht is ordered for us because zaak_beheer, zaak_edit, zaak_read
            # and zaak_search are already ordered. who would have thought we
            # did something this smart ;)
            order_by => [ qw(me.zaaktype_node_id me.recht) ],
        }
    );
}

=head1 NAME

Zaaksysteem::Admin::PermissionMatrix - Position matrix model for exporting

=head1 DESCRIPTION

=head1 SYNOPSIS

    use Zaaksysteem::Admin::PermissionMatrix;
    use Data::Dumper;

    my $model = Zaaksysteem::Admin::PermissionMatrix->new(
        schema => $schema,
        type   => 'user_role',
    );

    # arrayref
    my $results = $model->search();

    foreach (@$results) {
        # Dumps a bunch of data that can be used by an export tool
        print Dumper $_;
    }

=cut

sub search {
    my $self = shift;

    my $type = $self->type;
    if ($type eq 'user_role') {
        return $self->_search_user_role;
    }
    elsif ($type eq 'user_permission') {
        return $self->_search_user_permissions_in_casetype;
    }

}

sub _get_user_group_mapping {
    my $self = shift;
    my $groups = shift;

    my $employees = $self->employee_rs;

    my %group_to_user;
    while (my $emp = $employees->next) {
      my $id = $emp->group_ids->[0];
      my $group = first { $_->id eq $id } @$groups;

      # no die, we want to continue
      next unless defined $group;

      foreach my $path (@{$group->path}) {
          push(@{$group_to_user{$path}}, $emp);
      }
    }

    return \%group_to_user;
}

sub _search_user_permissions_in_casetype {
    my $self = shift;

    my @groups = $self->group_rs->all;
    my @roles  = $self->role_rs->all;

    my $group_to_user_mapping = $self->_get_user_group_mapping(\@groups);

    my @lines;

    # Add the header
    my @header;
    push(
        @header,
        "Zaaktype",
        "Afdeling",
        "Rol",
        "Vertrouwelijkheid",
        "Rechten",
        "Gebruikersnaam",
        "Volledige naam",
        "E-mail",
    );
    push(@lines, \@header);

    my $casetype_auth = $self->casetype_authorisation;

    my (%group_names, %role_names);

    my %permission_mapping = (
        'zaak_read'   => 'Leesrechten',
        'zaak_search' => 'Zoekrechten',
        'zaak_beheer' => 'Beheerrechten',
        'zaak_edit'   => 'Behandelrechten',
    );

    my %seen;
    while (my $auth = $casetype_auth->next) {
        my $title = $auth->zaaktype_node_id->titel;
        my @casetype = ($title);

        my $id = $auth->get_column('zaaktype_id');

        my %casetype_users;

        my (%confidential, %public);

        my $gid = $auth->get_column('ou_id');
        my $rid = $auth->get_column('role_id');
        my $permission = $auth->get_column('recht');

        $group_names{$gid} //= $self->_get_nested_group_name($gid, \@groups);
        $role_names{$rid} //= $self->_get_role_name($rid, \@roles, \@groups, \%group_names);

        my @line = (
            $title,
            $group_names{$gid},
            $role_names{$rid},
            $auth->confidential ? 'Vertrouwelijk' : 'Intern',
            $permission_mapping{$permission}
        );

        my $users = $group_to_user_mapping->{$gid};
        my $in_role;
        foreach (@$users) {
            next if none { $rid eq $_ } @{$_->role_ids};
            $in_role //= 1;
            next if $seen{$auth->confidential}{$id}{$_->id};

            $seen{$auth->confidential}{$id}{$_->id}++;
            push(
                @lines,
                [
                    @line,                         $_->username,
                    $_->properties->{displayname}, $_->properties->{mail}
                ]
            );
        }

        if (!@$users) {
            my $msg =
            push(
                @lines,
                [
                    @line, "Geen gebruikers in afdeling aanwezig"
                ]
            );
        }
        elsif (!$in_role) {
            push(
                @lines,
                [
                    @line, "Geen gebruikers met rol aanwezig"
                ]
            );
        }
    }

    return \@lines;
}

sub _search_user_role {
    my $self = shift;

    my @groups = $self->group_rs->all;
    my @roles  = $self->role_rs->all;

    my $employees = $self->employee_rs;

    my @lines;

    # Add the header
    my @header;
    push(
        @header,
        qw(
            Rol
            Afdeling
            Gebruikersnaam
            E-mail
            Telefoonnummer
            Initialen
            Voornaam
            Achternaam
            ),
        "Volledige naam",
    );
    push(@lines, \@header);

    my %group_names;
    my %role_names;
    while (my $emp = $employees->next) {

        my $gid = $emp->group_ids->[0];
        $group_names{$gid} //= $self->_get_nested_group_name($gid, \@groups);

        my @emp_roles;
        foreach my $rid (@{ $emp->role_ids }) {

            $role_names{$rid} //=
                $self->_get_role_name($rid, \@roles, \@groups, \%group_names);

            push(@emp_roles, $role_names{$rid});
        }

        my $properties = $emp->properties;

        my $display_name = $properties->{displayname};
        my $surname      = $properties->{sn};
        my $given_name   = $properties->{givenname};
        my $initials     = $properties->{initials};
        my $telno        = $properties->{telephonenumber};
        my $email        = $properties->{mail};
        my $username     = $emp->username;

        my @emp;
        push(
            @emp,
            $group_names{$gid},
            $username,
            $email,
            $telno,
            $initials,
            $given_name,
            $surname,
            $display_name,
        );

        foreach (@emp_roles) {
            push(@lines, [ $_, @emp ]);
        }
    }

    return \@lines;
}

sub _get_role_name {
    my ($self, $rid, $roles, $groups, $group_names) = @_;

    my $role = first { $rid eq $_->id } @$roles;
    return "Onbekende rol met id $rid" unless $role;

    my $name = $role->name;

    if (my $p = $role->get_column('parent_group_id')) {

        my $main = first { $p == $_->id } @$groups;
        # The main group always has a path of 1
        return $role->name if @{$main->path} == 1;

        $group_names->{$p} //= $self->_get_nested_group_name($p, $groups);
        my $group = $group_names->{$p};
        return join("/", $group, $role->name);
    }
    else {
        return $role->name;
    }
}

sub _get_nested_group_name {
    my ($self, $gid, $groups) = @_;

    my $main = first { $gid == $_->id } @$groups;

    return "Onbekende groep met id $gid" unless $main;

    my @group_names;
    foreach my $id (@{ $main->path }) {
        my $group = first { $id == $_->id } @$groups;
        push(@group_names,
            $group ? $group->name : "Onbekende groep met id $gid");
    }
    return join("/", @group_names);
}


__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2020, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.

