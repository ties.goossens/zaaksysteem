// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import isEmpty from './isEmpty';

let isEqualShallow = (a, b) => {
  let key;

  for (key in a) {
    if (b[key] !== a[key]) {
      return false;
    }
  }

  for (key in b) {
    if (a[key] !== b[key]) {
      return false;
    }
  }

  return true;
};

export default (a, b) => {
  if (a === b) {
    return true;
  }

  let aEmpty = isEmpty(a),
    bEmpty = isEmpty(b);

  if (aEmpty !== bEmpty) {
    return false;
  }

  if (aEmpty && bEmpty) {
    return true;
  }

  if (a instanceof Date || b instanceof Date) {
    return a && b && a.getTime() === b.getTime();
  }

  return typeof a === 'object' || typeof b === 'object'
    ? isEqualShallow(a, b)
    : false;
};
