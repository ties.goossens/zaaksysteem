package Zaaksysteem::Constants::Locks;
use warnings;
use strict;

=head1 NAME

Zaaksysteem::Constants::Locks - Constants for database locks

=head1 DESCRIPTION

This module defined constants for database locks

=head1 SYNOPSIS

    use Zaaksysteem::Constants::Locks qw(SOME_LOCK);

    $schema->lock(SOME_LOCK);
    $schema->try_lock(SOME_LOCK);
    $schema->unlock(SOME_LOCK);

=cut

require Exporter;
our @ISA        = qw(Exporter);
our @EXPORT_OK  = qw(
    ADD_QUEUE_ITEM_LOCK
    BUITENBETER_GET_NEW_MELDINGEN_LOCK
    TOUCH_CASE_OBJECT_RELATIONSHIPS_LOCK
    TOUCH_CASE_LOCK
);

use constant ADD_QUEUE_ITEM_LOCK => 42;
use constant BUITENBETER_GET_NEW_MELDINGEN_LOCK => 3;
use constant TOUCH_CASE_OBJECT_RELATIONSHIPS_LOCK => 1;
use constant TOUCH_CASE_LOCK => 32;

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
