// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular.module('Zaaksysteem.case').directive('zsCaseView', [
    '$http',
    '$interpolate',
    '$q',
    '$timeout',
    'caseService',
    'objectService',
    'objectMutationService',
    'windowUnloadService',
    'systemMessageService',
    'safeApply',
    function (
      $http,
      $interpolate,
      $q,
      $timeout,
      caseService,
      objectService,
      objectMutationService,
      windowUnloadService,
      systemMessageService,
      safeApply
    ) {
      return {
        controller: [
          '$scope',
          '$element',
          '$attrs',
          function ($scope, $element, $attrs) {
            var ctrl = this,
              caseId,
              caseObj,
              phaseControls = [],
              loaded = false,
              hidden = {},
              promise,
              stashedChanges = {},
              savingChanges = {},
              resolvedValues = {},
              invalidate,
              runningRequestPromise,
              saving = {},
              saveTimeoutPromise;

            caseId = $interpolate($attrs.zsCaseId)($scope);

            ctrl.caseLoadListeners = [];
            ctrl.relations = {
              authorizations: [],
            };

            function setCaseObj(obj) {
              caseObj = obj;

              _.each(ctrl.updateListeners, function (fn) {
                fn(caseObj);
              });
            }

            function hasUnsavedChanges() {
              var hasUnsaved = _.some(
                angular.extend({}, stashedChanges, savingChanges),
                function (value, key) {
                  var isNotEqual = !objectService.isEqualValue(
                    value,
                    caseObj.values[key]
                  );
                  return isNotEqual;
                }
              );

              return hasUnsaved;
            }

            function saveValues() {
              var attributes = {},
                phases = [],
                names = {};

              saving.values = false;

              if (!hasUnsavedChanges()) {
                return;
              }

              if (runningRequestPromise) {
                runningRequestPromise.resolve();
                runningRequestPromise = null;
              }

              _.each(stashedChanges, function (value, key) {
                attributes[key] = resolvedValues[key];
              });

              _.each(phaseControls, function (ctrl) {
                _.each(ctrl.getAttributeNames(), function (name) {
                  names[name] = ctrl.getPhaseId();
                });
              });

              _.each(attributes, function (value, key) {
                var phaseId = names[key];
                if (phaseId !== undefined) {
                  phases.push(phaseId);
                } else if (
                  // make sure system attributes (not case attributes) get saved
                  _.indexOf(['case.result', 'case.confidentiality'], key) ===
                    -1 &&
                  key.indexOf('attribute.') !== 0
                ) {
                  delete attributes[key];
                  caseObj.values[key] = value;
                }
              });

              if (_.isEmpty(attributes)) {
                return;
              }

              saving.values = true;

              phases = _.uniq(phases);

              savingChanges = angular.copy(stashedChanges);
              stashedChanges = {};
              resolvedValues = {};

              runningRequestPromise = $q.defer();

              if (saveTimeoutPromise) {
                $timeout.cancel(saveTimeoutPromise);
                saveTimeoutPromise = null;
              }

              $http({
                method: 'POST',
                url: '/api/case/' + caseId + '/attribute/save',
                data: {
                  fields: attributes,
                  phases: phases,
                },
                timeout: runningRequestPromise.promise,
              })
                ['finally'](function () {
                  savingChanges = {};
                  runningRequestPromise = null;
                  saveTimeoutPromise = $timeout(function () {
                    saveTimeoutPromise = null;
                    saveValues();
                  }, 2500);

                  saving.values = false;
                })
                .then(function (response) {
                  setCaseObj(response.data.result[0]);
                })
                ['catch'](function (/*response*/) {
                  systemMessageService.emitSaveError('uw wijzigingen');
                });
            }

            function onUnload() {
              if (ctrl.isSaving()) {
                if (hasUnsavedChanges() && !runningRequestPromise) {
                  saveValues();
                }
                return windowUnloadService.UNSAVED_CHANGES;
              }
            }

            ctrl.getCase = function () {
              return caseObj;
            };

            ctrl.getCaseId = function () {
              return caseId;
            };

            ctrl.setCaseValue = function (name, value, resolved) {
              if (
                ctrl.canChange() &&
                !objectService.isEqualValue(ctrl.getCaseValue(name), value)
              ) {
                stashedChanges[name] = value;
                resolvedValues[name] =
                  resolved !== undefined ? resolved : value;
                if (loaded) {
                  invalidate();
                }
              }
            };

            ctrl.getCaseValue = function (name) {
              var stashedValue =
                stashedChanges[name] !== undefined
                  ? stashedChanges[name]
                  : savingChanges[name];
              return stashedValue !== undefined
                ? stashedValue
                : caseObj
                ? caseObj.values[name]
                : undefined;
            };

            ctrl.isLoaded = function () {
              return loaded;
            };

            ctrl.reloadData = function () {
              promise = $http({
                method: 'GET',
                url: '/api/case/' + caseId,
                params: {
                  deep_relations: true,
                },
              })
                .success(function (response) {
                  caseObj = response.result[0];
                  loaded = true;

                  _.each(ctrl.caseLoadListeners, function (listener) {
                    listener(caseObj);
                  });

                  setCaseObj(caseObj);
                })
                .error(function (/*response*/) {
                  systemMessageService.emitLoadError('de zaak');
                });

              return promise;
            };

            ctrl.loadData = function () {
              if (!promise) {
                ctrl.reloadData();
              }
              return promise;
            };

            ctrl.addPhaseControl = function (ctrl) {
              phaseControls.push(ctrl);
            };

            ctrl.removePhaseControl = function (ctrl) {
              _.pull(phaseControls, ctrl);
            };

            ctrl.getNumMilestones = function () {
              return phaseControls.length;
            };

            ctrl.setAttributeVisibility = function (attributeName, visible) {
              hidden[attributeName] = !visible;
            };

            ctrl.isFieldVisible = function (attributeName) {
              return hidden[attributeName] !== true;
            };

            ctrl.hasUnsavedChanges = function () {
              return hasUnsavedChanges();
            };

            ctrl.isSaving = function () {
              return saving.mutation || saving.values;
            };

            ctrl.isValid = function () {
              var activePhaseName =
                  caseObj && caseObj.values
                    ? caseObj.values['case.phase']
                    : null,
                activePhaseCtrl;

              activePhaseCtrl = _.find(phaseControls, function (phaseCtrl) {
                return phaseCtrl.getPhaseName() === activePhaseName;
              });

              return activePhaseCtrl && activePhaseCtrl.isValid();
            };

            ctrl.canAdvance = function () {
              return (
                !ctrl.isSaving() && !ctrl.hasUnsavedChanges() && ctrl.isValid()
              );
            };

            ctrl.addMutation = function (objectTypeName, mutation) {
              var attrName = 'object.' + objectTypeName,
                mutations = caseObj.values[attrName];

              if (!mutations) {
                caseObj.values[attrName] = mutations = [];
              }

              mutations.push(mutation);

              saving.mutation = true;

              return objectMutationService
                .addMutation(ctrl.getCaseId(), objectTypeName, mutation)
                .then(function (data) {
                  setCaseObj(data);
                })
                ['catch'](function (error) {
                  if (error.type === 'object/mutation/target_locked') {
                    systemMessageService.emitError(
                      'Kan mutatie niet toevoegen: het bewuste object staat elders al in bewerking.'
                    );
                  } else {
                    systemMessageService.emitSaveError();
                  }
                  _.pull(mutations, mutation);

                  throw error;
                })
                ['finally'](function () {
                  saving.mutation = false;
                });
            };

            ctrl.removeMutation = function (objectTypeName, mutation) {
              var attrName = 'object.' + objectTypeName,
                mutations = caseObj.values[attrName],
                index = _.indexOf(mutations, mutation);

              _.pull(mutations, mutation);

              saving.mutation = true;

              return objectMutationService
                .removeMutation(ctrl.getCaseId(), mutation)
                .then(
                  function (data) {
                    setCaseObj(data);
                  },
                  function () {
                    mutations.splice(0, index, mutation);
                    systemMessageService.emitSaveError();
                  }
                )
                ['finally'](function () {
                  saving.mutation = false;
                });
            };

            ctrl.updateMutation = function (objectTypeName, mutation, values) {
              var oldVals = angular.copy(mutation.values);
              mutation.values = values;

              saving.mutation = true;

              return objectMutationService
                .updateMutation(ctrl.getCaseId(), mutation)
                .then(
                  function (data) {
                    setCaseObj(data);
                  },
                  function () {
                    mutation.values = oldVals;
                    systemMessageService.emitSaveError();
                  }
                )
                ['finally'](function () {
                  saving.mutation = false;
                });
            };

            ctrl.canChange = function () {
              return $scope.$eval($attrs.canChange);
            };

            ctrl.resolveCase = function (result, reason) {
              return caseService
                .resolveCase(caseId, result, reason)
                .then(function () {
                  caseObj.values['case.result'] = result;
                });
            };

            ctrl.updateListeners = [];

            ctrl.reloadData();

            invalidate = _.throttle(
              function () {
                safeApply($scope, function () {
                  if (!runningRequestPromise) {
                    saveValues();
                  }
                });
              },
              0,
              { leading: false, trailing: true }
            );

            windowUnloadService.register(onUnload);

            $scope.$on('$destroy', function () {
              windowUnloadService.unregister(onUnload);
            });

            return ctrl;
          },
        ],
        controllerAs: 'caseView',
      };
    },
  ]);
})();
