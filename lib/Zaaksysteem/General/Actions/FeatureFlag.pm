package Zaaksysteem::General::Actions::FeatureFlag;
use Zaaksysteem::Moose;

=head1 NAME

Zaaksysteem::General::Actions::FeatureFlag - Feature flag functions

=head1 DESCRIPTION

Enable/disable feature flags on the controller level

=head1 SYNOPSIS

=head1 METHODS

=cut

sub is_api_v0_via_db {
    my $self = shift;
    return $self->_is_split_brain_feature_flag('ZS_FEATURE_FLAG_SPLIT_BRAIN_v0');
}

sub is_search_via_db {
    my $self = shift;
    return $self->_is_split_brain_feature_flag('ZS_FEATURE_FLAG_SEARCH_ZAAK_META');
}

sub _is_split_brain_feature_flag {
    my $self = shift;
    my $env_var = shift;

    if (exists $self->req->params->{'split-brain'}) {
        return 1 if $self->req->params->{'split-brain'};
        return 0;
    }

    return 1 if $ENV{$env_var};
    return 0;
}

1;

__END__

=head1 SEE ALSO

L<Zaaksysteem::General>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2021, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
