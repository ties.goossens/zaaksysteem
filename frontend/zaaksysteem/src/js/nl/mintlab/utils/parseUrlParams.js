// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  window.zsDefine('nl.mintlab.utils.parseUrlParams', function () {
    return function (url) {
      var params = {},
        match = url.match(/\?(.*)$/),
        queryString = match ? match[1] : null,
        qs,
        tokens,
        re = /[?&]?([^=]+)=([^&]*)/g;

      if (!queryString) {
        return {};
      }

      qs = queryString.split('+').join(' ');

      while ((tokens = re.exec(qs))) {
        params[decodeURIComponent(tokens[1])] = decodeURIComponent(tokens[2]);
      }

      return params;
    };
  });
})();
