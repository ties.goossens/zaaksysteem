BEGIN;

ALTER TABLE zaak_meta ADD COLUMN unread_communication_count INTEGER;

-- Will be used in new "advanced search" for ">0" checks
CREATE INDEX zaak_meta_unread_communications_idx ON zaak_meta(unread_communication_count);

WITH zaak_unread_counts AS (
    -- Ensure counts are not < 0.
    SELECT case_id, GREATEST( SUM(unread_employee_count), 0 ) AS total FROM thread GROUP BY case_id
)
    UPDATE zaak_meta
        SET unread_communication_count = zaak_unread_counts.total
    FROM zaak_unread_counts
    WHERE zaak_unread_counts.case_id = zaak_meta.zaak_id;

UPDATE zaak_meta SET unread_communication_count = 0 WHERE unread_communication_count IS NULL;

ALTER TABLE zaak_meta
    ALTER COLUMN unread_communication_count SET DEFAULT 0,
    ALTER COLUMN unread_communication_count SET NOT NULL,
    ADD CONSTRAINT unread_minimum CHECK (unread_communication_count >= 0);

CREATE OR REPLACE FUNCTION zaak_meta_update_unread_count ()
    RETURNS TRIGGER
    LANGUAGE plpgsql
AS $$
    BEGIN
        UPDATE zaak_meta SET unread_communication_count = (SELECT GREATEST(SUM(unread_employee_count), 0) FROM thread WHERE case_id = NEW.case_id) WHERE zaak_id = NEW.case_id;

        RETURN NEW;
    END
$$;

DROP TRIGGER IF EXISTS update_zaak_meta_count ON thread;
CREATE TRIGGER update_zaak_meta_count
    AFTER INSERT OR UPDATE OF case_id, unread_employee_count ON thread
    FOR EACH ROW
    WHEN (NEW.case_id IS NOT NULL)
    EXECUTE PROCEDURE zaak_meta_update_unread_count();

COMMIT;
