// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import angularUiRouterModule from 'angular-ui-router';
import reactIframeModule from '../../../../shared/ui/zsReactIframe';
import template from './template.html';
import './styles.scss';

export default angular
  .module('zsCaseRelationView', [angularUiRouterModule, reactIframeModule])
  .directive('zsCaseRelationView', [
    () => {
      return {
        restrict: 'E',
        template,
        scope: {
          caseUuid: '&',
          reloadCase: '&',
        },
        bindToController: true,
        controller: [
          function () {
            let ctrl = this;

            ctrl.getStartUrl = () => {
              return `/main/case/${ctrl.caseUuid()}/relations`;
            };
          },
        ],
        controllerAs: 'vm',
      };
    },
  ]).name;
