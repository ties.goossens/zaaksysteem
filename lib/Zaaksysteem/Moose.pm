package Zaaksysteem::Moose;

use Moose ();
use BTTW::Tools ();
use namespace::autoclean ();
use Moose::Exporter;
use Import::Into;
use Moose::Util::MetaRole;

Moose::Exporter->setup_import_methods(
    with_meta => ['has'],
    as_is     => [map { 'BTTW::Tools::' . $_ } @BTTW::Tools::EXPORT],
    also      => ['Moose'],
);

=head1 NAME

Zaaksysteem::Moose - A default Moose class for Zaaksysteem

=head1 DESCRIPTION

=head1 SYNOPSIS

    package Foo;
    use Zaaksysteem::Moose;

    has foo => (
        isa => 'Foo',
        is  => 'ro'     # defaults to ro
    );

=cut

sub init_meta {
    my $class     = shift;
    my %params    = @_;
    my $for_class = $params{for_class};
    Moose->init_meta(@_);

    BTTW::Tools->import::into(1);
    namespace::autoclean->import(
        -cleanee => $for_class,
    );

    Moose::Util::MetaRole::apply_base_class_roles(
        for   => $for_class,
        roles => ['MooseX::Log::Log4perl'],
    );

    return $for_class->meta();

}

sub has {
    my ($meta, $name, %options) = @_;

    $options{is} //= 'ro';

    # "has [@attributes]" versus "has $attribute"
    foreach ( 'ARRAY' eq ref $name ? @$name : $name ) {
        $meta->add_attribute( $_, %options );
    }
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2020, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
