package Zaaksysteem::Search::Case::ZQL::Command::Count;

use Moose;

extends 'Zaaksysteem::Search::ZQL::Command::Count';

=head1 NAME

Zaaksysteem::Search::Case::ZQL::Command::Count

=cut

sub _prepare_resultset {
    my ($self, $rs) = @_;
    return $rs;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
