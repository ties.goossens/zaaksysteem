package Zaaksysteem::Test::Backend::Rules::Serializer;
use Zaaksysteem::Test;

use Zaaksysteem::Backend::Rules::Serializer;

sub test_decode_rules {

    my $rule = {
        regel => 'Naam',
        foo   => "Bar",
        bar   => undef,
    };

    my $s = Zaaksysteem::Backend::Rules::Serializer->new(
        schema => mock_dbix_schema,
    );

    my $ds_rule = $s->serializer->serialize($rule);
    cmp_deeply($s->decode($ds_rule), $rule, "Old rule matches rule");

    cmp_deeply($s->decode_to_rule($ds_rule), $rule, "Prove decode_to_rule works");

    my $json_rule = $s->json->encode($rule);
    cmp_deeply($s->decode($json_rule), $rule, "New rule matches rule");

    my $native_rule = $s->encode($rule);
    cmp_deeply($s->decode($native_rule), $rule, "Prove encode/decode");
}


1;

__END__

=head1 NAME

Zaaksysteem::Test::Backend::Rules::Serializer - Test the rules serialisation

=head1 DESCRIPTION

Tests code for the module L<Zaaksysteem::Backend::Rules::Serializer>.

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
