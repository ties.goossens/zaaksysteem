package TestFor::General::ZTT;

# ./zs_prove -v t/lib/TestFor/General/ZTT.pm
use base 'ZSTest';

use TestSetup;

use File::Spec::Functions qw(catfile);
use DateTime::Format::Strptime qw(strptime);
use OpenOffice::OODoc;

use Zaaksysteem::ZTT;
use Zaaksysteem::ZTT::MagicDirective;
use Zaaksysteem::ZTT::Element;

sub process_template {
    my ($self, $case, $filestore) = @_;

    my %args = $filestore ? (filestore => $filestore) : ();
    my $sjabloon = $zs->create_sjabloon_ok(%args);

    my $tmp_dir = '/tmp';
    my $filename = "my_temp_filename";

    # we make 'operations' what does that mean? -- this function should
    # be renamed to whatever it exactly does
    my $success = $sjabloon->_filestore_operations({
        file   => catfile($tmp_dir, $filename),
        dir    => $tmp_dir,
        format => 'odt',
        case   => $case,
    });

    ok $success, "Successfully performed filestore operations";

    my $replaced_document = catfile($tmp_dir, $filename);

    ok -e $replaced_document, "Filled-in template available on disk";

    return odfDocument(
        container => $replaced_document,
        part => 'content'
    );
}

sub ztt_apply_formatter : Tests {
    my $ztt = Zaaksysteem::ZTT->new;

    my $date = '15-03-2013';
    my $element = Zaaksysteem::ZTT::Element->new(value => $date);

    $ztt->apply_formatter({ element => $element });
    is $element->value, $date, 'Without formatter value is unchanged';

    $ztt->apply_formatter({ element => $element, formatter => { name => 'not_date', args => [] } });
    is $element->value, $date, 'With unconfigured formatter value is unchanged';

    my $desired = strptime('%d-%m-%Y', $date)->set_locale('nl')->strftime('%{day} %B %Y');

    $ztt->apply_formatter({ element => $element, formatter => { name => 'date', args => [] } });
    is $element->value, $desired, 'With configured formatter value is changed';

    # negative scenario - bad input
    my $not_a_date = 'i am not a date';
    $element = Zaaksysteem::ZTT::Element->new(value => $not_a_date);

    {
        my @warnings;
        local $SIG{__WARN__} = sub { push(@warnings, shift) };
        $ztt->apply_formatter({ element => $element, formatter => { name => 'date', args => [] } });
        is $element->value, $not_a_date, 'When given incorrect value no formatting is done';
        subtest warnings => sub {
            is(@warnings, 1, "One warning found");
            like($warnings[-1], qr/Exception when ztt formatting:/, "And it is the developer warnings");
        }

    }
}

sub ztt_link_filter : Tests {
    my $ztt = Zaaksysteem::ZTT->new;

    my $id = 1337;
    my $element = Zaaksysteem::ZTT::Element->new(value => $id);

    $ztt->apply_formatter({ element => $element, formatter => {
        name => 'link_to',
        args => [
            'case',
            'Zaak %s'
        ]
    }});

    is $element->type, 'hyperlink', 'element type is richtext after applying link filter';
    is $element->value, 'case', 'element value contains link-like string';
    is $element->title, 'Zaak 1337', 'element value contains correct title';
}

sub ztt_image_size_filter : Tests {
    my $ztt = Zaaksysteem::ZTT->new();

    my $element = Zaaksysteem::ZTT::Element->new(value => 'some_image.jpg');
    $ztt->apply_formatter(
        {
            element => $element,
            formatter => {
                name => 'image_size',
                args => [ 13, 37 ],
            },
        }
    );

    is($element->type, 'image', 'Element type was set to "image"');
    is_deeply(
        $element->value,
        {
            image_path => 'some_image.jpg',
            width      => 13,
            height     => 37,
        },
        "Element value was set to a hash reference with the correct data"
    );
}

sub ztt_number_format_filter : Tests {
    {
        my $ztt = Zaaksysteem::ZTT->new();
        my $element = Zaaksysteem::ZTT::Element->new(value => '3133.7');
        $ztt->apply_formatter(
            {
                element => $element,
                formatter => {
                    name => 'currency',
                    args => [],
                },
            }
        );

        is(
            $element->value,
            "3.133,70",
            "Number rounded to 2 decimals, thousands separator added, 'nl' locale style"
        );
    }
    {
        my $ztt = Zaaksysteem::ZTT->new(locale => 'en_us');
        my $element = Zaaksysteem::ZTT::Element->new(value => '3133.7');
        $ztt->apply_formatter(
            {
                element => $element,
                formatter => {
                    name => 'currency',
                    args => [],
                },
            }
        );

        is(
            $element->value,
            "3,133.70",
            "Number rounded to 2 decimals, thousands separator added, 'en_us' locale style"
        );
    }
    {
        my $ztt = Zaaksysteem::ZTT->new();
        my $element = Zaaksysteem::ZTT::Element->new(value => '1000.00000');
        $ztt->apply_formatter(
            {
                element => $element,
                formatter => {
                    name => 'currency',
                    args => [ 'whole_number' ],
                },
            }
        );

        is(
            $element->value,
            "1.000",
            "Number rounded to 0 decimals, thousands separator included"
        );
    }
    {
        my $ztt = Zaaksysteem::ZTT->new();
        my $element = Zaaksysteem::ZTT::Element->new(value => '100');
        $ztt->apply_formatter(
            {
                element => $element,
                formatter => {
                    name => 'currency',
                    args => [],
                },
            }
        );

        is(
            $element->value,
            "100,00",
            "Number rounded to 2 decimals, no thousands separator needed"
        );
    }
    {
        my $ztt = Zaaksysteem::ZTT->new();
        my $element = Zaaksysteem::ZTT::Element->new(value => '100');
        $ztt->apply_formatter(
            {
                element => $element,
                formatter => {
                    name => 'currency',
                    args => [ 'whole_number' ],
                },
            }
        );

        is(
            $element->value,
            "100",
            "Number rounded to 0 decimals, no thousands separator needed"
        );
    }
}

sub ztt_plaintext : Tests {
    my $ztt = Zaaksysteem::ZTT->new;

    my $string = '123';
    my $datum = '12-05-2011';

    $ztt->add_context({
        string => $string,
        datum => $datum
    });

    is $ztt->process_template('[[string]]')->string, $string,
        'Normal string interpolation';

    is $ztt->process_template('[[datum]]')->string, $datum,
        'Normal date interpolation';

    is $ztt->process_template('[[string]] [[datum]]')->string, "$string $datum",
        'Combined interpolation';

    my $desired = strptime('%d-%m-%Y', $datum)->set_locale('nl')->strftime('%{day} %B %Y');

    is $ztt->process_template('[[string]] [[datum | date]]')->string, "$string $desired",
        'Combined interpolation with formatting';

    my $syntax_err = "[[ magic_string_ with_spaces ]]";

    is $ztt->process_template($syntax_err)->string, $syntax_err,
        'Syntax error in magic directive returns the full directive';

    my $syntax_maybe = "[[ a b ]] [[ string ]]";

    is $ztt->process_template($syntax_maybe)->string, '[[ a b ]] 123',
        'Syntax error in one directive does not break processing of other tags';

    my $mail_template = 'Dit is voor bug ZS-3710, zodat de - – tekens geen â€“ wordt';
    my $zs_3710 = $ztt->process_template($mail_template)->string;
    is $zs_3710, $mail_template, 'ZS-3710';
}

sub ztt_plaintext_list : Tests {
    my $ztt = Zaaksysteem::ZTT->new;

    $ztt->add_context({
        failsafe => 'this is a scalar value',
        simple => [qw[a b c d]],
        longer => [
            "This is a longer teststring.",
            "This is an even longer teststring, meant to see if linebreaks are actually inserted at the right point (after 76 chars, exclusive).",
            "There is a paragraph with junk data: wde./fjukihwe4l5igth8e98hq3e58o7wghp9348yq2o983r2hj3uiy4brlviusgfhkildft9ob8hxcdfliubz yuitsefvisxzy459g8phziulobno8fdg7hbsox8z7e4hyof4yhs5or897y",
            "And this is a forced multiline paragraph\nFor real yo!",
            "And this paragraph is styled\n\nlike multiple\n\nparagraphs in one yo!"
        ]
    });

    is $ztt->process_template('[[ failsafe | list ]]')->string, ' * this is a scalar value',
        'failsafe scalar-value is interpreted as single item list';

    # HALT! ACHTUNG!
    # The HEREDOC in $simple_render has trailing whitespace on a few lines
    # This is correct. Do not 'fix', or you will break this test.

    my $simple_render = <<'EOR';
a, 
b, 
c, 
d
EOR

    # HALT! ACHTUNG!
    # The HEREDOC in $simple_render has trailing whitespace on a few lines
    # This is correct. Do not 'fix', or you will break this test.

    chomp $simple_render;

    is $ztt->process_template('[[ simple ]]')->string, $simple_render,
        'simple non-formatted case has not regressed';

    my $multi_render = <<'EOR';
 * a

 * b

 * c

 * d
EOR

    chomp $multi_render;

    is $ztt->process_template('[[ simple | list ]]')->string, $multi_render,
        'simple multi-valued list';

    # HALT! ACHTUNG!
    # The HEREDOC in $longer_render has trailing whitespace on a few lines
    # This is correct. Do not 'fix', or you will break this test.

    my $longer_render = <<'EOR';
 * This is a longer teststring.

 * This is an even longer teststring, meant to see if linebreaks are
   actually inserted at the right point (after 76 chars, exclusive).

 * There is a paragraph with junk data:
   wde./fjukihwe4l5igth8e98hq3e58o7wghp9348yq2o983r2hj3uiy4brlviusgfhkildft
   9ob8hxcdfliubz
   yuitsefvisxzy459g8phziulobno8fdg7hbsox8z7e4hyof4yhs5or897y

 * And this is a forced multiline paragraph
   For real yo!

 * And this paragraph is styled
   
   like multiple
   
   paragraphs in one yo!
EOR

    # HALT! ACHTUNG!
    # The HEREDOC in $longer_render has trailing whitespace on a few lines
    # This is correct. Do not 'fix', or you will break this test.

    chomp $longer_render;

    is $ztt->process_template('[[ longer | list ]]')->string, $longer_render,
        'longer multi-valued list';
}

sub ztt_openoffice : Tests {
    my $self = shift;

    $zs->txn_ok(sub {
        my $case     = $zs->create_case_ok;
        my $document = $self->process_template($case);

        my @elements = map { $_->text } $document->selectElementsByContent('.*');

        my $formatted = pop @elements;
        my $unformatted = pop @elements;

        my $today = DateTime->now->set_locale('nl')->strftime('%{day} %B %Y');
        is $formatted, $today, 'Formatting as advertised';
        is $unformatted, DateTime->now->dmy, 'Unformatted version still cool';
    }, 'simple case openoffice templates work');
}

sub ztt_openoffice_richtext : Tests {
    my $self = shift;

    $zs->txn_ok(sub {
        my $filename = 'ztt_openoffice_richtext.odt';
        my $filestore = $zs->create_filestore_ok(
            file_path     => catfile('t/inc/Documents', $filename),
            original_name => $filename,
        );

        my $case = $zs->create_case_ok;

        my $kenmerk = $zs->create_bibliotheek_kenmerk_ok(
            naam => 'rich_text_test',
            magic_string => 'rich_text_attribute',
            value_type => 'richtext',
        );

        my $zaaktype_status = $case->zaaktype_node_id->zaaktype_statuses->first;

        $zs->create_zaaktype_kenmerk_ok(
            status              => $zaaktype_status,
            bibliotheek_kenmerk => $kenmerk,
        );

        my $value = '<ul><li>payload</li><li>payload2</li></ul><div><p class="something something">test value</p></div>';
        $case->zaak_kenmerken->replace_kenmerk({
            bibliotheek_kenmerken_id    => $kenmerk->id,
            zaak_id                     => $case->id,
            values                      => [$value]
        });

        my $document = $self->process_template($case, $filestore);

        my @elements = map { $_->text } $document->selectElementsByContent('.*');

        is scalar @elements, 4, '4 elements retrieved';

        my ($first, $second, $third, $fourth) = @elements;

        # verify html strippage
        ok $first !~ m/\<li\>/, '<li> element stripped out';
        ok $first =~ m/payload/, 'payload still intact';

        ok $second !~ m/\<li\>/, '<li> element stripped out';
        ok $second =~ m/payload2/, 'payload still intact';

        is $third, 'test value', 'HTML removed, only text remain';
        is $fourth, $case->registratiedatum->dmy, 'Second value matches regdate';
    }, 'richtext case openoffice templates works');
}

sub ztt_zs10079_regression : Tests {
    my $self = shift;

    $zs->txn_ok(sub {
        my $case = $zs->create_case_ok;

        my $attribute = $zs->create_bibliotheek_kenmerk_ok(
            naam => 'document kenmerk',
            magic_string => 'thingy',
            value_type => 'file'
        );

        my $casetype_document = $zs->create_zaaktype_kenmerk_ok(
            status => $case->zaaktype_node_id->zaaktype_statuses->first,
            bibliotheek_kenmerk => $attribute
        );

        $zs->create_file_ok(
            db_params => { accepted => 1, case => $case },
            case_document_ids => [ $casetype_document ]
        ) for 1..5;

        my $ztt = Zaaksysteem::ZTT->new;

        $ztt->add_context($case);

        is $ztt->process_template('[[ thingy ]]')->string, '',
            'Document magic string is empty';
    });
}

sub ztt_expression_evaluation : Tests {
    my $ztt = Zaaksysteem::ZTT->new;
    my $parser = Zaaksysteem::ZTT::MagicDirective->new;

    # expr_string => [ result, parsed_expr ]
    my %simple_expressions = (
        1 => [
            1,
            1
        ],

        '"foo"' => [
            "foo",
            "foo"
        ],

        '1 + 1' => [
            2,
            ['+', 1, 1]
        ],

        '2 / 2 + 4' => [
            5,
            ['+', ['/', 2, 2], 4]
        ],

        '2 / (2 + 4)' => [
            1/3,
            ['/', 2, ['+', 2, 4]]
        ],

        '10 - 1' => [
            9,
            ['-', 10, 1]
        ],

        '10 / 2' => [
            5,
            ['/', 10, 2]
        ],

        '1 / 2' => [
            0.5,
            ['/', 1, 2]
        ],

        'a - b' => [
            0,
            ['-', \'a', \'b']
        ],

        '1 / 0' => [
            undef,
            ['/', 1, 0]
        ],

        '1 == 1' => [
            1,
            ['==', 1, 1]
        ],

        ':pi' => [
            3.14159265359,
            { call => 'constant', args => ['pi'] }
        ],

        ':PI' => [
            3.14159265359,
            { call => 'constant', args => ['PI'] }
        ],

        '1 > 2' => [
            '',
            ['>', 1, 2]
        ],

        ':pi + 1 > :pi' => [
            1,
            [
                '>',
                ['+', { call => 'constant', args => ['pi'] }, 1],
                { call => 'constant', args => ['pi'] }
            ]
        ],

        ':pi + 1' => [
            4.14159265359,
            ['+', { call => 'constant', args => ['pi'] }, 1 ]
        ],

        '"my test string" ~= "test"' => [
            1,
            ["~=", "my test string", "test"]
        ],

        '"my test string" ~= "foo"' => [
            '',
            ["~=", "my test string", "foo"]
        ],

        '"my TEST string" ~= "test"' => [
            1,
            ["~=", "my TEST string", "test"]
        ],

        '"my ☭ string" ~= "☭"' => [
            1,
            ["~=", "my ☭ string", "☭"]
        ],

        '"abc" == ["abc"]' => [
            1,
            ["==", "abc", { call => 'array', args => [qw[abc]] }]
        ],

        '"abc" == ["abc", "def"]' => [
            '',
            ["==", "abc", { call => 'array', args => [qw[abc def]] }]
        ],

        '"abc, def" == ["abc", "def"]' => [
            1,
            ["==", "abc, def", { call => 'array', args => [qw[abc def]] }]
        ],

        '"abc, def, ghi" == ["abc", ["def", ["ghi"]]]' => [
            1,
            ["==", "abc, def, ghi", { call => 'array', args => [
                "abc",
                { call => 'array', args => [
                    "def",
                    { call => 'array', args => ["ghi"] }
                ]}
            ]}]
        ],

        '"a" in ["a", "b", "c"]', => [
            1,
            ["in", "a", { call => 'array', args => [qw[a b c]] } ]
        ],

        '"z" in ["a", "b", "c"]', => [
            '',
            ["in", "z", { call => 'array', args => [qw[a b c]] }]
        ],

        '30 in [10, 20, 30]' => [
            '1',
            ["in", "30", { call => 'array', args => [qw[10 20 30]] } ]
        ],

        '[1,2,3,4]' => [
            [ 1, 2, 3, 4 ],
            { call => 'array', args => [qw[1 2 3 4]] }
        ],

        '[1,2,[3,4]]' => [
            [ 1, 2, 3, 4 ],
            { call => 'array', args => [
                qw[1 2],
                { call => 'array', args => [qw[3 4]] }
            ] }
        ],

        '[[1,2],[3,4]]' => [
            [ 1, 2, 3, 4 ],
            { call => 'array', args => [
                { call => 'array', args => [qw[1 2]] },
                { call => 'array', args => [qw[3 4]] }
            ] }
        ],

        'eval("1 + 1")' => [
            2,
            { call => 'eval', args => ["1 + 1"] }
        ],

        'eval("@") + 1337' => [
            1337,
            ['+', { call => 'eval', args => ['@'] }, 1337 ]
        ],

        'deparse(1 + 1)' => [
            '("1" + "1")',
            { call => 'deparse', args => [['+', 1, 1]] }
        ],

        'and(1, 1)' => [
            1,
            { call => 'and', args => [qw[1 1]] }
        ],

        'and(1, 0)' => [
            '',
            { call => 'and', args => [qw[1 0]] }
        ],

        'or(1, 0)' => [
            1,
            { call => 'or', args => [qw[1 0]] }
        ],

        'or(0, 0)' => [
            '',
            { call => 'or', args => [qw[0 0]] }
        ],

        'or(1, 1)' => [
            1,
            { call => 'or', args => [qw[1 1]] }
        ],

        'and(or(1, 0), or(0, 1))' => [
            1,
            {
                call => 'and',
                args => [
                    { call => 'or', args => [qw[1 0]] },
                    { call => 'or', args => [qw[0 1]] }
                ]
            }
        ],

        'and(1, 1, 1, 1, 1, 1)' => [
            1,
            { call => 'and', args => [qw[1 1 1 1 1 1]] }
        ],

        'or(0, 0, 0, 0, 0, 1)' => [
            1,
            { call => 'or', args => [qw[0 0 0 0 0 1]] }
        ],

        'not(1)' => [
            '',
            { call => 'not', args => [1] }
        ],

        'not(and(0, 1))' => [
            1,
            {
                call => 'not', args => [
                    { call => 'and', args => [qw[0 1]] }
                ]
            }
        ]
    );

    for (keys %simple_expressions) {
        subtest "parse/eval/deparse '$_'" => sub {
            my ($val, $test_expression) = @{ $simple_expressions{ $_ } };

            my $expression = eval { $parser->parse($_)->{ expression } };

            is_deeply $expression, $test_expression, sprintf('parse(\'%s\')', $_);

            is_deeply $ztt->eval_expression($expression), $val, sprintf(
                'eval(\'%s\') = %s',
                $_,
                defined $val ? ($val || '""') : '<undef>'
            );

            my $deparse = $parser->deparse($expression);

            my $expr2 = $parser->parse($deparse)->{ expression };

            is_deeply $expr2, $expression, sprintf(
                'deparse(\'%s\') is stable',
                $_
            );

            my $sweetened_deparse = $parser->deparse($expression, 1);

            my $expr3 = $parser->parse($sweetened_deparse)->{ expression };

            is_deeply $expr3, $expression, sprintf(
                'deparse(\'%s\', 1) is stable',
                $_
            );
        }
    }

    is_deeply $ztt->eval_expression($parser->parse('[]')->{ expression }), [],
        'identity array self-evaluates';
}

sub zs_10267_regression : Tests {
    my $self = shift;

    $zs->txn_ok(sub {
        my $node = $zs->create_zaaktype_node_ok;
        my $casetype = $zs->create_zaaktype_ok(node => $node);

        my $status = $zs->create_zaaktype_status_ok(
            status => 1,
            fase => 'Registratiefase',
            node => $node
        );

        my $txt_attr = $zs->create_zaaktype_kenmerk_ok(
            status => $status,
            bibliotheek_kenmerk => $zs->create_bibliotheek_kenmerk_ok(
                magic_string => 'txt_attr',
                value_type => 'text'
            )
        );

        my $opt_attr = $zs->create_zaaktype_kenmerk_ok(
            status => $status,
            bibliotheek_kenmerk => $zs->create_bibliotheek_kenmerk_ok(
                magic_string => 'opt_attr',
                value_type => 'option',
                values => [
                    { value => '10', active => 1 },
                    { value => '20', active => 1 }
                ]
            )
        );

        my $sel_attr = $zs->create_zaaktype_kenmerk_ok(
            status => $status,
            bibliotheek_kenmerk => $zs->create_bibliotheek_kenmerk_ok(
                magic_string => 'sel_attr',
                value_type => 'select',
                values => [
                    { value => '100', active => 1 },
                    { value => '200', active => 1 }
                ]
            )
        );

        my $case = $zs->create_case_ok(zaaktype => $casetype);

        $case->zaak_kenmerken->replace_kenmerk({
            bibliotheek_kenmerken_id => $txt_attr->bibliotheek_kenmerken_id->id,
            zaak_id => $case->id,
            values => [ '2' ]
        });

        $case->zaak_kenmerken->replace_kenmerk({
            bibliotheek_kenmerken_id => $opt_attr->bibliotheek_kenmerken_id->id,
            zaak_id => $case->id,
            values => [ '20' ]
        });

        $case->zaak_kenmerken->replace_kenmerk({
            bibliotheek_kenmerken_id => $sel_attr->bibliotheek_kenmerken_id->id,
            zaak_id => $case->id,
            values => [ '200' ]
        });

        my $ztt = Zaaksysteem::ZTT->new;

        $ztt->add_context($case);

        my $tpl = $ztt->process_template(
            '[[ txt_attr + opt_attr + sel_attr ]]'
        );

        is $tpl->string, '222',
            'ZS-10267 regression (calculating with option/select attributes)';
    });
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
