BEGIN;	
ALTER TABLE zaaktype_kenmerken ADD COLUMN uuid UUID UNIQUE NOT NULL DEFAULT uuid_generate_v4();
COMMIT;