package Zaaksysteem::Geo::BAG::Connection::Nationaalgeoregister;
use Zaaksysteem::Moose;

with 'Zaaksysteem::Geo::BAG::Connection';

use BTTW::Tools::UA qw(new_user_agent);
use JSON::MaybeXS;
use Zaaksysteem::StatsD;

my $UA = new_user_agent(timeout => 30);

=head1 NAME

Zaaksysteem::Geo::BAG::Connection::Nationaalgeoregister - Search for BAG objects using public APIs of nationaalgeoregister.nl

=head1 SYNOPSIS

    my $connection = Zaaksysteem::Geo::BAG::Connection::Nationaalgeoregister->new();

    my $result = $connection->search("1234AA 94");

=head1 ATTRIBURES

=head2 ngr_geocoder_url

Base URL of the NGR/PDOK geocoder service.

API docs live at: https://github.com/PDOK/locatieserver/wiki/API-Locatieserver

=cut

has ngr_geocoder_url => (
    is => 'ro',
    isa => 'Str',
    builder => '_build_ngr_geocoder_url'
);

sub _build_ngr_geocoder_url {
    return $ENV{BAG_NGR_URL} // "https://geodata.nationaalgeoregister.nl/locatieserver/v3/free";
}

=head2 bag_wfs_url

Base URL of the Kadater/BAG WFS service.

=cut

has bag_wfs_url => (
    is => 'ro',
    isa => 'Str',
    builder => '_build_bag_wfs_url',
);

sub _build_bag_wfs_url {
    return $ENV{BAG_WFS_URL} // "https://geodata.nationaalgeoregister.nl/bag/wfs/v1_1";
}

=head2 search

Execute a BAG query, in the form returned by
L<Zaaksysteem::Geo::BAG::Model/_parse_query>.

=cut

define_profile search => (
    required => {
        type  => 'Str',
        query => 'Str',
    }
);

sub search {
    my $self = shift;
    my $args = assert_profile({@_})->valid;

    if (defined $args->{type} && !ref $args->{type}) {
        $args->{type} = [$args->{type}];
    }

    my $fq = _map_type_to_fq($args->{type});

    my $search_url = URI->new($self->ngr_geocoder_url);
    $search_url->query_form(
        q => $args->{query},
        fq => $fq,
    );

    my $search_result = $self->_json_api_call($search_url);

    return $search_result->{response}{docs};
}

=head2 get_exact

Search the BAG for an object that matches the given fields (full string/number match).

Accepts the following named arguments:

=over

=item * fields

A hash reference containing the fields to search for.

=back

=cut

define_profile get_exact => (
    required => {
        type   => 'Str',
        fields => 'HashRef',
    },
);

sub get_exact {
    my $self = shift;
    my $args = assert_profile({@_})->valid;

    my $fq = _map_type_to_fq([$args->{type}]);

    my @query_parts;


    push @query_parts, sprintf('postcode:"%s"', $args->{fields}{postcode});
    push @query_parts, sprintf('huisnummer:"%s"', $args->{fields}{huisnummer});

    if (defined $args->{fields}{huisnummer_toevoeging}) {
        push @query_parts, sprintf(
            'huisnummertoevoeging:"%s"',
            $args->{fields}{huisnummer_toevoeging}
        );
    } else {
        push @query_parts, '-huisnummertoevoeging:*';
    }

    if (defined $args->{fields}{huisletter}) {
        push @query_parts, sprintf(
            'huisletter:"%s"',
            $args->{fields}{huisletter}
        );
    } else {
        push @query_parts, '-huisletter:*';
    }

    my $ngr_url = URI->new($self->ngr_geocoder_url);
    $ngr_url->query_form(
        q => join(" AND ", @query_parts),
        fq => $fq,
    );

    my $location_data = $self->_json_api_call($ngr_url);
    if (@{ $location_data->{response}{docs} }) {
        return $location_data->{response}{docs};
    } else {
        # No entries returned by the geolocation service.
        return [];
    }
}

=head2 get

Retrieve a single BAG item by type and identifier.

=cut

define_profile get => (
    required => {
        type => 'Str',
        id   => 'Str',
    },
);

sub get {
    my $self = shift;
    my $args = assert_profile({@_})->valid;

    # If we end up getting wrong results using this query method (I haven't been
    # able to find any), prefix with `nummeraanduiding_id:` or `openbareruimte_id`:
    my $query = sprintf("%016d", $args->{id});

    $self->log->info("Getting location for $query");

    my $location = $self->_get_location_data($query);
    return unless $location;

    my $wfs_data = $self->_get_wfs_data($query, $location);

    return $wfs_data;
}

sub _get_location_data {
    my $self = shift;
    my ($query) = @_;

    my $ngr_url = URI->new($self->ngr_geocoder_url);
    $ngr_url->query_form(q => $query);

    my $location_data = $self->_json_api_call($ngr_url);

    return unless @{ $location_data->{response}{docs} };

    return $location_data->{response}{docs}[0];
}

sub _get_wfs_data {
    my $self = shift;
    my ($query, $location) = @_;

    # From the "Systeembeschrijving van de Basisregistratie Adressen en Gebouwen"
    # Objecttypecode  Kind of object
    # 01              Verblijfsobject
    # 02              Ligplaats
    # 03              Standplaats

    my $typecode = substr($location->{adresseerbaarobject_id}, 4, 2);

    my $typename = '';
    if ($typecode eq "01") {
        $typename = 'bag:verblijfsobject';
    } elsif($typecode eq "02") {
        $typename = 'bag:ligplaats';
    } elsif($typecode eq "03") {
        $typename = 'bag:standplaats';
    } else {
        $self->log->debug("Unknown type: $typecode ($location->{adresseerbaarobject_id})");
        return $location;
    }

    # Add WFS data
    my $wfs_url = URI->new($self->bag_wfs_url);
    $wfs_url->query_form(
        service=> 'wfs',
        version => '2.0.0',
        request => 'GetFeature',
        typeName => $typename,
        filter =>
            '<Filter>'.
                '<PropertyIsEqualTo>'.
                    '<PropertyName>identificatie</PropertyName>'.
                    '<Literal>' . $query . '</Literal>'.
                '</PropertyIsEqualTo>'.
            '</Filter>',
        outputFormat => 'json',
        srsName => 'epsg:4326',
    );

    my $wfs_data = $self->_json_api_call($wfs_url);
    if (@{ $wfs_data->{features} }) {
        $location->{wfs_data} = $wfs_data->{features}[0]{properties};
    } else {
        $location->{wfs_data} = {};
    }

    return $location;
}

=head2 find_nearest

Find the BAG object of the specified type that's nearest to the specified location

=cut

define_profile find_nearest => (
    required => {
        type      => 'Str',
        latitude  => 'Num',
        longitude => 'Num',
    },
);

sub find_nearest {
    my $self = shift;
    my $args = assert_profile({@_})->valid;

    my $nearest_url = URI->new($self->ngr_geocoder_url);
    $nearest_url->query_form(
        lat => $args->{latitude},
        lon => $args->{longitude},
        fq => "type:adres",
    );

    my $location_data = $self->_json_api_call($nearest_url);

    if (@{ $location_data->{response}{docs} }) {
        return $location_data->{response}{docs}[0];
    } else {
        # No entries returned by the geolocation service.
        return;
    }
}

sub _json_api_call {
    my $self = shift;
    my ($url) = @_;

    $self->log->trace("BAG API call: $url");

    my $api_response = $UA->get($url);
    if (!$api_response->is_success) {
        $self->log->warn("Error during BAG API call: " . $api_response->status_line);

        if ($self->log->is_trace) {
            $self->log->trace(sprintf(
                "HTTP response:\n----------------\n%s\n----------------",
                $api_response->as_string,
            ));
        }
        return;
    }

    return decode_json($api_response->decoded_content);
}

sub _map_type_to_fq {
    my ($type) = @_;

    my %wanted = (
        "openbareruimte" => 0,
        "nummeraanduiding" => 0,
    );

    for my $t (@$type) {
        $wanted{$t} = 1;
    }

    my $fq = 'type:';
    if ($wanted{openbareruimte} and $wanted{nummeraanduiding}) {
        $fq = $fq . "(adres OR weg)";
    } elsif ($wanted{openbareruimte}) {
        $fq = $fq . "weg";
    } elsif ($wanted{nummeraanduiding}) {
        $fq = $fq . "adres";
    } else {
        $fq = $fq . "adres";
    }

    return $fq;
}

__PACKAGE__->meta->make_immutable;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2021, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
