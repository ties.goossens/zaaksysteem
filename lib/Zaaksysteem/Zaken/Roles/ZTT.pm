package Zaaksysteem::Zaken::Roles::ZTT;

use Moose::Role;

use Zaaksysteem::Constants;

use Zaaksysteem::Object::Attribute;
use Zaaksysteem::Attributes;

use Zaaksysteem::Types::Filestore;
use BTTW::Tools;

requires qw(log);

=head1 NAME

Zaaksysteem::Zaken::Roles::ZTT - Magic String handling and Zaaksysteem Template Toolkit

=head1 SYNOPSIS

    my @magic_strings = @{ $case->list_magic_strings };

    ### Returns
    # zaak_nummer, zaaktype_titel, [...]

    my $object_attributes = $case->object_attributes

    ### Returns an arrayref with L<Zaaksysteem::Object::Attribute> objects


=head1 DESCRIPTION

Component Magic Strings within zaaksysteem, technical documentation

=head1 ATTRIBUTES

=head2 object_attributes

Return value: ArrayRef[Zaaksysteem::Object::Attribute]

     my $object_attributes = $case->object_attributes

Returns a list of L<Zaaksysteem::Object::Attribute>, together with their values

=cut

has 'object_attributes'    => (
    'is'            => 'ro',
    'lazy'          => 1,
    'isa'           => 'ArrayRef[Zaaksysteem::Object::Attribute]',
    'builder'       => '_build_attribute_list',
    'clearer'       => 'clear_object_attributes'
);

=head2 _ztt_cache

Case-localized cache for ZTT template processing

=cut

has _ztt_cache => (
    is => 'rw',
    isa => 'HashRef',
    lazy => 1, # Lazy because DBIx::Class::Rows don't get defaults init'd
    default => sub { return {}; }
);

=head2 casetype_attributes

This attribute builds a list of L<attributes|Zaaksysteem::Object::Attribute>
by processing L<casetype|Zaaksysteem::DB::Component::ZaaktypeNode>'s list of
associated L<kenmerken|Zaaksysteem::DB::Component::ZaaktypeKenmerken>.

=cut

has casetype_attributes => (
    is => 'ro',
    isa => 'ArrayRef[Zaaksysteem::Object::Attribute]',
    lazy => 1,
    traits => [qw[Array]],
    handles => { casetype_attribute_list => 'elements' },
    builder => 'casetype_object_attributes'
);

=head2 magic_strings

This attribute builds a map of magic strings to their respective values. The
keys for this map are based off the
L<Zaaksysteem::Object::Attribute/get_bwcompat_name> method and the attribute
name, so expect the same value to pop up multiple times when iterating over
the map carelessly.

=cut

has magic_strings => (
    is            => 'ro',
    lazy          => 1,
    default       => sub {
        my $self = shift;

        my $t0 = Zaaksysteem::StatsD->statsd->start;
        my $attributes = $self->object_attributes;
        Zaaksysteem::StatsD->statsd->end("ztt_zaak_magic_strings_object_attributes", $t0);

        my $magic_strings = {};

        for my $attribute (@$attributes) {

            my $builder = sub { $attribute->human_value };

            my @aliases = ($attribute->name, $attribute->get_bwcompat_name);

            $magic_strings->{$_} = $builder for @aliases;
        }

        return $magic_strings;
    }
);

=head1 METHODS

=head2 casetype_object_attributes

Returns an array containing L<Zaaksysteem::Object::Attribute> instances,
derived from the zaak's associated zaaktype node.

The method takes an optional list of magic strings which will be used to
constrain the returned instances. If none are provided, all associated
attributes will be derived.

=cut

sub casetype_object_attributes {
    my ($self, @magic_strings) = @_;

    my %attributes;

    my $magic_string_cond = scalar @magic_strings
        ? { -in => \@magic_strings }
        : { '!=' => undef };

    my $kenmerken = $self->zaaktype_node_id->zaaktype_kenmerken->search(
        {
            'me.is_group' => 0,
            'me.bibliotheek_kenmerken_id' => { '!=' => undef },
            'me.object_id' => undef,
            'bibliotheek_kenmerken_id.magic_string' => $magic_string_cond
        },
        { prefetch => 'bibliotheek_kenmerken_id' }
    );

    # Index the latest pending changes, bottom up to get the most recent
    # proposal. (seriously, this table has create/update timestamps but
    # nothing uses them)
    my %pending_changes = map {
        $_->parameters->{ bibliotheek_kenmerken_id } => $_
    } $self->scheduled_jobs->search(
        { task => 'case/update_kenmerk', deleted => undef },
        { order_by => { -asc => 'id' } }
    );

    while (my $kenmerk = $kenmerken->next) {
        my $bibliotheek_kenmerk = $kenmerk->bibliotheek_kenmerken_id;
        my $magic_string = $bibliotheek_kenmerk->magic_string;

        # kenmerken can occur multiple times in a casetype.
        next if exists $attributes{ $magic_string };

        my $type = $bibliotheek_kenmerk->value_type;
        my $type_definition = ZAAKSYSTEEM_CONSTANTS->{veld_opties}->{$type};

        my $attribute = Zaaksysteem::Object::Attribute->new(
            name           => sprintf('attribute.%s', $magic_string),
            label          => $kenmerk->label || $bibliotheek_kenmerk->label || $bibliotheek_kenmerk->naam,
            attribute_type => $type_definition->{ object_search_type } // 'text',
            bwcompat_type  => $type,
            bwcompat_name  => $magic_string,
            property_name  => $magic_string,
            grouping       => 'case',
            object_row     => $kenmerk,
        );

        my $pending_change = $pending_changes{ $bibliotheek_kenmerk->id };

        # If one exists, push the pending change for the attribute as well
        if (defined $pending_change) {
            $attribute->pending_change({
                %{ $pending_change->parameters },
                created_by_name => $pending_change->format_created_by
            });
        }

        $attributes{ $magic_string } = $attribute;
    }

    return [ values %attributes ];
}


=head2 systeemkenmerk

Convenience method that dereferences L</magic_strings> based on the provided
key.

    my $value = $case->systeemkenmerk('case.number');

=cut

sub systeemkenmerk {
    my ($self, $label) = @_;

    my $builder = $self->magic_strings->{ $label };

    return unless $builder;

    return try {
        return $builder->();
    }
    catch {
        $self->log->fatal($_);
        return "$label failed with $_";
    };

}

=head2 get_string_fetchers

This method implements a behavior required for the case object's usage as
a source of data for L<Zaaksysteem::ZTT>. It builds a list of coderefs that
take a 'magic string' and return a value if the fetcher is familiar with the
key.

    my @fetchers = $case->get_string_fetchers;

    for my $f (@fetchers) {
        my $v = $f->($magic_string);

        return $v if defined $v;
    }

B<Note>: this method is public to a case object, but should really only be
used by L<ZTT|Zaaksysteem::ZTT>.

=cut

sub get_string_fetchers {
    my $self = shift;
    my $cache = shift // {};

    my $t0 = Zaaksysteem::StatsD->statsd->start;

    my $ident = sprintf('zaak_%s', $self->id);

    my @fetchers;

    unless (exists $cache->{ $ident }) {
        $cache->{ $ident } = {};
    }

    my $_fetch_cache = $cache->{ $ident };

    # Involved subjects
    push @fetchers, sub {
        my $tag = shift;

        unless (exists $_fetch_cache->{ subjects }) {
            $_fetch_cache->{ subjects } = [
                $self->zaak_betrokkenen->search_gerelateerd->all
            ];
        }

        # Usual magic strings here look like 'aanvrager_naam' or 'ontvanger_correspondentie_huisnummer'
        my ($prefix, $tagname) = $tag->name =~ /^([^_]+)_(.*)$/;

        # TODO remove this iterator, search the db directly for related subjects
        # that match the prefix
        for my $betrokkene (@{ $_fetch_cache->{ subjects } }) {
            next unless ($prefix // '') eq $betrokkene->magic_string_prefix;

            my $value = ZAAKSYSTEEM_BETROKKENE_SUB->(
                $self->betrokkene_object({ magic_string_prefix => $prefix }),
                $tagname
            );

            my $rv = Zaaksysteem::ZTT::Element->new(
                value => ($value || '')
            );

            return $rv;
        }
        return;
    };

    # Case attributes
    push @fetchers, sub {
        my $tag = shift;

        # Strip loading attribute. prefix if it exists.
        my @name_parts = split m[\.], $tag->name;
        shift @name_parts if $name_parts[0] eq 'attribute';
        my $name = join '.', @name_parts;

        unless (exists $_fetch_cache->{ attributes }) {
            my $rs = $self->zaaktype_node_id->zaaktype_kenmerken->search(
                undef,
                { prefetch => 'library_attribute' }
            );

            $_fetch_cache->{ attributes } = {
                map {
                    $_->library_attribute->magic_string => $_
                } $rs->all
            };
        }

        my $attribute = $_fetch_cache->{ attributes }{ $name };

        unless ($attribute) {
            return;
        }

        # We don't support document value types yet, explicitly return empty
        # element
        if ($attribute->library_attribute->value_type eq 'file') {
            my $rv = Zaaksysteem::ZTT::Element->new();
            return $rv;
        }

        my $library_id = $attribute->get_column('bibliotheek_kenmerken_id');

        unless (exists $_fetch_cache->{ attribute_values }) {
            $_fetch_cache->{ attribute_values } = $self->field_values;
        }

        my $rv = Zaaksysteem::ZTT::Element->new(
            attribute => $attribute,
            value => $_fetch_cache->{ attribute_values }{ $library_id }
        );
        return $rv;
    };

    # Simple case, translate systemattributes
    push @fetchers, sub {
        my $tagname = shift->name;

        my $value;
        if (exists $_fetch_cache->{ system_attributes }{ $tagname }) {
            $value = $_fetch_cache->{ system_attributes }{ $tagname };
        } else {
            $value = $self->systeemkenmerk($tagname);
        }

        if (defined $value && ref $value eq 'CODE') {
            $value = $value->();
        }

        unless ($value || $tagname eq 'null') {
            return;
        }

        # Stringify the value, so Archive::Zip (use internally by
        # OpenOffice::OODoc) knows how to add the file later on.
        my %params = (value => "$value");
        $params{type} = 'image' if ($tagname eq 'behandelaar_handtekening');

        unless (exists $_fetch_cache->{ system_attributes }{ $tagname }) {
            # But don't stringify in the cache, so a potential "File::Temp"
            # object is kept alive.
            $_fetch_cache->{ system_attributes }{ $tagname } = $value;
        }

        my $rv = Zaaksysteem::ZTT::Element->new(%params);

        return $rv;
    };

    return @fetchers;
}

=head2 get_context_iterators

This method builds a map of 'subcontexts' for use by L<Zaaksysteem::ZTT>.
Basically, it tells the template processor the case is dereferenceable, in
the context of a L<ZTT|Zaaksysteem::ZTT> template, allowing the engine to
'unnest' any 1:N relations the case may have, and use them as data sources
as well.

B<Note>: this method is public to a case object, but should really only be
used by L<ZTT|Zaaksysteem::ZTT>.

=cut

sub get_context_iterators {
    my $self = shift;

    return {
        zaak_relaties => sub {
            return [ map { $_->case } $self->zaak_relaties ];
        },

        'case.related_cases' => sub {
            return [ map { $_->case } $self->zaak_relaties ];
        },

        'case.child_cases' => sub {
            return [
                $self->zaak_children->search(
                    undef,
                    { order_by => { -asc => 'me.id' } }
                )
            ];
        }
    };
}

=head2 set_casetype_attribute_value

Given a L<Zaaksysteem::Object::Attribute> object, this method will attempt to
set the L<value|Zaaksysteem::Object::Attribute/value>.

Optionally, a 'field_values' hashref can be provided from which the values
should be dereferenced. If missing, self's field_values will be dereferenced
and used for that purpose.

=cut

sub set_casetype_attribute_value {
    my $self = shift;
    my $attribute = shift;
    my $field_values = shift || $self->field_values;

    return unless $attribute->has_object_row;

    my $bibliotheek_kenmerk = $attribute->object_row->bibliotheek_kenmerken_id;

    if($bibliotheek_kenmerk->value_type eq 'file') {
        my $case_files = $self->file_field_documents($bibliotheek_kenmerk->id);

        my @files;
        for my $case_file (@$case_files) {
            push(
                @files,
                Zaaksysteem::Types::Filestore->new(
                    map ({ $_ => ($case_file->{ $_ } || undef) } @{ $Zaaksysteem::Types::Filestore::ATTRIBUTES })
                )
            );
        }

        $attribute->value($bibliotheek_kenmerk->filter(\@files));
        return;
    }

    my $row = $attribute->object_row;
    if ($row->can('filtered_value')) {
        $attribute->value($row->filtered_value);
        return;
    }

    my $value = $field_values->{ $bibliotheek_kenmerk->id };
    if ($bibliotheek_kenmerk->value_type ne 'select'
        && !$bibliotheek_kenmerk->can_have_multiple_values)
    {
        $value = $value->[0];
    }
    $attribute->value($bibliotheek_kenmerk->filter($value));
    return;
}

=head1 PRIVATE METHODS

=head2 _build_attribute_list

See L</object_attributes>.

=cut

sub _build_attribute_list {
    my $self   = shift;

    # It is deleted, so no use in doing all the other bits
    return [] if $self->is_deleted;

    my @attributes;

    my $t0 = Zaaksysteem::StatsD->statsd->start;
    push @attributes, $self->_build_casetype_attribute_list;
    Zaaksysteem::StatsD->statsd->end("zaak_ztt_build_casetype_attribute_list", $t0);

    my $t1 = Zaaksysteem::StatsD->statsd->start;
    push @attributes, $self->_build_case_attribute_list;
    Zaaksysteem::StatsD->statsd->end("zaak_ztt_build_case_attribute_list", $t1);

    return \@attributes;
}

=head2 _build_case_attribute_list

Builds an array of L<Zaaksysteem::Object::Attribute> instances for the
predefined case attributes.

=cut

sub _build_case_attribute_list {
    my $self = shift;

    my @list = Zaaksysteem::Attributes::predefined_case_attributes;

    $_->object_row($self) for @list;

    return @list;
}

=head2 _build_casetype_attribute_list

See L</casetype_attributes>.

=cut

sub _build_casetype_attribute_list {
    my $self = shift;

    my @list;

    for my $attribute ($self->casetype_attribute_list) {
        $self->set_casetype_attribute_value($attribute);
        push @list, $attribute;
    }

    return @list;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, 2018 Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules
