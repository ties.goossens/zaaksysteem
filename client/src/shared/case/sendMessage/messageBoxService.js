// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';

export default angular
  .module('messageBoxService', [])
  .factory('messageBoxService', [
    '$q',
    '$rootScope',
    'smartHttp',
    function messageBoxService($q, $rootScope, smartHttp) {
      return {
        getInterfaces: () => {
          const startUrl = '/api/v1/sysin/interface/get_all?rows_per_page=250';
          const moduleName = 'mijnoverheid';
          const defer = $q.defer();
          let allRows = [];

          function processPage(url) {
            smartHttp
              .connect({
                method: 'GET',
                url,
              })
              .then((response) => {
                const {
                  data: {
                    result: { instance },
                  },
                } = response;
                const { rows, pager } = instance;

                allRows = allRows.concat(rows);

                if (pager.next) {
                  processPage(pager.next);
                } else {
                  defer.resolve(
                    allRows.filter((row) => {
                      const { instance } = row;
                      return (
                        instance.module === moduleName &&
                        instance.interface_config.allow_attachments == 1
                      );
                    })
                  );
                }
              })
              .catch((error) => {
                defer.reject(error);
              });
          }

          processPage(startUrl);
          return defer.promise;
        },

        isSubscribed: (interfaceUuid, subjectUuid) => {
          return smartHttp
            .connect({
              method: 'GET',
              url: `/api/v1/sysin/interface/${interfaceUuid}/trigger/check_subscription?subject_uuid=${subjectUuid}`,
            })
            .then((response) => {
              const {
                data: {
                  result: { instance },
                },
              } = response;
              return instance.ok === true;
            });
        },

        submit: (interfaceUuid, subjectUuid, data) => {
          return smartHttp
            .connect({
              method: 'POST',
              url: `api/v1/sysin/interface/${interfaceUuid}/trigger/send_berichtenbox_message?subject_uuid=${subjectUuid}`,
              data,
            })
            .then((response) => {
              const {
                data: {
                  result: { instance },
                },
              } = response;
              if (instance.ok !== true) {
                return $q.reject(instance.message);
              }
            });
        },
      };
    },
  ]).name;
