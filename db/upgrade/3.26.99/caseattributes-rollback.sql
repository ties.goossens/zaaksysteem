BEGIN;

    ALTER TABLE bibliotheek_kenmerken ADD COLUMN type_multiple_new INTEGER;
    UPDATE bibliotheek_kenmerken SET type_multiple_new = 1 where type_multiple = true;
    UPDATE bibliotheek_kenmerken SET type_multiple_new = 0 where type_multiple = false;
    ALTER TABLE bibliotheek_kenmerken DROP COLUMN type_multiple;
    ALTER TABLE bibliotheek_kenmerken RENAME COLUMN type_multiple_new to type_multiple;

    ALTER TABLE zaaktype_kenmerken ADD COLUMN is_group_new INTEGER;
    UPDATE zaaktype_kenmerken SET is_group_new = 1 where is_group = true;
    UPDATE zaaktype_kenmerken SET is_group_new = 0 where is_group = false;
    ALTER TABLE zaaktype_kenmerken DROP COLUMN is_group;
    ALTER TABLE zaaktype_kenmerken RENAME COLUMN is_group_new to is_group;

    ALTER TABLE zaak_kenmerk ADD COLUMN attribute_values text;
    ALTER TABLE zaak_kenmerk ALTER COLUMN value DROP NOT NULL;

    DROP INDEX IF EXISTS zaak_kenmerk_bibliotheek_kenmerken_id;

    INSERT INTO zaak_kenmerk (zaak_id, bibliotheek_kenmerken_id, attribute_values)
        SELECT zaak_id, bibliotheek_kenmerken_id, unnest((value))
        FROM zaak_kenmerk
        GROUP BY
            zaak_id,
            bibliotheek_kenmerken_id,
            value
        ORDER BY zaak_id, bibliotheek_kenmerken_id
    ;

    DELETE FROM zaak_kenmerk WHERE value IS NOT NULL;
    ALTER TABLE zaak_kenmerk DROP COLUMN value;

    ALTER TABLE zaak_kenmerk ALTER COLUMN attribute_values SET NOT NULL;
    ALTER TABLE zaak_kenmerk RENAME COLUMN attribute_values TO value;

COMMIT;

VACUUM FULL zaak_kenmerk;
