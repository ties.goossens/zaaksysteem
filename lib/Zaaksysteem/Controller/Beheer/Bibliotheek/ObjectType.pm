package Zaaksysteem::Controller::Beheer::Bibliotheek::ObjectType;

use Moose;

BEGIN { extends 'Zaaksysteem::Controller' }

sub search
    : Chained('/beheer/bibliotheek/base')
    : PathPart('objecttype/search')
    : Args()
{
    my ( $self, $c )        = @_;

    return unless $c->req->is_xhr($c);
    $c->stash->{nowrapper} = 1;

    $c->stash->{bib_type}   = "objecttype";

    $c->stash->{bib_cat}        = $c->model('DB::BibliotheekCategorie')->search(
        {
            'system'    => undef,
        },
        {
            order_by    => ['pid','naam']
        }
    );

    my $params = $c->req->params();

    if ($params->{search}) {
        # Only return custom types.
        my $search_query = {
            'me.object_type' => 'type',
        };

        if ($params->{naam}) {
            $search_query->{'me.search_term'} = {
                'ilike' => '%' . lc($params->{naam}) . '%',
            };
        }

        if($params->{bibliotheek_categorie_id}) {
            $search_query->{'me.bibliotheek_categorie_id'} = $params->{bibliotheek_categorie_id};
        }

        my $types = $c->model('DB::ObjectBibliotheekEntry')->search(
            $search_query,
            {
                order_by => { '-asc' => 'name' },
                prefetch => 'bibliotheek_categorie_id',
            }
        );

        my @json;
        while (my $type = $types->next) {
            my $category = $type->bibliotheek_categorie_id;

            push(@json,
                {
                    'naam'          => $type->name,
                    'invoertype'    => 'object',
                    'categorie'     => $category ? $category->naam : "-",
                    'id'            => $type->id,
                    'uuid'          => $type->get_column('object_uuid'),
                }
            );
        }
        $c->stash->{json} = \@json;
        $c->detach('Zaaksysteem::View::JSONlegacy');
    }

    $c->stash->{template} = 'widgets/beheer/bibliotheek/search.tt';
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 search

TODO: Fix the POD

=cut

