use utf8;
package Zaaksysteem::Schema::CaseRelationshipJsonView;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Zaaksysteem::Schema::CaseRelationshipJsonView

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<Zaaksysteem::Result>

=cut

use base 'Zaaksysteem::Result';
__PACKAGE__->table_class("DBIx::Class::ResultSource::View");

=head1 TABLE: C<case_relationship_json_view>

=cut

__PACKAGE__->table("case_relationship_json_view");
__PACKAGE__->result_source_instance->view_definition(" SELECT case_relationship_view.case_id,\n    case_relationship_view.type,\n    jsonb_agg(json_build_object('type', 'case', 'reference', case_relationship_view.relation_uuid)) AS relationship\n   FROM case_relationship_view\n  GROUP BY case_relationship_view.case_id, case_relationship_view.type");

=head1 ACCESSORS

=head2 case_id

  data_type: 'integer'
  is_nullable: 1

=head2 type

  data_type: 'text'
  is_nullable: 1
  original: {data_type => "varchar"}

=head2 relationship

  data_type: 'jsonb'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "case_id",
  { data_type => "integer", is_nullable => 1 },
  "type",
  {
    data_type   => "text",
    is_nullable => 1,
    original    => { data_type => "varchar" },
  },
  "relationship",
  { data_type => "jsonb", is_nullable => 1 },
);


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2021-05-11 14:50:55
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:j9Kri/bgEo6w9QPfWDuEpQ


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2021, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
