// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import vormTemplateServiceModule from './../vormTemplateService';
import vormInvokeModule from './../vormInvoke';
import composedReducerModule from './../../api/resource/composedReducer';
import zsConfirmModule from './../../ui/zsConfirm';
import VormField from './VormField';

export default angular
  .module('vormField', [
    vormTemplateServiceModule,
    vormInvokeModule,
    composedReducerModule,
    zsConfirmModule,
  ])
  .directive('vormField', [
    () => {
      return {
        restrict: 'E',
        scope: {
          onChange: '&',
          value: '&',
          template: '&',
          compiler: '&',
          limit: '&',
          valid: '&',
          required: '&',
          pip: '&',
          disabled: '&',
          label: '@',
          description: '@',
          addLabel: '@',
          editMode: '&',
          templateData: '&',
          modelOptions: '&',
          locals: '&',
          autofocus: '&',
          parsers: '&',
          hideLabel: '&',
          confirmDelete: '&',
        },
        bindToController: true,
        controller: VormField,
        controllerAs: 'vm',
      };
    },
  ]).name;
