package Zaaksysteem::Backend::ObjectSubscription::ResultSet;

use Moose;

use BTTW::Tools;

use Zaaksysteem::Constants qw/STUF_SUBSCRIPTION_VIEWS/;

extends 'Zaaksysteem::Backend::ResultSet';

with 'MooseX::Log::Log4perl';


has '_active_params' => (
    is          => 'ro',
    lazy        => 1,
    default     => sub {
        return {
            date_deleted     => undef,
        }
    }
);

=head2 object_subscription_create

Creates a ObjectSubscription entry.

=head3 Arguments

=over

=item interface_id [required]
=item local_table [required]
=item local_id [required]
=item external_id [required]

=back

=head3 Returns

A newly created Sysin::Transaction object.

=cut

Params::Profile->register_profile(
    method  => 'object_subscription_create',
    profile => {
        required => [qw/
            interface_id
            local_table
            local_id
            external_id
        /],
        optional => [qw/
            object_preview
        /],
        constraint_methods => {
            interface_id => qr/^\d+$/,
            local_table  => qr/^\w+$/,
            local_id     => qr/^\d+$/,
        },
    }
);

sub object_subscription_create {
    my $self   = shift;
    my $opts   = assert_profile(
        {
            %{ $_[0] },
            schema  => $self->result_source->schema,
        },
    )->valid;

    # Check if the record actually exists
    my ($record) = $self->result_source->schema
        ->resultset($opts->{local_table})
        ->find({id => $opts->{local_id}});

    if (!$record) {
        throw '/sysin/object_subscription_create/record_not_found', sprintf(
            "Record with ID %d in class '%s' not found.",
            ($opts->{local_id}, $opts->{local_table})
        );
    }

    return $self->create($opts);
}

=head2 search_filtered

Does a search on transactions with a given filter. Input is as you would pass to
DBIx::Class.

B<Filters>

=over 4

=item interface_id

Filter on interface id

=item local_table

Filter on table, like "NatuurlijkPersoon"

=item local_id

Filter on id of internal entry

=item freeform_filter

Filter on freeform data, like the name of a subject, or the external_id

=item view_id

Filter on a specific view, see L<search_by_view> below for more information

=back

=cut

Params::Profile->register_profile(
    method  => 'search_filtered',
    profile => {
        required => [],
        optional => [qw/
            interface_id
            local_table
            local_id

            freeform_filter

            view_id
            selection_id
            selection_type
        /],
        constraint_methods => {
            is_error => qr/(0|1)/,
        },
        defaults => {
            'me.date_deleted' => undef,
        }
    }
);

sub search_filtered {
    my $self    = shift;
    my $options = assert_profile(shift || {})->valid;

    if ($options->{freeform_filter}) {
        $options->{'-or'} = [];

        if ($options->{freeform_filter} =~ /^\d+$/) {
            push(
                @{ $options->{'-or'} },
                { external_id => $options->{freeform_filter} },
            );

            push(
                @{ $options->{'-or'} },
                { local_id => $options->{freeform_filter} },
            );
        }

        push(
            @{ $options->{'-or'} },
            { object_preview => { like => '%' . $options->{freeform_filter} . '%' } },
        );

        delete($options->{freeform_filter});
    }

    if (($options->{selection_type} // '') ne 'all') {
        if ($options->{selection_id}) {
            $options->{'me.id'}  = {
                '-in' => (ref $options->{selection_id} eq 'ARRAY' ? $options->{selection_id} : [$options->{selection_id}] )
            };
        }
    }

    if ($options->{selection_id}) {
        delete($options->{selection_id});
    }

    my $view = delete $options->{view_id};
    delete $options->{selection_type};

    my $rs = $self->search(
        $options,
        {
            prefetch    => ['interface_id'],
            order_by    => {-desc => 'me.date_created'},
        }
    );

    if ($view) {
        $rs = $rs->search_by_view($view);
    }

    return $rs->search;

}

=head2 search_by_view

Arguments: $STRING_VIEW

Return value: L<DBIx::Class::ResultSet>

    my (@subscriptions) = $rs->search_by_view('subscriptions_with_cases');

Will filter the object subscriptions according one of the following views.

B<Possible views>

=over 4

=item subscriptions_with_cases

Return all subscriptions which have at least one case registered.

=item subscriptions_without_cases

Return all subscriptions which have no cases registered

=item subscriptions_without_cases_since_1d

Return all subscriptions which have no cases registered, and are more than one day old. Because
some subscriptions may be still in progress, e.g.: person is imported, but registration of case
is still running.

=item subscriptions_outside_municipality_without_cases

Return all subscriptions living outside this municipality with no registered cases.

=item subscriptions_outside_municipality_with_cases

Return all subscriptions living outside this municipality WITH registered cases.

=item subscriptions_desceased

Returns all the subscriptions for deceased persons (with and without cases)

=item subscriptions_desceased_without_cases

Returns all the subscriptions for deceased persons without cases

=item subscriptions_inactive

Returns all the subscriptions for inactive persons

=item subscriptions_inactive_without_cases

Returns all the subscriptions for inactive persons

=back

=cut

sub search_by_view {
    my $self        = shift;
    my $view        = shift;

    throw(
        'objectsubscription/resultset/search_by_view/non_existent',
        'Cannot find view: ' . $view
    ) unless STUF_SUBSCRIPTION_VIEWS->{$view};

    if ($view =~ /subscriptions_(?:desceased|inactive)/) {
        return $self->_get_subscriptions_for_deceased_and_inactive($view)->search();
    }

    my $cases   = $self->result_source->schema->resultset('Zaak')->search(
        {
            'me.deleted'   => undef
        },
        {
            columns     => 'aanvrager.gegevens_magazijn_id',
            distinct    => 1,
            join        => 'aanvrager'
        }
    );

    my $distinct_requestors = $cases->get_column('aanvrager.gegevens_magazijn_id')->as_query;
    my $subscriptions       = $self->search(
        {
            local_table         => 'NatuurlijkPersoon',
            $view eq 'subscriptions_without_cases_since_1d' ? ( date_created => { '<' => DateTime->now->subtract(days => 1) }) : (),
            'local_id::integer' => { ($view =~ /without/ ? 'not in' : 'in') => $distinct_requestors },
        }
    );

    my $np_rs = $self->result_source->schema->resultset('NatuurlijkPersoon');
    my $sub_query_subs = $subscriptions->get_column('local_id::integer')->as_query;
    my $nps;

    if ($view =~ /outside_municipality/) {
        $nps = $np_rs->search(
            {
                active       => 1,
                in_gemeente  => [0, undef],
                id => { 'in' => $sub_query_subs }
            }
        );
    }

    if ($nps) {
        $subscriptions = $subscriptions->search_rs(
            {
                'me.local_id::integer' =>
                    { 'in' => $nps->get_column('id')->as_query }
            }
            ,
            {order_by => 'local_id'}
        );
    }

    return $subscriptions->search();
}

=head2 _get_subscriptions_for_deceased_and_inactive

Get the subcription for deceased and inactive persons.
When a person is inactive we show the 'deleted' object subscription
This function works for the following views:

=over

=item subscriptions_desceased

=item subscriptions_desceased_without_cases

=item subscriptions_inactive

=item subscriptions_inactive_without_cases

=back

=cut

sub _get_subscriptions_for_deceased_and_inactive {
    my ($self, $view)  = @_;

    my $np_rs = $self->result_source->schema->resultset('NatuurlijkPersoon');

    my $inactive = 0;

    if ($view =~ /subscriptions_desceased/) {
        my $subscriptions = $self->search(
            {
                local_table         => 'NatuurlijkPersoon',
            }
        );
        my $sub_query_subs = $subscriptions->get_column('local_id::integer')->as_query;
        $np_rs = $np_rs->search_rs(
            {
                id => { 'in' => $sub_query_subs },
                datum_overlijden => { '!=' => undef },
            }
        );
    } else {
        $inactive = 1;
        $np_rs = $np_rs->search_rs({active => 0});
    }

    my $betrokkene = $self->result_source->schema->resultset('ZaakBetrokkenen')
        ->search_rs(
        {
            'me.betrokkene_type' => 'natuurlijk_persoon',
            'me.deleted'         => undef,
            'zaak_id.deleted'    => undef,
        },
        { join => 'zaak_id' },
    )->get_column('me.gegevens_magazijn_id')->as_query;

    if ($view =~ /without/) {
        $np_rs = $np_rs->search_rs({ id => { 'not in' => $betrokkene } });
    }
    else {
        $np_rs = $np_rs->search_rs({ id => { in => $betrokkene } });
    }

    if ($inactive) {
        # We don't want date_deleted to be undef because we want to show
        # these entries. Mess with the internals of DBIX::Class::Resultset
        if (exists $self->{attrs}{where}{'me.date_deleted'}) {
            delete $self->{attrs}{where}{'me.date_deleted'};
        }
    }

    return $self->search(
        {
            'me.local_table' => 'NatuurlijkPersoon',
            'me.local_id::integer' =>
                { 'in' => $np_rs->get_column('id')->as_query }
        },
        {
            order_by => 'local_id',
        }
    );
}

=head1 INTERNAL METHODS

=head2 _prepare_options

Allows Moose Roles to work on the options given to a create or update call.

=cut

sub _prepare_options { shift; return shift; }


1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

