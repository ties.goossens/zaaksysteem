BEGIN;
    /* db/upgrade/3.25.39/1000-oprah_behandelaar_role.sql */

-- Adds systemrole 'Behandelaar' for every subject which doesn't have one yet

UPDATE subject
SET role_ids = role_ids || (
	SELECT id
	FROM roles
	WHERE name = 'Behandelaar' AND system_role = true
)
WHERE id IN (
	SELECT id
	FROM subject
	WHERE NOT role_ids @> array_append(ARRAY[]::integer[], (
		SELECT id
		FROM roles
		WHERE name = 'Behandelaar' AND system_role = true
	))
);

    /* db/upgrade/3.25.39/1000-wordapp_in_config.sql */

--    INSERT INTO config (parameter, value, advanced) VALUES ('app_enabled_wordapp', '0', true);

    /* db/upgrade/3.25.39/1001-add_systemrole_appuser.sql */

INSERT INTO roles (parent_group_id, name, description, system_role, date_created)
       SELECT id, 'App gebruiker', 'Systeemrol: Verleent enkel toegang tot apps', true, NOW() FROM groups WHERE path = array[groups.id];

    /* db/upgrade/3.25.39/1002-insert_all_users_acl.sql */

-- Implementation of 'all users' via 'behandelaar' role is deprecated, insert
-- new tag-based security entities for all (case)types

INSERT INTO object_acl_entry (object_uuid, entity_type, entity_id, capability, scope)
    SELECT uuid, 'tag', 'all_users', 'read', 'instance' FROM object_data WHERE object_class IN ('casetype', 'type');

COMMIT;
