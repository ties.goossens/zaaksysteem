/*global $,document*/
$(document).ready(function () {
    "use strict";

    $(document).on('click', '.ezra_notificatie_dialog_submit', function () {
        var $button = $(this),
            $form = $button.closest('form'),
            $input = $form.find('input[name="notificaties_bibliotheek_notificaties_id"]'),
            value = $input.val();

        if (value && parseInt(value, 10) > 0) {
            $form.submit();
        } else {
            alert('Kies een e-mailsjabloon.');
        }
    });

});