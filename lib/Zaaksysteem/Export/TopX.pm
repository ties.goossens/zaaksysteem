package Zaaksysteem::Export::TopX;
use Zaaksysteem::Moose;

require Zaaksysteem::Export::TopX::Zaaksysteem;
require Zaaksysteem::Export::TopX::RIP;

sub export_type {
    my $self = shift;
    my %args = @_;

    if ($args{export_type} eq 'zaaksysteem') {
        return Zaaksysteem::Export::TopX::Zaaksysteem->new(%args);
    }
    if ($args{export_type} eq 'rip') {
        return Zaaksysteem::Export::TopX::RIP->new(%args);
    }

    throw("topx/export_type/unknown",
        "Unknown export type $args{export_type}");

}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2020, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
