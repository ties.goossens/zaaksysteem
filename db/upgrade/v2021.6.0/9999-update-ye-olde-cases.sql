
BEGIN;

    WITH main_cases AS (
      SELECT distinct(pid) as pid FROM zaak WHERE pid is not null
    )
    INSERT INTO queue (type, label, priority, metadata, data)
    SELECT
    'update_referential_attributes_migration',
    'Release: Referential attributes with main-case: ' || pid,
    950,
    -- metadata
    json_build_object(
      'require_object_model', 0,
      'target', 'backend'
    ),
    -- data
    json_build_object(
      'case_number', pid
    )
  FROM main_cases;

  ;
COMMIT;


