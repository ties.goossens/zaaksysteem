package Zaaksysteem::Zaken::Roles::KenmerkenObjecten;
use Moose::Role;

use Array::Utils qw(array_minus intersect array_diff);
use BTTW::Tools;
use List::Util qw(uniq);
use Zaaksysteem::Constants;
use Zaaksysteem::Object::Attribute;
use Zaaksysteem::Zaken::AdvanceResult;

requires qw(log);


=head2 field_values

get values of zaak_kenmerken (case fields)

bibliotheek_kenmerken_id:  limit the results on a single field.

=cut

define_profile field_values => (
    optional => {
        bibliotheek_kenmerken_id => 'Int',
        fase                     => 'Int',
        use_magic_string_keys    => 'Bool',
        apply_field_filters      => 'Bool',
        as_object_attributes     => 'Bool',
    }
);

sub field_values {
    my $self = shift;
    my $params = assert_profile(shift // {})->valid;

    my $request = { %$params, zaak_id => $self->id };
    my $cache_key = 'field_values?' . join "=", map { $_ . '=' . $request->{$_} } sort keys %$request;
    if (my $cached = $self->{cached_field_values}->{$cache_key}) {
        return $cached;
    }

    my $where = {};
    if(my $id = $params->{'bibliotheek_kenmerken_id'}) {
        $where->{bibliotheek_kenmerken_id} = $id;
    }

    my $options = {
        prefetch        => [
            'bibliotheek_kenmerken_id',
        ],
    };

    if($params->{fase}) {
        $where->{bibliotheek_kenmerken_id} = $self->_get_phase_field_ids({
            fase => $params->{fase},
        });
    }

    # Skip other items without bibliotheek_kenmerken_id (text blocks)
    if (!exists $where->{bibliotheek_kenmerken_id}) {
        $where->{bibliotheek_kenmerken_id} = { '!=' => undef };
    }

    my $kenmerken = $self->zaak_kenmerken->search($where, $options);
    my %kenmerk_values;

    if ($params->{as_object_attributes}) {
        while (my $kenmerk = $kenmerken->next) {
            my $magic_string = $kenmerk->get_column('magic_string');
            my $value        = $kenmerk->as_object_attribute();
            $kenmerk_values{$magic_string} = $value;
        }
    }
    else {
        while (my $kenmerk = $kenmerken->next) {
            my $column_name = $params->{use_magic_string_keys}
                ? $kenmerk->get_column('magic_string')
                : $kenmerk->get_column('bibliotheek_kenmerken_id');

            my $value = $params->{apply_field_filters}
                ? $kenmerk->filtered_value : $kenmerk->value;

            $kenmerk_values{$column_name} = $value;
        }
    }


    $self->{cached_field_values}->{$cache_key} = \%kenmerk_values;
    return \%kenmerk_values;
}

sub get_custom_attributes_from_casetype {
    my $self = shift;
    return $self->zaaktype_node_id->get_custom_attributes(
        undef,
        {
            columns  => [qw[bibliotheek_kenmerken_id]],
            distinct => 1
        }
    );
}

sub get_custom_attributes {
    my $self = shift;

    return $self->case_properties->search_rs({namespace => 'casetype'});
}

=head2 get_referential_attributes

Get all the referential attributes from a case as a resultset where you can
loop over. The only column is the C<magic_strings>.

=cut

sig get_referential_attributes => '?ArrayRef';

sub get_referential_attributes {
    my ($self, $attrs) = @_;

    my $rs = $self->zaaktype_node_id->zaaktype_kenmerken->search->search_related(
        'bibliotheek_kenmerken_id',
        {
            'me.referential' => 1,
            $attrs && @$attrs
            ? ('bibliotheek_kenmerken_id.magic_string' =>
                    { -in => $attrs })
            : (),
        },
        {
            columns => [
                qw(
                    bibliotheek_kenmerken_id.magic_string
                    )
            ],
            distinct => 1
        }
    );
    return $rs;
}

sig get_referential_file_attributes => '?ArrayRef';

sub get_referential_file_attributes {
    my $self = shift;

    my $rs = $self->zaaktype_node_id->zaaktype_kenmerken->search_rs(
        {
            'me.referential' => 1,
            'bibliotheek_kenmerken_id.value_type' => 'file'
        },
        {
            prefetch => 'bibliotheek_kenmerken_id',
            distinct => 1
        }
    );
    return $rs;
}

#
# to limit the retrieve value to only the values of a given phase, an in-query
# is generated that select that ids of the fields for the phase.
#
sub _get_phase_field_ids {
    my ($self, $params) = @_;

    my $first_status = $self->zaaktype_node_id->zaaktype_statussen->search(
        { status => $params->{fase}, }
    )->first;

    if (!$first_status) {
        return;
    }

    my $zaaktype_kenmerken_rs = $self->zaaktype_node_id->zaaktype_kenmerken->search(
        { zaak_status_id => $first_status->id, },
        {
            prefetch => ['bibliotheek_kenmerken_id', 'zaak_status_id'],
            order_by => 'me.id'
        }
    );

    return {
        -in => $zaaktype_kenmerken_rs->get_column('bibliotheek_kenmerken_id')
            ->as_query };
}


sub required_fields_missing {
    my ($self) = @_;

    my $statusses = $self->get_rule_statusses;

    while (my $status = $statusses->next) {
        my $rules_result = $self->phase_fields_complete({
            phase => $status
        });

        # as soon as we find a missing field we're done. outta here.
        if ($rules_result->{required}) {
            return $rules_result->{missing};
        }
    }

    return;
}

sub update_unaccepted_file_count {
    my $self = shift;

    $self->zaak_meta->update(
        { unaccepted_files_count => 0 + $self->files->search->unaccepted });
    return 1;

}

sub get_unaccepted_file_count {
    my $self = shift;
    return $self->zaak_meta->unaccepted_files_count;
}

sub update_unaccepted_attribute_updates {
    my $self   = shift;

    my $pending = $self->_get_pending_changes;
    $self->_update_pending_changes($pending);

    my $changes = scalar @{$pending};

    $self->zaak_meta->update({unaccepted_attribute_update_count => $changes})->discard_changes;
    $self->ddd_update_system_attribute('case.num_unaccepted_updates');
    return $changes;
}

sub _get_pending_changes {
    my $self = shift;

    my $rs = $self->result_source->schema->resultset('ScheduledJobs');
    my @list_changes = $rs->search_update_field_tasks({ case_id => $self->id })
        ->only_most_recent_field_update;
    return \@list_changes;
}

sub _update_pending_changes {
    my ($self, $pending) = @_;

    $pending //= $self->_get_pending_changes;

    my %changes;

    for my $pending_change (@{$pending}) {
        my %change = (
            %{ $pending_change->parameters },
            created_by_name => $pending_change->format_created_by,
        );
        $changes{$change{bibliotheek_kenmerken_id}} = \%change;
    }

    $self->zaak_meta->update({pending_changes => \%changes})->discard_changes;
    return;
}

sub unaccepted_pip_updates {
    my $self   = shift;
    return $self->zaak_meta->unaccepted_attribute_update_count;
}

around can_volgende_fase => sub {
    my $orig    = shift;
    my $self    = shift;

    my $advance_result = $self->$orig(@_);

    if (my $fields = $self->required_fields_missing) {
        $advance_result->fail('fields_complete');
        $advance_result->missing($fields);
    } else {
        $advance_result->ok('fields_complete');
    }

    if ($self->unaccepted_pip_updates) {
        $advance_result->fail('pip_updates_complete');
    } else {
        $advance_result->ok('pip_updates_complete');
    }

    return $advance_result;
};


=head2 file_field_documents

fields are retrieved using 'field_value', except files, which are
stored somewhere else. this is taken from the template and perlified.
If we are a child, we try to lookup referential documents.

=cut

sub file_field_documents {
    my ($self, $bibliotheek_kenmerken_id) = @_;

    if ($self->has_parent) {
        my $attribute = $self->zaaktype_node_id->zaaktype_kenmerkens->search_rs(
            {
                bibliotheek_kenmerken_id => $bibliotheek_kenmerken_id,
                referential              => 1,
                'bibliotheek_kenmerken_id.value_type' => 'file',
            },
            {
                rows => 1,
                join => 'bibliotheek_kenmerken_id',
            }
        )->single;

        if ($attribute) {
            my $parent = $self->get_parent;
            return $parent->file_field_documents($bibliotheek_kenmerken_id);
        }
    }

    return $self->_get_case_documents(library_id => $bibliotheek_kenmerken_id);

}

=head2 _get_case_documents

    $self->_get_case_documents(
        case       => $case,        # optional
        library_id => $libary_id    #required
    );

Get case documents based on a library ID.

=cut

sub _get_case_documents {
    my $self = shift;
    my %params = @_;

    my $case = $params{case} // $self;

    # the constructive solution is to modify the table 'file_case_document' so that it will
    # use bibliotheek_kenmerken_ids instead of zaaktype_kenmerken.
    my $files = $case->files->search_by_case_and_library_id($case->id, $params{library_id})->search(
        undef,
        { prefetch => 'filestore' }
    );

    my @rv = map {
        {
            filename        => $_->filename,
            file_id         => $_->id,
            mimetype        => $_->filestore->mimetype,
            accepted        => $_->accepted,
            size            => $_->filestore->size,
            md5             => $_->filestore->md5,
            uuid            => $_->filestore->uuid,
            original_name   => $_->filestore->original_name,
            thumbnail_uuid  => $_->filestore->thumbnail_uuid,
            is_archivable     => $_->filestore->is_archivable,
            confidential    => $_->confidential ? \1 : \0,
        }
    } $files->all;
    return \@rv;
}


=head2 unaccepted_file_field_documents

companion function to file_field_documents

=cut

sub unaccepted_file_field_documents {
    my ($self, $file_field_documents) = @_;

    return scalar grep { !$_->{accepted} } @$file_field_documents;
}

=head2 handle_attribute_triggers

Given an event, look up the potential triggers for it and process them.

=cut

sig handle_attribute_triggers => 'Zaaksysteem::Object::Queue::Model, Zaaksysteem::DB::Component::Logging';

sub handle_attribute_triggers {
    my ($self, $queue, $event) = @_;

    return unless $event->does(
        'Zaaksysteem::DB::Component::Logging::Case::Attribute::Update'
    );

    return unless $event->attribute->value_type eq 'geolatlon';

    my $ztk = $self->zaaktype_node_id->zaaktype_kenmerken->find(
        $event->event_data->{ casetype_attribute_id }
    );

    unless (defined $ztk) {
        $self->log->warn(sprintf(
            'Attempted to retrieve zaaktype_kenmerk row for event "%s" (%s), found nothing. Cannot process triggers for this event',
            $event->onderwerp,
            $event->id
        ));

        return;
    }

    return unless $ztk->properties->{ map_case_location };

    my ($lat, $lon) = split m[,], $event->event_data->{ attribute_value };

    my $item = $queue->create_item({
        type => 'update_case_location',
        object_id => $self->get_column('uuid'),
        label => 'Zaaklocatie instellen',
        disable_acl => 1,
        data => {
            latitude => $lat,
            longitude => $lon
        }
    });

    $queue->queue_item($item) if defined $item;

    return;
}

=head2 clear_field_values

Clearer for the field values cache

=cut

sub clear_field_values {
    my $self = shift;
    delete $self->{cached_field_values};
}

sig update_referential_attributes_in_children => 'HashRef';

sub update_referential_attributes_in_children {
    my ($self, $data) = @_;

    my $children = $self->get_children;

    while (my $child = $children->next) {
        $child->update_referential_attributes($data);
    }
    return 1;
}

=head2 get_casetype_attributes

Get the casetype attributes of a case. Optional parameter for getting
referential or non-referential attributes. This is dependent on if the case has
a parent.

    $case->get_casetype_attributes(0); # Non-referential attributes
    $case->get_casetype_attributes(1); # Referential attributes
    $case->get_casetype_attributes();  # Both referential and non-referential

=cut

sig get_casetype_attributes => '?Bool';

sub get_casetype_attributes {
    my ($self, $referential) = @_;

    return $self->zaaktype_node_id->zaaktype_kenmerken->search_related_rs(
        'bibliotheek_kenmerken_id',
        {
            defined $referential
                && $self->has_parent ? ('me.referential' => $referential) : (),
        },
        {
            columns => [
                qw(
                    bibliotheek_kenmerken_id.value_type
                    bibliotheek_kenmerken_id.magic_string
                    bibliotheek_kenmerken_id.id
                    )
            ],
            distinct => 1
        }
    );
}

=head2 update_referential_attributes_from_parent

Ask the case to update all the referential attributes from its parent.
Trickles down the tree which is a potential performance issue. The use case is
to call this after a case create call, so there will probably not be any
children present.

=cut

sub update_referential_attributes_from_parent {
    my $self = shift;
    return unless $self->has_parent;
    my $parent = $self->get_parent;

    $self->update_referential_attributes(
        $parent->field_values({ use_magic_string_keys => 1 })
    );

    $self->update_referential_file_attributes_from_parent;
    return;
}

sub update_referential_file_attributes_from_parent {
    my ($self) = @_;

    return unless $self->has_parent;
    my $parent = $self->get_parent;
    $parent->update_referential_file_attributes_to_child;
}

sub _update_referential_file_attributes {
    my ($self, $file, @attributes) = @_;

    my $refs = $self->get_referential_file_attributes;
    my $me   = $refs->current_source_alias;
    $refs->search_rs(undef, { order_by => "$me.id" });

    my %referential;
    while (my $attr = $refs->next) {
        my $id = $attr->get_column('bibliotheek_kenmerken_id');
        next if exists $referential{$id};
        $referential{$id} = $attr;
    }

    my @referential = keys %referential;
    my @allowed     = intersect(@referential, @attributes);

    my $case_documents = $self->file_case_documents->search_rs({ file_id => $file->id });

    my @current = $case_documents->get_column('bibliotheek_kenmerken_id')->all;

    my $diff  = _get_file_attribute_differences(\@attributes, \@current);
    my @stays = @{ $diff->{stays} };
    my @del   = @{ $diff->{delete} };
    my @add   = @{ $diff->{add} };

    if (@del) {
        $case_documents->search_rs(
            { bibliotheek_kenmerken_id => { -in => \@del } })->delete;
    }

    if (@add) {
        my $fcd = $self->result_source->schema->resultset('FileCaseDocument');

        foreach my $cid (@add) {
            my $attr = $referential{$cid};
            next unless $attr;
            my $library = $attr->bibliotheek_kenmerken_id;

            $fcd->create(
                {
                    case_id                  => $self->id,
                    file_id                  => $file->id,
                    magic_string             => $library->magic_string,
                    bibliotheek_kenmerken_id => $library->id,
                }
            );
        }
    }

    $self->set_case_documents_in_case_property(@add, @del);

    $self->update_referential_file_attributes_to_child($file, @stays, @add);
}

sub update_referential_file_attributes_to_child {
    my ($self, $file, @attribute_id) = @_;

    return unless $self->has_children;

    if (!$file) {
        my $documents = $self->file_case_documents;
        my %labels;
        my %file;
        while (my $file = $documents->next) {
            my $id      = $file->get_column('file_id');
            my $attr_id = $file->get_column('bibliotheek_kenmerken_id');

            push(@{$labels{$id}}, $attr_id);
            $file{$id} //= $file->file_id;
        }

        my $children = $self->get_children;
        while (my $child = $children->next) {
            foreach my $id (keys %labels) {
                $child->_update_referential_file_attributes($file{$id}, @{$labels{$id}});
            }
        }
        return;
    }

    my @attributes = ();
    @attribute_id = grep { defined $_ } @attribute_id;
    if (@attribute_id) {
        my $casetype_attributes = $self->zaaktype_node_id->zaaktype_kenmerken->search_rs(
            { 'id'    => { -in => \@attribute_id } },
            { columns => 'bibliotheek_kenmerken_id' }
        );

        @attributes = $casetype_attributes->get_column('bibliotheek_kenmerken_id')->all;
    }

    my $children = $self->get_children;
    while (my $child = $children->next) {
        $child->_update_referential_file_attributes($file, @attributes);
    }
}

sub _get_file_attribute_differences {
    my ($want, $current) = @_;

    my @stays   = intersect(@$want, @$current);
    my @del     = array_minus(@$current, @$want);
    my @add     = array_diff(@$want, @stays);

    return {
        stays  => \@stays,
        delete => \@del,
        add    => \@add,
    };

}

sub update_file_attribute {
    my ($self, $file, @attributes) = @_;

    my $schema = $self->result_source->schema;

    my $case_documents = $self->file_case_documents->search_rs(
        { file_id => $file->id });

    my @old_case_documents = map {
        $_->id
    } $self->zaaktype_node_id->zaaktype_kenmerken->search_rs(
        {
            'bibliotheek_kenmerken_id' => {
                -in => $case_documents->get_column('bibliotheek_kenmerken_id')->as_query
            }
        },
    )->all;

    my $diff = _get_file_attribute_differences(\@attributes, \@old_case_documents);
    my @del = @{$diff->{delete}};
    my @add = @{$diff->{add}};

    return unless (@del || @add);

    my %mapping;
    my $casetype_attributes = $self->zaaktype_node_id->zaaktype_kenmerken->search_rs(
        { 'me.id'   => { -in => [@del, @add] }, },
        { prefetch => 'bibliotheek_kenmerken_id' }
    );

    while (my $attribute = $casetype_attributes->next) {
        $mapping{ $attribute->id } = $attribute;
    }

    my @touched_library_attributes;
    if (@del) {
        my @del_bibliotheek_kenmerken = map {
            $mapping{$_}->get_column('bibliotheek_kenmerken_id')
        } @del;

        push @touched_library_attributes, @del_bibliotheek_kenmerken;

        $case_documents->search_rs({
            bibliotheek_kenmerken_id => {
                -in => \@del_bibliotheek_kenmerken
            }
        })->delete;

        for (@del) {
            $file->trigger_label_logging('remove', $mapping{$_});
        }
    }

    if (@add) {
        my $fcd = $schema->resultset('FileCaseDocument');

        my %seen;
        for my $cid (@add) {
            if (not exists $mapping{$cid}) {
                $self->log->trace("Could not find attribute '$cid' to add. Skipping.");
                next;
            }
            my $attr = $mapping{$cid}->bibliotheek_kenmerken_id;
            my $bibliotheek_kenmerken_id = $attr->get_column('id');

            # Only add labels once, even if multiple zaaktype_kenmerken are
            # sent for the same label/bibliotheek_kenmerken row.
            next if $seen { $bibliotheek_kenmerken_id };
            $seen{ $bibliotheek_kenmerken_id } = 1;

            push @touched_library_attributes, $bibliotheek_kenmerken_id;

            $file->trigger_label_logging('add', $mapping{$cid});

            $fcd->create(
                {
                    case_id                  => $self->id,
                    file_id                  => $file->id,
                    magic_string             => $attr->magic_string,
                    bibliotheek_kenmerken_id => $attr->get_column('id'),
                }
            );
        }
    }

    $self->set_case_documents_in_case_property(@touched_library_attributes);
    $self->update({last_modified => \'NOW()' });

    $self->update_referential_file_attributes_to_child($file, @attributes);
}


sig update_referential_attributes => 'HashRef';

sub update_referential_attributes {
    my ($self, $data) = @_;

    my @magic_strings = keys %$data;

    my $attrs = $self->get_casetype_attributes(1)->search_rs(
        {
            @magic_strings
            ? ('bibliotheek_kenmerken_id.magic_string' =>
                    { -in => \@magic_strings })
            : ()
        }
    );

    my %updates;
    while (my $attr = $attrs->next) {
        next unless exists $data->{$attr->magic_string};

        $updates{$attr->magic_string} = $data->{$attr->magic_string};
    }

    return 0 unless %updates;

    $self->ddd_update_attributes_without_event(\%updates);
    $self->clear_field_values();
    $self->update_referential_attributes_in_children(\%updates);
    $self->touch();
    return 1;
}

sub get_case_documents {
    my $self = shift;

    my $rs = $self->zaaktype_node_id->zaaktype_kenmerken->search_rs(
        {
            'bibliotheek_kenmerken_id.value_type' => 'file',
        },
        { prefetch => 'bibliotheek_kenmerken_id', },
    );

    my %attributes;

    while (my $attribute = $rs->next) {
        $attributes{$attribute->bibliotheek_kenmerken_id->magic_string} = $self->ddd_get_file_object_attribute($attribute);
    }
    return \%attributes;
}

sub set_case_documents_in_case_property {
    my ($self, @documents) = @_;

    my $rs = $self->zaaktype_node_id->zaaktype_kenmerken->search_rs(
        {
            @documents ? ('me.bibliotheek_kenmerken_id' => { -in => \@documents }) : (),
            'bibliotheek_kenmerken_id.value_type' => 'file',
        },
        {
            prefetch => 'bibliotheek_kenmerken_id',
            order_by => [
                { -asc => 'me.zaak_status_id' },
                { -asc => 'me.id' },
            ],
        },
    );

    my %seen_library_attribute_ids;
    my @attributes;

    # We could use a 'PARTITION BY' in the above statement.. if DBIx::Class has proper
    # support for those.
    while (my $attribute = $rs->next) {
        my $library_attribute_id = $attribute->get_column('bibliotheek_kenmerken_id');

        if ($seen_library_attribute_ids{$library_attribute_id}) {
            next;
        } else {
            $seen_library_attribute_ids{$library_attribute_id} = 1;
        }

        push(@attributes, $self->ddd_get_file_object_attribute($attribute));
    }

    $self->update_case_properties(@attributes);
    $self->ddd_update_system_attribute('case.case_documents');
}


1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 ZAAKSYSTEEM_CONSTANTS

TODO: Fix the POD

=cut

=head2 required_fields_missing

TODO: Fix the POD

=cut

=head2 unaccepted_pip_updates

TODO: Fix the POD

=cut

