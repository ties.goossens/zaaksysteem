$(function() {

    function split( val ) {
      return val.split( /,\s*/ );
    }
    function extractLast( term ) {
      return split( term ).pop();
    }

    var availableTags = [];
    $.each(availableModules, function(key, value) {
        availableTags.push(key);
    });

    function getmatch(term, response) {
        var searchterms   = term.split(/\s+/);
        var foundtags     = availableTags;

        $.each(searchterms, function(key, val) {
            foundtags     = $.grep(foundtags, function(a) {
                var re = new RegExp(val,"gi");

                if (a.match(re)) { return true; }
                return false;
            });
        });

        response(foundtags);
    }

    $( "#tags" )
      // don't navigate away from the field on tab when selecting an item
      .autocomplete({
        minLength: 0,
        source: function (request, response) {
          return getmatch(request.term, response);
        },
        focus: function() {
          // prevent value inserted on focus
          return false;
        },
        select: function( event, ui ) {
          availableModules[ui.item.value];

          window.location.href = '/man/' + ui.item.value;
        }
      });
  });
