// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import first from 'lodash/first';
import mapValues from 'lodash/mapValues';

export default (proposal) => {
  let flattened = proposal.merge(
    {
      instance: {
        attributes: mapValues(proposal.instance.attributes, (attrValue) => {
          return first(attrValue);
        }),
      },
    },
    { deep: true }
  );

  return flattened;
};
