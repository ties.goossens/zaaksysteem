package Zaaksysteem::Backend::Object::Data::ResultSet;

use Moose;

use BTTW::Tools;
use Zaaksysteem::DB::HStore;
use Zaaksysteem::Types qw(UUID);
use List::Util qw(any);

extends 'Zaaksysteem::Backend::ResultSet';

with qw/
    Zaaksysteem::Backend::Object::Roles::ObjectResultSet
    Zaaksysteem::Search::HStoreResultSet
    Zaaksysteem::Search::TSVectorResultSet
    Zaaksysteem::Search::Acl
/;

=head1 NAME

Zaaksysteem::Backend::Object::Data::ResultSet - Returns a ResultSet according to the object principals

=head1 SYNOPSIS

    ### ZQL: Retrieve all objects of type case
    my $resultset   = $object->from_zql('select case.id, casetype.title from case');


    ### List of attributes requested
    print join(' , ', @{ $resultset->object_requested_attributes });

    # prints:
    # case.id , casetype.title

    ### Loop over rows
    while (my $row = $resultset->next) {

        ## Prints uuid of object
        print $row->id

        ## prints JSON representation of row
        print Data::Dumper::Dumper( $row->TO_JSON )
    }

=head1 DESCRIPTION

ResultSet returned when called from L<Zaaksysteem::Object::Model>. Every row retrieved from
this resultset, will be blessed with L<Zaaksysteem::Backend::Object::Data::Component>.

=cut

=head1 METHODS

=head2 hstore_column

Implements interface required by the
L<Zaaksysteem::Search::HStoreResultSet> role.

Returns the string C<index_hstore>.

=cut

sub hstore_column { 'index_hstore' }

=head2 text_vector_column

Implements interface required by the the
L<Zaaksysteem::Search::TSVectorResultSet> role.

Returns the string C<text_vector>.

=cut

sub text_vector_column { 'text_vector' }

=head2 find_or_create_by_object_id

Find-or-create an L<Zaaksysteem::Backend::Object::Data::Component> row by it's
C<object_class> and C<object_id> index.

Optionally accepts an owner L<Zaaksysteem::Object::SecurityIdentity>, which
will receive C<read>, C<write>, and C<manage> permissions on the created
object.

    my $row = $schema->resultset('ObjectData')->find_or_create_by_object_id(
        'case',
        $self->id
    );

=cut

define_profile find_or_create_by_object_id => (
    required => {
        object_class => 'Str',
        object_id    => 'Int',
    },
    optional => {
        owner         => 'Zaaksysteem::Object::SecurityIdentity',
        class_uuid    => UUID,
        uuid          => UUID,
        instance_uuid => UUID, # stay backward compatible for now
    },
);

sig find_or_create_by_object_id => 'HashRef';

sub find_or_create_by_object_id {
    my $self = shift;
    my $opts = assert_profile(shift)->valid;

    $opts->{uuid} = delete $opts->{instance_uuid} if $opts->{instance_uuid};
    my $owner = delete $opts->{owner};

    my $row = $self->search_rs($opts)->first;
    return $row if $row;

    $row = $self->create( {
        object_class => $opts->{object_class},
        object_id    => $opts->{object_id},
        $opts->{uuid}       ? (uuid       => $opts->{uuid})       : (),
        $opts->{class_uuid} ? (class_uuid => $opts->{class_uuid}) : (),
    });

    if (defined $owner) {
        $self->result_source->schema->resultset('ObjectAclEntry')->create({
            object_uuid => $row->uuid,
            entity_type => $owner->entity_type,
            entity_id => $owner->entity_id,
            scope => 'instance',
            capability => $_
        }) for qw[read write manage];
    }

    return $row;
}

=head2 apply_query

Processes a L<Zaaksysteem::Object::Query> instance and returns a resultset
constrained under that query.

    # New resultset searching for cases
    my $rs = $rs->apply_query(qb('case'));

=cut

sig apply_query => 'Zaaksysteem::Object::Query';

sub apply_query {
    my ($self, $query) = @_;

    my $rs = $self->search({ object_class => $query->type });

    if ($query->has_cond) {
        $rs = $rs->search(\[ $self->parse_expr($query->cond) ]);
    }

    if ($query->has_sort) {
        my $direction = $query->sort->reverse ? '-desc' : '-asc';
        my $sort_expr = $query->sort->expression;

        unless ($sort_expr->isa('Zaaksysteem::Object::Query::Expression::Field')) {
            throw('object/query/sort_expression_not_supported', sprintf(
                'Only simple sorting is currently supported'
            ));
        }

        $rs = $rs->search(undef, {
            order_by => { $direction => $self->parse_expr($sort_expr) }
        });
    }

    return $rs;
}

=head2 parse_expr

Converts instances of L<Zaaksysteem::Object::Query::Expression> into bare SQL
with placeholders and their bind values.

    # ('my_field = ?', [ '' => 'abc' ])
    $rs->parse_expr(qb_eq('my_field', 'abc'))

    # ('other_field = ? AND status IN (?, ?)', [ '' => 'def' ], [ '' => 'new' ], [ '' => 'open' ])
    $rs->parse_expr(qb_and(qb_eq('other_field', 'def'), qb_in('status', [qw[new open])))

=cut

sub parse_expr {
    my $self = shift;
    my $expr = shift;

    if ($expr->isa('Zaaksysteem::Object::Query::Expression::Conjunction')) {
        my (@sql, @args);

        for my $subexpr ($expr->all_expressions) {
            my ($subsql, @subargs) = $self->parse_expr($subexpr);

            push @sql, $subsql;
            push @args, @subargs;
        }

        return join(' AND ', @sql), @args;
    }

    if ($expr->isa('Zaaksysteem::Object::Query::Expression::Disjunction')) {
        my (@sql, @args);

        for my $subexpr ($expr->all_expressions) {
            my ($subsql, @subargs) = $self->parse_expr($subexpr);

            push @sql, $subsql;
            push @args, @subargs;
        }

        return join(' OR ', @sql), @args;
    }

    if ($expr->isa('Zaaksysteem::Object::Query::Expression::Comparison')) {
        my ($left, $right) = $expr->all_expressions;

        my %mode_map = (
            equal                 => '=',
            not_equal             => '!=',
            less_than             => '<',
            greater_than          => '>',
            equal_or_less_than    => '>=',
            equal_or_greater_than => '<='
        );

        my ($left_sql, @left_args) = $self->parse_expr($left);
        my ($right_sql, @right_args) = $self->parse_expr($right);

        my $stmt = sprintf(
            '(%s %s %s)',
            $left_sql,
            $mode_map{ $expr->mode },
            $right_sql
        );

        return $stmt, @left_args, @right_args;
    }

    if ($expr->isa('Zaaksysteem::Object::Query::Expression::Inversion')) {
        my ($subsql, @subargs) = $self->parse_expr($expr->expression);

        return sprintf('NOT (%s)', $subsql), @subargs;
    }

    if ($expr->isa('Zaaksysteem::Object::Query::Expression::ContainsString')) {
        my ($strsql, @strargs) = $self->parse_expr($expr->string);
        my ($matchsql, @matchargs) = $self->parse_expr($expr->match);

        my $pattern = sprintf('(%s)::text', $matchsql);

        if ($expr->has_mode) {
            my $mode = $expr->mode;

            if ($mode eq 'prefix') {
                $pattern = sprintf("%s || '%%'", $pattern);
            } elsif ($mode eq 'infix') {
                $pattern = sprintf("'%%' || %s || '%%'", $pattern);
            } elsif ($mode eq 'postfix') {
                $pattern = sprintf("'%%' || %s", $pattern);
            } else {
                throw('object/query/parse_like/mode_unsupported', sprintf(
                    'Mode "%s" is not supported for string pattern matching query ("%s")',
                    $mode,
                    $expr->stringify
                ));
            }
        }

        return sprintf('(%s ILIKE (%s))', $strsql, $pattern), @strargs, @matchargs;
    }

    if ($expr->isa('Zaaksysteem::Object::Query::Expression::Regex')) {
        my ($strsql, @strargs) = $self->parse_expr($expr->string);

        return sprintf("(%s ~ '%s')", $strsql, $expr->pattern), @strargs;
    }

    if ($expr->isa('Zaaksysteem::Object::Query::Expression::MemberRelation')) {
        my ($left_sql, @left_args) = $self->parse_expr($expr->expression);
        my ($right_sql, @right_args) = $self->parse_expr($expr->set);

        return sprintf('(%s IN %s)', $left_sql, $right_sql), @left_args, @right_args;
    }

    if ($expr->isa('Zaaksysteem::Object::Query::Expression::Set')) {
        my (@sql, @args);

        for my $subexpr ($expr->all_expressions) {
            my ($subsql, @subargs) = $self->parse_expr($subexpr);

            push @sql, $subsql;
            push @args, @subargs;
        }

        return sprintf('(%s)', join(', ', @sql)), @args;
    }

    if ($expr->isa('Zaaksysteem::Object::Query::Expression::Literal')) {
        my $value = $expr->value;

        if ($expr->type eq 'object') {
            $value = $value->id;
        }

        return '?', [ '', $value ];
    }

    if ($expr->isa('Zaaksysteem::Object::Query::Expression::Field')) {
        return sprintf("%s->'%s'", $self->hstore_column, $expr->name);
    }

    throw('object/query/dbix_class/unhandled_expression', sprintf(
        'Unable to parse/process expression: "%s"',
        $expr->stringify
    ));
}

=head2 _construct_object

Overrides L<DBIx::Class::ResultSet/_construct_object> method to automatically
trigger L<Zaaksysteem::Search::Acl/_load_acl_capabilities_on_row> for each
freshly inflated result object.

Previously, this behavior was implemented using the, now deprecated,
L<DBIx::Class::ResultSet/_construct_object> method, which was removed in
version C<0.082820> of L<DBIx::Class>.

=cut

# Taking bets for deprecation of this private method, closest date wins
### Rudolf - 2019-10-10

sub _construct_results {
    my $self = shift;

    my $rows = $self->next::method(@_);

    if ($self->{ attrs }{ zaaksysteem }{ user }) {
        $self->_load_acl_capabilities_on_row($_) for @{ $rows };
    }

    return $rows;
}

sub get_stable_sort_key {
    my $self = shift;
    return  { -asc => [ map { "me.$_" } $self->result_source->primary_columns ] };
}

sub map_native_column {
    my ($self, $k) = shift;
    if (any { $k eq $_ } qw(date_modified date_created)) {
        return "me.$k";
    }
    return;
}

1;

__END__

=head1 SEE ALSO

L<Zaaksysteem::Manual> L<Zaaksysteem::Manual::Template>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2014-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
