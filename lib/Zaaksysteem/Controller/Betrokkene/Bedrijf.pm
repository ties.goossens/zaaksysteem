package Zaaksysteem::Controller::Betrokkene::Bedrijf;

use Moose;

BEGIN { extends 'Zaaksysteem::Controller' }

sub bedrijven_profile : Private {
    my ($self, $c) = @_;

    ### Get profile from Model
    my $profile = $c->get_profile(
        'method'=> 'create',
        'caller' => 'Zaaksysteem::Betrokkene::Object::Bedrijf'
    ) or die('Could not find profile');

    my @required_fields = grep {
        $_ ne 'vestiging_postcodewoonplaats' ||
        $_ ne 'vestiging_adres'
    } @{ $profile->{required} };

    push(@required_fields, 'rechtsvorm');

    $profile->{required} = \@required_fields;
    $profile->{optional} = [
        @{ $profile->{optional} },
        qw(
            npc-telefoonnummer
            npc-email
            npc-mobiel
        ),
    ];
    $profile->{constraint_methods}{'npc-email'} = qr/^.+?\@.+\.[a-z0-9]{2,}$/;
    $profile->{constraint_methods}{'npc-telefoonnummer'} = qr/^[\d\+]{6,15}$/;
    $profile->{constraint_methods}{'npc-mobiel'}         = qr/^[\d\+]{6,15}$/;

    return $profile;
}

sub create : Local {
    my ($self, $c) = @_;

    $c->register_profile(
        method => 'create',
        profile => $c->forward('bedrijven_profile'),
    );

    if ($c->req->is_xhr) {
        $c->zvalidate;
        $c->detach;
    }

    ### Default: view
    $c->stash->{template}   = 'betrokkene/create.tt';

    if ($c->req->method eq 'POST') {
        # Validate information
        return unless $c->zvalidate;

        ### Create person
        my $id = $c->model('Betrokkene')->create('bedrijf', $c->req->params);

        return unless $id;

        $c->push_flash_message(sprintf('Organisatie aangemaakt: %d', $id));

        $c->res->redirect($c->uri_for(
            sprintf('/betrokkene/%d', $id),
            { gm => 1, type => 'bedrijf' }
        ));
    }
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 create

TODO: Fix the POD

=head2 bedrijven_profile

TODO: Fix the POD

=cut

