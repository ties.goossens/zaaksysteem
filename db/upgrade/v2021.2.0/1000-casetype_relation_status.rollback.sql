BEGIN;

  ALTER TABLE zaaktype_relatie ADD COLUMN status_new integer;
  UPDATE zaaktype_relatie set status_new = 1 where status = true;
  ALTER TABLE zaaktype_relatie DROP COLUMN status;
  ALTER TABLE zaaktype_relatie RENAME COLUMN status_new TO status;

COMMIT;
