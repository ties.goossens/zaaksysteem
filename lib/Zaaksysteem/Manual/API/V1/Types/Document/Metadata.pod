=head1 NAME

Zaaksysteem::Manual::API::V1::Types::Document::Metadata - Type definition for C<document metadata> objects

=head1 DESCRIPTION

This page documents the serialization of
L<C<document metadata>|Zaaksysteem::Object::Types::Document::Metadata> object instances.

Document metadata is an object that represents additional information about a document neccesary for
associated L<storage|Zaaksysteem::Object::Types::File> and
L<metadata|Zaaksysteem::Object::Types::Document::Metadata>.

The various attributes reference the specific
L<TMLO|https://archief2020.nl/downloads/toepassingsprofiel-metadatering-lokale-overheden-pdf>
requirements. Please refer to your TMLO document on how to interpret the data.

=head1 JSON

=begin javascript

{
    "instance" : {
        "appearance" : "Verschijningsvorm",
        "category" : "Notitie",
        "date_created" : "2018-01-30T14:14:41Z",
        "date_modified" : "2018-01-30T14:14:41Z",
        "description" : "This is the description of a file",
        "origin" : "Intern",
        "origin_date" : "2018-01-10T00:00:00Z",
        "pronom_format" : "x-fmt/111",
        "structure" : "Structuur",
        "trust_level" : "Openbaar"
    },
    "reference" : null,
    "type" : "document/metadata"
}

=end javascript

=head1 INSTANCE ATTRIBUTES

=head2 description E<raquo> L<C<text>|Zaaksysteem::Manual::API::V1::ValueTypes/text>

TMLO 5.2

=head2 trust_level E<raquo> L<C<File>|Zaaksysteem::Manual::API::V1::Types::File>

TMLO 17.1

=head2 category E<raquo> L<C<string>|Zaaksysteem::Manual::API::V1::ValueTypes/string>

TMLO 19.1

=head2 appearance E<raquo> L<C<text>|Zaaksysteem::Manual::API::V1::ValueTypes/text>

TMLO 19.2

=head2 structure E<raquo> L<C<text>|Zaaksysteem::Manual::API::V1::ValueTypes/text>

TMLO 19.3

=head2 pronom_format E<raquo> L<C<string>|Zaaksysteem::Manual::API::V1::ValueTypes/string>

TMLO 21.6.1

More about the PRONOM format can be found at L<http://www.nationalarchives.gov.uk/PRONOM/Default.aspx>

=head2 origin E<raquo> L<C<string>|Zaaksysteem::Manual::API::V1::ValueTypes/string>

The origin of the document, allowed values: C<Intern>, C<Incoming> or C<Outgoing>.

=head2 origin_date E<raquo> L<C<date>|Zaaksysteem::Manual::API::V1::ValueTypes/date>

If the origin is C<Incoming> this is the date when the document was received.
If the origin is C<Outgoing> this is the date when the document was send.
If the origin is C<Intern> this field will be unset.

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
