// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import zsCaseStatusIconModule from '../../../case/zsCaseStatusIcon';
import zsProgressBarModule from '../../zsProgressBar';
import zsTruncateHtmlModule from '../../zsTruncate/zsTruncateHtml';
import composedReducerModule from '../../../api/resource/composedReducer';
import template from './template.html';
import './styles.scss';

export default angular
  .module('zsCaseWidgetResultCompact', [
    zsCaseStatusIconModule,
    zsProgressBarModule,
    zsTruncateHtmlModule,
    composedReducerModule,
  ])
  .component('zsCaseWidgetResultCompact', {
    bindings: {
      item: '&',
    },
    controller: [
      '$scope',
      'composedReducer',
      function (scope, composedReducer) {
        let ctrl = this,
          itemReducer;

        itemReducer = composedReducer({ scope }, ctrl.item).reduce(
          (item) => item
        );

        ctrl.getItem = itemReducer.data;
      },
    ],
    template,
  }).name;
