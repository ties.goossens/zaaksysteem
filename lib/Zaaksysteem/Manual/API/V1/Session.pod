=head1 NAME

Zaaksysteem::Manual::API::V1::Session - Information about current zaaksysteem session

=head1 Description

This API-document describes the usage of our JSON Session API. These calls return information about
the current session. It contains information about the current company you are visiting, information
about the logged in user, and other information.

=head2 API

This document is based on the V1 API of Zaaksysteem, more information about the default format
of this API can be found in L<Zaaksysteem::Manual::API::V1>. Please make sure you read this
document before continuing.

=head2 URL

The base URL for this API is:

    /api/v1/session

Make sure you use the HTTP Method C<GET> for retrieving, and C<POST> for change requests.

=head1 Retrieve data

=head2 get

   /api/v1/session/current

Returns information about the current session

B<Example call>

  https://localhost/api/v1/session/current

B<Response JSON JSON>

=begin javascript

{
   "api_version" : 1,
   "request_id" : "mintlab-b0596f-c9a081",
   "development" : false,
   "result" : {
      "instance" : {
         "account" : {
            "instance" : {
               "address" : "Donker Curtiusstraat 7 - 521",
               "company" : "Mintlab",
               "email" : "servicedesk@mintlab.nl",
               "homepage" : "http://www.mintlab.nl/",
               "phonenumber" : "020 - 737 000 5",
               "place" : "Amsterdam",
               "zipcode" : "1051 JL"
            },
            "reference" : null,
            "type" : "session_account"
         },
         "design_template" : "mintlab",
         "hostname" : "testsuite",
         "logged_in_user" : {
            "id" : 1,
            "display_name" : "A. User",
            "email" : "info@mintlab.nl",
            "given_name" : "Admin",
            "initials" : "A.",
            "organizational_unit" : "Backoffice",
            "surname" : "User",

            "legacy" : {
                "positions" : [
                    {
                        type: "position",
                        reference: null,
                        instance: {
                            role: {
                            instance: {
                                    role_id: 30079,
                                    description: "Systeemgroep: Documentintaker",
                                    date_modified: "2018-02-09T14:19:26Z",
                                    name: "Documentintaker",
                                    date_created: "2018-02-09T14:19:26Z",
                                    system_role: true
                                },
                                type: "role",
                                reference: null
                            },
                            group: {
                                type: "group",
                                reference: null,
                                instance: {
                                    date_created: "2018-02-09T14:19:26Z",
                                    description: "Backoffice",
                                    group_id: 10011,
                                    date_modified: "2018-02-09T14:19:26Z",
                                    name: "Backoffice"
                                }
                        },
                        date_modified: "2018-02-09T14:19:26Z",
                        date_created: "2018-02-09T14:19:26Z"
                        }
                    },
                    // Potentially more here
                ]
            }

            // This listing declares the capabilities the user has within
            // the current session.
            "capabilities" : [
               "admin",
               "search",
               "vernietigingslijst",
               "zaak_afdeling",
               "plugin_mgmt",
               "zaak_eigen",
               "beheer_plugin_admin",
               "documenten_intake_subject",
               "dashboard",
               "beheer",
               "owner_signatures",
               "contact_search",
               "gebruiker",
               "beheer_gegevens_admin",
               "case_registration_allow_partial"
            ],

            "system_roles" : [
               "Administrator",
               "Behandelaar"
            ],

            "telephonenumber" : "0207370005"
         }
      },
      "reference" : null,
      "type" : "session"
   },
   "status_code" : 200
}

=end javascript

=head1 Objects

=head2 session

Most of the calls in this document return an instance of type C<session>. An object containing information
about the current session, like the logged in user, hostname of the instance and company info.

B<Properties>

=over 4

=item account [required]

Type: L<Zaaksysteem::Manual::API::V1::Session/session_account>

The accountinformation of the company running this zaaksysteem

=item hostname

Type: String

The hostname of this instance

=item logged_in_user

Type: Object

The currently logged in user

=item design_template

Type: String

The template used for the current layout

=back

=head2 session_account

Information over the account running this zaaksysteem

B<Properties>

=over 4

=item address

Type: Str

The address of the company

=item company

Type: Str

The name of the company

=item company_extended

Type: Str

The long name of the company

=item company_simple

Type: Str

The simple (short) name of the company

=item email

Type: Str

The main emailaddress of the company

=item homepage

Type: Str

The homepage of the company

=item phonenumber

Type: Str

The phone number of the company

=item place

Type: Str

The place (residence) of the company, e.g. "Amsterdam"

=item zipcode

Type: Str

The zipcode of the company

=back

=head1 Support

The data in this document is supported by the following test. Please make sure you use the API as described
in this test. Any use of this API outside the scope of this test is B<unsupported>

L<TestFor::Catalyst::API::V1::Subject>

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015-2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
