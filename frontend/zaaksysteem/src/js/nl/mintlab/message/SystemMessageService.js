// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

(function () {
  angular.module('Zaaksysteem.message').factory('systemMessageService', [
    '$rootScope',
    'translationService',
    function ($rootScope, translationService) {
      var systemMessageService = {},
        ready,
        queue = [];

      function send(msgObj) {
        if (!ready) {
          queue.push(msgObj);
        } else {
          $rootScope.$emit('systemMessage', msgObj);
        }
      }

      function emit(type, msg, context) {
        var msgObj = {
          type: type,
          content: translationService.get(msg),
        };

        msgObj = _.merge(msgObj, context);

        send(msgObj);
        $rootScope.$emit('systemMessage', msgObj);

        return msgObj;
      }

      function emitErr(type, obj) {
        var msg = translationService.get(
            'Er ging iets fout bij het %s van %s. Probeer het later opnieuw',
            type,
            obj || 'de data'
          ),
          msgObj = {
            type: 'error',
            content: msg,
          };

        send(msgObj);

        return msgObj;
      }

      function emitQueue() {
        while (queue.length) {
          $rootScope.$emit('systemMessage', queue.shift());
        }
      }

      systemMessageService.emitLoadError = function (obj) {
        return emitErr('laden', obj);
      };

      systemMessageService.emitSaveError = function (obj) {
        return emitErr('opslaan', obj);
      };

      systemMessageService.emitValidationError = function () {
        return emit(
          'error',
          'Enkele velden ontbreken of zijn niet juist ingevuld. Corrigeer deze velden en probeer het opnieuw.'
        );
      };

      systemMessageService.emitSave = function () {
        $rootScope.$emit('systemMessage', {
          type: 'info',
          content: translationService.get(
            'Uw wijzigingen zijn succesvol opgeslagen'
          ),
        });
      };

      systemMessageService.emit = function (type, msg, context) {
        return emit(type, msg, context);
      };

      systemMessageService.emitError = function (msg) {
        return emit('error', msg);
      };

      systemMessageService.setReady = function () {
        if (!ready) {
          ready = true;
          emitQueue();
        }
      };

      return systemMessageService;
    },
  ]);
})();
