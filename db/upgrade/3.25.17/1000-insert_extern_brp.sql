BEGIN;
    INSERT INTO roles (parent_group_id, name, description, system_role, date_created, date_modified)
    SELECT id, 'BRP externe bevrager', 'Systeemgroep: BRP externe bevrager', true, now(), now() from groups where path = array[groups.id];

COMMIT;
