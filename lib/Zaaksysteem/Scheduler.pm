package Zaaksysteem::Scheduler;
use Moose;
use namespace::autoclean;

with 'MooseX::Log::Log4perl';

use DateTime;
use Moose::Util qw/ensure_all_roles/;
use BTTW::Tools;


=head1 NAME

Zaaksysteem::Scheduler - Zaaksysteem job scheduler

=head1 SYNOPSIS

    # From a controller
    $c->model('Scheduler')->run_pending();

=head1 ATTRIBUTES

=head2 c

Catalyst context for the request.

=cut

has c => (
    is       => 'ro',
    required => 1,
);

=head1 METHODS

=head2 run_pending

Run all pending jobs (those with a "next run" date in the past).

Jobs are retrieved from the database.

=cut

sub run_pending {
    my $self = shift;

    my $schema = $self->c->model('DB')->schema;

    my $object_model = $self->c->model('Object', DisableACL => 1);

    my $jobs_done = $schema->txn_do(sub {
        my @jobs = $self->_get_pending_jobs();

        my $jobs_run = 0;
        for my $job (@jobs) {
            try {
                ensure_all_roles($job, sprintf('Zaaksysteem::Scheduler::Job::%s', $job->job));

                $schema->txn_do(sub {
                    $job->run($self->c);
                });

                if ($job->setup_next_run()) {
                    # Another run is required.
                    $object_model->save(object => $job);
                }
                else {
                    # No more runs.
                    $object_model->delete(object => $job);
                }

                $self->log->debug(sprintf("Done running job %s", $job->id));

                $jobs_run++;

            }
            catch {
                $self->log->error(sprintf("Could not run job %s: %s", $job->id, $_));
            };
        }

        return $jobs_run;
    });

    return $jobs_done;
}

=head2 install_default_jobs

Installs default jobs (fixtures of some sort) that should always be present
in the system.

=cut

sub install_default_jobs {
    my $self = shift;

    my $model = $self->c->model('Object', DisableACL => 1);

    my @default_jobs = $self->c->collect_script_retvals(qw[share fixtures scheduled_jobs]);

    for my $job (@default_jobs) {
        unless (eval { $job->isa('Zaaksysteem::Object::Types::ScheduledJob') }) {
            throw('scheduled/install_default_jobs/job_not_valid', sprintf(
                'Script retval "%s" is not a valid job object',
                "$job"
            ));
        }

        next if $model->count('scheduled_job', { job => $job->job });

        $self->log->info(sprintf('Installing scheduled job "%s"', $job));

        $model->save_object(object => $job);
    }

    return;
}

sub _get_pending_jobs {
    my $self = shift;
    my $now = DateTime->now();

    return $self->c->model('Object')->inflate_from_rs(
        $self->c->model('Object')->new_resultset->search_rs(
            {
                'me.object_class' => 'scheduled_job',
                "me.index_hstore->'next_run'" => { '<=' => $now->iso8601 },
            },
            {
                # Lock the rows, to prevent some race conditions
                for => 'update',
            },
        )
    );
}

__PACKAGE__->meta->make_immutable();

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
