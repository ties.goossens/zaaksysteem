BEGIN;
  TRUNCATE TABLE queue_no_partition;
  ALTER TABLE queue RENAME TO queue_partitioned;
  ALTER TABLE queue_no_partition RENAME TO queue;
  ALTER TABLE queue ADD CHECK (status IN ('pending', 'running', 'finished', 'failed', 'waiting', 'cancelled', 'postponed'));
COMMIT;
