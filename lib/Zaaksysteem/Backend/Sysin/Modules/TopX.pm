package Zaaksysteem::Backend::Sysin::Modules::TopX;
use Zaaksysteem::Moose;

use feature 'state';

extends 'Zaaksysteem::Backend::Sysin::Modules';
with qw(
    Zaaksysteem::Backend::Sysin::Modules::Roles::ProcessorParams
);


=head1 NAME

Zaaksysteem::Backend::Sysin::Modules::TopX - TopX integration module

=head1 DESCRIPTION

This module deals with TopX attribute mappings

=cut

use Zaaksysteem::ZAPI::Error;
use Zaaksysteem::ZAPI::Form::Field;
require Zaaksysteem::Export::TopX::Base;

=head1 CONSTANTS

=head2 INTERFACE_CONFIG_FIELDS

Collection of L<Zaaksysteem::ZAPI::Form> objects used in building the UI for
this module.

=cut

use constant INTERFACE_CONFIG_FIELDS => [
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_export_type',
        type        => 'select',
        label       => 'Exportformaat',
        required    => 1,
        description => 'Selecteer het gewenste export-formaat',
        data        => {
            options => [
                { value => 'zaaksysteem', label => 'Zaaksysteem' },
                { value => 'rip',         label => 'RIP v0.3' }
            ]
        }
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_export_id',
        type        => 'text',
        label       => 'Exportformaat',
        required    => 1,
        description => 'Identificatie van alle archiefstukken',
        data        => {
            placeholder => 'NL-9999-0000'
        },
        when  => "interface_export_type === 'rip'",
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_export_target',
        type        => 'text',
        label       => 'Exportdoel',
        required    => 1,
        description => 'Doelinstantie van de export',
        data        => {
            placeholder => 'Regionaal Archief Aruba'
        },
        when  => "interface_export_type === 'rip'",
    ),
];

=head2 INTERFACE_DESCRIPTION

Module description / pointers for the UI.

=cut

use constant INTERFACE_DESCRIPTION => <<'EOD';
<p>
Met dit koppelprofiel kan de TopX export geconfigureerd worden.
</p>
EOD

=head2 MODULE_SETTINGS

Sysin module parameters, used during instantiation to configure the module.

=cut

state @list;

foreach (Zaaksysteem::Export::TopX::Base->get_ztc_optional()) {
    push(
        @list,
        {
            external_name  => $_,
            optional       => 1,
            attribute_type => 'magic_string',
            all_casetypes  => 1,
        }
    );
}

use constant MODULE_SETTINGS => {
    name                          => 'export_mapping_topx',
    label                         => 'TopX kenmerkmapping',
    description                   => INTERFACE_DESCRIPTION,
    interface_config              => INTERFACE_CONFIG_FIELDS,
    is_multiple                   => 0,
    is_manual                     => 0,
    allow_multiple_configurations => 0,
    module_type                   => ['apiv1', 'api'],


    has_attributes => 1,
    attribute_list => \@list,
};

=head1 METHODS

=cut

around BUILDARGS => sub {
    my ($orig, $class) = @_;

    return $class->$orig(%{ MODULE_SETTINGS() });
};

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2021, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
