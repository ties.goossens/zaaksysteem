// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import vormTemplateServiceModule from './../../vormTemplateService';
import zsMapModule from './../../../ui/zsMap';
import zsMapSearchModule from './../../../ui/zsMap/zsMapSearch';
import snackbarServiceModule from './../../../ui/zsSnackbar/snackbarService';
import resourceModule from './../../../api/resource';
import composedReducerModule from './../../../api/resource/composedReducer';
import wickedGoodXpath from 'wicked-good-xpath';
import first from 'lodash/head';
import get from 'lodash/get';
import isArray from 'lodash/isArray';
import identity from 'lodash/identity';
import './styles.scss';

export default angular
  .module('vorm.types.map', [
    vormTemplateServiceModule,
    zsMapModule,
    zsMapSearchModule,
    resourceModule,
    composedReducerModule,
    snackbarServiceModule,
  ])
  .directive('vormMap', [
    () => {
      return {
        restrict: 'E',
        scope: {
          addressType: '&',
          featureLayers: '&',
        },
        bindToController: true,
        template: `<zs-map-search
						on-change="vm.handleSearchChange($location, $lat, $lng)"
						data-location="vm.getLocation()"
						address-type="vm.addressType()"
					></zs-map-search>`,
        require: ['vormMap', 'ngModel'],
        controller: [
          '$scope',
          function () {
            let ctrl = this,
              ngModel;

            ctrl.link = (...rest) => {
              [ngModel] = rest;
            };

            ctrl.handleSearchChange = (location) => {
              ngModel.$setViewValue(location, 'click');
            };

            ctrl.getLocation = () => {
              return ngModel ? ngModel.$modelValue : null;
            };
          },
        ],
        controllerAs: 'vm',
        link: (scope, element, attrs, controllers) => {
          controllers.shift().link(...controllers);
        },
      };
    },
  ])
  .directive('vormMapDisplay', [
    'composedReducer',
    'vormInvoke',
    (composedReducer, vormInvoke) => {
      return {
        restrict: 'E',
        scope: {
          delegateValue: '&',
          templateData: '&',
        },
        bindToController: true,
        template:
          '<div tabindex="0" ng-if="vm.isVisible()" class="object-list-item">{{vm.getValue()}}</div>',
        require: ['vormMapDisplay'],
        controller: [
          '$scope',
          function (scope) {
            let ctrl = this,
              valueReducer;

            valueReducer = composedReducer(
              { scope },
              ctrl.delegateValue,
              ctrl.templateData
            ).reduce((value, template) => {
              if (get(template, 'addressType') === 'coordinate') {
                return 'Locatie geselecteerd';
              }
              return value;
            });

            ctrl.getValue = valueReducer.data;

            ctrl.isVisible = () =>
              !vormInvoke(get(ctrl.templateData(), 'hideCoords'));
          },
        ],
        controllerAs: 'vm',
      };
    },
  ])
  .directive('vormMapComponent', [
    '$http',
    '$q',
    '$window',
    '$document',
    'resource',
    'snackbarService',
    'composedReducer',
    (
      $http,
      $q,
      $window,
      $document,
      resource,
      snackbarService,
      composedReducer
    ) => {
      return {
        restrict: 'E',
        scope: {
          values: '&',
          onChange: '&',
          disabled: '&',
          addressType: '&',
          featureLayers: '&',
          onFeaturesSelect: '&',
        },
        template: `<zs-map
						markers="vm.getMarkers()"
						on-map-click="vm.handleMapClick($lat, $lng, $features, $namespaces, $preventUpdate)"
						feature-layers="vm.featureLayers()"
					>
					</zs-map>`,
        bindToController: true,
        controller: [
          '$scope',
          function (scope) {
            function isValueParseable(val) {
              if (
                val.match(
                  /[0-9]+[a-zA-Z]?(?:-([^,]+))?, [0-9]{4}\ ?[A-Z]{2}/
                ) ||
                val.match(/[0-9]+[^,]*, [0-9]{4}\ ?[A-Z]{2}/)
              ) {
                return true;
              }
              return false;
            }

            let ctrl = this,
              locationResource = resource(
                () => {
                  let value = isArray(ctrl.values())
                    ? first(ctrl.values())
                    : ctrl.values();
                  let opts =
                    value &&
                    isValueParseable(value) &&
                    ctrl.addressType() !== 'coordinate'
                      ? {
                          url:
                            'https://geodata.nationaalgeoregister.nl/locatieserver/v3/free',
                          params: {
                            fq: 'type:adres',
                            q: value,
                          },
                        }
                      : null;

                  return opts;
                },
                { scope, cache: { disabled: true } }
              ).reduce((requestOptions, data) => {
                let doc = first(get(data, 'response.docs'));

                if (!doc) {
                  return null;
                }

                const [, lng, lat] = /POINT\((.+)\s(.+)\)/.exec(
                  doc.centroide_ll
                ) || [null, '', ''];
                const coordinates = { lng, lat };

                const markers = [
                  {
                    coordinates,
                    title: doc.weergavenaam,
                    description:
                      `<b>Stad</b>: ${doc.woonplaatsnaam}<br/>` +
                      `<b>Postcode</b>: ${doc.postcode || '-'}<br/>` +
                      `<b>Provincie</b>: ${doc.provincienaam}<br/>`,
                  },
                ];

                return markers;
              }),
              markerReducer = composedReducer(
                { scope },
                locationResource,
                ctrl.addressType,
                ctrl.values
              ).reduce((locations, addressType, values) => {
                let markers = [],
                  vals = isArray(values) ? values : [values];

                if (addressType === 'coordinate') {
                  markers = (vals || []).filter(identity).map((value) => {
                    let coordinate = value.split(',');

                    return {
                      coordinates: {
                        lat: coordinate[0],
                        lng: coordinate[1],
                      },
                      title: coordinate.join(', '),
                    };
                  });
                } else {
                  markers = locations;
                }

                return markers;
              });

            ctrl.getMarkers = markerReducer.data;

            ctrl.handleMapClick = (
              lat,
              lng,
              features,
              namespaces,
              preventUpdate
            ) => {
              preventUpdate = preventUpdate || false;
              const val = this.values();

              let promises = [];
              let pm;

              if (ctrl.disabled()) {
                return;
              } else if (ctrl.addressType() === 'coordinate') {
                pm = $q.resolve([lat, lng].join(','));
              } else if (lat || val) {
                const [l, n] = [
                  Math.max(Number(lat), Number(lng)),
                  Math.min(Number(lat), Number(lng)),
                ];
                const url = lat
                  ? `https://geodata.nationaalgeoregister.nl/locatieserver/v3/suggest?lat=${l}&lon=${n}&fq=type:adres`
                  : `https://geodata.nationaalgeoregister.nl/locatieserver/v3/suggest?q=${val}&fq=type:adres`;
                pm = fetch(url)
                  .then((r) => r.json())
                  .then((response) => {
                    let data = response.response.docs,
                      address =
                        data.find((doc) => doc.weergavenaam === val) ||
                        first(data);

                    if (!address) {
                      return $q.reject(response);
                    } else {
                      return $q.resolve(address.weergavenaam);
                    }
                  });
              }

              pm && (promises = promises.concat($q.resolve(pm)));

              // get names of clicked features
              if (features && features.length) {
                promises = promises.concat(
                  features.map((feature) =>
                    $http({
                      url: feature.url,
                      method: 'GET',
                      withCredentials: false,
                      headers: { 'X-Client-Type': undefined },
                    }).then((data) => {
                      let parser = new DOMParser(),
                        xml = parser.parseFromString(data.data, 'text/xml'),
                        path = feature.layer.instance.feature_info_xpath,
                        featureName,
                        featureResult;

                      // IE11 doesn't support XPath query, so we use a helper library
                      if (!document.evaluate) {
                        wickedGoodXpath.install();
                      }

                      try {
                        featureResult = xml.evaluate(
                          path,
                          xml,
                          (ns) => namespaces[ns],
                          XPathResult.ANY_TYPE,
                          null
                        );

                        if (featureResult) {
                          switch (featureResult.resultType) {
                            case XPathResult.STRING_TYPE:
                              featureName = featureResult.stringValue;
                              break;

                            case XPathResult.NUMBER_TYPE:
                              featureName = featureResult.numberValue;
                              break;

                            default:
                              featureName = featureResult.iterateNext()
                                .textContent;
                              break;
                          }
                        }
                      } catch (error) {
                        console.warn(
                          `xpath "${path}" did not return a value, resolving value as empty`
                        );
                        return $q.resolve(null);
                      }

                      return $q.resolve(featureName);
                    })
                  )
                );
              }

              snackbarService
                .wait('Bezig met ophalen van locatie', {
                  promise: promises,
                  catch: (err) => {
                    console.error('Cant get location info', err);

                    return 'Er kon geen locatie gevonden worden voor deze coordinaat';
                  },
                })
                .then((values) => {
                  let [value, ...ftrs] = values;

                  if (!preventUpdate) {
                    ctrl.onChange({
                      $value: value,
                    });
                  }

                  if (features && features.length) {
                    ctrl.onFeaturesSelect({
                      $features: ftrs.filter(identity),
                    });
                  }
                });
            };
          },
        ],
        controllerAs: 'vm',
      };
    },
  ])
  .run([
    'vormTemplateService',
    (vormTemplateService) => {
      vormTemplateService.registerType('map', {
        control: angular.element(
          '<vorm-map address-type="vm.invokeData(\'addressType\')" feature-layers="vm.invokeData(\'featureLayers\')" ng-model></vorm-map>'
        ),
        display: angular.element(
          '<vorm-map-display delegate-value="delegate.value"></vorm-map-display>'
        ),

        wrapper: (el) => {
          angular.element(el[1]).append(
            `<vorm-map-component
								data-disabled="vm.disabled()"
								data-values="vm.value()"
								address-type="vm.invokeData('addressType')"
								feature-layers="vm.invokeData('featureLayers')"
								on-change="vm.onChange({ $value: $value })"
								ng-if="!vm.disabled()||!vm.$empty"
								on-features-select="vm.templateData().onFeaturesSelect($features)"
							>
							</vorm-map-component>`
          );

          return el;
        },
        defaults: {
          editMode: 'empty',
        },
      });
    },
  ]).name;
