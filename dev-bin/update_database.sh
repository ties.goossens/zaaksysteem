#! /bin/bash
set -e

up_the_docker() {
    local container=$1
    docker-compose ps $container | grep -q 'Up'
    [ $? -eq 0 ] && return 0;
    docker-compose up -d --no-deps $container && sleep 7 && return 0
    return 1
}

if [ $# -lt "1" ]; then
    echo "USAGE: $0 [LIST OF SQL-UPDATE FILES]"
    echo
    echo "Loads update scripts on test template, and dumps is back"
    exit
fi

BASE_DIR="."
GIT_ROOT=$(git rev-parse --show-toplevel)
perlbackend=backend

if [ -z "$GIT_ROOT" ]; then
    echo "Please run this script from a Zaaksysteem or Start git repository"
    exit 1
elif [ -d "zsnl-perl-api" ]; then
    GIT_ROOT="$GIT_ROOT/zsnl-perl-api"
    BASE_DIR="zsnl-perl-api"
    perlbackend=perl-api
fi

set +e
up_the_docker database
set -e

PROCID=$$
DATABASE="zs_test_${USER}_${PROCID}"

dbexists=$(docker-compose exec database psql -U zaaksysteem -t -c "SELECT 1 FROM pg_database WHERE datname = '$DATABASE'")
dbexists=$(echo "$dbexists" | tr -d '[:space:]')
if [ "$dbexists" != "" ]; then
    echo "Temporary database $DATABASE already (still?) exists. Please try again."
    echo "$dbexists" | od -x
    exit
fi

UPGRADE_FILE=$(mktemp $BASE_DIR/db/db_upgrade.XXXXXXXX)
TEMPLATE="$BASE_DIR/db/test-template.sql"
EMPTYTEMPLATE="$BASE_DIR/db/template.sql"

(
    cd "$GIT_ROOT"

    read -r -a files <<< "$@"
    old_sql=$(printf -- '%s\n' "${files[@]}" | grep -Ev '/(pre|rel|post)-' | sort)
    pre_sql=$(printf -- '%s\n' "${files[@]}" | grep '/pre-' | sort)
    rel_sql=$(printf -- '%s\n' "${files[@]}" | grep '/rel-' | sort)
    post_sql=$(printf -- '%s\n' "${files[@]}" | grep '/post-'| sort)

    for args in $old_sql $pre_sql $rel_sql $post_sql
    do
        file="${args#"$BASE_DIR/"}"
        cat "$file" >> "${UPGRADE_FILE#"$BASE_DIR/"}";
        set +e
        git check-ignore "$file"
        [ $? -eq 1 ] && git add "$file"
        set -e
    done
)

echo "Creating test database: $DATABASE"
docker-compose exec database psql -U zaaksysteem -c "CREATE DATABASE $DATABASE ENCODING 'UTF-8';"

echo "Loading '${TEMPLATE#$BASE_DIR/}' into '$DATABASE'"
docker-compose exec database psql -U zaaksysteem -f "/opt/zaaksysteem/${TEMPLATE#$BASE_DIR/}" "$DATABASE"

echo "Executing upgrades to $DATABASE"
docker-compose exec database psql -U zaaksysteem -f "/opt/zaaksysteem/${UPGRADE_FILE#$BASE_DIR/}" "$DATABASE"

echo "Recreating DBIx::Class schema"
docker-compose run --no-deps --rm $perlbackend /opt/zaaksysteem/dev-bin/db_redeploy.sh "dbi:Pg:dbname=${DATABASE};host=database" zaaksysteem zaaksysteem123

echo "Dumping database '$DATABASE' to '${TEMPLATE#$BASE_DIR/}'"
docker-compose exec -T database pg_dump -U zaaksysteem --no-owner "$DATABASE" > "$TEMPLATE"

echo "Dumping database '$DATABASE' schema to '${EMPTYTEMPLATE#$BASE_DIR/}'"
docker-compose exec -T database pg_dump -U zaaksysteem --no-owner --schema-only "$DATABASE" > "$EMPTYTEMPLATE"

echo "Cleaning up database: '$DATABASE'"
docker-compose exec database psql -U zaaksysteem -c "DROP DATABASE $DATABASE"

rm "${UPGRADE_FILE}"

if [ $BASE_DIR != '.' ]
then
    cd $BASE_DIR
fi

find . -user 0 -exec sudo chown $USER:$USER {} \;

for i in $(git status --porcelain | egrep '^\?\? lib/Zaaksysteem/Schema/' | awk '{print $NF}')
do

    cat <<OEF >> $i

__END__

$(cat drafts/license_block.pod)
OEF

    git add $i
done

for i in $(git status --porcelain | grep -E '^(M| )M lib/Zaaksysteem/Schema/' | awk '{print $NF}')
do
    git add $i
done

git add db/template.sql db/test-template.sql

echo "Added your files to git - you can commit now if you want"
