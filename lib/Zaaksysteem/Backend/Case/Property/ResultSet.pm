package Zaaksysteem::Backend::Case::Property::ResultSet;

use Moose;

extends 'DBIx::Class::ResultSet';

=head1 NAME

Zaaksysteem::Backend::Case::Property::ResultSet

=head1 DESCRIPTION

=cut

use BTTW::Tools;

=head1 METHODS

=head2 values_v0

Returns a hashref containing the attribute->value pairs as used in the v0 API
infrastructure.

B<Remember> to call this method only on a constrained resultset of the table,
otherwise a hashref of B<all> properties in the table will be constructed.

=cut

sub values_v0 {
    my $self = shift;

    my %acc;

    while (my $property = $self->next) {
        for my $value (@{ $property->value_v0 }) {
            $acc{ $value->{ name } } = $value->{ value };
        }
    }

    return \%acc;
}

=head2 pending_changes_v0

Returns a hashref containing the bibliotheek_kenmerken_id->pending_change
pairs as used in the v0 API infrastructure.

B<Remember> to call this method only on a constrained resultset of the table,
otherwise a hashref of B<all> properties in the table will be constructed.

=cut

sub pending_changes_v0 {
    my $self = shift;

    my @pending_changes;

    while (my $property = $self->next) {
        for my $value (@{ $property->value_v0 }) {
            next unless defined $value->{ pending_change };

            push @pending_changes, $value->{ pending_change };
        }
    }

    return {
        map { $_->{ bibliotheek_kenmerken_id } => $_ } @pending_changes
    };
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
