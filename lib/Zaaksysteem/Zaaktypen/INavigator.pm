package Zaaksysteem::Zaaktypen::INavigator;

use Moose::Role;

use Clone qw(clone);
use List::Util qw/max/;
use List::MoreUtils qw/all/;
use JSON;

use Zaaksysteem::Constants;
use BTTW::Tools;
use Zaaksysteem::Backend::Tools::DottedPath qw/get_by_dotted_path set_by_dotted_path/;


use Exporter 'import';
our @EXPORT_OK = qw/GENERAL_FIELDS DOCUMENT_FIELDS RESULT_FIELDS CHECKLIST_FIELDS/;

# we could use Backend::Term for this job
use constant TERM_LOOKUP => {
    Werkdag => 1,
    Dag     => 1,
    Week    => 7,
    Maand   => 31,
    Jaar    => 365,
};


use constant GENERAL_FIELDS => (
    {
        field => 'node.titel',
        xpath => 'velden/kernomschrijving',
        label => 'Naam zaaktype',
        type  => 'title'
    },
    {
        field => 'node.code',
        xpath => '@id',
        label => 'Identificatie',
    },
    {
        field => 'definitie.procesbeschrijving',
        xpath => 'velden/schemalocatie',
        label => 'Procesbeschrijving',
    },
    {
        field => 'definitie.handelingsinitiator',
        xpath => 'velden/zaaktype-naam/structuur/handeling-initiator',
        label => 'Handeling initiator',
        type  => 'handelingsinitiator'
    },
    {
        field => 'node.properties.doel',
        xpath => 'velden/naam',
        label => 'Doel',
    },
    {
        field => 'node.properties.archiefclassicatiecode',
        xpath => 'velden/basisarchiefcode',
        label => 'Archiefclassificatiecode',
    },
    {
        field => 'node.properties.verantwoordelijke',
        xpath => 'velden/proceseigenaar',
        label => 'Verantwoordelijke',
    },
    {
        field => 'node.properties.beroep_mogelijk',
        xpath => 'velden/beroep-mogelijk',
        label => 'Bezwaar en beroep mogelijk',
        type  => 'checkbox',
    },
    {
        field => 'node.properties.bag',
        xpath => 'velden/bag',
        label => 'BAG',
        type  => 'checkbox',
    },
    {
        field => 'node.properties.lex_silencio_positivo',
        xpath => 'velden/lex-silencio-positivo',
        label => 'Lex silencio positivo',
        type  => 'checkbox',
    },
    {
        field => 'node.properties.wet_dwangsom',
        xpath => 'velden/wet-dwangsom',
        label => 'Wet dwangsom',
        type  => 'checkbox',
    },
    {
        field => 'node.properties.wkpb',
        xpath => 'velden/wkpb',
        label => 'WKPB',
        type  => 'checkbox',
    },
    {
        field => 'node.properties.publicatie',
        xpath => 'velden/publicatie-indicatie',
        label => 'Publicatie',
        type  => 'checkbox'
    },
    {
        field => 'node.properties.verdagingstermijn',
        xpath => 'velden/verdagingstermijn',
        label => 'Verdagingstermijn'
    },
    {
        field => 'node.properties.verantwoordingsrelatie',
        xpath => 'velden/zaaktype-iv3-categorie',
        label => 'Verantwoordingsrelatie',
    },
    {
        field => 'node.zaaktype_trefwoorden',
        xpath => 'velden/lokale-trefwoorden',
        label => 'Trefwoorden',
    },
    {
        field => 'node.zaaktype_omschrijving',
        xpath => 'velden/toelichting-proces',
        label => 'Toelichting of omschrijving',
    },
    {
        field => 'definitie.grondslag',
        xpath => 'velden/grondslag/list/fields',
        label => 'Wettelijke grondslag',
        required => 1,
        type  => 'grondslag'
    },
    {
        field => 'node.properties.lokale_grondslag',
        xpath => 'velden/lokale-regelgeving/list/fields',
        label => 'Lokale grondslag',
        type  => 'grondslag'
    },
    {
        field => 'definitie.afhandeltermijn',
        xpath => 'velden/wettelijke-afdoeningstermijn',
        label => 'Doorlooptijd wettelijk (kalenderdagen)',
    },
);



use constant RESULT_FIELDS => (
    {
        field => 'id',
        xpath => '@id',
        label => 'id',
        type  => 'hidden'
    },
    {
        field => 'resultaat',
        xpath => 'velden/naam-model',
        label => 'Resultaattype-generiek',
        type  => 'resultaattype'
    },
    {
        field => 'label',
        xpath => 'velden/naam',
        label => 'Naam (omschrijving)',
    },
    {
        field => 'selectielijst',
        xpath => 'velden/vernietigingsgrondslag',
        label => 'Selectielijst',
    },
    {
        field => 'archiefnominatie',
        xpath => 'velden/waardering',
        label => 'Archiefnominatie',
        type  => 'archiefnominatie',
    },
    {
        field => 'bewaartermijn',
        xpath => 'velden/bewaartermijn',
        label => 'Bewaartermijn',
        type  => 'bewaartermijn'
    },
    {
        field => 'comments',
        xpath => 'velden/toelichting-bewaartermijn',
        label => 'Toelichting',
    },
);


use constant DOCUMENT_FIELDS => (
    {
        field => 'label',
        xpath => 'velden/naam',
        label => 'Titel (in zaaktype)',
    },
    {
        field => 'naam',
        xpath => 'velden/naam-model',
        label => 'Documentnaam',
        type  => 'documentnaam'
    },
    {
        field => 'value_mandatory',
        xpath => 'velden/indossier',
        label => 'Verplicht',
        type  => 'checkbox'
    },
);


use constant CHECKLIST_FIELDS => (
    {
        field => 'label',
        xpath => 'velden/vraag',
        label => 'Checklistitem',
    },
    {
        field => 'id',
        xpath => '@id',
        label => 'id',
        type  => 'hidden'
    },
);


=head2 inavigator_xml_to_json

Parses the supplied I-Navigator file into a JSON savvy structure. This structure will
be edited by the GUI and later on consumed by the logic that applies the changes.

=cut

define_profile inavigator_xml_to_json => (
    required => [qw/filename/]
);
sub inavigator_xml_to_json {
    my $self = shift;
    my $arguments = assert_profile(shift)->valid;

    my $xpath = XML::XPath->new(filename => $arguments->{filename});

    my $nodeset = $xpath->find('/dsp/processen/proces');

    my @casetypes = map {{
        general        => $self->parse_general($_),
        checklistitems => $self->parse_checklistitems($_),
        documents      => $self->parse_documents($_),
        results        => $self->parse_results($_, $xpath),
    }} $nodeset->get_nodelist;

    my @spliced = splice @casetypes, 0, 100;

    return {map { $_->{general}->{'node.code'} => $_ } @spliced};
}


# Parsing API

sub parse_segment {
    my ($self, $xpath, $node, $fields, $callback) = @_;

    # find the list of requested nodes
    my $nodes = $node->find($xpath);

    # foreach node, execute the reaping function
    return [ map { $self->parse_item($_, $fields, $callback) } $nodes->get_nodelist ];

}


sub parse_item {
    my ($self, $node, $fields, $callback) = @_;

    my $item = { map { $_->{field} => $self->parse_field($node, $_) } @$fields };

    $callback->($node, $item) if $callback;

    return $item;
}


sub parse_field {
    my ($self, $node, $field) = @_;

    my $type = $field->{type} || '';

    return $self->parse_grondslag($node, $field) if $type eq 'grondslag';

    my $string_value = $node->find($field->{xpath})->string_value;

    # I-Navigator uses 'Ja' for a true value, anything else for false.
    my $boolean = sub { shift eq 'Ja' ? JSON::true : JSON::false };

    # if we supply the frontend with actual JSON booleans the
    # checkboxes will natively understand
    return $type eq 'checkbox' ? $boolean->($string_value) : $string_value;
}

sub trim {
    my ($self, $string) = @_;
    $string =~ s/^\s+|\s+$//g;
    return $string;
}

sub parse_grondslag {
    my ($self, $node, $field) = @_;

    my $nodes = $node->find($field->{xpath});

    return join "\n", map {
        '<a target="_blank" href="'.
        $self->trim($_->find('field[@naam="URL"]')->string_value) .
        '">' .
        $self->trim($_->find('field[@naam="NAAM"]')->string_value) .
        '</a>'
    } $nodes->get_nodelist;
}

# Parsing functions for individual parts

sub parse_general {
    my ($self, $node) = @_;

    return $self->parse_item($node, [GENERAL_FIELDS], sub {
        my ($node, $casetype) = @_;

        $casetype->{'definitie.handelingsinitiator'} = lc($casetype->{'definitie.handelingsinitiator'} || '');

        # override combined terms
        if (my $eenheid = $node->find('velden/verdagingstermijn-eenheid')->string_value) {
            $casetype->{'node.properties.verdagingstermijn'} *= TERM_LOOKUP->{$eenheid};
        }

        if (my $eenheid = $node->find('velden/wettelijke-afdoeningstermijn-eenheid')->string_value) {
            $casetype->{'definitie.afhandeltermijn'} *= TERM_LOOKUP->{$eenheid};
        }
    });
}


sub parse_results {
    my ($self, $node, $xpath) = @_;

    return $self->parse_segment('resultaattypen/resultaattype', $node, [RESULT_FIELDS], sub {

        my ($node, $result) = @_;

        # Get the official name of a result
        my $result_id = $node->getAttribute('soort-id');
        my $xpath_path = sprintf('/dsp/resultaatsoorten/resultaatsoort[@id="%s"]', $result_id);
        my $nodelist = $xpath->find($xpath_path);
        if ($nodelist->size) {
            my $resultaat = $nodelist->shift;
            $result->{resultaat} = $resultaat->find('velden/naam')->string_value;
        }

        my $actief = $node->find('velden/actief')->string_value // '';
        $result->{enabled} = $actief eq 'Ja' ? 1 : 0;

        if (my $eenheid = $node->find('velden/bewaartermijn-eenheid')->string_value) {
            $result->{bewaartermijn} *= TERM_LOOKUP->{$eenheid};
        }
    });
}


sub parse_documents {
    my ($self, $node) = @_;

    my $rs = $self->dbic->resultset('BibliotheekKenmerken');

    return $self->parse_segment('documenttypen/documenttype', $node, [DOCUMENT_FIELDS], sub {
        my ($node, $document) = @_;

        # do we have a file attribute with this name already? notate it.
        my $row = $rs->search({ naam => $document->{label} })->first;
        if ($row) {
            $document->{bibliotheek_kenmerken_id} = $row->id;
        }
    });
}


sub parse_checklistitems {
    my ($self, $node) = @_;

    return $self->parse_segment('statustypen/statustype/checks/check', $node, [CHECKLIST_FIELDS]);
}


=head2 apply_inavigator_settings

Given a casetype in session form - a hashref structure -
apply a number of given changes to it. These settings
originate from an I-Navigator XML file

=cut

define_profile apply_inavigator_settings => (
    required => {
        casetype => 'HashRef',
        settings => 'HashRef'
    }
);
sub apply_inavigator_settings {
    my $self = shift;
    my $arguments = assert_profile(shift)->valid;

    my $settings = assert_profile(
        $arguments->{settings},
        profile => {
            required => [qw(general results)],
            optional => [qw(documents checklistitems)],
        }
    )->valid;

    my $casetype = $arguments->{casetype};

    $self->apply_general_settings($casetype, $settings->{general});

    $self->apply_documents_settings($casetype, $settings->{documents})
        if $settings->{documents};
    $self->apply_checklist_settings($casetype, $settings->{checklistitems})
        if $settings->{checklistitems};

    $self->apply_result_settings($casetype, $settings->{results});

    return $casetype;
}


=head2 apply_general_settings

The settings from the I-Navigator XML are given to the user for review,
after this they are passed down for processing. Here for every relevant
field the value is copied to the target casetype.

=cut

sub apply_general_settings {
    my ($self, $casetype, $general_settings) = @_;

    foreach my $field (GENERAL_FIELDS) {
        my $path = $field->{field};

        # special case, don't update title unless specifically requested
        next if $path eq 'node.titel' && !$general_settings->{modify_title};

        throw('import/inavigator/value_missing', 'Input value expected for ' . $path)
            unless exists $general_settings->{$path};

        my $raw = $general_settings->{$path};

        # for checkboxes, convert into zaaksysteem boolean
        my $value = ($field->{type}||'') eq 'checkbox' ? ($raw ? 'Ja' : 'Nee') : $raw;

        # boter.kaas.eieren will translate into ->{boter}->{kaas}->{eieren}
        set_by_dotted_path({
            target => $casetype,
            path   => $path,
            value  => $value
        });
    }
    # Because we only use the wettelijke kalenderdagen in the import:
    # Doorlooptijd wettelijk (kalenderdagen)
    $casetype->{definitie}{afhandeltermijn_type} = 'kalenderdagen';
    return;
}


=head2 apply_document_settings

See if the target casetype has the document in the given phase.
If it does, all we want to do is update the settings.

If it doesn't, add it. If a bibliotheek_kenmerken_id is given, use that it in
the new zaaktype_kenmerk. If not, create a new bibliotheek_kenmerken in the given category.

=cut

sub apply_documents_settings {
    my ($self, $casetype, $document_settings) = @_;

    foreach my $document (grep { $_->{enabled} || $_->{existingKenmerk}} @$document_settings) {
        $self->apply_document($casetype, $document);
    }
}


sub apply_document {
    my ($self, $casetype, $document) = @_;

    my $phase = $document->{phase}{value} or throw('inavigator/import', 'Fase is niet ingesteld voor document');

    my $kenmerken = $casetype->{statussen}{$phase}{elementen}{kenmerken};

    my ($existing) = grep {
        $_->{naam} &&
        $_->{naam} eq $document->{label} &&
        $_->{type} eq 'file'
    } values %$kenmerken;

    if ($existing) {
        $existing->{value_mandatory} = $document->{value_mandatory};
        $existing->{pip} = $document->{publish_pip};
        $existing->{publish_public} = $document->{publish_website};
    } else {
        $document->{bibliotheek_kenmerken_id} ||= $self->create_file_attribute($document);

        # TODO necessary/useful?
        $document->{naam} = $document->{label};
        $document->{pip} = $document->{publish_pip};
        $document->{publish_public} = $document->{publish_website};

        append_to_indexed_hash($kenmerken, $document);
    }
}




=head2 create_file_attribute

If INavigator settings dictate addition of a new attribute we may
need to create it in the catalogue

=cut

sub create_file_attribute {
    my ($self, $document) = @_;

    throw('import/inavigator', 'Categorie voor document ' . $document->{naam} . ' mist.')
        unless $document->{bibliotheek_categorie_id}->{id};

    my $rs = $self->dbic->resultset('BibliotheekKenmerken');
    my $label = $document->{label};

    return $rs->create({
        naam                     => $label,
        value_type               => 'file',
        label                    => $label,
        magic_string             => $rs->generate_magic_string($label),
        bibliotheek_categorie_id => $document->{bibliotheek_categorie_id}->{id},
    })->id;
}



sub apply_result_settings {
    my ($self, $casetype, $result_settings) = @_;

    my %results = ();
    my $count = 1;
    foreach my $result (@$result_settings) {
        next unless $result->{enabled};

        my $archive_nomination = ZAAKSYSTEEM_OPTIONS->{ARCHIEFNOMINATIE_OPTIONS}
            { lc($result->{archiefnominatie}) };

        $results{$count} = {
            label         => $result->{label},
            comments      => $result->{comments},
            bewaartermijn => $result->{bewaartermijn},
            selectielijst => $result->{selectielijst},

            resultaat => lc($result->{resultaat}),

            $archive_nomination
                ? (archiefnominatie => $archive_nomination)
                : (),

            external_reference => $result->{id},
        };
        $count++;
    }

    my $statussen = $casetype->{statussen};
    my $last_phase = max (keys %$statussen, 0);
    $statussen->{$last_phase}{elementen}{resultaten} = \%results;
}


sub apply_checklist_settings {
    my ($self, $casetype, $checklist_settings) = @_;

    foreach my $item (grep { $_->{enabled} } @$checklist_settings) {
        my $phase = $item->{phase}{value};
        $item->{external_reference} = $item->{id};
        my $checklists = $casetype->{statussen}{$phase}{elementen}{checklists};
        append_to_indexed_hash($checklists, $item);
    }
}


=head2 append_to_indexed_hash

given a structure like

{
    1 => { a => 2},
    2 => { b => 3}
}

add a new entry 3 => {c => 4}}

=cut

sub append_to_indexed_hash {
    my ($target, $item) = @_;

    my $new_id = 1 + max (keys %$target, 0);
    $target->{$new_id} = $item;
}


1;


__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 TERM_LOOKUP

TODO: Fix the POD

=cut

=head2 ZAAKSYSTEEM_OPTIONS

TODO: Fix the POD

=cut

=head2 apply_checklist_settings

TODO: Fix the POD

=cut

=head2 apply_document

TODO: Fix the POD

=cut

=head2 apply_documents_settings

TODO: Fix the POD

=cut

=head2 apply_result_settings

TODO: Fix the POD

=cut

=head2 parse_checklistitems

TODO: Fix the POD

=cut

=head2 parse_documents

TODO: Fix the POD

=cut

=head2 parse_field

TODO: Fix the POD

=cut

=head2 parse_general

TODO: Fix the POD

=cut

=head2 parse_grondslag

TODO: Fix the POD

=cut

=head2 parse_item

TODO: Fix the POD

=cut

=head2 parse_results

TODO: Fix the POD

=cut

=head2 parse_segment

TODO: Fix the POD

=cut

=head2 trim

TODO: Fix the POD

=cut

