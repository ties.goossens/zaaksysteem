
BEGIN;


    ALTER TABLE bag_cache DROP CONSTRAINT bag_cache_bag_type_check;
    ALTER TABLE bag_cache ADD CONSTRAINT bag_cache_bag_type_check CHECK (bag_type IN
      (
        'ligplaats',
        'nummeraanduiding',
        'openbareruimte',
        'pand',
        'standplaats',
        'verblijfsobject',
        'woonplaats'
      ));

COMMIT;
