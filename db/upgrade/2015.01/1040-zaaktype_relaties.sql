BEGIN;

ALTER TABLE zaaktype_relatie ADD COLUMN betrokkene_authorized BOOLEAN;
ALTER TABLE zaaktype_relatie ADD COLUMN betrokkene_notify BOOLEAN;
ALTER TABLE zaaktype_relatie ADD COLUMN betrokkene_id TEXT;
ALTER TABLE zaaktype_relatie ADD COLUMN betrokkene_role TEXT;
ALTER TABLE zaaktype_relatie ADD COLUMN betrokkene_role_set TEXT;
ALTER TABLE zaaktype_relatie ADD COLUMN betrokkene_prefix TEXT;
ALTER TABLE zaaktype_relatie ADD COLUMN eigenaar_id TEXT;

COMMIT;
