package TestFor::General::Attributes;

# ./zs_prove -v t/lib/TestFor/General/Attributes.pm
use base qw(ZSTest);

use TestSetup;
use Zaaksysteem::Constants qw/CASE_PAYMENT_STATUS_PENDING/;
use Zaaksysteem::Attributes;
use Zaaksysteem::Zaken::DelayedTouch;

=pod

TEST_METHOD=attributes_natuurlijk_persoon inotifyrun.sh ./zs_prove -v t/100-test_class.t

# OR, quicker
ZS_DISABLE_STUF_PRELOAD=1 ./zs_prove -v t/lib/TestFor/General/Attributes.pm

The test approach for the attributes is:
- prove that all predefined attributes do the same as the old code.
  After the old code has been removed this will become obsolete
- prove that all predefined attributes yield an actual value
- specifically test all new values.
- TODO: test all legacy values for edge cases. Right now, only test is that
  it didn't change.
- Bedrijf-related and LDAP related attrs have not been tested and poop out
  todo-warnings.

=cut

my $USER_ATTRIBUTES = [
  'aanleiding',
  'aanvrager_aanhef',
  'aanvrager_aanhef1',
  'aanvrager_aanhef2',
  'aanvrager_achternaam',
  'aanvrager_afdeling',
  'aanvrager_anummer',
  'aanvrager_burgerservicenummer',
  'aanvrager_correspondentie_huisnummer',
  'aanvrager_correspondentie_nummeraanduiding',
  'aanvrager_correspondentie_postcode',
  'aanvrager_correspondentie_straat',
  'aanvrager_correspondentie_woonplaats',
  'aanvrager_email',
  'aanvrager_geboortedatum',
  'aanvrager_geboorteplaats',
  'aanvrager_geslacht',
  'aanvrager_geslachtsnaam',
  'aanvrager_handelsnaam',
  'aanvrager_has_briefadres',
  'aanvrager_huisnummer',
  'aanvrager_huwelijksdatum',
  'aanvrager_indicatie_geheim',
  'aanvrager_kvknummer',
  'aanvrager_login',
  'aanvrager_mobiel',
  'aanvrager_naam',
  'aanvrager_overlijdensdatum',
  'aanvrager_password',
  'aanvrager_postcode',
  'aanvrager_straat',
  'aanvrager_tel',
  'aanvrager_type',
  'aanvrager_verblijf_huisnummer',
  'aanvrager_verblijf_nummeraanduiding',
  'aanvrager_verblijf_postcode',
  'aanvrager_verblijf_straat',
  'aanvrager_verblijf_woonplaats',
  'aanvrager_volledigenaam',
  'aanvrager_voornamen',
  'aanvrager_voorvoegsel',
  'aanvrager_woonplaats',
  'actieve_selectielijst',
  'afdeling',
  'afhandeldatum',
  'afhandeldatum_volledig',
  'aggregatieniveau',
  'alle_relaties',
  'archiefclassicatiecode',
  'archiefnominatie',
  'archiefstatus',
  'bag',
  'bedrag_web',
  'bedrijven_id',
  'behandelaar',
  'behandelaar_afdeling',
  'behandelaar_email',
  'behandelaar_handtekening',
  'behandelaar_tel',
  'betaalstatus',
  'bewaartermijn',
  'bezwaar_en_beroep_mogelijk',
  'contactkanaal',
  'coordinator',
  'coordinator_email',
  'coordinator_tel',
  'deelzaken_afgehandeld',
  'doel',
  'doorlooptijd_service',
  'doorlooptijd_wettelijk',
  'e_formulier',
  'gebruiker_naam',
  'generieke_categorie',
  'handelingsinitiator',
  'identificatie',
  'lex_silencio_positivo',
  'lokale_grondslag',
  'omschrijving_of_toelichting',
  'ontvanger',
  'ontvanger_aanhef',
  'ontvanger_aanhef1',
  'ontvanger_aanhef2',
  'ontvanger_achternaam',
  'ontvanger_afdeling',
  'ontvanger_burgerservicenummer',
  'ontvanger_email',
  'ontvanger_geboortedatum',
  'ontvanger_geboorteplaats',
  'ontvanger_geslacht',
  'ontvanger_geslachtsnaam',
  'ontvanger_handelsnaam',
  'ontvanger_huisnummer',
  'ontvanger_huwelijksdatum',
  'ontvanger_kvknummer',
  'ontvanger_mobiel',
  'ontvanger_naam',
  'ontvanger_overlijdensdatum',
  'ontvanger_postcode',
  'ontvanger_straat',
  'ontvanger_tel',
  'ontvanger_type',
  'ontvanger_volledigenaam',
  'ontvanger_voornamen',
  'ontvanger_voorvoegsel',
  'ontvanger_woonplaats',
  'openbaarheid',
  'opgeschort_tot',
  'opschorten_mogelijk',
  'pdc_tarief',
  'pid',
  'procesbeschrijving',
  'publicatie',
  'publicatietekst',
  'published',
  'reden_opschorting',
  'reden_vroegtijdig_afhandelen',
  'registratiedatum',
  'registratiedatum_volledig',
  'relates_to',
  'resultaat',
  'resultaat_omschrijving',
  'resultaat_toelichting',
  'selectielijst',
  'sjabloon_aanmaakdatum',
  'startdatum',
  'statusnummer',
  'streefafhandeldatum',
  'trefwoorden',
  'trigger',
  'uiterste_vernietigingsdatum',
  'uname',
  'verantwoordelijke',
  'verantwoordingsrelatie',
  'verdagingstermijn',
  'verlenging_mogelijk',
  'verlengingstermijn',
  'vertrouwelijkheid',
  'vertrouwelijkheidsaanduiding',
  'vervolg_van',
  'wet_dwangsom',
  'wettelijke_grondslag',
  'wkpb',
  'zaak_bedrag',
  'zaak_fase',
  'zaak_mijlpaal',
  'zaaknummer',
  'zaaknummer_hoofdzaak',
  'zaaktype',
  'zaaktype_versie',
  'zaaktype_versie_begindatum',
  'zaaktype_versie_einddatum'
];

my @BEDRIJF_ATTRIBUTES = qw/aanvrager_login ontvanger_kvknummer aanvrager_kvknummer bedrijven_id aanvrager_password ontvanger_handelsnaam aanvrager_handelsnaam aanvrager_afdeling ontvanger_burgerservicenummer aanvrager_anummer/;
my @MEDEWERKER_ATTRIBUTES = qw/ontvanger_afdeling afdeling behandelaar_afdeling/;

use constant GEBOORTELAND => 'Tatooine';
use constant YESTERDAY => DateTime->now->add(days => -1); # all my troubles seemed so far away

# bypass moose caching by creating a copy
sub update_in_clone {
    my ($case_id, $callback) = @_;

    my $clone_case = $schema->resultset('Zaak')->find($case_id);

    $callback->($clone_case);
    $clone_case->update;
}


sub prove_attribute {
    my ($case, $attribute_name) = @_;

    my $magic_strings = $case->magic_strings;

    ok exists $magic_strings->{$attribute_name}, "new result value $attribute_name exists";

    return $magic_strings->{$attribute_name};
}


sub format_payment_amount {
    my $amount = shift;
    $amount =~ s/\./,/;
    return $amount;
}


sub attributes_natuurlijk_persoon : Tests {
    $zs->zs_transaction_ok(sub {
        $zs->schema->default_resultset_attributes->{delayed_touch} = Zaaksysteem::Zaken::DelayedTouch->new;

        my $handtekening = $zs->create_filestore_ok(
            original_name => 'KEKEKEKE.docx',
            file_path     => $zs->config->{filestore_test_file_path},
        );

        my $current_user = $zs->set_current_user;
        my $settings = $current_user->settings;
        $settings->{signature_filestore_id} = $handtekening->id;
        $current_user->settings($settings);
        $current_user->update;

        my $ontvanger = $zs->create_natuurlijk_persoon_ok;
        my $voorgaande_zaak = $zs->create_case_ok;
        my $parent_case = $zs->create_case_ok;

        my $case = $zs->create_case_ok(
            ontvanger => 'betrokkene-natuurlijk_persoon-' . $ontvanger->id
        );
        $case->pid($parent_case->id);
        $case->open_zaak;
        $case->vernietigingsdatum(DateTime->now->add(years => 1));
        $case->vervolg_van($voorgaande_zaak->id);
        $case->relates_to($voorgaande_zaak->id);
        $case->stalled_until(DateTime->now->add(days => 3));
        $case->payment_status(CASE_PAYMENT_STATUS_PENDING);

        my $node = $case->zaaktype_node_id;
        $node->deleted(DateTime->now);
        $node->properties({map { $_ => $_ . ' inhoud' } qw/
            aanleiding
            archiefclassicatiecode
            archiefstatus
            bag
            beroep_mogelijk
            doel
            e_formulier
            lex_silencio_positivo
            lokale_grondslag
            opschorten_mogelijk
            publicatie
            publicatietekst
            verantwoordelijke
            verantwoordingsrelatie
            verdagingstermijn
            verlenging_mogelijk
            verlengingstermijn
            vertrouwelijkheidsaanduiding
            wet_dwangsom
            wkpb
            /
        });
        $node->update;

        my $resultaat = $case->zaaktype_node_id->zaaktype_resultaten->first->resultaat;
        $case->resultaat($resultaat);

        # simulate a married requestor who passed yesterday
        my $aanvrager_record = $case->aanvrager_object->gm_extern_np;
        $aanvrager_record->datum_overlijden(YESTERDAY);
        $aanvrager_record->onderzoek_persoon(1);
        $aanvrager_record->update;


        $case->afhandeldatum(DateTime->now);
        $case->update;

        # needed in combinatino with delayedtouch. better way?
        $case->_touch;

        $case->add_log_entry({
            unpublish => 'dit is nep unpublish xml',
            publish => 'dit is nep publish xml'
        });

        update_in_clone($case->id, sub {
            my $case = shift;

            # ontvanger only becomes accessible after the case has reincarnated
            my $ontvanger = $case->ontvanger_object;
            $ontvanger->datum_huwelijk(DateTime->now->add(years => -60));
            $ontvanger->telefoonnummer('0612345678');
            $ontvanger->geboortedatum(DateTime->now->add(years => 60));
            $ontvanger->mobiel('0612345678');

            my $ontvanger_record = $ontvanger->gm_extern_np;
            $ontvanger_record->datum_overlijden(DateTime->now->add(days => -2));
            $ontvanger_record->update;

            my $aanvrager = $case->aanvrager_object;
            $aanvrager->geboortedatum(DateTime->now->add(years => 60));
            $aanvrager->burgerservicenummer('12345678');
            $aanvrager->telefoonnummer('0612345678');
            $aanvrager->datum_huwelijk(DateTime->now->add(years => -60));
            $aanvrager->mobiel('0612345678');
            $aanvrager->geboorteland(GEBOORTELAND);

            $aanvrager->datum_huwelijk_ontbinding(YESTERDAY);
        });

        # a lot of the magic strings info are cached at various places. to bypass this,
        # create a new object using the same id.
        my $clone_case = $schema->resultset('Zaak')->find($case->id);

        # prove that for every systeemkenmerk the same value is returned in the
        # renewed state as in the old state, and that isn't empty as to exclude
        # false positives
        for my $attribute_name (@$USER_ATTRIBUTES) {

            next if grep { $attribute_name eq $_ }
                (@BEDRIJF_ATTRIBUTES, @MEDEWERKER_ATTRIBUTES);

            TODO: {
                local $TODO = "AUTUMN2015BREAK: ontvanger needs fixing" if $attribute_name =~ /^ontvanger/;
                prove_attribute($clone_case, $attribute_name);
            };
        }

        my $aanvrager_naamgebruik = $clone_case->systeemkenmerk('aanvrager_naamgebruik');
        ok $aanvrager_naamgebruik, "Naamgebruik filled in for natuurlijk_persoon";
        is $aanvrager_naamgebruik, $clone_case->systeemkenmerk('aanvrager_achternaam'), "Naamgebruik equals achternaam";

        is $clone_case->systeemkenmerk('aanvrager_geboorteplaats'),
            $clone_case->aanvrager_object->geboorteplaats,
            "Attribute aanvrager_geboorteplaats works";

        my $qr = qr/^\d{4}\s[A-Z]{2}$/i;
        like($clone_case->systeemkenmerk('aanvrager_postcode'),$qr , "Zipcode formatted correctly");

        is $clone_case->systeemkenmerk('aanvrager_geboorteland'), GEBOORTELAND, "Attribute aanvrager_geboorteland works";

        is $clone_case->systeemkenmerk('aanvrager_status'), 'Overleden', "Status of requestor is 'Overleden'";

        is $clone_case->systeemkenmerk('aanvrager_in_onderzoek'), "In onderzoek", "Aanvrager is in onderzoek";


        is $clone_case->systeemkenmerk('aanvrager_datum_ontbinding_partnerschap'),
            YESTERDAY->strftime('%d-%m-%Y'),
            'Attribute aanvrager_datum_ontbinding_partnerschap works';

    }, 'Prove natuurlijk_persoon attributes');
}


sub attributes_requestor_status : Tests {
    $zs->zs_transaction_ok(sub {
        $zs->schema->default_resultset_attributes->{delayed_touch} = Zaaksysteem::Zaken::DelayedTouch->new;
        my $case = $zs->create_case_ok;

        my $aanvrager = $case->aanvrager_object;
        $aanvrager->woonplaats('Wookie Planet');
        $case->_touch;

        is $case->systeemkenmerk('aanvrager_status'), 'Verhuisd', "Status of requestor is 'Verhuisd'";
        is $case->systeemkenmerk('aanvrager_in_onderzoek'), "Niet in onderzoek", "Aanvrager is in onderzoek";

    }, 'Prove natuurlijk_persoon aanvrager_status');
}



sub attributes_bedrijf : Tests {
    $zs->zs_transaction_ok(sub {
        $zs->set_current_user;
        my $aanvrager = $zs->create_aanvrager_bedrijf_ok;
        my $ontvanger = $zs->create_natuurlijk_persoon_ok;

        my $case = $zs->create_case_ok(
            aanvragers => [$aanvrager],
            ontvanger => 'betrokkene-natuurlijk_persoon-' . $ontvanger->id
        );

        # a lot of the magic strings info are cached at various places. to bypass this,
        # create a new object using the same id.
        my $clone_case = $schema->resultset('Zaak')->find($case->id);

        # prove that for every systeemkenmerk the same value is returned in the
        # renewed state as in the old state, and that isn't empty as to exclude
        # false positives
        TODO: {
            local $TODO = "needs work, testsuite needs to be enhanced with bedrijfauthenticatie knowledge to fix this";
            for my $attribute_name (@BEDRIJF_ATTRIBUTES, @MEDEWERKER_ATTRIBUTES) {
                prove_attribute($clone_case, $attribute_name);
            }
        }

        my $aanvrager_object = $case->aanvrager_object;
        is $case->systeemkenmerk('aanvrager_rechtsvorm'),
            $aanvrager_object->rechtsvorm,
            "Attribute aanvrager_rechtsvorm works";

        is $case->systeemkenmerk('aanvrager_vestigingsnummer'),
            $aanvrager_object->vestigingsnummer,
            "Attribute aanvrager_vestigingsnummer works";

    }, 'Prove bedrijf attributes');
}


sub attributes_medewerker : Tests {
    $zs->zs_transaction_ok(sub {
        $zs->schema->default_resultset_attributes->{delayed_touch} = Zaaksysteem::Zaken::DelayedTouch->new;

        $zs->set_current_user;
        my $medewerker = $zs->create_medewerker_ok;
        my $aanvrager = {
            'betrokkene' => 'betrokkene-medewerker-' . $medewerker->subject_id->id,
            'verificatie' => 'medewerker'
        };

        my $case = $zs->create_case_ok(aanvragers => [$aanvrager]);
        $case->open_zaak;
        $case->_touch;

        # bypass pesky moose caches
        $case = $schema->resultset('Zaak')->find($case->id);

        is $case->systeemkenmerk('behandelaar_voornaam'),
            $case->behandelaar_object->voornamen,
            "Attribute behandelaar_voornaam works";

        is $case->systeemkenmerk('behandelaar_initialen'),
            $case->behandelaar_object->voorletters,
            "Attribute behandelaar_initialen works";

        is $case->systeemkenmerk('behandelaar_achternaam'),
            $case->behandelaar_object->geslachtsnaam,
            "Attribute behandelaar_achternaam works";


        my $aanvrager_naamgebruik = $case->systeemkenmerk('aanvrager_naamgebruik');
        ok $aanvrager_naamgebruik, "Naamgebruik filled in for medewerker";

        is $aanvrager_naamgebruik,
            $case->systeemkenmerk('aanvrager_achternaam'),
            "Naamgebruik equals achternaam";

    }, 'Prove medewerker attributes');
}

sub attributes_simple : Tests {
    $zs->zs_transaction_ok(sub {

        my $pdc_tarief              = 123;
        my $pdc_tarief_balie        = 233;
        my $pdc_tarief_behandelaar  = 333;
        my $pdc_tarief_email        = 456;
        my $pdc_tarief_post         = 567;
        my $pdc_tarief_telefoon     = 678;

        my $definitie = $zs->create_zaaktype_definitie_ok(
            pdc_tarief => $pdc_tarief
        );
        my $zaaktype_node = $zs->create_zaaktype_node_ok(
            zaaktype_definitie => $definitie,
            properties => {
                pdc_tarief_balie => $pdc_tarief_balie,
                pdc_tarief_behandelaar => $pdc_tarief_behandelaar,
                pdc_tarief_email => $pdc_tarief_email,
                pdc_tarief_post => $pdc_tarief_post,
                pdc_tarief_telefoon => $pdc_tarief_telefoon,
            }
        );

        my $zaaktype_status = $zs->create_zaaktype_status_ok(
            status => 1,
            fase   => 'registratiefase',
            node   => $zaaktype_node
        );

        my $afhandelfase = $zs->create_zaaktype_status_ok(
            status => 2,
            fase   => 'afhandelfase',
            node   => $zaaktype_node
        );

        $zs->schema->default_resultset_attributes->{delayed_touch} = Zaaksysteem::Zaken::DelayedTouch->new;
        my $case = $zs->create_case_ok(
            zaaktype => $zs->create_zaaktype_ok(node => $zaaktype_node)
        );
        my $payment_amount = 122.56;

        $case->registratiedatum(YESTERDAY);
        $case->payment_amount($payment_amount);
        $case->update;
        $case->_touch;

        is $case->systeemkenmerk('pdc_tarief'), format_payment_amount($payment_amount), "Attribute pdc_tarief works";

        is $case->systeemkenmerk('tarief_balie'), $pdc_tarief_balie, "Attribute tarief_balie works";
        is $case->systeemkenmerk('tarief_behandelaar'), $pdc_tarief_behandelaar, "Attribute tarief_behandelaar works";
        is $case->systeemkenmerk('tarief_email'), $pdc_tarief_email, "Attribute tarief_email works";
        is $case->systeemkenmerk('tarief_post'), $pdc_tarief_post, "Attribute tarief_post works";
        is $case->systeemkenmerk('tarief_telefoon'), $pdc_tarief_telefoon, "Attribute tarief_telefoon works";

        is $case->systeemkenmerk('zaak_doorlooptijd'), '', 'zaak_doorlooptijd empty for open case';
        is $case->systeemkenmerk('case.lead_time_real'), '', 'Lead time empty for open case';

        $case->afhandeldatum(DateTime->now->add(days => 3));
        $case->update;

        # work around pesky moose cache
        $case = $zs->schema->resultset('Zaak')->find($case->id);

        # case started yesterday, closed 3 days from now
        is $case->systeemkenmerk('case.lead_time_real'), 4, 'Lead time 3+1 days for closed case';

    }, 'Prove simple attributes');
}

sub _make_array { Zaaksysteem::Attributes::_make_array(@_) }

sub attributes_make_array : Tests {
    $zs->zs_transaction_ok(sub {

        is_deeply [_make_array('thing')],
            ['thing'],
            "Returns a list with one item when given a scalar";

        is_deeply [_make_array],
            [],
            "Returns empty list when given no input";

        is_deeply [_make_array([1, 2, 3])],
            [1, 2, 3],
            "Returns a list when given a list ref";

    }, 'Prove _make_array');
}


sub _check_bwcompat { Zaaksysteem::Attributes::_check_bwcompat(@_) }

sub attributes_check_bwcompat : Tests {
    $zs->zs_transaction_ok(sub {

        is_deeply [_check_bwcompat(undef, 'prefix_')],
            [],
            "Returns an empty list when given no bwcompatname";

        is_deeply [_check_bwcompat('test', 'prefix_')],
            ['bwcompat_name', ['prefix_test']],
            "Returns structure when given input";

        is_deeply [_check_bwcompat(['test1', 'test2'], 'prefix_')],
            ['bwcompat_name', ['prefix_test1', 'prefix_test2']],
            "Returns multi structure when given input";


    }, 'Prove _make_array');
}

sub attributes_relations : Tests {
    $zs->zs_transaction_ok(sub {
        $zs->schema->default_resultset_attributes->{delayed_touch} = Zaaksysteem::Zaken::DelayedTouch->new;
        my $case = $zs->create_case_ok;

        $case->_touch;
        # check single
        ok $case->systeemkenmerk('alle_relaties') eq '', "case confesses its lack of relationship";

        my $second = $zs->create_case_ok;
        my $relation = $schema->resultset('CaseRelation')->add_relation($case->id, $second->id);

        # bypass moose caching
        $case = $zs->schema->resultset('Zaak')->find($case->id);

        # check 1 relation
        ok $case->systeemkenmerk('alle_relaties') eq $second->id, "case confesses its relationship";

        my $third = $zs->create_case_ok;
        $relation = $schema->resultset('CaseRelation')->add_relation($case->id, $third->id);

        # bypass moose caching
        $case = $zs->schema->resultset('Zaak')->find($case->id);

        # check 2 relations
        my $desired = $second->id . ", " . $third->id;
        ok $case->systeemkenmerk('alle_relaties') eq $desired, "case confesses its multiple relationships";

        is $case->systeemkenmerk('case.relations'), $desired, "so does case.relations";
        is $case->systeemkenmerk('zaak_relaties'), $desired, "and so does zaak_relaties";

    }, 'Prove case.relations');
}

sub attributes_iterators : Tests {
    $zs->zs_transaction_ok(sub {
        $zs->schema->default_resultset_attributes->{delayed_touch} = Zaaksysteem::Zaken::DelayedTouch->new;
        my $case = $zs->create_case_ok;

        my $second = $zs->create_case_ok;
        my $relation = $schema->resultset('CaseRelation')->add_relation($case->id, $second->id);

        # bypass moose caching
        $case = $zs->schema->resultset('Zaak')->find($case->id);

        my $iterators = $case->get_context_iterators;
        isa_ok $iterators, 'HASH', 'Got hash';

        TODO: {
          local $TODO = 'RUDOLF: Fix this one please';

          is_deeply $iterators->{zaak_relaties}, $iterators->{'case.related_cases'}, 'Aliases work';

          my $related_cases = $iterators->{'case.related_cases'};
          isa_ok $related_cases, 'ARRAY', 'Related cases is array';

          # And fix this:
          #my ($related_case) = @$related_cases;
          #isnt $case->id, $related_case->id, 'Cases have different ids';
        };

    }, 'Prove case attribute iterators',);
}

sub attributes_documents : Tests {
    $zs->zs_transaction_ok(sub {
        $zs->schema->default_resultset_attributes->{delayed_touch} = Zaaksysteem::Zaken::DelayedTouch->new;
        my $case = $zs->create_case_ok;

        my $document_name1 = 'super boring testfile name 1.txt';
        my $document_name2 = 'super boring testfile name 2.txt';
        my $document_name3 = 'super boring testfile name 3.txt';
        my $document_name4 = 'deleted document.txt';

        my $case_document = $zs->create_case_document_ok;
        my $subject = $zs->get_subject_ok;
        my $doc1 = $zs->schema->resultset('File')->file_create({
            db_params => {
                created_by        => $subject,
                case_id           => $case->id,
            },
            case_document_ids => [$case_document->id],
            file_path         => $zs->config->{filestore_test_file_path},
            name              => $document_name1
        });

        my $doc2 = $zs->schema->resultset('File')->file_create({
            db_params => {
                created_by        => $subject,
                case_id           => $case->id,
            },
            file_path         => $zs->config->{filestore_test_file_path},
            name              => $document_name2
        });

        my $doc3 = $zs->schema->resultset('File')->file_create({
            db_params => {
                created_by        => $subject,
                case_id           => $case->id,
            },
            case_document_ids => [$case_document->id],
            file_path         => $zs->config->{filestore_test_file_path},
            name              => $document_name3
        });

        my $doc4 = $zs->schema->resultset('File')->file_create({
            db_params => {
                created_by        => $subject,
                case_id           => $case->id,
                date_deleted      => DateTime->now(),
                accepted          => 1
            },
            case_document_ids => [$case_document->id],
            file_path         => $zs->config->{filestore_test_file_path},
            name              => $document_name4
        });

        $case->_touch;

        my $zaak_documenten = $case->systeemkenmerk('zaak_documenten');
        my $documents = $case->systeemkenmerk('case.documents');
        my $zaak_dossierdocumenten = $case->systeemkenmerk('zaak_dossierdocumenten');
        my $case_documents = $case->systeemkenmerk('case.case_documents');

        sub doc_string {
            my $d = shift;
            return sprintf("%s (%s)", $d->filename, $d->document_status);
        }

        is($zaak_documenten, join(", ", doc_string($doc1), doc_string($doc2), doc_string($doc3)), "zaakdocumenten works");

        is $zaak_documenten, $documents, 'documents also';

        is($zaak_dossierdocumenten, join(", ", doc_string($doc1), doc_string($doc3)), "zaakdossierdocumenten works");
        is $zaak_dossierdocumenten, $case_documents, 'case_documents also';

    }, 'Prove case attribute documents',);
}


sub zs_attributes_magic_strings : Tests {

    my @magic_strings = sort { $a cmp $b } Zaaksysteem::Attributes::ZAAKSYSTEEM_MAGIC_STRINGS();
    my @expect = qw[
        aanleiding
        aanvrager
        aanvrager_aanhef
        aanvrager_aanhef1
        aanvrager_aanhef2
        aanvrager_achternaam
        aanvrager_afdeling
        aanvrager_anummer
        aanvrager_burgerservicenummer
        aanvrager_correspondentie_huisnummer
        aanvrager_correspondentie_nummeraanduiding
        aanvrager_correspondentie_postcode
        aanvrager_correspondentie_straat
        aanvrager_correspondentie_woonplaats
        aanvrager_datum_ontbinding_partnerschap
        aanvrager_email
        aanvrager_geboortedatum
        aanvrager_geboorteland
        aanvrager_geboorteplaats
        aanvrager_geslacht
        aanvrager_geslachtsnaam
        aanvrager_handelsnaam
        aanvrager_has_briefadres
        aanvrager_huisnummer
        aanvrager_huwelijksdatum
        aanvrager_in_onderzoek
        aanvrager_indicatie_geheim
        aanvrager_kvknummer
        aanvrager_login
        aanvrager_mobiel
        aanvrager_naam
        aanvrager_naamgebruik
        aanvrager_nummeraanduiding
        aanvrager_overlijdensdatum
        aanvrager_password
        aanvrager_postcode
        aanvrager_rechtsvorm
        aanvrager_status
        aanvrager_straat
        aanvrager_tel
        aanvrager_type
        aanvrager_verblijf_huisnummer
        aanvrager_verblijf_nummeraanduiding
        aanvrager_verblijf_postcode
        aanvrager_verblijf_straat
        aanvrager_verblijf_woonplaats
        aanvrager_vestigingsnummer
        aanvrager_volledigenaam
        aanvrager_voornamen
        aanvrager_voorvoegsel
        aanvrager_woonplaats
        actieve_selectielijst
        afdeling
        afhandeldatum
        afhandeldatum_volledig
        aggregatieniveau
        alle_relaties
        archiefclassicatiecode
        archiefnominatie
        archiefstatus
        bag
        bedrag_web
        behandelaar
        behandelaar_achternaam
        behandelaar_afdeling
        behandelaar_email
        behandelaar_handtekening
        behandelaar_id
        behandelaar_initialen
        behandelaar_tel
        behandelaar_voornaam
        betaalstatus
        bewaartermijn
        bezwaar_en_beroep_mogelijk
        contactkanaal
        coordinator
        coordinator_email
        coordinator_id
        coordinator_tel
        dagen
        dagen_resterend
        datum
        datum_aangemaakt
        days_left
        days_perc
        deelzaken_afgehandeld
        doel
        doorlooptijd_service
        doorlooptijd_wettelijk
        e_formulier
        gebruiker_naam
        generieke_categorie
        handelingsinitiator
        identificatie
        laatst_gewijzigd
        lex_silencio_positivo
        lokale_grondslag
        omschrijving_of_toelichting
        openbaarheid
        opgeschort_sinds
        opgeschort_tot
        opschorten_mogelijk
        pdc_tarief
        pid
        procesbeschrijving
        publicatie
        publicatietekst
        published
        reden_opschorting
        reden_vroegtijdig_afhandelen
        registratiedatum
        registratiedatum_volledig
        relates_to
        resultaat
        resultaat_omschrijving
        resultaat_toelichting
        selectielijst
        sjabloon_aanmaakdatum
        startdatum
        status
        status
        statusnummer
        streefafhandeldatum
        tarief_balie
        tarief_behandelaar
        tarief_email
        tarief_post
        tarief_telefoon
        tarief_web
        trefwoorden
        trigger
        uiterste_vernietigingsdatum
        uname
        verantwoordelijke
        verantwoordingsrelatie
        verdagingstermijn
        verlenging_mogelijk
        verlengingstermijn
        vertrouwelijkheid
        vertrouwelijkheidsaanduiding
        vervolg_van
        voortgang
        voortgang_status
        wet_dwangsom
        wettelijke_grondslag
        wkpb
        zaak_bedrag
        zaak_documenten
        zaak_doorlooptijd
        zaak_dossierdocumenten
        zaak_fase
        zaak_mijlpaal
        zaak_onderwerp
        zaak_onderwerp_extern
        zaak_relaties
        zaaknummer
        zaaknummer_hoofdzaak
        zaaktype
        zaaktype_id
        zaaktype_versie
        zaaktype_versie_begindatum
        zaaktype_versie_einddatum
    ];

    is_deeply(\@magic_strings, \@expect, "All magic strings found!");
}

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

