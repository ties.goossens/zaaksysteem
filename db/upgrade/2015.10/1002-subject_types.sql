BEGIN;
    ALTER TABLE subject DROP CONSTRAINT subject_subject_type_check;
    ALTER TABLE subject ADD  CONSTRAINT subject_subject_type_check
        CHECK (subject_type IN ('natuurlijk_persoon', 'bedrijf', 'employee'));
COMMIT;
