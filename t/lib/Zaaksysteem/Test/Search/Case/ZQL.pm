package Zaaksysteem::Test::Search::Case::ZQL;
use Moose;
extends "Zaaksysteem::Test::Moose";

=head1 NAME

Zaaksysteem::Test::Search::Case::ZQL - Test Zaaksysteem::Search::Case::ZQL

=head1 DESCRIPTION

Tests for the ZQL query parser for case

=head1 SYNOPSIS

    prove -lv :: Zaaksysteem::Test::Search::Case::ZQL

=cut

use Zaaksysteem::Search::Case::ZQL;
use Zaaksysteem::Test;
use Zaaksysteem::Test::Mocks;

sub test_zql_simple {

    throws_ok(sub {
        Zaaksysteem::Search::Case::ZQL->new('SELECT {} FROM foobar');
        },
        qr#case/zql/object_type: #,
        "Can only look for cases",
    );

    my $zql = Zaaksysteem::Search::Case::ZQL->new('SELECT {} FROM case');
    isa_ok($zql, 'Zaaksysteem::Search::Case::ZQL');

    isa_ok($zql->cmd, 'Zaaksysteem::Search::ZQL::Command::Select', 'Command
        object'); isa_ok($zql->cmd->from,
        'Zaaksysteem::Search::ZQL::Literal::ObjectType', 'FROM object');
    is($zql->cmd->where, undef, 'Query without WHERE clause parsed properly');
}


__PACKAGE__->meta->make_immutable();

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2021, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
