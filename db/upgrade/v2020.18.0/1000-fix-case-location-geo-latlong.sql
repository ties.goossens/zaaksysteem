BEGIN;

    UPDATE zaak_bag
    SET bag_id = bag_nummeraanduiding_id
    WHERE bag_id LIKE 'nummeraanduiding-%'
    AND bag_type = 'nummeraanduiding'
    ;

COMMIT;
