package Zaaksysteem::DB::Component::ContactRelationView;
use base qw/DBIx::Class::Row/;
use Zaaksysteem::Moose;

sub as_object {
    my $self = shift;

    return {
        type     => 'contact/link',
        instance => {
            contact => {
                reference => $self->contact_uuid,
                type      => $self->contact_type
            },
            relation => {
                reference => $self->relation_uuid,
                type      => $self->relation_type
            },
        },
    };
}



1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2020, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
