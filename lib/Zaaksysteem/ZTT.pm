package Zaaksysteem::ZTT;

use Moose;

use BTTW::Tools;
use Zaaksysteem::Constants;
use Zaaksysteem::StatsD;

use Zaaksysteem::ZTT::JSONPath;
use Zaaksysteem::ZTT::Element;
use Zaaksysteem::ZTT::Tag;
use Zaaksysteem::ZTT::Template;
use Zaaksysteem::ZTT::Constants qw[
    ZTT_DEFAULT_OPERATORS
    ZTT_DEFAULT_FUNCTIONS
];

use DateTime::Format::Strptime qw(strptime);
use Locales;
use Number::Format;

with 'MooseX::Log::Log4perl';

=head1 NAME

Zaaksysteem::ZTT - Zaaksysteem Template Toolkit

=head1 DESCRIPTION

This package abstracts the processing of OpenOffice or plaintext templates.

=head1 ATTRIBUTES

=head2 string_fetchers

This attribute contains all the stringfetchers the ZTT instance can use.

String fetchers are coderefs that reference functions that take as their first
argument a ZTT::Tag object, and return a ZTT::Element object containing the
value to be injected in the document.

=head3 Delegations

Via the L<Array|Moose::Meta::Attribute::Native::Trait::Array> trait.

=over 4

=item add_string_fetcher => push

=item all_string_fetchers => elements

=back

=cut

has string_fetchers => (
    is => 'rw',
    isa => 'ArrayRef[CodeRef]',
    traits => [qw[Array]],
    default => sub { [] },
    handles => {
        add_string_fetcher => 'push',
        all_string_fetchers => 'elements'
    }
);

=head2 tag_value_cache

Contains key<->value pairs for a tag's name (magic string) to it's eventual
value.

=head3 Delegations

Via the L<Array|Moose::Meta::Attribute::Native::Trait::Hash> trait.

=over 4

=item cache_tag_value => set

=item is_tag_value_cached => exists

=item get_cached_tag_value => get

=back

=cut

has tag_value_cache => (
    is => 'rw',
    isa => 'HashRef[Zaaksysteem::ZTT::Element]',
    traits => [qw[Hash]],
    default => sub { {} },
    handles => {
        cache_tag_value => 'set',
        is_tag_value_cached => 'exists',
        get_cached_tag_value => 'get'
    }
);

=head2 iterators

This attribute may contain iterable contexts that the template may request.

Conceptually, objects associated with the main context for this template can
be fed to ZTT, and L</process_template> will use them when a iteration context
is declared in the template.

For example, in a plaintext template:

    Ohai [[ name ]],

    Your orders: [[ itereer:case_relations:order_title ]].

    Cya

When the above template is processed, ZTT expects there to be an iterator
context named C<case_relations>, which in turn has fetcher(s) for the
C<order_title> attribute.

=head3 Delegations

Via the L<Hash|Moose::Meta::Attribute::Native::Trait::Hash> trait.

=over 4

=item set_iterator => set

    $ztt->set_iterator(case_relations => sub { # return contexts arrayref });

=back

=cut

has iterators => (
    is => 'ro',
    isa => 'HashRef[CodeRef]',
    traits => [qw[Hash]],
    default => sub { {} },
    handles => {
        set_iterator => 'set'
    }
);

=head2 iterator_contexts_cache

Contains key<->value pairs for a context's name to the objects the context
it references.
value.

=cut

has iterator_contexts_cache => (
    is => 'ro',
    isa => 'HashRef[ArrayRef]',
    traits => [qw[Hash]],
    default => sub { {} },
    handles => {
        is_iterator_context_cached => 'exists',
        set_iterator_context_cache => 'set',
        get_iterator_context_cache => 'get'
    }
);

=head2 cache

Generic cache attribute. String fetchers can use this HashRef to store data
for local retrieval later on.

The cache is actively passed around to sub-templates.

=cut

has cache => (
    is => 'rw',
    isa => 'HashRef',
    default => sub { {} },
);

=head2 functions

This is a WIP implementation. It works for a handful of functions, but should
be refactored to something nicer (with integration with processes and such).

Handles C<set_function>, C<get_function>, and C<has_function> proxies.

=cut

has functions => (
    is => 'rw',
    isa => 'HashRef[CodeRef]',
    traits => [qw[Hash]],

    handles => {
        set_function => 'set',
        has_function => 'exists',
        get_function => 'get'
    },

    default => sub { return ZTT_DEFAULT_FUNCTIONS }
);

=head2 operators

This attribute holds a hashref of operators keys, implemented via subref
values. Defaults to a set implementing addition, subtraction, multiplication,
and devision. Overriding / augmenting this set can be done via the
C<set_operator> proxy method.

    $ztt->set_operator('**', sub { shift ** shift });

Handles C<set_operator>, C<get_operator>, and C<has_operator> proxies.

=cut

has operators => (
    is => 'rw',
    isa => 'HashRef[CodeRef]',
    traits => [qw[Hash]],

    handles => {
        set_operator => 'set',
        has_operator => 'exists',
        get_operator => 'get'
    },

    default => sub { return ZTT_DEFAULT_OPERATORS }
);

=head2 locale

Locale to use for number/date/etc. formatting.

=cut

has locale => (
    is      => 'ro',
    isa     => 'Str',
    default => 'nl',
);

=head2 parser

This parser is a lazy-loaded instance of L<Zaaksysteem::ZTT::MagicDirective>.
It's used when evaluating a call to C<eval> in ZTT.

=cut

has parser => (
    is => 'rw',
    isa => 'Zaaksysteem::ZTT::MagicDirective',
    lazy => 1,
    default => sub {
        return Zaaksysteem::ZTT::MagicDirective->new;
    }
);

=head1 METHODS

=head2 add_context

This method adds a template processing context to the ZTT instance. A context
can be one of three things, an object, an arrayref, or a hashref.

The object variant is most likely what you'd want to use when adding a new
processing context. It is expected to have a method C<get_string_fetchers>
which should return a list of string fetchers. These will be added to the
L</string_fetchers> attribute. The object may also define a
C<get_context_iterators> method, which is expected to return a hashref of
coderefs which when executed return arrays of contexts to be used in
subtemplates.

The idiomatic implementation of the object-variant can be found in
L<Zaaksysteem::Zaken::Roles::ZTT/get_string_fetchers>.

The second form, where the provided context is a hashref, is simpler. The
hashref will be used as a source of string fetcher data. If the template
engine encounters a tag, and hits this context, the tag name will be used as
a key for which the hashref may have a value. That value is then plainly
returned.

    $ztt->add_context({ name => 'Derpy', age => 15 });
    my $doc = $ztt->process_template('[[ age ]] year old [[ name ]] is a few fries short of a happy meal.');

    warn $doc;     # will print '15 year old Derpy is a few fries short of a happy meal.'

The last form, where the provided context is an arrayref, is somewhat
different. A string fetcher will be added to the ZTT instance that simply
shifts the first value off the arrayref every time it is hit.

    $ztt->add_context([qw[a b c d e f g]]);
    my $doc = $ztt->process_template('[[ name ]] [[ tag ]] [ derp ]]');

    warn $doc;      # will print 'a b c'

=cut

has ztt_jpath => (
    is      => 'ro',
    isa     => 'Zaaksysteem::ZTT::JSONPath',
    default => sub { return Zaaksysteem::ZTT::JSONPath->new() },
    lazy    => 1,
);

sub add_context {
    my $self = shift;
    my $context = shift;

    $self->ztt_jpath->add_context($context);

    if (blessed $context) {
        unless($context->can('get_string_fetchers')) {
            throw('ztt/add_context', sprintf(
                "Provided a %s instance, but package does not provide string fetchers",
                blessed($context)
            ));
        }

        $self->add_string_fetcher($context->get_string_fetchers($self->cache));

        if ($context->can('get_context_iterators')) {
            my $iterators = $context->get_context_iterators;
            if (keys %$iterators) {
                $self->set_iterator(%{ $context->get_context_iterators });
            }
        }

        return $self;
    }

    if (ref $context eq 'HASH') {
        $self->add_string_fetcher(sub {
            my $tag = shift;
            my $name = $tag->name;

            $name =~ s/^attribute\.//g;

            my $value = $context->{ $tag->name } // $context->{$name}
                // $context->{ 'attribute.' . $name } // $context->{ 'case.' . $name };

            $self->log->trace(
                sprintf(
                    "Element %s/%s has value '%s'",
                    $tag->name, $name, dump_terse($value)
                )
            );

            return unless defined $value;
            return Zaaksysteem::ZTT::Element->new(value => $value);
        });

        return $self;
    }

    if (ref $context eq 'ARRAY') {
        $self->add_string_fetcher(sub {
            my $value = shift @{ $context };

            return unless $value;
            return Zaaksysteem::ZTT::Element->new(value => $value);
        });

        return $self;
    }

    throw('ztt/add_context', 'Unable to interpret context, not a blessed object, not a HASHREF or ARRAY, giving up man.');
}

=head2 apply_formatter

In templates you can use the construct [[registratiedatum | date]]
which should yield something like 'maandag 14 april 2013' instead
of 14-04-2013. When parsing the parsed tag gets its formatter property
set. This function scans for such a property and applies the filter.

If no formatter is set, this will leave the value of the element untouched.

If the value of the element can not be parsed properly, the original value
will be retained.

I would prefer to build this with the element outputting a formatted version
itself, however this would have high impact and would require a fair amount
of tests to be added.

=cut

define_profile apply_formatter => (
    required => [qw/element/],
    optional => [qw/formatter/],
    typed => {
        element => 'Zaaksysteem::ZTT::Element',
        formatter => 'Maybe[HashRef]'
    }
);

sub apply_formatter {
    my $self = shift;
    my $params = assert_profile(shift)->valid;

    my $element = $params->{element};
    my $formatter = $params->{formatter};

    my $value = $element->value;

    unless ($formatter) {
        $self->log->trace("No formatter");
        return;
    }

    my $formatter_name = $formatter->{name};
    $self->log->trace("Formatter name is $formatter_name");

    if ($formatter_name eq 'date') {

        if (ref($value) eq 'ARRAY') {
            $value = $value->[0];
        }

        return if (!defined $value || !length($value));

        # strptime is sensitive and will bork out if given incorrect input
        try {
            my $datetime = strptime('%d-%m-%Y', $value);
            my $formatted
                = $datetime->set_locale('nl')->strftime('%{day} %B %Y');
            $element->value($formatted);
        }
        catch {
            $self->log->warn(
                sprintf(
                    "Exception when ztt formatting: %s. Could not strptime '%s', not applying formatting",
                    $_, $value
                )
            );
        };

        return;
    }

    if($formatter_name eq 'link_to') {
        my ($link_target, $link_name) = @{ $formatter->{ args } };

        unless($link_name) {
            $link_name = $link_target;
        }

        my $value = $element->value;

        $value = shift @{ $value } if ref $value eq 'ARRAY';

        if ($value) {
            $element->type('hyperlink');
            $element->value(sprintf($link_target, $value));
            $element->title(sprintf($link_name, $value));
        }
    }

    if($formatter_name eq 'hyperlink') {
        my ($uri, $title) = @{ $formatter->{ args } };

        $element->type('hyperlink');
        $element->value($uri);
        $element->title($title);
    }

    if($formatter_name eq 'image_size' && $value) {
        my ($width, $height) = @{ $formatter->{ args } };

        $element->type('image');
        $element->value(
            {
                image_path => $value,
                width      => $width,
                height     => $height,
            }
        );
    }

    if($formatter_name eq 'currency') {

        my ($format) = @{ $formatter->{ args } };

        my $decimals = 2;
        if ($format && $format eq 'whole_number') {
            $decimals = 0;
        }

        my $locale = Locales->new($self->locale);

        # TODO This doesn't cope with locales that don't have groups of 3
        # digits between their "thousands" separator.
        my $numf = $locale->numf(1);

        my $formatter = Number::Format->new(
            THOUSANDS_SEP  => ($numf == 1 ? ',' : '.'),
            DECIMAL_POINT  => ($numf == 1 ? '.' : ','),
            DECIMAL_DIGITS => $decimals,
            DECIMAL_FILL   => 1,
        );

        if (ref($value) eq 'ARRAY') {
            $value = $value->[0];
        }

        $element->cleaner_skip_set(valuta => 1);
        if (defined $value) {
            $element->value($formatter->format_number($value));
        }
        else {
            $element->value($value);
        }
    }

    if ($formatter_name eq 'list') {
        $element->type('list');
        $element->cleaner_skip_set(array => 1);
    }

    if ($formatter_name eq 'break') {
        my $value = $element->value;

        # Check that the value has *some* printable chars.
        if ($value =~ m[\w]u) {
            $element->value($value . "\n");
        }
    }

    if ($formatter_name eq 'strip_when_empty') {
        $element->strip_when_empty(1);
    }
}

=head2 fetch_string

Scan through the available string fetcher to see if any of these
are able to provide an interpolation for the given magic string.

The first one to answer with an interpolation gets the cake. This
means that when configuring the string fetchers the order matters,
if they share namespace.

=cut

sig fetch_string => 'Zaaksysteem::ZTT::Tag => Zaaksysteem::ZTT::Element';

sub fetch_string {
    my $self = shift;
    my $tag = shift;
    my $element;

    if ($self->is_tag_value_cached($tag->name)) {
        $element = $self->get_cached_tag_value($tag->name);
    } else {
        for my $fetcher ($self->all_string_fetchers) {
            my $t1 = Zaaksysteem::StatsD->statsd->start;

            $element = $fetcher->($tag);

            # I'd like this to be 'if defined ...', but I'm not 100% sure
            # that all string fetchers in existance play nicely.
            if ($element) {
                $self->cache_tag_value($tag->name, $element);
                last;
            }
        }
    }

    # Return an empty element by default, if it cannot be found by string fetchers
    # the tag should be removed from the document still
    unless (defined $element) {
        $element = Zaaksysteem::ZTT::Element->new(value => '', defined => 0);

        $self->cache_tag_value($tag->name, $element);
    }

    my $rv = $element->clone;

    return $rv;
}

=head2 iterator_contexts

Convenience method for retrieving context by name.

Memoizes return value via L</iterator_context_cache>.

=cut

sig iterator_contexts => 'Str';

sub iterator_contexts {
    my $self = shift;
    my $name = shift;

    if ($self->is_iterator_context_cached($name)) {
        return $self->get_iterator_context_cache($name);
    }

    unless (exists $self->iterators->{ $name }) {
        throw('ztt/template/iterator', sprintf(
            'Unable to find iterator for type "%s"',
            $name
        ));
    }

    my $contexts = $self->iterators->{ $name }->();

    $self->set_iterator_context_cache($name, $contexts);

    return $contexts;
}

=head2 eval_expression

This method provides support for 'executing' mathemagical directives. It takes
as an argument the parse tree produced by the
L<Zaaksysteem::ZTT::MagicDirective> parser, and attempts to evaluate it.

    Examples of input:

    'non-magic' # Strings evaluate to themselves

    \'magic_string' # This should eval to the value of the attribute

    [ '+', 1, 2 ] # This should eval to 3

    [ '+', [ '*', 2, 3 ], 1 ] # This should eval to 7

=cut

sub eval_expression {
    my $self = shift;
    my $expression = shift;

    my $reftype = ref $expression;

    # Simple case, return if we're down to a constant
    unless ($reftype) {
        return $expression;
    }

    # In case of a scalar reference we want to dereference the expression
    # and fetch it as a magic_string.
    if ($reftype eq 'SCALAR') {
        my $element = $self->fetch_string(Zaaksysteem::ZTT::Tag->new(
            expression => $expression
        ));

        return undef unless $element->defined;
        return $element->value;
    }

    if ($reftype eq 'ARRAY') {
        # Array expressions imply prefix binary cruft, unpack it.
        my ($op, $x, $y) = @{ $expression };

        unless ($self->has_operator($op)) {
            throw('ztt/eval_expression/operator_unsupported', sprintf(
                'Unsupported operator: %s',
                $op
            ));
        }

        return $self->get_operator($op)->(
            $self->eval_expression($x),
            $self->eval_expression($y)
        );
    }

    if ($reftype eq 'HASH') {
        my $fn = $expression->{ call };
        my @args = @{ $expression->{ args } };

        # Here follow a few functions that may or may not evaluate their
        # arguments, which is not the default behavior for functions. Only
        # add things here if they do some syntactual magic.
        if ($fn eq 'deparse') {
            return $self->parser->deparse(@args);
        }

        if ($fn eq 'eval') {
            my $subexpression = eval {
                my @eval_args = map {
                    $self->eval_expression($_)
                } @args;

                $self->parser->parse(@eval_args)->{ expression }
            };

            unless (defined $subexpression) {
                $self->log->warn(sprintf(
                    'Parse error in eval() for "%s"',
                    $args[0]
                ));

                return undef;
            }

            return $self->eval_expression($subexpression);
        }

        if ($fn eq 'or') {
            unless (scalar @args) {
                throw('ztt/eval_expression/no_arguments', sprintf(
                    "Call to or() without arguments, behavior is undefined"
                ));
            }

            for my $subexpression (@args) {
                return 1 if $self->eval_expression($subexpression);
            }

            return '';
        }

        if ($fn eq 'and') {
            unless (scalar @args) {
                throw('ztt/eval_expression/no_arguments', sprintf(
                    "Call to and() without arguments, behavior is undefined"
                ));
            }

            for my $subexpression (@args) {
                return '' unless $self->eval_expression($subexpression);
            }

            return 1;
        }

        if ($fn eq 'not') {
            unless (scalar @args) {
                throw('ztt/eval_expression/no_arguments', sprintf(
                    "Call to not() without arguments, behavior is undefined"
                ));
            }

            return $self->eval_expression($args[0]) ? '' : 1;
        }

        if ($fn eq 'j') {
            return $self->ztt_jpath->resolve_jpath($args[0]);
        }

        unless ($self->has_function($fn)) {
            throw('ztt/eval_expression/function_not_found',, sprintf(
                'Function "%s" was not found, cannot call.',
                $expression->{ call }
            ));
        }

        my $function = $self->get_function($fn);
        my @eval_args = map { $self->eval_expression($_) } @args;

        return $function->(@eval_args);
    }

    throw('ztt/eval_expression/invalid_expression_type', sprintf(
        'Invalid reference type (%s) for expression',
        $reftype
    ));
}

=head2 process_template

This method takes one parameter and attempts to process it as a template. If
the provided item is not an instance of L<Zaaksysteem::ZTT::Template>, that
package's L<new_from_thing|Zaaksysteem::ZTT::Template/new_from_thing>
constructor will be used to create a template instance.

Next, this method will collect all modifications that can be found in the
template body, and will attempt to apply them to the template. A singular run
of applying modifications may result in a finished document, or an
intermediate document that has new directives that need to be processed. In
this latter case up to 10 interative attempts are done to resolve all
directives.

The simplest form of template processing would be something like the
following:

    my $document = $ztt->process_template("[[ system.uname ]]");

=cut

sub process_template {
    my $self = shift;
    my $template = shift;
    my $recurse_limit = shift || 10;

    my $t0 = Zaaksysteem::StatsD->statsd->start;

    unless (eval { $template->isa('Zaaksysteem::ZTT::Template'); }) {
        $template = Zaaksysteem::ZTT::Template->new_from_thing($template);
    }

    while($recurse_limit--) {
        my @mods = $template->modifications;

        $self->log->trace(sprintf(
            'Processing iteration %d',
            10 - $recurse_limit,
        ));

        last unless scalar(@mods);

        for my $mod (@mods) {

            if ($self->log->is_trace) {
                $self->log->trace(
                    sprintf('Applying %s: %s',
                        $mod->type, $mod->selection->summary)
                );
            }

            $self->apply($mod, $template);
        }
    }

    if($template->can('post_process')) {
        $self->log->trace('Postprocessing');
        $template->post_process($self);
    }

    Zaaksysteem::StatsD->statsd->end("ztt_process_template", $t0);

    return $template;
}

=head2 apply

This method takes a modification and a template, and attempts to apply said
modification to the template. Currently the following modification types are
generically supported:

=over 4

=item replace

Calls L<Zaaksysteem::ZTT::Template/replace> with the modification's text
selection and a fetched_string.

=item iterate

Calls L<Zaaksysteem::ZTT::Template/iterate> with a registered iteration
context and the modification's selection.

=item iterate_inline

Calls L<Zaaksysteem::ZTT::Template/iterate_inline> with a registered iteration
context and the modification's selection.

=back

This method may throw a C<ztt/template/apply> exception when a unknown
modification type is found. It also will not wrap the concrete application
call in a try/catch block, so lower-level exceptions may bubble up.

Also a C<ztt/template/iterator> or C<ztt/template/inline_iterator> exception
may be thrown when a iterator context that is not registered is requested by
a modification.

=cut

sub apply {
    my ($self, $mod, $template) = @_;

    $template->increment_modifications;

    my %dispatch = (
        replace => sub {
            my $tag = $mod->selection->tag;

            my $element;

            # Special case for magic string expressions, fetch_string builds
            # a special Element instance with meta information from the
            # attribute
            if (ref $tag->expression eq 'SCALAR') {
                $element = $self->fetch_string($tag);
            } else {
                $element = Zaaksysteem::ZTT::Element->new(
                    value => $self->eval_expression($tag->expression)
                );

                unless ($tag->has_formatter) {
                    $tag->formatter({
                        name => 'currency',
                        args => [ 'whole_number' ]
                    });
                }
            }

            if ($element->has_value && $tag->formatter) {
                $self->apply_formatter(
                    {
                        element   => $element,
                        formatter => $tag->formatter
                    }
                );
            }

            $template->replace(
                $self,
                $mod->selection,
                $element,
                $self->string_fetchers
            );
        },

        show_when => sub {
            my $result = $self->eval_expression(
                $mod->selection->show_when->{ expression }
            );

            unless ($result) {
                $template->remove_section($mod->selection);
            }
        },

        iterate => sub {
            my $contexts = $self->iterator_contexts(
                $mod->selection->iterate
            );

            if ($mod->selection->show_when) {
                $contexts = [ grep {
                    $self->new(cache => $self->cache)->add_context($_)->eval_expression(
                        $mod->selection->show_when->{ expression }
                    );
                } @{ $contexts } ];
            }

            $template->iterate($self, $contexts, $mod->selection);
        },

        iterate_inline => sub {
            my $contexts = $self->iterator_contexts(
                $mod->selection->iterate
            );

            $template->iterate_inline($self, $contexts, $mod->selection);
        },
    );

    unless (exists $dispatch{ $mod->type }) {
        throw('ztt/template/apply', "Don't know how to handle modifications of '$mod->type' type.");
    }

    $dispatch{ $mod->type }->($mod, $template);
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
