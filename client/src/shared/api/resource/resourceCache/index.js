// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import apiCacherModule from './../../cacher';
import pull from 'lodash/pull';
import invoke from 'lodash/invokeMap';
import assign from 'lodash/assign';
import reject from 'lodash/reject';
import find from 'lodash/find';
import get from 'lodash/get';
import first from 'lodash/head';
import mapValues from 'lodash/mapValues';
import listenerFn from './../../../util/listenerFn';
import some from 'lodash/some';
import { omit } from 'lodash';

export default angular
  .module('shared.api.resource.resourceCache', [apiCacherModule])
  .factory('resourceCache', [
    '$rootScope',
    '$q',
    'apiCacher',
    'api',
    ($rootScope, $q, apiCacher, api) => {
      let requests = [],
        invalidationListeners = listenerFn(),
        factory;

      let onInvalidation = invalidationListeners.register;

      factory = (
        preferredRequestOptions,
        preferredCacheOptions = { disabled: false, every: 0, poll: false }
      ) => {
        let cache,
          listeners = [],
          data = null,
          runningRequests = [],
          state = 'pending',
          lastUpdated = Number.NEGATIVE_INFINITY,
          requestOptions = preferredRequestOptions,
          cacheOptions = preferredCacheOptions,
          timeout,
          invalidationListener,
          stateChangeListeners = listenerFn(),
          onError = listenerFn();

        let setState = (st) => {
          if (state !== st) {
            state = st;
            stateChangeListeners.invoke(state);
          }
        };

        let invokeUpdateListeners = () => {
          invoke(listeners, 'call', null);
        };

        let setData = (d) => {
          data = d;
          invokeUpdateListeners();
        };

        let store = (nwData) => {
          setState('resolved');

          if (!cacheOptions.disabled) {
            apiCacher.store(requestOptions, nwData);
          } else {
            setData(nwData);
          }
        };

        let hasData = () => !!(data || cache);

        let fetchData = () => {
          let promise;

          if (requestOptions) {
            lastUpdated = Date.now();

            promise = get(
              find(requests, (request) =>
                angular.equals(request.requestOptions, requestOptions)
              ),
              'promise'
            );

            if (!promise) {
              const main = requestOptions.url.includes(
                'https://geodata.nationaalgeoregister.nl/locatieserver/v3'
              )
                ? fetch(
                    `${requestOptions.url}?${new URLSearchParams(
                      omit(requestOptions.params, [
                        'zapi_num_rows',
                        'zapi_page',
                      ])
                    )}`
                  ).then((r) => r.json())
                : api(
                    assign(
                      { method: 'GET' },

                      // convert headers w/ null value to undefined
                      // can't use undefined because it gets lost in the merge() call

                      mapValues(requestOptions, (value, key) => {
                        if (key === 'headers') {
                          return mapValues(value, (v) => {
                            if (v === null) {
                              return undefined;
                            }

                            return v;
                          });
                        }

                        return value;
                      })
                    )
                  ).finally(() => {
                    requests = reject(
                      requests,
                      (request) => request.promise === promise
                    );
                  });

              promise = $q.resolve(
                main.then((response) => {
                  store(response.data || response);

                  return response.data || response;
                })
              );

              requests.push({ requestOptions, promise });
            } else if (
              runningRequests.indexOf(promise) === -1 &&
              cacheOptions.disabled
            ) {
              // make sure request piggybacking works if cache is disabled

              promise = promise.then((response) => {
                store(response);

                return response;
              });
            }

            promise
              .catch((err) => {
                if (!hasData() || ('status' in err && err.status > 0)) {
                  setState('rejected');

                  onError.invoke(err);

                  // TODO: don't set data, call an error handler

                  setData(err && 'data' in err ? err.data : err);
                }
              })
              .finally(() => {
                pull(runningRequests, promise);

                if (cacheOptions.poll) {
                  determineFetchState(); //eslint-disable-line
                }
              });

            runningRequests.push(promise);
          } else {
            promise = $q.when(null);
          }

          return promise;
        };

        let determineFetchState = () => {
          let fetch = true,
            diff;

          if (cacheOptions.every) {
            diff = Date.now() - lastUpdated;
            fetch = diff > cacheOptions.every;
          }

          if (fetch) {
            fetchData();
          } else if (cacheOptions.poll && !timeout) {
            timeout = setTimeout(() => {
              $rootScope.$evalAsync(() => {
                timeout = null;
                determineFetchState();
              });
            }, -(diff - cacheOptions.every) % cacheOptions.every);
          }
        };

        let findCache = () => {
          let latestCache;

          if (cacheOptions.disabled) {
            return;
          }

          if (cache) {
            latestCache = apiCacher.getCacheById(cache.$id);
          }

          if (!latestCache && requestOptions) {
            latestCache = apiCacher.get(requestOptions, cacheOptions);
          }

          if (latestCache) {
            cache = latestCache;
            lastUpdated = Math.max(cache.$lastUpdated, lastUpdated);

            setState('resolved');

            setData(cache.$data);
          }
        };
        let handleCacheUpdate = (/*ids*/) => {
          findCache();
        };

        let cancelRunningRequests = () => {
          let running;

          while (runningRequests.length) {
            running = runningRequests.shift();

            running.abortRequestAndIgnore();

            requests = requests.filter((req) => {
              return !angular.equals(req.requestOptions, requestOptions);
            });
          }
        };

        let setRequestOptions = (newOpts) => {
          cancelRunningRequests();

          requestOptions = newOpts;
          cache = null;

          if (timeout) {
            clearTimeout(timeout);
            timeout = null;
          }

          setState('pending');

          if (newOpts) {
            lastUpdated = Number.NEGATIVE_INFINITY;

            findCache();
            determineFetchState();
          } else {
            setState('resolved');
            setData(null);
          }
        };

        apiCacher.onUpdate.push(handleCacheUpdate);

        setRequestOptions(requestOptions);

        invalidationListener = onInvalidation((clauses) => {
          let shouldReload =
            requestOptions &&
            some(clauses, (clause) => {
              let isMatch;

              if (clause instanceof RegExp) {
                isMatch = clause.test(requestOptions.url);
              } else {
                isMatch = requestOptions.url === clause;
              }

              return isMatch;
            });

          if (shouldReload) {
            fetchData();
          }
        });

        return {
          onUpdate: (fn) => {
            listeners.push(fn);

            fn();

            return () => {
              pull(listeners, fn);
            };
          },
          data: () => data,
          destroy: () => {
            invalidationListener();

            cancelRunningRequests();
            if (timeout) {
              clearTimeout(timeout);
            }
            pull(apiCacher.onUpdate, handleCacheUpdate);
          },
          hasData,
          reload: fetchData,
          state: () => state,
          fetching: () => runningRequests.length > 0,
          asPromise: () => {
            if (!hasData() && runningRequests.length) {
              return first(runningRequests);
            }

            return $q.resolve(data);
          },
          setRequestOptions,
          onError: onError.register,
          onStateChange: stateChangeListeners.register,
        };
      };

      factory.invalidate = (clauses) => {
        invalidationListeners.invoke(clauses);
      };

      return factory;
    },
  ]).name;
