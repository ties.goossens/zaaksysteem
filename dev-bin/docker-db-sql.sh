#!/bin/bash

dirname $0;
upgrade_dir=$(readlink -m $(dirname $0)/../db/upgrade);
db=zaaksysteem

usage_exit() {
    cat <<OEF
Usage: $(basename $0) OPTIONS [ file ] [ directory ]

OPTIONS
    -d database

You can input both files and directories.

OEF
}

upgrade_directory() {
    local directory=$1

    if compgen -G "$directory/*.sql" > /dev/null
    then
        read -r -a files <<< $directory/*.sql
        old_sql=$(printf -- '%s\n' "${files[@]}" | grep -Ev '/(pre|rel|post)-' | sort)
        pre_sql=$(printf -- '%s\n' "${files[@]}" | grep '/pre-' | sort)
        rel_sql=$(printf -- '%s\n' "${files[@]}" | grep '/rel-' | sort)
        post_sql=$(printf -- '%s\n' "${files[@]}" | grep '/post-'| sort)

        for i in  $old_sql $pre_sql $rel_sql $post_sql
        do
            echo $i | grep -q rollback && continue
            psql -U zaaksysteem $db -f $i
        done
    fi
}

while getopts "hd" opts
do
    case $opts in
        h) usage_exit;;
        d) db=$OPTARG;;
    esac
done
shift $((OPTIND - 1))


if [ -z "$*" ]
then
    upgrade_directory "$upgrade_dir/NEXT"
fi

read -r -a files <<< $@
old_sql=$(printf -- '%s\n' "${files[@]}" | grep -Ev '/(pre|rel|post)-' | sort)
pre_sql=$(printf -- '%s\n' "${files[@]}" | grep '/pre-' | sort)
rel_sql=$(printf -- '%s\n' "${files[@]}" | grep '/rel-' | sort)
post_sql=$(printf -- '%s\n' "${files[@]}" | grep '/post-'| sort)

for i in  $old_sql $pre_sql $rel_sql $post_sql
do

    i=$(echo $i | sed -e "s#.*db/upgrade/\?#$upgrade_dir/#")

    if [ -f "$i" ]
    then
        psql -U zaaksysteem $db -f $i
    elif [ -d "$i" ]
    then
        upgrade_directory $i
    else
        echo "File or directory $i does not exists!" 1>&2
    fi
done;


