=head1 NAME

Zaaksysteem::Manual::API::V1::Types::Config::Definition - Type definition for
config/definition objects

=head1 DESCRIPTION

This page documents the serialization of C<config/definition> objects.

=head1 JSON

=begin javascript

	  {
		 "instance" : {
			"config_item_name" : "config_item_name",
			"date_created" : "2018-02-27T09:39:28Z",
			"date_modified" : "2018-02-27T09:39:28Z",
			"default" : null,
			"label" : "Definition label",
			"category" : {
			   "instance" : null,
			   "reference" : "6e1dfd26-a508-46fc-bd96-95d277db3812",
			   "type" : "config/category"
			},
			"value_type" : {
			   "options" : { ... }
			   "parent_type_name" : "text"
			},
			"value_type_name" : "text"
		 },
		 "reference" : "e9cf0c02-f64f-44a2-b7d7-ac6004218084",
		 "type" : "config/definition"
	  }

=end javascript

=head1 INSTANCE ATTRIBUTES

=head2 cardinality E<raquo> L<C<string>|Zaaksysteem::Manual::API::V1::ValueTypes/string>

Cardinality of the configuration item. Enumerated values: C<one>,
C<one-or-more>, or C<zero-or-more>.

=head2 category E<raquo> L<C<object_ref>|Zaaksysteem::Manual::API::V1::ValueTypes/object_ref>

Object reference to the definition's category.

=head2 config_item_name E<raquo> L<C<string>|Zaaksysteem::Manual::API::V1::ValueTypes/string>

Internal slug-like name of the configuration item. Used as a symbol throughout
Zaaksysteem to reference a specific configuration item, instead of hard-coding
UUID's everywhere.

=head2 default E<raquo> C<any>

Default value for the definition. Value depends on the value type of the
definition.

=head2 label E<raquo> L<C<text>|Zaaksysteem::Manual::API::V1::ValueTypes/text>

Localized label for displaying the definition.

=head2 value_type E<raquo> L<C<complex>|Zaaksysteem::Manual::API::V1::ValueTypes/complex>

If present, defines a value type for the definition. This data type is
subject to change.

=head2 value_type_name E<raquo> L<C<string>|Zaaksysteem::Manual::API::V1::ValuesTypes/string>

If present, declares the value type of the definition by the value type's
name.

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

aaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
