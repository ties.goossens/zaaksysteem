// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import isEmpty from './../util/isEmpty';
import isArray from 'lodash/isArray';
import first from 'lodash/head';
import get from 'lodash/get';
import last from 'lodash/last';
import findLastIndex from 'lodash/findLastIndex';
import uniq from 'lodash/uniq';
import take from 'lodash/take';
import merge from 'lodash/merge';
import isUndefined from 'lodash/isUndefined';
import omitBy from 'lodash/omitBy';
import filter from 'lodash/filter';
import identity from 'lodash/identity';
import isEqual from './../util/isEqual';
import shortid from 'shortid';
import { toNumber } from '../../util/number';

export default class VormField {
  static get $inject() {
    return [
      '$scope',
      '$element',
      '$animate',
      'vormTemplateService',
      'vormInvoke',
      'composedReducer',
      'zsConfirm',
    ];
  }

  constructor(
    $scope,
    $element,
    $animate,
    vormTemplateService,
    vormInvoke,
    composedReducer,
    zsConfirm
  ) {
    this.$scope = $scope;
    this.$element = $element;
    this.$animate = $animate;
    this.vormTemplateService = vormTemplateService;
    this.vormInvoke = vormInvoke;
    this.composedReducer = composedReducer;
    this.zsConfirm = zsConfirm;
    this.toNumber = toNumber;
    this.$empty = true;
    this.type =
      typeof this.template() === 'string'
        ? this.template()
        : get(this.template(), 'inherits');
    this.shouldConfirmDelete = this.confirmDelete();
    this.$inputId = shortid().replace(/^\d+/, '');
    this.onInit();
  }

  $getAvailableDelegate() {
    return this.$pool.shift() || this.$createDelegate();
  }

  $reduceValueDelegates(value, disabled, valid) {
    let val = isArray(value) ? value : [value],
      validations = isArray(valid) ? valid : [valid],
      lastWithValue = findLastIndex(val, (v) => !isEmpty(v));

    val = take(val, lastWithValue + 1);

    if (val.length === 0) {
      val = [null];
    }

    return val.map((delegateValue, index) => {
      // re-use the currently available delegates
      let delegate = this.$getAvailableDelegate();

      // compare against model value to prevent issues w/ date field
      // being updated when focused
      if (!isEqual(delegate.value, delegateValue)) {
        delegate.$value = delegateValue;
      }

      delegate.$valid = validations[index] || null;
      delete delegate.removed;

      return delegate;
    });
  }

  $reduceDelegates(valueDelegates, userDels, disabled, editMode, singleValue) {
    let delegates = uniq(valueDelegates.concat(userDels)),
      lastDel = last(delegates),
      lastVal,
      allEmpty = true;

    if (lastDel) {
      lastVal = '$value' in lastDel ? lastDel.$value : lastDel.value;
    }

    if (
      !disabled &&
      (delegates.length === 0 ||
        (editMode === 'empty' && !singleValue && !isEmpty(lastVal)))
    ) {
      delegates = delegates.concat(this.$getAvailableDelegate());
    }

    delegates.forEach((delegate, index) => {
      let isDelegateEmpty, isValid;

      // TODO: we need this ATM to ensure everything updates on-screen
      // at the same time. this reducer's called a frame later than
      // the value reducer for some reason

      // accept update
      if ('$value' in delegate) {
        delegate.value = delegate.$value;

        delete delegate.$value;
      }

      if ('$valid' in delegate) {
        delegate.valid = delegate.$valid;

        delete delegate.$valid;
      }

      isValid = !filter(delegate.valid, identity).length;
      isDelegateEmpty = isEmpty(delegate.value);

      delegate.disabled =
        disabled || (editMode === 'empty' && !isDelegateEmpty);

      delegate.clearable =
        !disabled &&
        !this.$options.disableClear &&
        !(isDelegateEmpty && index === 0);

      delegate.empty = isDelegateEmpty;

      if (!isDelegateEmpty) {
        allEmpty = false;
      }

      delegate.isValid = isValid;

      delegate.classes = {
        'value-item-valid': isValid,
        'value-item-invalid': !isValid,
        'value-item-empty': isDelegateEmpty,
      };
    });

    this.$empty = allEmpty;

    return delegates;
  }

  $isSingleValue() {
    return this.limit() === 1 || this.limit() === undefined;
  }

  $getEditMode() {
    return this.$options.editMode;
  }

  $createDelegate() {
    return {
      value: null,
      removed: false,
      onChange: () => this.$handleDelegateChange(),
      required: this.required,
    };
  }

  getDelegates() {
    return this.$delegateReducer.data();
  }

  $getValueFromDelegates() {
    let delegates = this.getDelegates().filter((delegate) => !delegate.removed),
      parsers = this.parsers(),
      value = delegates.map((delegate) => {
        let val = delegate.value;

        if (val === undefined) {
          val = null;
        }

        val = parsers
          ? parsers.reduce((prev, current) => {
              return current(prev);
            }, val)
          : val;

        return val;
      }),
      lastWithValue = findLastIndex(delegates, (delegate) => delegate.value);

    if (lastWithValue !== -1) {
      value = take(value, lastWithValue + 1);
    }

    if (this.$isSingleValue()) {
      value = value.length ? first(value) : null;
    }

    return value;
  }

  $handleDelegateChange() {
    this.onChange({
      $value: this.$getValueFromDelegates(),
    });
    this.setCorrectFocusElement();
  }

  addDelegate() {
    this.$userDelegates = this.$userDelegates.concat(this.$createDelegate());

    if (
      this.$userDelegates.length === 1 &&
      this.$valueDelegateReducer.data().length === 0
    ) {
      this.$userDelegates = this.$userDelegates.concat(this.$createDelegate());
    }
  }

  clearDelegate(delegate) {
    delegate.removed = true;
    delegate.value = null;

    if (this.$userDelegates.indexOf(delegate) !== -1) {
      this.$userDelegates = this.$userDelegates.filter((d) => d !== delegate);
    }

    this.$pool = this.$pool.filter((d) => d !== delegate);

    this.$handleDelegateChange();
  }

  handleDelegateRemove(delegate) {
    if (this.shouldConfirmDelete) {
      this.zsConfirm(
        'Weet u zeker dat u deze waarde wilt verwijderen?',
        'Verwijderen'
      ).then(() => {
        this.clearDelegate(delegate);
      });
    } else {
      this.clearDelegate(delegate);
    }
  }

  getInputId() {
    return this.$inputId;
  }

  invoke(invokable) {
    return this.vormInvoke(invokable, this.locals());
  }

  invokeData(key) {
    return this.vormInvoke(
      this.templateData() && this.templateData()[key],
      this.locals()
    );
  }

  hasReachedLimit() {
    return (
      this.$isSingleValue() ||
      (this.limit() !== -1 && this.limit() >= this.getDelegates().length)
    );
  }

  isEmpty(vals) {
    let values = vals === undefined ? this.value() : vals,
      empty;

    if (!isArray(values)) {
      empty = isEmpty(values);
    } else {
      empty = values.filter(isEmpty).length > 0;
    }

    return empty;
  }

  isAddButtonVisible() {
    return (
      !this.disabled() &&
      !this.hasReachedLimit() &&
      this.$getEditMode() !== 'empty'
    );
  }

  getModelOptions() {
    return this.$options.modelOptions;
  }

  $handleDelegateReducerUpdate() {
    this.$pool = this.getDelegates();
  }

  getActiveView() {
    let value = this.$getValueFromDelegates();

    return value === null ? 'display' : 'control';
  }

  getControl() {
    const input = this.$element
      .find('vorm-object-suggest-model')
      .find('input')[0];
    const mapInput = this.$element.find('zs-map-search').find('input')[0];

    return this.type === 'map' ? mapInput : input;
  }

  getDisplay() {
    const display = this.$element.find('vorm-object-suggest-display')[0];
    const mapDisplay = this.$element.find('vorm-map-display').find('div')[0];

    return this.type === 'map' ? mapDisplay : display;
  }

  setCorrectFocusElement() {
    this.$animate.on('enter', this.$element, (element, phase) => {
      let activeView = this.getActiveView();

      if (phase === 'close') {
        let elementToActivate =
          activeView === 'control' ? this.getDisplay() : this.getControl();

        try {
          elementToActivate.focus();
        } catch (e) {
          /* ignore failures */
        }

        this.$animate.off('enter', this.$element);
      }
    });
  }

  onInit() {
    let compiler = this.compiler() || this.vormTemplateService;
    let modelOptions = this.modelOptions() || {};

    this.$options = merge(
      {
        modelOptions: {
          allowInvalid: true,
        },
        editMode: 'multiple',
        control: {},
      },
      omitBy(compiler.getDefaults(this.type), isUndefined),
      { modelOptions }
    );

    this.$pool = [];

    this.$userDelegates = [];

    this.$valueDelegateReducer = this.composedReducer(
      { scope: this.$scope },
      this.value,
      this.disabled,
      this.valid
    ).reduce((...rest) => this.$reduceValueDelegates(...rest));

    this.$delegateReducer = this.composedReducer(
      { scope: this.$scope },
      this.$valueDelegateReducer,
      () => this.$userDelegates,
      this.disabled,
      this.$getEditMode(),
      () => this.$isSingleValue()
    ).reduce((...rest) => this.$reduceDelegates(...rest));

    this.$delegateReducer.subscribe(() => this.$handleDelegateReducerUpdate());

    if (this.autofocus() && !this.disabled()) {
      this.$scope.$$postDigest(() => {
        let input = this.$element[0].querySelector(`#${this.getInputId()}`);

        if (input) {
          input.focus();
        }
      });
    }

    compiler.compile(this.template(), this.$scope, this.$element);
  }
}
