// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import contextualActionServiceModule from './../../../shared/ui/zsContextualActionMenu/contextualActionService';
import resourceModule from './../../../shared/api/resource';
import composedReducerModule from './../../../shared/api/resource/composedReducer';
import first from 'lodash/head';
import identity from 'lodash/identity';
import get from 'lodash/get';
import assign from 'lodash/assign';
import seamlessImmutable from 'seamless-immutable';
import template from './template.html';
import shortid from 'shortid';
import './styles.scss';

export default angular
  .module('zsActiveSubject', [
    resourceModule,
    composedReducerModule,
    contextualActionServiceModule,
  ])
  .directive('zsActiveSubject', [
    '$rootScope',
    'composedReducer',
    'resource',
    'contextualActionService',
    ($rootScope, composedReducer, resource, contextualActionService) => {
      return {
        restrict: 'E',
        template,
        scope: {},
        bindToController: true,
        controller: [
          '$scope',
          function (scope) {
            let ctrl = this,
              collapsed = {},
              convertSubject = (subject) => {
                return subject
                  ? {
                      id: subject.id,
                      gmid: subject.gmid,
                      uuid: subject.uuid,
                      title: subject.name,
                      subjectType: subject.type,
                      name: subject.name,
                      address: [
                        subject.street,
                        subject.postal_code,
                        subject.city,
                      ]
                        .filter(identity)
                        .join(', '),
                      email: (subject.email_addresses || []).join(', '),
                      phoneNumber: (subject.telephone_numbers || []).join(', '),
                    }
                  : null;
              },
              createTabFromSubject = (subject) => {
                const subjectTypeV2 = {
                  natuurlijk_persoon: 'person',
                  bedrijf: 'organization',
                };

                return {
                  id: `subject_${subject.id}`,
                  title: subject.name,
                  icon:
                    subject.subjectType === 'natuurlijk_persoon'
                      ? 'account'
                      : 'domain',
                  link: `/main/contact-view/${
                    subjectTypeV2[subject.subjectType]
                  }/${subject.uuid}`,
                  fields: [
                    {
                      name: 'address',
                      icon: 'home',
                      label: subject.address,
                    },
                    {
                      name: 'phonenumber',
                      icon: 'phone',
                      label: subject.phoneNumber,
                    },
                    {
                      name: 'email',
                      icon: 'email',
                      label: subject.email,
                    },
                  ],
                };
              },
              activeSubjectResource = resource('/betrokkene/get_session', {
                scope,
              }).reduce((requestOptions, data) => {
                return convertSubject(first(data));
              }),
              kccModuleResource = resource(
                '/api/v1/sysin/interface/get_by_module_name/kcc',
                { scope }
              ).reduce((requestOptions, data) => first(data)),
              kccActiveResource = resource(
                () => {
                  return kccModuleResource.data()
                    ? '/api/kcc/user/status'
                    : null;
                },
                { scope }
              ).reduce(
                (requestOptions, data) =>
                  get(first(data), 'user_status', 0) === 1
              ),
              callResource = resource(
                () => {
                  return kccActiveResource.data() ? '/api/kcc/call/list' : null;
                },
                { scope, cache: { every: 5 * 1000, poll: true } }
              ),
              tabReducer = composedReducer(
                { scope },
                activeSubjectResource,
                callResource,
                () => collapsed
              ).reduce((activeSubject, calls) => {
                let tabs = [];

                if (activeSubject) {
                  tabs = tabs.concat(
                    assign(createTabFromSubject(activeSubject), {
                      type: 'subject',
                      actions: [
                        {
                          name: 'create-case',
                          icon: 'folder-outline',
                          type: 'button',
                          label: 'Maak zaak aan',
                          click: () => {
                            contextualActionService.openAction(
                              contextualActionService.findActionByName('zaak'),
                              {
                                requestor: {
                                  type: activeSubject.subjectType,
                                  label: activeSubject.title,
                                  data: {
                                    id: activeSubject.id,
                                    uuid: activeSubject.uuid,
                                  },
                                },
                              }
                            );
                          },
                        },
                        {
                          name: 'create-contactmoment',
                          icon: 'comment-plus-outline',
                          type: 'button',
                          label: 'Maak contactmoment aan',
                          click: () => {
                            contextualActionService.openAction(
                              contextualActionService.findActionByName(
                                'contact-moment'
                              ),
                              {
                                subject: {
                                  data: {
                                    id: activeSubject.gmid,
                                    uuid: activeSubject.uuid,
                                  },
                                  type: activeSubject.subjectType,
                                  label: activeSubject.title,
                                },
                              }
                            );
                          },
                        },
                        {
                          name: 'close',
                          icon: 'close',
                          type: 'button',
                          label: 'Deactiveer contact',
                          click: () => {
                            activeSubjectResource.mutate(
                              'active_subject/disable'
                            );
                          },
                        },
                      ],
                    })
                  );
                }

                tabs = tabs.concat(
                  (calls || []).map((call) => {
                    let subject = first(call.betrokkene),
                      tab;

                    if (!subject) {
                      subject = {
                        id: shortid(),
                        name: get(call, 'processor_params.phonenumber'),
                        type: 'natuurlijk_persoon',
                      };
                    }

                    tab = createTabFromSubject(convertSubject(subject));

                    return assign(tab, {
                      id: `call_${call.id}`,
                      type: 'call',
                      actions: [
                        {
                          name: 'accept',
                          icon: 'phone',
                          label: 'Opnemen',
                          click: () => {
                            callResource
                              .mutate('active_subject/kcc/accept', {
                                callId: call.id,
                              })
                              .asPromise()
                              .then(() => {
                                return activeSubjectResource.reload();
                              });
                          },
                        },
                        {
                          name: 'reject',
                          icon: 'phone-hangup',
                          label: 'Ophangen',
                          click: () => {
                            callResource
                              .mutate('active_subject/kcc/reject', {
                                callId: call.id,
                              })
                              .asPromise()
                              .then(() => {
                                return activeSubjectResource.reload();
                              });
                          },
                        },
                      ],
                    });
                  })
                );

                tabs = seamlessImmutable(tabs).map((tab) => {
                  let tabCollapsed = ctrl.isCollapsed(tab.id);

                  return tab.merge({
                    collapsed: tabCollapsed,
                    actions: tab.actions.concat({
                      name: 'minimize',
                      icon: tabCollapsed
                        ? 'window-maximize'
                        : 'window-minimize',
                      label: tabCollapsed ? 'Uitklappen' : 'Inklappen',
                      click: () => {
                        ctrl.toggleCollapse(tab.id);
                      },
                    }),
                    fields: tab.fields.map((field) =>
                      field.merge({ label: field.label || '-' })
                    ),
                  });
                });

                return tabs;
              });

            ctrl.getTabs = tabReducer.data;

            ctrl.isCollapsed = (id) => {
              return !!collapsed[id];
            };

            ctrl.toggleCollapse = (id) => {
              collapsed = assign({}, collapsed, {
                [id]: !ctrl.isCollapsed(id),
              });
            };

            // legacy

            $rootScope.$on('subject.change', () => {
              activeSubjectResource.reload();
            });
          },
        ],
        controllerAs: 'vm',
      };
    },
  ]).name;
