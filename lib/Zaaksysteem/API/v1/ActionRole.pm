package Zaaksysteem::API::v1::ActionRole;

use Moose::Role;
use namespace::autoclean;

use BTTW::Tools;

=head1 NAME

Zaaksysteem::API::v1::ActionRole - Assorted augmentations on API::v1 actions

=head1 SYNOPSIS

    my $action = $c->action;

    if ($action->has_attribute('Scope')) { ... }

=head1 DESCRIPTION

This L<role|Moose::Meta::Role> provides some helper functionality on actions
within the v1 API controller/action infrastructure.

=head1 METHODS

=head2 execute

This method wraps L<Catalyst::Action/execute>. Catalyst's default behavior
when an action dies is to push the error into L<Catalyst/c-error> and
proceed to the next action (if chained).

This wrapper places a try/catch block around the original C<execute> call and
if an exception is caught, it is instead pushed to the L<Catalyst/c-stash> key
C<result>, so the serializer can take care of it.

=cut

around execute => sub {
    my $orig = shift;
    my $self = shift;
    my $controller = shift;
    my $c = shift;
    my @args = @_;

    my $retval;

    try {
        $retval = $self->$orig($controller, $c, @args);
    } catch {
        # Special case for catalyst, plain 'ol passthru
        if (blessed $_ && $_->isa('Catalyst::Exception::Detach')) {
            die $_;
        } elsif( blessed $_ && $_->isa('BTTW::Exception::Base')) {
            $c->res->code($_->http_code);
            $c->stash->{ result } = $_;
        } elsif (blessed $_ && $_->isa('Moose::Exception')) {
            $c->res->code(500);
            $c->stash->{ result } = BTTW::Exception::Base->new(
                type => 'api/v1/server_error',
                message => $_->message,
            );
        } else { # Exception is presumably db, perl or plain die, wrap it up
            $c->res->code(500);
            $c->stash->{ result } = BTTW::Exception::Base->new(
                type => 'api/v1/server_error',
                message => "$_",
            );
        }

        $c->log->info("Exception caught during API v1 chain execution: $_");

        $c->detach;
    };

    return $retval;
};

=head2 attribute

This is a helper method for finding the attributes on actions.

=cut

sub attribute {
    my $self = shift;
    my $key = shift;
    my $callback = shift;

    if($self->isa('Catalyst::ActionChain')) {
        my @attrs = grep { defined }
                    map  { $_->attribute($key, $callback) }
                         @{ $self->chain };

        return wantarray ? @attrs : $attrs[0];
    }

    my $attr = $self->attributes->{ $key };

    return unless $attr;

    if (ref $callback eq 'CODE') {
        return $callback->($attr);
    }

    return $attr;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
