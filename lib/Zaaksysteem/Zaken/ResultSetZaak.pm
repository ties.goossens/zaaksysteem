package Zaaksysteem::Zaken::ResultSetZaak;

use Zaaksysteem::Moose;
use feature 'state';

use Archive::Zip qw( :ERROR_CODES :CONSTANTS );
use Cwd 'abs_path';
use Encode;
use File::Path qw/remove_tree make_path/;
use File::Spec::Functions qw/catfile catdir/;
use File::stat;
use Hash::Merge::Simple qw( clone_merge );
use Net::SCP::Expect;
use Params::Profile;
use Text::CSV;
use XML::Dumper;
use XML::Simple;

use Zaaksysteem::Constants qw/
    ZAAKSYSTEEM_ZAAK_URGENTIE_PERC_HIGH
    ZAAKSYSTEEM_ZAAK_URGENTIE_PERC_MEDIUM
    ZAAKSYSTEEM_ZAAK_URGENTIE_PERC_LATE
/;
use Zaaksysteem::Search::Case::ZQL;
use Zaaksysteem::Zaken::DestroyReport;

extends 'DBIx::Class::ResultSet';


with qw(
    Zaaksysteem::Zaken::Roles::ZaakSetup
    Zaaksysteem::Roles::ZQL
);

my $SPECIAL_WHERE_CLAUSES   = {
    '_urgency'  => '
        abs(ROUND(100 *(
            date_part(\'epoch\', COALESCE(me.afhandeldatum, NOW()) - me.registratiedatum )
            /
            GREATEST(date_part(\'epoch\', me.streefafhandeldatum - me.registratiedatum), 1)
         ) ))
    '
};

### Prevent division by zero (date_part(\'epoch\', me.streefafhandeldatum - me.registratiedatum) + 1)
my $SPECIAL_SELECTS         = {
    # Date - date is integers, cast to date and let postgres do the rest
    'days_left'     => 'me.streefafhandeldatum::date - COALESCE(me.afhandeldatum::date, NOW()::date)',
    'days_perc'     => 'ROUND( 100 *(
            date_part(\'epoch\', COALESCE(me.afhandeldatum, NOW()) - me.registratiedatum )
            /
            GREATEST(date_part(\'epoch\', me.streefafhandeldatum - me.registratiedatum), 1)
         ) )
    ',
    'days_running'  => 'date_part(\'days\', COALESCE(me.afhandeldatum, NOW()) - me.registratiedatum )',
};

=head2 generate_case_id

Retrieve a new, unused case id, without creating a case (used by StUF-ZKN).

=cut

sub generate_case_id {
    my $self = shift;

    my $id;
    $self->result_source->schema->storage->dbh_do(
        sub {
            my ($storage, $dbh, @args) = @_;

            my $sth = $dbh->prepare(q{
                SELECT nextval('zaak_id_seq');
            });
            $sth->execute();

            my @result = $sth->fetchrow_array;
            $id = $result[0];
        }
    );

    return $id;
}

sub search_restricted {
    my ($self, $permission, $where, @rest) = @_;

    my $user = $self->{attrs}{current_user};

    if (!$user) {
        $self->log->info("Unable to determine authenticated user!");
        throw(
            "case/search/restricted/user/unknown",
            "Unable to search restricted without user"
        );
    }

    $permission //= 'search';

    my @acl_clauses = $self->get_acl_subquery(
        $user,
        $permission,
        { "acl.case_id" => { '=' => \'me.id' } },
    );

    return $self->search_rs(
        {
            -and => [
                { 'me.deleted' => undef},
                { @acl_clauses },
            ]
        }
    )->search_rs(
        $where ? $where : (),
        @rest,
    );
}

sub search {
    my $self    = shift;

    my @caller = caller(1);

    if (
        $_[1] &&
        UNIVERSAL::isa($_[1], 'HASH') &&
        (my $order_by = $_[1]->{order_by})
    ) {
        if (UNIVERSAL::isa($order_by, 'HASH')) {
            while (my ($key, $order_by) = each %{ $order_by }) {
                if ($SPECIAL_SELECTS->{$order_by}) {
                    $_[1]->{order_by}->{$key} = $SPECIAL_SELECTS->{$order_by};
                }
            }
        } else {
            if ($SPECIAL_SELECTS->{$order_by}) {
                $_[1]->{order_by} = $SPECIAL_SELECTS->{$order_by};
            }
        }
    }

    my $rs = $self->_prepare_search(@_);
    return $rs->next::method(@_);
}

=head2 search_zql

    my $rs = $schema->resultset("Zaak")->search_zql(
        "SELECT  {} FROM case"
    );

=cut

sub search_zql {
    my ($self, $zql) = @_;

    my $search = Zaaksysteem::Search::Case::ZQL->new($zql);

    my $rs = $self->search_rs(
        undef,
        {
            join => 'zaak_meta',
            '+select' => ['zaak_meta.index_hstore', 'zaak_meta.text_vector'],
            '+as'     => ['index_hstore', 'text_vector'],
        }
    );

    $rs = $search->apply_to_resultset($rs);
    return $rs;
}

=head2 find_v0

Helper for code that intends to use the fetched case in v0 context.

Proxies L<DBIx::Class::ResultSet/find>, while setting prefetch relations for
C<case_properties>, and C<zaak_authorisations> for cheap serialization.

    my $maybe_zaak = $zaak_resultset->find_v0(123);

=cut

sig find_v0 => "Int,Zaaksysteem::Backend::Subject::Component,?Str";

sub find_v0 {
    my ($self, $id, $subject, $level) = @_;

    $level //= 'read';

    my @acl_clauses = $self->get_acl_subquery(
        $subject,
        $level,
        { "acl.case_id" => { '=' => \'me.id' } }
    );

    my $found = $self->search_rs(
        { @acl_clauses },
    )->search_rs(
        { 'me.id' => $id, 'me.deleted' => undef },
        { prefetch => 'case_properties' },
    )->first;

    return $found if $found;
    return;
}

sub get_acl_subquery {
    my ($self, $subject, $permission, $extra_conditions, $case_alias) = @_;

    $case_alias //= 'me';
    $extra_conditions //= {};

    return if not defined $subject;

    # Admins get a free pass
    return if $subject->is_admin;

    my $acl_query = $self->result_source->schema->resultset('CaseACLView')->search_rs(
        {
            'acl.subject_id' => $subject->id,
            'acl.permission' => $permission,
            %$extra_conditions,
        },
        {
            join => 'casetype_id',
            alias => 'acl',
            columns => ['case_id'],
        },
    )->as_query;

    my $user_id = $subject->uidnumber;
    return (
        '-or' => [
            {
                "${case_alias}.aanvrager_gm_id" => $user_id,
                "${case_alias}.aanvrager_type" => 'medewerker'
            },
            { "${case_alias}.behandelaar_gm_id" => $user_id },
            { "${case_alias}.coordinator_gm_id" => $user_id },
            { -exists => $acl_query },
        ],
    );
}

sub _prepare_search {
    my $self                = shift;
    my $where               = shift;
    my $additional_options  = {};

    ## Additional options
    unless ($self->{attrs}->{ran}) {
        $additional_options->{'join'}   = [
            #'zaak_betrokkenen',
            {
                zaaktype_node_id    => 'zaaktype_definitie_id',
#               zaak_kenmerken      => 'zaak_kenmerken_values',
            }
        ];

        $additional_options->{'prefetch'}   = [
            'aanvrager',
            'behandelaar',
            {
                zaaktype_node_id    => 'zaaktype_definitie_id',
                #zaak_kenmerken      => 'zaak_kenmerken_values',
                #zaak_kenmerken      => 'bibliotheek_kenmerken_id',
            }
        ];

        $additional_options->{'+select'}    = [];
        for my $key (sort keys %{ $SPECIAL_SELECTS }) {
            my $value = $SPECIAL_SELECTS->{$key};
            push(
                @{ $additional_options->{'+select'} },
                \$value
            );
        }

#        $additional_options->{'+select'}    = [
#            [ \'date_part(\'days\', NOW() - me.registratiedatum )' ],
#            [ \'date_part(\'days\', me.streefafhandeldatum - NOW())'],
#            [ \'ROUND( 100 *(
#                    date_part(\'epoch\', NOW() - me.registratiedatum )
#                    /
#                    date_part(\'epoch\', me.streefafhandeldatum - me.registratiedatum)
#                 ) )
#            '],
#        ];

        # ROUND(100 * (EXTRACT( EPOCH FROM( AGE( NOW(), me.registratiedatum ) ) )
        # ) / EXTRACT( EPOCH FROM( AGE( me.streefafhandeldatum,
        # me.registratiedatum ) ) )) as percentage_complete

        $additional_options->{'+as'}        = [
            sort keys %{ $SPECIAL_SELECTS }
        ];
    }

    $self->{attrs}->{ran} = 1;

    my $rs = $self->search_rs(undef, $additional_options);

    if (ref $where eq 'HASH' && exists $where->{ urgentie }) {
        warn 'Use of "urgentie" virtual column on zaak table is deprecated, please migrate to "urgency" field.';

        $where->{ _urgency } = delete $where->{ urgentie };
    }

    ### SPECIAL CONSTRUCT FOR URGENT!
    while (my ($column, $definition)    = each %{ $SPECIAL_WHERE_CLAUSES }) {
        next unless UNIVERSAL::isa($where, 'HASH') && $where->{$column};

        my @where_clauses;
        if (UNIVERSAL::isa($where->{$column}, 'ARRAY')) {
            push(@where_clauses, @{ $where->{$column} });
        } else {
            push(@where_clauses, $where->{$column});
        }

        my @sql;
        for my $where_clause (@where_clauses) {
            if ($where_clause && UNIVERSAL::isa($where_clause, 'HASH')) {
                push(@sql, $definition .
                    [ keys(%{ $where_clause }) ]->[0] .
                    [ values(%{ $where_clause }) ]->[0]
                );
            } elsif (
                grep { $_ eq lc($where_clause) } qw/normal medium high late/
            ) {
                if (lc($where_clause) eq 'normal') {
                    push(@sql, $definition . '<' .
                        (100 - (ZAAKSYSTEEM_ZAAK_URGENTIE_PERC_MEDIUM * 100))
                    );
                } elsif ( lc($where_clause) eq 'medium') {
                    push(@sql, $definition . ' < ' .
                        (100 - (ZAAKSYSTEEM_ZAAK_URGENTIE_PERC_HIGH * 100)) .
                        ' AND ' .
                        $definition . ' >= ' .
                        (100 - (ZAAKSYSTEEM_ZAAK_URGENTIE_PERC_MEDIUM * 100))
                    );
                } elsif ( lc($where_clause) eq 'high') {
                    push(@sql, $definition . ' >= ' .
                        (100 - (ZAAKSYSTEEM_ZAAK_URGENTIE_PERC_HIGH * 100))
                        . ' AND ' .
                        $definition . ' < ' .
                        (ZAAKSYSTEEM_ZAAK_URGENTIE_PERC_LATE * 100)
                    );
                } elsif ( lc($where_clause) eq 'late') {
                    push(@sql, $definition . ' >= ' .
                        (ZAAKSYSTEEM_ZAAK_URGENTIE_PERC_LATE * 100)
                    );
                }
            } else {
                die(
                    'ERROR: Special Z::Zaken::ZaakResultSet where '
                    . 'column "' . $column . '" needs HASH/ARRAY as parameter.'
                );
            }
        }
        #my $sql = ' ( me.status != \'resolved\' AND me.status != \'stalled\' AND
        #me.status != \'deleted\' AND ( ' . join(' ) OR ( ', @sql) . ') )';
        my $sql = ' ( ( ' . join(' ) OR ( ', @sql) . ') )';
        $rs = $rs->search_rs(\[ $sql ]);

        delete($where->{$column});
    }

    #$rs->{attrs}->{ran} = 1;

    return $rs->search_rs($where, @_);
}

sub search_extended {
    my ($self, $where, $display, $opts) = @_;

    my $rs = $self->search_rs(
        {
            -and => [
                { 'me.uuid' => \'is not null' },
                { 'me.deleted' => undef },
                $where,
            ]
        },
        $display,
    );

    my $user = $self->{attrs}->{current_user};

    my $schema = $self->result_source->schema;

    if ($user) {
        my @acl_clauses = $self->get_acl_subquery(
            $user,
            'search',
            { "acl.case_id" => { '=' => \'me.id'}},
        );

        $rs = $rs->search_rs(
            { @acl_clauses }
        );
    }

    unless ($opts->{show_future_cases} && $user && $user->is_admin) {
        $rs = $rs->search(
            {
                'me.registratiedatum' => {
                    '<' => $schema->format_datetime_object(DateTime->now())
                }
            }
        );
    }

    return $rs;
}


define_profile get_auth_list => (
    required => [qw/zaaktype_ids role_ids ou_ids confidentiality/]
);

sub get_auth_list {
    my $self         = shift;
    my $arguments    = assert_profile(shift)->valid;

    my $schema       = $self->result_source->schema;
    my $auth_list_rs = $schema->source('ZaaktypeAuthorisation')->resultset->search({
        zaaktype_id  => {
            -in => $arguments->{zaaktype_ids}->get_column('me.id')->as_query,
        },
        role_id      => {
            -in => $arguments->{role_ids},
        },
        ou_id        => {
            -in => $arguments->{ou_ids},
        },

        # we'll be comparing with a array value, but since the string
        # version of an array never matches 'confidential' this should
        # work out fine
        confidential => $arguments->{confidentiality} eq 'confidential' ? 't' : 'f'
    });

    return {
        'me.confidentiality' => {
            -in => $arguments->{confidentiality},
        },
        'me.zaaktype_id'  => {
            -in => $auth_list_rs->get_column('zaaktype_id')->as_query
        }
    };
}


sub with_progress {
    my $self                            = shift;

    return $self->search(@_);
}

sub search_grouped {
    my $self    = shift;
    my $group   = pop;

    #$group = 'zaak_betrokkenen.gegevens_magazijn_id';

#    while (my ($key, $value) = each %{ $self->{attrs} }) {
#        next if $key eq 'betrokkene_model';
#        next if $key eq 'c';
#        warn($key . ' => ' . Dumper($value));
#    }
    my $search = $self->search(@_);
#    while (my ($key, $value) = each %{ $search->{attrs} }) {
#        next if $key eq 'betrokkene_model';
#        next if $key eq 'c';
#        warn($key . ' => ' . Dumper($value));
#    }

    ### Ok, het ziet er uit als een hack, en ja, dat is het ook. Punt is dat
    ### de prefetch functie ons problemen geeft bij het vinden van de me
    ### (zaak) tabel.
    ###
    ### De attrs +select en +as zijn voor het weergeven van extra kolommen
    ### welke uiteraard uit moeten staan: we willen immers alleen een count

    delete($search->{attrs}->{'+select'});
    delete($search->{attrs}->{'+as'});
    delete($search->{attrs}->{'prefetch'});

#    while (my ($key, $value) = each %{ $search->{attrs} }) {
#        next if $key eq 'betrokkene_model';
#        warn($key . ' => ' . Dumper($value));
#    }

    my $search_opts = {};

    my $attrs       = {
        as          => [ 'group_naam', 'group_count' ],
        group_by    => [ $group ],
    };

    if ($group eq 'behandelaar') {
        my $behandelaar_clause = '= zaak_betrokkenen.id';
        $search_opts->{'me.behandelaar'} = \$behandelaar_clause;
        $attrs->{group_by} = [ 'zaak_betrokkenen.gegevens_magazijn_id' ];
        $attrs->{select} = [ 'zaak_betrokkenen.gegevens_magazijn_id', { count => { distinct => 'me.id'} } ],
    } else {
        $attrs->{select} = [ $group, { count => { distinct => 'me.id'} } ],
    }

    return $search->search($search_opts,$attrs);
}

sub overlapt {
    my $self        = shift;
    my $startdt     = shift;
    my $stopdt      = shift;

    $startdt    = $startdt->datetime;
    $stopdt     = $stopdt->datetime;

    $startdt    =~ s/T/ /;
    $stopdt    =~ s/T/ /;

    return $self->search(\[
            "(DATE('" . $startdt . "'), DATE('" . $stopdt . "'))"
                . " OVERLAPS " .
            "(me.registratiedatum, me.afhandeldatum)"
    ]);

    return $self;
}

sub betrokkene_model {
    my $self    = shift;

    return $self->result_source->schema->betrokkene_model;
}

sub gegevens_model {
    my $self    = shift;

    return $self->{attrs}->{dbic_gegevens};
}

sub config {
    my $self    = shift;

    return $self->{attrs}->{config};
}

sub current_user {
    my $self    = shift;

    return unless $self->{attrs}->{current_user};

    return $self->betrokkene_model->get(
        {
            extern  => 1,
            type    => 'medewerker',
        },
        $self->{attrs}->{current_user}->uidnumber
    );
}

=head2 get_open_case_by_id

Get an open case by ID

=cut

sub get_open_case_by_id {
    my ($self, $id) = @_;

    my $case = $self->find($id);

    throw(
        "case/get_open_case_by_id/not_found",
        "Case $id is not found"
    ) if !$case;

    throw(
        "case/get_open_case_by_id/deleted",
        "Case $id is not found"
    ) if $case->is_deleted;

    throw(
        "case/get_open_case_by_id/closed",
        "Case $id is closed"
    ) if $case->is_afgehandeld;

    return $case;
}

define_profile publish => (
    required => [qw(
        profile
        root_dir
        files_dir
        display_fields
        published_file_ids
        published_related_ids
        publish_script
    )],
    optional => [qw(dry_run hostname)],
    defaults => { dry_run => 0 },
);

sub publish {
    my $self = shift;
    my $opts = assert_profile(shift)->valid;

    my $schema = $self->result_source->schema;

    my $profile = $opts->{profile} // die "Need a profile";
    my $interface
        = $schema->resultset('Interface')
        ->search_active({ module => 'legacy_publicaties', name => $profile })
        ->first;

    if (!$interface) {
        throw('ZS/zaak/publish/interface/inactive',
            "Geen actieve publicatie interface gevonden met naam '$profile'");
    }

    my $config = $interface->get_interface_config;
    my $hostname = $config->{hostname};

    my $files_dir = catdir($opts->{files_dir}, abs_path($config->{source_directory}));
    my $root_dir = abs_path($opts->{root_dir});

    my $published_file_ids    = $opts->{published_file_ids};
    my $published_related_ids = $opts->{published_related_ids};

    my $logging = "Export profile is '$profile'.\nGenerating export files..\n";
    my $tmp_publish_dir = catdir($files_dir, 'publish');

    if ($config->{internal_name} eq 'perfectview_tholen') {
        $logging .= $self->_make_publish_dir($tmp_publish_dir);
        $logging .= $self->export_perfectview_tholen(
            {
                files_dir       => $files_dir,
                tmp_publish_dir => $tmp_publish_dir,
                display_fields  => $opts->{display_fields},
                config          => $config,
            }
        );
    }
    elsif ($config->{internal_name} =~ /^vergaderingen_ede/) {
        $tmp_publish_dir = catdir($files_dir, 'vergaderingen_ede', '.');
        $logging .= $self->_make_publish_dir($tmp_publish_dir);

        $logging .= $self->export_vergaderingen_ede(
            {
                files_dir             => $files_dir,
                publish_dir           => $tmp_publish_dir,
                root_dir              => $root_dir,
                published_file_ids    => $opts->{published_file_ids},
                published_related_ids => $opts->{published_related_ids},
            }
        );
    }

    $logging .= "Finished generation of export files for profile '$profile'.\n";

    my $result = 0;

    my $publish_script = _assert_publish_script($opts->{publish_script});

    $logging .= "Starting transfer to external server.\n";

    my $command
        = sprintf(
        '%s --protocol "%s" --hostname "%s" --username "%s" --port "%d" --src_dir "%s" --dst_dir "%s"',
        $publish_script, $config->{protocol}, $hostname, $config->{username}, $config->{port},
        $tmp_publish_dir, $config->{destination_directory});

    if ($config->{notify_url}) {
        $command .= sprintf(' --notify_url "%s"', $config->{notify_url});
        if ($config->{notify_wait}) {
            $command .= sprintf(' --notify_wait "%s"', $config->{notify_wait});
        }
    }

    if ($config->{csv_filename}) {
        $command .= sprintf(' --csv_filename "%s"', $config->{csv_filename});
    }
    if ($config->{notify_filename}) {
        $command .= sprintf(' --notify_filename "%s"', $config->{notify_filename});
    }

    $logging .= "Calling '$command --password *******'\n";
    $command .= sprintf(' --password "%s"', $config->{password});

    unless ($opts->{dry_run}) {

        my $publish_script_output = `$command`;
        # $? contains the exit code from the publish script. if it is 0, all is well,
        # else there is a problem.
        $result = $?;
        $logging .= "Output from transfer script:\n-----------BEGIN-----------\n";
        if ($result) {
            $logging .= "$publish_script_output.\n------------END----------\n";
        }
        else {
            $logging .= "Files transfered to remote host '$hostname'.\n------------END----------\n";
        }
        $logging .= "Exit code from transfer script: '$?'.\n";
        $logging .= "Finished.\n";
    }

    return {
        result  => $result,
        logging => $logging,
    };
}

sub _assert_publish_script {
    my $script = shift;
    unless(-x $script) {
        die "Publicatie script $script is niet beschikbaar, vraag beheer om de serverconfiguratie te corrigeren.\n";
    }
    return $script;
}

sub _make_publish_dir {

    my ($self, $dir) = @_;

    my $logging = "";
    my $error;

    if (-d $dir) {
        remove_tree($dir, { keep_root => 1, error => \$error });
        die "Could not clean up previous publish dir: $dir: ". join("\n", @$error) if @$error;
        $logging .= "Cleaned publish directory '$dir'.\n";
    }
    elsif(!-d $dir) {
        make_path($dir, { error => \$error });
        die "Unable to create publish dir: $dir: ". join("\n", @$error) if @$error;
        $logging .= "Created publish directory '$dir'.\n";
    }
    else {
        die "Unable to create publish dir ($dir): someone created a file or symlink.. ";
    }
    return $logging;
}


sub export_perfectview_tholen {
    my ($self, $opts) = @_;

    my $files_dir       = $opts->{files_dir}        or die "need files_dir";
    my $tmp_publish_dir = $opts->{tmp_publish_dir}  or die "need tmp_publish_dir";
    my $display_fields  = $opts->{display_fields}   or die "need display_fields";
    my $config          = $opts->{config}           or die "need config";

    my $csv = Text::CSV->new({
        binary          => 1,
        sep_char        => ';',
        always_quote    => 1,
        eol             => "\n",
    }) or die "Cannot use CSV: ".Text::CSV->error_diag();

    my $filename = $tmp_publish_dir . "/" . $config->{csv_filename};

    my $logging = "Writing to filename: '$filename'\n";

    # check field order
    my @fields = map { $_->{class} } @$display_fields;
    $logging .= "Checking field format (@fields).\n";
    unless(@fields > 3 && $fields[0] eq 'zaaknummer' && $fields[1] eq 'kenmerk' && $fields[2] eq 'kenmerk') {
        die "Incorrect fields (@fields), use 'Alle zoekresultaten', aborting export.\n";
    }

    my $fh;
    open $fh, ">:encoding(utf8)", $filename or die "could not open $filename: $!";
    print $fh '"RegistratieId";"SubcategorieId";"Omschrijving";' .
        '"Plaats";"Datum";"Foto";"Thumbnail";"Adres";"Postcode";"Telefoon";"Email"'."\n";

    while(my $case = $self->next()) {
        my @columns = ();

        foreach my $display_field (@$display_fields) {

            my $datafieldname   = $display_field->{systeemkenmerk};
            my $value           = $case->systeemkenmerk($datafieldname) || '';

            # get rid of newlines, they mess up csv export
            if($value =~ m|[\r\n]|) {
                die "Lege regels (newlines) aangetroffen in zaak " . $case->id . ".\n";
            }

            push @columns, $value;
        }
        $csv->print ($fh, \@columns);
    }

    close $fh or die "could not close $filename: $!";
    return $logging;
}



sub export_vergaderingen_ede {
    my ($self, $options) = @_;

    my $logging = '';
    while(my $case = $self->next()) {
        $logging .= "Generating XML export for case ". $case->id . ".\n";
        $logging .= $case->export_vergaderingen_ede($options);
    }
    return $logging;
}

sub _generate_uuid {
    my $self    = shift;

    my $ug      = new Data::UUID;
    my $uuid    = $ug->create();
    return $ug->to_string($uuid);
}


=head2 check_destroy

Before deleting a slew of cases, check if they can be destroyed. Generate
a report with errors and warnings. Errors means the case can not be deleted.
Warnings mean that the user needs to be informed of the impact, but the case
can be destroyed after user confirmation. The controller logic is responsible
for checking if the user has sufficiently been warned and responded. Here we
just gather info.

=cut

sub check_destroy {
    my $self = shift;

    # a little custom object to manage the slightly complex structure
    my $destroy_report = new Zaaksysteem::Zaken::DestroyReport;

    while (my $case = $self->next) {
        $destroy_report->add_case({
            case_id  => $case->id,
            errors   => $case->deletion_errors,
            warnings => $case->deletion_warnings
        });
    }

    return $destroy_report;
}

=head2 destroy_cases

Given a resultset of cases, traverse and destroyed every one.
The checking should have been done, but re-check because:
a) somewhere somehow other code may have been changed
b) a different user request may have updated the database since the check

=cut

sub destroy_cases {
    my $self = shift;

    # the resultset typically has been traversed already for checking
    # so for convenience reset it here, otherwise magically nothing
    # will happen
    $self->reset;

    while (my $case = $self->next) {
        $case->set_deleted if $case->can_delete;
    }
}

sub get_stable_sort_key { return }

sub map_native_column {
    my ($self, $k) = @_;
    state %mapping = (
        'case.number' => 'me.id',
    );

    if (exists $mapping{$k}) {
        return $mapping{$k};
    }
    return;
}
1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, 2019 Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 ZAAKSYSTEEM_ZAAK_URGENTIE_PERC_HIGH

TODO: Fix the POD

=cut

=head2 ZAAKSYSTEEM_ZAAK_URGENTIE_PERC_LATE

TODO: Fix the POD

=cut

=head2 ZAAKSYSTEEM_ZAAK_URGENTIE_PERC_MEDIUM

TODO: Fix the POD

=cut

=head2 betrokkene_model

TODO: Fix the POD

=cut

=head2 config

TODO: Fix the POD

=cut

=head2 current_user

TODO: Fix the POD

=cut

=head2 export

TODO: Fix the POD

=cut

=head2 export_perfectview_tholen

TODO: Fix the POD

=cut

=head2 export_vergaderingen_ede

TODO: Fix the POD

=cut

=head2 gegevens_model

TODO: Fix the POD

=cut

=head2 get_auth_list

TODO: Fix the POD

=cut

=head2 hstore_column

TODO: Fix the POD

=cut

=head2 overlapt

TODO: Fix the POD

=cut

=head2 publish

TODO: Fix the POD

=cut

=head2 search

TODO: Fix the POD

=cut

=head2 search_extended

TODO: Fix the POD

=cut

=head2 search_grouped

TODO: Fix the POD

=cut

=head2 with_progress

TODO: Fix the POD

=cut

