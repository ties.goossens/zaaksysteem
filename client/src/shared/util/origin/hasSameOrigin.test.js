// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import { hasSameOrigin } from '.';

describe('The `hasSameOrigin` function', () => {
  test('returns `true` if the URL starts with an absolute path', () => {
    expect(hasSameOrigin('/api')).toBe(true);
  });

  test('returns `false` if the absolute path’s first segment is empty', () => {
    expect(hasSameOrigin('/')).toBe(false);
  });

  test('return `false` if the URL is absolute', () => {
    expect(hasSameOrigin('https://example.org/api')).toBe(false);
  });

  test('returns `false` if the URL is relative to the protocol', () => {
    expect(hasSameOrigin('//example.org/api')).toBe(false);
  });
});
