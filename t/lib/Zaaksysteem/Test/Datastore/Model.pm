package Zaaksysteem::Test::Datastore::Model;
use Moose;
extends 'Zaaksysteem::Test::Moose';

use Zaaksysteem::Test;
use BTTW::Tools::RandomData qw(generate_uuid_v4);
require List::Util;
require DateTime;
use URI;

use Zaaksysteem::Datastore::Model;

my %ok_resultsets = (
    Subject     => undef,
    NatuurlijkPersoon => undef,
    Bedrijf   => undef,
    ObjectSubscription   => undef,
    Adres   => undef,
);

sub _get_schema {
    my %args = @_;
    return mock_strict(
        resultset => sub {
            my $resultset = shift;
            if (List::Util::any { $resultset eq $_ } keys %ok_resultsets) {
                return mock_strict(search_rs => mock_strict())
                    unless defined $ok_resultsets{$resultset};
                return mock_strict();
            }
            else {
                die "Unsupported resultset '$resultset' for this model!"
            }
        },
        %args
    );
}

sub _get_model {
    my %args = (
        schema => _get_schema(),
        type   => 'NatuurlijkPersoon',
        @_
    );
    my @args = map { $_, $args{$_} } keys %args;
    return new_ok('Zaaksysteem::Datastore::Model' => \@args);
}

sub test_csv_building {

    my $model = _get_model();

    my $object = $model->_build_csv;
    isa_ok($object, 'Text::CSV_XS', "Got a CSV maker in our hands");
}


__PACKAGE__->meta->make_immutable;

__END__

=head1 NAME

Zaaksysteem::Test::Datastore::Model - Tests for the export model

=head1 DESCRIPTION

=head1 SYNOPSIS

    prove -lv :: Zaaksysteem::Test::Datastore::Model;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2020, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
