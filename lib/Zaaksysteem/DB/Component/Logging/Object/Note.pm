package Zaaksysteem::DB::Component::Logging::Object::Note;

use Moose::Role;
with qw(Zaaksysteem::Moose::Role::LoggingSubject);


=head1 NAME

Zaaksysteem::DB::Component::Logging::Object::Note - Event handler/mangler for
notes on objects.

=head1 METHODS

=head2 onderwerp

This method is hardwired to return the string C<Notitie toegevoegd>.

=cut

sub onderwerp {
    my $self = shift;

    return "Notitie toegevoegd";
}

=head2 event_category

This method is hardwired to return the string C<note>.

=cut

sub event_category { 'note' }

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 onderwerp

TODO: Fix the POD

=cut

