package Zaaksysteem::Controller::Schedule;

use Moose;

use NetAddr::IP;

use BTTW::Tools;
use JSON;
use DateTime;

BEGIN { extends 'Zaaksysteem::General::ZAPIController' }

=head1 NAME

Zaaksysteem::Controller::Schedule

=head1 SYNOPSIS

=head1 DESCRIPTION

Zaaksysteem controller for handling scheduled processes.

=head1 INSTRUCTIONS

=head1 METHODS

=head2 /schedule/run

Runs all jobs that are available on this domain/instance.

=cut

sub run
    : Chained('/')
    : PathPart('schedule/run')
    : ZAPI
    : DisableACL
{
    my ($self, $c) = @_;

    $c->assert_platform_access();

    $self->_execute_scheduled_run(
        "Transacties",
        sub {
            $c->model('DB::Transaction')->process_pending();
        },
    );

    $self->_execute_scheduled_run(
        "Onafgeronde zaken",
        sub {
            $self->remove_onafgeronde_zaken($c);
        },
    );

    $self->_execute_scheduled_run(
        "Opgeschorte zaken",
        sub {
            $c->model('Zaken')->resume_temporarily_stalled_cases($c->model('Queue'));
        },
    );

    $self->_execute_scheduled_run(
        "DefaultScheduledJobs",
        sub {
            $c->model('Scheduler')->install_default_jobs;
        }
    );

    # Ogone cases get picked up by the Scheduler right after, win!
    $self->_execute_scheduled_run(
        'Ogone case cleanup',
        sub {
            $self->deal_with_ogone_postponed_case_creations($c);
        },
    );

    $self->_execute_scheduled_run(
        "Scheduler",
        sub {
            $c->model('Scheduler')->run_pending();
        },
    );

    $self->_execute_scheduled_run(
        "Buitenbeter",
        sub {
            $self->check_buitenbeter_meldingen($c);
        },
    );

    $self->_execute_scheduled_run(
        "Incoming email",
        sub {
            my @interfaces = $c->model('DB::Interface')->search_active(
                { module => 'pop3client' }
            )->all;

            for my $interface (@interfaces) {
                my $model = $interface->model;

                $model->login();
                my $messages = $interface->model->messages();

                while (my $message = $messages->()) {
                    $c->stash->{message} = $message->retrieve()
                        or next;

                    my $success = $c->forward('/api/mail/intake');

                    $message->delete() if $success;
                }

                $model->close();
            }
        }
    );

    $self->_execute_scheduled_run(SessionInvitations => sub {
        $c->model('Session::Invitation')->cleanup_expired_invitations;
    });

    $self->_execute_scheduled_run('Cleanup locked files', sub {
        $c->model('DB::File')->cleanup_file_locks(DateTime->now);
    });

    $self->_execute_scheduled_run('Repush pending virus scans', sub {
        my $days = $ENV{ZS_VIRUSSCAN_DAYS} // 7;
        $c->model('DB::Filestore')->repush_pending_file_scan_queue_items({max_days => $days});
    });

    $self->_execute_scheduled_run('Cleanup old transactions', sub {
        $c->model('Transaction')->cleanup_old_transactions();
    });

    $self->_execute_scheduled_run(
        "Delete expired activation links",
        sub {
            $self->delete_expired_activation_links($c);
        },
    );

    $self->_execute_scheduled_run(
        "Update phase deadlines",
        sub {
            $c->model('Zaken')->update_phase_deadline_for_cases($c->model('Queue'));
        },
    );

    $c->stash->{zapi} = [{
        result => 'success',
    }];

}

=head2 remove_onafgeronde_zaken

Cleanup for the L<Zaaksysteem::Schema::ZaakOnafgerond> table.

=cut

sub remove_onafgeronde_zaken {
    my ($self, $c) = @_;

    my $time = DateTime->now->subtract(days => 30)->epoch;

    my $rs = $c->model('DB::ZaakOnafgerond')
        ->search({ create_unixtime => { '<=' => $time } });

    $rs->delete;
}


=head2 delete_expired_activation_links

Clean up old activation links

=cut

sub delete_expired_activation_links {
    my ($self, $c) = @_;

    my $now = $c->model('DB')->schema->format_datetime_object(DateTime->now);

    my $rs = $c->model('DB::AlternativeAuthenticationActivationLink')->search_rs({
        expires => { '<' => $now },
    });

    $rs->delete;

}

=head2 deal_with_ogone_postponed_case_creations

Clean up ye olde cases that are not going to be paid anymore.

=cut

sub deal_with_ogone_postponed_case_creations {
    my ($self, $c) = @_;

    my $cutoff = $c->model('DB')->schema->format_datetime_object(
        DateTime->now->subtract(minutes => 30)
    );

    my $rs = $c->model('DB::Queue')->search_rs(
        {
            status       => 'postponed',
            date_created => { '<' => $cutoff },
            type         => 'create_case_form',
        }
    );

    while (my $item = $rs->next) {
        $c->stash->{postponed} = $item;
        $c->forward('/plugins/ogone/cancel_by_zaaksysteem');
    }
}

=head2 check_buitenbeter_meldingen

Attempts to check if the C<buitenbeter> sysin interface has new messages
pending.

=cut

sub check_buitenbeter_meldingen {
    my ($self, $c) = @_;

    my $rs = $c->model('DB::Interface')->search_active(
        { module => 'buitenbeter' }
    );

    while (my $interface = $rs->next) {
        my $cases = $interface->process_trigger('GetNewMeldingen', {}, {
            zaak => $c->model('Zaak')
        });
    }
}

sub _execute_scheduled_run {
    my ($self, $job, $sub) = @_;

    try {
        $self->log->debug("Running job $job");
        $sub->();
    } catch {
        $self->log->error("Unable to process Scheduled run $job: $_");
    };

}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
