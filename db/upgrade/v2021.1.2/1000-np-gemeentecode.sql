BEGIN;

  ALTER TABLE natuurlijk_persoon ADD COLUMN gemeentecode smallint;
  ALTER TABLE natuurlijk_persoon ALTER COLUMN in_gemeente SET DEFAULT false;

  UPDATE natuurlijk_persoon SET gemeentecode = adres.gemeente_code
    FROM adres
    WHERE adres.gemeente_code IS NOT NULL
      AND adres.id = natuurlijk_persoon.adres_id
  ;

COMMIT;
