BEGIN;

    insert into queue (type, label, priority, data) select 'touch_case', 'devops sql queue item', 2000, '{"case_object_id":"' || object_id || '"}' from object_relation where object_type = 'case/result' and object_embedding NOT LIKE '%"result":"%"%';
    delete from object_relation where object_type = 'case/result' and object_embedding NOT LIKE '%"result":"%"%';

COMMIT;
