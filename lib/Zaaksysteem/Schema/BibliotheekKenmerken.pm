use utf8;
package Zaaksysteem::Schema::BibliotheekKenmerken;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Zaaksysteem::Schema::BibliotheekKenmerken

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<Zaaksysteem::Result>

=cut

use base 'Zaaksysteem::Result';

=head1 TABLE: C<bibliotheek_kenmerken>

=cut

__PACKAGE__->table("bibliotheek_kenmerken");

=head1 ACCESSORS

=head2 search_index

  data_type: 'tsvector'
  is_nullable: 1

=head2 search_term

  data_type: 'text'
  is_nullable: 1

=head2 object_type

  data_type: 'text'
  default_value: 'bibliotheek_kenmerken'
  is_nullable: 1

=head2 searchable_id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'searchable_searchable_id_seq'

=head2 search_order

  data_type: 'text'
  is_nullable: 1

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'bibliotheek_kenmerken_id_seq'

=head2 naam

  data_type: 'varchar'
  is_nullable: 1
  size: 256

=head2 value_type

  data_type: 'text'
  is_nullable: 1

=head2 value_default

  data_type: 'text'
  default_value: (empty string)
  is_nullable: 0

=head2 label

  data_type: 'text'
  is_nullable: 1

=head2 description

  data_type: 'text'
  is_nullable: 1

=head2 help

  data_type: 'text'
  default_value: (empty string)
  is_nullable: 0

=head2 magic_string

  data_type: 'text'
  is_nullable: 1

=head2 created

  data_type: 'timestamp'
  is_nullable: 1
  timezone: 'UTC'

=head2 last_modified

  data_type: 'timestamp'
  is_nullable: 1
  timezone: 'UTC'

=head2 bibliotheek_categorie_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 document_categorie

  data_type: 'text'
  is_nullable: 1

=head2 system

  data_type: 'integer'
  is_nullable: 1

=head2 deleted

  data_type: 'timestamp'
  is_nullable: 1
  timezone: 'UTC'

=head2 file_metadata_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 version

  data_type: 'integer'
  default_value: 1
  is_nullable: 0

=head2 properties

  data_type: 'text'
  default_value: '{}'
  is_nullable: 1

=head2 naam_public

  data_type: 'text'
  default_value: (empty string)
  is_nullable: 0

=head2 type_multiple

  data_type: 'boolean'
  default_value: false
  is_nullable: 0

=head2 uuid

  data_type: 'uuid'
  default_value: uuid_generate_v4()
  is_nullable: 0
  size: 16

=head2 relationship_type

  data_type: 'text'
  is_nullable: 1

=head2 relationship_name

  data_type: 'text'
  is_nullable: 1

=head2 relationship_uuid

  data_type: 'uuid'
  is_nullable: 1
  size: 16

=cut

__PACKAGE__->add_columns(
  "search_index",
  { data_type => "tsvector", is_nullable => 1 },
  "search_term",
  { data_type => "text", is_nullable => 1 },
  "object_type",
  {
    data_type     => "text",
    default_value => "bibliotheek_kenmerken",
    is_nullable   => 1,
  },
  "searchable_id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "searchable_searchable_id_seq",
  },
  "search_order",
  { data_type => "text", is_nullable => 1 },
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "bibliotheek_kenmerken_id_seq",
  },
  "naam",
  { data_type => "varchar", is_nullable => 1, size => 256 },
  "value_type",
  { data_type => "text", is_nullable => 1 },
  "value_default",
  { data_type => "text", default_value => "", is_nullable => 0 },
  "label",
  { data_type => "text", is_nullable => 1 },
  "description",
  { data_type => "text", is_nullable => 1 },
  "help",
  { data_type => "text", default_value => "", is_nullable => 0 },
  "magic_string",
  { data_type => "text", is_nullable => 1 },
  "created",
  { data_type => "timestamp", is_nullable => 1, timezone => "UTC" },
  "last_modified",
  { data_type => "timestamp", is_nullable => 1, timezone => "UTC" },
  "bibliotheek_categorie_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "document_categorie",
  { data_type => "text", is_nullable => 1 },
  "system",
  { data_type => "integer", is_nullable => 1 },
  "deleted",
  { data_type => "timestamp", is_nullable => 1, timezone => "UTC" },
  "file_metadata_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "version",
  { data_type => "integer", default_value => 1, is_nullable => 0 },
  "properties",
  { data_type => "text", default_value => "{}", is_nullable => 1 },
  "naam_public",
  { data_type => "text", default_value => "", is_nullable => 0 },
  "type_multiple",
  { data_type => "boolean", default_value => \"false", is_nullable => 0 },
  "uuid",
  {
    data_type => "uuid",
    default_value => \"uuid_generate_v4()",
    is_nullable => 0,
    size => 16,
  },
  "relationship_type",
  { data_type => "text", is_nullable => 1 },
  "relationship_name",
  { data_type => "text", is_nullable => 1 },
  "relationship_uuid",
  { data_type => "uuid", is_nullable => 1, size => 16 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 UNIQUE CONSTRAINTS

=head2 C<bibliotheek_kenmerken_uuid_idx>

=over 4

=item * L</uuid>

=back

=cut

__PACKAGE__->add_unique_constraint("bibliotheek_kenmerken_uuid_idx", ["uuid"]);

=head1 RELATIONS

=head2 bibliotheek_categorie_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::BibliotheekCategorie>

=cut

__PACKAGE__->belongs_to(
  "bibliotheek_categorie_id",
  "Zaaksysteem::Schema::BibliotheekCategorie",
  { id => "bibliotheek_categorie_id" },
);

=head2 bibliotheek_kenmerken_values

Type: has_many

Related object: L<Zaaksysteem::Schema::BibliotheekKenmerkenValues>

=cut

__PACKAGE__->has_many(
  "bibliotheek_kenmerken_values",
  "Zaaksysteem::Schema::BibliotheekKenmerkenValues",
  { "foreign.bibliotheek_kenmerken_id" => "self.id" },
  undef,
);

=head2 bibliotheek_notificatie_kenmerks

Type: has_many

Related object: L<Zaaksysteem::Schema::BibliotheekNotificatieKenmerk>

=cut

__PACKAGE__->has_many(
  "bibliotheek_notificatie_kenmerks",
  "Zaaksysteem::Schema::BibliotheekNotificatieKenmerk",
  { "foreign.bibliotheek_kenmerken_id" => "self.id" },
  undef,
);

=head2 custom_object_relationships

Type: has_many

Related object: L<Zaaksysteem::Schema::CustomObjectRelationship>

=cut

__PACKAGE__->has_many(
  "custom_object_relationships",
  "Zaaksysteem::Schema::CustomObjectRelationship",
  { "foreign.source_custom_field_type_id" => "self.id" },
  undef,
);

=head2 file_case_documents

Type: has_many

Related object: L<Zaaksysteem::Schema::FileCaseDocument>

=cut

__PACKAGE__->has_many(
  "file_case_documents",
  "Zaaksysteem::Schema::FileCaseDocument",
  { "foreign.bibliotheek_kenmerken_id" => "self.id" },
  undef,
);

=head2 file_metadata_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::FileMetadata>

=cut

__PACKAGE__->belongs_to(
  "file_metadata_id",
  "Zaaksysteem::Schema::FileMetadata",
  { id => "file_metadata_id" },
);

=head2 zaak_betrokkenens

Type: has_many

Related object: L<Zaaksysteem::Schema::ZaakBetrokkenen>

=cut

__PACKAGE__->has_many(
  "zaak_betrokkenens",
  "Zaaksysteem::Schema::ZaakBetrokkenen",
  { "foreign.bibliotheek_kenmerken_id" => "self.id" },
  undef,
);

=head2 zaak_kenmerks

Type: has_many

Related object: L<Zaaksysteem::Schema::ZaakKenmerk>

=cut

__PACKAGE__->has_many(
  "zaak_kenmerks",
  "Zaaksysteem::Schema::ZaakKenmerk",
  { "foreign.bibliotheek_kenmerken_id" => "self.id" },
  undef,
);

=head2 zaaktype_kenmerkens

Type: has_many

Related object: L<Zaaksysteem::Schema::ZaaktypeKenmerken>

=cut

__PACKAGE__->has_many(
  "zaaktype_kenmerkens",
  "Zaaksysteem::Schema::ZaaktypeKenmerken",
  { "foreign.bibliotheek_kenmerken_id" => "self.id" },
  undef,
);

=head2 zaaktype_sjablonens

Type: has_many

Related object: L<Zaaksysteem::Schema::ZaaktypeSjablonen>

=cut

__PACKAGE__->has_many(
  "zaaktype_sjablonens",
  "Zaaksysteem::Schema::ZaaktypeSjablonen",
  { "foreign.bibliotheek_kenmerken_id" => "self.id" },
  undef,
);


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2021-03-17 12:53:03
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:wTtG9fCBHt3lmn4Hy+yd1A

__PACKAGE__->resultset_class('Zaaksysteem::DB::ResultSet::BibliotheekKenmerken');

__PACKAGE__->load_components(
    "+Zaaksysteem::DB::Component::BibliotheekKenmerken",
    "+Zaaksysteem::Helper::ToJSON",
    __PACKAGE__->load_components()
);

__PACKAGE__->has_many(
  "bibliotheek_kenmerken_values",
  "Zaaksysteem::Schema::BibliotheekKenmerkenValues",
  { "foreign.bibliotheek_kenmerken_id" => "self.id" },
  {
    join_type => 'LEFT'
  },
);

use JSON::XS qw();

__PACKAGE__->inflate_column('properties', {
    inflate => sub { JSON::XS->new->decode(shift // '{}') },
    deflate => sub { JSON::XS->new->encode(shift // {}) },
});

# You can replace this text with custom content, and it will be preserved on regeneration
1;


__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

