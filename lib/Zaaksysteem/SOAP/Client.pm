package Zaaksysteem::SOAP::Client;
use Moose;

use Encode qw(encode_utf8);

=head1 NAME

Zaaksysteem::SOAP::Client - Basic no-frills SOAP client

=head1 SYNOPSIS

    my $client = Zaaksysteem::SOAP::Client->new(
        endpoint => 'https://foo.com/bar',
        ua => LWP::UserAgent->new(),
    );

    my $rv = $client->call('https://foo.com/SOAPAction', '<?xml version="1.0"?>...etc');

    # $rv is a has reference with 2 keys: "request" and "response", containing
    # the HTTP request and response for the SOAP call.

=head1 ATTRIBUTES

=head2 endpoint

SOAP endpoint to send the call to.

=head2 ua

L<LWP::UserAgent> instance to use for doing the HTTP(s) request.

=head2 spoof_hook

L<LWP::UserAgent> instance to use for doing the HTTP(s) request.

=cut

has endpoint => (
    is       => 'ro',
    isa      => 'Str',
    required => 1,
);

has ua => (
    is       => 'ro',
    isa      => 'LWP::UserAgent',
    required => 1,
);

has spoof_response => (
    is       => 'ro',
);

=head1 METHODS

=head2 call

Perform a SOAP call to the configured endpoint. Only document-literal SOAP
without headers is currently supported.

Accepts 2 positional parameters:

=over

=item 1. The value to use for the SOAPAction header (can be an empty string)

=item 2. The XML to send inside the SOAP Body

=back

=cut

sub call {
    my $self = shift;
    my ($action, $xml) = @_;

    my $SOAP_ENVELOPE = q{<?xml version="1.0"?>
<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/">
<SOAP-ENV:Body>%s</SOAP-ENV:Body>
</SOAP-ENV:Envelope>};

    my $req = HTTP::Request->new(
        POST => $self->endpoint,
        HTTP::Headers->new(
            Content_Type => 'text/xml; charset=UTF-8',
            SOAPAction   => $action,
        ),
        sprintf($SOAP_ENVELOPE, encode_utf8($xml)),
    );

    my $res;
    if ($self->spoof_response) {
        if (ref $self->spoof_response) {
            $res = $self->spoof_response->($req);    
        } else {
            $res = HTTP::Response->new(
                200,
                'answer manually created',
                [ 'Content-Type' => 'text/xml' ],
                $self->spoof_response
            );
        }       
    } else {
        $res = $self->ua->request($req);
    }

    return {
        request  => $req,
        response => $res,
    };
}

__PACKAGE__->meta->make_immutable();

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
