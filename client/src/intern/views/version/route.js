// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import get from 'lodash/get';
import assign from 'lodash/assign';
import angularUiRouterModule from 'angular-ui-router';
import zsModalModule from '../../../shared/ui/zsModal';
import auxiliaryRouteModule from '../../../shared/util/route/auxiliaryRoute';
import template from './index.html';
import resourceModule from '../../../shared/api/resource';
import seamlessImmutable from 'seamless-immutable';
import snackbarServiceModule from '../../../shared/ui/zsSnackbar/snackbarService';
import './styles.scss';

// support center will make sure that the articles will always
// begin with a classless paragraph which contains no html tags
const regexFirstParagraph = /<p>(.*?)<\/p>/;
const getFeatures = ({ results }) =>
  results.map((result) =>
    assign(
      {
        description: result.body.match(regexFirstParagraph)[1],
      },
      result
    )
  );

export default angular
  .module('Zaaksysteem.intern.version.route', [
    angularUiRouterModule,
    zsModalModule,
    auxiliaryRouteModule,
    resourceModule,
    snackbarServiceModule,
  ])
  .config([
    '$stateProvider',
    '$urlMatcherFactoryProvider',
    ($stateProvider, $urlMatcherFactoryProvider) => {
      $urlMatcherFactoryProvider.strictMode(false);

      const path = '/api/v2/help_center/articles/search.json';

      $stateProvider.state('version', {
        url: '/version',
        auxiliary: true,
        controllerAs: 'vm',
        resolve: {
          userFeaturesResource: [
            '$rootScope',
            'resource',
            '$q',
            'snackbarService',
            'user',
            ($rootScope, resource, $q, snackbarService, user) => {
              const currentBundleVersion = get(
                user.data(),
                'instance.current_bundle_version'
              );
              let userFeaturesResource = resource(
                {
                  url: `https://zaaksysteem.zendesk.com${path}`,
                  params: {
                    label_names: currentBundleVersion,
                  },
                  headers: {
                    'X-Client-Type': null,
                  },
                  withCredentials: false,
                },
                { scope: $rootScope }
              ).reduce((requestOptions, data) => data || seamlessImmutable([]));

              return userFeaturesResource
                .asPromise()
                .then(() => userFeaturesResource)
                .catch((err) => {
                  snackbarService.error(
                    'De versieinformatie kon niet worden geladen.'
                  );

                  return $q.reject(err);
                });
            },
          ],
          adminFeaturesResource: [
            '$rootScope',
            'resource',
            '$q',
            'snackbarService',
            'user',
            ($rootScope, resource, $q, snackbarService, user) => {
              const currentBundleVersion = user.data().instance
                .current_bundle_version;
              // The resource provider can handle multiple unique requests in parallel. Non-unique requests are 'merged'.
              // Uniqueness is determined by the path+params, therefore the userFeatures and adminFeatures are non-unique.
              // To prevent them from returning the same response, I've added an extra param to make them unique.
              // Very clever
              let adminFeaturesResource = resource(
                {
                  url: `https://zaaksysteembeheer.zendesk.com${path}`,
                  params: {
                    label_names: currentBundleVersion,
                    hack: 'bad',
                  },
                  headers: {
                    'X-Client-Type': null,
                  },
                  withCredentials: false,
                },
                { scope: $rootScope }
              ).reduce((requestOptions, data) => data || seamlessImmutable([]));

              return adminFeaturesResource
                .asPromise()
                .then(() => adminFeaturesResource)
                .catch((err) => {
                  snackbarService.error(
                    'De versieinformatie kon niet worden geladen.'
                  );

                  return $q.reject(err);
                });
            },
          ],
        },
        onActivate: [
          '$http',
          '$state',
          '$timeout',
          '$rootScope',
          '$window',
          '$document',
          '$compile',
          'composedReducer',
          'zsModal',
          'user',
          'userFeaturesResource',
          'adminFeaturesResource',
          function (
            $http,
            $state,
            $timeout,
            $rootScope,
            $window,
            $document,
            $compile,
            composedReducer,
            zsModal,
            user,
            userFeaturesResource,
            adminFeaturesResource
          ) {
            let openModal = () => {
              let modal,
                unregister,
                scope = $rootScope.$new(true);

              const currentBundleVersion = get(
                user.data(),
                'instance.current_bundle_version'
              );
              const lastSeenVersion = get(
                user.data(),
                'instance.logged_in_user.last_seen_version'
              );

              const userFeatureReducer = composedReducer(
                { scope: $rootScope },
                userFeaturesResource
              ).reduce((userFeatures) => getFeatures(userFeatures));

              const adminFeatureReducer = composedReducer(
                { scope: $rootScope },
                adminFeaturesResource
              ).reduce((adminFeatures) => getFeatures(adminFeatures));

              scope.getUserFeatures = () => userFeatureReducer.data();

              scope.getAdminFeatures = () => adminFeatureReducer.data();

              modal = zsModal({
                el: $compile(angular.element(template))(scope),
                title: `Nieuw in deze versie (${currentBundleVersion})`,
                classes: 'version-modal center-modal',
              });

              modal.onClose(() => {
                if (currentBundleVersion !== lastSeenVersion) {
                  $http({
                    url: '/api/user/sync_version',
                    method: 'POST',
                  });
                }

                $state.go('^');
                return true;
              });

              modal.open();

              unregister = $rootScope.$on('$stateChangeStart', () => {
                $window.requestAnimationFrame(() => {
                  $rootScope.$evalAsync(() => {
                    modal.close().then(() => {
                      scope.$destroy();
                    });

                    unregister();
                  });
                });
              });
            };

            $window.requestAnimationFrame(() => {
              $rootScope.$evalAsync(openModal);
            });
          },
        ],
      });
    },
  ]).name;
