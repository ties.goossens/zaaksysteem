package TestFor::General::Backend::Sysin::Modules::Map;
use base qw(ZSTest);
use TestSetup;

use Zaaksysteem::Backend::Sysin::Modules::Map;

=head1 NAME

TestFor::General::Backend::Sysin::Modules::Map - Module Map tests

=head1 SYNOPSIS

    ./zs_prove -v t/lib/TestFor/General/Backend/Sysin/Modules/Map.pm

=head1 LAYOUT

This file implements all USAGE tests, in other words: if you would like
to use the engine, here is your inspiration.

=head1 USAGE (or end-to-end)

Usage tests: use the below tests when you would like to give this module
a spin.

=head1 IMPLEMENTATION

=head2 module_map_get_wms_layers

Test the get_wms_layers function

=cut

sub module_map_get_wms_layers : Tests {
    $zs->txn_ok(
        sub {
            my $interface   = $zs->create_map_interface_ok();

            my $settings    = $interface->process_trigger('get_ol_settings');

            ### Check center attribute
            can_ok($settings, qw/wms_layers map_center/);

            ### Check layers
            {
                my $layers      = $settings->wms_layers;

                isa_ok($layers, 'ARRAY', 'Got an array of layers');

                for my $layer (@$layers) {
                    isa_ok($layer, 'Zaaksysteem::Backend::Sysin::Modules::Map::OL::LayerWMS', 'ISA for every row: ');
                }

                ### Check first entry

                my $layer = $layers->[0];
                can_ok($layer, qw/id label url active uri/);

                is($layer->url, 'https://geodata.nationaalgeoregister.nl/ahn1/wms?&request=GetCapabilities', 'Got a correct value for "url"');
                is($layer->label, 'Hoogtebestand Nederland', 'Got a correct value for "label"');
                is($layer->active, 1, 'Got a correct value for "active"');
            }
        },
        'get_ol_settings tests'
    );

    $zs->txn_ok(
        sub {

            ### Create invlid layer
            my $interface   = $zs->create_named_interface_ok(
                {
                    name             => 'Map configuration',
                    module           => 'map',
                    interface_config => {
                        wms_layers      => [
                            {
                                label   => 'Hoogtebestand Nederland',
                                url     => 'https://geodata.nationaalgeoregister.nl/ahn1/wms?&request=GetCapabilities',
                                active  => 1,
                            },
                            {
                                id      => 'adrnl',
                                label   => 'Adressen Nederland',
                                url     => 'https://geodata.nationaalgeoregister.nl/inspireadressen/wms?&request=GetCapabilities',
                                active  => 0,
                            }
                        ],
                    },
                }
            );

            my $settings    = $interface->process_trigger('get_ol_settings');

            ### Check center attribute
            can_ok($settings, qw/wms_layers map_center/);

            ### Check layers
            {
                my $layers      = $settings->wms_layers;

                is(scalar @$layers, 1, 'Got one invalid layer, but other still exists');
            }
        },
        'get_ol_settings: invalid layer'
    );
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
