package Zaaksysteem::API::v1::Serializer::ValidationExceptionRole;

use Moose::Role;

use BTTW::Tools;

=head1 NAME

Zaaksysteem::API::v1::Serializer::ValidationExceptionRole - Role which extends the exception handler
for readable validation exceptions

=head1 SYNOPSIS

    with 'Zaaksysteem::API::v1::Serializer::ValidationExceptionRole';

=head1 DESCRIPTION

This role checks whether an exception is thrown due to a validation error, and returns a structured exception
based on the validation problems.

=head1 METHOD MODIFIERS

=head2 read_exception [around]

Turns a normal exception into a validationexception

=cut

around 'read_exception' => sub {
    my $method      = shift;
    my $self        = shift;
    my ($serializer, $exception) = @_;

    my $response    = $self->$method(@_);

    if (
        $exception->type eq 'params/profile' &&
        blessed($exception->object) &&
        $exception->object->isa('Data::FormValidator::Results')
    ) {
        $response->{type}       = 'validationexception';
        $response->{instance}   = {};

        my $exception_messages  = $exception->object->msgs;

        for my $validity (qw/missing invalid valid/) {
            for my $param ($exception->object->$validity) {
                my $message     = ref $exception_messages eq 'HASH' ? $exception_messages->{$param} : undef;

                $response->{instance}->{ $param } = {
                    validity => $validity,
                    message  => $self->validation_exception_message($param, $validity, $message)
                }
            }
        }
    }

    return $response;
};


=head2 validation_exception_message

TODO: Fix me

=cut

sub validation_exception_message {
    my $self        = shift;
    my $key         = shift;
    my $validity    = shift;
    my $message     = shift;

    unless ($message) {
        return "validity [$key] $validity"
    };

    return sprintf($message, $key);
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
