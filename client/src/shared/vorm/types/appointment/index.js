// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import vormTemplateServiceModule from './../../vormTemplateService';
import resourceModule from './../../../api/resource';
import composedReducerModule from './../../../api/resource/composedReducer';
import snackbarServiceModule from './../../../ui/zsSnackbar/snackbarService';
import zsSpinnerModule from './../../../ui/zsSpinner';
import appointmentControlDirective from './appointmentControlDirective';
import appointmentDisplayComponent from './appointmentDisplayComponent';

import './styles.scss';

export default angular
  .module('vorm.types.appointment', [
    vormTemplateServiceModule,
    resourceModule,
    composedReducerModule,
    snackbarServiceModule,
    zsSpinnerModule,
  ])
  .directive('vormAppointmentControl', appointmentControlDirective)
  .component('vormAppointmentDisplay', appointmentDisplayComponent)
  .run([
    '$http',
    '$q',
    'vormTemplateService',
    ($http, $q, vormTemplateService) => {
      vormTemplateService.registerType('appointment', {
        control: angular.element(
          `<vorm-appointment-control
						ng-model
						data-provider="vm.invokeData('provider')"
					></vorm-appointment-control>`
        ),
        display: angular.element(
          `<vorm-appointment-display
						data-value="delegate.value"
						data-provider="vm.invokeData('provider')"
						on-remove="vm.clearDelegate(delegate)"
					></vorm-appointment-display>`
        ),
        defaults: {
          editMode: 'empty',
          disableClear: true,
        },
      });
    },
  ]).name;
