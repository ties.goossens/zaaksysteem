package Zaaksysteem::Backend::Contactmoment::ResultSet;
use Moose;

use Params::Profile;
use Zaaksysteem::Constants;

extends 'DBIx::Class::ResultSet';

use Exception::Class (
    'Zaaksysteem::Backend::Contactmoment::ResultSet::Exception' => {fields => 'code'},
    'Zaaksysteem::Backend::Contactmoment::ResultSet::Exception::Parameter' => {
        isa         => 'Zaaksysteem::Backend::Contactmoment::ResultSet::Exception',
        description => 'Parameter exception',
        alias       => 'throw_parameter_exception',
    },
);

=head2 contactmoment_create

TBA

=cut

Params::Profile->register_profile(
    method  => 'contactmoment_create',
    profile => {
        required => [qw/
            type
            created_by
            medium
        /],
        optional => [qw/
            subject_id
            message
            case_id
            created_for
            subject
        /],
        constraint_methods => {
            type => qr/(email|note)/,
        },
        require_some => {
            subject_or_case => [1, qw/subject_id case_id/],
        },
    }
);

sub contactmoment_create {
    my ($self, $opts) = @_;

    my $dv = Params::Profile->check(
        params  => $opts,
    );
    if ($dv->has_invalid) {
        my @invalid = join ',',$dv->invalid;
        throw_parameter_exception(
            code  => '/contactmoment/contactmoment_create/invalid_parameters',
            error => "Invalid options given: @invalid"
        );
    }
    if ($dv->has_missing) {
        my @missing = join ',',$dv->missing;
        throw_parameter_exception(
            code  => '/contactmoment/contactmoment_create/missing_parameters',
            error => "Missing options: @missing"
        );
    }

    my $zaak = $opts->{case_id} ? $self->result_source->schema->resultset('Zaak')->find($opts->{case_id}) : undef;
    my $subject = $opts->{subject};

    my $cm = $self->create({
        subject_id => $opts->{subject_id} || undef,
        case_id    => $opts->{case_id} || undef,
        created_by => $opts->{created_by},
        medium     => $opts->{medium},
        type       => $opts->{type},
    });

    my $result;
    if ($opts->{type} eq 'email') {

        $cm->create_email($opts->{email});

        my %logdata = (
            'email/send' => {
                component => 'email',
                component_id  => $cm->id,
                zaak_id       => $opts->{case_id} || undef,
                betrokkene_id => $opts->{subject_id} || undef,
                created_by    => $opts->{created_by},
                created_for   => $opts->{created_for} || undef,
                data => {
                    case_id             => $opts->{case_id} || undef,
                    contactmoment_id    => $cm->id,
                    from                => $opts->{email}{from},
                    recipient           => $opts->{email}{recipient},
                    cc                  => $opts->{email}{cc},
                    bcc                 => $opts->{email}{bcc},
                    attachments         => $opts->{email}{attachments} || undef,
                    content             => $opts->{email}{body},
                    subject             => $opts->{email}{subject},
                }
            }
        );

        if ($zaak) {
            my $message;
            if (@{ $opts->{email}{attachments} }) {
                $message = "Contactmoment e-mail met bijlage toegevoegd";
            }
            else {
                $message = "Contactmoment e-mail toegevoegd";
            }

            my $log = $zaak->trigger_logging(%logdata);

            $zaak->create_message_for_behandelaar(
                event_type => 'contactmoment/email',
                log        => $log,
                message    => $message,
                subject    => $subject,
            );
        }
        else {
            $self->result_source->schema->resultset('Logging')->trigger(%logdata);
        };
    }
    elsif ($opts->{type} eq 'note') {
        $cm->create_note({message => $opts->{message}});
        if ($zaak) {
            $zaak->create_message_for_behandelaar(
                event_type => 'contactmoment/note',
                message    => "Contactmoment notitie toegevoegd",
                subject    => $subject,
            );
        }
    }
    elsif ($zaak) {
        $zaak->create_message_for_behandelaar(
            event_type => 'contactmoment/overig',
            message    => "Contactmoment toegevoegd",
            subject    => $subject,
        );
    }
    return $cm->discard_changes;
}

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

