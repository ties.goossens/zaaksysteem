// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import angularUiRouter from 'angular-ui-router';
import template from './template.html';
import zsFileUploadModule from './../../../../shared/ui/zsFileUpload';
import snackbarServiceModule from './../../../../shared/ui/zsSnackbar/snackbarService';

export default angular
  .module('zsCaseFileUpload', [
    zsFileUploadModule,
    angularUiRouter,
    snackbarServiceModule,
  ])
  .directive('zsCaseFileUpload', [
    '$state',
    '$q',
    'snackbarService',
    ($state, $q, snackbarService) => {
      return {
        restrict: 'E',
        template,
        scope: {
          caseUuid: '&',
          close: '&',
        },
        bindToController: true,
        controller: [
          function () {
            let ctrl = this;

            ctrl.getTarget = () => '/api/v2/document/create_document';

            ctrl.getData = ($file) => {
              return {
                case_uuid: ctrl.caseUuid(),
                document_file: $file,
              };
            };

            ctrl.handleBatchStart = (uploads) => {
              let promise = $q.all(uploads.map((upload) => upload.promise)),
                isDocs = $state.current.name.indexOf('case.docs') === 0,
                actions;

              if (isDocs) {
                actions = [];
              } else {
                actions = [
                  {
                    type: 'link',
                    link: $state.href('case.docs', null, { inherit: true }),
                    label: 'Documenten bekijken',
                  },
                ];
              }

              snackbarService
                .wait('Bezig met uploaden', {
                  promise,
                  then: () => {
                    return {
                      message: 'Uw bestanden zijn toegevoegd',
                      actions,
                    };
                  },
                  catch: () =>
                    'Er ging iets mis bij het uploaden van de bestanden. Neem contact op met uw beheerder voor meer informatie.',
                })
                .then(() => {
                  if (isDocs) {
                    return $state.reload();
                  }
                });

              ctrl.close({ $promise: promise });
            };
          },
        ],
        controllerAs: 'vm',
      };
    },
  ]).name;
