package Zaaksysteem::DB::Component::Logging::Case::Document::Metadata::Update;

use Moose::Role;
with qw(
    Zaaksysteem::Moose::Role::LoggingSubject
    MooseX::Log::Log4perl
);

use BTTW::Tools;

use DateTime::Format::ISO8601;
use JSON;

=head1 METHODS

=head2 onderwerp

Builds a "subject" string from te log entry for displaying to the user.

=cut

sub onderwerp {
    my $self = shift;

    my $file;

    if ($self->data->{file_id}) {
        $file = $self->result_source->schema->resultset('File')->find($self->data->{file_id});
    }

    if ($file) {
        return sprintf(
            "Document-metadata aangepast voor '%s' versie %d (ID %d)",
            $file->filename,
            $file->version,
            $self->data->{file_id},
        );
    }
    else {
        return sprintf(
            "Document-metadata aangepast voor onbekend bestand met ID '%d'",
            $self->data->{file_id},
        );
    }
}

my %key_name_map = (
    description       => "Omschrijving",
    trust_level       => "Vertrouwelijkheid",
    document_category => "Documentcategorie",
    origin            => "Richting",
    origin_date       => "Ontvangst/verzenddatum",
    pronom_format     => 'PRONOM formaat',
    appearance        => 'Verschijningsvorm',
    structure         => 'Structuur',
    creation_date     => 'Aanmaakdatum',
);

around TO_JSON => sub {
    my $orig = shift;
    my $self = shift;

    my $data = $self->$orig(@_);

    # Backwards compatibility
    if (not exists $self->data->{metadata}) {
        return $data;
    }

    my @changes = map {
        if (($_ eq 'origin_date' || $_ eq 'creation_date') && defined $self->data->{metadata}{$_}) {
            # If there's a log entry with an invalid date, parse_datetime would die.
            # That would make it impossible to view log entries on the frontend,
            # so prevent that.
            my $date = eval {
                DateTime::Format::ISO8601->parse_datetime($self->data->{metadata}{$_})->set_time_zone('Europe/Amsterdam')->dmy;
            };
            if (!$date) {
                $date = "<onbekend>";
            }

            ($key_name_map{$_} => $date);
        }
        else {
            ($key_name_map{$_} => $self->data->{metadata}{$_})
        }
    } sort keys %{ $self->data->{metadata} };

    my @content;

    while (@changes) {
        my $key=  shift @changes;
        my $value = shift @changes;

        $key   //= '<onbekende key>';
        $value //= '<geen waarde>';

        push @content, sprintf("%s ingesteld op \"%s\"", $key, $value);
    }

    $data->{ content } =
        join("\n", @content);

    $data->{ expanded } = JSON::false;

    return $data;
};

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
