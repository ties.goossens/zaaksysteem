[%
# Display a 'zaak veld' based on a few parameters
# This template expects output from the DB::Zaak finder
#
# input:
# - display_field.class: a string describing the type of formatting that is needed for this data item
# - display_field.fieldname: a string for fieldname
# - zaak: the complete zaak object. this template can pick and choose from this 
#
%]

[% USE Scalar %]
[% USE date %]

[% BLOCK status_display %]
    [% zaak_status_icon = 'new';
        IF (zaakdata.status == 'open'); zaak_status_icon = 'open';
        END;

        IF (zaakdata.status == 'resolved' && zaakdata.te_vernietigen);
            zaak_status_icon = 'deleted';
        ELSIF (zaakdata.status == 'resolved');
            IF (zaakdata.archival_state == 'overdragen');
                zaak_status_icon = 'overdragen';
            ELSE;
                zaak_status_icon = 'resolved';
            END;
        END;

        IF (zaakdata.status == 'stalled');
            zaak_status_icon = 'stalled';
        END;
    %]

    <a href="[% c.uri_for(( pip ? '/pip' : '') _ '/zaak/' _ zaakdata.nr) %][% IF case_intake_automatic_open %]/open[% END %]" class="mdi icon-status icon-status-[% zaak_status_icon %]"></a>
[% END %]

[% BLOCK zaaknummer_display %]

    <a href="[% c.uri_for(( pip ? '/pip' : '')_ '/zaak/' _ zaakdata.nr) %][% IF case_intake_automatic_open %]/open[% END %]">
        <span class="hide">Zaak</span> [% zaakdata.nr %]
    </a>

[% END %]

[% BLOCK voortgang_display %]
    [%
        total_statussen = zaakdata.scalar.zaaktype_node_id.scalar.zaaktype_statussen.count;
        IF total_statussen;
            status_perc = ((zaakdata.milestone / total_statussen) * 100);
        END;

        status_perc = status_perc | format('%.0f');

        IF status_perc > 100;
            status_perc=100;
        END;
    %]

    <div class="progress">
        <div class="progress-bar holder rounded">
            <div class="progress-value value rounded">
                <div class="perc">[% status_perc | format('%.0f') %]</div>
            </div>
        </div>
    </div>
[% END %]

[% BLOCK zaaktype_display %]
    [% IF pip %]
    <div class="pip-overview-case-title">
        <a href="[% c.uri_for(( pip ? '/pip' : '')_ '/zaak/' _ zaakdata.nr) %][% IF case_intake_automatic_open %]/open[% END %]">
        [% zaakdata.nr %]:
    [% END %]
            
    [% IF show_zaaktype_popup %]
        <a href="[% (pip ? '/pip' : '') %]/zaak/[% zaakdata.nr %]/zaaktypeinfo" class="ezra_dialog" title="Informatie over dit zaaktype">[% zaakdata.zaaktype_node_id.titel | html_entity %]</a>
    [% ELSE %]
        [% zaakdata.zaaktype_node_id.titel | html_entity %]
    [% END %]
    [% IF pip %]
        </a>
    </div>
    [% END %]
[% END %]

[% BLOCK subject_display %]
    
    [% IF pip %]
    Registratiedatum: [% zaakdata.registratiedatum.strftime('%d-%m-%Y') %]
    [% ELSE %]
    [% zaakdata.onderwerp | html_entity | html_line_break %]
    [% END %]
[% END %]

[% BLOCK external_subject_display %]
    [% zaakdata.onderwerp_extern | html_entity | html_line_break %]
[% END %]

[% BLOCK route_ou_display %]
    [% UNLESS pip %]
        [% zaakdata.ou_object.omschrijving | html_entity %]
    [% END %]
[% END %]

[% BLOCK aanvrager_display %]
    [% UNLESS pip %]
       [% zaakdata.aanvrager.naam | html_entity %]
	[% END %]
[% END %]

[% BLOCK besluit_display %]
    besluit TODO
[% END %]

[% BLOCK remainingtime_display %]
    [% IF pip %]
        <div class="pip-case-progress">
            
            <div class="pip-case-progress-message">
            [% IF zaakdata.is_afgehandeld %]
                Resultaat: [% zaak.resultaat %]
            [% ELSIF zaakdata.status == 'stalled' %]
                [% zaakdata.systeemkenmerk('streefafhandeldatum') %]
            [% ELSIF zaakdata.days_left < 0 %]
                [% zaakdata.days_left.replace('-','') %] dag[% (zaakdata.days_left.replace('-', '') != 1 ? 'en' : '') %] te laat
            [% ELSE %]
                Nog [% zaakdata.days_left %] dag[% (zaakdata.days_left != 1 ? 'en' : '') %]
            [% END %]
            </div>
            <div class="pip-case-progress-bar[% ((!zaakdata.is_afgehandeld && zaakdata.days_perc > 100) ? ' pip-case-progress-late' : '') %][% IF zaakdata.status == 'stalled' %] pip-case-progress-stalled[% END %][% ((zaakdata.is_afgehandeld) ? ' pip-case-progress-resolved' : '') %]">
                <div class="pip-case-progress-percentages" style="width:[% ((zaakdata.is_afgehandeld || zaakdata.days_perc > 100) ? '100' : zaakdata.days_perc) %]%"></div>
            </div>

        </div>
    [% ELSIF !zaakdata.is_afgehandeld %]
        [% PROCESS widgets/general/remaining_time.tt
            percentage   = zaakdata.days_perc
            days         = zaakdata.days_left
            IS_STALLED   = (zaakdata.status == 'stalled')
        %]
    [% END %]
[% END %]




[% BLOCK actie_display %]
    [% UNLESS pip %]
        [% PROCESS widgets/general/zaak_veld/action.tt %]
    [% END %]
[% END %]

[% BLOCK attachments_display %]
   [% IF zaakdata.bericht.is_alert %]

   <!-- TODO LET OP: Dit is tijdelijk, attachment icon -->
   <a href="[% c.uri_for('/zaak/' _ zaakdata.nr _ '#zaak-elements-documents') %]">
		<img
            src="images/icon_attachment.png"
            width="16"
            height="16"
            alt="Er staan documenten in de wachtrij voor deze zaak"
            border="0"
        />
	</a>

	[% END %]
[% END %]


[% BLOCK kenmerk_display %]
    [% kenmerken    = zaakdata.scalar.zaak_kenmerken.scalar.search(bibliotheek_kenmerken_id => display_field.replace('\D', '')) %]
    [% ztkenmerk    = zaakdata.scalar.zaaktype_node_id.scalar.zaaktype_kenmerken.search(
        {
            'bibliotheek_kenmerken_id.id' => display_field.replace('\D', '')
        },
        {
            prefetch => 'bibliotheek_kenmerken_id',
        }
    ).first %]

    [% IF ztkenmerk.bibliotheek_kenmerken_id.value_type == 'file' %]
        [% kenmerkdocs = zaakdata.scalar.files.scalar.search_by_case_document_id(
                ztkenmerk.id
           )
        %]
        [% ### MULTIPLE DOCS READY: %]
        [% WHILE (kenmerkdoc = kenmerkdocs.next) %]
            <a href="[% c.uri_for('/zaak/' _ zaak.id _ '/document/' _ kenmerkdoc.id _ '/download') %]">
                [% kenmerkdoc.filename | html_entity %]
            </a>
            <br />
        [% END %]
    [% ELSIF kenmerken.count %]
        [% values = [] %]
        [% WHILE (kenmerk = kenmerken.next) %]
            [% kenmerk_type = kenmerk.bibliotheek_kenmerken_id.value_type %]
            [% values.push(kenmerk.value) %]
        [% END %]

        [% PROCESS widgets/general/veldoptie_view.tt 
            veldoptie_type = kenmerk_type
            veldoptie_value=values.join(', ') 
        %]
    [% END %]
[% END %]




[% # Default display %]
[% BLOCK text_display %]
    [% zaakdata.systeemkenmerk(display_field) | html_entity %]
[% END %]


[% BLOCK level_display %]
    [% IF !level %]
        Error: Field 'level' not found.
        [% RETURN %]
    [% END %]
    [% level %]
[% END %]

[% BLOCK info_display %]
    <div class="row-actions">
        <a href="[% c.uri_for('/zaak/get_meta/' _ zaakdata.nr) %]" class="ezra_dialog row-action" title="Informatie over zaak">
            <i class="mdi mdi-information-outline"></i>
        </a>
    </div>
[% END %]

[% BLOCK behandelaar_display %]
    <a href="[% c.uri_for('/betrokkene/get/' _ zaakdata.behandelaar_object.betrokkene_identifier) %]" class="ezra_dialog" title="Informatie over behandelaar">[% zaakdata.behandelaar_object.naam %]</a>
[% END %]

[% BLOCK behandelaar_handtekening_display %]
    [% # empty on purpose %]
[% END %]

[% BLOCK besluit_display %]
    [% IF zaakdata.besluit %]
        [% zaakdata.besluit %]
    [% ELSE %]
        Onbekend
    [% END %]
[% END %]

[% BLOCK resultaat_display %]
    [% IF zaakdata.resultaat %]
        [% zaakdata.resultaat | ucfirst %]
    [% ELSE %]
        Onbekend
    [% END %]
[% END %]

[% BLOCK relaties_display %]
    [% zaakdata.systeemkenmerk('alle_relaties').join(', ') %]
[% END %]

[% BLOCK confidentiality_display %]
    [% zaakdata.systeemkenmerk('vertrouwelijkheid') %]
[% END %]

[% IF (display_class && display_field && zaak) %]
    [% 
    #
    # This is the code choosing a display block 
    #
    %]

    [% display_block = display_class _ '_display' %]
    [% INCLUDE $display_block %]
[% ELSE %]
    <pre>
    Missing info: 
    - display class: [% display_class %]
    - display field: [% display_field %]
    - zaak object:   [% zaak %]
    </pre>
[% END %]
