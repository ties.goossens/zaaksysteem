package Zaaksysteem::Search::Term::Set;
use Moose;

extends 'Zaaksysteem::Search::Term';

=head1 NAME

Zaaksysteem::Search::Term::Literal - "Literal" term of a conditional

=head1 ATTRIBUTES

=head2 value

The value of this term.

=head1 METHODS

=head2 evaluate

Evaluate the term. Returns an array:

    ( "( ?, ?, etc.) ", [{} => $member1_val], [{} => $self->member2_val] )

=cut

has values => (
    is  => 'ro',
    isa => 'ArrayRef[Zaaksysteem::Search::Term::Literal]',
);

override 'evaluate' => sub {
    my $self = shift;
    my ($resultset, $conditional) = @_;

    my @args;
    my @sql_parts;

    for my $item (@{ $self->values }) {
        my @subargs = $item->evaluate($resultset, $conditional);

        push @sql_parts, shift @subargs;
        push @args, @subargs;
    }

    return (sprintf('( %s )', join(', ', @sql_parts)), @args);
};

=head2 guess_type

Guess the type of the values in the set.

If all values have the same type, returns that type, otherwise default to
'TEXT'.

=cut

sub guess_type {
    my $self = shift;

    my %value_types;
    for my $value (@{ $self->values }) {
        $value_types{ $value->guess_type() } = 1;
    }

    if (keys %value_types == 1) {
        return (keys %value_types)[0];
    }

    # If the set has mixed types, all bets are off. Default to 'TEXT'.
    return 'TEXT';
}

__PACKAGE__->meta->make_immutable();



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

