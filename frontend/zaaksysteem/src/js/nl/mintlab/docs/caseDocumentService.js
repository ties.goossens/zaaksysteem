// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular.module('Zaaksysteem.docs').factory('caseDocumentService', [
    '$http',
    '$q',
    function ($http, $q) {
      var caseDocumentService = {},
        deferredByCaseId = {},
        caseDocsById = {};

      caseDocumentService.getCaseDocumentsByCaseId = function (caseId) {
        var deferred = deferredByCaseId[caseId];

        if (deferred) {
          return deferred.promise;
        }

        deferred = deferredByCaseId[caseId] = $q.defer();

        $http({
          method: 'GET',
          url: '/zaak/' + caseId + '/case_documents',
        })
          .success(function (response) {
            _.each(response.result, function (caseDoc) {
              caseDocsById[caseDoc.id] = caseDoc;
            });
            deferred.resolve(response.result);
          })
          .error(function (response) {
            deferred.reject(response);
          });

        return deferred.promise;
      };

      caseDocumentService.getCaseDocById = function (id) {
        return caseDocsById[id];
      };

      return caseDocumentService;
    },
  ]);
})();
