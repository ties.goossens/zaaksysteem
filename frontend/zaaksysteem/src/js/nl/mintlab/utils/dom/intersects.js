// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

(function () {
  window.zsDefine('nl.mintlab.utils.dom.intersects', function () {
    var getDocumentPosition = window.zsFetch(
      'nl.mintlab.utils.dom.getDocumentPosition'
    );

    return function (element, point) {
      var origin = getDocumentPosition(element),
        rect = {
          x: origin.x,
          y: origin.y,
          width: element.clientWidth,
          height: element.clientHeight,
        };

      return (
        point.x >= rect.x &&
        point.x <= rect.x + rect.width &&
        point.y >= rect.y &&
        point.y <= rect.y + rect.height
      );
    };
  });
})();
