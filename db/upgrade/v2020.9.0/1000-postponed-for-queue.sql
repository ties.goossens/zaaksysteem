BEGIN;

  ALTER TABLE queue DROP CONSTRAINT "queue_status_check";
  ALTER TABLE queue ADD CHECK(
      status IN(
          'postponed', 'pending', 'waiting', 'running', 'finished', 'failed',
          'cancelled'
      )
  );

COMMIT;
