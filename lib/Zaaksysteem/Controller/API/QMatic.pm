package Zaaksysteem::Controller::API::QMatic;

use Moose;
use namespace::autoclean;

use BTTW::Tools;

BEGIN { extends 'Zaaksysteem::General::ZAPIController' }


sub index : Local : ZAPI {
    my ($self, $c) = @_;

    eval {
        my $qmatic = $c->model('DB::Interface')->search_active({ module => 'qmatic' })->first
            or die "need qmatic interface";

        my $params = $c->req->params;
        my $action = $params->{action};

        my $result = $qmatic->process_trigger($action, $params);

        die "need result array" unless $result && @$result;

        $c->stash->{zapi} = $result;
    };

    if ($@) {
        $c->log->error("qmatic error: $@");

        my $type = $@ =~ m|^no qmatic interface was configured for case_type_id|
            ? 'qmatic/config_missing'
            : 'qmatic/unknown';

        $c->stash->{zapi} = Zaaksysteem::ZAPI::Error->new(
            type             => $type,
            messages         => []
        );
    }
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 index

TODO: Fix the POD

=cut

