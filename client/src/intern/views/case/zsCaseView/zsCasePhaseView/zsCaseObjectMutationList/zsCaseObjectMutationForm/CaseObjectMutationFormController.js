// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import assign from 'lodash/assign';
import first from 'lodash/head';
import find from 'lodash/find';
import get from 'lodash/get';
import identity from 'lodash/identity';
import isArray from 'lodash/isArray';
import mapValues from 'lodash/mapValues';
import omit from 'lodash/omit';
import pickBy from 'lodash/pickBy';
import seamlessImmutable from 'seamless-immutable';
import attrToVorm from './../../../../../../../shared/zs/vorm/attrToVorm';
import { stringToNumber } from '../../../../../../../shared/util/number';

export default class CaseObjectMutationFormController {
  static get $inject() {
    return [
      '$scope',
      '$animate',
      'composedReducer',
      'resource',
      'vormValidator',
      'caseAttrTemplateCompiler',
    ];
  }

  constructor(
    scope,
    $animate,
    composedReducer,
    resource,
    vormValidator,
    caseAttrTemplateCompiler
  ) {
    let ctrl = this;
    let values = seamlessImmutable({});

    let convertAttr = (attribute, val) => {
      let activeValues = get(attribute.data, 'values', []).filter(
        (option) => option.active || option.value === val
      );

      return {
        id: attribute.attribute_id,
        label: attribute.label || attribute.attribute_label,
        type: attribute.attribute_type,
        required: attribute.required,
        help: '',
        magic_string: attribute.name.replace('attribute.', ''),
        limit_values:
          attribute.attribute_type === 'subject' || attribute.multiple_values
            ? -1
            : 1,
        values: activeValues,
      };
    };

    // ZS-FIXME: We already do this in multiple places with
    // slightly different business rules; cf.
    // - client/src/shared/case/zsCaseRegistration/getApiValues.js
    // - client/src/shared/zs/case/parseAttributeValue/index.js
    const getApiValue = (type, value) => {
      if (value === null) {
        return value;
      }

      switch (type) {
        case 'numeric':
        case 'valuta':
        case 'valutaex':
        case 'valutaex21':
        case 'valutaex6':
        case 'valutain':
        case 'valutain21':
        case 'valutain6':
          return stringToNumber(value);
      }

      return value;
    };

    const findFieldByName = (name) => find(fieldReducer.data(), { name });

    let transformValues = (type, vals) => {
      let reduce = (field, value) => {
        return field[type]
          ? field[type].reduce((current, reducer) => {
              return reducer(current);
            }, value)
          : value;
      };

      return pickBy(
        mapValues(vals, (value, key) => {
          let field = find(fieldReducer.data(), { name: key });
          let val = value;

          if (!field) {
            return undefined;
          }

          val =
            isArray(val) && field.template !== 'checkbox-list'
              ? val.map((v) => reduce(field, v))
              : reduce(field, val);

          if (
            field.limit === 1 &&
            field.template !== 'checkbox-list' &&
            isArray(val)
          ) {
            val = first(val);
          }

          return val;
        }),
        (val) => val !== undefined
      );
    };

    const actionReducer = composedReducer(
      { scope },
      () => ctrl.mutationVerb
    ).reduce((verb) => [
      {
        label: 'Annuleren',
        click: () => {
          ctrl.onCancel();
        },
      },
      {
        label: verb,
        type: 'submit',
        click: () => {
          const parsedValues = transformValues(
            'parsers',
            omit(values, '$object')
          );
          // NB: Adding a parser in attrToVorm is not possible because
          // the parsers trigger the formatters again during user input.
          const apiValues = Object.keys(parsedValues).reduce(
            (accumulator, value) =>
              assign(accumulator, {
                [value]: getApiValue(
                  findFieldByName(value).type,
                  parsedValues[value]
                ),
              }),
            {}
          );

          ctrl.onSubmit({
            $values: apiValues,
            $object: values.$object,
          });
        },
        classes: {
          'btn-primary': true,
        },
      },
    ]);

    const fieldReducer = composedReducer(
      { scope },
      ctrl.objectTypeResource(),
      ctrl.mutationType,
      ctrl.objectId,
      () => values
    ).reduce((objectType, type, objectId, vals) => {
      let fields = [];

      if (type === 'relate' || type === 'update' || type === 'delete') {
        fields = fields.concat({
          name: '$object',
          label: ctrl.objectLabel,
          template: {
            inherits: 'object-suggest',
            display: () => {
              return angular.element(
                `<div class="file-list-item object-mutation-form-item">
													<a ng-href="/object/{{delegate.value.id}}" target="_blank" rel="noopener">
														<span>{{delegate.value.label}}</span>
														<zs-icon icon-type="launch"></zs-icon>
													</a>
													<zs-spinner class="spinner-tiny" is-loading="vm.templateData().isLoading()"></zs-spinner>
												</div>`
              );
            },
          },
          data: {
            objectType: objectType.values.prefix,
            isLoading: () => objectResource.state() === 'pending',
          },
          required: true,
          disabled: !!objectId,
        });
      }

      fields = fields.concat(
        get(objectType, 'values.attributes', [])
          .map((attribute) => {
            let attrName = attribute.name.replace('attribute.', '');
            let attr = attrToVorm(convertAttr(attribute, vals[attrName]));
            let disabled = type === 'delete' || type === 'relate';
            let required = attribute.required && !disabled;
            let when = true;
            let config;

            if (type === 'update' || type === 'delete' || type === 'relate') {
              when = () => {
                return values.$object && objectResource.state() === 'resolved';
              };
            }

            config = assign({}, attr, {
              disabled,
              required,
              when,
            });

            if (attr.type === 'file') {
              config = assign({}, config, {
                template: 'file',
                data: assign({}, config.data, {
                  target: '/filestore/upload',
                  transform: [
                    '$file',
                    '$data',
                    (file, data) => {
                      return data.result[0];
                    },
                  ],
                  display: (file) => {
                    return get(file, 'filename');
                  },
                }),
              });
            }

            return config;
          })
          .filter(identity)
      );

      return seamlessImmutable(fields).asMutable({ deep: true });
    });

    const objectResource = resource(
      () => {
        let objectId = ctrl.objectId()
          ? ctrl.objectId()
          : get(values, '$object.id');

        return objectId ? `/api/object/${objectId}` : null;
      },
      { scope }
    ).reduce((requestOptions, data) => first(data));

    if (ctrl.objectId()) {
      objectResource.onUpdate(() => {
        values = values.merge({ $object: objectResource.data() });
      });
    }

    composedReducer(
      {
        scope,
        mode: 'hot',
        waitUntilResolved: false,
      },
      objectResource,
      ctrl.objectTypeResource(),
      ctrl.defaults()
    ).onUpdate(() => {
      let object = objectResource.data();
      let objectType = ctrl.objectTypeResource().data();
      let defaults = ctrl.defaults();
      let vals = {};

      if (object && objectType) {
        vals = omit(object.values, 'date_created', 'date_modified');
      }

      values = values.merge(
        transformValues('formatters', assign(vals, defaults))
      );
    });

    const validityReducer = composedReducer(
      { scope },
      fieldReducer,
      () => values
    ).reduce((fields = [], vals = {}) => vormValidator(fields, vals));

    ctrl.getCompiler = () => caseAttrTemplateCompiler;

    ctrl.getActions = actionReducer.data;

    ctrl.getFields = fieldReducer.data;

    ctrl.isValid = () => get(validityReducer.data(), 'valid');

    ctrl.handleChange = (name, value) => {
      if (name === '$object' && !value) {
        values = seamlessImmutable({});
      } else {
        const checkForEmptyArray = (val) => (val[0] === null ? undefined : val);

        values = values.merge({ [name]: checkForEmptyArray(value) });
      }
    };

    ctrl.handleClick = (action) => {
      action.click();
    };

    ctrl.getValues = () => values;
  }
}
