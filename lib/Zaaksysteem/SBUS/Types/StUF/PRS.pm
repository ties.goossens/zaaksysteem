package Zaaksysteem::SBUS::Types::StUF::PRS;

use Data::Dumper;
use Moose;

use Zaaksysteem::SBUS::Constants;

extends 'Zaaksysteem::SBUS::Types::StUF::GenericAdapter';

has 'xml_structure' => (
    'is'        => 'rw',
    'lazy'      => 1,
    'default'   => sub {
        my $xml_structure   = STUF_XML_STRUCTURE;
        my $xml_definition  = STUF_XML_DEFINITION;

        $xml_structure->{PRS}->{ $_ } = $xml_definition->{PRS}->{$_}
            for keys %{ $xml_definition->{PRS} };

        return $xml_structure;
    }
);

sub _stuf_to_params {
    my ($self, $prs_xml, $stuf_options) = @_;

    my $create_params = $self->_convert_stuf_to_hash(
        $prs_xml,
        STUF_PRS_NP_MAPPING
    );

    $create_params->{geboorteplaats} = GBA_CODE_GEMEENTE->{
        int($create_params->{geboorteplaats})
    } if defined($create_params->{geboorteplaats});

    $create_params->{geboorteland} = GBA_CODE_LAND->{
        int($create_params->{geboorteland})
    } if defined($create_params->{geboorteland});

    return $create_params;
}

sub _stuf_relaties {
    my ($self, $prs_xml, $stuf_options, $create_options) = @_;

    ### Handle partnerdata
    if (
        $prs_xml->{PRSPRSHUW} &&
        ref($prs_xml->{PRSPRSHUW}) &&
        ref($prs_xml->{PRSPRSHUW}->[0]) &&
        (my $partner = $prs_xml->{PRSPRSHUW}->[0]->{PRS})
    ) {
        my $PARTNER_MAPPING = STUF_PRS_PARTNER_MAPPING;

        my $partner_data    = {};

        $partner_data = $self->_convert_stuf_to_hash(
            $partner,
            $PARTNER_MAPPING
        );

        $partner_data->{datum_huwelijk} = $self->_parse_value(
            $prs_xml->{PRSPRSHUW}->[0]->{datumSluiting}
        );
        $partner_data->{datum_huwelijk_ontbinding} = $self->_parse_value(
            $prs_xml->{PRSPRSHUW}->[0]->{datumOntbinding}
        );

        $create_options->{ $_ } = $partner_data->{ $_ }
            for keys %{ $partner_data };
    }

    ### Handle verblijfsadres
    {
        my ($prsadr, $verblijf);
        if (
            $prs_xml->{PRSADRVBL} &&
            ref($prs_xml->{PRSADRVBL}) &&
            ($verblijf = $prs_xml->{PRSADRVBL}->{ADR})
        ) {
            $prsadr = $verblijf;
            $create_options->{functie_adres} = 'W';
        } elsif (
            $prs_xml->{PRSADRCOR} &&
            ref($prs_xml->{PRSADRCOR}) &&
            ($verblijf = $prs_xml->{PRSADRCOR}->{ADR})
        ) {
            $prsadr = $verblijf;
            $create_options->{functie_adres} = 'B';
        }

        my $adres_data      = $self->_stuf_prs_adres(
            $prsadr
        );

        $create_options->{ $_ } = $adres_data->{ $_ }
            for keys %{ $adres_data };
    }

    ### Moved to other gemeentecode? Then delete it!
    if (
        defined($self->config->{gemeentecode})
    ) {
        if (
            !$create_options->{gemeente_code} ||
            $create_options->{gemeente_code} ne $self->config->{gemeentecode}
        ) {
            $stuf_options->{mutatie_type} = 'V'
        }
    } else {
        die('SBUS Option: gemeentecode NOT set. Cannot continue');
    }
}

sub _stuf_prs_adres {
    my ($self, $adresxml) = @_;

    my $ADRES_MAPPING = STUF_PRS_ADRES_MAPPING;

    unless (ref($adresxml)) {
        return {};

    }

    my $adres_data = $self->_convert_stuf_to_hash(
        $adresxml,
        $ADRES_MAPPING
    );

    my $extra_elementen = $self->_convert_stuf_extra_elementen_to_hash(
        $adresxml->{extraElementen}
    );

    if ($extra_elementen->{authentiekeWoonplaatsnaam}) {
        my $mapping         = {
            identificatiecodeVerblijfplaats => 'verblijfsobject_id',
            woonplaatsNaamBAG               => 'woonplaats',
        };

        my $extra   = $self->_convert_stuf_to_hash(
            $extra_elementen,
            $mapping,
        );

        $adres_data->{ $_ }     = $extra->{ $_ }
            for keys %{ $extra };

        ### Special: use officieleStraatnaam as straatnaam, because some systems
        ### will shorten the straatnaam to something like Damstr instead of
        ### Damstraat
        $adres_data->{straatnaam}   = $extra_elementen->{officieleStraatnaam}
            if $extra_elementen->{officieleStraatnaam};
    }

    return $adres_data;
}

around 'kennisgeving'   => sub {
    my $orig    = shift;
    my $self    = shift;
    my $search  = shift;
    my $opt     = shift;
    my $stuf_options = shift;

    ### Implement safety check: only indication with bsn-number
    if (
        defined($search->{afnemerIndicatie})
    ) {
        unless (
            defined($search->{'sleutelGegevensbeheer'}) &&
            $search->{'sleutelGegevensbeheer'}
        ) {
            die('Afnemen only possible on sleutelGegevensbeheer');
        }

        $opt->{afnemerIndicatie}    = $search->{afnemerIndicatie};
        delete($search->{afnemerIndicatie});

        $stuf_options->{dispatch_method}    = 'ontvangKennisgeving';
        $stuf_options->{dispatch_port}      = 'asynchroon';
    }

    unless ($opt->{entiteittype}) {
        $opt->{entiteittype}        = 'PRS';
        $opt->{sectormodel}         = 'BG';
        $opt->{versieStUF}          = '0204';
        $opt->{versieSectormodel}   = '0204';
        $opt->{referentienummer}    = $stuf_options
                                    ->{traffic_object}
                                    ->id;
        $opt->{tijdstipBericht}     = $stuf_options
                                    ->{traffic_object}
                                    ->created
                                    ->strftime('%Y%m%d%H%M%S00');

        if (!defined($stuf_options->{dispatch_method})) {
            $stuf_options->{dispatch_method}
                = 'beantwoordSynchroneVraag';
        }
    }

    $self->$orig($search, $opt, @_);
};

around 'search' => sub {
    my $orig    = shift;
    my $self    = shift;
    my $search  = shift;
    my $opt     = shift;
    my $stuf_options = shift;

    ### Implement safety check: only indication with bsn-number
    if (
        defined($search->{afnemerIndicatie})
    ) {
        unless (
            defined($search->{'sleutelGegevensbeheer'}) &&
            $search->{'sleutelGegevensbeheer'}
        ) {
            die('Afnemen only possible on sleutelGegevensbeheer');
        }

        $opt->{afnemerIndicatie}    = $search->{afnemerIndicatie};
        delete($search->{afnemerIndicatie});

        $stuf_options->{dispatch_method} = 'ontvangAsynchroneVraag';
    }

    ### Default search
    unless ($opt->{entiteittype}) {
        $opt->{entiteittype}        = 'PRS';
        $opt->{sectormodel}         = 'BG';
        $opt->{versieStUF}          = '0204';
        $opt->{versieSectormodel}   = '0204';
        $opt->{referentienummer}    = $stuf_options
                                    ->{traffic_object}
                                    ->id;
        $opt->{tijdstipBericht}     = $stuf_options
                                    ->{traffic_object}
                                    ->created
                                    ->strftime('%Y%m%d%H%M%S00');

        $stuf_options->{dispatch_method} = 'beantwoordSynchroneVraag';
    }

    $self->$orig($search, $opt, @_);
};

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 GBA_CODE_GEMEENTE

TODO: Fix the POD

=cut

=head2 GBA_CODE_LAND

TODO: Fix the POD

=cut

=head2 STUF_PRS_ADRES_MAPPING

TODO: Fix the POD

=cut

=head2 STUF_PRS_NP_MAPPING

TODO: Fix the POD

=cut

=head2 STUF_PRS_PARTNER_MAPPING

TODO: Fix the POD

=cut

=head2 STUF_XML_DEFINITION

TODO: Fix the POD

=cut

=head2 STUF_XML_STRUCTURE

TODO: Fix the POD

=cut

