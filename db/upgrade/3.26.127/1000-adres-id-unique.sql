BEGIN;

    ALTER TABLE natuurlijk_persoon ADD CONSTRAINT natuurlijk_persoon_adres_id_unique UNIQUE (adres_id);

COMMIT;
