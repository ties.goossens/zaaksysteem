#! perl

use TestSetup;
initialize_test_globals_ok;

use Zaaksysteem::Backend::Subject::Naamgebruik qw/naamgebruik/;

$zs->zs_transaction_ok(sub {

    for (qw/X vcc % `/) {
        throws_ok(sub {
            naamgebruik({aanduiding => $_});
        }, qr/Validation of profile/, "Aanduiding moet leeg zijn of P/V/N");
    }

    # P
    is naamgebruik({
        aanduiding => 'P',
        partner_voorvoegsel => 'van de',
        partner_geslachtsnaam => 'Brink',
    }), 'van de Brink', "Partner: van de Brink OK";


    is naamgebruik({
        aanduiding => 'P',
        partner_voorvoegsel => '',
        partner_geslachtsnaam => 'Brink',
    }), 'Brink', "Partner: Brink OK";


    # V
    is naamgebruik({
        aanduiding => 'V',
        partner_voorvoegsel => 'van de',
        partner_geslachtsnaam => 'Brink',
        voorvoegsel => 'de',
        geslachtsnaam => 'Jong'
    }), 'van de Brink-de Jong', "V: van de Brink-de Jong OK";


    is naamgebruik({
        aanduiding => 'V',
        partner_voorvoegsel => '',
        partner_geslachtsnaam => 'Brink',
        voorvoegsel => '',
        geslachtsnaam => 'Jong'
    }), 'Brink-Jong', "Partner: Brink-Jong OK";


    # N
    is naamgebruik({
        aanduiding => 'N',
        partner_voorvoegsel => 'van de',
        partner_geslachtsnaam => 'Brink',
        voorvoegsel => 'de',
        geslachtsnaam => 'Jong'
    }), 'de Jong-van de Brink', "V: de Jong-van de Brink OK";


    is naamgebruik({
        aanduiding => 'N',
        partner_voorvoegsel => '',
        partner_geslachtsnaam => 'Brink',
        voorvoegsel => '',
        geslachtsnaam => 'Jong'
    }), 'Jong-Brink', "Partner: Jong-Brink OK";


    # E
    is naamgebruik({
        aanduiding => 'E',
        partner_voorvoegsel => 'van de',
        partner_geslachtsnaam => 'Brink',
        voorvoegsel => '',
        geslachtsnaam => 'Jong'
    }), 'Jong', "Eigen: Jong OK";


    is naamgebruik({
        aanduiding => 'E',
        partner_voorvoegsel => '',
        partner_geslachtsnaam => 'Brink',
        voorvoegsel => 'de',
        geslachtsnaam => 'Jong'
    }), 'de Jong', "Eigen: de Jong OK";

}, "Testing naamstelling");

zs_done_testing();
