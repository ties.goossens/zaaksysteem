package Zaaksysteem::DB::ResultSet::ZaaktypeResultaten;

use Moose;

use BTTW::Tools;
use DateTime;

extends qw[
    DBIx::Class::ResultSet
    Zaaksysteem::Zaaktypen::BaseResultSet
];

use constant PROFILE => {
    required        => [qw/
    /],
    optional        => [qw/
    /],
};

around _retrieve_as_session => sub {
    my $orig = shift;
    my $self = shift;

    my $rv = $self->$orig(@_);

    for my $row (values %{ $rv }) {
        $row->{ $_ } = $row->{ $_ } ? $row->{ $_ }->dmy : undef for qw[
            selectielijst_brondatum
            selectielijst_einddatum
        ];

        for my $property (keys %{ $row->{ properties } }) {
            $row->{"properties_$property"} = $row->{ properties }{ $property };
        }
    }

    return $rv;
};

sub _validate_session {
    my $self            = shift;
    my $profile         = PROFILE;
    my $rv              = {};

    $self->__validate_session(
        @_,
        $profile,
    );
}

sub _commit_session {
    my ($self, $node, $element_session_data) = @_;

    my $profile         = PROFILE;
    my $rv              = {};

    while (my ($key, $data) = each %{ $element_session_data }) {
        $data->{trigger_archival} //= 0;
        $data->{standaard_keuze}  //= 0;

        $data->{ $_ } = $self->_mangle_date($data->{ $_ }) for qw[
            selectielijst_brondatum
            selectielijst_einddatum
        ];

        my %properties = ();
        for my $prop (grep { /^properties_/ } keys %$data) {
            (my $property_name = $prop) =~ s/^properties_//;

            $properties{ $property_name } = delete $data->{$prop};
        }
        $data->{ properties } = \%properties;
    }

    $self->next::method(
        $node,
        $element_session_data,
        {
            status_id_column_name   => 'zaaktype_status_id',
        }
    );
}

sub _mangle_date {
    my $self = shift;
    my $date = shift;

    return undef unless defined $date;
    return $date if blessed $date && $date->isa('DateTime');

    my ($day, $month, $year) = $date =~ m[^(\d{2})\-(\d{2})\-(\d{4})$];

    return undef unless $day && $month && $year;

    return DateTime->new(
        day => $day,
        month => $month,
        year => $year
    );
}

=head2 find_by_result_name()

Searches related result types, under restraints of 'preferred choice' and
returns the one interested in.

We used to be able to search for a result by name. Some (external) systems only
can do this by this non-unique identifier. In such cases, we used to return the
'first', what soever that would be, there was no sorting.

A new approach is to make one of those non-unique results preferred, by setting
the 'preferred choice'.

If we have multiple results by the same name, the following will be happening:

=over

=item * Return the first if none is marked as 'preferred choice'.

This used to be the 'unpredictable' behaviour, but ala, nothing breaks. Please
considder using the option to set one, and only one to be the 'preferred choice'
Sorting is done by C<id>, which happens to be the same order as the results are
shown at the case type managment page where results can be reordered.

=item * Return the one that has been marked as 'preferred' if there is only one

=item * Throw an exception if multiple have been marked as 'preferred choice'.

That is just a very bad setup of the casetype

=back

=cut

sig find_by_result_name => 'Str';

sub find_by_result_name {
    my $self = shift;
    my $result_name = shift;

    my $rs = $self->search_rs(
        {
            'LOWER(resultaat)' => lc($result_name)
        },
        {
            order_by => {'-asc' => 'id'},
        }
    );
    my @result_types = $rs->all;
    my $count = @result_types;
    return if !$count;
    return $result_types[0] if $count == 1;

    my @prefered_result_types = grep { $_->standaard_keuze } @result_types;
    $count = @prefered_result_types;

    if ($count == 0) {
        $self->log->warn(
            sprintf(
                "Multiple result-types for '%s', but non are prefered, choosing 'first'"
                . " " .
                "(Result: '%s', Description: '%s')",
                $result_name,
                $result_types[0]->resultaat,
                $result_types[0]->label,
            ),
        );
        return $result_types[0];
    }
    elsif ($count == 1) {
        return $prefered_result_types[0];
    }
    else {
        throw(
            "casetype_results/find_by_result_name/multiple_prefered",
            sprintf( "Multiple result types for '%s', but more than one are prefered",
                $result_name,
            ),
        );
    }
}

=head2 find_by_natural_index

Returns the n-th CasetypeResult, by natural index (1-based, not 0-based)

=cut

sig find_by_natural_index => 'Int';

sub find_by_natural_index {
    my $self = shift;
    my $index_order = shift;

    return $self->search_rs(
        {},
        {
            order_by => {'-asc' => 'id'},
            offset   => $index_order -1,
            rows     => 1,
        }
    )->first;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
