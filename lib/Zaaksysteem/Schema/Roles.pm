use utf8;
package Zaaksysteem::Schema::Roles;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Zaaksysteem::Schema::Roles

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<Zaaksysteem::Result>

=cut

use base 'Zaaksysteem::Result';

=head1 TABLE: C<roles>

=cut

__PACKAGE__->table("roles");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'roles_id_seq'

=head2 parent_group_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 name

  data_type: 'text'
  is_nullable: 1

=head2 description

  data_type: 'text'
  is_nullable: 1

=head2 system_role

  data_type: 'boolean'
  is_nullable: 1

=head2 date_created

  data_type: 'timestamp'
  is_nullable: 1
  timezone: 'UTC'

=head2 date_modified

  data_type: 'timestamp'
  is_nullable: 1
  timezone: 'UTC'

=head2 uuid

  data_type: 'uuid'
  default_value: uuid_generate_v4()
  is_nullable: 0
  size: 16

=head2 v1_json

  data_type: 'jsonb'
  default_value: '{}'
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "roles_id_seq",
  },
  "parent_group_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "name",
  { data_type => "text", is_nullable => 1 },
  "description",
  { data_type => "text", is_nullable => 1 },
  "system_role",
  { data_type => "boolean", is_nullable => 1 },
  "date_created",
  { data_type => "timestamp", is_nullable => 1, timezone => "UTC" },
  "date_modified",
  { data_type => "timestamp", is_nullable => 1, timezone => "UTC" },
  "uuid",
  {
    data_type => "uuid",
    default_value => \"uuid_generate_v4()",
    is_nullable => 0,
    size => 16,
  },
  "v1_json",
  { data_type => "jsonb", default_value => "{}", is_nullable => 0 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 UNIQUE CONSTRAINTS

=head2 C<roles_uuid_idx>

=over 4

=item * L</uuid>

=back

=cut

__PACKAGE__->add_unique_constraint("roles_uuid_idx", ["uuid"]);

=head1 RELATIONS

=head2 files

Type: has_many

Related object: L<Zaaksysteem::Schema::File>

=cut

__PACKAGE__->has_many(
  "files",
  "Zaaksysteem::Schema::File",
  { "foreign.intake_role_id" => "self.id" },
  undef,
);

=head2 parent_group_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::Groups>

=cut

__PACKAGE__->belongs_to(
  "parent_group_id",
  "Zaaksysteem::Schema::Groups",
  { id => "parent_group_id" },
);

=head2 role_rights

Type: has_many

Related object: L<Zaaksysteem::Schema::RoleRights>

=cut

__PACKAGE__->has_many(
  "role_rights",
  "Zaaksysteem::Schema::RoleRights",
  { "foreign.role_id" => "self.id" },
  undef,
);

=head2 rights_names

Type: many_to_many

Composing rels: L</role_rights> -> rights_name

=cut

__PACKAGE__->many_to_many("rights_names", "role_rights", "rights_name");


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2021-07-05 17:04:51
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:GKHp4jHDFld+Zzq236S0RA

__PACKAGE__->resultset_class('Zaaksysteem::Backend::Roles::ResultSet');

__PACKAGE__->load_components(
    '+Zaaksysteem::Backend::Roles::Component',
    __PACKAGE__->load_components()
);

__PACKAGE__->add_columns('date_created',
    { %{ __PACKAGE__->column_info('date_created') },
    set_on_create => 1,
});

__PACKAGE__->add_columns('date_modified',
    { %{ __PACKAGE__->column_info('date_modified') },
    set_on_update => 1,
    set_on_create => 1,
});

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
