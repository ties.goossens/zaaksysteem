package Zaaksysteem::Zaken::Roles::Fields;
use Moose::Role;

use List::Util qw(first);

sub visible_fields {
    my ($self, $args) = @_;

    my $fase          = $args->{phase};
    my $mandatory     = $args->{mandatory};
    my $include_docs  = $args->{include_documents};

    my $rules         = $self->rules;
    my $rules_profile = $rules->validate_from_case($self);

    my $mapping       = { reverse %{ $rules->_attribute_mapping } };
    my $status_number = $fase ? $fase->status : $self->milestone;

    my $fields = {};
    for my $attribute (@{ $rules_profile->active_attributes }) {
        my $raw_attribute   = $attribute;
        $raw_attribute      =~ s/^attribute\.//;
        my $bibid           = $mapping->{$raw_attribute};

        my $zt_kenmerk = first {
            ($_->get_column('bibliotheek_kenmerken_id') // '') eq $bibid
                && $_->zaak_status_id->status == $status_number
        } @{ $rules->old_case_attributes };

        next unless $zt_kenmerk;

        ### When NO option include_docs is given, skip files.
        next if (
            !$include_docs &&
            $zt_kenmerk->bibliotheek_kenmerken_id->value_type eq 'file'
        );

        ### When option mandatory is given, only show mandatory fields
        next if ($mandatory && !$zt_kenmerk->value_mandatory);

        $fields->{ $bibid } = 1;
    }

    return $fields;
}

=head2 empty_hidden_attributes

Arguments: $PHASE_OBJECT

    $ok = $case->empty_hidden_attributes($case->zaaktype_statussen->search({status => 1})->first);

Empty invisible fields

=cut

sub fill_attributes_from_rules {
    my $self            = shift;
    my $phase           = shift;

    my $rules           = $self->rules(
        {
            'case.number_status' => $phase->status,
            'reload'             => 1,
        }
    );

    my $val_profile     = $rules->validate_from_case($self, { 'case.number_status' => $phase->status });
    my $mapping         = $rules->_attribute_mapping;
    my $visible_fields  = $rules->_get_list_of_visible_attributes_for_number_status($phase->status);

    my (@empty_fields, @empty_files);
    for my $kenmerk (@{ $rules->old_case_attributes }) {
        my $attribute_name;

        ### Only look at non-systeemkenmerken with a bibliotheek_kenmerken_id
        next unless (
            $kenmerk->get_column('bibliotheek_kenmerken_id') &&
            $kenmerk->zaak_status_id->status == $phase->status &&
            !$kenmerk->is_systeemkenmerk
        );

        next unless $kenmerk->get_column('zaak_status_id') eq $phase->id;

        next unless ($attribute_name = $mapping->{ $kenmerk->get_column('bibliotheek_kenmerken_id') });

        ### The problem here is, a hidden field in one phase could be visible in the other. Because
        ### of the hack: show the same attributes in different phases. To prevent this,
        ### check if our visible_fields check tells us the field is visible.
        next if grep { 'attribute.' . $mapping->{ $kenmerk->get_column('bibliotheek_kenmerken_id') } eq $_ && $kenmerk->is_systeemkenmerk} @$visible_fields;

        $attribute_name     = 'attribute.' . $attribute_name;

        if (!grep({ $attribute_name eq $_ } @{ $val_profile->active_attributes })) {
            if ($kenmerk->bibliotheek_kenmerken_id->value_type eq 'file') {
                push(@empty_files, $kenmerk->id);
            } else {
                push(@empty_fields, $kenmerk->get_column('bibliotheek_kenmerken_id'));
            }
        }
    }

    # print STDERR 'Deleting fields: ' . join(',', @empty_fields) . "\n";

    ### Delete attributes
    if (@empty_fields) {
        $self->zaak_kenmerken->delete_fields({
            bibliotheek_kenmerken_ids => \@empty_fields,
            zaak => $self,
        });

        ### Remove wijzigingsverzoeken on these fields, because these are not longer visible
        my $change_requests = $self->result_source->schema->resultset('ScheduledJobs')->search_update_field_tasks(
            {
                case_id     => $self->id,
                kenmerken   => \@empty_fields,
            }
        );

        if ($change_requests->count) {
            $change_requests->update({deleted => DateTime->now()});
        }
    }

    ### Remove labels from all hidden file kenmerken
    if (@empty_files) {
        $self->result_source->schema->resultset('FileCaseDocument')->search(
            {
                case_document_id    => \@empty_files,
                file_id             => { -in => $self->files->search()->get_column('id')->as_query }
            }
        )->delete;
    }

    return 1;
}


=head2 phase_fields_complete

Checks if all required fields for this phase are filled,
taking into account the fields that are hidden by rules.

=cut

sub phase_fields_complete {
    my ($self, $args) = @_;

    my $fase = $args->{phase} or die "need phase";

    my $given_kenmerken = $args->{custom_fields}; # optional

    $given_kenmerken ||= $self->field_values({ fase => $fase->status });

    my $kenmerken  = { %{ $given_kenmerken } };

    my $rules_result;
    my $required_fields = $self->visible_fields({
        phase           => $fase,
        mandatory       => 1,
        field_values    => $given_kenmerken,
        result          => $rules_result,
    });

    #$kenmerken  = { %{ $given_kenmerken } };

    ### PAUZE?

    if ($rules_result->{pauzeer_aanvraag}) {
        my $key = [ keys %{ $rules_result->{pauzeer_aanvraag} } ]->[0];
        return {
            'succes'    => 0,
            'pauze'     => $rules_result->{pauzeer_aanvraag}->{$key},
        }
    }

    my @missing_required;
    foreach my $bibliotheek_kenmerken_id (keys %$required_fields) {
        my $value = $kenmerken->{$bibliotheek_kenmerken_id};
        $value = ref $value && ref $value eq 'ARRAY' ? join "", @$value : $value;

        next if length($value);

        push @missing_required, $bibliotheek_kenmerken_id;
    }

    return { succes => 1 } if (!@missing_required);

    my $bk = $self->result_source->schema->resultset('BibliotheekKenmerken')->search(
        { id => \@missing_required },
        { order_by => 'magic_string' },
    );

    return {
        succes   => 0,
        required => 1,
        missing  => [ map { $_->magic_string } $bk->all ],
    };

}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 visible_fields

TODO: Fix the POD

=cut

