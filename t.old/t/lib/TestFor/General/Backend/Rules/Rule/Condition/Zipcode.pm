package TestFor::General::Backend::Rules::Rule::Condition::Zipcode;

# ./zs_prove -v t/lib/TestFor/General/Backend/Rules/Rule/Condition/Zipcode.pm
use base qw(ZSTest);

use Moose;
use TestSetup;
use File::Spec::Functions qw(catfile);

use Zaaksysteem::Backend::Rules;
use Zaaksysteem::Backend::Rules::Rule::Condition;

use JSON qw/decode_json encode_json/;


=head1 NAME

TestFor::General::Backend::Rules::Rule::Condition::Zipcode - Rule condition: Zipcode

=head1 CODE TESTS

Code tests, testing the implementation itself

=head2 rules_rule_condition_area_wijk

=cut

sub rules_rule_condition_zipcode_aanvrager : Tests {
    my $self            = shift;

    $zs->zs_transaction_ok(sub {
        my ($casetype, $zaaktype_status) = $self->_generate_rules_for_area();

        my $customnp = $zs->create_aanvrager_np_ok(
            postcode    => '1051JL',
            straatnaam  => 'Donker Curtiusstraat',
            huisletter  => 'A',
            huisnummer  => 23,
            huisnummertoevoeging => '1rec'
        );
        my $case     = $zs->create_case_ok(zaaktype => $casetype, aanvragers => [$customnp]);

        # ### Now create an area for this person in west
        # my $parkeer  = $schema->resultset('Parkeergebied')->create({
        #     postcode    => '1051JL',
        #     huisnummer  => 23,
        #     huisletter  => 'A',
        #     huisnummertoevoeging => '1rec',
        #     straatnaam  => 'Donker Curtiusstraat',
        #     parkeergebied => 'Zuid',
        # });

        $zs->create_bag_records;

        my $engine   = Zaaksysteem::Backend::Rules->new_from_case(
            $case,
            {
                'case.number_status'   => 1,
            }
        );

        ### Second condition, with undef values, should be validated true
        is($engine->rules->[0]->then->[0]->data->{value}, 'West', 'Westwijk: Condition is "West"');
        TODO : {
            local $TODO = "Michiel please check!";
            is($engine->rules->[0]->conditions->[0]->{validates_true}, 1, 'Westwijk: Validates to true');
        };

        ### Change to unknown zipcode
        $case->aanvrager_object->postcode('1234AA');

        # $parkeer->delete;
        $engine   = Zaaksysteem::Backend::Rules->new_from_case(
            $case,
            {
                'case.number_status'   => 1,
            }
        );

        is($engine->rules->[0]->then->[0]->data->{value}, 'West', 'Westwijk: Condition is "West"');
        is($engine->rules->[0]->conditions->[0]->{validates_true}, 0, 'Westwijk: Validates to FALSE');

        ### Do some extra checks
        TODO : {
            local $TODO = "Michiel please check!";
            for my $zipcode (qw/1051JA 1051JB 1051JZ/) {
                $case->aanvrager_object->postcode($zipcode);

                $engine   = Zaaksysteem::Backend::Rules->new_from_case(
                    $case,
                    {
                        'case.number_status'   => 1,
                    }
                );

                is($engine->rules->[0]->conditions->[0]->{validates_true}, 1, 'Westwijk validates true on zipcode: ' . $zipcode);
            }
        };

        for my $zipcode (qw/1050JA 1052JB 1234JL 1053XX/) {
            $case->aanvrager_object->postcode($zipcode);

            $engine   = Zaaksysteem::Backend::Rules->new_from_case(
                $case,
                {
                    'case.number_status'   => 1,
                }
            );

            is($engine->rules->[0]->conditions->[0]->{validates_true}, 0, 'Westwijk validates false on zipcode: ' . $zipcode);
        }

    }, 'condition::zipcode: checked zipcode from aanvrager (aanvrager_postcode) (1051JL, must be between 1051JA and 1051JZ)');
}

sub rules_rule_condition_zipcode_internal_functions : Tests {
    $zs->zs_transaction_ok(sub {
        my $values = {
            'nummeraanduiding-9876543218375842'     => '1051JL',
            'nummeraanduiding-9876543218375841'     => undef,
            '1051JL'                                => '1051JL',
            '1244AB'                                => '1244AB',
            '1B44AB'                                => undef,
            '12441B'                                => undef,
        };

        $zs->create_bag_records;

        for my $key (keys %$values) {
            my $cond_zipcode    = Zaaksysteem::Backend::Rules::Rule::Condition->new(
                attribute => 'aanvrager_postcode',
                _schema   => $schema,
                rules_params => {
                    'case.requestor.zipcode' => $key
                }
            );

            is ($cond_zipcode->_extract_zipcode(), $values->{$key}, 'Found correct value for location: ' . $key . ': ' . ($values->{$key} || 'undef'));

        }
    }, '_extract_zipcode: checked different values');
}

sub _generate_rules_for_area {
    my $self            = shift;
    my (%opts)          = @_;

    my $zaaktype_node   = $zs->create_zaaktype_node_ok;
    my $casetype        = $zs->create_zaaktype_ok(node => $zaaktype_node);
    my $zaaktype_status = $zs->create_zaaktype_status_ok(
        status => 1,
        fase   => 'registratiefase',
        node   => $zaaktype_node
    );
    $zs->create_zaaktype_status_ok(
        status => 3,
        fase   => 'afhandelfase',
        node   => $zaaktype_node
    );

    ### Generate zaaktype_kenmerken, checkboxes
    my $zt_kenmerk = $zs->create_zaaktype_kenmerk_ok(
        status              => $zaaktype_status,
        bibliotheek_kenmerk => $zs->create_bibliotheek_kenmerk_ok(
            naam            => 'checkbox_wijken',
            magic_string    => 'checkbox_wijken',
            value_type      => 'checkbox',
            values          => [
                {
                    value   => 'Noord',
                    active  => 1,
                },
                {
                    value   => 'Oost',
                    active  => 1,
                },
                {
                    value   => 'Zuid',
                    active  => 1,
                },
                {
                    value   => 'West',
                    active  => 1,
                },
                {
                    value   => 'Geen',
                    active  => 1,
                }
            ]
        )
    );

    $zs->create_zaaktype_regel_ok(
        status  => $zaaktype_status,
        # node => $zaaktype_node,
        naam    => 'Rule check: aanvrager_wijk ',
        settings => {
            'voorwaarde_1_kenmerk'          => 'aanvrager_postcode',
            'voorwaarde_1_value'            => '1',
            'voorwaarde_1_postcode_from'    => '1051JA',
            'voorwaarde_1_postcode_to'      => '1051JZ',
            'voorwaarden'                   => '1',


            'actie_1'                       => 'vul_waarde_in',
            'actie_1_kenmerk'               => $zt_kenmerk->bibliotheek_kenmerken_id->id,
            'actie_1_value'                 => 'West',
            'acties'                        => '1',

            'naam'                          => 'Vul west in',
        }
    );

    return ($casetype, $zaaktype_status);
};

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
