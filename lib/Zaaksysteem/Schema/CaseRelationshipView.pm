use utf8;
package Zaaksysteem::Schema::CaseRelationshipView;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Zaaksysteem::Schema::CaseRelationshipView

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<Zaaksysteem::Result>

=cut

use base 'Zaaksysteem::Result';
__PACKAGE__->table_class("DBIx::Class::ResultSource::View");

=head1 TABLE: C<case_relationship_view>

=cut

__PACKAGE__->table("case_relationship_view");
__PACKAGE__->result_source_instance->view_definition(" SELECT cr.case_id_a AS case_id,\n    cr.case_id_b AS relation_id,\n    cr.type_a AS type,\n    z.uuid AS relation_uuid\n   FROM (case_relation cr\n     JOIN zaak z ON (((z.id = cr.case_id_b) AND (z.deleted IS NULL))))\nUNION\n SELECT cr.case_id_b AS case_id,\n    cr.case_id_a AS relation_id,\n    cr.type_b AS type,\n    z.uuid AS relation_uuid\n   FROM (case_relation cr\n     JOIN zaak z ON (((z.id = cr.case_id_a) AND (z.deleted IS NULL))))\nUNION\n SELECT parent.id AS case_id,\n    child.id AS relation_id,\n    'parent'::character varying AS type,\n    child.uuid AS relation_uuid\n   FROM (zaak parent\n     JOIN zaak child ON ((child.pid = parent.id)))\n  WHERE ((child.deleted IS NULL) AND (parent.deleted IS NULL))");

=head1 ACCESSORS

=head2 case_id

  data_type: 'integer'
  is_nullable: 1

=head2 relation_id

  data_type: 'integer'
  is_nullable: 1

=head2 type

  data_type: 'text'
  is_nullable: 1
  original: {data_type => "varchar"}

=head2 relation_uuid

  data_type: 'uuid'
  is_nullable: 1
  size: 16

=cut

__PACKAGE__->add_columns(
  "case_id",
  { data_type => "integer", is_nullable => 1 },
  "relation_id",
  { data_type => "integer", is_nullable => 1 },
  "type",
  {
    data_type   => "text",
    is_nullable => 1,
    original    => { data_type => "varchar" },
  },
  "relation_uuid",
  { data_type => "uuid", is_nullable => 1, size => 16 },
);


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2021-05-11 14:50:55
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:i8EuwyjK/7gHWHB86kSLWw


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2021, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
