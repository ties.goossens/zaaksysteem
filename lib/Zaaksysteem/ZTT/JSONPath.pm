package Zaaksysteem::ZTT::JSONPath;
use Zaaksysteem::Moose;

with 'Zaaksysteem::Moose::Role::Schema';

use Scalar::Util qw(blessed);
use JSON::Path;
use JSON::XS qw(decode_json);

has '+schema' => (
    required => 0,
    writer   => '_schema',
);

has context => (
    isa       => 'Defined',
    predicate => 'has_context',
    writer    => '_context',
);

has custom_object_version_rs => (
    is => 'ro',
    isa => 'Defined',
    lazy => 1,
    builder => '_build_custom_object_version',
);

sub _build_custom_object_version {
    my $self = shift;
    return $self->build_resultset(
        'CustomObjectVersion',
        undef,
        {
            join     => 'custom_object_id',
            prefetch => 'custom_object_version_content_id',
        },
    );
}

sub add_context {
    my ($self, $context) = @_;

    if (blessed $context && $context->isa('Zaaksysteem::Model::DB::Zaak')) {
        my $values = $context->field_values(
            {
                use_magic_string_keys => 1,
                apply_field_filters   => 1
            }
        );
        $self->_context($values);
        $self->_schema($context->result_source->schema);
        return 1;
    }
    return 0;

}

sub _resolve_simple_value {
    my ($self, $value) = @_;

    my $ref = ref $value->[0];

    return join(" ", @$value) unless $ref;

    if ($ref ne 'HASH') {
        return 'Unsupported value, unable to serialize';
    }

    my @rv;

    foreach my $object (@$value) {

        if (($object->{type} // '') eq 'custom_object') {
            push(@rv,$object->{label});
        }

        if (($object->{type} // '') eq 'organization') {
            push(@rv,$object->{label});
        }

        if (($object->{type} // '') eq 'person') {
            push(@rv,$object->{label});
        }
    }

    return join(" ", @rv);
}

my $root_path = qr/^\$\.attributes\.custom_fields\./;

sub resolve_jpath {
    my ($self, $path) = @_;

    return '' unless $self->has_context;
    return '' if $path !~ m/$root_path/;

    $path =~ s/$root_path//;

    my ($magic_string, $what) = split(/\./, $path);

    my $value = $self->context->{$magic_string};

    return '' unless defined $value;
    return '' unless defined $value->[0];

    return $self->_resolve_simple_value($value) if $what eq 'value';

    # If we here.. assume relationship objects
    $path =~ s/^$magic_string\.//;
    return $self->_resolve_relationship_objects($value->[0], $path);
}

my $object_path = qr/^attributes\.custom_fields\./;

sub _resolve_relationship_objects {
    my ($self, $object, $jpath) = @_;

    return '' unless $jpath =~ m/$object_path/;

    $jpath =~ s/$object_path/\$./;

    my $ref = ref $object;
    if ($ref ne 'HASH') {
        return 'Unsupported value, unable to serialize';
    }

    if ($object->{type} eq 'custom_object') {
        return $self->_resolve_custom_object($object->{id}, $jpath);

    }
}

sub _find_custom_object_version {
    my ($self, $object_id) = @_;

    return $self->custom_object_version_rs->search_rs(
        { 'custom_object_id.uuid' => $object_id },
        {
            order_by => { '-desc' => 'version' },
            rows     => 1,
        },
    )->single;
}

sub _resolve_custom_object {
    my ($self, $object_id, $path) = @_;

    my $version_object = $self->_find_custom_object_version($object_id);
    return '' unless $version_object;

    my $co_content = $version_object->custom_object_version_content_id;
    return '' unless $co_content;

    my $data = $co_content->custom_fields;
    return '' unless $data;

    my $jpath = JSON::Path->new($path);
    return $jpath->value(decode_json($data));
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 DESCRIPTION

=head1 SYNOPSIS

    use Zaaksysteem::ZTT::JSONPath;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2021, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.

