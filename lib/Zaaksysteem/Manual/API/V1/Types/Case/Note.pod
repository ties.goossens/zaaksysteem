=head1 NAME

Zaaksysteem::Manual::API::V1::Types::Case::Note - Type definition for case notes

=head1 DESCRIPTION

This page documents the serialization of C<case/note> objects.

=head1 JSON

=begin javascript

{
    "type": "case/note",
    "reference": "f88d4300-0c93-4cbc-b4bd-08143db3e96e",
    "instance": {
        "case": {
            "instance": null,
            "reference": "14d4da69-e746-452f-8f79-82e09ba58f07",
            "type": "case"
        },
        "content": "note content",
        "date_created" : "2017-03-02T10:14:07Z",
        "date_modified" : "2017-03-02T10:35:45Z",
        "owner_id": "61d6851c-6c7a-4d0d-9b90-b2639283d6e5"
    }
}

=end javascript

=head1 INSTANCE ATTRIBUTES

=head2 case E<raquo> L<C<reference>|Zaaksysteem::Manual::API::V1::ValueTypes/reference>

Reference to the case the note is related to.

=head2 content E<raquo> L<C<string>|Zaaksysteem::Manual::API::V1::ValueTypes/string>

The content of the note.

=head2 date_created E<raquo> L<C<datetime>|Zaaksysteem::Manual::API::V1::ValueTypes/datetime>

Object creation timestamp.

=head2 date_modified E<raquo> L<C<datetime>|Zaaksysteem::Manual::API::V1::ValueTypes/datetime>

Object modification timestamp.

=head2 owner_id E<raquo> L<C<uuid>|Zaaksysteem::Manual::API::V1::ValueTypes/uuid>

UUID of the subject (employee) that owns this note.

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
