
BEGIN;

  ALTER TABLE export_queue DROP CONSTRAINT export_queue_filestore_id_fkey;
  ALTER TABLE export_queue ADD CONSTRAINT export_queue_filestore_id_fkey FOREIGN KEY (filestore_id) REFERENCES filestore(id) ON DELETE CASCADE;

  ALTER TABLE export_queue DROP CONSTRAINT export_queue_filestore_uuid_fkey;
  ALTER TABLE export_queue ADD CONSTRAINT export_queue_filestore_uuid_fkey FOREIGN KEY (filestore_uuid) REFERENCES filestore(uuid) ON DELETE CASCADE;
COMMIT;
