// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import composedReducerModule from './../../../api/resource/composedReducer';
import template from './template.html';
import './styles.scss';

export default angular
  .module('shared.ui.zsTimelineEvent', [composedReducerModule])
  .component('zsTimelineEvent', {
    bindings: {
      event: '&',
    },
    controller: [
      '$scope',
      'composedReducer',
      function (scope, composedReducer) {
        let ctrl = this,
          eventReducer = composedReducer({ scope }, ctrl.event).reduce(
            (event) => event
          );

        ctrl.getSubject = () => eventReducer.data().subject;

        ctrl.getDate = () => eventReducer.data().date;

        ctrl.getTitle = () => eventReducer.data().title;

        ctrl.getContent = () => eventReducer.data().content;

        ctrl.getIcon = () => eventReducer.data().icon;

        ctrl.getLink = () => eventReducer.data().link;

        ctrl.getColor = () => eventReducer.data().color;
      },
    ],
    template,
  }).name;
