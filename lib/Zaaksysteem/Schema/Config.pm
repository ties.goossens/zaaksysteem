use utf8;
package Zaaksysteem::Schema::Config;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Zaaksysteem::Schema::Config

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<Zaaksysteem::Result>

=cut

use base 'Zaaksysteem::Result';

=head1 TABLE: C<config>

=cut

__PACKAGE__->table("config");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'config_id_seq'

=head2 parameter

  data_type: 'varchar'
  is_nullable: 1
  size: 128

=head2 value

  data_type: 'text'
  is_nullable: 1

=head2 advanced

  data_type: 'boolean'
  default_value: true
  is_nullable: 0

=head2 uuid

  data_type: 'uuid'
  default_value: uuid_generate_v4()
  is_nullable: 1
  size: 16

=head2 definition_id

  data_type: 'uuid'
  is_nullable: 0
  size: 16

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "config_id_seq",
  },
  "parameter",
  { data_type => "varchar", is_nullable => 1, size => 128 },
  "value",
  { data_type => "text", is_nullable => 1 },
  "advanced",
  { data_type => "boolean", default_value => \"true", is_nullable => 0 },
  "uuid",
  {
    data_type => "uuid",
    default_value => \"uuid_generate_v4()",
    is_nullable => 1,
    size => 16,
  },
  "definition_id",
  { data_type => "uuid", is_nullable => 0, size => 16 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 UNIQUE CONSTRAINTS

=head2 C<config_definition_id_key>

=over 4

=item * L</definition_id>

=back

=cut

__PACKAGE__->add_unique_constraint("config_definition_id_key", ["definition_id"]);

=head2 C<parameter_unique>

=over 4

=item * L</parameter>

=back

=cut

__PACKAGE__->add_unique_constraint("parameter_unique", ["parameter"]);


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2018-07-05 13:29:28
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:ximxvDQG4802RULfLoghpQ

__PACKAGE__->resultset_class('Zaaksysteem::Backend::Config::ResultSet');

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

