
BEGIN;
    ALTER TABLE transaction_record DROP CONSTRAINT
    transaction_record_transaction_id_fkey;

    ALTER TABLE transaction_record ADD CONSTRAINT
    transaction_record_transaction_id_fkey FOREIGN KEY (transaction_id)
    REFERENCES transaction(id) ON DELETE CASCADE DEFERRABLE;

    ALTER TABLE transaction_record_to_object DROP CONSTRAINT
    transaction_record_to_object_transaction_record_id_fkey;

    ALTER TABLE transaction_record_to_object
    ADD CONSTRAINT transaction_record_to_object_transaction_record_id_fkey
    FOREIGN KEY (transaction_record_id)
    REFERENCES transaction_record(id)
    ON DELETE CASCADE DEFERRABLE;

COMMIT;
