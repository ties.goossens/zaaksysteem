package Zaaksysteem::Backend::Sysin::Modules::Roles::JSON;

use Moose::Role;

use BTTW::Tools;
use Data::Dumper;

use JSON;


sub _process_validate_input {
    my $self            = shift;
    my $transaction     = shift;

    ### We do not allow string CSV
    throw(
        'sysin/json/invalid_input_format',
        'Invalid input format, we only allow input_data'
    ) if (
        !$transaction->input_data
    );

    eval {
        JSON->new->utf8(0)->decode($transaction->input_data);
    };

    if ($@) {
        throw(
            'sysin/json/invalid_json',
            'Invalid json format, got error: ' . $@
        );
    }
}

sub _process_get_next_row {
    my $self        = shift;

    $self->_json_load_stash();

    my $csv         = $self->process_stash->{csv}->{parser};
    my $fh          = $self->process_stash->{csv}->{fh};

    return unless exists(
        $self->process_stash->{json}->{results}->[
            $self->process_stash->{json}->{index}
        ]
    );

    my $row         = $self->process_stash->{json}->{results}->[
        $self->process_stash->{json}->{index}++
    ];

    return ($row, Data::Dumper::Dumper($row));
}

sub _json_load_stash {
    my $self        = shift;

    return if $self->process_stash->{json}->{index};

    my $input_data  = JSON->new->utf8(0)->decode(
        $self
        ->process_stash
        ->{transaction}
        ->input_data
    );

    $self->process_stash->{json}->{index}   = 0;

    $self->process_stash->{json}->{results} = (
        UNIVERSAL::isa($input_data->{results}, 'ARRAY')
            ? $input_data->{results}
            : []
    );

}

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

