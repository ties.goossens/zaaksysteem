package Zaaksysteem::Controller::API::v1::Controlpanel::Instance;

use Moose;
use namespace::autoclean;

BEGIN { extends 'Zaaksysteem::API::v1::Controller' }

=head1 NAME

Zaaksysteem::Controller::API::v1::Controlpanel::Instance - APIv1 controller for Controlpanel objects

=head1 DESCRIPTION

This is the controller API class for
C<api/v1/controlpanel/[CONTROLPANEL_UUID]/instance>. Extensive documentation
about this API can be found in
L<Zaaksysteem::Manual::API::V1::Controlpanel::Instance>

Extensive tests about the usage via the JSON API can be found in:

L<TestFor::Catalyst::Controller::API::V1::Controlpanel::Instance>

=cut

use BTTW::Tools;
use Crypt::SaltedHash;
use Zaaksysteem::BR::Controlpanel qw/CONTROLPANEL_VALIDATION_PROFILES/;
use Zaaksysteem::Object::Types::Instance;
use Zaaksysteem::Types qw[
    FQDN
    CustomerType
    Otap
    Timestamp
    Host
    IPv4
    SoftwareVersions
];

sub BUILD {
    my $self = shift;

    $self->add_api_control_module_type('controlpanel');
    $self->add_api_context_permission('extern', 'allow_pip');
}

=head1 ACTIONS

=head2 base

Reserves the C</api/v1/controlpanel/[CONTROLPANEL_UUID]/instance> routing namespace.

=cut

sub base : Chained('/api/v1/controlpanel/instance_base') : PathPart('instance') : CaptureArgs(0) {
    my ($self, $c)      = @_;

    my $zql             = 'SELECT {} FROM instance where owner="' . $c->stash->{controlpanel}->get_object_attribute('owner')->value . '" ORDER BY label';

    $c->stash->{zql}    = Zaaksysteem::Search::ZQL->new($zql);

    my $set = try {
        return Zaaksysteem::API::v1::ResultSet->new(
            iterator => $c->model('Object')->zql_search($zql)->rs
        );
    } catch {
        $c->log->warn($_);

        throw(
            'api/v1/controlpanel/instance',
            'API configuration error, unable to continue.'
        );
    };

    $c->stash->{ instance_set }     = $set;
    $c->stash->{ instances }        = $set->build_iterator;
}

=head2 instance_base

Reserves the C</api/v1/controlpanel/[CONTROLPANEL_UUID]/instance/[INSTANCE_UUID]> routing namespace.

=cut

sub instance_base : Chained('base') : PathPart('') : CaptureArgs(1) {
    my ($self, $c, $uuid) = @_;

    # Retrieve case via Object model so we can benefit from builtin ACL stuff
    $c->stash->{ instance } = try {
        return $c->stash->{ instances }->find($uuid);
    } catch {
        $c->log->warn($_);

        throw('api/v1/controlpanel/instance/not_found', sprintf(
            "The controlpanel instance object with UUID '%s' could not be found.",
            $uuid
        ), { http_code => 404 });
    };

    unless (defined $c->stash->{ instance } && $c->stash->{ instance }->object_class eq 'instance') {
        throw('api/v1/controlpanel/instance/not_found', sprintf(
            "The controlpanel instance object with UUID '%s' could not be found.",
            $uuid
        ), { http_code => 404 });
    }
}

=head2 list

=head3 URL Path

C</api/v1/controlpanel/[CONTROLPANEL_UUID]/instance>

=cut

sub list : Chained('base') : PathPart('') : Args(0) : RO {
    my ($self, $c) = @_;

    $c->stash->{instance_set}->rows_per_page(100);
    $c->stash->{ result } = $c->stash->{ instance_set }->init_paging($c->request);
}

=head2 get

=head3 URL Path

C</api/v1/controlpanel/[CONTROLPANEL_UUID]/instance/[INSTANCE_UUID]>

=cut

sub get : Chained('instance_base') : PathPart('') : Args(0) : RO {
    my ($self, $c) = @_;

    $c->stash->{ result } = $c->stash->{ instance };
}

=head2 create

=head3 URL Path

C</api/v1/controlpanel/[CONTROLPANEL_UUID]/instance/create>

=cut

define_profile create => (
    %{ CONTROLPANEL_VALIDATION_PROFILES->{'instance/create'} }
);

sub create : Chained('base') : PathPart('create') : Args(0) : RW {
    my ($self, $c) = @_;

    ## Pip restrictions
    my %request_params = %{ $c->req->params };

    if ($c->session->{pip}) {
        delete $request_params{software_version};
        delete $request_params{customer_type};
    }

    ### All validation and checks of existing hostnames in one validation profile
    my $params  = assert_profile({
        %request_params,
        _objectmodel => $c->model('Object'),
        _controlpanel => $c->stash->{controlpanel},
    })->valid;

    my $instance;
    try {
        $c->model('DB')->txn_do(sub {
            $instance = $self->_create_instance_object(
                $c->model('Object'),
                {
                    controlpanel        => $c->stash->{controlpanel},
                    %$params
                },
                $c
            );
        });
    } catch {
        throw('api/v1/controlpanel/instance/fault', sprintf(
            'There was a problem creating this controlpanel instance object: %s',
            $_
        ));
    };

    $c->stash->{result} = $c->model('DB::ObjectData')->find({ uuid => $instance->id });
}

=head2 update

=head3 URL Path

C</api/v1/controlpanel/[CONTROLPANEL_UUID]/instance/[CONTROLPANEL_UUID]/update>

=cut

define_profile update => (
    %{ CONTROLPANEL_VALIDATION_PROFILES->{'instance/update'} },
    missing_optional_valid => 1
);

sub update : Chained('instance_base') : PathPart('update') : Args(0) : RW {
    my ($self, $c) = @_;

    ## Pip restrictions
    my %request_params = %{ $c->req->params };

    if ($c->session->{pip}) {
        delete $request_params{software_version};
        delete $request_params{customer_type};
    }

    my $params  = assert_profile(
        {
            %request_params,
            _controlpanel => $c->stash->{controlpanel},
            _objectmodel  => $c->model('Object'),
            _current_instance => $c->stash->{instance}
        }
    )->valid;

    $self->log->debug(sprintf(
            "Updating instance %s, with params %s, profiles to %s",
            ($c->stash->{instance} ? $c->stash->{instance}->uuid : 'undefined'), dump_terse($c->req->params), dump_terse($params)));

    if (keys %$params) {
        $c->model('DB')->txn_do(
            sub {
                $self->_update_instance_object($c->model('Object'), $c->stash->{instance}, $c->user, $params);
            }
        );
    }

    $c->stash->{result} = $c->model('DB::ObjectData')->find({ uuid => $c->stash->{instance}->uuid });
}

define_profile _update_instance_object => (
    optional    => {
        fqdn                => 'Str',   # Better verification in previous call ("update")
        label               => 'Str',
        template            => 'Str',
        protected           => 'Bool',
        api_domain          => FQDN,
        services_domain     => FQDN,
        mail                => 'Bool',
        delete_on           => Timestamp,
        provisioned_on      => Timestamp,
        network_acl         => IPv4,
        database_provisioned  => Timestamp,
        filestore_provisioned => Timestamp,
        disabled            => 'Bool',
        password            => 'Any',
        freeform_reference  => 'Str',
        software_version     => SoftwareVersions,
        customer_type       => CustomerType,
    },
    missing_optional_valid => 1
);

sub _update_instance_object {
    my $self                    = shift;
    my $objectmodel             = shift;
    my $instance                = shift;
    my $user                    = shift;
    my $params                  = assert_profile(shift || {})->valid;

    my $object                  = $objectmodel->retrieve(uuid => $instance->id);

    ### Convert password to hashed version
    if ($params->{password}) {
        my $csh             = Crypt::SaltedHash->new(algorithm => 'SHA-1');

        $csh->add($params->{password});
        $params->{password} = $csh->generate();
    }

    ### Make sure only a logged in user can protect a controlpanel instance
    if ($object->protected && !$user) {
        throw(
            'api/v1/controlpanel/instance/cannot_update_protected_instance',
            'protected controlpanel instance cannot be updated'
        );
    }

    for my $param (grep { exists $params->{$_} } keys %$params) {
        my $clearer = '_clear_' . $param;
        if (!defined $params->{ $param } && $object->can($clearer)) {
            $object->$clearer();
            next;
        }
        $object->$param($params->{ $param });
    }

    $objectmodel->save(object => $object);
}

define_profile _create_instance_object => (
    required    => {
        controlpanel        => 'Zaaksysteem::Backend::Object::Data::Component',
        label               => 'Str',
        otap                => Otap,
    },
    optional => {
        customer_type => CustomerType,
        freeform_reference => 'Str',
        software_version => SoftwareVersions,
        template => 'Str',
        password => 'Str',
        protected => 'Bool',
        api_domain => FQDN,
        services_domain => FQDN,
        mail => 'Bool',
        host => Host,
        fqdn => FQDN,
        network_acl => IPv4
    },
    require_some => {
        host_or_fqdn => [1, qw/host fqdn/],
    }
);

sub _create_instance_object {
    my $self                    = shift;
    my $objectmodel             = shift;

    my $params                  = assert_profile(shift || {})->valid;
    my $c                       = shift;

    ### Convert password to hashed version
    if ($params->{password}) {
        my $csh             = Crypt::SaltedHash->new(algorithm => 'SHA-1');

        $csh->add($params->{password});
        $params->{password} = $csh->generate();
    }

    my $i = Zaaksysteem::Object::Types::Instance->new(
        owner           => $params->{controlpanel}->get_object_attribute('owner')->value,
        template        => $params->{controlpanel}->get_object_attribute('template')->value,
        customer_type   => $params->{controlpanel}->get_object_attribute('customer_type')->value,
        %$params
    );

    ### Every controlpanel gets a default ACL for behandelaar
    my ($behandelaar) = grep ({ $_->system_role && $_->name eq 'Behandelaar' } @{ $c->model('DB::Roles')->get_all_cached($c->stash) });
    $i->permit($behandelaar, qw/read write/);

    return $objectmodel->save(
        object => $i
    );

}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
