package Zaaksysteem::Zaken::Roles::API;

use Moose::Role;
use Zaaksysteem::Object::Types::Case::Milestone;
use Zaaksysteem::Object::Types::Case::Result;
use Zaaksysteem::Object::Types::Case::Route;
use Zaaksysteem::Types::MappedString;
use BTTW::Tools;
use List::Util qw(all);
use Zaaksysteem::Constants qw(ZAAKSYSTEEM_CONSTANTS);

requires qw(log);

sub as_api_v1_output {
    my ($self) = @_;

    my $uuid = $self->get_column('uuid');

    return {
        type      => 'case',
        reference => $uuid,

        preview   => 'case(...' . substr($uuid, -6) . ')',


        instance  => {
            id              => $uuid,
            number          => $self->id,
            number_master   => $self->master_number,
            number_parent   => $self->get_column('pid'),
            number_previous => $self->get_column('vervolg_van'),

            # VERY expensive calls
            $self->_get_involved,

            # Relatively expensive calls
            relations          => $self->case_case_relation,
            case_relationships => $self->case_case_relationships,

            aggregation_scope => "Dossier",

            subject          => $self->onderwerp,
            subject_external => $self->onderwerp_extern,

            status => $self->status,

            date_created         => $self->created             // undef,
            date_modified        => $self->last_modified       // undef,
            date_destruction     => $self->vernietigingsdatum  // undef,
            date_of_completion   => $self->afhandeldatum       // undef,
            date_of_registration => $self->registratiedatum    // undef,
            date_target          => $self->streefafhandeldatum // undef,

            payment_status => $self->payment_status,
            price          => $self->dutch_price,

            channel_of_contact => $self->contactkanaal,

            stalled_until => $self->stalled_until // undef,
            stalled_since => $self->stalled_since // undef,

            suspension_rationale => $self->suspension_rationale // undef,

            $self->_get_case_result_things,

            $self->case_location,

            archival_state => $self->archival_state // undef,

            route => $self->case_route,

            phase => $self->case_phase // undef,

            premature_completion_rationale => $self->premature_completion_rationale // undef,

            milestone => $self->case_milestone // undef,
            outcome   => $self->case_outcome   // undef,

            casetype  => $self->case_casetype,

            confidentiality => $self->case_confidentiality->TO_JSON,
            attributes      => $self->case_custom_attibutes,

            current_deadline => $self->zaak_meta->current_deadline // {},
            deadline_timeline  => $self->zaak_meta->deadline_timeline // [],
        }
    };

}

sub _get_involved {
    my $self = shift;

    return (
        requestor   => $self->get_involved_entity_by_type('aanvrager') // undef,
        assignee    => $self->get_involved_entity_by_type('behandelaar') // undef,
        coordinator => $self->get_involved_entity_by_type('coordinator') // undef,
    );
}

sub _get_case_result_things {
    my $self        = shift;
    my $case_result = $self->zaaktype_resultaat;

    return (
        result                => undef,
        result_id             => undef,
        active_selection_list => undef,
    ) unless $case_result;

    return (
        result                => $case_result->resultaat,
        result_id             => $case_result->id,
        active_selection_list => $case_result->selectielijst,
    );
}

=head2 get_involved_entity_by_type

Get the entity for the case by type. This is an expensive call: 50 ms per type.

=cut

sub get_involved_entity_by_type {
    my ($self, $type) = @_;

    my $id = $self->get_column($type);
    return undef unless $id;

    # Let's do without the magic that is called betrokkene model
    my $involved = $self->result_source->schema->resultset('ZaakBetrokkenen')
        ->search_rs(
        { id   => $id },
        { rows => 1, columns => [qw(subject_id betrokkene_type)] }
    )->first;
    my $subject_id = $involved->subject_id;

    my $resultset_name;
    my $btype = $involved->betrokkene_type;
    if ($btype eq 'natuurlijk_persoon') {
        $resultset_name = "NatuurlijkPersoon",
    }
    elsif ($btype eq 'bedrijf') {
        $resultset_name = "Bedrijf",
    }
    else {
        $resultset_name = "Subject",
    }
    my $rs = $self->result_source->schema->resultset($resultset_name)
        ->search_rs({ uuid => $subject_id });
    my $subject = $rs->first;
    return undef unless $subject;
    return $subject->as_object;
}

sub case_custom_attibutes {
    my $self  = shift;

    my $attrs = $self->get_custom_attributes;

    my %result;
    my $value;
    while (my $attr = $attrs->next) {
        $value = $attr->value_v0->[0];
        if ($value->{bibliotheek_kenmerken_type} eq 'file') {
            my @documents;
            foreach (@{$value->{value}}) {
                push(@documents, $_->{uuid});
            }
            $result{ $attr->name } = \@documents;
            next;
        }
        $result{ $attr->name } = [ $value->{value} ];
    }

    my $empty = $self->get_custom_attributes_from_casetype;
    $empty = $empty->search_rs(
        {

            'bibliotheek_kenmerken_id.magic_string' => {
                -not_in => [ keys %result ]
            },
        },
        {
            join => 'bibliotheek_kenmerken_id',
        },
    );

    # Hand craft missing pieces which are found in object data but not in
    # case_properties. We should update all cases to get all the empty
    # attributes
    while (my $attr = $empty->next) {
        my $attribute = $attr->bibliotheek_kenmerken_id;

        if ($attribute->value_type eq 'file' || $attribute->type_multiple) {
            $result{ $attribute->magic_string } = [ ];
        }
        else {
            $result{ $attribute->magic_string } = [ undef ];
        }
    }

    return \%result;
}

sub case_location {
    my $self = shift;

    return (
        # For some reason this isn't in the api/v1 output
        #correspondence_location => $self->locatie_correspondentie,
        #case_location           => $self->locatie_zaak,
        #
        correspondence_location => undef,
        case_location           => undef,
    );
}

sub case_milestone {
    my $self = shift;

    # Current phase should always be > 1
    my $current_phase = $self->volgende_fase;

    # Milestone should always be > 0
    my $previous_phase = $self->huidige_fase;
    my $resolve_phase  = $self->afhandel_fase;

    return Zaaksysteem::Object::Types::Case::Milestone->new(
        milestone_label           => $previous_phase->naam,
        milestone_sequence_number => $previous_phase->status,
        $current_phase ? (
            phase_label               => $current_phase->fase,
            phase_sequence_number     => $current_phase->status,
        ) : (
            phase_label => $resolve_phase->fase,
            phase_sequence_number => $resolve_phase->status,
        ),
        last_sequence_number      => $resolve_phase->status
    );

}

sub case_case_relation {
    my $self = shift;
    my $rs   = $self->result_source->schema->resultset('CaseRelation')
        ->search({ case_id => $self->id });

    my @relations;
    while (my $rel = $rs->next) {
        my $case
            = $rel->get_column('case_id_a') == $self->id
            ? $rel->case_id_b
            : $rel->case_id_a;
        push(@relations, $case->case_reference) unless $case->is_deleted;
    }
    return {
        instance => {
            type => 'set',
            rows => \@relations,
        }
    };
}

sub case_reference {
    my $self = shift;
    return if $self->is_deleted;
    return { reference => $self->get_column('uuid'), type => 'case' };
}

sub case_route {
    my $self = shift;

    my $group = $self->case_route_group;
    my $role  = $self->case_route_role;

    return Zaaksysteem::Object::Types::Case::Route->new(
        $group ? (group => $group->as_object) : (),
        $role  ? (role  => $role->as_object)  : (),
    );
}

sub case_route_group {
    my $self = shift;
    if (my $id = $self->route_ou) {
        return $self->result_source->schema->resultset('Groups')->find($id);
    }
    return;
}

sub case_route_role {
    my $self = shift;
    if (my $id = $self->route_role) {
        return $self->result_source->schema->resultset('Roles')->find($id);
    }
    return;
}

sub case_case_relationships {
    my $self = shift;
    return {
        child  => $self->case_children // undef,
        parent => $self->case_parent   // undef,
    };
}

sub case_children {
    my $self = shift;

    my $rs = $self->zaak_children->search_rs({ 'me.deleted' => undef });
    my @child;
    while (my $case = $rs->next) {
        push(@child, $case->case_reference);
    }
    return {
        instance => {
            type => 'set',
            rows => \@child,
        }
    };
}

sub case_parent {
    my $self   = shift;
    return unless $self->has_parent;
    return $self->get_parent->case_reference;
}

sub case_casetype {
    my $self     = shift;
    my $zaaktype = $self->zaaktype_id;
    my $node     = $self->zaaktype_node_id;
    return {
        instance => {
            name    => $node->titel,
            version => $node->version,
        },
        preview   => $node->titel,
        reference => $zaaktype->get_column('uuid'),
        type      => 'casetype',
    };
}

sub case_confidentiality {
    my $self = shift;

    my $setting = $self->confidentiality;
    return Zaaksysteem::Types::MappedString->new(
        original => $setting,
        mapped   => ZAAKSYSTEEM_CONSTANTS->{confidentiality}{$setting}
    );
}

sub case_outcome {
    my $self = shift;

    my $zt_result = try { $self->get_zaaktype_result };

    return unless $zt_result;

    my %args = (
        name                 => ($zt_result->label || $zt_result->resultaat),
        result               => ($zt_result->resultaat),
        dossier_type         => $zt_result->dossiertype,
        archival_type        => $zt_result->archiefnominatie,
        retention_period     => $zt_result->bewaartermijn,
        selection_list       => $zt_result->selectielijst,
        selection_list_start => $zt_result->selectielijst_brondatum,
        selection_list_end   => $zt_result->selectielijst_einddatum
    );

    return Zaaksysteem::Object::Types::Case::Result->new(
        map { $_ => $args{$_} } grep { defined $args{$_} } keys %args);
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2019, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
