package Zaaksysteem::Search::ZQL::Role::ResultSet;
use Moose::Role;

requires qw(apply_zql_roles);

=head1 DESCRIPTION

=head1 SYNOPSIS

=cut

=head1 ATTRIBUTES

=head2 describe_rows(BOOLEAN)

When set, it will describe the rows on JSON output

=cut

has describe_rows => (
    is      => 'rw',
    isa     => 'Bool',
    trigger => sub {
        my ($self, $val) = @_;

        $self->_set_zql_options('describe_rows', $val);
    }
);

has hydrate_actions => (
    is      => 'rw',
    isa     => 'Bool',
    trigger => sub {
        my ($self, $val) = @_;
        $self->_set_zql_options('include_row_actions', $val);
    }
);

=head2 object_requested_attributes($ARRAY)

Will let TO_JSON know which attributes to return

=cut

has object_requested_attributes => (
    is => 'rw',
    trigger => sub {
        my ($self, $val) = @_;
        $self->_set_zql_options('requested_attributes', $val);
    }
);

sub _set_zql_options {
    my ($self, $option, $val) = @_;
    return $self->{attrs}{_zql_options}{$option} = $val;
}

around [qw(single next find)] => sub {
    my ($orig, $self, @args) = @_;
    my $row = $self->$orig(@args);
    $self->apply_zql_roles($row);
    return $row;
};

around all => sub {
    my ($orig, $self, @args) = @_;
    my @rows = $self->$orig(@args);
    $self->apply_zql_roles(@rows);
    return @rows;
};

=head2 search

=head2 search_rs

Wrappers around "search" on the resultset that preserve the "describe_rows" flag.

=cut

around search => sub {
    my $orig = shift;
    my $self = shift;

    my $rs = $self->search_rs();
    return $rs->$orig(@_);
};

around search_rs => sub {
    my $orig = shift;
    my $self = shift;

    my $rv = $self->$orig(@_);

    $rv->describe_rows($self->describe_rows);

    return $rv;
};

1;

__END__


=head1 COPYRIGHT and LICENSE

Copyright (c) 2021, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
