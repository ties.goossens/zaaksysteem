use utf8;
package Zaaksysteem::Schema::CaseDocuments;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Zaaksysteem::Schema::CaseDocuments

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<Zaaksysteem::Result>

=cut

use base 'Zaaksysteem::Result';
__PACKAGE__->table_class("DBIx::Class::ResultSource::View");

=head1 TABLE: C<case_documents>

=cut

__PACKAGE__->table("case_documents");
__PACKAGE__->result_source_instance->view_definition(" SELECT z.id AS case_id,\n    (COALESCE(array_agg(fs.uuid) FILTER (WHERE (fs.uuid IS NOT NULL)), '{}'::uuid[]))::text[] AS value,\n    bk.magic_string,\n    bk.id AS library_id\n   FROM (((zaak z\n     JOIN zaaktype_kenmerken ztk ON ((z.zaaktype_node_id = ztk.zaaktype_node_id)))\n     JOIN bibliotheek_kenmerken bk ON (((ztk.bibliotheek_kenmerken_id = bk.id) AND (bk.value_type = 'file'::text))))\n     LEFT JOIN (file_case_document fcd\n     JOIN (file f\n     JOIN filestore fs ON ((f.filestore_id = fs.id))) ON ((fcd.file_id = f.id))) ON (((z.id = fcd.case_id) AND (bk.id = fcd.bibliotheek_kenmerken_id))))\n  GROUP BY z.id, bk.magic_string, bk.id");

=head1 ACCESSORS

=head2 case_id

  data_type: 'integer'
  is_nullable: 1

=head2 value

  data_type: 'text[]'
  is_nullable: 1

=head2 magic_string

  data_type: 'text'
  is_nullable: 1

=head2 library_id

  data_type: 'integer'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "case_id",
  { data_type => "integer", is_nullable => 1 },
  "value",
  { data_type => "text[]", is_nullable => 1 },
  "magic_string",
  { data_type => "text", is_nullable => 1 },
  "library_id",
  { data_type => "integer", is_nullable => 1 },
);


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2021-08-18 13:40:22
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:0i+7e/JtE/BZxlMFyL2ePg


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2021, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
