#!/bin/bash

if [ $# -lt "1" ]; then
    echo "USAGE: $0 databasename"
    exit;
fi

DATABASE=$1;

DBFILLED=`echo "SELECT * FROM roles " | psql $DATABASE| grep Behandelaar`

if [ "$DBFILLED" != "" ]; then
    echo "USERS IN $DATABASE ALREADY LOADED";
    exit 1;
fi
