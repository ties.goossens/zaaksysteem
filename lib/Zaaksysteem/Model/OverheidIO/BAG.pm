package Zaaksysteem::Model::OverheidIO::BAG;
use Moose;
use namespace::autoclean;

extends 'Catalyst::Model::Factory::PerRequest';

__PACKAGE__->config(
    class => 'Zaaksysteem::OverheidIO::BAG',
    constructor => 'new'
);

=head1 NAME

Zaaksysteem::Model::OverheidIO - Catalyst model factory for
L<Zaaksysteem::OverheidIO::Model>.

=head1 SYNOPSIS

    my $overheidio = $c->model('OverheidIO');

=head1 METHODS

=head2 prepare_arguments

Prepares the arguments for L<<Zaaksysteem::Object::Queue::Model->new>>.

=cut

sub prepare_arguments {
    my ($self, $c) = @_;

    return {
        schema => $c->model('DB')->schema,
    };
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
