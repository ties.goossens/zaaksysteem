// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular.module('Zaaksysteem.case').directive('zsCaseWebformObjectTypeField', [
    function () {
      var addEventListener = window.zsFetch(
          'nl.mintlab.utils.events.addEventListener'
        ),
        removeEventListener = window.zsFetch(
          'nl.mintlab.utils.events.removeEventListener'
        ),
        cancelEvent = window.zsFetch('nl.mintlab.utils.events.cancelEvent');

      return {
        scope: true,
        require: [
          'zsCaseWebformObjectTypeField',
          '^zsCaseWebformField',
          '^zsCaseView',
        ],
        controller: [
          '$scope',
          '$attrs',
          '$element',
          function ($scope, $attrs, $element) {
            var ctrl = this,
              controls = [],
              zsCaseWebformField,
              zsCaseView,
              objectType,
              rights,
              loading;

            function onChange(event) {
              cancelEvent(event);
            }

            ctrl.setControls = function () {
              zsCaseWebformField = arguments[0];
              zsCaseView = arguments[1];

              objectType = $scope.$eval(
                $attrs.zsCaseWebformObjectTypeFieldObject
              );
              rights = $scope.$eval($attrs.objectRights);

              zsCaseWebformField.setSetter(function (value) {
                ctrl.mutations = value ? angular.copy(value) : {};
              });

              zsCaseWebformField.setGetter(function () {
                return ctrl.mutations;
              });
            };

            ctrl.addControl = function (child) {
              controls.push(child);
            };

            ctrl.removeControl = function (child) {
              _.pull(controls, child);
            };

            ctrl.getObjectType = function () {
              return objectType;
            };

            ctrl.getObjectTypeId = function () {
              var objectTypeId;

              if (objectType) {
                objectTypeId = objectType.id;
              }

              return objectTypeId;
            };

            ctrl.getObjectTypePrefix = function () {
              var prefix;

              if (objectType) {
                prefix = objectType.values.prefix;
              }

              return prefix;
            };

            ctrl.getObjectTypeName = function () {
              var name;

              if (objectType) {
                name = objectType.values.name;
              }

              return name;
            };

            ctrl.addMutation = function (mutation) {
              loading = true;

              ctrl.mutations.push(mutation);
              return zsCaseView
                .addMutation(ctrl.getObjectTypePrefix(), mutation)
                ['catch'](function () {
                  _.pull(ctrl.mutations, mutation);
                })
                ['finally'](function () {
                  loading = false;
                });
            };

            ctrl.removeMutation = function (mutation) {
              _.pull(ctrl.mutations, mutation);
              return zsCaseView.removeMutation(
                ctrl.getObjectTypePrefix(),
                mutation
              );
            };

            ctrl.updateMutation = function (mutation, values) {
              return zsCaseView.updateMutation(
                ctrl.getObjectTypePrefix(),
                mutation,
                values
              );
            };

            ctrl.canChange = function () {
              return $scope.$eval($attrs.zsCaseWebformObjectTypeFieldCanChange);
            };

            ctrl.hasPermission = function (type) {
              return parseInt(rights[type + '_object'], 10) === 1;
            };

            ctrl.getMetadata = function () {
              return rights;
            };

            ctrl.isLoading = function () {
              return loading;
            };

            ctrl.newObject = null;
            ctrl.mutations = [];

            addEventListener($element[0], 'change, keypress', onChange);

            $scope.$on('$destroy', function () {
              removeEventListener($element[0], 'change, keypress', onChange);
            });

            return ctrl;
          },
        ],
        controllerAs: 'caseWebformObjectTypeField',
        link: function (scope, element, attrs, controllers) {
          controllers[0].setControls.apply(
            controllers[0],
            controllers.slice(1)
          );
        },
      };
    },
  ]);
})();
