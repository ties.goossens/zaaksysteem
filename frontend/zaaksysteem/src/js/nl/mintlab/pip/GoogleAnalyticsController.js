// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular
    .module('Zaaksysteem.pip')
    .controller('nl.mintlab.pip.GoogleAnalyticsController', [
      '$scope',
      function ($scope) {
        $scope.googleAnalytics = function (id) {
          var w = window;
          var d = document;
          var l = 'dataLayer';

          w[l] = w[l] || [];
          w[l].push({
            'gtm.start': new Date().getTime(),
            event: 'gtm.js',
          });

          var f = d.getElementsByTagName('meta')[0];
          var j = d.createElement('script');

          j.async = true;
          j.src = 'https://www.googletagmanager.com/gtm.js?id=' + id;
          f.parentNode.insertBefore(j, f);
        };
      },
    ]);
})();
