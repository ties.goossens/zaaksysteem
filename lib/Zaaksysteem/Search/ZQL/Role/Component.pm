package Zaaksysteem::Search::ZQL::Role::Component;
use Moose::Role;

=head1 DESCRIPTION

=head1 SYNOPSIS

=cut

has _zql_options => (
    is        => 'rw',
    lazy      => 1,
    default   => sub { {}; },
    isa       => 'HashRef',
    predicate => 'has_zql_options',
);

sub _initialize_zql {
    my $self = shift;
    $self->_zql_options(shift)
}

sub get_zql_options {
    my $self = shift;
    my $key = shift;
    return unless $self->has_zql_options;
    return $self->_zql_options->{$key};
}

sub get_requested_columns {
    my ($self) = @_;
    return $self->get_zql_options('requested_attributes');
}

sub get_include_row_actions {
    my ($self) = @_;
    return $self->get_zql_options('include_row_actions');
}


1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2021, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
