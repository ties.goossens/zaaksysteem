package TestFor::General::Zaaktypen::Import;
use base qw(ZSTest);

use TestSetup;
use File::Spec::Functions;

sub zs_magic_strings_from_ztb : Tests {

    $zs->zs_transaction_ok(
        sub {
            my $ztn = $zs->zaaktype_importer->import_from_ztb(catfile(qw(t data zaaktypen Magic_strings.ztb)));
            isa_ok($ztn, "Zaaksysteem::Model::DB::ZaaktypeNode");

            my $attr_rs = $zs->schema->resultset('BibliotheekKenmerken')->search({magic_string => 'hoofdletter'});

            my $capital = $attr_rs->first;
            ok($capital, "has magicstring");

            my $name = Zaaksysteem::TestUtils::generate_random_string;
            $capital->update({ naam => $name });

            $ztn = $zs->zaaktype_importer->import_from_ztb(catfile(qw(t data zaaktypen Magic_strings.ztb)));
            is($attr_rs->count, 1);

        },
        "Make sure magic strings are matched and not names by automatic importer",
    );
}

sub zs_zaaktypen_import_from_ztb : Tests {
    my $self = shift;

    $zs->zs_transaction_ok(
        sub {
            _zaaktype_import_and_create_case_ok('Zaaktype_voor_email_parsing.ztb');
        },
        "Zaaktype import met simpel zaaktype"
    );

    $zs->zs_transaction_ok(
        sub {
            _zaaktype_import_and_create_case_ok('Vergunning.ztb');
            {
                my $rs = $zs->schema->resultset("BibliotheekCategorie");
                my $r  = $rs->search_rs({naam => "1.1 Ontwikkelen"});
                is($r->count, 1, "One entry found");
                my $c = $r->next;
                is($c->id, 8, "Has correct ID");
                is($c->pid->id, 3, "Has correct parent");
            }

            {
                my $rs = $zs->schema->resultset("BibliotheekSjablonen");
                my $r = $rs->search_rs({naam => "Brief"});

                is($r->count, 1, "One entry found");

                my $c = $r->next;

                is($c->id, 3, "Has correct id");
                is($c->bibliotheek_categorie_id->id, 3, "Has correct bibliotheek categorie ID");
                is($c->filestore_id->id, 12, "Has correct filestore");

                my $fs = $c->filestore_id;
                is_deeply(
                    {
                        md5            => $fs->md5,
                        mimetype       => $fs->mimetype,
                        original_name  => $fs->original_name,
                        size           => $fs->size,
                    },
                    {
                        md5            => '2da3830a9987dab01efea739e1232d22',
                        mimetype       => 'text/plain',
                        original_name  => 'Brief.odt',
                        size           => 10,
                    },
                    "Filestore is ok for brief"
                );
            }
        },
        "Zaaktype import met Voerendaal Vergunningen"
    );

    $zs->zs_transaction_ok(
        sub {
            _zaaktype_import_and_create_case_ok('7_Beveiligingsanalyse.ztb');
        },
        "Zaaktype import met authorisaties",
    );

}

sub _zaaktype_import_and_create_case_ok {
    my $filename = shift;

    my $ztn = $zs->zaaktype_importer->import_from_ztb(catfile(qw(t data zaaktypen), $filename));
    isa_ok($ztn, "Zaaksysteem::Model::DB::ZaaktypeNode");

    my $zt = $zs->schema->resultset('Zaaktype')->find({ zaaktype_node_id => $ztn->id });
    my $case = $zs->create_case_ok(zaaktype => $zt, delayed_touch => 1);
    isa_ok($case, "Zaaksysteem::Model::DB::Zaak");
    ok($case->id, "Zaak has an ID " . $case->id);
}

sub zs_zaaktypen_import : Tests {

    $zs->zs_transaction_ok(
        sub {
            my $importer = $zs->zaaktype_importer;
            my $kenmerk  = $zs->create_bibliotheek_kenmerk_ok(value_type => 'select');

            my $merged = $importer->merge_local_options(
                {
                    bibliotheek_kenmerken_id => $kenmerk->id,
                    remote_options           => []
                }
            );
            is scalar @$merged, 0, "No options put in, result is empty list";


            $merged = $importer->merge_local_options(
                {
                    bibliotheek_kenmerken_id => $kenmerk->id,
                    remote_options           => [{ value => 'boter' }]
                }
            );
            is scalar @$merged, 1,
                "One remote option put in, result is list with 1";


            $kenmerk->save_options(
                { options => [{ value => 'boter', active => 1 },] });
            $merged = $importer->merge_local_options(
                {
                    bibliotheek_kenmerken_id => $kenmerk->id,
                    remote_options           => [{ value => 'boter' }]
                }
            );
            is scalar @$merged, 1,
                "One local matching one remote, result is list with one";


            $merged = $importer->merge_local_options(
                {
                    bibliotheek_kenmerken_id => $kenmerk->id,
                    remote_options           => [{ value => 'not-boter' }]
                }
            );
            is scalar @$merged, 2,
                "One local not-matching one remote, result is list with one";


            $kenmerk->save_options(
                {
                    options => [
                        { value => 'boter', active => 1 },
                        { value => 'kaas',  active => 0 }
                    ]
                }
            );

            my $remote_options = [{ value => 'boter' }, { value => 'melk' }];

            $merged = $importer->merge_local_options(
                {
                    bibliotheek_kenmerken_id => $kenmerk->id,
                    remote_options           => $remote_options
                }
            );

            is scalar @$merged, 3, "Three elements remain";

            is $merged->[0]->{value}, 'boter', 'First element value is correct';
            ok !exists $merged->[0]->{active}, 'First element is not inactive';

            is $merged->[1]->{value}, 'melk', 'Second element value is correct';
            ok !exists $merged->[0]->{active}, 'Second element is not inactive';

            is $merged->[2]->{value}, 'kaas', 'Third element value is correct';
            ok !$merged->[1]->{active}, 'Third element is inactive';

        },
        'Unit test: merge_local_options'
    );


    $zs->zs_transaction_ok(
        sub {
            my $importer = $zs->zaaktype_importer;

            my $remote_options = $importer->get_remote_options({});
            is_deeply $remote_options, [],
                "Returns empty list if no options are presented";

            my $extended_options = [1, 2, 3];
            $remote_options = $importer->get_remote_options(
                { extended_options => $extended_options });

            is_deeply $remote_options, $extended_options,
                "Returns extended options if present";


            my $options = [qw/kaas boter/];

            $remote_options
                = $importer->get_remote_options({ options => $options });
            my $expected = [map { { value => $_, active => 1 } } @$options];

            is_deeply $remote_options, $expected, "Returns decorated options";


            $remote_options
                = $importer->get_remote_options({ options => $options });

        },
        'Unit test: get_remote_options'
    );


    $zs->zs_transaction_ok(
        sub {
            my $importer = $zs->zaaktype_importer;
            $importer->session({});
            my $kenmerk
                = $zs->create_bibliotheek_kenmerk_ok(value_type => 'select');

            my $extended_options = [
                { value => 'kaas',  active => 1 },
                { value => 'boter', active => 0 }
            ];

            my $remote_record = { extended_options => $extended_options };

            my $field_options
                = $importer->determine_field_options($remote_record,
                $kenmerk->id);

            $_->{remote} = 1 for @$extended_options;

            is_deeply $field_options, $extended_options,
                "Extended options returned";

            # add a local inactive option
            $kenmerk->save_options(
                { options => [{ value => 'eieren', active => 1 },] });

            $field_options = $importer->determine_field_options($remote_record,
                $kenmerk->id);

            my $expected
                = [@$extended_options, { value => 'eieren', active => 0 }];

            is_deeply $field_options, $expected,
                "Local option is retained with active = 0";

        },
        'Unit test: determine_field_options'
    );
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
