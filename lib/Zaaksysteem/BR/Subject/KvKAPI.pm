package Zaaksysteem::BR::Subject::KvKAPI;

use namespace::autoclean; 
use Moose::Role;

use BTTW::Tools;
use Scalar::Util 'blessed';
use Zaaksysteem::BR::Subject::Constants 'REMOTE_SEARCH_MODULE_NAME_KVKAPI';

=head1 NAME

Zaaksysteem::BR::Subject::KvKAPI - Bridge helpers for the KvK API module

=head1 DESCRIPTION

Adds logic to the L<Zaaksysteem::BR::Subject> bridge for inflating KvK API results.

=head1 METHODS

=head2 search

See L<Zaaksysteem::BR::Subject#search> for usage information.

=cut

around search => sub {
    my $method = shift;
    my $self   = shift;
    my ($params) = @_;

    if (!$self->_is_remote_kvkapi($params)) {
        return $self->$method(@_);
    }

    if (!wantarray) {
        throw('br/subject/search_remote', 'Remote searches require list context');
    }

    my $interface = $self->_assert_kvkapi_interface;

    my %cleanparams = map { my $ckey = $_; $ckey =~ s/^subject\.//; $ckey => $params->{$_} } keys %$params;
    delete($cleanparams{subject_type});

    my $transaction = $interface->process_trigger('search_companies', \%cleanparams);
    my $result      = $transaction->get_processor_params->{result};
    $result ||= [];

    my @objects;
    for my $company (@$result) {
        push(@objects, $self->object_from_params($company));
    }

    return @objects;
};

=head2 remote_import

See L<Zaaksysteem::BR::Subject#remote_import> for usage information.

=cut

around remote_import => sub {
    my $method   = shift;
    my $self     = shift;
    my ($params) = @_;

    my $type = (blessed($params) ? $params->subject_type : $params->{subject_type});
    if (!$self->_is_remote_kvkapi({subject_type => $type})) {
        return $self->$method(@_);
    }
    my $external_identifier = $params->{external_subscription}{external_identifier};

    return $self->_remote_import($external_identifier);
};

=head2 save

    $saved_object = $bridge->save($fresh_object);

Along some housekeeping, this will save a C<$fresh_object> or replace an
existing one.

=cut

around save => sub {
    my $method   = shift;
    my $self     = shift;
    my ($object) = @_;

    my $type = (blessed($object) ? $object->subject_type : $object->{subject_type});
    if (!$self->_is_remote_kvkapi({subject_type => $type})) {
        return $self->$method(@_);
    }

    my $interface = $self->_assert_kvkapi_interface;

    my $existing_objects = $self->search_rs({
        'subject_type'                  => 'company',
        'subject.coc_number'            => $object->subject->coc_number,
        $object->subject->coc_location_number
            ? ('subject.coc_location_number' => $object->subject->coc_location_number)
            : (),
    });

    $self->schema->resultset('Logging')->trigger(
        'subject/company/view', {
        component    => 'betrokkene',
        component_id => undef,
        data         => {
            coc_number          => $object->subject->coc_number,
            coc_location_number => ($object->subject->coc_location_number || ''),
            company             => $object->subject->company,
            module              => 'kvkapi'
        }
    });

    # If the object already exists, update it
    if ($existing_objects->count == 1) {
        my $existing_object = $existing_objects->first->as_object;

        # The easiest way to overwrite an object: give a new object the old id, and
        # save it
        $object->id($existing_object->id);

        foreach (qw(id mobile_phone_number phone_number email_address)) {
            if (defined $existing_object->subject->$_) {
                $object->subject->$_($existing_object->subject->$_);
            }
        }

        $object = $self->$method($object);
        return $object->discard_changes(schema => $self->schema);
    }

    # Otherwise, create a new one
    try {
        $self->schema->txn_do(
            sub {
                my $subscription_id = $object->external_subscription->external_identifier;

                $object = $self->$method($object);
                $object->set_authenticated($self->schema,$interface,$subscription_id) if $subscription_id;
            }
        );
    } catch {
        throw(
            'br/subject/remote_import/remote_error',
            "$_"
        );
    };

    return $object->discard_changes(schema => $self->schema);
};

=head1 PRIVATE METHODS

=head2 _is_remote_kvkapi

    $self->_is_remote_kvkapi({ subject_type => 'company' });
    # Returns true if $self->remote_search equals REMOTE_SEARCH_MODULE_NAME_KVKAPI constant

Returns a true value when searching remotely for a company using the KvK API.

=cut

sub _is_remote_kvkapi {
    my $self = shift;
    my $params = shift;

    return 1 if (
        $params->{subject_type} &&
        $params->{subject_type} eq 'company' &&
        $self->remote_search &&
        lc($self->remote_search) eq REMOTE_SEARCH_MODULE_NAME_KVKAPI
    );

    return;
}

=head2 _assert_kvkapi_interface

Returns the active KvK API interface if it exists, throws an exception
if none or more than one interface are found.

=cut

sub _assert_kvkapi_interface {
    my $self = shift;

    my $config_ifaces = $self->schema->resultset('Interface')->search_active(
        { module => 'kvkapi' },
    );

    my $interface = $config_ifaces->next;

    throw(
        'br/subject/search_remote/multiple_cfg_ifaces',
        'Multiple config interfaces found, cannot continue'
    ) if $config_ifaces->next;

    throw(
        'br/subject/search_remote/no_kvkapi_iface',
        'Remote search requested, but no active KvK API configuration found'
    ) if !$interface;

    return $interface;
}

sub _remote_import {
    my $self                = shift;
    my $external_identifier = shift;

    my $interface = $self->_assert_kvkapi_interface;

    my $transaction = $interface->process_trigger('remote_import' =>
        _profile_query_params($external_identifier),
    );
    throw(
        'br/subject/remote_import/error',
        'Error in processing external request for KvK API'
    ) unless blessed($transaction) and $transaction->isa('Zaaksysteem::Model::DB::Transaction');

    my $result = $transaction->get_processor_params->{result};
    throw(
        'br/subject/remote_import/not_a_company',
        'Did not get a Subject or subject returned is of type Company' 
    ) unless blessed($result)
         and $result->isa('Zaaksysteem::Object::Types::Subject')
         and $result->subject->isa('Zaaksysteem::Object::Types::Company');

    return $result;
}

sub _profile_query_params{
    my $external_identifier = shift;
    my ($kvk, $branch, $rsin) = split '/', $external_identifier;

    # strip or add trailing zeroos such that it validates with
    # Zaaksysteem::Backend::Sysin::Modules::KvKAPI remote_import
    $kvk    = sprintf '%08d',  $kvk    if $kvk;
    $branch = sprintf '%012d', $branch if $branch;
    $rsin   = sprintf '%d',    $rsin   if $rsin;

    return {
        coc_number          => $kvk,
        coc_location_number => $branch,
    } if defined $kvk && defined $branch;

    return {
        coc_number          => $kvk,
        rsin                => $branch,
    } if defined $kvk && defined $rsin;

    return {
        coc_number          => $kvk,
    };
}

1;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
