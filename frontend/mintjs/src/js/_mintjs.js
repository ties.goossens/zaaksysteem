/*global angular*/
(function ( ) {
	
	angular.module('MintJS', [
		'MintJS.core',
		'MintJS.form'
	]);
	
})();