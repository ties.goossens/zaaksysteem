#!/usr/bin/env perl
use warnings;
use strict;

use Pod::Text;

my $parser = Pod::Text->new(sentence => 0, width => 78);
foreach (qw(LICENSE CONTRIBUTORS)) {
    $parser->parse_from_file("lib/Zaaksysteem/$_.pod", $_);
}



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

