// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

(function () {
  window.zsDefine('nl.mintlab.utils.dom.getViewportSize', function () {
    var win = window,
      docEl = document.documentElement;

    if ('innerHeight' in win) {
      return function () {
        return { width: win.innerWidth, height: win.innerHeight };
      };
    } else {
      return function () {
        return { width: docEl.clientWidth, height: docEl.clientHeight };
      };
    }
  });
})();
