package Zaaksysteem::Zaken::ResultSetBag;
use Moose;
extends 'DBIx::Class::ResultSet';

=head2 create_bag

Create a BAG entry

=cut

sub create_bag {
    my $self = shift;

    my $row = $self->create(@_);

    $row->update_bag;

    return $row;
}

=head2 create_bag

Create a bagnummeraanduiding row based on a bag object

=cut

sub create_nummeraanduiding_from_bag_object {
    my $self = shift;
    my $bag_object = shift;

    my (undef, $bag_id) = split(/\-/, $bag_object->nummeraanduiding_id);

    my $row = $self->create({
        bag_type                => 'nummeraanduiding',
        bag_id                  => sprintf("%012d", $bag_id),
        bag_nummeraanduiding_id => $bag_object->nummeraanduiding_id,

        $bag_object->coordinates ? (
            bag_coordinates_wsg => sprintf('(%s)', $bag_object->coordinates)
            ) : (),
    });

    my $all_identifiers = $bag_object->get_identifiers;
    $row->update($all_identifiers);
    return $row;
};

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
