package Zaaksysteem::Search::ZQL::Role::ApplyResultset;
use Moose::Role;

sub apply_to_resultset {
    my ($self, $rs) = @_;

    if ($self->can('_prepare_resultset')) {
        $rs = $self->_prepare_resultset($rs);
    }

    if ($self->can('_apply_to_resultset')) {
        $rs = $self->_apply_to_resultset($rs);
    }

    return $rs;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2021, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
