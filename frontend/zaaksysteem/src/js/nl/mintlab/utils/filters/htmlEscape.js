// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
var replaceValues = {
  '&': '&amp;',
  '<': '&lt;',
  '>': '&gt;',
  '"': '&quot;',
  "'": '&#039;',
};
var re = new RegExp(Object.keys(replaceValues).join('|'), 'gi');

(function () {
  angular.module('Zaaksysteem').filter('htmlEscape', function () {
    return function (val) {
      if (_.isString(val)) {
        val = val.replace(re, function (matched) {
          return replaceValues[matched];
        });
      }
      return val;
    };
  });
})();
