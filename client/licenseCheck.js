const checker = require("license-checker");

checker.init({ start: process.cwd() }, function (err, packages) {
  if (err) {
    throw "Cannot access dependency licenses";
  } else {
    const agpl = Object.values(packages).find((pckg) =>
      pckg.licenses.includes("agpl")
    );

    if (agpl) {
      throw "AGPL license detected";
    }
  }
});
