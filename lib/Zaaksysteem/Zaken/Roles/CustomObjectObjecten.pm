package Zaaksysteem::Zaken::Roles::CustomObjectObjecten;
use Moose::Role;

use Zaaksysteem::Zaken::AdvanceResult;

use BTTW::Tools;
use List::Util qw(first);

requires qw(
    log
    build_resultset
);

has case_attribute_custom_object_types => (
    is      => 'ro',
    isa     => 'Zaaksysteem::DB::ResultSet::ZaaktypeKenmerken',
    lazy    => 1,
    builder => '_get_custom_object_types'
);

sub _get_custom_object_types {
    my $self = shift;
    return $self->zaaktype_node_id->zaaktype_kenmerken->search_rs(
        {
            custom_object_uuid => { '!=' => undef },
        },
        {
            prefetch => [qw(custom_object_uuid)],
        }
    );
}

around can_volgende_fase => sub {
    my $orig    = shift;
    my $self    = shift;

    my $advance_result = $self->$orig(@_);

    my $check = 'custom_objects_complete';

    if (my $fields = $self->has_missing_required_objects) {
        $advance_result->fail($check);
        $advance_result->missing($fields);
    } else {
        $advance_result->ok($check);
    }

    return $advance_result;
};

sub _get_custom_objects {
    my $self = shift;
    my $rs = $self->build_resultset('CaseCustomObjectView');
    return $rs->search_rs({case_id => $self->id});
}

sub has_missing_required_objects {
    my ($self) = @_;

    my $phases = $self->get_rule_statusses;

    my $types = $self->_get_required_custom_object_types_for_phase($phases);
    my $objects = $self->_get_custom_objects->search_rs();

    # casetype attributes
    my $cta = $types->search_rs(
        {
            custom_object_uuid => {
                -not_in => $objects->get_column('type_uuid')->as_query
            }
        },
    );

    my $exclusions = $self->get_hidden_custom_objects;

    my @missing;
    while (my $attr = $cta->next) {
        my $name = $attr->custom_object_uuid->custom_object_type_version_id->name;
        next if first { $name eq $_ } @$exclusions;
        push(@missing, $attr->custom_object_uuid->custom_object_type_version_id->name);
    }

    return \@missing if @missing;
    return;

}

has _rule_result => (
    is      => 'ro',
    isa     => 'Defined',
    builder => '_build_rules',
    lazy    => 1,
);

sub _build_rules {
    my $self  = shift;
    my $rules = $self->rules;
    return $rules->validate_from_case($self);
}

sub get_visible_custom_objects {
    my $self = shift;
    return $self->_rule_result->active_custom_objects;
}

sub get_hidden_custom_objects {
    my $self = shift;
    return $self->_rule_result->hidden_custom_objects;
}

sub _get_required_custom_object_types_for_phase {
    my ($self, $phase) = @_;
    my $ref = ref $phase;

    return $self->case_attribute_custom_object_types->search_rs(
        {
            zaak_status_id => $ref eq 'Zaaksysteem::Zaaktypen::BaseStatus'
                ? { -in => $phase->get_column('id')->as_query } : $phase->id,
            value_mandatory => 1,
        },
        {
            columns  => [qw(custom_object_uuid zaak_status_id)],
            order_by => 'me.id'
        }
    );
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2020, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
