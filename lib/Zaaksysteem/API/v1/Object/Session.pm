package Zaaksysteem::API::v1::Object::Session;

use Moose;
use namespace::autoclean;

=head1 NAME

Zaaksysteem::API::v1::Object::Session - Current session object

=head1 SYNOPSIS

    my $obj =  Zaaksysteem::API::v1::Object::Session->new_from_catalyst($c);

=head1 DESCRIPTION

A custom object for C<api/v1> containing the session information for this zaaksysteem. It contains
information about the company, the user, etc.

=cut

use BTTW::Tools;
use List::MoreUtils qw[any];
use Zaaksysteem::API::v1::Object::Session::Account;

=head1 Tests

    ZS_DISABLE_STUF_PRELOAD=1 ./zs_prove -v t/lib/TestFor/General/API/v1/Object/Session.pm

=head1 ATTRIBUTES

=head2 logged_in_user

B<required> L<Zaaksysteem::Backend::Subject::Component>

=cut

has 'logged_in_user' => (
    is  => 'ro',
    isa => 'Zaaksysteem::Backend::Subject::Component',
);

=head2 hostname

B<required> C<Str>

Hostname of this session

=cut

has 'hostname' => (
    is          => 'ro',
    isa         => 'Str',
    required    => 1,
);

=head2 design_template

B<required> C<Str>

The design template of this zaaksysteem

=cut

has 'design_template' => (
    is          => 'ro',
    isa         => 'Str',
    required    => 1,
);

=head2 active_interfaces

Shows which interfaces are active for this session.

=cut

has active_interfaces => (
    is => 'rw',
    isa => 'ArrayRef[Str]',
    traits => [qw[Array]],
    default => sub { return [] },
    handles => {
        add_interface => 'push'
    }
);

=head2 current_bundle_version

Shows the current "bundle version" for the Zaaksysteem application.

=cut

has current_bundle_version => (
    is => 'rw',
    isa => 'Str',
    required => 1,
);

=head2 account

B<required> C<Zaaksysteem::API::v1::Object::Session::Account>

The current account settings of this session. E.g.: the company info.

=cut

has 'account' => (
    is          => 'ro',
    isa         => 'Zaaksysteem::API::v1::Object::Session::Account',
    required    => 1,
);

=head2 capabilities

Aggregated capabilities the user associated with the session has, implied or
explicit.

=cut

has capabilities => (
    is => 'rw',
    isa => 'ArrayRef[Str]',
    traits => [qw[Array]],
    default => sub { return [] },
    handles => {
        add_capability => 'push'
    }
);

=head2 configurable

Items from the configuration table which are relevant for the user session.

=cut

has configurable => (
    is      => 'rw',
    isa     => 'HashRef',
    default => sub { {} },
);

=head1 METHODS

=head2 new_from_catalyst

=over 4

=item Arguments: L<Catalyst>

=item Return value: L<Zaaksysteem::API::v1::Object::Session>

=back

    my $session = Zaaksysteem::API::v1::Object::Session->new_from_catalyst($c);

Constructs L<Zaaksysteem::API::v1::Object::Session> by inflating values from a Catalyst C<$c> object

=cut

sub new_from_catalyst {
    my $class   = shift;
    my $c       = shift;

    my $self = $class->new(
        account => Zaaksysteem::API::v1::Object::Session::Account->new_from_catalyst($c),
        hostname => $c->req->uri->host,
        design_template => $c->config->{ gemeente_id },
        ($c->user_exists ? (logged_in_user => $c->user) : ()),
        current_bundle_version => $ENV{ZAAKSYSTEEM_BUNDLE_VERSION} // '',
    );

    $self->add_interface($self->_get_active_interfaces($c));

    $self->configurable($self->_get_configurables($c));

    return $self;
}

sub _get_active_interfaces {
    my $self = shift;
    my $c    = shift;

    my $redis = $c->model('Redis');
    my $data = $redis->get_or_set_json(
        'active_interfaces',
        sub {
            my $model      = $c->model('DB::Interface');
            my @interfaces = $model->get_whitelisted_interfaces(1);
            return \@interfaces;
        }
    );
    return @{$data};
}

sub _get_configurables {
    my ($self, $c) = @_;

    my $bag = $c->model('DB::Config')->get_bag_config;

    my %configurables = (%$bag);

    $configurables{files_locally_editable} = $c->model('Redis')->get_or_set_json(
        'files_locally_editable',
        sub {
            return $c->model('DB::Config')->get("files_locally_editable", 1);
        },
        {
            # This setting cannot be modified by the user, only by people with actuall
            # DB access, set the cache to 4 hrs
            expire => 14400,
        }

    );

    my @cacheable_configurables = qw(
        dashboard_ui_xss_uri
        edit_document_online
        edit_document_msonline
        signature_upload_role
        show_object_v1
        show_object_v2
    );
    for my $config_item_name (@cacheable_configurables) {
        $configurables{$config_item_name} = $c->model('Redis')->get_or_set_json(
            $config_item_name,
            sub {
                return $c->model('DB::Config')->get($config_item_name, 1);
            },
        );
    }

    return \%configurables;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015-2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
