package Zaaksysteem::DB::Component::ZaaktypeKenmerken;

use strict;
use warnings;
use Data::Dumper;
use JSON;
use Zaaksysteem::Constants qw/ZAAKSYSTEEM_CONSTANTS/;

use List::Util qw/first/;

use base qw/DBIx::Class/;

sub added_columns {
    return [qw/
        naam
        type
        options
    /];
}

sub options {
    my ($self) = @_;

    if ($self->bibliotheek_kenmerken_id) {
        return $self->bibliotheek_kenmerken_id->options;
    }
}

sub naam {
    my $self    = shift;

    if ($self->bibliotheek_kenmerken_id) {
        return $self->bibliotheek_kenmerken_id->naam;
    }
}

sub type {
    my $self    = shift;

    if ($self->bibliotheek_kenmerken_id) {
        return $self->bibliotheek_kenmerken_id->value_type;
    }
    elsif ($self->properties->{text_content}) {
        return "text_block";
    }
    elsif ($self->get_column('object_id')) {
        return "objecttype";
    }
    elsif ($self->get_column('custom_object_uuid')) {
        return "custom_object";
    }
}

sub label {
    my $self    = shift;

    ### Make sure we write
    return $self->next::method(@_) if @_;

    ### Show label when exists
    return $self->next::method
        if $self->next::method;

    if ($self->bibliotheek_kenmerken_id) {
        return $self->bibliotheek_kenmerken_id->label;
    }
}

sub magic_string {
    my $self    = shift;

    if ($self->bibliotheek_kenmerken_id) {
        return $self->bibliotheek_kenmerken_id->magic_string;
    }
}

sub description {
    my $self    = shift;

    if ($self->bibliotheek_kenmerken_id) {
        return $self->bibliotheek_kenmerken_id->description;
    }
}

sub speciaal_kenmerk {
    my $self    = shift;

    if ($self->bibliotheek_kenmerken_id) {
        return $self->bibliotheek_kenmerken_id->speciaal_kenmerk;
    }
}

sub help {
    my $self    = shift;

    ### Make sure we write
    return $self->next::method(@_) if @_;

    ### Show label when exists
    return $self->next::method
        if $self->next::method;

    if ($self->bibliotheek_kenmerken_id) {
        return $self->bibliotheek_kenmerken_id->help;
    }
}

sub kenmerken_categorie {
    my $self    = shift;

    if ($self->bibliotheek_kenmerken_id) {
        return $self->bibliotheek_kenmerken_id->document_categorie;
    }

    return;
}

sub rtkey {
    my $self    = shift;

    my $key = $self->get_column('bibliotheek_kenmerken_id');

    return unless $key;
    return sprintf('kenmerk_id_%s', $key);
}

sub verplicht {
    my $self    = shift;

    return 1 if $self->value_mandatory;

    return;
}


sub default_value {
    my $self = shift;

    # If there is an update to the column, we'll let the original accessor
    # deal with it.
    return $self->value_default(@_) if @_;

    # Fetch the column value.
    my $value_default = $self->value_default;

    # If there's something in the description field, then just return that.
    return $value_default if defined $value_default && length $value_default;

    # Otherwise, get the default value from the bibliotheek_kenmerken table
    my $bib_row = $self->bibliotheek_kenmerken_id();

    if (defined $bib_row) {
       return $bib_row->value_default;
    }
}

=head2 required_permissions_decoded

Stored as json, and edited by the frontend without needing decoding. To suit
the backoffice code, default behaviour is not decode/encode.

This is an accessor for backend code that will actually use the information.

=cut

sub required_permissions_decoded {
    my ($self) = @_;

    # handle empty which is legacy - which will be around for a long time
    my $required = $self->required_permissions ? JSON::decode_json($self->required_permissions) : {};

    # legacy code - only necessary in dev environments, but never hurts.
    return [] unless $required && ref $required && ref $required eq 'HASH';

    return $required->{selectedUnits} || [];
}

=head2 format_required_permissions

    $hash = $obj->_format_required_permissions([ $serializer ]);

Transforms permissions into terminology our api understands

=cut

sub format_required_permissions {
    my $self        = shift;
    my $serializer  = shift;

    my $schema      = $self->result_source->schema;

    my $permissions = $self->required_permissions_decoded;

    my @rv;
    if (@$permissions) {
        my $groups      = $schema->resultset('Groups')->get_all_cached($schema->cache);
        my $roles       = $schema->resultset('Roles')->get_all_cached($schema->cache);

        for my $unit (@{ $permissions }) {
            my $db_unit   = first { $unit->{org_unit_id} eq $_->id } @$groups;
            my $db_role   = first { $unit->{role_id} eq $_->id } @$roles;

            if ($db_unit) {
                $db_unit = ($serializer ? $serializer->read($db_unit) : $db_unit);
            }
            if ($db_role) {
                $db_role = ($serializer ? $serializer->read($db_role) : $db_role);
            }

            # If there is no group or role, this means the dept/role is
            # deleted from Zaaksysteem. Hand craft an "exception" group/role.
            $db_unit //= {
                instance => {
                    id      => 0,
                    name    => "Deleted group",
                    invalid => \1,
                },
                reference => undef,
                type      => 'group'
            };
            $db_role //= {
                instance => {
                    id          => 0,
                    name        => "Deleted role",
                    invalid     => \1,
                    system_role => \0
                },
                reference => undef,
                type      => 'role'
            };
            push(@rv, {group => $db_unit, role => $db_role});
        }
    }

    return \@rv;
}

sub TO_JSON {
    my $self            = shift;

    my $cols            = { $self->get_columns };

    $cols->{bibliotheek_kenmerken_id}   = { $self->bibliotheek_kenmerken_id->get_columns };

    $cols->{zaaktype_node_id}           = { $self->zaaktype_node_id->get_columns };

    return $cols;
}

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 TO_JSON

TODO: Fix the POD

=cut

=head2 added_columns

TODO: Fix the POD

=cut

=head2 default_value

TODO: Fix the POD

=cut

=head2 description

TODO: Fix the POD

=cut

=head2 help

TODO: Fix the POD

=cut

=head2 kenmerken_categorie

TODO: Fix the POD

=cut

=head2 label

TODO: Fix the POD

=cut

=head2 magic_string

TODO: Fix the POD

=cut

=head2 naam

TODO: Fix the POD

=cut

=head2 options

TODO: Fix the POD

=cut

=head2 rtkey

TODO: Fix the POD

=cut

=head2 speciaal_kenmerk

TODO: Fix the POD

=cut

=head2 type

TODO: Fix the POD

=cut

=head2 verplicht

TODO: Fix the POD

=cut

