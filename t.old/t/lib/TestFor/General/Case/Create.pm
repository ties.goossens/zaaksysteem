package TestFor::General::Case::Create;
use base qw(ZSTest);
use TestSetup;

=head1 NAME

TestFor::General::Case::Create - Create case scenario's for Zaaksysteem

=head1 SYNOPSIS

    ./zs_prove -v t/lib/TestFor/Generaal/Case/Create.pm

=cut

sub zs_create_case_natuurlijk_persoon_weird_birthday : Tests {
    my $self = shift;

    $zs->txn_ok(
        sub {
            my @timeflux = (
                DateTime->new(
                    year      => 1909,
                    month     => 5,
                    day       => 1,
                    hour      => 0,
                    minute    => 0,
                    second    => 0,
                    time_zone => "floating",
                ),
                DateTime->new(
                    year      => 1937,
                    month     => 7,
                    day       => 1,
                    hour      => 0,
                    minute    => 0,
                    second    => 0,
                    time_zone => "floating",
                ),
                DateTime->new(
                    year      => 1940,
                    month     => 5,
                    day       => 16,
                    hour      => 0,
                    minute    => 0,
                    second    => 0,
                    time_zone => "floating",
                ),
            );
            foreach my $dt (@timeflux) {
                my $np   = $zs->create_aanvrager_np_ok(geboortedatum=> $dt);
                lives_ok(sub {
                    my $case = $zs->create_case_ok(aanvragers => [$np]);
                }, "Case can be created with birthday:" . $dt->iso8601);
            }


        },
        "ZS-6321: Natuurlijk persoon met geboortedatum op een time flux moment"
    );
}

sub zs_create_case_ok : Tests {

    $zs->zs_transaction_ok(
        sub {
            my $case
                = $zs->create_case_ok(registratiedatum => DateTime->now(),);
            ok($case->id, "Found a case ID");

        },
        "Valid datetime object"
    );

    $zs->zs_transaction_ok(
        sub {
            my $case = $zs->create_case_ok(
                registratiedatum => DateTime->now->datetime());
            ok($case->id, "Found a case ID");
        },
        "DateTime->datetime()"
    );

    $zs->zs_transaction_ok(
        sub {
            my $zt = $zs->create_zaaktype_predefined_ok();
            my $days_running = 2;

            my $case = $zs->create_case_ok(zaaktype => $zt, registratiedatum => DateTime->now->subtract(days => $days_running));

            my $def = $case->zaaktype_node_id->zaaktype_definitie_id;
            my $max = $def->servicenorm;

            my $days_left = $def->servicenorm - $days_running;
            my $days_perc = sprintf("%d", $days_running/$max * 100);

            my $case_json = $case->object_data->TO_JSON->{values};

            is($case->days_left, $days_left, 'Got correct days left');
            is($case_json->{'case.days_left'}, $days_left, "Object data has correct days left");

            is($case->days_running, $days_running, 'Got correct days running');
            TODO : {
                local $TODO = "days running is not implemented in case objects";
                is($case_json->{'case.days_running'}, $days_running, "Object data has correct days running");
                is($case->days_perc, $days_perc, 'Got correct days percentage');
            }

            is($case_json->{'case.progress_days'}, $days_perc, "Object data has correct days perc");

        },
        'Correctly calculated days'
    );
}

sub zs_case_toewijzing : Tests {


SKIP: {
    skip 'AUTUMN2015BREAK: DBIx::Class::Storage::DBI::txn_do(): Can\'t call method "betrokkene_id" on an undefined value',3;

    $zs->zs_transaction_ok(
        sub {

            my $medewerker  = $zs->create_medewerker_ok()->subject_id;
            my $bid         = 'betrokkene-medewerker-' . $medewerker->id;

            my $zt          = $zs->create_zaaktype_predefined_ok();
            my $case        = $zs->create_case_ok(
                zaaktype    => $zt,
                assignee_id => $bid
            );
            my $behandelaar = $case->behandelaar;

            isa_ok($behandelaar, "Zaaksysteem::Schema::ZaakBetrokkenen");
            is($behandelaar->betrokkene_id, $medewerker->id,
                "The correct behandelaar is set");

            my $properties = $zt->zaaktype_node_id->properties;
            $properties->{check_permissions_for_assignee} = 1;
            $zt->zaaktype_node_id->properties($properties);
            $zt->zaaktype_node_id->update();
            $zt->zaaktype_node_id->discard_changes;
            $case->discard_changes;

            ok($case->assert_assignee(betrokkene_id => $bid), 'assert_assignee on betrokkene_id');
            ok($case->assert_assignee(subject => $medewerker), 'assert_assignee on subject');

            throws_ok(sub {
                    no warnings qw(redefine once);
                    local *Zaaksysteem::Backend::Object::Data::ResultSet::count = sub { return 0 };
                    $case->assert_assignee(betrokkene_id => $bid);
                }, qr#case/assignee/unauthorized:#,
                "Betrokkene has no rights on the case",
            );

            lives_ok(sub {
                    no warnings qw(redefine once);
                    local *Zaaksysteem::Backend::Object::Data::ResultSet::count = sub { return 42 };
                    $case->assert_assignee(betrokkene_id => $bid);
                },
                "Betrokken is allowed to be assigned on the case",
            );

            throws_ok(
                sub {
                    $case->assert_assignee();
                },
                qr/missing: subject_or_betrokkene_id/,
                "Invalid usage of assert_assignee",
            );

            throws_ok(sub {
                    $case->assert_assignee(betrokkene_id => 'foo-bar');
                }, qr#case/assignee/betrokkene_id/unknown:#,
                "Betrokkene not found",
            );

            throws_ok(sub {
                    no warnings qw(redefine once);
                    local *Zaaksysteem::Backend::Subject::ResultSet::find = sub { return undef };
                    $case->assert_assignee(betrokkene_id => $bid);
                }, qr#case/assignee/subject/unknown:#,
                "Betrokkene found but no subject (serious error)",
            );

            $case = $zs->create_case_ok(
                zaaktype    => $zt,
                assignee_id => $bid
            );
            $behandelaar = $case->behandelaar;

            isa_ok($behandelaar, "Zaaksysteem::Schema::ZaakBetrokkenen");
            is($behandelaar->betrokkene_id, $medewerker->id,
                "The correct behandelaar is set");
        },
        "Create case met toewijzing behandelaar"
    );
}; # END SKIP


    $zs->zs_transaction_ok(
        sub {

            my $case       = $zs->create_case_ok(
                ou_id   => 42,
                role_id => 666,
            );

            my $res = { map { $_ => $case->$_ } qw(route_ou route_role) };
            is_deeply(
                $res,
                { route_role => 666, route_ou => 42 },
                "Group and role are set correctly"
            );
        },
        "Create case met toewijzing route"
    );
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
