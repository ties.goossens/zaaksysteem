BEGIN;
alter table zaak drop confidentiality;
alter table zaak add confidentiality varchar(255) default 'public' check (confidentiality in ('public', 'internal', 'confidential'));
COMMIT;
