BEGIN;

  CREATE OR REPLACE FUNCTION get_case_status_perc(
    IN milestone int,
    IN node int,
    OUT percentage numeric(100,2)
  )
  LANGUAGE plpgsql
  AS $$
  DECLARE
    status_count int;
  BEGIN

    select into status_count count(id) from zaaktype_status where zaaktype_node_id = node;

    IF status_count = 0
    THEN
      percentage := '0';
      RETURN;
    END IF;

    percentage := 100 * milestone / status_count;

  RETURN;
  END;
  $$;


COMMIT;

