BEGIN;
  ALTER TABLE filestore ADD CONSTRAINT filestore_uuid_key UNIQUE (uuid);
COMMIT;
