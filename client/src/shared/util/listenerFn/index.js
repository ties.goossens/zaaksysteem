// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import invoke from 'lodash/invokeMap';
import pull from 'lodash/pull';
import once from 'lodash/once';

export default (options = { immediate: false }) => {
  let listeners = [],
    { immediate } = options,
    args = [];

  return {
    register: (fn, opts = { once: false, immediate }) => {
      let proxy = fn,
        remove = () => {
          pull(listeners, proxy);
        };

      if (opts.once) {
        proxy = once((...rest) => {
          fn(...rest);
          remove();
        });
      }

      listeners.push(proxy);

      if (opts.immediate !== false || !!immediate) {
        proxy(...args);
      }

      return remove;
    },
    invoke: (...rest) => {
      args = rest;
      invoke(listeners, 'call', null, ...args);
    },
    listeners,
    destroy: () => {
      args = null;
      listeners = null;
    },
  };
};
