BEGIN;

  CREATE OR REPLACE FUNCTION add_missing_acl_for_casetype(
    IN r hstore
  )
  RETURNS void LANGUAGE plpgsql AS $$

  DECLARE
    has_record record;
    permission text;
    permission_in text[];
    legacy_recht text;
    role int;
    dept int;
    priv boolean;
    zt int;
  BEGIN
          legacy_recht := r->'recht';
          CASE
            WHEN legacy_recht = 'zaak_beheer'
              THEN
                permission := 'zaak_edit';

            WHEN legacy_recht = 'zaak_edit'
              THEN
                permission := 'zaak_read';

            WHEN legacy_recht = 'zaak_read'
              THEN
                permission := 'zaak_search';

            ELSE
              RETURN;

          END CASE;

          role := r->'role_id';
          dept := r->'ou_id';
          priv := r->'confidential';
          zt := r->'zaaktype_id';

          SELECT INTO has_record * FROM zaaktype_authorisation
            WHERE
              role_id = role::int
            AND
              ou_id = dept::int
            AND
              confidential = priv::boolean
            AND
              zaaktype_id = zt::int
            AND
              recht = permission;

          IF has_record.id IS NOT NULL
          THEN
            RAISE NOTICE 'Skipping, already has a correct record: %', has_record;
            RETURN;
          END IF;
          --RAISE NOTICE 'Updating authorisation with id %', r;

          INSERT INTO zaaktype_authorisation
           (recht, role_id, ou_id, confidential, zaaktype_id, zaaktype_node_id,
            created, last_modified, deleted)
            SELECT
              permission,
              role_id,
              ou_id,
              confidential,
              zaaktype_id,
              zaaktype_node_id,

              created,
              last_modified,
              deleted
              FROM zaaktype_authorisation
              WHERE id = CAST(r->'id' as INT)
              ;

  END $$;

  CREATE OR REPLACE FUNCTION update_casetype_acl() RETURNS void
    LANGUAGE plpgsql AS $$
  DECLARE
    r record;
    h hstore;
  BEGIN
        FOR r IN
          SELECT
            *
          FROM zaaktype_authorisation
          where recht = 'zaak_beheer'
          ORDER BY id asc
        LOOP

          h := hstore(r);
          PERFORM add_missing_acl_for_casetype(h);

        END LOOP;

        FOR r IN
          SELECT
            *
          FROM zaaktype_authorisation
          where recht = 'zaak_edit'
          ORDER BY id asc
        LOOP

          h := hstore(r);
          PERFORM add_missing_acl_for_casetype(h);

        END LOOP;

        FOR r IN
          SELECT
            *
          FROM zaaktype_authorisation
          where recht = 'zaak_read'
          ORDER BY id asc
        LOOP

          h := hstore(r);
          PERFORM add_missing_acl_for_casetype(h);

        END LOOP;

        RETURN;

  END $$;

  SELECT update_casetype_acl();

  DROP FUNCTION update_casetype_acl();
  DROP FUNCTION add_missing_acl_for_casetype;

COMMIT;
