package Zaaksysteem::XML::MijnOverheid::HTTPClient::Spoof::UserAgent;

use Moose;
use namespace::autoclean;

use HTTP::Response;
use HTTP::Request::Common;

=head1 NAME

Zaaksysteem::XML::MijnOverheid::HTTPClient::Spoof::UserAgent - Spoof UserAgent

=head1 SYNOPSIS

    $spoof_ua = Zaaksysteem::XML::MijnOverheid::HTTPClient::Spoof::UserAgent->new(
        spoofmode => {
            has_berichtenbox     => $has_berichtenbox,
            accepted_by_provider => $accepted_by_provider,
            allow_attachments    => $allow_attachments,
        }
    );

and then later

    my $spoof_urls = {
        has_berichtenbox     => '/spoof/has_berichtenbox',
        berichtenbox_message => '/spoof/berichtenbox_message',
        lopende_zaak         => '/spoof/lopende_zaak',
    };
    
    my $spoof_client = Zaaksysteem::XML::MijnOverheid::HTTPClient->new(
        xml_instance => $instance, # for doing all the XML magic
        ua           => $spoof_ua,
        call_urls    => $spoof_urls,
    );

and much later:

    my ($result, $xml_resp, $xml_rqst) = $spoof_client->$xml_method(
        param_x => 'foo',
    );

This class will provide a drop-in replacement for a normal LWP::UserAgent. This
drop-in will be the low-level, last port in the entire XML HTTP request cycle.
At the very end, it analyzes the request and produces a spoofed response that
otherwise would had been returned by the remote service.

In order for this to work, it requires a predefined set of url-mappings, one for
each XML-method. Hence this being hardcoded in the calling code. That is where
the URL's are being set up.

The returned spoofed response can then be used in the rest of the XML HTTP cycle
to extract the data as a HashRef.

=head1 ATTRIBUTES

=head2 spoofmode

A hashref to flexible set of spoof-settings that this 'client' will act on like:

=over

=item has_berichtenbox

=item allow_attachments

=item accepted_by_provider

=back

These can all be set at the configuration screen of the Interface

=cut

has spoofmode => (
    is => 'ro',
    isa => 'Any',
    default => sub { {} },
);

=head2 request_parsers

A HashRef to CodeRefs to parse the request xml, one for each xml-method.

These parsers simply return a HashRef to extracted params.

=cut

has request_parsers => (
    is => 'ro',
    isa => 'HashRef',
    builder => '_build_request_parsers',
);

sub _build_request_parsers {
    return {
        has_berichtenbox     => \&_parse__has_berichtenbox__xml_rqst,
        berichtenbox_message => \&_parse__berichtenbox_message__xml_rqst,
    }
}

=head2 response_templates

A Hashref to response templates, one for each xml-method

These templates contain the xml to be expected from the remote service. Inside
the templates use C<{> and C<}> to delimit parameters that will be sustituted,
like C<< <accepted>{accepted_by_provider}</accepted> >>.

=cut

has response_templates => (
    is => 'ro',
    isa => 'HashRef',
    builder => '_build_response_templates',
);

sub _build_response_templates {
    return {
        has_berichtenbox     => __PACKAGE__->_template__has_berichtenbox__xml_resp(),
        berichtenbox_message => __PACKAGE__->_template__berichtenbox_message__xml_resp(),
    }
}

=head1 METHODS

=head2 post

Accepts a normal HTTP::Request message and returns a spoofed HTTP::Response.

Based on the URL, it reconstructs the used xml-method. This method name selects
which xml-parser will be used to extract some parameters. It will also select
which xml-response subtitution will be used to generate the HTTP::Response.

=cut

sub post {
    my $self = shift;
    my $mapping_url = shift;
    my %params = @_;

    my ($xml_method) = $mapping_url =~ m| ^/spoof/ (.+) $ |x;
    my $xml_rqst = $params{'Content'};
    my $params_parsed = $self->request_parsers->{$xml_method}($xml_rqst);
    my $xml_resp = $self->_substitute_resp(
        $self->response_templates->{$xml_method},
        $params_parsed
    );

    return HTTP::Response->new(
        200 => "OK",
        HTTP::Headers->new(
            'Content-Type' => 'text/xml; charset=utf-8',
        ),
        $xml_resp
    ) unless $self->spoofmode->{server_error};

    return HTTP::Response->new(
        502 => "Spoofed External Server Error"
    )
}

sub _substitute_resp {
    my ( $self, $template, $params ) = @_;

    my $spoof_params = $self->_spoof_params();
    my $substitutes = { %$params, %$spoof_params };

    return __PACKAGE__->_substitute_template( $template, $substitutes )
}

sub _spoof_params {
    my $self = shift;

    my $params = {};

    $params->{has_berichtenbox} = 
        ( exists $self->spoofmode->{has_berichtenbox}
          && !$self->spoofmode->{has_berichtenbox}
        ) ? 'false' : 'true';
    $params->{xsd_version} = 
        ( exists $self->spoofmode->{allow_attachments}
          && $self->spoofmode->{allow_attachments}
        ) ? 'v2' : 'v1';
    $params->{accepted_by_provider} = 
        ( exists $self->spoofmode->{accepted_by_provider}
          && !$self->spoofmode->{accepted_by_provider}
        ) ? 'false' : 'true';

    return $params
}

sub _substitute_template {
    my $self        = shift;
    my $template    = shift;
    my $substitutes = shift;
    
    my $regex_text = join '|', map { quotemeta } sort { $b cmp $a } keys %$substitutes;
    my $regex = qr|$regex_text|;
    $template =~ s|{($regex)}|$substitutes->{$1}|g;

    return $template;
}

sub _parse__has_berichtenbox__xml_rqst {
    my $xml_rqst = shift;

    my ($bsn)        = $xml_rqst =~ m| bsn>        \s* (\d+) \s* </ |gx;
    my ($identifier) = $xml_rqst =~ m| identifier> \s* (\S+) \s* </ |gx;
    my ($timestamp)  = $xml_rqst =~ m| timestamp>  \s* (\S+) \s* </ |gx;
    
    return {
        bsn              => $bsn,
        identifier       => $identifier,
        timestamp        => $timestamp,
    }
}

sub _parse__berichtenbox_message__xml_rqst {
    my $xml_rqst = shift;

    my ($identifier) = $xml_rqst =~ m| identifier> \s* (\S+) \s* </ |gx;
    my ($timestamp)  = $xml_rqst =~ m| timestamp>  \s* (\S+) \s* </ |gx;
    
    return {
        identifier       => $identifier,
        timestamp        => $timestamp,
    };
}

sub _template__has_berichtenbox__xml_resp {
qq|<?xml version="1.0"?>
<has_berichtenbox_response xmlns="http://www.zaaksysteem.nl/xml/mijnoverheid/berichtenbox-check-{xsd_version}" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <header>
    <timestamp>{timestamp}</timestamp>
    <identifier>{identifier}</identifier>  <!-- wordt bij ZS opgeslagen ter referentie -->
    <sender>OpenTunnel</sender>            <!-- wordt bij ZS opgeslagen ter referentie -->
  </header>
  <bsn>{bsn}</bsn>
  <has_berichtenbox>{has_berichtenbox}</has_berichtenbox>
</has_berichtenbox_response>
|
}

sub _template__berichtenbox_message__xml_resp {
qq|<?xml version="1.0"?>
<berichtenbox_message_response xmlns="http://www.zaaksysteem.nl/xml/mijnoverheid/berichtenbox-{xsd_version}" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <header>
    <timestamp>{timestamp}</timestamp>
    <identifier>{identifier}</identifier>
    <sender>OpenTunnel</sender>
  </header>
  <accepted>{accepted_by_provider}</accepted>
  <message><!-- Element is optional -->SPOOFMODE - NOT ACCEPTED: REASON</message>
</berichtenbox_message_response>
|
}



__PACKAGE__->meta->make_immutable();

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
