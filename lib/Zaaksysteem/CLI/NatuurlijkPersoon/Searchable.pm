package Zaaksysteem::CLI::NatuurlijkPersoon::Searchable;
use Moose;

extends 'Zaaksysteem::CLI';

around run => sub {
    my $orig = shift;
    my $self = shift;

    $self->$orig(@_);

    my $found = $self->schema->resultset('NatuurlijkPersoon')->search(undef, { order_by => { '-asc' => 'deleted_on' }});
    while (my $np = $found->next) {
        $self->do_transaction(
            sub {
                my ($self) = @_;
                $self->log->info("Updating search terms for NP with ID: " . $np->id);
                $np->update();
            }
        );
    }
};

__PACKAGE__->meta->make_immutable;

__END__

=head1 NAME

Zaaksysteem::CLI::NatuurlijkPersoon::Deduplicate - Deduplicate Natuurlijk Personen.

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, 2017 Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
