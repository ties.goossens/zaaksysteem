package Zaaksysteem::Backend::Sysin::Next2Know::Model;
use Moose;

=head1 NAME

Zaaksysteem::Backend::Sysin::Next2Know::Model - Next2Know Module

=head1 DESCRIPTION

A simple Next2Know API module

=head1 SYNOPSIS

    use Zaaksysteem::Backend::Sysin::Next2Know::Model;

    my $next2know = Zaaksysteem::Backend::Sysin::Next2Know::Model->new(
        oauth_endpoint => 'https://foo.next2know.nl',
        grant_type     => 'client_credentials',         # optional
    );

    my $token = $next2know->get_oauth_token;

=cut

use HTTP::Request;
use JSON::XS qw(encode_json);
use URI;
use BTTW::Tools;

with qw(
    MooseX::Log::Log4perl
    Zaaksysteem::Backend::Sysin::Roles::UA
);

=head1 ATTRIBUTES

=head2 oauth_endpoint

The OAuth endpoint

=cut

has oauth_endpoint => (
    is       => 'ro',
    isa      => 'Str',
    required => 1,
);

=head2 grant_type

The grant_type, defaults to C<client_credentials>

=cut

has grant_type => (
    is      => 'ro',
    isa     => 'Str',
    default => 'client_credentials',
);

=head1 METHODS

=head2 get_oauth_token

    my $token = $next2know->get_oauth_token(username => 'some_user');

Returns a perl hash datastructure with the Next2Know token.
For more information see:
L<http://zaaksysteem-api.next2know.nl/apidoc/#api-OAuth-PostOauthAccess_token>

The specified "username" will be used by Next2Know to restrict access to
results.

=cut

define_profile get_oauth_token => (
    required => {
        username => 'Str',
    },
);

sub get_oauth_token {
    my $self = shift;
    my $args = assert_profile({@_})->valid;

    $self->assert_endpoint($self->endpoints->{oauth});

    my $request = HTTP::Request->new(
        'POST',
        $self->endpoints->{oauth},
        ['Content-Type' => 'application/json'],
        encode_json(
            {
                grant_type    => $self->grant_type,
                client_id     => $self->username,
                client_secret => $self->password,
                username      => $args->{username},
            }
        )
    );

    return $self->parse_json_response($self->ua->request($request));
}

=head1 PRIVATE METHODS

=head2 _build_endpoints

Builder which needs to be implement for use of L<Zaaksysteem::Backend::Sysin::Roles::UA>

=cut

sub _build_endpoints {
    my $self = shift;
    return { oauth => URI->new($self->oauth_endpoint) };
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
