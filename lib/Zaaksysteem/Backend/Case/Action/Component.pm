package Zaaksysteem::Backend::Case::Action::Component;

use Moose;
use JSON;
use File::Basename;

use BTTW::Tools;

BEGIN { extends 'DBIx::Class'; }

use constant SUBCASE_NAMES => {
    deelzaak => 'Deelzaak',
    vervolgzaak => 'Vervolgzaak',
    vervolgzaak_datum => 'Vervolgzaak',
    gerelateerd => 'Gerelateerde zaak'
};

sub TO_JSON {
    my $self = shift;

    return {
        id => $self->id,
        type => $self->type,
        label => $self->mangle_label,
        automatic => $self->automatic ? JSON::true : JSON::false,
        tainted => $self->tainted ? JSON::true : JSON::false,
        description => $self->description,

        # Dude! Nasty!
        # Use Catalyst->uri_for maaaaaaaaaaan!
        url => '/zaak/' . $self->get_column('case_id') . '/action?id=' . $self->id,

        data => $self->mangle_data
    };
}

sub tainted {
    my $self = shift;

    return $self->state_tainted || $self->data_tainted;
}

sub description {
    my $self = shift;

    my %mapping = (
        email => sub { 'Bericht bewerken of direct versturen' },
        allocation => sub { 'Toewijzing bewerken of direct wijzigen' },
        template => sub { 'Sjabloon bewerken of direct aanmaken' },
        case => sub { SUBCASE_NAMES->{ shift->data->{ relatie_type } } . ' bewerken of direct starten' },
        subject => sub { sprintf("Voeg betrokkene \"%s\" toe", shift->data->{naam}) }
    );

    return 'Geen beschrijving voor deze actie' unless exists $mapping{ $self->type };

    return $mapping{ $self->type }->($self);
}

sub mangle_label {
    my $self = shift;

    return $self->label unless($self->type eq 'allocation');
    my $schema = $self->result_source->schema;

    my $user = $schema->current_user;
    throw(
        'case/action/mangle_label/no_user',
        'We only allow to show actions when there is a logged in user'
    ) unless $user;

    my $users = Zaaksysteem::Schema->default_resultset_attributes->{ users };

    my $group = $user->find_group_by_id($self->data->{ou_id});
    my $role  = $user->find_role_by_id($self->data->{role_id});

    return sprintf(
        '%s, %s',
        ($group ? $group->name : sprintf('afdeling-onbekend(%d)', $self->data->{ ou_id })),
        ($role ? $role->name : sprintf('rol-onbekend(%d)', $self->data->{ role_id }))
    );
}

sub mangle_data {
    my $self = shift;

    my $data = $self->data;

    if ($self->type eq 'email') {
        $data->{case_document_attachments} = $self->non_empty_attachments;

        if (   $data->{rcpt} eq 'behandelaar'
            && $data->{behandelaar}
            && !$data->{betrokkene_naam})
        {
            my $betrokkene
                = $self->result_source->schema->betrokkene_model->get(
                {}, $data->{behandelaar});
            $data->{betrokkene_naam}
                = $betrokkene
                ? $betrokkene->display_name
                : "Onbekende gebruiker";
        }
    }
    elsif($self->type eq 'case') {
        $data->{ description } = SUBCASE_NAMES->{ $data->{ relatie_type } };
    }

    return $data;
}


=head2 non_empty_attachments

a casetype can include emails, emails can include
attachments from pre-defined bibliotheek_kenmerken. This
selects only those attachment kenmerken in which users have
added actual files.

=cut

sub non_empty_attachments {
    my ($self) = @_;

    my $requested_attachments = $self->data->{case_document_attachments} || [];

    my $schema = $self->result_source->schema;
    my $bibliotheek_kenmerken_id_query = $schema->resultset('ZaaktypeKenmerken')->search_rs(
        {
            'me.id' => {
                -in => [
                    map { $_->{case_document_ids} } @$requested_attachments
                ]
            }
        }
    )->get_column('bibliotheek_kenmerken_id')->as_query;

    my $file_case_documents_query = $schema->resultset('FileCaseDocument')->search_rs(
        {
            'me.bibliotheek_kenmerken_id' => { -in => $bibliotheek_kenmerken_id_query }
        }
    )->get_column('file_id')->as_query;

    my $attachment_rs = $schema->resultset('File')->active_rs->search(
        {
            'me.case_id' => $self->get_column('case_id'),
            'me.id' => {
                -in => $file_case_documents_query
            }
        }
    );

    return $attachment_rs->all;
}

=head1 METHODS

=head2 set_automatic({ automatic => 1 })

Return value: None C<$STRING_RESPONSE>

    $action->set_automatic({ automatic => 1 }

Updates the automatic property of the action. First checks if status
changed. Because of potential frequent updates of this we may end up
with doing the same SQL UPDATE 10 times per page view - therefore the
current value is first inspected. But maybe DBIX::Class is already
smart enough? Not taking that risk.

B<Parameters>

=over 4

=item automatic

Boolean value that will replace the current value of automatic.

=back
=cut

sub set_automatic {
    my ($self, $options) = @_;

    die "need automatic parameter" unless exists $options->{automatic};

    my $desired = $options->{automatic} ? 1 : 0; # normalize for boolean input
    my $current = $self->automatic || 0;

    if($desired ne $current) {
        $self->automatic($desired);
        $self->update;
    }
}


1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 TO_JSON

TODO: Fix the POD

=cut

=head2 description

TODO: Fix the POD

=cut

=head2 mangle_data

TODO: Fix the POD

=cut

=head2 mangle_label

TODO: Fix the POD

=cut

=head2 tainted

TODO: Fix the POD

=cut

