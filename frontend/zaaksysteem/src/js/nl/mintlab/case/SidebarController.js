// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

(function () {
  angular
    .module('Zaaksysteem.case')
    .controller('nl.mintlab.case.SidebarController', [
      '$scope',
      function ($scope) {
        $scope.caseId = null;

        $scope.reloadData = function () {};

        $scope.init = function (caseId, phaseId, closed, showChecklist) {
          $scope.caseId = caseId;
          $scope.phaseId = phaseId;
          $scope.closed = closed;
          $scope.showChecklist = showChecklist;
        };
      },
    ]);
})();
