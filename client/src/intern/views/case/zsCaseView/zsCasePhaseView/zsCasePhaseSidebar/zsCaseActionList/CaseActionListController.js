// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import _keys from 'lodash/keys';
import assign from 'lodash/assign';
import each from 'lodash/each';
import find from 'lodash/find';
import first from 'lodash/head';
import get from 'lodash/get';
import identity from 'lodash/identity';
import keyBy from 'lodash/keyBy';
import pickBy from 'lodash/pickBy';
import sortBy from 'lodash/sortBy';
import getIcon from './icon';

export default class CaseActionListController {
  static get $inject() {
    return [
      '$scope',
      '$state',
      '$compile',
      '$q',
      'composedReducer',
      'zsModal',
      'snackbarService',
    ];
  }

  constructor(
    $scope,
    $state,
    $compile,
    $q,
    composedReducer,
    zsModal,
    snackbarService
  ) {
    let ctrl = this,
      actionReducer;

    actionReducer = composedReducer(
      { scope: $scope },
      ctrl.actions,
      ctrl.disabled,
      ctrl.phaseState
    ).reduce((actions, disabled, phaseState) => {
      let sortedActions = sortBy(actions, (action) =>
        ['subject', 'template', 'email', 'case', 'allocation'].indexOf(
          action.type
        )
      );

      return sortedActions.map((action) => {
        // Note on 'uncheckmarkable': don't allow checking of action if
        // it is a template by an external doc generator (i.e. Xential),
        // or if the action is a create new case of type deelzaak and
        // we're in the last phase of the case
        let actionObject = {
          id: action.id,
          type: action.type,
          icon: getIcon(action),
          label: action.label,
          checked: action.automatic,
          tainted: action.tainted,
          description: action.description,
          disabled:
            disabled || get(action, 'data.relation_type') === 'deelzaak',
          uncheckmarkable:
            !!get(action, 'data.interface_id') ||
            !!(
              get(action, 'data.relatie_type') === 'deelzaak' &&
              phaseState.isLast
            ),
          classes: {
            disabled,
          },
        };

        if (action.type === 'template') {
          actionObject.templateType = get(action, 'data.interface_id')
            ? 'external'
            : 'internal';
        }

        return actionObject;
      });
    });

    ctrl.getActions = actionReducer.data;

    ctrl.isLoading = () => ctrl.actionsLoading();

    ctrl.handleActionClick = (action) => {
      if (action.disabled) {
        return;
      }

      let modal,
        message =
          action.type === 'template' &&
          get(action, 'templateType') === 'external'
            ? 'Let op! Dit sjabloon wordt gegenereerd met een externe documentgenerator.'
            : '',
        scope = $scope.$new(true),
        el = angular.element(
          `<zs-case-action-form
            data-action="action"
            case-id="${ctrl.caseId()}"
            case-custom-html-template-name="'${ctrl.caseCustomHtmlTemplateName()}'"
            form-note="'${message}'"
            on-action-save="handleActionSave($data, $destination)"
            on-action-execute="handleActionExecute($data)"
            email-template-data="emailTemplateData()"
          ></zs-case-action-form>`
        ),
        keys = keyBy(
          'subject-relations requestor phases templates case-documents'.split(
            ' '
          )
        );

      scope.vm = ctrl;
      scope.action = find(ctrl.actions(), { id: action.id });

      let startAsync = (type, data, destination) => {
        let promise,
          verb,
          redirect = {
            allocate: type === 'execute' && scope.action.type === 'allocation',
            template: type === 'execute' && scope.action.type === 'template',
          },
          reason = first(_keys(pickBy(redirect, identity))),
          newState;

        if (type === 'save') {
          verb = 'opgeslagen';
        } else if (type === 'execute') {
          verb = 'uitgevoerd';
        }

        if (redirect.allocate) {
          newState = 'home';
        }

        modal.hide();

        promise = ctrl
          .onActionTrigger({
            $action: scope.action,
            $trigger: type,
            $data: assign({}, action.data, data),
          })
          .then((responseData) => {
            modal.close();
            scope.$destroy();

            return $q.resolve(responseData);
          })
          .catch((err) => {
            modal.show();

            return $q.reject(err);
          });

        snackbarService
          .wait(`De actie wordt ${verb}.`, {
            promise,
            then: (mutationData) => {
              let msg,
                actions = [];

              switch (reason) {
                case 'allocate':
                  msg =
                    'De toewijzing is gewijzigd. U bent doorverwezen naar het dashboard.';
                  break;
                case 'template':
                  {
                    let templateType = mutationData.data.queue_item.data.type;

                    switch (templateType) {
                      case 'redirect':
                        window.open(
                          mutationData.data.queue_item.data.data.document
                            .resume_url
                        );
                        msg =
                          'Voor het genereren van het document moeten er acties uitgevoerd worden op een externe website.';
                        actions = [];
                        break;
                      case 'pending':
                        msg =
                          'Uw document is in de wachtrij geplaatst.<br>U krijgt een notificatie op het moment dat deze is verwerkt.';
                        break;
                      case undefined:
                        msg = 'Uw document is aangemaakt.';
                        actions =
                          $state.current.name === 'case.docs'
                            ? actions
                            : [
                                {
                                  type: 'link',
                                  label: 'Documenten bekijken',
                                  link: $state.href('case.docs', null, {
                                    reload: true,
                                  }),
                                },
                              ];
                        break;
                    }
                  }
                  break;
                default:
                  msg =
                    get(mutationData, 'flash_message') ||
                    `De actie is succesvol ${verb}`;
                  break;
              }

              return {
                message: msg,
                actions,
              };
            },
            catch: () =>
              `De actie kon niet worden ${verb}. Neem contact op met uw beheerder voor meer informatie.`,
          })
          .then(() => {
            if (destination) {
              window.location.href = destination;
            } else if (newState) {
              $state.go(newState);
            }
          });
      };

      scope.handleActionSave = (data, destination) => {
        startAsync('save', data, destination);
      };

      scope.handleActionExecute = (data) => {
        startAsync('execute', data);
      };

      scope.emailTemplateData = () => ctrl.emailTemplateData();

      each(keys, (attr) => {
        let value = attr.replace(/-([a-z])/g, (str, match) => {
          return match.toUpperCase();
        });

        el.attr(`data-${attr}`, `vm.${value}()`);
      });

      modal = zsModal({
        title: `${action.label}: ${action.description}`,
        el: $compile(el)(scope),
        classes: action.type,
      });

      modal.open();
      modal.onClose(() => {
        scope.$destroy();
      });
    };

    ctrl.handleActionToggle = (action, $event) => {
      $event.stopPropagation();
      ctrl.onActionAutomaticToggle({
        $actionId: action.id,
        $automatic: !action.checked,
      });
    };

    ctrl.handleUntaintClick = (action, $event) => {
      $event.stopPropagation();
      ctrl.onActionUntaint({
        $actionId: action.id,
      });
    };
  }
}
