package Zaaksysteem::Search::Case::Object;
use Zaaksysteem::Moose;

with qw(
    Zaaksysteem::Moose::Role::Schema
    Zaaksysteem::Search::Role::Base
);

=head1 NAME

Zaaksysteem::Search::Case::Object - Search case object

=head1 DESCRIPTION

A migration class for Zaaksysteem::Search::Object to remove the dependency to object data

=head1 SYNOPSIS

    use Foo;

=cut

sub _build_resultset {
    my $self = shift;
    return $self->build_resultset('Zaak')->search_restricted('search');
};

sub _build_zql_parser {
    my $self = shift;
    return Zaaksysteem::Search::Case::ZQL->new($self->query);
};

sub search_documents {
    my $self = shift;
    return $self->resultset->search_zql($self->query, 'search');
}

sub search_select {
    my $self = shift;
    return $self->resultset->search_zql($self->query, 'search');
}

sub search_intake {
    my $self = shift;
    die "Not implemented yet";
}

sub search_distinct {
    my $self = shift;
    die "Not implemented yet";
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2021, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
