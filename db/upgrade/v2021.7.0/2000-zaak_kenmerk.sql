BEGIN;

  ALTER TABLE zaak_kenmerk ADD COLUMN magic_string TEXT;

  UPDATE zaak_kenmerk SET magic_string = bk.magic_string
  FROM bibliotheek_kenmerken bk
  WHERE bibliotheek_kenmerken_id = bk.id;

  ALTER TABLE zaak_kenmerk ALTER COLUMN magic_string SET NOT NULL;

COMMIT;


