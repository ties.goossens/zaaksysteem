BEGIN;

DROP TRIGGER IF EXISTS refresh_casetype_end_status ON "zaaktype_kenmerken";

-- Last thing Perl does in "zaaktype.commit" is updating of the logging column
CREATE TRIGGER refresh_casetype_end_status
    AFTER UPDATE OF logging_id ON "zaaktype_node"
    FOR EACH statement
    EXECUTE PROCEDURE refresh_casetype_document_id_map ();

-- Last thing Python does when rolling back to an old case type version is UPDATE
-- of zaaktype.zaaktype_node and zaaktype.version
-- zaaktype.version is never updated by Perl so that's a "safe" way to only run
-- once.
CREATE TRIGGER refresh_casetype_end_status
    AFTER UPDATE OF "version" ON "zaaktype"
    FOR EACH statement
    EXECUTE PROCEDURE refresh_casetype_document_id_map ();

COMMIT;

