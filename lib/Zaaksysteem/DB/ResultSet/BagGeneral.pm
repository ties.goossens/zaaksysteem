package Zaaksysteem::DB::ResultSet::BagGeneral;

use Moose;

use Moose::Util::TypeConstraints qw[enum];

use Zaaksysteem::Constants qw(BAG_TYPES);
use BTTW::Tools;

extends 'Zaaksysteem::SBUS::ResultSet::BAG',
        'Zaaksysteem::Geo::BAG';

sub _get_zaak_bag {
    my  $self       = shift;
    my  $entry      = shift;
    my  $types      = shift;

    my  $rv         = {};

    for my $type (keys %{ $types }) {
        if ($type eq 'bag_id') {
            $rv->{bag_id} = $types->{$type};
            next;
        }

        $rv->{'bag_' . $type . '_id'} = $types->{$type};
    }

    return $rv;
}

define_profile _retrieve_bag_entry => (
    required => {
        id => 'Int',
        type => enum(BAG_TYPES())
    }
);

sub _retrieve_bag_entry {
    my ($self, $opts) = @_;
    $opts = assert_profile($opts)->valid;

    my $resultsetname = 'Bag' . ucfirst(lc($opts->{type}));
    my $bag = $self->result_source->schema->resultset($resultsetname)->search({'identificatie' => $opts->{id}});

    my $count = $bag->count;
    if ($count == 1) {
        return $bag->first;
    }
    elsif ($count > 1) {
        throw("Zaaksysteem/BAG", sprintf("Found more than one entry for BAG type %s with id %d", $opts->{type}, $opts->{id}));
    }
    else {
        throw("Zaaksysteem/BAG", sprintf("Found no entry for BAG type %s with id %d", $opts->{type}, $opts->{id}));
    }
}


1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

