package Zaaksysteem::ZAPI::Form::Field;

use Moose;

use BTTW::Tools;

=head1 NAME

Zaaksysteem::ZAPI::Form::Field - Construct a form field object.

=head1 DESCRIPTION

This generates a readable form field for the Angular Forms goodness. Please
don't use this module directly, but use: L<Zaaksysteem::ZAPI::Form>.

=head1 SYNOPSIS

    $field      = Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'subject_type',
        label       => 'Subject Type',
        type        => 'radio',
        required    => 1,
        options     => [
            {
                value   => 'extern',
                label   => 'Extern',
            },
            {
                value   => 'intern',
                label   => 'Intern'
            }

        ]
        description => 'Select a subject type'
    );

    $field->TO_JSON;

=head1 CONSTANTS

=head2 NAME_CONSTRAINT

=cut

use constant NAME_CONSTRAINT    => qr/[a-zA-Z0-9_-]+/;

=head2 FIELD_TYPES

Contstant value for defined field types which we support. Defines:

=over

=item hidden

=item password

=item text

=item richtext

=item textarea

=item select

=item radio

=item checkbox

=item file

=item number

=item integer

=back

=cut

use constant FIELD_TYPES        => [qw/
    hidden

    password
    text
    textarea
    richtext

    select
    radio
    checkbox

    file

    number
    integer

    spot-enlighter
/];

=head2 FIELD_PROFILE

TODO: Fix the POD

=cut

use constant FIELD_PROFILE          => {
    required            => [qw/
        name
        type
    /],
    optional            => [qw/
        label
        required
        description

        data
        when
        default

        value
    /],
    constraint_methods  => {
        type            => sub { return grep { pop } @{ FIELD_TYPES() } },
        name            => NAME_CONSTRAINT,
        required        => qr/^[01]$/,
    },
};

=head1 ATTRIBUTES

=head2 name (required)

Input "name" for field, e.g. <input name="subject" type="text">

=cut

has 'name'  => (
    'is'        => 'rw',
    'isa'       => 'Str',
    'required'  => 1,
);

=head2 label (optional)

Friendly label for this field

=cut

has 'label'  => (
    'is'        => 'rw',
    'isa'       => 'Str',
);

=head2 type (required)

Type of field, one of:

=over 4

=item text

Simple text field

=item hidden

Hidden input field

=item select (has options)

Select box, use options (below).

=item radio (has options)

Radio box, use options (below)

=item checkbox (has options)

Checkbox, use options (below)

=item password

Password box

=item textarea

Testarea

=item file

File box

=item spot-enlighter

A spot enlighter

=back

=cut

has 'type'  => (
    'is'        => 'rw',
    'isa'       => 'Str',
    'required'  => 1,
);

=head2 required

Marks this field as a required field

=cut

has 'required'  => (
    'is'        => 'rw',
    'isa'       => 'Bool',
);

=head2 default

A default value for this field

=cut

has 'default'  => (
    'is'        => 'rw',
);

=head2 when

A condition for this field

=cut

has 'when'  => (
    'is'        => 'rw',
    'isa'       => 'Str',
);

=head2 options

 [
    {
        name    => 'm',
        label   => 'man'
    },
    {
        name    => 'f',
        label   => 'woman',
    }
 ]

Options for field types containing options. See L<type> for a list. Options
must be given as a hashreference, containing the following keys.

=over 4

=item name

Name of the option / checkbox / radio field.

=item label

human readable label for the field

=back

=cut

has 'options'   => (
    'is'        => 'rw',
    'isa'       => 'ArrayRef',
);


=head2 description

Human friendly description for this field. "Tooltip"

=cut

has 'description'   => (
    'is'        => 'rw',
    'isa'       => 'Maybe[Str]',
);

=head2 data

Some of our types, e.g. spot-enlighter, need more information to extract
usefull information. This can be set on this attribute, make sure it is a
HashRef

=cut

has 'data'          => (
    'is'        => 'rw',
    'isa'       => 'HashRef',
);

=head2 value

Value for this field

=cut

has 'value'   => (
    'is'        => 'rw',
);

=head1 METHODS

=head2 TO_JSON

Returns a Angular readable json representation of this field

=cut

sub TO_JSON {
    my $self        = shift;

    $self->validate;

    my $data = $self->_object_params;

    # https://mintlab.atlassian.net/browse/ZS-740
    delete $data->{ value } unless defined $data->{ value };

    # Boolean hydration in JSON
    $data->{ required } = $data->{ required } ? \1 : \0;

    return $data;
}

=head2 validate

Validates this object, to prove it is complete and contains valid attributes

=cut

sub validate {
    my $self            = shift;

    my $dv              = Data::FormValidator->check(
        $self->_object_params,
        FIELD_PROFILE
    );

    throw('form/field/invalid', sprintf(
        "Cannot validate form field '%s', invalid: %s",
        $self->name,
        join(', ', $dv->invalid),
    )) if $dv->has_invalid;

    throw('form/field/missing', sprintf(
        "Cannot validate form field '%s', missing: %s",
        $self->name,
        join(', ', $dv->missing),
    )) if $dv->has_missing;

    return;
}


=head1 INTERNAL METHODS

=head2 _object_params

Returns a HASHRef containing this object parameters according to this object
profile

=cut

sub _object_params {
    my $self        = shift;

    return { map {
        $_ => $self->$_
    } (
        @{ FIELD_PROFILE->{required} },
        @{ FIELD_PROFILE->{optional} }
    ) };
}

1;

__END__

=head1 SEE ALSO

L<Zaaksysteem::ZAPI::Form> L<Zaaksysteem::Manual> L<Zaaksysteem::Manual::Template>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
