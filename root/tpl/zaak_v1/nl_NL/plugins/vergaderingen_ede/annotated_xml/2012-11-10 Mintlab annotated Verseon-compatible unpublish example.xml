<?xml version="1.0" encoding="utf-8" ?>
<!--

  Note:

  - Each XML file only contains one single Vergadering. Depending on the XML document's root node either being
    <publish /> or <unpublish />, the items referenced or described in the XML document are added to the database 
    or removed from it.

  - When a change occurs to a Vergadering or one or more of its children, then:
    * The entire Vergadering including all children needs to be unpublished.
    * The entire updated Vergadering including all updated children need to be republished.
    * Unpublishing and republishing is done in separate XML files. The current file is an unpublishing file.
    * Files are processed in alphabetical order of file names.
    * In alphabetical sorting of file names, the <unpublish /> XML file appears before the <publish /> file.
    
      (Means: In numeric filenames, as used in Gemeente Heemstede, where XML file names are composed of 24 digits,
      a <publish /> file's corresponding <unpublish /> file has a *pre*ceding number as its file name.)
      
      This way, it is ensured that the record is first unpublished before the updated version is published.

  - Portefeuillehouder is misspelled as <Portefeulliehouder />. This is intended. It needs to be kept this way. :)

  -->

<!-- 

  Items within the 'unpublish' root node inside a unpublishing XML file are deleted.
  No informative data needs to be included. Only containers, items and their IDs are required.
  
  -->
<unpublish>
  
  <!--
  
    Per XML file, the 'publish' or 'unpublish' root node contains one single 'Vergaderingen_Bestuurlijk' node.
    The 'Vergaderingen_Bestuurlijk' node represents - despite its plural name - a single Vergadering.
    
    Attributes:
      id: The Vergadering is identified by this attribute's value. The value needs to be unique within the Gemeente's Vergaderingen. (GUID, required)
      
    -->
  <Vergaderingen_Bestuurlijk id="948B1EEE-5567-4C30-9808-4A4D319FC0CC">
    
    <!--
    
      The 'Vergaderingen_Bestuurlijk' node may contain one single 'Behandelvoorstel_en_Agendapunten' container node. (optional)
      This node is the container for all Agendapunten of the Vergadering, which are represented by child nodes of the same name.
    
      -->
    <Behandelvoorstel_en_Agendapunten>
      
      <!--
      
        The 'Behandelvoorstel_en_Agendapunten' container node may contain one or multiple 'Behandelvoorstel_en_Agendapunten' item nodes. (optional)
        
        -->
      <Behandelvoorstel_en_Agendapunten oId="C6F94CBF-2A0B-4B76-86BC-806A50162B12">
        
        <!--
    
          A 'Behandelvoorstel_en_Agendapunten' item node may contain one single 'Portefeulliehouder' container node. (optional)
          This node is the container for all (usually one) Portefeuillehouders of the Vergadering, which are represented by child nodes of the same name.
          
          Please note that 'Portefeuillehouder' is intentionally misspelled as 'Portefeulliehouder'.
        
          -->
        <Portefeulliehouder>
          <!--
        
          A 'Portefeulliehouder' container node may contain a single or multiple 'Portefeulliehouder' item nodes. (optional)
          
          Attributes:
            oId: The Portefeuillehouder is identified by this attribute. It needs to be unique within the Gemeente's Portefeuillehouders. (GUID, required)
                 The database is searched for this GUID and the item attached if found. 
                 If no Portefeuillehouder is found with this GUID, a new item is created using the child node data (see below). 
                 Existing Portefeuillehouders are not updated using the child node data.
                 This means that to update a Portefeuillehouder item, it needs to be unpublished first.
        
            -->
          <Portefeulliehouder oId="601B0456-A5F0-409B-B1A6-82116E73DE89" />
          
        </Portefeulliehouder>
        
        <!--
    
          A 'Behandelvoorstel_en_Agendapunten' item node may contain one single 'Bijlagen' container node. (optional)
          This node is the container for all attachments of the Vergadering, which are represented by child nodes of four different kinds.
        
          -->
        <Bijlagen>
          
          <!--
          
            A 'Bijlagen' node may contain one or multiple 'Documenten' nodes. (optional)
            The 'Documenten' node represents a document attached to the Agendapunt.
            
            Attributes:
              oId: The document is identified by this attribute. It needs to be unique within the Gemeente's documents. (GUID, required)
                   The database is searched for this GUID and the item attached if found. 
                   If no document is found with this GUID, a new item is created using the child node data (see below). 
                   Existing documents are not updated using the child node data.
                   This means that to update a document, it needs to be unpublished first.
          
            -->
          <Documenten oId="1457A1E0-9E87-4B63-8801-71D100BF6EDA" />
          
          <!--
          
            A 'Bijlagen' node may contain one or multiple 'vergaderdocument' nodes. (optional)
            The 'vergaderdocument' node represents a document attached to the Agendapunt, in reference to the Vergadering.
            
            The function of the 'oId' attribute is identical to the one of 'Documenten' nodes. (see above)
          
            -->
          <vergaderdocument oId="4583E788-8DA3-4C7E-8DBF-E7F42A2F1934" />
          
          <!--
          
            A 'Bijlagen' node may contain one or multiple 'Producten' item nodes. (optional)
            The 'Producten' node represents a product document attached to the Agendapunt.
            
            The function of the 'oId' attribute described for 'Documenten' nodes above also applies here.
          
            -->
          <Producten oId="66BB1B7E-C48E-474D-B176-9CD91FEF86C6">
            
            <!--
            
              Other than 'Documenten' and 'vergaderdocument' nodes, 'Producten' nodes may contain one or multiple further 'Documenten'.
              
              The function of the 'oId' attribute described for 'Documenten' nodes above also applies here.
            
              -->
            <Documenten oId="10085742-77FD-44EC-8303-B29FE1397178" />
            
          </Producten>
          
          <!--
          
            A 'Bijlagen' node may contain one or multiple 'Behandelvoorstel_en_Agendapunten' attachment item nodes. (optional)
            The 'Behandelvoorstel_en_Agendapunten' attachment item node represents an Agendapunt related to the parent Agendapunt.
            
            The function of the 'oId' attribute is equal to the one of 'Documenten' nodes. (see above)
          
            -->
          <Behandelvoorstel_en_Agendapunten oId="8C3F3A9B-71F4-47A5-BB92-BDB4556A1B38" />
          
        </Bijlagen>
        
        <!--
    
          A 'Behandelvoorstel_en_Agendapunten' item node may contain one single 'Producten' container node. (optional)
          This node is the container for all product attachments of the Vergadering, which are represented by child nodes of four different kinds.
        
          -->
        <Producten>
          
          <!--
          
            As 'Bijlagen' container nodes described above, 'Producten' container node can contain one or multiple child nodes 
            of the following kinds:
            
            - 'Documenten' nodes.
            - 'vergaderdocument' nodes.
            - 'Producten' item nodes.
            - 'Behandelvoorstel_en_Agendapunten' attachment item nodes.
            
            For reference about these four kinds, please refer to the section about 'Bijlagen' container nodes. (see above)
          
            -->
            
        </Producten>
        
      </Behandelvoorstel_en_Agendapunten>
      
    </Behandelvoorstel_en_Agendapunten>
    
  </Vergaderingen_Bestuurlijk>
  
</unpublish>
