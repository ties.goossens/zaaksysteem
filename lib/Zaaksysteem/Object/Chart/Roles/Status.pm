package Zaaksysteem::Object::Chart::Roles::Status;

use Moose::Role;
use DateTime::Format::Pg;

=head2 status

Generate chart profile for "registered/completed" cases graph

=cut

sub status {
    my ($self) = @_;

    my $profile = {
        chart => {
            defaultSeriesType => 'line',
        },
        title => { text  => 'Geregistreerd/afgehandeld' },
        yAxis => { title => { text => 'Aantal zaken' }, min => 0 },
    };

    my ($rsr,$rsa,$axis)   = $self->group_geregistreerd();

    die 'Could not find resultset for chart' unless $rsa;

    my $data = [];
    while (my $rsrrow = $rsr->next) {

        my $rsarow      = $rsa->next;
        my $axis_label  = shift(@{ $axis->{x} });

        push(@{ $data },
            {
                day => $axis_label,
                geregistreerd => int($rsrrow->zaken),
                afgehandeld   => int($rsarow->zaken)
            }
        );
    }
    if(@$data) {
        $profile->{xAxis} = {'categories' => [ map {$_->{day}} @$data ]    };

        my $afgehandeld    = { name => 'Afgehandeld', data => [ map {$_->{afgehandeld}} @$data ]};
        my $geregistreerd  = { name => 'Geregistreerd', data => [ map {$_->{geregistreerd}} @$data ]};
        $profile->{series} = [$afgehandeld, $geregistreerd];
    }

    return $profile;
}

# XXX This part was copied from ResultsetZaak.pm, as that's the *wrong* place
# to put graph-specific knowledge, and we're not putting it in that same
# (wrong) place in Object/Data/ResultSet
#
# ===============================

sub group_geregistreerd {
    my $self = shift;
    my $rs = $self->resultset;

# select period.date as period, count(zaak.id) as zaken from (select
# generate_series('2011-07-01'::timestamp, '2011-07-30'::timestamp, '1 week')
# as date) as period left outer join zaak on zaak.created between period.date
# AND (date(period.date) + interval '1 week') and zaak_id IN (QUERY) group by period.date order by period.date;
    delete($rs->{attrs}->{'+as'});
    delete($rs->{attrs}->{'+select'});
    delete($rs->{attrs}->{'prefetch'});
    delete($rs->{attrs}->{'order_by'});

    my ($startdatum,$einddatum,$interval, $interval_label)     = $self->_group_geregistreerd_data($rs, @_);

    return unless ($startdatum && $einddatum);

    ### Define defintion
    my ($geregistreerd_resultset, $afgehandeld_resultset);

    my ($regsource, $afsource, @extraargs);

    ($regsource, @extraargs) = $self->_group_geregistreerd_resultset($rs, 'geregistreerd', $interval);


    ### Get clean resultset
    my $resultset_registratie = $regsource->resultset->search(
        {},
        {
            bind    => [$startdatum,$einddatum, @extraargs],
            order_by => { '-asc' => 'periode' },
        }
    );

    ($afsource, @extraargs) = $self->_group_geregistreerd_resultset($rs, 'afgehandeld', $interval);
    my $resultset_afhandeling = $afsource->resultset->search(
        {},
        {
            bind    => [$startdatum,$einddatum, @extraargs]
        }
    );

    my $axis    = $self->_define_axis($resultset_registratie, $interval_label);

    return ($resultset_registratie,$resultset_afhandeling, $axis);
}

sub _group_geregistreerd_data {
    my $self = shift;
    my ($resultset, $startdatum, $einddatum)    = @_;
    my $interval = '1 day';
    my $interval_label = 'day';

    if (!$startdatum) {
        #delete($resultset->{attrs}->{'group_by'});
        my $zaken = $resultset->search(
            {},
            {
                order_by => { '-asc' => "hstore_to_timestamp((me.index_hstore->'case.date_of_registration'))" },
                group_by => undef,
                select   => ["hstore_to_timestamp((me.index_hstore->'case.date_of_registration'))"],
                'as'     => ['registratiedatum'],
            }
        );

        my $startzaak = $zaken->first;

        return (undef,undef) unless $startzaak;

        $startdatum = DateTime::Format::Pg->parse_datetime($startzaak->get_column('registratiedatum'));

        if ($startdatum->year < 2000) {
            $startdatum = DateTime->now->subtract(years => 1);
        }
    }


    if (!$einddatum) {
        my $zaken = $resultset->search(
            {},
            {
                order_by => { '-desc' => "hstore_to_timestamp((me.index_hstore->'case.date_of_registration'))" },
                group_by => undef,
                select   => ["hstore_to_timestamp((me.index_hstore->'case.date_of_registration'))"],
                'as'     => ['registratiedatum'],
            }
        );
        delete($zaken->{attrs}->{'group_by'});

        my $eindzaak = $zaken->first;

        return (undef,undef) unless $eindzaak;

        $einddatum = DateTime::Format::Pg->parse_datetime($eindzaak->get_column('registratiedatum'));
    }

    ### Do some checks
    if (($einddatum->epoch - $startdatum->epoch) > (365*86400)) {
        # Don't allow intervals > 1 year for performance reasons
        $startdatum = $einddatum->clone->subtract(years => 1);
    }

    if (($einddatum->epoch - $startdatum->epoch) > ((31*86400) * 3)) {
        $interval = '1 month';
        $interval_label = 'month';
    } elsif (($einddatum->epoch - $startdatum->epoch) > (31 * 86400) ) {
        ### groter dan 3 maanden, interval 1 week
        $interval = '1 week';
        $interval_label = 'week';
#    } elsif (($einddatum->epoch - $startdatum->epoch) < 3600) {
#        $interval = '1 minute';
#        $interval_label = 'minute';
    } elsif (($einddatum->epoch - $startdatum->epoch) < 86400) {
        $interval       = '1 hour';
        $interval_label = 'hour';
    }

    return (
        DateTime->new(
            day => $startdatum->day,
            month => $startdatum->month,
            year => $startdatum->year,
        ),
        DateTime->new(
            day     => $einddatum->day,
            month   => $einddatum->month,
            year    => $einddatum->year,
            hour    => 23,
            minute  => 59,
            second  => 59
        ),
        $interval,
        $interval_label
    );
}

sub _group_geregistreerd_resultset {
    my $self        = shift;
    my $resultset   = shift;
    my $replace     = shift;
    my $interval    = shift;

    my ($query, @bindargs);

    my $queryobject   = $resultset->search({}, {
        select      => "object_id",
        group_by    => undef,
        order_by    => undef,
    })->as_query;

    ($query, @bindargs)  = @{ ${ $queryobject } };

    ### Overwrite definition
    my $source                 =
        $resultset->result_source->schema->source('ObjectGrafiek' . ucfirst($replace));

    my $new_definition      = $source->view_definition_template;
    $new_definition         =~ s/INNERQUERY/$query/;
    $new_definition         =~ s/INTERVAL/\'$interval\'/g;

    #$new_definition         =~ s/DATAREPL/\'$interval\'/g;

    $source->view_definition(
        $new_definition
    );

    return ($source, @bindargs);
}

sub _define_axis {
    my $self      = shift;
    my $resultset = shift;
    my $interval  = shift;

    my $axis        = {
        x   => [],
    };

    my $total = $resultset->count;
    my $counter = 0;
    while (my $row = $resultset->next) {
        $counter++;

        my $x;

        if ($interval eq 'month') {
            $row->periode->set_locale('nl_NL');
            if (
                $counter == 1 ||
                $counter == $total ||
                $row->periode->month == 1
            ) {
                $x = $row->periode->year . ': ' . $row->periode->month_name;
            } else {
                $x = $row->periode->month_name;
            }
        } elsif ($interval eq 'week') {
            if (
                $counter == 1 ||
                $counter == $total ||
                $row->periode->week == 1
            ) {
                $x = $row->periode->year . ': ' . $row->periode->week
            } else {
                $x = $row->periode->week;
            }
        } elsif ($interval eq 'minute') {
            $x = $row->periode->hour . ':' . $row->periode->minute;
        } elsif ($interval eq 'hour') {
            if (
                $counter == 1 ||
                $counter == $total ||
                $row->periode->hour == 0
            ) {
                $x = $row->periode->hour . "\n(" . $row->periode->day . '-' .
                $row->periode->month . ')';
            } else {
                $x = $row->periode->hour;
            }
        } elsif ($interval eq 'day') {
            $row->periode->set_locale('nl_NL');
            $x = $row->periode->day . '-' . $row->periode->month;
        }

        push(@{ $axis->{x} }, $x);
    }

    $resultset->reset;

    return $axis;
}

# End copied bit
# ===============================

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 group_geregistreerd

TODO: Fix the POD

=cut

