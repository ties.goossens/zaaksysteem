package Test::DummySignature;

use Moose;

use BTTW::Tools qw[sig];

has fail => (
    is => 'rw'
);

sig test_simple => 'Num => Num';

sub test_simple {
    my ($self, $num) = @_;

    return $self->fail ? 'string' : $num;
}

sig test_noargs => '=> Str';

sub test_noargs {
    my ($self) = @_;

    return $self->fail ? undef : 'string';
}

sig test_noret => 'Defined';

sub test_noret {
    my ($self, $defined) = @_;

    return unless $self->fail;
    return $defined;
}

sig test_slurp_array => 'Num, @Str => Num';

sub test_slurp_array {
    my ($self, $num, @rest) = @_;

    return $self->fail ? '' : scalar @rest;
}

sig test_slurp_typed_array => '@HashRef[Str]';

sub test_slurp_typed_array {
    my ($self, @rest) = @_;

    return scalar @rest;
}

sig test_slurp_hash => 'Num, %Defined => Num';

sub test_slurp_hash {
    my ($self, $num, %hash) = @_;

    return $self->fail ? '' : scalar keys %hash;
}

sig test_slurp_typed_hash => '%ArrayRef[Str]';

sub test_slurp_typed_hash {
    my ($self, %hash) = @_;

    return scalar keys %hash;
}

sig test_optional => 'Num, ?Str, @Num => Num';

sub test_optional {
    my ($self, $num, @rest) = @_;

    return scalar @rest;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
