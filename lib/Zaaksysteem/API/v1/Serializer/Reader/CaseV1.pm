package Zaaksysteem::API::v1::Serializer::Reader::CaseV1;
use Zaaksysteem::Moose;

with 'Zaaksysteem::API::v1::Serializer::ReaderRole';

=head1 NAME

=head1 SYNOPSIS

=head1 METHODS

=head2 class

=cut

sub class { 'Zaaksysteem::Model::DB::CaseV1' }

=head2 read

=cut

sub read {
    my ($class, $serializer, $view) = @_;

    my $uuid = $view->get_column('id');

    my %instance = $view->get_inflated_columns();

    return {
        type      => 'case',
        reference => $uuid,
        preview   => 'case(...' . substr($uuid, -6) . ')',
        instance  => \%instance,
    };
}


__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2021, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
