// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
const { keys } = Object;

export default function createElement(genericIdentifier, attributes = []) {
  const anchor = document.createElement(genericIdentifier.toUpperCase());

  for (const name of keys(attributes)) {
    anchor.setAttribute(name, attributes[name]);
  }

  return anchor;
}
