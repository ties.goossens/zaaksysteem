BEGIN;

  ALTER TABLE filestore ALTER COLUMN size TYPE bigint;

COMMIT;
