package Zaaksysteem::Auth::Credential::LDAP;

use Moose;

use BTTW::Tools;
use Zaaksysteem::Constants;

has _config => ( is => 'ro' );
has app => ( is => 'ro' );
has realm => ( is => 'ro' );
has interface => ( is => 'rw', isa => 'Zaaksysteem::Model::DB::Interface' );

around BUILDARGS => sub {
    my $orig    = shift;
    my $class   = shift;
    my $config  = shift;


    $config->{'password_field'}     ||= 'password';


    $class->$orig(
        _config => $config,
        app => shift,
        realm => shift
    );
};


sub authenticate {
    my ($self, $c, $realm, $authinfo) = @_;

    my $user = $realm->find_user($c, {
        username => $authinfo->{ username },
        source => $self->interface->id
    });

    if (!ref $user) {
        $c->log->debug(
            'Unable to locate user matching user info provided in realm: '
                . $realm->name)
            if $c->debug;
        return;
    }

    my $password = $authinfo->{ $self->_config->{'password_field'} };
    if ($user->login_entity->check_password($password)) {
        return $user;
    }
}

1;




__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 authenticate

TODO: Fix the POD

=cut

=head2 config

TODO: Fix the POD

=cut

=head2 search_user_dn

TODO: Fix the POD

=cut

