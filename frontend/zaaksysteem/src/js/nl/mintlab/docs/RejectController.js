// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

angular
  .module('Zaaksysteem.docs')
  .controller('nl.mintlab.docs.RejectController', [
    '$scope',
    function ($scope) {
      $scope.reject = function (entities) {
        $scope.rejectFile(
          entities,
          $scope.rejection_reason,
          $scope.reject_to_queue
        );
        $scope.closePopup();
      };
    },
  ]);
